<?
ob_start();
require_once("about_comedix.php");
require_once("yui_calendar_util.ini");
require_once("fplus_common.ini");
ob_end_clean();

$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$fplusadm_auth = check_authority($session, 79, $fname);
$con = @connect2db($fname);
if(!$session || !$fplusadm_auth || !$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}

$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';
$epi_title_a = "A 針刺し切創事故レポート";
$epi_title_b = "B 皮膚粘膜汚染レポート";

$epi_title_ao = "AO 針刺し切創事故レポート";
$epi_title_bo = "BO 皮膚粘膜汚染レポート";

//====================================================================
// 縦横列フィールド定義
//====================================================================

//------------------
// 横列フィールドの定義
//------------------
$horz_field_ab_definision = array(
	"a" => array(
		array("dbkey"=> "data_10_2_tools_needle,data_10_6_tools_ope,data_10_9_tools_glass", "text" => "原因器材"),
		array("dbkey"=> "data_10_1_tools",        "text" => "原因器材(種別単位)"),
		array("dbkey"=> "data_2_1_arise_date",    "text" => "月")
	),
	"b" => array(
		array("dbkey"=> "data_6_1_fluid_blood", "text" => "汚染した体液"),
		array("dbkey"=> "data_2_1_arise_date",  "text" => "月")
	),
        "ao" => array(
		array("dbkey"=> "data_13_2_tools", "text" => "原因器材"),
		//array("dbkey"=> "data_10_1_tools",        "text" => "原因器材(種別単位)"),
		array("dbkey"=> "data_2_1_arise_date",    "text" => "月")
	),
        "bo" => array(
		array("dbkey"=> "data_9_1_fluid_blood", "text" => "汚染した体液"),
		array("dbkey"=> "data_2_1_arise_date",  "text" => "月")
	)
);


//------------------
// 縦列フィールドの定義
//------------------
$vert_field_ab_definision = array(
	"a" => array(
		array("dbkey"=> "data_1_2_belong_dept",          "text" => "部門"),
		array("dbkey"=> "data_1_9_exp_year",             "text" => "経験年数"),
		array("dbkey"=> "data_3_1_job",                  "text" => "職種"),
		array("dbkey"=> "data_3_2_hospital_dept",        "text" => "診療科"),
		array("dbkey"=> "data_4_1_arise_area",           "text" => "発生場所"),
		array("dbkey"=> "data_4_2_arise_ward",           "text" => "事例発生場所詳細（病棟）"),
		array("dbkey"=> "data_4_3_arise_dept",           "text" => "事例発生場所詳細（診療科）"),
		array("dbkey"=> "data_5_1_patient_decision",     "text" => "患者確定"),
		array("dbkey"=> "data_5_5_patient_hiv",          "text" => "確定患者の検査結果"),
		array("dbkey"=> "data_6_1_user_select",          "text" => "器材の選択・使用者"),
		array("dbkey"=> "data_6_3_user_owner",           "text" => "器材の所持者"),
		array("dbkey"=> "data_7_1_pollution",            "text" => "器材の汚染"),
		array("dbkey"=> "data_8_1_use_view",             "text" => "使用目的"),
		array("dbkey"=> "data_9_1_situation",            "text" => "事例発生状況"),
		array("dbkey"=> "data_10_2_tools_needle,data_10_6_tools_ope,data_10_9_tools_glass",  "text" => "器材"),
		array("dbkey"=> "data_10_1_tools",               "text" => "器材（種別単位）"),
		array("dbkey"=> "data_11_1_safe_tls",            "text" => "安全器材"),
		array("dbkey"=> "data_11_3_safe_tls_precaution", "text" => "安全装置作動有無"),
		array("dbkey"=> "data_11_4_safe_period",         "text" => "受傷の時期"),
		array("dbkey"=> "data_11_5_safe_func",           "text" => "安全機能の可否"),
		array("dbkey"=> "data_12_1_injury_deep",         "text" => "受傷部位"),
		array("dbkey"=> "data_13_1_injury_degree",       "text" => "受傷の程度"),
		array("dbkey"=> "data_14_1_injury_glove",        "text" => "手袋の着用"),
		array("dbkey"=> "data_15_1_hbs_positivity",      "text" => "HBｓ抗体"),
		array("dbkey"=> "data_16_1_emergency_injury",    "text" => "緊急処置時受傷"),
		array("dbkey"=> "data_2_1_arise_date",           "text" => "年度")
	),
	"b" => array(
		array("dbkey"=> "data_1_2_belong_dept",          "text" => "部門"),
		array("dbkey"=> "data_1_9_exp_year",             "text" => "経験年数"),
		array("dbkey"=> "data_3_1_job",                  "text" => "職種"),
		array("dbkey"=> "data_3_2_hospital_dept",        "text" => "診療科"),
		array("dbkey"=> "data_4_1_arise_area",           "text" => "発生場所"),
		array("dbkey"=> "data_4_2_arise_ward",           "text" => "事例発生場所詳細（病棟）"),
		array("dbkey"=> "data_4_3_arise_dept",           "text" => "事例発生場所詳細（診療科）"),
		array("dbkey"=> "data_5_1_patient_decision",     "text" => "患者確定"),
		array("dbkey"=> "data_5_5_patient_hiv",          "text" => "確定患者の検査結果"),
		array("dbkey"=> "data_6_1_fluid_blood",          "text" => "汚染した体液"),
		array("dbkey"=> "data_7_1_part_clean_skin",      "text" => "汚染組織・状態"),
		array("dbkey"=> "data_8_1_touch",                "text" => "汚染時の状況"),
		array("dbkey"=> "data_9_1_cloth_no_glove",       "text" => "汚染時の防衣・防具"),
		array("dbkey"=> "data_10_1_how_arise",        "text" => "汚染理由"),
		array("dbkey"=> "data_11_1_how_long",         "text" => "汚染時間"),
		array("dbkey"=> "data_12_1_how_much",         "text" => "接触した量"),
		array("dbkey"=> "data_13_1_injury_deep",      "text" => "接触した部位"),
		array("dbkey"=> "data_14_1_hbs_positivity",   "text" => "HBｓ抗体"),
		array("dbkey"=> "data_15_1_emergency_hurt",   "text" => "緊急処置時受傷"),
		array("dbkey"=> "data_2_1_arise_date",        "text" => "年度")
	),
        "ao" => array(
		array("dbkey"=> "data_1_2_belong_dept",          "text" => "部門"),
		array("dbkey"=> "data_1_9_exp_year",             "text" => "経験年数"),
		array("dbkey"=> "data_5_1_job",                  "text" => "職種"),
		array("dbkey"=> "data_3_1_hospital_dept",        "text" => "診療科"),
		array("dbkey"=> "data_7_1_arise_area",           "text" => "発生場所"),
		array("dbkey"=> "data_8_1_patient_decision",     "text" => "患者確定"),
		array("dbkey"=> "data_8_5_patient_hiv",          "text" => "確定患者の検査結果"),
		array("dbkey"=> "data_11_1_user_select",          "text" => "器材の選択・使用者"),
		array("dbkey"=> "data_12_1_pollution",            "text" => "器材の汚染"),
		array("dbkey"=> "data_9_1_use_view",             "text" => "使用目的"),
		array("dbkey"=> "data_10_1_situation",            "text" => "事例発生状況"),
		array("dbkey"=> "data_13_2_tools",         "text" => "器材"),
		//array("dbkey"=> "data_13_2_tools",               "text" => "器材（種別単位）"),
		array("dbkey"=> "data_14_1_safe_tls",            "text" => "安全器材"),
		array("dbkey"=> "data_14_2_safe_tls_precaution", "text" => "安全装置作動有無"),
		array("dbkey"=> "data_14_3_safe_period",         "text" => "受傷の時期"),
		array("dbkey"=> "data_21_1_safe_func",           "text" => "安全機能の可否"),
                array("dbkey"=> "data_15_1_injury_deep",         "text" => "受傷部位"),
		array("dbkey"=> "data_16_1_injury_degree",       "text" => "受傷の程度"),
		array("dbkey"=> "data_17_1_injury_glove",        "text" => "手袋の着用"),
		array("dbkey"=> "data_19_1_hbs_positivity",      "text" => "HBｓ抗体"),
		array("dbkey"=> "data_2_1_arise_date",           "text" => "年度")
	),	
        "bo" => array(
		array("dbkey"=> "data_1_2_belong_dept",          "text" => "部門"),
		array("dbkey"=> "data_1_9_exp_year",             "text" => "経験年数"),
		array("dbkey"=> "data_5_1_job",                  "text" => "職種"),
		array("dbkey"=> "data_3_1_hospital_dept",        "text" => "診療科"),
		array("dbkey"=> "data_7_1_arise_area",           "text" => "発生場所"),
		array("dbkey"=> "data_8_1_patient_decision",     "text" => "患者確定"),
		array("dbkey"=> "data_8_5_patient_hiv",          "text" => "確定患者の検査結果"),
		array("dbkey"=> "data_9_1_fluid_blood",          "text" => "汚染した体液"),
		array("dbkey"=> "data_10_1_part_clean_skin",      "text" => "汚染組織・状態"),
		array("dbkey"=> "data_11_1_touch",                "text" => "汚染時の状況"),
		array("dbkey"=> "data_12_1_cloth_no_glove",       "text" => "汚染時の防衣・防具"),
		array("dbkey"=> "data_13_1_how_arise",        "text" => "汚染理由"),
		array("dbkey"=> "data_14_1_how_long",         "text" => "汚染時間"),
		array("dbkey"=> "data_15_1_how_much",         "text" => "接触した量"),
		array("dbkey"=> "data_16_1_injury_deep",      "text" => "接触した部位"),
		array("dbkey"=> "data_17_1_hbs_positivity",   "text" => "HBｓ抗体"),
		array("dbkey"=> "data_2_1_arise_date",        "text" => "年度")
	)
);



//====================================================================
// 汎用マスタを取得する。プログラムからはエピシス項目名でアクセスできるようにする。
//====================================================================
fplus_common_maerge_general_master_to_episys_master("A401", "data_3_1_job");
fplus_common_maerge_general_master_to_episys_master("A413", "data_5_1_job");
fplus_common_maerge_general_master_to_episys_master("A402", "data_3_2_hospital_dept");
fplus_common_maerge_general_master_to_episys_master("A403", "data_4_1_arise_area");
fplus_common_maerge_general_master_to_episys_master("A410", "data_1_2_belong_dept");
fplus_common_maerge_general_master_to_episys_master("A404", "data_4_2_arise_ward");
fplus_common_maerge_general_master_to_episys_master("A411", "data_7_1_arise_area");
fplus_common_maerge_general_master_to_episys_master("A412", "data_3_1_hospital_dept");

//====================================================================
// パラメータ取得
//====================================================================
// epiタイプ "a" or "b" が入る
if($_REQUEST["epi_type"]=="b"){
    $epi_type = "b";
    $epi_title = $epi_title_b;
}else if($_REQUEST["epi_type"]=="ao"){
    $epi_type = "ao";
    $epi_title = $epi_title_ao;
}else if($_REQUEST["epi_type"]=="bo"){
    $epi_type = "bo";
    $epi_title = $epi_title_bo;
}else{
    $epi_type = "a";
    $epi_title = $epi_title_a;
}
//$epi_type = (@$_REQUEST["epi_type"]=="b" ? "b" : "a");

//$epi_title = ($epi_type=="a" ? $epi_title_a : $epi_title_b);

// A/Bタイプ別の、縦横軸項目定義を取得
$horz_field_info = $horz_field_ab_definision[$epi_type]; // 列の定義
$vert_field_info = $vert_field_ab_definision[$epi_type]; // 行の定義

// 指定された縦横軸項目内のインデックス
$sel_horz_field_idx = (int)@$_REQUEST["sel_horz_field_idx"]; // 選択中の横軸項目インデックス番号(0〜)
$sel_vert_field_idx = (int)@$_REQUEST["sel_vert_field_idx"]; // 選択中の縦軸項目インデックス番号(0〜)

// 指定された縦横軸項目の名前
$horz_text = $horz_field_info[$sel_horz_field_idx]["text"];
$vert_text = $vert_field_info[$sel_vert_field_idx]["text"];

// 指定された縦横軸項目のデータベース上のフィールド名
$horz_dbkey = $horz_field_info[$sel_horz_field_idx]["dbkey"];
$vert_dbkey = $vert_field_info[$sel_vert_field_idx]["dbkey"];


//設定項目を取得する
$item_list = "";
$sql = "select * from fplus_etc_setting";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$item_list = pg_fetch_all($sel);

//検索実行時
if($item_list[0]["search_approve_date_save"] == "t")
{

	if ($search_mode == "1")
	{
		//保存されている検索用報告日を更新する
		
		//前の日付
		$set_f_date = sprintf("%04d", $_REQUEST["sel_d1_y"]).sprintf("%02d", $_REQUEST["sel_d1_m"]).sprintf("%02d", $_REQUEST["sel_d1_d"]);
		
		//後ろの日付
		$set_r_date = sprintf("%04d", $_REQUEST["sel_d2_y"]).sprintf("%02d", $_REQUEST["sel_d2_m"]).sprintf("%02d", $_REQUEST["sel_d2_d"]);
		
		$sql = "update fplus_etc_setting set";
		$set = array("search_epinet_start_date","search_epinet_end_date");
		$setvalue = array(	$set_f_date,$set_r_date);
		$cond = "";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con,"rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
		
	$item_list = "";
	$sql = "select * from fplus_etc_setting";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$item_list = pg_fetch_all($sel);
	
	if($item_list[0]["search_epinet_start_date"] != null)
	{
		$front_date = substr($item_list[0]["search_epinet_start_date"],0,4);
		//前の日付
		$sel_d1_y = substr($item_list[0]["search_epinet_start_date"],0,4);
		$sel_d1_m = substr($item_list[0]["search_epinet_start_date"],4,2);
		$sel_d1_d = substr($item_list[0]["search_epinet_start_date"],6,2);
	}
	
	if($item_list[0]["search_epinet_end_date"] != null)
	{
		$rear_date = $item_list[0]["search_epinet_end_date"];
		//後ろの日付
		$sel_d2_y = substr($item_list[0]["search_epinet_end_date"],0,4);
		$sel_d2_m = substr($item_list[0]["search_epinet_end_date"],4,2);
		$sel_d2_d = substr($item_list[0]["search_epinet_end_date"],6,2);
	}
}
else
{
	// 指定された日付
	$sel_d1_y = (int)@$_REQUEST["sel_d1_y"];
	$sel_d1_m = (int)@$_REQUEST["sel_d1_m"];
	$sel_d1_d = (int)@$_REQUEST["sel_d1_d"];
	$sel_d2_y = (int)@$_REQUEST["sel_d2_y"];
	$sel_d2_m = (int)@$_REQUEST["sel_d2_m"];
	$sel_d2_d = (int)@$_REQUEST["sel_d2_d"];
}

// 指定された 所属部門 / 職種 / 発生場所のコード
$sel_belong_dept = @$_REQUEST["sel_belong_dept"];

//$sel_job =         @$_REQUEST["sel_job"];
$sql = " select *  ".
	" from tmplitem ".
	" where mst_cd = 'A401' order by disp_order";
$sel = @select_from_table($con, $sql, "", $fname);
$selall_shokushu = pg_fetch_all($sel);



$sel_arise_area =  @$_REQUEST["sel_arise_area"];

// 件数のあるもののみ表示
$is_shrink_horz = $_REQUEST["is_shrink_horz"] ? true : false;
$is_shrink_vert = $_REQUEST["is_shrink_vert"] ? true : false;

// 印刷（エクセル出力）ボタンを押した場合は、以下の値が"1"で入る
$is_print_mode = @$_REQUEST["print_mode"];

// グラフ対象とするのは縦軸("V"ertical) か横軸("V"以外)か
$graphVertOrHorz = (@$_REQUEST["graph_vert_or_horz"]!="V" ? "H" : "V");

// グラフ対象行（対象列）のキーID（値なし、またはゼロのとき、合計値をグラフ化することを意味する）
$graphVertOrHorzId = (int)@$_REQUEST["graph_vert_or_horz_id"];

// 日付けがなければ、一ヶ月前から本日にセット
if (!checkdate((int)$sel_d1_m, (int)$sel_d1_d, (int)$sel_d1_y)){
	$sel_d1_y = date("Y", strtotime(date("Y/m/d")." -1 month"));
	$sel_d1_m = date("m", strtotime(date("Y/m/d")." -1 month"));
	$sel_d1_d = date("d", strtotime(date("Y/m/d")." -1 month"));
}
if (!checkdate((int)$sel_d2_m, (int)$sel_d2_d, (int)$sel_d2_y)){
	$sel_d2_y = date("Y"); $sel_d2_m = (int)date("m"); (int)$sel_d2_d = date("d");
}



function array_full_merge($a_base, $a_plus){
	foreach($a_plus as $idx => $row) $a_base[] = $row;
	return $a_base;
}


//====================================================================
// 横軸のヘッダリストを作成する
//====================================================================
$horz_headers = array();
// 月の場合
//2015.12.24 fuUniv by Y.Yamamoto 
if ($horz_dbkey=="data_2_1_arise_date"){
	$mon = $sel_d1_m-1;
        $year = $sel_d1_y;
	for ($i = 1; $i <= 12; $i++){
		$mon++; 
                if ($mon > 12) {
                    $mon = 1;
                     $year++;
                }
                if ($vert_dbkey == "data_2_1_arise_date") {
                    $horz_headers[] = array("record_id"=>sprintf("%02d",$mon), "record_caption"=>$mon."月", "record_id_path"=>$horz_dbkey."@".sprintf("%02d",$mon));

                }else{
                    $horz_headers[] = array("record_id"=>sprintf("%02d",$mon), "record_caption"=>$year."年".$mon."月", "record_id_path"=>$horz_dbkey."@".sprintf("%02d",$mon));
                }
	}
}
// Ａタイプ：原因器材(種別単位)のときは、横軸は３種のマスタリストを混合
else if ($horz_dbkey == "data_10_2_tools_needle,data_10_6_tools_ope,data_10_9_tools_glass"){
	$horz_headers = fplus_common_get_episys_master_field_list("data_10_2_tools_needle", "【針】", true);
	$horz_headers = array_full_merge($horz_headers, fplus_common_get_episys_master_field_list("data_10_6_tools_ope", "【手術器材等】", true));
	$horz_headers = array_full_merge($horz_headers, fplus_common_get_episys_master_field_list("data_10_9_tools_glass", "【ガラス製品材】", true));
}
// その他なら、マスタからリストを取得
else {
	$horz_headers = fplus_common_get_episys_master_field_list($horz_dbkey, "", true);
}




//====================================================================
// 縦軸のヘッダリストを作成する
//====================================================================
$vert_headers = array();

// 「年度」指定の場合は、年度期間作成のため、ＤＢより最大と最小を取得する。データがなくとも最低、今年の行を含める。
// 最大年度の月が１から３月までなら、マイナス１して前年度までとする
// 最小年度の月が１から３月までならマイナス１して前年度からとする
if ($vert_dbkey == "data_2_1_arise_date") {
	$sql =
		" select max(data_2_1_arise_date) as maxdate, min(data_2_1_arise_date) as mindate".
		" from fplus_epi_".$epi_type.
		" inner join fplusapply on (".
		"   fplusapply.apply_id = fplus_epi_".$epi_type.".apply_id".
		"   and fplusapply.delete_flg = 'f'".
		"   and fplusapply.apply_stat in ('0','1')".
		"   and fplusapply.draft_flg = 'f'".
		" )";
	$sel = @select_from_table($con, $sql, "", $fname);
	$row = @pg_fetch_array($sel);
	$year = (int)substr(@$row["maxdate"],0,4);
	$mon  = (int)substr(@$row["maxdate"],5,2);
	if ($mon < 4) $year--;
	$max_year = max(date("Y"), $year);
	$year = (int)substr(@$row["mindate"],0,4);
	$mon  = (int)substr(@$row["mindate"],5,2);
	if ($mon < 4) $year--;
	$min_year = max(2000, $year);
	while ($min_year <= $max_year) {
		$vert_headers[] = array("record_id"=>$min_year, "record_caption"=>$min_year."年", "record_id_path"=>$vert_dbkey."@".$min_year);
		$min_year++;
	}
}

// 「Ａタイプ：確定患者の検査結果」のときは、複数マスタを連結
else if ($vert_dbkey == "data_5_5_patient_hiv"){
	$vert_headers = fplus_common_get_episys_master_field_list("data_5_5_patient_hiv", "【HIV】", true);
	$vert_headers = array_full_merge($vert_headers, fplus_common_get_episys_master_field_list("data_5_6_patient_hcv", "【HCV】", true));
	$vert_headers = array_full_merge($vert_headers, fplus_common_get_episys_master_field_list("data_5_7_patient_hbs", "【HBｓ抗原】", true));
	$vert_headers = array_full_merge($vert_headers, fplus_common_get_episys_master_field_list("data_5_8_patient_hbe", "【HBｅ抗原】", true));
	$vert_headers = array_full_merge($vert_headers, fplus_common_get_episys_master_field_list("data_5_9_patient_syphilis", "【梅毒】", true));
	$vert_headers = array_full_merge($vert_headers, fplus_common_get_episys_master_field_list("data_5_10_patient_atla", "【ATLA(HTLV-1)】", true));
}

// 「Ａタイプ：器材」のときは、３種のマスタリストを混合
else if ($vert_dbkey == "data_10_2_tools_needle,data_10_6_tools_ope,data_10_9_tools_glass") {
	$vert_headers = fplus_common_get_episys_master_field_list("data_10_2_tools_needle", "【針】", true);
	$vert_headers = array_full_merge($vert_headers, fplus_common_get_episys_master_field_list("data_10_6_tools_ope", "【手術器材等】", true));
	$vert_headers = array_full_merge($vert_headers, fplus_common_get_episys_master_field_list("data_10_9_tools_glass", "【ガラス製品材】", true));
}

// 「Ａタイプ：使用目的」のときは、使用目的マスタのうち、静脈と動脈フィールドを別のマスタにネストしつつ置換する
else if ($vert_dbkey == "data_8_1_use_view"){
	$vert_headers1 = fplus_common_get_episys_master_field_list("data_8_1_use_view", "", true);
	$vert_headers2 = fplus_common_get_episys_master_field_list("data_8_2_use_view_vein", "", true);
	$vert_headers3 = fplus_common_get_episys_master_field_list("data_8_3_use_view_artery", "", true);
	$vert_headers = array();
	foreach ($vert_headers1 as $idx1 => $row1){
		if ($row1["record_id"]=="07") { // 静脈採血
			foreach ($vert_headers2 as $idx2 => $row2){
				$id = $row1["record_id"] . "@" . $row2["record_id"];
				$caption = $row1["record_caption"] . "：" . $row2["record_caption"];
				$record_id_path = $row1["record_id_path"] . "@" . $row2["record_id"];
				$vert_headers[] = array("record_id"=>$row2["record_id"], "record_caption"=>$caption, "record_id_path"=>$record_id_path);
			}
		}
		else if ($row1["record_id"]=="08") { // 動脈採血
			foreach ($vert_headers3 as $idx3 => $row3){
				$id = $row1["record_id"] . "@" . $row3["record_id"];
				$caption = $row1["record_caption"] . "：" . $row3["record_caption"];
				$record_id_path = $row1["record_id_path"] . "@" . $row3["record_id"];
				$vert_headers[] = array("record_id"=>$row3["record_id"], "record_caption"=>$caption, "record_id_path"=>$record_id_path);
			}
		}
		else {
			$vert_headers[] = array("record_id"=>$row1["record_id"], "record_caption"=>$row1["record_caption"], "record_id_path"=>$row1["record_id_path"]);
		}
	}
}
// その他なら、マスタからリストを取得
else {
	$vert_headers = fplus_common_get_episys_master_field_list($vert_dbkey, "", true);
}



//====================================================================
// DB検索
//====================================================================

//$is_shrink_horz = ($horz_dbkey == "data_10_2_tools_needle,data_10_6_tools_ope,data_10_9_tools_glass" ? 1 : 0);

$datalist = array();


//**************************
// 検索対象追加フィールド
//**************************
$add_field_sql = "";
// 「Ａタイプ：確定患者の検査結果」のときは、取得フィールドは複数存在
if ($vert_dbkey == "data_5_5_patient_hiv"){
	$add_field_sql .= ", data_5_6_patient_hcv, data_5_7_patient_hbs, data_5_8_patient_hbe, data_5_9_patient_syphilis, data_5_10_patient_atla";
}
// 「Ａタイプ：使用目的」のときは、取得フィールドは複数存在
if ($vert_dbkey == "data_8_1_use_view"){
	$add_field_sql .= ", data_8_2_use_view_vein, data_8_3_use_view_artery";
}
// 「Ａタイプ：受傷部位」のときは、取得フィールドは複数存在
if ($vert_dbkey == "data_12_1_injury_deep"){
	$add_field_sql .= ", data_12_2_injury_middle, data_12_3_injury_superficial";
}
// 「Ｂタイプ：受傷部位」のときは、取得フィールドは複数存在
if ($vert_dbkey == "data_13_1_injury_deep"){
	$add_field_sql .= ", data_13_2_injury_middle, data_13_3_injury_superficial";
}
// 「Ｂタイプ（縦横軸どちらか）：汚染した体液」のときは、取得フィールドは複数存在
if ($vert_dbkey == "data_6_1_fluid_blood" || $horz_dbkey == "data_6_1_fluid_blood"){
	$add_field_sql .=
		", data_6_2_fluid_vomit, data_6_3_fluid_phlegm, data_6_4_fluid_saliva, data_6_5_fluid_spinal_cord, data_6_6_fluid_ascites".
		", data_6_7_fluid_pleural, data_6_8_fluid_amniotic, data_6_9_fluid_urine, data_6_10_fluid_etc";
}
// 「Ｂタイプ：汚染組織・状態」のときは、取得フィールドは複数存在
if ($vert_dbkey == "data_7_1_part_clean_skin"){
	$add_field_sql .= ", data_7_2_part_blemished_skin, data_7_3_part_eye, data_7_4_part_nose, data_7_5_part_mouth, data_7_6_part_etc";
}
// 「Ｂタイプ：汚染時の防衣・防具」のときは、取得フィールドは複数存在
if ($vert_dbkey == "data_9_1_cloth_no_glove"){
	$add_field_sql .=
		", data_9_2_cloth_one_glove, data_9_4_cloth_double_glove, data_9_6_cloth_goggle, data_9_7_cloth_glass".
		", data_9_8_cloth_glass_aside, data_9_9_cloth_face_shield, data_9_10_cloth_ope_mask, data_9_11_cloth_glass_mask".
		", data_9_12_cloth_ope_gown, data_9_13_cloth_gown, data_9_14_cloth_experiment, data_9_15_cloth_exp_etc, data_9_17_cloth_etc";
}


//**************************
// 検索
//**************************
$sql =
	" select count(*) as cnt, " . pg_escape_string($vert_dbkey). "," . pg_escape_string($horz_dbkey) . $add_field_sql.
	" from fplus_epi_".$epi_type.
	" inner join fplusapply on (".
	"   fplusapply.apply_id = fplus_epi_".$epi_type.".apply_id".
	"   and fplusapply.delete_flg = 'f'".
	"   and fplusapply.apply_stat in ('0','1')".
	"   and fplusapply.draft_flg = 'f'".
	" )".
	" where (1=1)";
if ($sel_d1_y)        $sql .= " and data_2_1_arise_date >= '".sprintf("%04d/%02d/%02d",$sel_d1_y,$sel_d1_m,$sel_d1_d)."'"; // 開始日
if ($sel_d2_y)        $sql .= " and data_2_1_arise_date <= '".sprintf("%04d/%02d/%02d",$sel_d2_y,$sel_d2_m,$sel_d2_d)."'"; // 終了日
if ($sel_belong_dept) $sql .= " and data_1_2_belong_dept = '".pg_escape_string($sel_belong_dept)."'"; // 所属部門



for($cnt_shokushu = 0;$cnt_shokushu < count($chk_shokushu);$cnt_shokushu++)
{
	if(($cnt_shokushu == 0) &&(1 < count($chk_shokushu)))//選択された職種が複数件の場合
	{
		$sql .= " and (data_3_1_job = '".pg_escape_string($chk_shokushu[$cnt_shokushu])."'"; // 職種
	}
	elseif(($cnt_shokushu == 0) &&(1 == count($chk_shokushu)))//選択された職種が１件の場合
	{
		$sql .= " and data_3_1_job = '".pg_escape_string($chk_shokushu[$cnt_shokushu])."'"; // 職種
	}
	elseif($cnt_shokushu == (count($chk_shokushu) - 1))//選択された職種が複数件の場合
	{
		$sql .= " or data_3_1_job = '".pg_escape_string($chk_shokushu[$cnt_shokushu])."' ) "; // 職種
	}
	else//選択された職種が複数件の場合
	{
		$sql .= " or data_3_1_job = '".pg_escape_string($chk_shokushu[$cnt_shokushu])."'"; // 職種
	}
}

//if ($sel_job)         $sql .= " and data_3_1_job = '".pg_escape_string($sel_job)."'"; // 職種

if($epi_type=="a" || $epi_type=="b"){
    if ($sel_arise_area)  $sql .= " and data_4_1_arise_area = '".pg_escape_string($sel_arise_area)."'"; // 発生場所
}else if($epi_type=="ao" || $epi_type=="bo"){
    if ($sel_arise_area)  $sql .= " and data_7_1_arise_area = '".pg_escape_string($sel_arise_area)."'"; // 発生場所
}

if ($vert_dbkey == "data_4_2_arise_ward") $sql .= " and (data_4_2_arise_ward is not null and data_4_2_arise_ward <> '')"; // 事例発生場所詳細（病棟）
if ($vert_dbkey == "data_4_3_arise_dept") $sql .= " and (data_4_3_arise_dept is not null and data_4_3_arise_dept <> '')"; // 事例発生場所詳細（診療科）
if ($vert_dbkey == "data_11_3_safe_tls_precaution") $sql .= " and data_11_1_safe_tls = '01'"; // 安全装置作動有無のときは、安全器材チェックありのみ
if ($vert_dbkey == "data_11_4_safe_period")         $sql .= " and data_11_1_safe_tls = '01'"; // 受傷の時期のときは、安全器材チェックありのみ
if ($vert_dbkey == "data_11_5_safe_func")           $sql .= " and data_11_1_safe_tls = '01'"; // 安全機能の可否のときは、安全器材チェックありのみ

$sql .= " group by " . pg_escape_string($vert_dbkey) . "," . pg_escape_string($horz_dbkey) . $add_field_sql;
$sel = @select_from_table($con, $sql, "", $fname);
$selall = pg_fetch_all($sel);




//====================================================================
// データ表マトリクスを作成するための、縦横インデックス配列を作成しておく。
//====================================================================
$idx = 0;
$vert_headers_sortnumbers = array();
foreach($vert_headers as $idx => $row){
	$vert_headers_sortnumbers[$row["record_id_path"]] = $idx;
	$idx++;
}
$idx = 0;
$horz_headers_sortnumbers = array();
foreach($horz_headers as $idx => $row){
	$horz_headers_sortnumbers[$row["record_id_path"]] = $idx;
	$idx++;
}



//====================================================================
// データ表マトリクスを作成する
//====================================================================
foreach($selall as $idx => $row){

	//==================================
	// 横軸について
	//==================================
	$loop_cols = array();

	// 月の場合
	if ($horz_dbkey == "data_2_1_arise_date") {
		$loop_cols[] = $horz_headers_sortnumbers[$horz_dbkey."@".substr($row[$horz_dbkey], 5, 2)];
	}

	//「Ｂタイプ：汚染した体液」の場合は、取得フィールドは複数存在
	else if ($horz_dbkey == "data_6_1_fluid_blood"){
		if ($row["data_6_1_fluid_blood"])       $loop_cols[] = @$horz_headers_sortnumbers[$horz_dbkey."@01"];
		if ($row["data_6_2_fluid_vomit"])       $loop_cols[] = @$horz_headers_sortnumbers[$horz_dbkey."@02"];
		if ($row["data_6_3_fluid_phlegm"])      $loop_cols[] = @$horz_headers_sortnumbers[$horz_dbkey."@03"];
		if ($row["data_6_4_fluid_saliva"])      $loop_cols[] = @$horz_headers_sortnumbers[$horz_dbkey."@04"];
		if ($row["data_6_5_fluid_spinal_cord"]) $loop_cols[] = @$horz_headers_sortnumbers[$horz_dbkey."@05"];
		if ($row["data_6_6_fluid_ascites"])     $loop_cols[] = @$horz_headers_sortnumbers[$horz_dbkey."@06"];
		if ($row["data_6_7_fluid_pleural"])     $loop_cols[] = @$horz_headers_sortnumbers[$horz_dbkey."@07"];
		if ($row["data_6_8_fluid_amniotic"])    $loop_cols[] = @$horz_headers_sortnumbers[$horz_dbkey."@08"];
		if ($row["data_6_9_fluid_urine"])       $loop_cols[] = @$horz_headers_sortnumbers[$horz_dbkey."@09"];
		if ($row["data_6_10_fluid_etc"])        $loop_cols[] = @$horz_headers_sortnumbers[$horz_dbkey."@99"];
	}

	// 「Ａタイプ：器材」の場合は、取得フィールドは複数存在
	else if ($horz_dbkey == "data_10_2_tools_needle,data_10_6_tools_ope,data_10_9_tools_glass"){
		if ($row["data_10_2_tools_needle"]!="") {
			$loop_cols[] = @$horz_headers_sortnumbers["data_10_2_tools_needle@".$row["data_10_2_tools_needle"]];
		}
		if ($row["data_10_6_tools_ope"]!="") {
			$loop_cols[] = @$horz_headers_sortnumbers["data_10_6_tools_ope@".$row["data_10_6_tools_ope"]];
		}
		if ($row["data_10_9_tools_glass"]!="") {
			$loop_cols[] = @$horz_headers_sortnumbers["data_10_9_tools_glass@".$row["data_10_9_tools_glass"]];
		}
	}

	// 通常
	else {
		$loop_cols[] = $horz_headers_sortnumbers[$horz_dbkey."@".$row[$horz_dbkey]];
	}


	//==================================
	// 縦軸について（横軸ごとにループ）
	//==================================
	foreach($loop_cols as $colidx){

		// 年度の場合
		if ($vert_dbkey == "data_2_1_arise_date") {
			$mon = (int)substr($row[$vert_dbkey], 5, 2);
			$year = (int)substr($row[$vert_dbkey], 0, 4);
			$row_field_id = ($mon < 4) ? $year-1 : $year;
			$rowidx = $vert_headers_sortnumbers[$vert_dbkey."@".$row_field_id];
			@$datalist[$rowidx][$colidx] += $row["cnt"];
		}

		// 「Ａタイプ：器材」の場合は、取得フィールドは複数存在
		else if ($vert_dbkey == "data_10_2_tools_needle,data_10_6_tools_ope,data_10_9_tools_glass"){
			if ($row["data_10_2_tools_needle"]!=""){
				$rowidx = @$vert_headers_sortnumbers["data_10_2_tools_needle@".$row["data_10_2_tools_needle"]];
				if ($rowidx!=="") @$datalist[$rowidx][$colidx] += $row["cnt"];
			}
			if ($row["data_10_6_tools_ope"]!=""){
				$rowidx = @$vert_headers_sortnumbers["data_10_6_tools_ope@".$row["data_10_6_tools_ope"]];
				if ($rowidx!=="") @$datalist[$rowidx][$colidx] += $row["cnt"];
			}
			if ($row["data_10_9_tools_glass"]!=""){
				$rowidx = @$vert_headers_sortnumbers["data_10_9_tools_glass@".$row["data_10_9_tools_glass"]];
				if ($rowidx!=="") @$datalist[$rowidx][$colidx] += $row["cnt"];
			}
		}

		// 「Ａタイプ：確定患者の検査結果」の場合は、取得フィールドは複数存在
		else if ($vert_dbkey == "data_5_5_patient_hiv"){
			if ($row["data_5_5_patient_hiv"]!=""){
				$rowidx = @$vert_headers_sortnumbers["data_5_5_patient_hiv@".$row["data_5_5_patient_hiv"]];
				if ($rowidx!=="") @$datalist[$rowidx][$colidx] += $row["cnt"];
			}
			if ($row["data_5_6_patient_hcv"]!=""){
				$rowidx = @$vert_headers_sortnumbers["data_5_6_patient_hcv@".$row["data_5_6_patient_hcv"]];
				if ($rowidx!=="") @$datalist[$rowidx][$colidx] += $row["cnt"];
			}
			if ($row["data_5_7_patient_hbs"]!=""){
				$rowidx = @$vert_headers_sortnumbers["data_5_7_patient_hbs@".$row["data_5_7_patient_hbs"]];
				if ($rowidx!=="") @$datalist[$rowidx][$colidx] += $row["cnt"];
			}
			if ($row["data_5_8_patient_hbe"]!=""){
				$rowidx = @$vert_headers_sortnumbers["data_5_8_patient_hbe@".$row["data_5_8_patient_hbe"]];
				if ($rowidx!=="") @$datalist[$rowidx][$colidx] += $row["cnt"];
			}
			if ($row["data_5_9_patient_syphilis"]!=""){
				$rowidx = @$vert_headers_sortnumbers["data_5_9_patient_syphilis@".$row["data_5_9_patient_syphilis"]];
				if ($rowidx!=="") @$datalist[$rowidx][$colidx] += $row["cnt"];
			}
			if ($row["data_5_10_patient_atla"]!=""){
				$rowidx = @$vert_headers_sortnumbers["data_5_10_patient_atla@".$row["data_5_10_patient_atla"]];
				if ($rowidx!=="") @$datalist[$rowidx][$colidx] += $row["cnt"];
			}
		}

		// 「Ａタイプ：受傷部位」の場合は、取得フィールドは複数存在
		else if ($vert_dbkey == "data_12_1_injury_deep"){
			if ($row["data_12_1_injury_deep"]!=""){
				$rowidx = @$vert_headers_sortnumbers[$vert_dbkey."@".$row["data_12_1_injury_deep"]];
				@$datalist[$rowidx][$colidx] += $row["cnt"];
			}
			if ($row["data_12_2_injury_middle"]!=""){
				$rowidx = @$vert_headers_sortnumbers[$vert_dbkey."@".$row["data_12_2_injury_middle"]];
				@$datalist[$rowidx][$colidx] += $row["cnt"];
			}
			if ($row["data_12_3_injury_superficial"]!=""){
				$rowidx = @$vert_headers_sortnumbers[$vert_dbkey."@".$row["data_12_3_injury_superficial"]];
				@$datalist[$rowidx][$colidx] += $row["cnt"];
			}
		}

		// 「Ａタイプ：使用目的」の場合は、取得フィールドは複数存在かつネスト
		else if ($vert_dbkey == "data_8_1_use_view"){
			if ($row["data_8_1_use_view"] == "07"){
				$rowidx = @$vert_headers_sortnumbers[$vert_dbkey."@".$row["data_8_1_use_view"]."@".$row["data_8_2_use_view_vein"]];
			} else if ($row["data_8_1_use_view"] == "08"){
				$rowidx = @$vert_headers_sortnumbers[$vert_dbkey."@".$row["data_8_1_use_view"]."@".$row["data_8_3_use_view_artery"]];
			} else {
				$rowidx = @$vert_headers_sortnumbers[$vert_dbkey."@".$row["data_8_1_use_view"]];
			}
			@$datalist[$rowidx][$colidx] += $row["cnt"];
		}

		// 「Ｂタイプ：受傷部位」の場合は、取得フィールドは複数存在
		else if ($vert_dbkey == "data_13_1_injury_deep"){
			if ($row["data_13_1_injury_deep"]!=""){
				$rowidx = @$vert_headers_sortnumbers[$vert_dbkey."@".$row["data_13_1_injury_deep"]];
				@$datalist[$rowidx][$colidx] += $row["cnt"];
			}
			if ($row["data_13_2_injury_middle"]!=""){
				$rowidx = @$vert_headers_sortnumbers[$vert_dbkey."@".$row["data_13_2_injury_middle"]];
				@$datalist[$rowidx][$colidx] += $row["cnt"];
			}
			if ($row["data_13_3_injury_superficial"]!=""){
				$rowidx = @$vert_headers_sortnumbers[$vert_dbkey."@".$row["data_13_3_injury_superficial"]];
				@$datalist[$rowidx][$colidx] += $row["cnt"];
			}
		}

		//「Ｂタイプ：汚染した体液」の場合は、取得フィールドは複数存在
		else if ($vert_dbkey == "data_6_1_fluid_blood"){
			if ($row["data_6_1_fluid_blood"])       @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@01"]][$colidx] += $row["cnt"];
			if ($row["data_6_2_fluid_vomit"])       @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@02"]][$colidx] += $row["cnt"];
			if ($row["data_6_3_fluid_phlegm"])      @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@03"]][$colidx] += $row["cnt"];
			if ($row["data_6_4_fluid_saliva"])      @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@04"]][$colidx] += $row["cnt"];
			if ($row["data_6_5_fluid_spinal_cord"]) @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@05"]][$colidx] += $row["cnt"];
			if ($row["data_6_6_fluid_ascites"])     @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@06"]][$colidx] += $row["cnt"];
			if ($row["data_6_7_fluid_pleural"])     @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@07"]][$colidx] += $row["cnt"];
			if ($row["data_6_8_fluid_amniotic"])    @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@08"]][$colidx] += $row["cnt"];
			if ($row["data_6_9_fluid_urine"])       @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@09"]][$colidx] += $row["cnt"];
			if ($row["data_6_10_fluid_etc"])        @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@99"]][$colidx] += $row["cnt"];
		}

		// 「Ｂタイプ：汚染時の防衣・防具」の場合は、取得フィールドは複数存在
		else if ($vert_dbkey == "data_7_1_part_clean_skin"){
			if ($row["data_7_1_part_clean_skin"])     @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@01"]][$colidx] += $row["cnt"];
			if ($row["data_7_2_part_blemished_skin"]) @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@02"]][$colidx] += $row["cnt"];
			if ($row["data_7_3_part_eye"])            @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@03"]][$colidx] += $row["cnt"];
			if ($row["data_7_4_part_nose"])           @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@04"]][$colidx] += $row["cnt"];
			if ($row["data_7_5_part_mouth"])          @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@05"]][$colidx] += $row["cnt"];
			if ($row["data_7_6_part_etc"])            @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@99"]][$colidx] += $row["cnt"];
		}

		// 「Ｂタイプ：汚染時の防衣・防具」の場合は、取得フィールドは複数存在
		else if ($vert_dbkey == "data_9_1_cloth_no_glove"){
			if ($row["data_9_1_cloth_no_glove"])          @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@01"]][$colidx] += $row["cnt"];
			if ($row["data_9_2_cloth_one_glove"])         @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@02"]][$colidx] += $row["cnt"];
			if ($row["data_9_4_cloth_double_glove"])      @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@03"]][$colidx] += $row["cnt"];
			if ($row["data_9_6_cloth_goggle"])            @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@04"]][$colidx] += $row["cnt"];
			if ($row["data_9_7_cloth_glass"])             @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@05"]][$colidx] += $row["cnt"];
			if ($row["data_9_8_cloth_glass_aside"])       @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@06"]][$colidx] += $row["cnt"];
			if ($row["data_9_9_cloth_face_shield"])       @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@07"]][$colidx] += $row["cnt"];
			if ($row["data_9_10_cloth_ope_mask"])         @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@08"]][$colidx] += $row["cnt"];
			if ($row["data_9_11_cloth_glass_mask"])       @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@09"]][$colidx] += $row["cnt"];
			if ($row["data_9_12_cloth_ope_gown"])         @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@10"]][$colidx] += $row["cnt"];
			if ($row["data_9_13_cloth_gown"])             @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@11"]][$colidx] += $row["cnt"];
			if ($row["data_9_14_cloth_experiment"])       @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@12"]][$colidx] += $row["cnt"];
			if ($row["data_9_15_cloth_exp_etc"])          @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@13"]][$colidx] += $row["cnt"];
			if ($row["data_9_17_cloth_etc"])              @$datalist[@$vert_headers_sortnumbers[$vert_dbkey."@99"]][$colidx] += $row["cnt"];
		}

		// 通常
		else {
			$rowidx = $vert_headers_sortnumbers[$vert_dbkey."@".$row[$vert_dbkey]];
			@$datalist[$rowidx][$colidx] += $row["cnt"];
		}
	}
}


//====================================================================
// データの無い行と列を削除する
//====================================================================
if ($is_shrink_vert || $is_shrink_horz) {
	$existColIdx = array();
	$existRowIdx = array();
	$tmplist = array();

	foreach($datalist as $rowidx => $row){
		$existRowIdx[$rowidx==="" ? "9999" : $rowidx] = true;
		foreach($row as $colidx => $cnt){
			$tmplist[$rowidx][$colidx] = $cnt;
			$existColIdx[$colidx==="" ? "9999" : $colidx] = true;
		}
	}
	ksort($existColIdx);
	ksort($existRowIdx);
	if ($existColIdx["9999"]) { $existColIdx[""] = $existColIdx["9999"]; unset($existColIdx["9999"]); }
	if ($existRowIdx["9999"]) { $existRowIdx[""] = $existRowIdx["9999"]; unset($existRowIdx["9999"]); }

	$i = -1;
	foreach($existColIdx as $idx => $t) { $i++; $existColIdx[$idx] = $i; }
	$i = -1;
	foreach($existRowIdx as $idx => $t) { $i++; $existRowIdx[$idx] = $i; }

	if ($is_shrink_vert){
		$tmp_vert_headers = array();
		foreach($vert_headers as $idx => $row){
			if (!$existRowIdx[$idx] && $existRowIdx[$idx]!==0) continue;
			$tmp_vert_headers[] = $row;
		}
		$vert_headers = array();
		foreach($tmp_vert_headers as $idx => $row) $vert_headers[] = $row;
	}

	if ($is_shrink_horz){
		$tmp_horz_headers = array();
		foreach($horz_headers as $idx => $row){
			if (!$existColIdx[$idx] && $existColIdx[$idx]!==0) continue;
			$tmp_horz_headers[] = $row;
		}
		$horz_headers = array();
		foreach($tmp_horz_headers as $idx => $row) $horz_headers[] = $row;
	}

	$datalist = array();
	foreach($tmplist as $rowidx => $row){
		foreach($row as $colidx => $cnt){
			$newrow = ($is_shrink_vert ? $existRowIdx[$rowidx] : $rowidx);
			$newcol = ($is_shrink_horz ? $existColIdx[$colidx] : $colidx);
			$datalist[$newrow][$newcol] = $cnt;
		}
	}
}

//====================================================================
// カラ登録があったか確認する。カラ登録があれば、カラタイトルを作成する。
//====================================================================
foreach($datalist as $rowidx => $row){
	if ($rowidx < count($vert_headers) && $rowidx!=="") continue;
	$vert_headers[] = array("record_caption"=>"≪該当なし≫", "record_id_path"=>"");
	$datalist[count($vert_headers)-1] = $row;
	break;
}

$newcol = 0;
foreach($datalist as $rowidx => $row){
	foreach($row as $colidx => $cnt){
		if ($colidx < count($horz_headers) && $colidx!=="") continue;
		if (!$newcol) {
			$horz_headers[] = array("record_caption"=>"≪該当なし≫", "record_id_path"=>"");
			$newcol = count($horz_headers) - 1;
		}
		$datalist[$rowidx][$newcol] = $cnt;
	}
}



//====================================================================
// 印刷（XLSエクセル出力）の場合。内容を出力してdieする。
// もし失敗すれば、ブラウザ上に内容が表示されるであろう。
//====================================================================
if ($is_print_mode){

	//=================================
	// グラフ対象の行/列の、日本語表記名の取得と、テーブル上のインデックスの取得
	//=================================
	$graphVertOrHorzIndex = 0;

	$idx = 0;
	$graphVertOrHorzName = "";
	if ($graphVertOrHorz=="V"){
		foreach($horz_headers as $idx => $row){
			if ($graphVertOrHorzId!=$row["record_id"]) continue;
			$graphVertOrHorzName = $row["record_caption"];
			$graphVertOrHorzIndex = $idx+1;
			$idx++;
		}
		$subName = $vert_text."別グラフ (".$horz_text . "：" . ((int)$graphVertOrHorzId==0 ? "合計" : $graphVertOrHorzName).")";
	} else {
		foreach($vert_headers as $idx => $row){
			if ($graphVertOrHorzId!=$row["record_id"]) continue;
			$graphVertOrHorzName = $row["record_caption"];
			$graphVertOrHorzIndex = $idx+1;
			$idx++;
		}
		$subName = $horz_text."別グラフ (".$vert_text . "：" . ((int)$graphVertOrHorzId==0 ? "合計" : $graphVertOrHorzName).")";
	}
	
	for($cnt_shokushu = 0;$cnt_shokushu < count($chk_shokushu);$cnt_shokushu++)
	{
		$sql = " select *  ".
			" from tmplitem ".
			" where mst_cd = 'A401' and item_cd='".$chk_shokushu[$cnt_shokushu]."'";
		$sel = @select_from_table($con, $sql, "", $fname);		
		$str_shoku_name = $str_shoku_name."　".pg_fetch_result($sel, 0, "item_name");
	}
	
	require_once("fplus_aggregate_print.ini");
	fplus_aggregate_print_execute(
		$graphVertOrHorz, // "V"ertical または それ以外
		$graphVertOrHorzIndex, // ゼロのとき、合計値をグラフ化します。($xlsGraphVertOrHorz=="V"のとき最右列、それ以外なら最下行）
		$datalist,
		$horz_headers,
		$vert_headers,
		$epi_title . " (" . $horz_text . " - " . $vert_text . "集計)",
		$subName,
		$sel_d1_y."年".$sel_d1_m."月".$sel_d1_d."日 〜 ".$sel_d2_y."年".$sel_d2_m."月".$sel_d2_d."日", // 検索条件：期間
		fplus_common_get_episys_master_value("data_1_2_belong_dept", $sel_belong_dept, true), // 検索条件：部門
		$str_shoku_name,//fplus_common_get_episys_master_value("data_3_1_job", $sel_job, true), // 検索条件：職種
		fplus_common_get_episys_master_value("data_4_1_arise_area", $sel_arise_area, true) // 検索条件：発生場所
	);
	die;
}





//====================================================================
// HTMLヘッダ
//====================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ファントルくん＋ | 集計表 | <?=$epi_title?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list1 {border-collapse:collapse;}
.list1 td {border:#E6B3D4 solid 1px; padding:3px; }
.list2 {border-collapse:collapse;}
.list2 td {border:#dfd95e solid 1px; padding:3px; }
</style>


<? //-------------------------------------------------------------------------?>
<? // JavaScript                                                              ?>
<? //-------------------------------------------------------------------------?>
<script type="text/javascript">

	<? // ************* エクセル出力開始 ************* ?>
	function startPrint(){
		if (document.frm.graph_vert_or_horz.value == ""){
			alert("グラフ対象の行または列を指定してください。");
			return;
		}
		document.frm.action = 'fplus_aggregate.php';
		document.frm.print_mode.value = "1";
		document.frm.search_mode.value = "";
		document.frm.target = "print_window";
		document.frm.submit();
		return false;
	}

	<? // ************* 検索開始 ************* ?>
	function startSearch(){
		document.frm.action = 'fplus_aggregate.php';
		document.frm.print_mode.value = "";
		document.frm.search_mode.value = "1";
		document.frm.graph_vert_or_horz.value = "";
		document.frm.graph_vert_or_horz_id.value = "";
		document.frm.target = "_self";
		document.frm.submit();
		return false;
	}

	<? // ************* ＡとＢの切替ボタン押下 ************* ?>
	function changeEpiTypeAndstartSearch(epi_type){
		document.frm.action = 'fplus_aggregate.php';
		document.frm.epi_type.value = epi_type;
		document.frm.sel_horz_field_idx.selectedIndex = 0;
		document.frm.sel_vert_field_idx.selectedIndex = 0;
		return startSearch();
	}

	<? // ************* 画面の幅の取得 ************* ?>
	function getClientWidth(){
		if (document.body.clientWidth) return document.body.clientWidth;
		if (window.innerWidth) return window.innerWidth;
		if (document.documentElement && document.documentElement.clientWidth) {
			return document.documentElement.clientWidth;
		}
		return 0;
	}


	<? // ************* 上部スクロールバー制御 ************* ?>
	var scroll_div = null;
	var scrollbar_div = null;
	var scroll_image_gauge = null;
	var scroll_initialized = false;
	function listScrolled(id){
		var newW = getClientWidth();
		if (!newW) return;
		if (scroll_div==null) scroll_div = document.getElementById("scroll_div");
		if (!scroll_div) return;
		if (scrollbar_div==null) scrollbar_div = document.getElementById("scrollbar_div");
			scroll_div.style.overflowX = "scroll";

			if (scroll_div.scrollWidth > 0){
				if (scroll_image_gauge==null) scroll_image_gauge = document.getElementById("scroll_image_gauge");
				scroll_image_gauge.style.height = "1px";
				scroll_image_gauge.style.width = scroll_div.scrollWidth + "px";
			}

		scrollbar_div.style.width = (newW-2) + "px";
		scroll_div.style.width = (newW-2) + "px";

		scroll_initialized = true;
		if(!id) return;
		if (scrollbar_div.scrollLeft != scroll_div.scrollLeft){
			if (id == scroll_div.id){
				scrollbar_div.scrollLeft = scroll_div.scrollLeft;
			} else {
				scroll_div.scrollLeft = scrollbar_div.scrollLeft;
			}
		}
	}

	<? // ************* 一覧表のヘッダクリック、グラフ出力範囲の選択、色の変更 ************* ?>
	var curCol = -1;
	var curRow = -1;
	function changeGraphTarget(v_or_h, record_id, a_tag){
		var td = a_tag.parentNode;

		if (curCol >= 0 || curRow >= 0){
			document.getElementById("cell_"+curCol+"_"+curRow).style.border = "1px solid #E6B3D4";
			if (curCol==0){
				for (var h = 1; h <= <?=count($horz_headers)?>; h++){
						if (!document.getElementById("cell_"+h+"_0")) continue;
						document.getElementById("cell_"+h+"_0").style.border = "1px solid #E6B3D4";
						document.getElementById("cell_"+h+"_"+curRow).style.border = "1px solid #E6B3D4";
				}
			} else {
				for (var v = 1; v <= <?=count($vert_headers)?>; v++){
						if (!document.getElementById("cell_0_"+v)) continue;
						document.getElementById("cell_0_"+v).style.border = "1px solid #E6B3D4";
						document.getElementById("cell_"+curCol+"_"+v).style.border = "1px solid #E6B3D4";
				}
			}
		}

		var tablepos = td.id.split("_");
		var col = tablepos[1];
		var row = tablepos[2];
		curCol = col;
		curRow = row;

		td.style.border = "2px solid #00f";
		var graphTitle = "";
		if (curCol==0){
			graphTitle = "<?=$horz_text?>別グラフ<span style='font-size:12px'>  (<?=$vert_text?>：" + a_tag.innerHTML+")</span>	";
			for (var h = 1; h <= <?=count($horz_headers)?>; h++){
					if (!document.getElementById("cell_"+h+"_0")) continue;
					document.getElementById("cell_"+h+"_0").style.border = "2px solid #ff3c26";
					document.getElementById("cell_"+h+"_"+curRow).style.border = "2px solid #ff3c26";
			}
		} else {
			graphTitle = "<?=$vert_text?>別グラフ <span style='font-size:12px'> (<?=$horz_text?>：" + a_tag.innerHTML+")</span>";
			for (var v = 1; v <= <?=count($vert_headers)?>; v++){
					if (!document.getElementById("cell_0_"+v)) continue;
					document.getElementById("cell_0_"+v).style.border = "2px solid #ff3c26";
					document.getElementById("cell_"+curCol+"_"+v).style.border = "2px solid #ff3c26";
			}
		}
		document.frm.graph_vert_or_horz.value = v_or_h;
		document.frm.graph_vert_or_horz_id.value = record_id;
	}

function clickDownLoad() 
{
    pjtForm = document.getElementById("frm");

    pjtForm.action = 'fplus_epinet_apply_excel_109.php';
    pjtForm.method = 'post';
    pjtForm.submit();
}


</script>


<? // カレンダ ?>
<? write_yui_calendar_use_file_read_0_12_2(); ?>
<? write_yui_calendar_script2(2); ?>


</head>
<body style="margin:0" onload="initcal(); listScrolled();" onresize="listScrolled();">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>

<? //-------------------------------------------------------------------------?>
<? // ぱんくず、管理画面へのリンク                                            ?>
<? //-------------------------------------------------------------------------?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr bgcolor="#f6f9ff">
    <td width="32" height="32" class="spacing">
      <a href="fplus_menu.php?session=<? echo($session); ?>"><img src="img/icon/b47.gif" width="32" height="32" border="0" alt="ファントルくん＋"></a>
    </td>
    <td>
      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="fplus_menu.php?session=<? echo($session); ?>"><b>ファントルくん＋</b></a> &gt; <a href="fplus_aggregate.php?session=<? echo($session); ?>&epi_type=<?=$epi_type?>"><b>集計表 - <?=$epi_title?></b></a></font>
    </td>
    <td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="fplusadm_menu.php?session=<? echo($session); ?>&workflow=1"><b>管理画面へ</b></a></font>
    </td>
  </tr>
</table>


<? //-------------------------------------------------------------------------?>
<? // メインメニュータブと、ボトムマージン描画                                ?>
<? //-------------------------------------------------------------------------?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr><? show_applycation_menuitem($session,$fname,""); ?></tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr><td bgcolor="#e6b3d4"><img src="img/spacer.gif" width="1" height="2" alt=""></td></tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>


<? //-------------------------------------------------------------------------?>
<? // 入力フォーム描画開始                                                    ?>
<? //-------------------------------------------------------------------------?>
<form name="frm" id="frm" action="fplus_aggregate.php" method="post" enctype="multipart/form-data">
  <input type="hidden" name="session" value="<? echo($session); ?>">
  <input type="hidden" name="epi_type" value="<?=$epi_type?>">
  <input type="hidden" name="print_mode" value="">
  <input type="hidden" name="search_mode" value="">
  <input type="hidden" name="graph_vert_or_horz" value="">
  <input type="hidden" name="graph_vert_or_horz_id" value="">


<? //-------------------------------------------------------------------------?>
<? // AとBの切り替え サブメニューリンクボタン描画                             ?>
<? //-------------------------------------------------------------------------?>
<div style="margin:4px; padding:3px 6px; float:left; background-color:#<?=$epi_type=="a"?"ff99cc":"f4f4f4"?>; border:1px solid #ccc;">
	<?=$font?><a href="javascript:void(0)" onclick="return changeEpiTypeAndstartSearch('a');" style="color:#<?=$epi_type=="a"?"fff":"00f"?>"><?=$epi_title_a?></a></font>
</div>
<div style="margin:4px; padding:3px 6px; float:left; background-color:#<?=$epi_type=="b"?"ff99cc":"f4f4f4"?>; border:1px solid #ccc;">
	<?=$font?><a href="javascript:void(0)" onclick="return changeEpiTypeAndstartSearch('b');" style="color:#<?=$epi_type=="b"?"fff":"00f"?>"><?=$epi_title_b?></a></font>
</div>
<div style="margin:4px; padding:3px 6px; float:left; background-color:#<?=$epi_type=="ao"?"ff99cc":"f4f4f4"?>; border:1px solid #ccc;">
	<?=$font?><a href="javascript:void(0)" onclick="return changeEpiTypeAndstartSearch('ao');" style="color:#<?=$epi_type=="ao"?"fff":"00f"?>"><?=$epi_title_ao?></a></font>
</div>
<div style="margin:4px; padding:3px 6px; float:left; background-color:#<?=$epi_type=="bo"?"ff99cc":"f4f4f4"?>; border:1px solid #ccc;">
	<?=$font?><a href="javascript:void(0)" onclick="return changeEpiTypeAndstartSearch('bo');" style="color:#<?=$epi_type=="bo"?"fff":"00f"?>"><?=$epi_title_bo?></a></font>
</div>
<div style="clear:both"></div>




<? //-------------------------------------------------------------------------?>
<? // 検索条件                                                                ?>
<? //-------------------------------------------------------------------------?>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list1">
<tr>
	<td bgcolor="#feeae9"><?=$font?>期間</font></td>
	<td bgcolor="#fff8f7">
	<? // ************* 期間（日付From） ドロップダウン ******************** ?>
	<div style="float:left">
	  <select id="date_y1" name="sel_d1_y"><option></option>
	  	<? for($i=date("Y"); $i>=2000; $i--) { ?><option value="<?=$i?>" <?= ($i==$sel_d1_y ? 'selected="selected"': "") ?>><?=$i?></option><? } ?>
	  </select>
	  <select id="date_m1" name="sel_d1_m"><option></option>
	  	<? for($i=1; $i<=12; $i++) { ?><option value="<?=$i?>" <?= ($i==$sel_d1_m ? 'selected="selected"': "") ?>><?=$i?></option><? } ?>
	  </select>
	  <select id="date_d1" name="sel_d1_d"><option></option>
	  	<? for($i=1; $i<=31; $i++) { ?><option value="<?=$i?>" <?= ($i==$sel_d1_d ? 'selected="selected"': "") ?>><?=$i?></option><? } ?>
	  </select>
	</div>
	<? // ************* 日付From のカレンダボタン ******************** ?>
  <div style="float:left; padding:0px 2px"><?=$font?>
  	<img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/>
		<div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div></font>
	</div>
	<div style="float:left">
  〜
  </div>
	<? // ************* 期間（日付To） ドロップダウン ******************** ?>
	<div style="float:left">
	  <select id="date_y2" name="sel_d2_y"><option></option>
	  	<? for($i=date("Y"); $i>=2000; $i--) { ?><option value="<?=$i?>" <?= ($i==$sel_d2_y ? 'selected="selected"': "") ?>><?=$i?></option><? } ?>
	  </select>
	  <select id="date_m2" name="sel_d2_m"><option></option>
	  	<? for($i=1; $i<=12; $i++) { ?><option value="<?=$i?>" <?= ($i==$sel_d2_m ? 'selected="selected"': "") ?>><?=$i?></option><? } ?>
	  </select>
	  <select id="date_d2" name="sel_d2_d"><option></option>
	  	<? for($i=1; $i<=31; $i++) { ?><option value="<?=$i?>" <?= ($i==$sel_d2_d ? 'selected="selected"': "") ?>><?=$i?></option><? } ?>
	  </select>
	</div>
	<? // ************* 日付Toのカレンダボタン ******************** ?>
  <div style="float:left; padding:0px 2px"><?=$font?>
  	<img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal2();"/>
		<div id="cal2Container" style="position:absolute;display:none;z-index:10000;"></div></font>
	</div>
	<div style="clear:both"></div>
  </td>
	<? // ************* 所属部門ドロップダウン ******************** ?>
 <td bgcolor="#feeae9"><?=$font?>所属部門</font></td>
 <td bgcolor="#fff8f7">
  <select name="sel_belong_dept"><option></option>
  <?= fplus_common_get_episys_dropdown_option_html("data_1_2_belong_dept", $sel_belong_dept, true) ?>
  </select>
 </td>

</tr>

<tr>
	<? // ************* 職種ドロップダウン ******************** ?>
  <td bgcolor="#feeae9"><?=$font?>職種</font></td>
  <td bgcolor="#fff8f7" colspan="5">


<?

foreach($selall_shokushu as $idx => $row)
{
	for($cnt_shokushu = 0;$cnt_shokushu < count($chk_shokushu);$cnt_shokushu++)
	{
		if($chk_shokushu[$cnt_shokushu] == $row["item_cd"])
		{
			$str_check = "checked";
		}
	}
	
?>
<?=$font?><input type="checkbox" name="chk_shokushu[]" <?=$str_check?> value="<?=$row["item_cd"]?>"><?=$row["item_name"]?></font>
<?
	$str_check = "";
}
?>
  </td>
</tr>
	<? // ************* 発生場所ドロップダウン ******************** ?>
<tr>
	<td bgcolor="#feeae9"><?=$font?>発生場所</font></td>
	<td colspan="5" bgcolor="#fff8f7">
            <? if ($epi_type=="a" || $epi_type=="b"){ ?>
  <select name="sel_arise_area"><option></option>
  
    <?= fplus_common_get_episys_dropdown_option_html("data_4_1_arise_area", $sel_arise_area, true) ?>
  </select>
            <? }else{ ?>
   <select name="sel_arise_area"><option></option>
  
    <?= fplus_common_get_episys_dropdown_option_html("data_7_1_arise_area", $sel_arise_area, true) ?>
  </select>
            <? } ?>
        </td>
</tr>
</table>

	<div style="margin-top:4px"><?=$font?>
		<div style="float:left;">
			<table border="0" cellspacing="0" cellpadding="2" class="list2">
				<tr>

					<? // ************* 縦軸項目ドロップダウン ******************** ?>
					<td bgcolor="#fefcdf"><?=$font?>縦軸&nbsp;</font></td>
					<td bgcolor="#fefded"><?=$font?>
					  <select name="sel_vert_field_idx">
					  	<? foreach ($vert_field_info as $idx => $row) {?>
				  		<option value="<?=$idx?>" <?= ($idx==$sel_vert_field_idx ? 'selected="selected"' : "") ?>><?=$row["text"]?></option>
					  	<? } ?>
					  </select>
					  <label style="cursor:pointer; color:#777"><input type="checkbox" name="is_shrink_vert" value="1" <?=$is_shrink_vert?"checked":""?>>件数のあるもののみ表示</label>&nbsp;
					</font></td>

					<? // ************* 横軸項目ドロップダウン ******************** ?>
					<td bgcolor="#fefcdf"><?=$font?>横軸&nbsp;</font></td>
					<td bgcolor="#fefded"><?=$font?>
					  <select name="sel_horz_field_idx">
					  	<? foreach ($horz_field_info as $idx => $row) {?>
					  		<option value="<?=$idx?>" <?= ($idx==$sel_horz_field_idx ? 'selected="selected"' : "") ?>><?=$row["text"]?></option>
					  	<? } ?>
					  </select>
					  <label style="cursor:pointer; color:#777"><input type="checkbox" name="is_shrink_horz" value="1" <?=$is_shrink_horz?"checked":""?>>件数のあるもののみ表示</label>&nbsp;
					</font></td>
				</tr>
			</table>
		</div>

		<? // ************* 検索と印刷（エクセル出力）ボタン ******************** ?>
		<div style="float:right;">
			<input type="button" onclick="startSearch();" value="検索"/>
			<input type="button" onclick="startPrint();" value="エクセル出力" <?= !count($datalist) ? "disabled" : "" ?> />
			<input type="button" name="excel_btn" value="職業感染報告用出力" onclick="clickDownLoad()" >
		</div>
		<br style="clear:both"/>
		</font>
	</div>

</form>


</td></tr></table>


<? //-------------------------------------------------------------------------?>
<? // 一覧表                                                                  ?>
<? //-------------------------------------------------------------------------?>
<? if (!count($datalist)){ ?>
	<div style="color:#999; text-align:center; margin-top:20px"><?=$font?>該当データはありません。</font></div>
<? } else { ?>
<div style="margin-top:10px; border:1px solid #E6B3D4; background-color:#eee;">
<div id="scrollbar_div" style="line-height:0; font-size:1px; height:18px; overflow-x:scroll;" onscroll="listScrolled(this.id);"><img src="img/spacer.gif" id="scroll_image_gauge"/></div>
<div id="scroll_div" onscroll="listScrolled(this.id);" style="width:100px; overflow-x:scroll;">
	<? $colcnt = count($horz_headers); ?>
	<? $colPercentWidth = floor(100 / ($colcnt+2)); ?>
	<? $tableWidth = $colcnt > 13 ? "width:".($colcnt*80)."px" : "width:100%"; ?>
<table class="list1" style="<?=$tableWidth?>; background-color:#fff;">

	<? // 列ヘッダ行 ?>
	<tr style="background-color:#ffdfff;">
		<td id="cell_0_0" style="width:<?=$colPercentWidth?>%"></td>
		<? $colttl = array(); ?>
		<? for ($hh = 0; $hh < count($horz_headers); $hh++){ ?>
			<td id="cell_<?=$hh+1?>_0" class="j12" style="text-align:center; width:<?=$colPercentWidth?>%">
				<a href="javascript:void(0)" onclick="javascript:changeGraphTarget('V', '<?=$horz_headers[$hh]["record_id"]?>', this);" style="color:#00f">
				<?=$horz_headers[$hh]["record_caption"]?>
				</a>
			</td>
			<? $colttl[$hh] = 0; ?>
		<? } ?>
		<td id="cell_<?=count($horz_headers)+1?>_0" class="j12" style="text-align:center; width:<?=$colPercentWidth?>%">
			<a href="javascript:void(0)" onclick="changeGraphTarget('V', '', this);" style="color:#00f">合計</a>
		</td>
	</tr>

	<? // 行ヘッダ、行データ、行ごとの合計行  くりかえし描画 ?>
	<? for ($vh = 0; $vh < count($vert_headers); $vh++){ ?>
	<tr>
		<td id="cell_0_<?=$vh+1?>" style="background-color:#ffdfff;" class="j12">
			<a href="javascript:void(0)" onclick="changeGraphTarget('H', '<?=$vert_headers[$vh]["record_id"]?>', this);" style="color:#00f">
			<?=$vert_headers[$vh]["record_caption"]?></a>
		</td>
		<? $ttl = 0; ?>
		<? for ($hh = 0; $hh < count($horz_headers); $hh++){ ?>
			<? $result_count = (int)@$datalist[$vh][$hh]; ?>
			<? $ttl += $result_count; ?>
			<? $colttl[$hh] += $result_count; ?>
			<td id="cell_<?=$hh+1?>_<?=$vh+1?>" align="right"><?=$font?><?=@$datalist[$vh][$hh]?></font></td>
		<? } ?>
		<td id="cell_<?=count($horz_headers)+1?>_<?=$vh+1?>" align="right" style="background-color:#fff4ff;">
			<?=$font?><?=$ttl?></font>
		</td>
	</tr>
	<? } ?>

	<? // 列ごとの合計行 ?>
	<tr>
		<td id="cell_0_<?=count($vert_headers)+1?>" style="background-color:#ffdfff;" class="j12">
			<a href="javascript:void(0)" onclick="changeGraphTarget('H', '', this);" style="color:#00f">合計</a></td>
		<? $ttlall = 0; ?>
		<? for ($hh = 0; $hh < count($horz_headers); $hh++){ ?>
			<td id="cell_<?=$hh+1?>_<?=count($vert_headers)+1?>" style="background-color:#fff4ff" align="right"><?=$font?><?=$colttl[$hh]?></font></td>
			<? $ttlall += $colttl[$hh]?>
		<? } ?>
		<td id="cell_<?=count($horz_headers)+1?>_<?=count($vert_headers)+1?>" style="background-color:#fff4ff" align="right"><?=$font?><?=$ttlall?></font></td>
	</tr>

</table>

</div>
</div>

<? } ?>



</body>
</html>
