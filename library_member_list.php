<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 文書管理 | 名簿</title>
<?
require("about_session.php");
require("about_authority.php");
require("show_library_member.ini");

$fname = $PHP_SELF;
if ($session != "") {

	// セッションのチェック
	$session = qualify_session($session, $fname);
	if ($session == "0") {
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showLoginPage(window);</script>");
		exit;
	}

	// 権限のチェック
	$checkauth = check_authority($session, 32, $fname);
	if ($checkauth == "0") {
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showLoginPage(window);</script>");
		exit;
	}
} else {
	require("about_postgres.php");
}

// データベースに接続
$con = connect2db($fname);

// イニシャルのデフォルトは「あ行」とする
if ($in_id == "") {
	$in_id = 1;
}

if ($session != "") {
	$select_id = "select emp_id from login where emp_id in (select emp_id from session where session_id = '$session')";
	$result_select = pg_exec($con, $select_id);
	if($result_select == false){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_result($result_select, 0, "emp_id");

	if($job_id == ""){
		$select_empmst = "select emp_job from empmst where emp_id = '$emp_id'";
		$result_j = pg_exec($con,$select_empmst);
		if($result_j == false){
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$job_id = pg_result($result_j,0,"emp_job");
	}
}

$select_name = "select job_id, job_nm from jobmst where job_del_flg = 'f' order by order_no";
$result_name = pg_exec($con,$select_name);
if($result_name == false){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$count = pg_numrows($result_name);
?>
<script language="javascript">
<!--
function getSelected(){
	var select = document.member.select.value;
	var mem = document.member.member.value;
	var url = "./library_member_list.php?session=<? echo($session); ?>&job_id="+select+"&member="+mem;
	location.href= url;
}

function checkIn(in_member, in_name, route) {
	opener.document.search.emp_name.value = in_name;
	opener.document.search.hid_emp_name.value = in_name;
	opener.document.search.hid_emp_id.value = in_member;
	self.close();
}
//-->
</script>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279a5">
<td width="520" height="22" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>名簿</b></font></td>
<td width="80" align="center"><a href="javascript:window.close()"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">■閉じる</font></a></td>
</tr>
</table>
<form name="member">
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td height="22"><?
echo("<select name=\"select\" onChange=\"getSelected();\">\n");
echo("<option value=\"0\" \n");
if($select == 0){
	echo("selected");
}

echo(">すべて</option>\n");
for($i=0; $i<$count; $i++){
	$j_id = pg_result($result_name, $i, job_id);
	$j_nm = pg_result($result_name, $i, job_nm);
	echo("<option value=\"$j_id\"");
	if($job_id == $j_id){
	echo(" selected");
	}
	echo(">\n");
	echo($j_nm);
	echo("</option>\n");
}
echo("</select>\n");
?>
</td>
</tr>
<tr bgcolor="#f6f9ff">
<td height="22"><? show_library_member_list($job_id,$in_id,$member,$session,$num); ?></td>
</tr>
<tr bgcolor="#f6f9ff">
<td height="22"><? show_member_list($job_id,$in_id,$member,$session,$num); ?></td>
</tr>
</table>
<input type="hidden" name="member" value="<? echo($member); ?>">
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
