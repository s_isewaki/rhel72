<?php
//ini_set('display_errors', 1);
if(!function_exists ("get_emp_id"))
{
	require_once("get_values.php");
}
/*
 * ログを取るポイントで
 * 下記3行のプログラムを記述します。
 *
--------------------------------
require_once("Cmx.php");
require_once("aclg_set.php");

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);
--------------------------------
*/

function aclg_regist($session,$filename,$http_referer,$user_agent,$post_data,$con,$get_data=null) {
	$fname = __FILE__;
	$simple_filename = basename($filename);
	$sql = "select * from aclg_data_link";
	$cond = "where filename = '{$simple_filename}' and priority <> '4'";
	// 「文書管理」用
	// 文書とフォルダは同時削除したときにログを分ける
	if ($simple_filename == "library_delete.php") {
		switch ($get_data) {
			case "1":
				$cond .= " and detail_id='1'";
				break;
			case "2":
				$cond .= " and detail_id='2'";
				break;
			default:
				break;
		}
	}

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == "0") {
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$post_get = "";
	$status1 = "";
	$cate = "";
	$detail = "";
	$priority = "";

	while($row = pg_fetch_array($sel)) {
		//if($row["filename"] == $simple_filename) {
			//////////////////////////
			//詳細データ取得用に加工処理あり
			//////////////////////////
			if($row["cate_id"] === "0001") {// ログイン
				// ログイン画面からのログイン処理 もしくは systemidを使ったログイン
				if (
                        ($post_data['id'] != "" && $post_data['pass'] != "") || 
                        ($post_data['id'] != "" && $post_data['systemid'] != "")
                )
                {
					$status1 = "";
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}

				if($post_data == null)
				{
					//POSTデータがNULLの場合
					foreach ($get_data as $getkey => $getval)
					{
						if($getkey == "id")
						{
							//URLにid,pass直接入力方式のログイン
							$status1 = "";
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
							$loop_end = "t";
							break;
						}
					}

					if($priority != "")
					{
						break;
					}
				}

				if($row["filename"] == "create_session.php")
				{
					//ランチャーからのログイン
					$status1 = "";
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}

				//ログ出力対象にならないので、暫定的にpriorityを'4'とする
				$priority ='4';
				//break;
			}
			else if ($row["cate_id"] === "0002") {// 職員登録
				if ($post_data["suspend"] == 't') {
					// 利用停止の状態から利用停止にした場合は取得しない
					$empsql = "select emp_lt_nm,emp_ft_nm from empmst";
					$empcond = "where emp_personal_id='{$post_data["personal_id"]}'";
					$empsel = select_from_table($con, $empsql, $empcond, $fname);
					if ($empsel == 0) {
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
					$emp_lt_nm = pg_fetch_result($empsel, 0, "emp_lt_nm");
					$emp_ft_nm = pg_fetch_result($empsel, 0, "emp_ft_nm");

					if ($emp_lt_nm == '' and $emp_ft_nm == '') {
						$priority = '4';
					}
					else {
						$status1 .= "emp_name={$emp_lt_nm} {$emp_ft_nm}";
						$priority = $row["priority"];
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						break;
					}
				}
				else {
					$priority = '4';
				}
			}
			else if ($row["cate_id"] === "0003") {// マスターメンテナンス
				foreach ($post_data["trashbox"] as $tmp_trashbox){
					switch ($row["detail_id"]) {
						case "1":
							$class = explode("-", $tmp_trashbox);
							if ($class[1] != '') {
								if ($class[2] != '') {
									if ($class[3] != '') {
										$roomsql = "select room_nm from classroom";
										$roomcond = "where room_id = {$class[3]}";
										$roomsel = select_from_table($con, $roomsql, $roomcond, $fname);
										if ($roomsel == 0) {
											pg_close($con);
											echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
											echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
											exit;
										}
										$room_nm = pg_fetch_result($roomsel, 0, "room_nm");
										$status1 .= "4th={$room_nm}<br />";
									}
									else {
										$deptsql = "select dept_nm from deptmst";
										$deptcond = "where dept_id = {$class[2]}";
										$deptsel = select_from_table($con, $deptsql, $deptcond, $fname);
										if ($deptsel == 0) {
											pg_close($con);
											echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
											echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
											exit;
										}
										$dept_nm = pg_fetch_result($deptsel, 0, "dept_nm");
										$status1 .= "3rd={$dept_nm}<br />";
									}
								}
								else {
									$atrbsql = "select atrb_nm from atrbmst";
									$atrbcond = "where atrb_id = {$class[1]}";
									$atrbsel = select_from_table($con, $atrbsql, $atrbcond, $fname);
									if ($atrbsel == 0) {
										pg_close($con);
										echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
										echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
										exit;
									}
									$atrb_nm = pg_fetch_result($atrbsel, 0, "atrb_nm");
									$status1 .= "2nd={$atrb_nm}<br />";
								}
							}
							else {
								$classsql = "select class_nm from classmst";
								$classcond = "where class_id = {$class[0]}";
								$classsel = select_from_table($con, $classsql, $classcond, $fname);
								if ($classsel == 0) {
									pg_close($con);
									echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
									echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
									exit;
								}
								$class_nm = pg_fetch_result($classsel, 0, "class_nm");
								$status1 .= "1st={$class_nm}<br />";
							}
							break;
						case "2":
							$mstsql = "select job_nm from jobmst";
							$mstcond = "where job_id = {$tmp_trashbox}";
							$mstsel = select_from_table($con, $mstsql, $mstcond, $fname);
							if ($mstsel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$job_nm = pg_fetch_result($mstsel, 0, "job_nm");
							$status1 .= "job={$job_nm}<br />";
							break;
						case "3":
							$mstsql = "select st_nm from stmst";
							$mstcond = "where st_id = {$tmp_trashbox}";
							$mstsel = select_from_table($con, $mstsql, $mstcond, $fname);
							if ($mstsel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$st_nm = pg_fetch_result($mstsel, 0, "st_nm");
							$status1 .= "status={$st_nm}<br />";
							break;
					}
				}
				$cate = $row["cate_id"];
				$detail = $row["detail_id"];
				$priority = $row["priority"];
				break;
			}
			else if($row["cate_id"] === "0006") {// 日別一覧
				//ログイン処理
				$status1 = $row["status1"];
				$cate = $row["cate_id"];
				$detail = $row["detail_id"];
				$priority = $row["priority"];
				//break;
			}
			else if($row["cate_id"] === "0101") {// ウェブメール(送信：添付ファイルサイズ付き)
				if ($post_data["send_to"] != '') {
					$send_to = htmlspecialchars($post_data["send_to"], ENT_COMPAT, 'ISO-8859-1');
					$send_to_cc = htmlspecialchars($post_data["send_to_cc"], ENT_COMPAT, 'ISO-8859-1');
					$send_to_bcc = htmlspecialchars($post_data["send_to_bcc"], ENT_COMPAT, 'ISO-8859-1');
					$status1 = "[TO]:{$send_to} ";
					$status1 .= "[CC]:{$send_to_cc} ";
					$status1 .= "[BCC]:{$send_to_bcc}<br />";
					if ($get_data["attachfile"]["name"] !== '') {
						$sizeKB = $get_data["attachfile"]["size"] / 1024;
						$status1 .= "{$get_data["attachfile"]["name"]}/{$sizeKB}KB<br />";
					}
					$siteidsql = "select site_id from config";
					$siteidcond = "";
					$siteidsel = select_from_table($con, $siteidsql, $siteidcond, $fname);
					if ($siteidsel == 0) {
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
					$current_site_id = pg_fetch_result($siteidsel, 0, "site_id");
					if ($current_site_id == "") {
						// サイトID未指定
						$where_id = "emp_login_id";
						$username = $post_data["username"];
					}
					else {
						// サイトID指定有り
						$rpos = strrpos($post_data["username"], "_");
						$username = substr($post_data["username"],0,$rpos);
						$where_id = "emp_login_mail";
					}
					$wmsql = "select emp_id from login";
					$wmcond = "where {$where_id} = '{$username}'";
					$wmsel = select_from_table($con, $wmsql, $wmcond, $fname);
					if ($wmsel == 0) {
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
					$emp_id = pg_fetch_result($wmsel, 0, "emp_id");

					foreach ($post_data as $k => $v) {
						if ($k != 'restoremessages') {
							$tmp_post_data .= "{$k}={$v}:";
						}
					}
					$post_get = $tmp_post_data;

					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
				}
				break;
			}
			else if($row["cate_id"] === "0103") {// スケジュール
				switch ($row["detail_id"]) {
					case "1":
						// 共通グループの削除
						foreach ($post_data["group_ids"] as $tmp_id) {
							$cgsql = "select group_nm from comgroupmst";
							$cgcond = "where group_id = {$tmp_id}";
							$cgsel = select_from_table($con, $cgsql, $cgcond, $fname);
							if ($cgsel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$group_nm = pg_fetch_result($cgsel, 0, "group_nm");
							$status1 .= "group={$group_nm}<br />";
						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "2":
						// 行き先の削除
						foreach ($post_data["trashbox"] as $tmp_trashbox) {
							$placesql = "select place_name from scheduleplace";
							$placecond = "where place_id = {$tmp_trashbox}";
							$placesel = select_from_table($con, $placesql, $placecond, $fname);
							if ($placesel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$place_name = pg_fetch_result($placesel, 0, "place_name");
							$status1 .= "place={$place_name}<br />";
						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "3":
						// マイグループの削除
						foreach ($post_data["mygroup_ids"] as $tmp_id) {
							$mgsql = "select mygroup_nm from mygroupmst";
							$mgcond = "where owner_id = '{$get_data["emp_id"]}' and mygroup_id = {$tmp_id}";
							$mgsel = select_from_table($con, $mgsql, $mgcond, $fname);
							if ($mgsel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$mygroup_nm = pg_fetch_result($mgsel, 0, "mygroup_nm");
							$status1 .= "mygroup={$mygroup_nm}<br />";
						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "4":
						// スケジュールの削除
						$post_data = $get_data;
						if ($post_data["timeless"] == 't') {
							$schdsql = "select schd_title from schdmst2";
						}
						else {
							$schdsql = "select schd_title from schdmst";
						}

						$schdcond = "where schd_id = '{$post_data["schd_id"]}'";
						$schdsel = select_from_table($con, $schdsql, $schdcond, $fname);
						if ($schdsel == 0) {
							pg_close($con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}
						$schd_title = pg_fetch_result($schdsel, 0, "schd_title");
						$status1 .= htmlspecialchars("title={$schd_title}", ENT_COMPAT, 'ISO-8859-1') . "<br />";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "5":
						// 共通グループの作成
						$identirier = $row["status1"];
						$status1 = "group={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "6":
						// 共通グループの編集
						$identirier = $row["status1"];
						$status1 = "group={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "7":
						// 種別の更新
						$identirier = $row["status1"];
						for ($i = 2; $i < 15; $i++) {
							$ArrSchd[$i - 2] = "schd{$i}={$post_data[$identirier.$i]}";
						}
						$status1 = implode(",", $ArrSchd);
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "8":
						// 行き先の作成
						$identirier = $row["status1"];
						$status1 = "place={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "9":
						// 行き先の更新
						$identirier = $row["status1"];
						$status1 = "place={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "10":
						// マイグループの作成
						$identirier = $row["status1"];
						$status1 = "mygroup={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "11":
						// マイグループの編集
						$identirier = $row["status1"];
						$status1 = "mygroup={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "12":
						// スケジュールの作成
						$identirier = $row["status1"];
						$status1 = htmlspecialchars("title={$post_data[$identirier]}", ENT_COMPAT, 'ISO-8859-1');
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "13":
						// スケジュールの編集
						$identirier = $row["status1"];
						$status1 = htmlspecialchars("title={$post_data[$identirier]}", ENT_COMPAT, 'ISO-8859-1');
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
				}
				break;
			}
			else if($row["cate_id"] === "0104") {// 委員会・WG
				switch ($row["detail_id"]) {
					case "1":
						// 委員会・WGの削除
						$pjtsql = "select pjt_name from project";
						$pjtcond = "where pjt_id in (";
						$pjtcond .= implode(",", $post_data['trashbox']);
						$pjtcond .= ")";
						$pjtsel = select_from_table($con, $pjtsql, $pjtcond, $fname);
						if ($pjtsel == 0) {
							pg_close($con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}
						while($pjtrow = pg_fetch_array($pjtsel)) {
							$status1 .= "name={$pjtrow['pjt_name']}<br />";
						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "2":
						// 委員会スケジュールの削除
						$post_data = $get_data;
						if ($post_data['timeless'] != 't') {
							// 時刻 指定あり
							$pssql = "select pjt_schd_title from proschd";
						}
						else {
							// 時刻 指定なし
							$pssql = "select pjt_schd_title from proschd2";
						}
						$pscond = "where pjt_schd_id='{$post_data["pjt_schd_id"]}'";
						$pssel = select_from_table($con, $pssql, $pscond, $fname);
						if ($pssel == 0) {
							pg_close($con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}

						// 委員会・WG一覧から[時間指定なし]のスケジュールを削除した場合
						if (0 == pg_num_rows($pssel)) {
							$pssql = "select pjt_schd_title from proschd2";
							$pscond = "where pjt_schd_id='{$post_data["pjt_schd_id"]}'";
							$pssel = select_from_table($con, $pssql, $pscond, $fname);
							if ($pssel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
						}

						$pjt_schd_title = pg_fetch_result($pssel, 0, "pjt_schd_title");
						$status1 .= "title={$pjt_schd_title}<br />";

						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "3":
						// 委員会の作成
						$identirier = $row["status1"];
						$status1 = "name(project)={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "4":
						// 委員会の編集
						$identirier = $row["status1"];
						$status1 = "name(project)={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "5":
						// WGの作成
						$identirier = $row["status1"];
						$status1 = "name(WG)={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "6":
						// WGの編集
						$identirier = $row["status1"];
						$status1 = "name(WG)={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "7":
						// 委員会・WGのスケジュール作成
						$identirier = $row["status1"];
						$status1 = "title={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "8":
						// 委員会スケジュール編集
						$identirier = $row["status1"];
						$status1 = "title={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "9":
						// 種別の更新
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] === $mode[1]) {
							$identirier = $row["status1"];
							for ($i = 2; $i < 12; $i++) {
								$ArrSchd[$i - 2] = "proschd{$i}={$post_data[$identirier.$i]}";
							}
							$status1 = implode(",", $ArrSchd);
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "10":
						// 議事録作成
						$identirier = $row["status1"];
						$status1 = "sbj={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "11":
						// 議事録編集
						$identirier = $row["status1"];
						$status1 = "sbj={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "12":
						// 議事録承認
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] === $mode[1]) {
							$identirier = $row["status1"];
							$status1 = "sbj={$post_data[$identirier]}";
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "13":
						// 議事録差戻し
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] === $mode[1]) {
							$identirier = $row["status1"];
							$status1 = "sbj={$post_data[$identirier]}";
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
				}
			}
			else if($row["cate_id"] === "0105") {// お知らせ・回覧板
				$status1 = "";
				// 通達区分の削除
				if($row["detail_id"] == "1")
				{
					$key = array_keys($post_data);
					$identirier = explode(":", $row["status1"]);
					$lp_cnt_cond = 0;
					$cond_in = "";
					foreach ($identirier as $tmp_identirier)
					{
						foreach ($post_data[$tmp_identirier] as $tmp_id)
						{
							if($lp_cnt_cond == 0)
							{
								$cond_in = " where notice_id in ('".$tmp_id."'";
								//echo(" :01key=".$tmp_name);
							}
							else
							{
								$cond_in .= ",'".$tmp_id."'";
							}
							$lp_cnt_cond++;
						}
						$cond_in .= ")";
					}
					$sql = "select * from newsnotice ";
					$selNews = select_from_table($con, $sql, $cond_in, $fname);
					if ($selNews == 0) {
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}

					$lp_cnt_ext = 0;
					while($rowNews = pg_fetch_array($selNews))
					{
						if($lp_cnt_ext == 0)
						{
							$status1 .= "name={$rowNews["notice_name"]}";
							//echo(" :01key=".$tmp_name);
						}
						else
						{
							$status1 .= "<br />name={$rowNews["notice_name"]}";
						}
						$lp_cnt_ext++;
					}

					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				// カテゴリの削除
				else if($row["detail_id"] == "2")
				{
					$key = array_keys($post_data);
					$identirier = explode(":", $row["status1"]);
					$lp_cnt_cond = 0;
					$cond_in = "";
					foreach ($identirier as $tmp_identirier)
					{
						foreach ($post_data[$tmp_identirier] as $tmp_id)
						{
							if($lp_cnt_cond == 0)
							{
								$cond_in = " where newscate_id in ('".$tmp_id."'";
								//echo(" :01key=".$tmp_name);
							}
							else
							{
								$cond_in .= ",'".$tmp_id."'";
							}
							$lp_cnt_cond++;
						}
						$cond_in .= ")";
					}
					$sql = "select * from newscate ";
					$selNews = select_from_table($con, $sql, $cond_in, $fname);
					if ($selNews == 0) {
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}

					$lp_cnt_ext = 0;
					while($rowNews = pg_fetch_array($selNews))
					{
							if($lp_cnt_ext == 0)
							{
								$status1 .= "name={$rowNews["newscate_name"]}";
								//echo(" :01key=".$tmp_name);
							}
							else
							{
								$status1 .= "<br />name={$rowNews["newscate_name"]}";
							}
							$lp_cnt_ext++;
					}

					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				// 3:ゴミ箱から削除、4:ゴミ箱から元に戻す
				else if($row["detail_id"] == "3" || $row["detail_id"] == "4")
				{
					$key = array_keys($post_data);
					$identirier = explode("=", $row["identifier"]);
					$lp_cnt_cond = 0;
					$cond_in = "";
					if($post_data[$identirier[0]] === $identirier[1])
					{
						$statuses = explode("=", $row["status1"]);

						foreach ($statuses as $tmp_status)
						{
							foreach ($post_data[$tmp_status] as $tmp_id)
							{
								if($lp_cnt_cond == 0)
								{
									$cond_in = " where news_id in ('".$tmp_id."'";
								//echo(" :01key=".$tmp_name);
								}
								else
								{
									$cond_in .= ",'".$tmp_id."'";
								}
								$lp_cnt_cond++;
							}
							$cond_in .= ")";
						}
						$sql = "select * from news ";
						$selNews = select_from_table($con, $sql, $cond_in, $fname);
						if ($selNews == 0) {
							pg_close($con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}

						$lp_cnt_ext = 0;
						while($rowNews = pg_fetch_array($selNews))
						{
							if($lp_cnt_ext == 0)
							{
								$status1 .= htmlspecialchars("title={$rowNews["news_title"]}", ENT_COMPAT, 'ISO-8859-1');
								//echo(" :01key=".$tmp_name);
							}
							else
							{
								$status1 .= "<br />" . htmlspecialchars("title={$rowNews["news_title"]}", ENT_COMPAT, 'ISO-8859-1');
							}
							$lp_cnt_ext++;
						}

						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					}
				}
				// お知らせの削除
				else if($row["detail_id"] == "5"){
					foreach ($post_data["news_ids"] as $tmp_id){
						$newssql = "select news_title from news";
						$newscond = "where news_id = {$tmp_id}";
						$newssel = select_from_table($con, $newssql, $newscond, $fname);
						if ($newssel == 0) {
							pg_close($con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}
						$news_title = pg_fetch_result($newssel, 0, "news_title");
						$status1 .= htmlspecialchars("title={$news_title}", ENT_COMPAT, 'ISO-8859-1') . "<br />";
					}
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				// 通達区分の登録
				else if($row["detail_id"] == "6"){
					$identirier = $row["status1"];
					$status1 = "name={$post_data[$identirier]}";
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				// 通達区分の編集
				else if($row["detail_id"] == "7"){
					$identirier = $row["status1"];
					$status1 = "name={$post_data[$identirier]}";
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				// カテゴリの登録
				else if($row["detail_id"] == "8"){
					$identirier = $row["status1"];
					$status1 = "name={$post_data[$identirier]}";
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				// カテゴリの編集
				else if($row["detail_id"] == "9"){
					$identirier = $row["status1"];
					$status1 = "name={$post_data[$identirier]}";
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				// お知らせの登録
				else if($row["detail_id"] == "10"){
					$identirier = $row["status1"];
					$status1 = htmlspecialchars("title={$post_data[$identirier]}", ENT_COMPAT, 'ISO-8859-1');
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				// お知らせの編集
				else if($row["detail_id"] == "11"){
					$identirier = $row["status1"];
					$status1 = htmlspecialchars("title={$post_data[$identirier]}", ENT_COMPAT, 'ISO-8859-1');
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				// お知らせ（集計）CSV書き出し
				else if($row["detail_id"] == "12"){
					$mode = explode("=", $row['identifier']);
					if ($get_data[$mode[0]] === $mode[1]) {
						$identirier = $row["status1"];
						//
						$newssql = "select news_title from news";
						$newscond = "where news_id = {$get_data[$row['status1']]}";
						$newssel = select_from_table($con, $newssql, $newscond, $fname);
						if ($newssel == 0) {
							pg_close($con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}
						$news_title = pg_fetch_result($newssel, 0, "news_title");
						$status1 .= htmlspecialchars("title={$news_title}", ENT_COMPAT, 'ISO-8859-1') . "<br />";
						//
						//$status1 = "csv={$get_data['file_name']}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
					}
				}
				// お知らせ（明細）CSV書き出し
				else if($row["detail_id"] == "13"){
					$mode = explode("=", $row['identifier']);
					if ($get_data[$mode[0]] === $mode[1]) {
						$identirier = $row["status1"];
						//
						$newssql = "select news_title from news";
						$newscond = "where news_id = {$get_data[$row['status1']]}";
						$newssel = select_from_table($con, $newssql, $newscond, $fname);
						if ($newssel == 0) {
							pg_close($con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}
						$news_title = pg_fetch_result($newssel, 0, "news_title");
						$status1 .= htmlspecialchars("title={$news_title}", ENT_COMPAT, 'ISO-8859-1') . "<br />";
						//
						//$status1 = "csv={$get_data['file_name']}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
					}
				}
				// お知らせ一覧CSV書き出し
				else if($row["detail_id"] == "14"){
					$status1 = "csv={$get_data}";
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				// お知らせ督促メール送信
				else if($row["detail_id"] == "15"){
					$identirier = $row["status1"];
					//
					$newssql = "select news_title from news";
					$newscond = "where news_id = {$post_data[$row['status1']]}";
					$newssel = select_from_table($con, $newssql, $newscond, $fname);
					if ($newssel == 0) {
							pg_close($con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}
					$news_title = pg_fetch_result($newssel, 0, "news_title");
					$status1 .= htmlspecialchars("title={$news_title}", ENT_COMPAT, 'ISO-8859-1') . "<br />";
					//
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				// オプション設定の編集
				else if($row["detail_id"] == "16"){
					if ($get_data === '1') {
						$status1 = "menu=お知らせ登録画面";
					}
					else if ($get_data === '2') {
						$status1 = "menu=マイページの表示条件、一括変更";
					}
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}

				if($status1 != "")
				{
					// 処理終了
					break;
				}
			}
			else if($row["cate_id"] === "0106") {// タスク
				switch ($row["detail_id"]) {
					case '1':
						// タスクの削除
						foreach ($post_data["trashbox"] as $tmp_trashbox){
							list($tmp_pjt_id, $tmp_work_id) = explode("-", $tmp_trashbox);
							if ($tmp_pjt_id == "") {
								$nameWork = "title";
								$tblWork = "work";
							}
							else {
								$nameWork = "title（委員会）";
								$tblWork = "prowork";
							}
							$worksql = "select work_name from {$tblWork}";
							$workcond = "where work_id = {$tmp_work_id}";
							$worksel = select_from_table($con, $worksql, $workcond, $fname);
							if ($worksel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$work_name = pg_fetch_result($worksel, 0, "work_name");
							$status1 .= "{$nameWork}={$work_name}<br />";
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case '2':
						// タスクの登録
						$identirier = $row["status1"];
						$status1 = "title={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case '3':
						// タスクの編集
						$identirier = $row["status1"];
						$status1 = "title={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					default :
						break;
				}
			}
			else if($row["cate_id"] === "0107") {// 伝言メモ
				switch ($row["detail_id"]) {
					case '1':
						// 伝言メモの削除（受信履歴）
						foreach ($post_data["trashbox"] as $tmp_trashbox){
							$msgsql = "select date from message";
							$msgcond = "where message_id = {$tmp_trashbox}";
							$msgsel = select_from_table($con, $msgsql, $msgcond, $fname);
							if ($msgsel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$date = pg_fetch_result($msgsel, 0, "date");
							$year = substr($date, 0, 4);
							$month = substr($date, 4, 2);
							$day = substr($date, 6, 2);
							$hour = substr($date, 8, 2);
							$minute = substr($date, 10, 2);
							$status1 .= "date={$year}年{$month}月{$day}日{$hour}時{$minute}分<br />";
						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case '2':
						// 伝言メモの削除（送信履歴）
						foreach ($post_data["trashbox"] as $tmp_trashbox){
							$msgsql = "select date from message";
							$msgcond = "where message_id = {$tmp_trashbox}";
							$msgsel = select_from_table($con, $msgsql, $msgcond, $fname);
							if ($msgsel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$date = pg_fetch_result($msgsel, 0, "date");
							$year = substr($date, 0, 4);
							$month = substr($date, 4, 2);
							$day = substr($date, 6, 2);
							$hour = substr($date, 8, 2);
							$minute = substr($date, 10, 2);
							$status1 .= "date={$year}年{$month}月{$day}日{$hour}時{$minute}分<br />";
						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case '3':
						// 伝言メモの作成
						$identirier = $row["status1"];
						$status1 = "date={$post_data['message_date1']}年{$post_data['message_date2']}月{$post_data['message_date3']}日{$post_data['message_date4']}時{$post_data['message_date5']}分";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case '4':
						// 伝言メモの再送
						$identirier = $row["status1"];
						$status1 = "date={$post_data['message_date1']}年{$post_data['message_date2']}月{$post_data['message_date3']}日{$post_data['message_date4']}時{$post_data['message_date5']}分";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					default:
						break;
				}
			}
			else if($row["cate_id"] === "0108") {// 設備予約
				$status1 = "";
				if($row["detail_id"] == "1")
				{
					// カテゴリ・設備の削除
					$cate_list = "";
					$fcl_list = "";
					foreach ($post_data["trashbox"] as $tmp_trashbox)
					{
						//$status1 .= "message_id={$tmp_trashbox}";

						$spl_fcl = explode("-",$tmp_trashbox);

						if(count($spl_fcl) < 2)
						{
							if($cate_list == "")
							{
								$cate_list = "'".$spl_fcl[0]."'";
							}
							else
							{
								$cate_list .= ",'".$spl_fcl[0]."'";
							}
						}
						else
						{
							if($fcl_list == "")
							{
								$fcl_list = "'".$spl_fcl[1]."'";
							}
							else
							{
								$fcl_list .= ",'".$spl_fcl[1]."'";
							}
						}
					}

					if($cate_list == "")
					{
						$sql = "select facility_name, fclcate_name, fcl_code from (select facility_name ,'1' as fcl_code, '' as fclcate_name from facility where facility_id in (".$fcl_list.")) aaa";
					}
					else if($fcl_list == "")
					{
						$sql = "select facility_name, fclcate_name, fcl_code from (select fclcate_name, '2' as fcl_code, '' as facility_name from fclcate where fclcate_id in (".$cate_list.")) bbb ";
					}
					else
					{
						$sql = "select facility_name, fclcate_name, fcl_code from
						(select facility_name ,'1' as fcl_code, '' as fclcate_name from facility where facility_id in (".$fcl_list.")) aaa
						union
						select facility_name, fclcate_name, fcl_code from
						(select fclcate_name, '2' as fcl_code, '' as facility_name from fclcate where fclcate_id in (".$cate_list.")) bbb";
					}

					$selFcl = select_from_table($con, $sql, "", $fname);
					if ($selFcl == 0) {
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}

					$lp_cnt_ext = 0;
					while($rowFcl = pg_fetch_array($selFcl))
					{
						if($rowFcl["fcl_code"] == '1')
						{
							if($lp_cnt_ext == 0)
							{
								$status1 .= "name={$rowFcl["facility_name"]}";
								//echo(" :01key=".$tmp_name);
							}
							else
							{
								$status1 .= "<br />name={$rowFcl["facility_name"]}";
							}
						}
						else
						{
							if($lp_cnt_ext == 0)
							{
								$status1 .= "name(category)={$rowFcl["fclcate_name"]}";
								//echo(" :01key=".$tmp_name);
							}
							else
							{
								$status1 .= "<br />name(category)={$rowFcl["fclcate_name"]}";
							}
						}
						$lp_cnt_ext++;
					}
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				else if($row["detail_id"] == "2")
				{
					// テンプレートの削除
					$key = array_keys($post_data);
					$identirier = explode(":", $row["status1"]);
					$lp_cnt_cond = 0;
					$cond_in = "";
					foreach ($identirier as $tmp_identirier)
					{
						foreach ($post_data[$tmp_identirier] as $tmp_id)
						{
							if($lp_cnt_cond == 0)
							{
								$cond_in = " where fcltmpl_id in ('".$tmp_id."'";
								//echo(" :01key=".$tmp_name);
							}
							else
							{
								$cond_in .= ",'".$tmp_id."'";
							}
							$lp_cnt_cond++;
						}
						$cond_in .= ")";
					}

					$sql = "select * from fcltmpl ";
					$selFcl = select_from_table($con, $sql, $cond_in, $fname);
					if ($selFcl == 0) {
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}

					$lp_cnt_ext = 0;
					while($rowFcl = pg_fetch_array($selFcl))
					{
						if($lp_cnt_ext == 0)
						{
							$status1 .= "name={$rowFcl["fcltmpl_name"]}";
							//echo(" :01key=".$tmp_name);
						}
						else
						{
							$status1 .= "<br />name={$rowFcl["fcltmpl_name"]}";
						}
						$lp_cnt_ext++;
					}

					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				else if($row["detail_id"] == "3")
				{
					// 設備予約の削除
					$key = array_keys($post_data);
					$identirier = explode(":", $row["status1"]);
					foreach ($identirier as $tmp_identirier)
					{
						$status1 .= htmlspecialchars("name={$post_data[$tmp_identirier]}", ENT_COMPAT, 'ISO-8859-1') . "<br />";

						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					}
				}
				else if ($row["detail_id"] == "4") {
					// カテゴリの登録
					$identirier = $row["status1"];
					$status1 = "name(category)={$post_data[$identirier]}";
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				else if ($row["detail_id"] == "5") {
					// カテゴリの編集
					$identirier = $row["status1"];
					$status1 = "name(category)={$post_data[$identirier]}";
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				else if ($row["detail_id"] == "6") {
					// 施設・設備の登録
					$identirier = $row["status1"];
					$status1 = "name={$post_data[$identirier]}";
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				else if ($row["detail_id"] == "7") {
					// 施設・設備の編集
					$identirier = $row["status1"];
					$status1 = "name={$post_data[$identirier]}";
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				else if ($row["detail_id"] == "8") {
					// 設備予約の登録
					$identirier = $row["status1"];
					$status1 = htmlspecialchars("name={$post_data[$identirier]}", ENT_COMPAT, 'ISO-8859-1');
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				else if ($row["detail_id"] == "9") {
					// 設備予約の編集
					$identirier = $row["status1"];
					$status1 = htmlspecialchars("name={$post_data[$identirier]}", ENT_COMPAT, 'ISO-8859-1');
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				else if ($row["detail_id"] == "10") {
					// 一括登録
					$identirier = explode(":", $row['status1']);
					$fclsql = "select fclcate.fclcate_name,facility.facility_name from facility inner join fclcate on facility.fclcate_id=fclcate.fclcate_id";
					$fclcond = "where fclcate.fclcate_id={$post_data[$identirier[0]]} and facility.facility_id={$post_data[$identirier[1]]}";
					$fclsel = select_from_table($con, $fclsql, $fclcond, $fname);
					if ($fclsel == 0) {
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
					$fclcate_name = pg_fetch_result($fclsel, 0, "fclcate_name");
					$facility_name = pg_fetch_result($fclsel, 0, "facility_name");

					$status1 = "name=（{$fclcate_name}）{$facility_name}";
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				else if ($row["detail_id"] == "11") {
					// テンプレートの登録
					$identirier = $row["status1"];
					$status1 = "name={$post_data[$identirier]}";
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				else if ($row["detail_id"] == "12") {
					// テンプレートの編集
					$identirier = $row["status1"];
					$status1 = "name={$post_data[$identirier]}";
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				else if ($row["detail_id"] == "13") {
					// 権限設定の編集
					$identirier = explode(":", $row["status1"]);
					if ($identirier[1] == '') {
						$fclsql = "select fclcate_name from fclcate";
						$fclcond = "where fclcate_id='{$post_data[$identirier[0]]}'";
					}
					else {
						$fclsql = "select b.facility_name from fclcate a left join facility b on a.fclcate_id = b.fclcate_id";
						$fclcond = "where a.fclcate_id='{$post_data[$identirier[0]]}' and b.facility_id='{$post_data[$identirier[1]]}'";
					}
					$fclsel = select_from_table($con, $fclsql, $fclcond, $fname);
					if ($fclsel == 0) {
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
					$fclcate_name = pg_fetch_result($fclsel, 0, "fclcate_name");
					$facility_name = pg_fetch_result($fclsel, 0, "facility_name");

					if ($facility_name == '') {
						$status1 = "name(category)={$fclcate_name}";
					}
					else {
						$status1 = "name={$facility_name}";
					}
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				else if ($row["detail_id"] == "14") {
					// 管理者設定の編集
					$identirier = explode(":", $row["status1"]);
					if ($identirier[1] == '') {
						$fclsql = "select fclcate_name from fclcate";
						$fclcond = "where fclcate_id='{$post_data[$identirier[0]]}'";
					}
					else {
						$fclsql = "select b.facility_name from fclcate a left join facility b on a.fclcate_id = b.fclcate_id";
						$fclcond = "where a.fclcate_id='{$post_data[$identirier[0]]}' and b.facility_id='{$post_data[$identirier[1]]}'";
					}
					$fclsel = select_from_table($con, $fclsql, $fclcond, $fname);
					if ($fclsel == 0) {
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
					$fclcate_name = pg_fetch_result($fclsel, 0, "fclcate_name");
					$facility_name = pg_fetch_result($fclsel, 0, "facility_name");

					if ($facility_name == '') {
						$status1 = "name(category)={$fclcate_name}";
					}
					else {
						$status1 = "name={$facility_name}";
					}
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				else if ($row["detail_id"] == "15") {
					// 種別の編集
					$identirier = $row["status1"];
					for ($i = 0; $i < 15; $i++) {
						$ArrSchd[$i] = "type{$i}={$post_data[$identirier][$i]}";
					}
					$status1 = implode(",", $ArrSchd);
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
			}
			else if($row["cate_id"] === "0109") {// 決裁・申請
				switch ($row["detail_id"]) {
					case "1":
						// フォルダの削除
						foreach ($post_data as $key => $tmp_post_data) {
							switch ($key) {
								case "cate_ids":
									$wkfwsql = "select wkfw_nm from wkfwcatemst";
									$wkfwcond = "where wkfw_type in  (";
									$wkfwcond .= implode(",", $post_data[$key]);
									$wkfwcond .= ");";
									$wkfwsel = select_from_table($con, $wkfwsql, $wkfwcond, $fname);
									if ($wkfwsel == 0) {
										pg_close($con);
										echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
										echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
										exit;
									}
									while($wkfwrow = pg_fetch_array($wkfwsel)) {
										$status1 .= "folder={$wkfwrow['wkfw_nm']}<br />";
									}
									$cate = $row["cate_id"];
									$detail = $row["detail_id"];
									$priority = $row["priority"];
									break;
								case "folder_ids":
									$wkfwsql = "select wkfw_folder_name from wkfwfolder";
									$wkfwcond = "where wkfw_folder_id in (";
									$wkfwcond .= implode(",", $post_data[$key]);
									$wkfwcond .= ");";
									$wkfwsel = select_from_table($con, $wkfwsql, $wkfwcond, $fname);
									if ($wkfwsel == 0) {
										pg_close($con);
										echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
										echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
										exit;
									}
									while($wkfwrow = pg_fetch_array($wkfwsel)) {
										$status1 .= "folder={$wkfwrow['wkfw_folder_name']}<br />";
									}
									$cate = $row["cate_id"];
									$detail = $row["detail_id"];
									$priority = $row["priority"];
									break;
								default :
									break;
							}
						}
						break;
					case "2":
						// ワークフローの削除
						if ($get_data != '') {
							$identirier = explode("=", $row['identifier']);
							if ($get_data[$identirier[0]] == $identirier[1]) {
								$post_data['file_ids_real'] = $get_data[$row['status1']];
							}
						}
						foreach ($post_data as $key => $tmp_post_data) {
							switch ($key) {
								case "file_ids":
									$wkfwsql = "select wkfw_title from wkfwmst";
									$wkfwcond = "where wkfw_id in  (";
									$wkfwcond .= implode(",", $post_data[$key]);
									$wkfwcond .= ");";
									$wkfwsel = select_from_table($con, $wkfwsql, $wkfwcond, $fname);
									if ($wkfwsel == 0) {
										pg_close($con);
										echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
										echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
										exit;
									}
									while($wkfwrow = pg_fetch_array($wkfwsel)) {
										$status1 .= "file={$wkfwrow['wkfw_title']}<br />";
									}
									$cate = $row["cate_id"];
									$detail = $row["detail_id"];
									$priority = $row["priority"];
									break;
								case "file_ids_real":
									$wkfwsql = "select wkfw_title from wkfwmst_real";
									$wkfwcond = "where wkfw_id in (";
									if ($get_data != '') {
										// ドラッグドロップで削除
										$wkfwcond .= $post_data['file_ids_real'];
									} else {
										// チェックボックスをチェックして[削除]ボタンクリックで削除
										$wkfwcond .= implode(",", $post_data[$key]);
									}
									$wkfwcond .= ");";
									$wkfwsel = select_from_table($con, $wkfwsql, $wkfwcond, $fname);
									if ($wkfwsel == 0) {
										pg_close($con);
										echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
										echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
										exit;
									}
									while($wkfwrow = pg_fetch_array($wkfwsel)) {
										$status1 .= "file_real={$wkfwrow['wkfw_title']}<br />";
									}
									$cate = $row["cate_id"];
									$detail = $row["detail_id"];
									$priority = $row["priority"];
									break;
								default :
									break;
							}
						}
						break;
					case "3":
						// 申請一覧からの申請書削除
						$identirier = explode("=", $row["identifier"]);
						if($_GET[$identirier[0]] == $identirier[1]) {
							$identirier = $row["status1"];
							$wkfwsql = "select wkfwmst.short_wkfw_name as wkname, apply.apply_no , substr(apply_date,0,5) as wdate from apply, wkfwmst";
							$wkfwcond = " where apply.wkfw_id=wkfwmst.wkfw_id and apply_id in (";
							$wkfwcond .= implode(",", $post_data[$identirier]);
							$wkfwcond .= ");";
							$wkfwsel = select_from_table($con, $wkfwsql, $wkfwcond, $fname);
							if ($wkfwsel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							while($wkfwrow = pg_fetch_array($wkfwsel)) {
								$apply_no = $wkfwrow["wkname"]."-".$wkfwrow["wdate"]."".sprintf("%04d", $wkfwrow["apply_no"]);
								$status1 .= "number={$apply_no}<br />";
							}
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
							break;
						}
						break;
					case "4":
						// フォルダの作成
						$identirier = $row["status1"];
						$status1 = "folder={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "5":
						// フォルダの編集
						$identirier = $row["status1"];
						$status1 = "folder={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "6":
						// ワークフローの作成（フォーマットファイル名）
						$identirier = explode(":", $row["status1"]);
						$FileList = implode(",", $post_data[$identirier[1]]);
						$status1 = "wkfw={$post_data[$identirier[0]]}";
						if ($FileList != "") {
							$status1 .= "<br />（フォーマットファイル:{$FileList}）";
						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "7":
						// ワークフローの編集（フォーマットファイル名）
						$identirier = explode(":", $row["status1"]);
						$FileList = implode(",", $post_data[$identirier[1]]);
						$status1 = "wkfw={$post_data[$identirier[0]]}";
						if ($FileList != "") {
							$status1 .= "<br />（フォーマットファイル:{$FileList}）";
						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "8":
						// 印影の登録
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] === $mode[1]) {
							$status1 = "";
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
							break;
						}
						break;
					case "9":
						// 承認依頼通知文書の編集
						$identirier = $row["status1"];
						$wkfwcontent = nl2br($post_data[$identirier]);
						$status1 = "[content]<br />{$wkfwcontent}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "10":
						// 申請書の編集（添付ファイル名）
						$identirier = explode(":", $row['status1']);
						$wkfwsql = "select wkfwmst.short_wkfw_name as wkname, apply.apply_no , substr(apply_date,0,5) as wdate from apply, wkfwmst";
						$wkfwcond = " where apply.wkfw_id=wkfwmst.wkfw_id and apply_id={$post_data[$identirier[1]]}";
						$wkfwsel = select_from_table($con, $wkfwsql, $wkfwcond, $fname);
						if ($wkfwsel == 0) {
							pg_close($con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}
						$wkname = pg_fetch_result($wkfwsel, 0, "wkname");
						$wdate = pg_fetch_result($wkfwsel, 0, "wdate");
						$apply_no = pg_fetch_result($wkfwsel, 0, "apply_no");
						$FileList = implode(",", $post_data[$identirier[2]]);
						$apply_num = $wkname."-".$wdate."".sprintf("%04d", $apply_no);
						$status1 .= "number={$apply_num}";
						if ($FileList != "") {
							$status1 .= "<br />（添付ファイル:{$FileList}）";
						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "11":
						// 申請内容更新
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] === $mode[1]) {
							$identirier = $row['status1'];
							$wkfwsql = "select wkfwmst.short_wkfw_name as wkname, apply.apply_no , substr(apply_date,0,5) as wdate from apply, wkfwmst";
							$wkfwcond = " where apply.wkfw_id=wkfwmst.wkfw_id and apply_id={$post_data[$identirier]}";
							$wkfwsel = select_from_table($con, $wkfwsql, $wkfwcond, $fname);
							if ($wkfwsel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$wkname = pg_fetch_result($wkfwsel, 0, "wkname");
							$wdate = pg_fetch_result($wkfwsel, 0, "wdate");
							$apply_no = pg_fetch_result($wkfwsel, 0, "apply_no");
							$apply_num = $wkname."-".$wdate."".sprintf("%04d", $apply_no);
							$status1 .= "number={$apply_num}";
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "12":
						// 申請一覧からのCSV書き出し
						$status1 = "CSV={$get_data}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "13":
						// 申請書の申請（添付ファイル名）
						$identirier = explode(":", $row['status1']);
						$wkfwsql = "select wkfw_title from wkfwmst";
						$wkfwcond = "where wkfw_id={$post_data[$identirier[0]]}";
						$wkfwsel = select_from_table($con, $wkfwsql, $wkfwcond, $fname);
						if ($wkfwsel == 0) {
							pg_close($con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}
						$wkfw_title = pg_fetch_result($wkfwsel, 0, "wkfw_title");
						$FileList = implode(",", $post_data[$identirier[2]]);
						$status1 .= "wkfw=（{$wkfw_title}）{$post_data[$identirier[1]]}";
						if ($FileList != "") {
							$status1 .= "<br />（添付ファイル:{$FileList}）";
						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "14":
						// 申請書の再申請（添付ファイル名）
						$identirier = explode(":", $row['status1']);
						if (count($identirier) == 3) {
							$wkfwsql = "select wkfw_title from wkfwmst";
							$wkfwcond = "where wkfw_id={$post_data[$identirier[0]]}";
							$wkfwsel = select_from_table($con, $wkfwsql, $wkfwcond, $fname);
							if ($wkfwsel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$wkfw_title = pg_fetch_result($wkfwsel, 0, "wkfw_title");
							$FileList = implode(",", $post_data[$identirier[2]]);
							$status1 = "wkfw=（{$wkfw_title}）{$post_data[$identirier[1]]}";
							if ($FileList != "") {
								$status1 .= "<br />（添付ファイル:{$FileList}）";
							}
						}
						else {
							$wkfwsql = "select wkfwmst.short_wkfw_name as wkname, apply.apply_no , substr(apply_date,0,5) as wdate from apply inner join wkfwmst on apply.wkfw_id = wkfwmst.wkfw_id";
							$wkfwcond = "where apply_id in (";
							$wkfwcond .= implode(",", $post_data[$identirier[0]]);
							$wkfwcond .= ")";
							$wkfwsel = select_from_table($con, $wkfwsql, $wkfwcond, $fname);
							if ($wkfwsel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}

							while ($wkfwrow = pg_fetch_array($wkfwsel)) {
								$wkname = $wkfwrow["wkname"];
								$wdate = $wkfwrow["wdate"];
								$apply_no = $wkfwrow["apply_no"];
								$apply_num = $wkname."-".$wdate."".sprintf("%04d", $apply_no);
								$status1 .= "number={$apply_num}<br />";
							}
						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "15":
						// 申請書の承認
						$mode = explode("=", $row['identifier']);
						if ($get_data[$mode[0]] != '') {
							$post_data[$mode[0]] = $get_data[$mode[0]];
						}
						if ($post_data[$mode[0]] === $mode[1]) {
							$identirier = $row['status1'];
							$wkfwsql = "select wkfwmst.short_wkfw_name as wkname, apply.apply_no , substr(apply_date,0,5) as wdate from apply, wkfwmst";
							$wkfwcond = "where apply.wkfw_id=wkfwmst.wkfw_id and apply_id in(";
							if (is_array($post_data[$identirier])) {
								$i = 0;
								foreach ($post_data[$identirier] as $pValue) {
									$tmp = explode(",", $pValue);
									$tmp_wkfw[$i] = $tmp[0];
									$i++;
								}
								$wkfwcond .= implode(",", $tmp_wkfw);
							}
							else {
								$wkfwcond .= $post_data[$identirier];
							}
							$wkfwcond .= ")";
							$wkfwsel = select_from_table($con, $wkfwsql, $wkfwcond, $fname);
							if ($wkfwsel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							while ($wkfwrow = pg_fetch_array($wkfwsel)) {
								$wkname = $wkfwrow["wkname"];
								$wdate = $wkfwrow["wdate"];
								$apply_no = $wkfwrow["apply_no"];
								$apply_num = $wkname."-".$wdate."".sprintf("%04d", $apply_no);
								$status1 .= "number={$apply_num}<br />";
							}
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
							break;
						}
					case "16":
						// 申請書の否認
						$mode = explode("=", $row['identifier']);
						if ($get_data[$mode[0]] != '') {
							$post_data[$mode[0]] = $get_data[$mode[0]];
						}
						if ($post_data[$mode[0]] === $mode[1]) {
							$identirier = $row['status1'];
							$wkfwsql = "select wkfwmst.short_wkfw_name as wkname, apply.apply_no , substr(apply_date,0,5) as wdate from apply, wkfwmst";
							$wkfwcond = "where apply.wkfw_id=wkfwmst.wkfw_id and apply_id in(";
							if (is_array($post_data[$identirier])) {
								$i = 0;
								foreach ($post_data[$identirier] as $pValue) {
									$tmp = explode(",", $pValue);
									$tmp_wkfw[$i] = $tmp[0];
									$i++;
								}
								$wkfwcond .= implode(",", $tmp_wkfw);
							}
							else {
								$wkfwcond .= $post_data[$identirier];
							}
							$wkfwcond .= ")";
							$wkfwsel = select_from_table($con, $wkfwsql, $wkfwcond, $fname);
							if ($wkfwsel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							while ($wkfwrow = pg_fetch_array($wkfwsel)) {
								$wkname = $wkfwrow["wkname"];
								$wdate = $wkfwrow["wdate"];
								$apply_no = $wkfwrow["apply_no"];
								$apply_num = $wkname."-".$wdate."".sprintf("%04d", $apply_no);
								$status1 .= "number={$apply_num}<br />";
							}
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
							break;
						}
					case "17":
						// 申請書の差戻し
						$mode = explode("=", $row['identifier']);
						if ($get_data[$mode[0]] != '') {
							$post_data[$mode[0]] = $get_data[$mode[0]];
						}
						if ($post_data[$mode[0]] === $mode[1]) {
							$identirier = $row['status1'];
							$wkfwsql = "select wkfwmst.short_wkfw_name as wkname, apply.apply_no , substr(apply_date,0,5) as wdate from apply, wkfwmst";
							$wkfwcond = "where apply.wkfw_id=wkfwmst.wkfw_id and apply_id in(";
							if (is_array($post_data[$identirier])) {
								$i = 0;
								foreach ($post_data[$identirier] as $pValue) {
									$tmp = explode(",", $pValue);
									$tmp_wkfw[$i] = $tmp[0];
									$i++;
								}
								$wkfwcond .= implode(",", $tmp_wkfw);
							}
							else {
								$wkfwcond .= $post_data[$identirier];
							}
							$wkfwcond .= ")";
							$wkfwsel = select_from_table($con, $wkfwsql, $wkfwcond, $fname);
							if ($wkfwsel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							while ($wkfwrow = pg_fetch_array($wkfwsel)) {
								$wkname = $wkfwrow["wkname"];
								$wdate = $wkfwrow["wdate"];
								$apply_no = $wkfwrow["apply_no"];
								$apply_num = $wkname."-".$wdate."".sprintf("%04d", $apply_no);
								$status1 .= "number={$apply_num}<br />";
							}
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
							break;
						}
						break;
				}
			}
			else if($row["cate_id"] === "0110") {// 掲示板・電子会議室
				$key = array_keys($post_data);
				switch ($row["detail_id"]) {
					case "1":
						// カテゴリ・テーマの削除
						foreach ($post_data["category_ids"] as $value) {
							$bbssql = "select bbscategory_title from bbscategory";
							$bbscond = "where bbscategory_id = {$value}";
							$bbssel = select_from_table($con, $bbssql, $bbscond, $fname);
							if ($bbssel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$bbs_title = pg_fetch_result($bbssel, 0, "bbscategory_title");
							$status1 .= "category={$bbs_title}<br />";
						}
						foreach ($post_data["theme_ids"] as $value) {
							$bbssql = "select bbsthread_title from bbsthread";
							$bbscond = "where bbsthread_id = {$value}";
							$bbssel = select_from_table($con, $bbssql, $bbscond, $fname);
							if ($bbssel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$bbs_title = pg_fetch_result($bbssel, 0, "bbsthread_title");
							$status1 .= "theme={$bbs_title}<br />";
						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "2":
						// カテゴリの作成
						$identirier = $row["status1"];
						$status1 = "category={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "3":
						// 投稿削除
						$bbs_title = $post_data['bbs_title'];
						$status1 = "title={$bbs_title}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "4":
						// カテゴリの編集
						$identirier = $row["status1"];
						$status1 = "category={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "5":
						// テーマの作成
						$identirier = $row["status1"];
						$status1 = "theme={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "6":
						// テーマの編集
						$identirier = $row["status1"];
						$status1 = "theme={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "7":
						// 投稿登録（添付ファイル名）
						$identirier = explode(":", $row["status1"]);
						$FileList = implode(",", $post_data[$identirier[1]]);
						$status1 = "title={$post_data[$identirier[0]]}";
						if ($FileList != "") {
							$status1 .= "<br />（添付ファイル:{$FileList}）";
						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "8":
						// 投稿編集（添付ファイル名）
						$identirier = explode(":", $row["status1"]);
						$FileList = implode(",", $post_data[$identirier[1]]);
						$status1 = "title={$post_data[$identirier[0]]}";
						if ($FileList != "") {
							$status1 .= "<br />（添付ファイル:{$FileList}）";
						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "9":
						// オプション設定の編集
						$identirier = explode(":", $row['status1']);
						$status1 = "ユーザ画面からのカテゴリ作成を許可";
						if ($post_data[$identirier[0]] == 'f') {
							$status1 .= "しない";
						}
						else {
							$status1 .= "する";
						}
						$status1 .= "<br />ユーザ画面からのテーマ作成を許可";
						if ($post_data[$identirier[1]] == 'f') {
							$status1 .= "しない";
						}
						else {
							$status1 .= "する";
						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
				}
				break;
			}
			else if($row["cate_id"] === "0111") {// Q&A
				switch ($row["detail_id"]) {
					case "1":
						// カテゴリの削除
						foreach ($post_data["category_ids"] as $tmp_id){
							$qasql = "select qa_category from qacategory";
							$qacond = "where qa_category_id = {$tmp_id}";
							$qasel = select_from_table($con, $qasql, $qacond, $fname);
							if ($qasel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$qa_category = pg_fetch_result($qasel, 0, "qa_category");
							$status1 .= "category={$qa_category}<br />";
						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "2":
						// 質問の削除
						foreach ($post_data["question_ids"] as $tmp_id){
							$qasql = "select question_keyword from question";
							$qacond = "where question_id = {$tmp_id}";
							$qasel = select_from_table($con, $qasql, $qacond, $fname);
							if ($qasel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$question_keyword = pg_fetch_result($qasel, 0, "question_keyword");
							$status1 .= "keyword={$question_keyword}<br />";
						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "3":
						// 回答の削除
						foreach ($post_data["answer_ids"] as $tmp_id){
							$qasql = "select answer from answer";
							$qacond = "where answer_id = {$tmp_id}";
							$qasel = select_from_table($con, $qasql, $qacond, $fname);
							if ($qasel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$tmp_answer = pg_fetch_result($qasel, 0, "answer");
							$answer = nl2br($tmp_answer);
							$status1 = "[answer]<br />{$answer}";

						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "4":
						// カテゴリの作成
						$identirier = $row["status1"];
						$status1 = "category={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "5":
						// カテゴリの編集
						$identirier = $row["status1"];
						$status1 = "category={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "6":
						// 質問の登録
						$identirier = $row["status1"];
						$status1 = "keyword={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "7":
						// 質問の編集
						$identirier = $row["status1"];
						$status1 = "keyword={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "8":
						// 回答の登録
						$identirier = $row["status1"];
						$answer = nl2br($post_data[$identirier]);
						$status1 = "[answer]<br />{$answer}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "9":
						// 回答の編集
						$identirier = $row["status1"];
						$answer = nl2br($post_data[$identirier]);
						$status1 = "[answer]<br />{$answer}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
				}
			}
			else if($row["cate_id"] === "0112") {// 文書管理
				switch ($row["detail_id"]) {
					// フォルダ削除
					case "1":
						$identirier = explode(":", $row['identifier']);
						if ($post_data[$identirier[0]] != '') {
						// カテゴリフォルダの削除
							$cidsql = "select lib_cate_nm from libcate";
							$cidcond = "where lib_cate_id in (";
							$cidcond .= implode(",", $post_data[$identirier[0]]);
							$cidcond .= ")";
							$cidsel = select_from_table($con, $cidsql, $cidcond, $fname);
							if ($cidsel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}

							while($cidrow = pg_fetch_array($cidsel)) {
								$status1 .= "folder={$cidrow['lib_cate_nm']}<br />";
							}
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
							break;
						}
						else if ($post_data[$identirier[1]] != '') {
							// 子フォルダの削除
							$fidsql = "select folder_name from libfolder";
							$fidcond = "where folder_id in (";
							$fidcond .= implode(",", $post_data[$identirier[1]]);
							$fidcond .= ")";
							$fidsel = select_from_table($con, $fidsql, $fidcond, $fname);
							if ($fidsel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}

							while($fidrow = pg_fetch_array($fidsel)) {
								$status1 .= "folder={$fidrow['folder_name']}<br />";
							}
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
							break;
						}
					// 文書削除
					case "2":
						if ($post_data[$row['identifier']] == '') {
							// 「文書削除」と「フォルダ削除」は処理するPHPファイルが同じであるため
							// フォルダのみの削除であれば「文書削除」は処理しない
							continue;
						}
						$didsql = "select lib_nm from libinfo";
						$didcond = "where lib_id in (";
						$didcond .= implode(",", $post_data[$row['identifier']]);
						$didcond .= ")";
						$didsel = select_from_table($con, $didsql, $didcond, $fname);
						if ($didsel == 0) {
							pg_close($con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}

						while($didrow = pg_fetch_array($didsel)) {
							$status1 .= "document={$didrow['lib_nm']}<br />";
						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "3":
						// フォルダの作成
						$identirier = $row["status1"];
						$status1 = "folder={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "4":
						// フォルダの編集
						$identirier = $row["status1"];
						$status1 = "folder={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "5":
						// 文書の登録
						$identirier = $row["status1"];
						for ($i = 1;$i < 6;$i++) {
							if ($post_data[$identirier.$i] != '') {
								$status1 .= "document_name={$post_data[$identirier.$i]}<br />";
							}
						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "6":
						// 文書の編集
						$identirier = $row["status1"];
						$status1 .= "document_name={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "7":
                        // オプション設定の編集
                        $identirier = $row["status1"];

                        $status1 = $post_data["section_use"] == 't' ? "部署共有：使用する<br />" : "部署共有：使用しない<br />";
                        $status1 .= $post_data["project_use"] == 't' ? "委員会・WG共有：使用する<br />" : "委員会・WG共有：使用しない<br />";
                        $status1 .= $post_data["private_use"] == 't' ? "個人ファイル：使用する<br />" : "個人ファイル：使用しない<br />";
                        $status1 .= $post_data["show_login_flg"] == 't' ? "ログインページへ表示する：表示する<br />" : "ログインページへ表示する：表示しない<br />";
                        $status1 .= $post_data["stick_folder"] == 't' ? "文書一覧でフォルダを上に表示する：表示する<br />" : "文書一覧でフォルダを上に表示する：表示しない<br />";
                        $status1 .= $post_data["lock_show_flg"] == 't' ? "鍵つきフォルダ表示：表示する<br />" : "鍵つきフォルダ表示：表示しない<br />";
                        $status1 .= $post_data["count_flg"] == 't' ? "フォルダ内文書数表示：表示する<br />" : "フォルダ内文書数表示：表示しない<br />";
                        $status1 .= $post_data["dragdrop_flg"] == 't' ? "文書のドラッグ＆ドロップ：有効にする<br />" : "文書のドラッグ＆ドロップ：無効にする<br />";
                        $status1 .= $post_data["real_name_flg"] == 't' ? "ダウンロード時の文書名：登録文書名<br />" : "ダウンロード時の文書名：システム名<br />";
                        $status1 .= $post_data["show_tree_flg"] == 't' ? "フォルダツリーの表示：表示する<br />" : "フォルダツリーの表示：表示しない<br />";
                        $status1 .= $post_data["pjt_private_flg"] == 't' ? "委員会・WG登録時の参照可能範囲の初期設定：メンバーのみ参照可能<br />" : "委員会・WG登録時の参照可能範囲の初期設定：全員参照可能<br />";
                        $status1 .= $post_data["show_newlist_flg"] == 't' ? "最新文書一覧を表示する：表示する<br />" : "最新文書一覧を表示する：表示しない<br />";
                        $status1 .= $post_data["all_createfolder_flg"] == 't' ? "全体共有にフォルダ作成を許可する：許可する" : "全体共有にフォルダ作成を許可する：許可しない";

                        $cate = $row["cate_id"];
                        $detail = $row["detail_id"];
                        $priority = $row["priority"];
                        break;
					default:
						break;
				}
			}
			else if($row["cate_id"] === "0113") {// アドレス帳
				switch ($row["detail_id"]) {
					case "1":
						//  アドレス帳の削除
						$addsql = "select name1,name2 from address";
						$addcond = "where address_id in (";
						$addcond .= implode(",", $post_data["trashbox"]);
						$addcond .= ");";
						$addsel = select_from_table($con, $addsql, $addcond, $fname);
						if ($addsel == 0) {
							pg_close($con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}
						while($addrow = pg_fetch_array($addsel)) {
							$status1 .= "name={$addrow['name1']} {$addrow['name2']}<br />";
						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "2":
						// アドレス帳の登録
						if (array_key_exists($row["identifier"], $get_data)) {
							// アドレス帳の編集時に共有にした場合はログを取得しない
							break;
						}
						$identirier = explode(":",$row["status1"]);
						$status1 = "name={$post_data[$identirier[0]]} {$post_data[$identirier[1]]}";
						if ($post_data[$row["identifier"]] == 't') {
							$status1 .= "（共有）";
						}
						else {
							$status1 .= "（個人）";
						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "3":
						// アドレス帳の編集
						$identirier = explode(":",$row["status1"]);
						$status1 .= "name={$post_data[$identirier[0]]} {$post_data[$identirier[1]]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "4":
						// アドレス帳の一括登録の実行
						$identirier = explode(":", $row["status1"]);
						if ($post_data[$identirier[0]] == 't') {
							$status1 .= "mode=管理画面（{$post_data[$identirier[1]]}";
							$status1 .= $post_data[$identirier[2]] == 't' ? ":共有）" : ":個人）";
						}
						else {
							$status1 .= "mode=ユーザ画面";
						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "5":
						// アドレス帳を共有へコピー
						$shared_flg = explode("=", $row["identifier"]);
						if (!array_key_exists($shared_flg[0], $get_data)) {
							// アドレス帳の登録時に「共有アドレス帳」にした場合はログを取得しない
							break;
						}
						$identirier = explode(":",$row["status1"]);
						$status1 = "name={$post_data[$identirier[0]]} {$post_data[$identirier[1]]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "6":
						// 本人連絡先更新（内容に表示するものはありません。）
						$status1 = "";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
				}
			}
			else if($row["cate_id"] === "0114") {// リンクライブラリ
				$status1 = "";
				if($row["detail_id"] == "1")
				{
					$key = array_keys($post_data);
					$identirier = explode(":", $row["status1"]);
					$lp_cnt = 0;
					foreach ($identirier as $tmp_identirier)
					{
						foreach ($post_data[$tmp_identirier] as $tmp_name)
						{
							if($lp_cnt == 0)
							{
								if ($tmp_name != '') {
									$status1 .= "name=[{$tmp_name}]";
									//echo(" :01key=".$tmp_name);
								}
							}
							else
							{
								if ($tmp_name != '') {
									$status1 .= "<br />name=[{$tmp_name}]";
								}
							}
							$lp_cnt++;
						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					}
				}
				else if($row["detail_id"] == "2")
				{
					$key = array_keys($post_data);
					$identirier = explode(":", $row["status1"]);
					$lp_cnt = 0;
					foreach ($identirier as $tmp_identirier)
					{
						foreach ($post_data[$tmp_identirier] as $tmp_name)
						{
							if($lp_cnt == 0)
							{
								if ($tmp_name != '') {
									$status1 .= "name=[{$tmp_name}]";
									//echo(" :01key=".$tmp_name);
								}
							}
							else
							{
								if ($tmp_name != '') {
									$status1 .= "<br />name=[{$tmp_name}]";
								}
							}
							$lp_cnt++;
						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					}
				}

				if($status1 != "")
				{
					// 処理終了
					break;
				}
			}
			else if($row["cate_id"] === "0115") {// 内線電話帳
				$status1 = "";
				if($row["detail_id"] == "1")
				{
					// 施設連絡先の削除
					$key = array_keys($post_data);
					$identirier = explode(":", $row["status1"]);
					$lp_cnt_cond = 0;
					$cond_in = "";
					foreach ($identirier as $tmp_identirier)
					{
						foreach ($post_data[$tmp_identirier] as $tmp_id)
						{
							if($lp_cnt_cond == 0)
							{
								$cond_in = " where extfcl_id in ('".$tmp_id."'";
								//echo(" :01key=".$tmp_name);
							}
							else
							{
								$cond_in .= ",'".$tmp_id."'";
							}
							$lp_cnt_cond++;
						}
						$cond_in .= ")";
					}
					// 施設連絡先の削除は画面の制限上20件が限界なのでin句を使用する
					$sql = "select * from extfcl ";
					$selExt = select_from_table($con, $sql, $cond_in, $fname);
					if ($selExt == 0) {
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}

					$lp_cnt_ext = 0;
					while($rowExt = pg_fetch_array($selExt))
					{
							if($lp_cnt_ext == 0)
							{
								$status1 .= "name={$rowExt["fcl_nm"]}";
								//echo(" :01key=".$tmp_name);
							}
							else
							{
								$status1 .= "<br />name={$rowExt["fcl_nm"]}";
							}
							$lp_cnt_ext++;

					}
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;

				}
				else if($row["detail_id"] == "2")
				{
					// 番号リストの削除
					$key = array_keys($post_data);
					$identirier = explode(":", $row["status1"]);
					$lp_cnt_cond = 0;
					$cond_in = "";
					foreach ($identirier as $tmp_identirier)
					{
						foreach ($post_data[$tmp_identirier] as $tmp_id)
						{
							if($lp_cnt_cond == 0)
							{
								$cond_in = " where extdoc_id in ('".$tmp_id."'";
								//echo(" :01key=".$tmp_name);
							}
							else
							{
								$cond_in .= ",'".$tmp_id."'";
							}
							$lp_cnt_cond++;
						}
						$cond_in .= ")";
					}
					//番号リストの削除は画面の制限上20件が限界なのでin句を使用する
					$sql = "select * from extdoc ";
					$selExt = select_from_table($con, $sql, $cond_in, $fname);
					if ($selExt == 0) {
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}

					$lp_cnt_ext = 0;
					while($rowExt = pg_fetch_array($selExt))
					{
						if($lp_cnt_ext == 0)
						{
							$status1 .= "document={$rowExt["doc_nm"]}";
							//echo(" :01key=".$tmp_name);
						}
						else
						{
							$status1 .= "<br />document={$rowExt["doc_nm"]}";
						}
						$lp_cnt_ext++;
					}
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;

				}
				else if($row["detail_id"] == "3") {
					// 施設連絡先の登録（添付ファイル名）
					$identirier = $row["status1"];
					$status1 .= "fcl_name={$post_data[$identirier]}<br />";
					if ($get_data['file']['name'] != '') {
						$status1 .= "file={$get_data['file']['name']}";
					}
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				else if($row["detail_id"] == "4") {
					// 施設連絡先の編集（添付ファイル名）
					$identirier = explode(":", $row["status1"]);
					$status1 .= "fcl_name={$post_data[$identirier[0]]}<br />";
					switch ($post_data[$identirier[1]]) {
						case "2":
							$status1 .= "file={$get_data['file']['name']}";
							break;
						case "3":
							$status1 .= "添付ファイル削除";
							break;
						default :
							break;
					}
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				else if($row["detail_id"] == "5") {
					// 部門連絡先の登録（内容に表示するものはありません。）
					$status1 = "";
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				else if($row["detail_id"] == "6") {
					// 番号リスト登録（添付ファイル名）
					$identirier = $row["status1"];
					$status1 = "document={$post_data[$identirier]}";
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				else if($row["detail_id"] == "7") {
					// 連絡先番号の編集
					$identirier = $row["status1"];
					if (array_key_exists($identirier, $post_data)) {
						$extsql = "select emp_lt_nm,emp_ft_nm from empmst";
						$extcond = "where emp_id = '{$post_data[$identirier]}'";
						$extsel = select_from_table($con, $extsql, $extcond, $fname);
						if ($extsel == 0) {
							pg_close($con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}
						$emp_lt_nm = pg_fetch_result($extsel, 0, "emp_lt_nm");
						$emp_ft_nm = pg_fetch_result($extsel, 0, "emp_ft_nm");
						$status1 = "target={$emp_lt_nm} {$emp_ft_nm}";
					}
					else {
						$ext_emp_id = get_emp_id($con, $session, $fname);
						$extsql = "select emp_lt_nm,emp_ft_nm from empmst";
						$extcond = "where emp_id = '{$ext_emp_id}'";
						$extsel = select_from_table($con, $extsql, $extcond, $fname);
						if ($extsel == 0) {
							pg_close($con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}
						$emp_lt_nm = pg_fetch_result($extsel, 0, "emp_lt_nm");
						$emp_ft_nm = pg_fetch_result($extsel, 0, "emp_ft_nm");
						$status1 = "target={$emp_lt_nm} {$emp_ft_nm}";
					}
					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}
				else if($row["detail_id"] == "8") {
					// オプション設定の編集（管理画面）
					$identirier = explode(":", $row['status1']);

					switch ($post_data[$identirier[0]]) {
						case "t":
							$status1 = "頭文字を表示する<br />";
							break;
						case "f":
							$status1 = "頭文字を表示しない<br />";
							break;
						default:
							$status1 = "頭文字の表示は変更しない<br />";
							break;
					}
					switch ($post_data[$identirier[1]]) {
						case "t":
							$status1 .= "施設を表示する<br />";
							break;
						case "f":
							$status1 .= "施設を表示しない<br />";
							break;
						default:
							$status1 .= "施設の表示は変更しない<br />";
							break;
					}
					switch ($post_data[$identirier[2]]) {
						case "t":
							$status1 .= "部門を表示する<br />";
							break;
						case "f":
							$status1 .= "部門を表示しない<br />";
							break;
						default:
							$status1 .= "部門の表示は変更しない<br />";
							break;
					}
					switch ($post_data[$identirier[3]]) {
						case "t":
							$status1 .= "「今月の新人」機能を使用する";
							break;
						case "f":
							$status1 .= "「今月の新人」機能を使用しない";
							break;
					}

					$cate = $row["cate_id"];
					$detail = $row["detail_id"];
					$priority = $row["priority"];
					break;
				}

				if($status1 != "")
				{
					// 処理終了
					break;
				}
			}
			else if($row["cate_id"] === "0202") { // スタッフ・ポートフォリオ
				if($row["detail_id"] != "1" && $row["detail_id"] != "2") break;
				$status1 = "";
				$key = array_keys($post_data);
				$identirier = explode(":", $row["status1"]);
				$cate = $row["cate_id"];
				$detail = $row["detail_id"];
				$priority = $row["priority"];
				$ary = array();
				if($row["detail_id"] == "1") { // ユーザ画面閲覧
					foreach ($identirier as $tmp_identirier) {
						if (!is_array($post_data[$tmp_identirier])) continue;
						foreach ($post_data[$tmp_identirier] as $tmp_name) {
							if ($tmp_name) $ary[]= $tmp_name;
						}
					}
				}
				else if($row["detail_id"] == "2") { // 管理画面閲覧
					foreach ($identirier as $tmp_identirier) {
						if (!is_array($post_data[$tmp_identirier])) continue;
						foreach ($post_data[$tmp_identirier] as $tmp_name) {
							if ($tmp_name) $ary[]= $tmp_name;
						}
					}
				}
				$status1 = implode("<br />", $ary);
				break;
			}
			else if($row["cate_id"] === "0201") {// ファントルくん
				switch ($row["detail_id"]) {
					case "1":
						// 担当者設定の更新
						$identirier = $row['status1'];
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] == $mode[1]) {
							$fauthsql = "select auth_name from inci_auth_mst";
							$fauthcond = "where auth = '{$post_data[$identirier]}'";
							$fauthsel = select_from_table($con, $fauthsql, $fauthcond, $fname);
							if ($fauthsel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$auth_name = pg_fetch_result($fauthsel, 0, "auth_name");
							$status1 .= "charge={$auth_name}";
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "2":
						// 管理項目の変更
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "3":
						// eラーニング感想の削除
						if ($post_data["postback_mode"] == "delete_kansou") {
							$fsql = "select emp_lt_nm,emp_ft_nm from empmst";
							$fcond = "where emp_id = '{$post_data["kansou_emp_id"]}'";
							$fsel = select_from_table($con, $fsql, $fcond, $fname);
							if ($fsel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$femp_lt_nm = pg_fetch_result($fsel, 0, "emp_lt_nm");
							$femp_ft_nm .= pg_fetch_result($fsel, 0, "emp_ft_nm");
							$status1 .= "name={$femp_lt_nm} {$femp_ft_nm}";
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "4":
						// お知らせの削除
						foreach ($post_data["news_ids"] as $tmp_id) {
							$fnsql = "select news_title from inci_news";
							$fncond = "where news_id = '{$tmp_id}'";
							$fnsel = select_from_table($con, $fnsql, $fncond, $fname);
							if ($fnsel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$news_title = pg_fetch_result($fnsel, 0, "news_title");
							$status1 .= "title={$news_title}<br />";
						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "5":
						// 報告書メールの削除（受信フォルダ）
						$itChk = explode("=", $row["identifier"]);
						if ($post_data[$itChk[0]] == $itChk[1])
						{
							$key = array_keys($post_data);
							$identirier = explode(":", $row["status1"]);
							$lp_cnt_cond = 0;
							$cond_in = "";
							foreach ($identirier as $tmp_identirier)
							{
								foreach ($post_data[$tmp_identirier] as $tmp_id)
								{
									if($lp_cnt_cond == 0)
									{
										$cond_in = " where aa.report_id=bb.report_id and bb.mail_id in ('".$tmp_id."'";
										//echo(" :01key=".$tmp_name);
									}
									else
									{
										$cond_in .= ",'".$tmp_id."'";
									}
									$lp_cnt_cond++;
								}
								$cond_in .= ")";
							}
							$sql = "select report_no from inci_report aa , inci_mail_send_info bb  ";
							$selExt = select_from_table($con, $sql, $cond_in, $fname);
							if ($selExt == 0)
							{
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}

							$lp_cnt_ext = 0;
							while($rowExt = pg_fetch_array($selExt))
							{
								if($lp_cnt_ext == 0)
								{
									$status1 .= "report_no={$rowExt["report_no"]}";
									//echo(" :01key=".$tmp_name);
								}
								else
								{
									$status1 .= "<br />report_no={$rowExt["report_no"]}";
								}
								$lp_cnt_ext++;
							}
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
							break;
						}
						break;
					case "6":
						// 報告書メールの削除（送信フォルダ）
						$itChk = explode("=", $row["identifier"]);
						if ($post_data[$itChk[0]] == $itChk[1])
						{
							$key = array_keys($post_data);
							$identirier = explode(":", $row["status1"]);
							$lp_cnt_cond = 0;
							$cond_in = "";
							foreach ($identirier as $tmp_identirier)
							{
								foreach ($post_data[$tmp_identirier] as $tmp_id)
								{
									if($lp_cnt_cond == 0)
									{
										$cond_in = " where aa.report_id=bb.report_id and bb.mail_id in ('".$tmp_id."'";
										//echo(" :01key=".$tmp_name);
									}
									else
									{
										$cond_in .= ",'".$tmp_id."'";
									}
									$lp_cnt_cond++;
								}
								$cond_in .= ")";
							}
							$sql = "select report_no from inci_report aa , inci_mail_send_info bb  ";
							$selExt = select_from_table($con, $sql, $cond_in, $fname);
							if ($selExt == 0)
							{
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}

							$lp_cnt_ext = 0;
							while($rowExt = pg_fetch_array($selExt))
							{
								if($lp_cnt_ext == 0)
								{
									$status1 .= "report_no={$rowExt["report_no"]}";
									//echo(" :01key=".$tmp_name);
								}
								else
								{
									$status1 .= "<br />report_no={$rowExt["report_no"]}";
								}
								$lp_cnt_ext++;
							}
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
							break;
						}
						break;
					case "7":
						// 報告書をゴミ箱へ移動
						$itChk = explode("=", $row["identifier"]);
						if ($post_data[$itChk[0]] == $itChk[1])
						{
							$identirier = explode(":", $row["status1"]);
							$lp_cnt_cond = 0;
							$cond_in = "";
							foreach ($identirier as $tmp_identirier)
							{
								foreach ($post_data[$tmp_identirier] as $tmp_id)
								{
									if($lp_cnt_cond == 0)
									{
										$cond_in = " where report_id in ('".$tmp_id."'";
										//echo(" :01key=".$tmp_name);
									}
									else
									{
										$cond_in .= ",'".$tmp_id."'";
									}
									$lp_cnt_cond++;
								}
								$cond_in .= ")";
							}
							$sql = "select report_no from inci_report ";
							$selExt = select_from_table($con, $sql, $cond_in, $fname);
							if ($selExt == 0)
							{
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}

							$lp_cnt_ext = 0;
							while($rowExt = pg_fetch_array($selExt))
							{
								if($lp_cnt_ext == 0)
								{
									$status1 .= "report_no={$rowExt["report_no"]}";
									//echo(" :01key=".$tmp_name);
								}
								else
								{
									$status1 .= "<br />report_no={$rowExt["report_no"]}";
								}
								$lp_cnt_ext++;
							}
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
							break;
						}
						break;
					case "8":
						// 報告書をゴミ箱から戻す
						$itChk = explode("=", $row["identifier"]);
						if ($post_data[$itChk[0]] == $itChk[1])
						{

							$key = array_keys($post_data);
							$identirier = explode(":", $row["status1"]);
							$lp_cnt_cond = 0;
							$cond_in = "";
							foreach ($identirier as $tmp_identirier)
							{
								foreach ($post_data[$tmp_identirier] as $tmp_id)
								{
									if($lp_cnt_cond == 0)
									{
										$cond_in = " where report_id in ('".$tmp_id."'";
										//echo(" :01key=".$tmp_name);
									}
									else
									{
										$cond_in .= ",'".$tmp_id."'";
									}
									$lp_cnt_cond++;
								}
								$cond_in .= ")";
							}
							$sql = "select report_no from inci_report ";
							$selExt = select_from_table($con, $sql, $cond_in, $fname);
							if ($selExt == 0)
							{
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}

							$lp_cnt_ext = 0;
							while($rowExt = pg_fetch_array($selExt))
							{
								if($lp_cnt_ext == 0)
								{
									$status1 .= "report_no={$rowExt["report_no"]}";
									//echo(" :01key=".$tmp_name);
								}
								else
								{
									$status1 .= "<br />report_no={$rowExt["report_no"]}";
								}
								$lp_cnt_ext++;

							}
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
							break;

						}
						break;
					case "9":
						// 報告書をゴミ箱から削除
						if ($post_data["mode"] == "kill") {
							$sql = "select report_no from inci_report ";
							$cond_in = " where not shitagaki_flg and del_flag and kill_flg='f'";
							//$sql = "select report_no from inci_report ";
							$selFcl = select_from_table($con, $sql, $cond_in, $fname);
							if ($selFcl == 0)
							{
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}

							$lp_cnt_ext = 0;
							while($rowFcl = pg_fetch_array($selFcl))
							{
								if($lp_cnt_ext == 0)
								{
									$status1 .= "report_no={$rowFcl["report_no"]}";
									//echo(" :01key=".$tmp_name);
								}
								else
								{
									$status1 .= "<br />report_no={$rowFcl["report_no"]}";
								}
								$lp_cnt_ext++;

							}
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "10":
						// 分類フォルダの削除
						if ($post_data["mode"] == "folder_delete") {
							foreach ($post_data["folder_select"] as $tmp_folder) {
								$ffsql = "select folder_name from inci_classification_folder";
								$ffcond = "where folder_id = '{$tmp_folder}'";
								$ffsel = select_from_table($con, $ffsql, $ffcond, $fname);
								if ($ffsel == 0) {
									pg_close($con);
									echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
									echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
									exit;
								}
								$folder_name = pg_fetch_result($ffsel, 0, "folder_name");
								$status1 .= "folder-name={$folder_name}<br />";
							}
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "11":
						// データ提出(CSV)のフォルダ削除
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] == $mode[1]) {
							$identirier = $row["status1"];
							$fcsvsql = "select folder_name from inci_hiyari_folder";
							$fcsvcond = "where folder_id in(";
							$fcsvcond .= implode(",", $post_data[$identirier]);
							$fcsvcond .= ")";
							$fcsvsel = select_from_table($con, $fcsvsql, $fcsvcond, $fname);
							if ($fcsvsel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							while($fcsvrow = pg_fetch_array($fcsvsel)) {
								$status1 .= "folder={$fcsvrow['folder_name']}<br />";
							}
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "12":
						// データ提出(XML)のフォルダ削除
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] == $mode[1]) {
							$identirier = $row["status1"];
							$fxmlsql = "select folder_name from inci_hiyari_xml_folder";
							$fxmlcond = "where folder_id in(";
							$fxmlcond .= implode(",", $post_data[$identirier]);
							$fxmlcond .= ")";
							$fxmlsel = select_from_table($con, $fxmlsql, $fxmlcond, $fname);
							if ($fxmlsel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							while($fxmlrow = pg_fetch_array($fxmlsel)) {
								$status1 .= "folder={$fxmlrow['folder_name']}<br />";
							}
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "13":
						// GRMロック
						$mode = explode("=", $row['identifier']);
						if ($get_data[$mode[0]] == $mode[1]) {
							// 「進捗設定」タブ
							$identirier = explode(":", $row["status1"]);
							$sql = "select report_no from inci_report";
							$cond = "where {$identirier[0]}='{$post_data[$identirier[0]]}'";
							$selFtl = select_from_table($con, $sql, $cond, $fname);
							if ($selFtl == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$report_no = pg_fetch_result($selFtl, 0, "report_no");
							$status1 = "進捗設定：report_no={$report_no}";
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
							break;
						}
						else if ($get_data[$mode[0]] == $mode[2]) {
							// 「進捗登録」タブ
							// GRMの略称を取得
							$grmnmsql = "select auth_short_name from inci_auth_mst";
							$grmnmcond = "where auth='SM'";
							$grmnmsel = select_from_table($con, $grmnmsql, $grmnmcond, $fname);
							if ($grmnmsel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$auth_short_name = pg_fetch_result($grmnmsel, 0, "auth_short_name");

							// 事案番号を取得
							$identirier = explode(":", $row["status1"]);
							$sql = "select report_no from inci_report";
							$cond = "where {$identirier[0]}='{$post_data[$identirier[0]]}'";
							$selFtl = select_from_table($con, $sql, $cond, $fname);
							if ($selFtl == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$report_no = pg_fetch_result($selFtl, 0, "report_no");

							foreach ($post_data as $key => $value) {
								if (preg_match("/{$identirier[2]}/", $key)) {
									$yearFrom = $value;
								}
								if (preg_match("/{$identirier[3]}/", $key)) {
									$monFrom = $value;
								}
								if (preg_match("/{$identirier[4]}/", $key)) {
									$dayFrom = $value;
								}
								if (preg_match("/{$identirier[5]}/", $key)) {
									$yearTo = $value;
								}
								if (preg_match("/{$identirier[6]}/", $key)) {
									$monTo = $value;
								}
								if (preg_match("/{$identirier[7]}/", $key)) {
									$dayTo = $value;
								}
							}

							$status1 = "進捗登録：report_no={$report_no}<br />";
							$status1 .= "受付:{$yearFrom}/{$monFrom}/{$dayFrom}　確認:{$yearTo}/{$monTo}/{$dayTo}";
							$status1 .= $post_data[$identirier[1]] == 't' ? "<br />{$auth_short_name}_Lock=ON" : "<br />{$auth_short_name}_Lock=OFF";
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
							break;
						}
						break;
					case "14":
						// 出来事分析の削除
						if ($post_data["mode"] == "delete") {
							foreach ($post_data as $key => $value) {
								if ($key == $row["status1"]) {
									foreach ($post_data[$key] as $tmp_id) {
										$hadsql = "select analysis_title from inci_analysis_regist";
										$hadcond = "where analysis_id= '{$tmp_id}'";
										$hadsel = select_from_table($con, $hadsql, $hadcond, $fname);
										if ($hadsel == 0) {
											pg_close($con);
											echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
											echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
											exit;
										}
										$status1 .= "title=".pg_fetch_result($hadsel, 0, "analysis_title")."<br />";

									}
									$detail = $row["detail_id"];
									$cate = $row["cate_id"];
									$priority = $row["priority"];
									break;
								}
							}
						}
						break;
					case "15":
						// 「報告書をゴミ箱へ移動」ドラッグドロップ
						$itChk = explode(":", $row["identifier"]);
						$param1 = explode("=", $itChk[0]);
						$param2 = explode("=", $itChk[1]);
						if (($post_data[$param1[0]] == $param1[1]) && ($post_data[$param2[0]] == $param2[1])) {
							$report_id = $post_data[$row["status1"]];
							$ddsql = "select report_no from inci_report ";
							$ddcond = "where report_id = {$report_id}";
							$ddsel = select_from_table($con, $ddsql, $ddcond, $fname);
							if ($ddsel == 0)
							{
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$report_no = pg_fetch_result($ddsel, 0, "report_no");
							$status1 .= "report_no={$report_no}<br />";

							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
							break;
						}
						break;
					case "16":
						// 「報告書をゴミ箱から戻す」ドラッグドロップ
						$itChk = explode(":", $row["identifier"]);
						$param1 = explode("=", $itChk[0]);
						$param2 = explode("=", $itChk[1]);
						if (($post_data[$param1[0]] == $param1[1]) && ($post_data[$param2[0]] == $param2[1])) {
							$report_id = $post_data[$row["status1"]];
							$ddsql = "select report_no from inci_report ";
							$ddcond = "where report_id = {$report_id}";
							$ddsel = select_from_table($con, $ddsql, $ddcond, $fname);
							if ($ddsel == 0)
							{
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$report_no = pg_fetch_result($ddsel, 0, "report_no");
							$status1 .= "report_no={$report_no}<br />";

							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
							break;
						}
						break;
					case "17":
						// 報告書の更新
						session_name("hyr_sid");
						session_start();
						$report_id = $_SESSION['easyinput']['report_id'];

						$itChk = explode(":", $row["identifier"]);
						$param = explode("=", $itChk[0]);
						if ($post_data[$param[0]] == $param[1]) {
							$fupdsql = "select report_no from inci_report";
							$fupdcond = "where report_id = {$report_id}";
							$fupdsel = select_from_table($con, $fupdsql, $fupdcond, $fname);
							if ($fupdsel == 0)
							{
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$report_no = pg_fetch_result($fupdsel, 0, "report_no");
							$status1 .= "report_no={$report_no}<br />";

							$detail = $row["detail_id"];
							$cate = $row["cate_id"];
							$priority = $row["priority"];
						}
						break;
					case "18":
						// 報告書様式の登録
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] == $mode[1]) {
							$identirier = $row["status1"];
							$status1 = "name={$post_data[$identirier]}";
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "19":
						// 報告書様式の編集
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] == $mode[1]) {
							$identirier = $row["status1"];
							$status1 = "name={$post_data[$identirier]}";
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "20":
						// お知らせの登録
						$identirier = $row["status1"];
						$status1 = "title={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "21":
						// お知らせの編集
						$identirier = $row["status1"];
						$status1 = "title={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "22":
						// 分類フォルダの作成
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] == $mode[1]) {
							$identirier = $row["status1"];
							$status1 = "folder={$post_data[$identirier]}";
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "23":
						// 分類フォルダの編集
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] == $mode[1]) {
							$identirier = $row["status1"];
							$status1 = "folder={$post_data[$identirier]}";
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "24":
						// 報告書メールの送信
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] == $mode[1]) {
							$identirier = $row["status1"];
							$status1 = "subject={$post_data[$identirier]}";
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "25":
						// 報告書メールの返信
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] == $mode[1]) {
							$identirier = $row["status1"];
							$status1 = "subject={$post_data[$identirier]}";
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "26":
						// 報告書メールの転送
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] == $mode[1]) {
							$identirier = $row["status1"];
							$status1 = "subject={$post_data[$identirier]}";
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "27":
						// e-ラーニングフォルダの作成
						$identirier = $row["status1"];
						$status1 = "folder={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "28":
						// e-ラーニングフォルダの編集
						$identirier = $row["status1"];
						$status1 = "folder={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "29":
						// e-ラーニングコンテンツの登録
						$identirier = $row["status1"];
						for ($i = 1;$i < 6;$i++) {
							if ($post_data[$identirier.$i] != '') {
								$status1 .= "document_name={$post_data[$identirier.$i]}<br />";
							}
						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "30":
						// e-ラーニングコンテンツの編集
						$identirier = $row["status1"];
						$status1 = "document_name={$post_data[$identirier]}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "31":
						// データ提出（CSV）のCSV書き出し
						$status1 = "csv={$get_data}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "32":
						// データ提出（CSV）のフォルダ作成
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] == $mode[1]) {
							$identirier = $row["status1"];
							$status1 = "folder={$post_data[$identirier]}";
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "33":
						// データ提出（CSV）のフォルダ編集
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] == $mode[1]) {
							$identirier = $row["status1"];
							$status1 = "folder={$post_data[$identirier]}";
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "34":
						// データ提出（XML）のXML書き出し
						$status1 = "xml={$get_data}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "35":
						// データ提出（XML）のフォルダ作成
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] == $mode[1]) {
							$identirier = $row["status1"];
							$status1 = "folder={$post_data[$identirier]}";
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "36":
						// データ提出（XML）のフォルダ編集
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] == $mode[1]) {
							$identirier = $row["status1"];
							$status1 = "folder={$post_data[$identirier]}";
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "37":
						// 担当者名称の更新
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] == $mode[1]) {
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "38":
						// 担当者の登録
						$identirier = $row['status1'];
						$fchargesql = "select auth_name from inci_auth_mst";
						switch ($post_data[$identirier]) {
							case "1":
								$fchargecond = "where auth='RM'";
								break;
							case "2":
								$fchargecond = "where auth='RA'";
								break;
							case "3":
								$fchargecond = "where auth='MD'";
								break;
							case "4":
								$fchargecond = "where auth='SD'";
								break;
							default:
								$fchargecond = "where auth='HD'";
								break;
						}
						$fchargesel = select_from_table($con, $fchargesql, $fchargecond, $fname);
						if ($fchargesel == 0)
						{
							pg_close($con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}
						$auth_name = pg_fetch_result($fchargesel, 0, "auth_name");
						$status1 = "charge={$auth_name}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "39":
						// ボタン設定の編集
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] == $mode[1]) {
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "40":
						// 運用フローの編集
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] == $mode[1]) {
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "41":
						// 匿名設定の編集
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] == $mode[1]) {
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "42":
						// 送信先設定の編集
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] == $mode[1]) {
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "43":
						// 進捗設定初期登録の編集
						$mode = explode("=", $row['identifier']);
						if ($get_data[$mode[0]] == $mode[1]) {
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "44":
						// 様式管理の編集
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] == $mode[1]) {
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "45":
						// 様式割当の編集
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] == $mode[1]) {
							$identirier = $row["status1"];
							$feissql = "select job_nm from jobmst";
							$feiscond = "where job_id = {$post_data[$identirier]}";
							$feissel = select_from_table($con, $feissql, $feiscond, $fname);
							if ($feissel == 0)
							{
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$job_nm = pg_fetch_result($feissel, 0, "job_nm");
							$status1 = "job={$job_nm}";
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "46":
						// 分析画面設定の編集
						$mode = explode("=", $row['identifier']);
						if ($get_data[$mode[0]] == $mode[1]) {
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "47":
						// プロフィール設定の編集
						$mode = explode("=", $row['identifier']);
						if ($get_data[$mode[0]] == $mode[1]) {
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "48":
						// プロフィール個人設定の編集
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] == $mode[1]) {
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "49":
						// プロフィールボタン表示設定の編集
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] == $mode[1]) {
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "50":
						// 部署表示の編集
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] == $mode[1]) {
							$identirier = explode(":", $row["status1"]);
							$fclssql = "select class_nm, atrb_nm, dept_nm, room_nm, class_cnt from classname";
							$fclscond = "";
							$fclssel = select_from_table($con, $fclssql, $fclscond, $fname);
							if ($fclssel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							if ($post_data[$identirier[0]] == 't') {
								$statusArr[] = pg_fetch_result($fclssel, 0, "class_nm");
							}
							if ($post_data[$identirier[1]] == 't') {
								$statusArr[] = pg_fetch_result($fclssel, 0, "atrb_nm");
							}
							if ($post_data[$identirier[2]] == 't') {
								$statusArr[] = pg_fetch_result($fclssel, 0, "dept_nm");
							}
							if ($post_data[$identirier[3]] == 't') {
								$statusArr[] = pg_fetch_result($fclssel, 0, "room_nm");
							}
							$status1 = implode("、", $statusArr);
							$status1 .= " を表示";
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "51":
						// e-ラーニングオプションの編集
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] == $mode[1]) {
							$identirier = explode(":", $row["status1"]);
							$status1 =  $post_data[$identirier[0]] == 't' ? "フォルダ内コンテンツ数：表示する<br />" : "フォルダ内コンテンツ数：表示しない<br />";
							$status1 .= $post_data[$identirier[1]] == 't' ? "コンテンツのドラッグ＆ドロップ：有効にする<br />" : "コンテンツのドラッグ＆ドロップ：無効にする<br />";
							$status1 .= $post_data[$identirier[2]] == 't' ? "ダウンロード時のコンテンツ名：登録コンテンツ名<br />" : "ダウンロード時のコンテンツ名：システム名<br />";
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "52":
						// e-ラーニング感想の編集
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] == $mode[1]) {
							$identirier = $row['status1'];
							$felsql = "select lib_nm from el_info";
							$felcond = "where lib_id = (select lib_id from el_edition where base_lib_id = {$post_data[$identirier]})";
							$felsel = select_from_table($con, $felsql, $felcond, $fname);
							if ($felsel == 0) {
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							$lib_nm = pg_fetch_result($felsel, 0, "lib_nm");
							$status1 = "document={$lib_nm}";
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "53":
						// 報告書の印刷
						$identirier = $row['status1'];
						$frpsql = "select report_no from inci_report";
						$frpcond = "where report_id = {$get_data[$identirier]}";
						$frpsel = select_from_table($con, $frpsql, $frpcond, $fname);
						if ($frpsel == 0)
						{
							pg_close($con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}
						$report_no = pg_fetch_result($frpsel, 0, "report_no");
						$status1 = "report_no={$report_no}";
						$detail = $row["detail_id"];
						$cate = $row["cate_id"];
						$priority = $row["priority"];
						break;
					case "54":
						// メールの印刷
						$identirier = $row['status1'];
						$fsendsql = "select subject from inci_mail_send_info";
						$fsendcond = "where mail_id = {$get_data[$identirier]}";
						$fsendsel = select_from_table($con, $fsendsql, $fsendcond, $fname);
						if ($fsendsel == 0)
						{
							pg_close($con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}
						$subject = pg_fetch_result($fsendsel, 0, "subject");
						$status1 = "subject={$subject}";
						$detail = $row["detail_id"];
						$cate = $row["cate_id"];
						$priority = $row["priority"];
						break;
					case "55":
						// 報告書単位での未読・既読の編集
						$mode = explode("=", $row['identifier']);
						if ($post_data[$mode[0]] == $mode[1]) {
							$identirier = $row["status1"];
							$freadsql = "select subject from inci_mail_send_info";
							$freadcond = "where mail_id in(";
							$freadcond .= implode(",", $post_data[$identirier]);
							$freadcond .= ")";
							$freadsel = select_from_table($con, $freadsql, $freadcond, $fname);
							if ($freadsel == 0)
							{
								pg_close($con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
							while ($freadrow = pg_fetch_array($freadsel)) {
								$subject = $freadrow["subject"];
								$status1 .= "subject={$subject}<br />";
							}
							$cate = $row["cate_id"];
							$detail = $row["detail_id"];
							$priority = $row["priority"];
						}
						break;
					case "56":
						// 報告書のCSV書き出し
						$status1 = "CSV={$get_data}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "57":
						// 報告書のExcel書き出し
						$status1 = "Excel={$get_data}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "58":
						// 報告書の副報告の編集
						break;
					case "59":
						// 統計分析画面でのExcel書き出し（クロス集計）
						$status1 = "Excel={$get_data}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "60":
						// 統計分析画面での印刷
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "61":
						// 代行者設定の編集
						$identirier = $row['status1'];
						$agent = explode(",", $post_data[$identirier]);
						foreach ($agent as $tmp_agent) {
							$status1 .= "agent={$tmp_agent}<br />";
						}
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					case "62":
						// 統計分析画面でのExcel書き出し（定型集計）
						$status1 = "Excel={$get_data}";
						$cate = $row["cate_id"];
						$detail = $row["detail_id"];
						$priority = $row["priority"];
						break;
					default:
						$detail = $row["detail_id"];
						$cate = $row["cate_id"];
						$priority = $row["priority"];
						break;
				}
			}
			else {
				//////////////////////////
				//ファイル名のみで一致可能	//
				//////////////////////////
				$cate = $row["cate_id"];
				$detail = $row["detail_id"];
				$priority = $row["priority"];
				break;
			}
		//}
	}

	if($priority == '1' || $priority == '2' || $priority == '3')
	{
		//ログ取得対象と認識する
		if ($emp_id == '') {
			$emp_id = get_emp_id($con, $session, $fname);

			foreach ($post_data as $key => $val) {
				if(is_array($val)){
					foreach ($val as $xkey => $xval) {
						$post_get .= "{$key}[{$xkey}]={$xval}:";
					}
				}
				else {
					$post_get .= "{$key}={$val}:";
				}
			}
		}
		$date = date("YmdHis");
		$ip_address = $_SERVER["REMOTE_ADDR"];

		// トランザクションの開始
		pg_query($con, "begin");

		$data_work2 = explode("/",$filename);
		$tmp_filename = $data_work2[count($data_work2) - 1];
		//$sql = "insert into aclg_data(emp_id,ac_date,filename,ip_address,http_referer,user_agent,data,cate_id,detail_id,status1) values(";
		$sql = "insert into aclg_data(emp_id,ac_date,filename,ip_address,user_agent,data,cate_id,detail_id,status1) values(";
		//$content = array($emp_id,$date,$tmp_filename,$ip_address,$tmp_http_referer[1],$user_agent,pg_escape_string($post_get),$cate,$detail,pg_escape_string($status1));
		$content = array($emp_id,$date,$tmp_filename,$ip_address,$user_agent,pg_escape_string($post_get),$cate,$detail,pg_escape_string($status1));
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		pg_query($con, "commit");
	}
}
?>
