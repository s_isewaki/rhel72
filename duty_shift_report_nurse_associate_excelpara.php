<!--Excel印刷パラメータ指定画面-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix Excel印刷オプション</TITLE>
<link rel="stylesheet" type="text/css" href="comedix/css/main.css">

<?
require_once("about_comedix.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
?>
<script type="text/javascript">
	yesno = location.search;
	yesno = yesno.substring(1,3);
	function AddVal(_value){
		_value ++;
		return (_value);
	}
	function SubVal(_value){
		_value --;
		if( _value <= 0 ){_value = 0;}
		return (_value);
	}
	function OKbutton(){
		kango_space_line=document.excelparam.kango.value;
		hojo_space_line=document.excelparam.hojo.value;
		parent.opener.makeAssociate(kango_space_line , hojo_space_line);
		window.close();
	}
</script>
</head>

<BODY>
<CENTER>
<FORM NAME="excelparam" METHOD="POST">

<TABLE width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
<TR BGCOLOR="#5279a5" height="32">
 <TD class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>届出様式　予備行挿入の指定</b></font></TD>
 <TD width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></TD>
<TR>
</TABLE>

<TABLE BORDER="0" cellspacing="0" cellpadding="0" >
<TR>
 <TD COLSPAN="3"><BR><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" >予備行数を指定してください</FONT><BR></TD>
</TR>
<TR>
 <TD><BR><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" >看護職員表</FONT><BR></TD>
 <TD>&nbsp&nbsp&nbsp</TD>
 <TD><BR>
    <input type=button value="+" onclick="kango.value=AddVal(kango.value)">
    <input type=button value="-" onclick="kango.value=SubVal(kango.value)">
  <input type=text size=3 name="kango" value=0>人分
</TD>
</TR>
<TR>
 <TD><BR><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" >看護補助者表</FONT><BR></TD>
 <TD>&nbsp&nbsp&nbsp</TD>
 <TD><BR>
    <input type=button value="+" onclick="hojo.value=AddVal(hojo.value)">
    <input type=button value="-" onclick="hojo.value=SubVal(hojo.value)">
  <input type=text size=3 name="hojo" value=0>人分
</TD>
</TR>
</TABLE>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="100%" align="center">
<BR>
<INPUT TYPE="submit" VALUE="　　ＯＫ　　" onclick="OKbutton();">
<INPUT TYPE="button" VALUE="キャンセル" onclick="window.close();">
</td>
</tr>
</FORM>
</table>
<table>
<tr>
<td>
<BR>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" >
<script type="text/javascript">
if( yesno =='no' ){
 var m = '看護職員、看護補助者が存在しません。<BR><BR>空白行数を指定することで指定された<BR>行数の空白表を作成します。';
 document.write(m);
}
</script>
</font>
</td>
</tr>
</table>

</CENTER>

</BODY>
</HTML>
