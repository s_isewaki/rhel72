<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 掲示板管理 | 投稿一覧</title>
<?php
//ini_set("display_errors","1");
require_once("Cmx.php");
require("about_session.php");
require("about_authority.php");
require("get_values.ini");
require("show_bbs_admin_thread.ini");
require_once("aclg_set.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$check_auth = check_authority($session, 62, $fname);
if ($check_auth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 表示順のデフォルトを「ツリー表示（新しい順）」とする
if ($sort == "") {$sort = "1";}

// データベースに接続
$con = connect2db($fname);
// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

// NEW表示期間を取得
$sql = "select newpost_days from bbsoption";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$newpost_days = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "newpost_days") : "";
if ($newpost_days == "") {
    $newpost_days = 3;
}

// 順番変更処理
if ($action == "up" || $action == "down") {
    pg_query($con, "begin");

    // カテゴリが選択された場合
    if ($theme_id == "") {

        // カテゴリの表示順を取得
        $sql = "select disp_order from bbscategory";
        $cond = "where bbscategory_id = $category_id";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $disp_order = pg_fetch_result($sel, 0, "disp_order");

        // 隣のカテゴリを上げ下げ
        $diff = ($action == "up") ? 1 : -1;
        $sql = "update bbscategory set";
        $set = array("disp_order");
        $setvalue = array($disp_order);
        $cond = "where disp_order = " . ($disp_order + $diff);
        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        // 選択カテゴリを上げ下げ
        $sql = "update bbscategory set";
        $set = array("disp_order");
        $setvalue = array($disp_order + $diff);
        $cond = "where bbscategory_id = $category_id";
        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

    // テーマが指定された場合
    } else {

        // テーマの表示順を取得
        $sql = "select disp_order from bbsthread";
        $cond = "where bbsthread_id = $theme_id";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $disp_order = pg_fetch_result($sel, 0, "disp_order");

        // 隣のテーマを上げ下げ
        $diff = ($action == "up") ? 1 : -1;
        $sql = "update bbsthread set";
        $set = array("disp_order");
        $setvalue = array($disp_order);
        $cond = "where bbscategory_id = {$category_id} and disp_order = " . ($disp_order + $diff);
        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        // 選択テーマを上げ下げ
        $sql = "update bbsthread set";
        $set = array("disp_order");
        $setvalue = array($disp_order + $diff);
        $cond = "where bbsthread_id = $theme_id";
        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }

    pg_query($con, "commit");
}

// カテゴリ一覧を取得
$sql = "select bbscategory.bbscategory_id, bbscategory.bbscategory_title, case when bbsthread.date is null then bbscategory.date else bbsthread.date end as date, coalesce(bbs_count.count, 0) as count, bbscategory.disp_order from bbscategory left join (select bbsthread.bbscategory_id, max(case when bbs_date.date is null then bbsthread.date else bbs_date.date end) as date from bbsthread left join (select bbs.bbsthread_id, max(bbs.date) as date from bbs group by bbs.bbsthread_id) bbs_date on bbsthread.bbsthread_id = bbs_date.bbsthread_id where bbsthread.bbsthread_del_flg = 'f' group by bbsthread.bbscategory_id) bbsthread on bbscategory.bbscategory_id = bbsthread.bbscategory_id left join (select bbsthread.bbscategory_id, sum(bbs_count.count) as count from (select bbs.bbsthread_id, count(bbs.bbs_id) as count from bbs where bbs.bbs_del_flg = 'f' group by bbs.bbsthread_id) bbs_count inner join (select bbsthread.bbscategory_id, bbsthread.bbsthread_id from bbsthread where bbsthread.bbsthread_del_flg = 'f') bbsthread on bbsthread.bbsthread_id = bbs_count.bbsthread_id group by bbsthread.bbscategory_id) bbs_count on bbs_count.bbscategory_id = bbscategory.bbscategory_id";
$cond = "where bbscategory.bbscategory_del_flg = 'f' order by bbscategory.disp_order desc";
$sel_category = select_from_table($con, $sql, $cond, $fname);
if ($sel_category == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// テーマ一覧を取得
$sql = "select bbsthread.bbscategory_id, bbsthread.bbsthread_id, bbsthread.bbsthread_title, case when bbs_date.date is null then bbsthread.date else bbs_date.date end as date, coalesce(bbs_count.count, 0) as count, bbsthread.disp_order, empmst.emp_lt_nm, empmst.emp_ft_nm, bbsthread.bbsthread from bbsthread inner join empmst on empmst.emp_id = bbsthread.emp_id left join (select bbs.bbsthread_id, max(bbs.date) as date from bbs group by bbs.bbsthread_id) bbs_date on bbsthread.bbsthread_id = bbs_date.bbsthread_id left join (select bbs.bbsthread_id, count(bbs.bbs_id) as count from bbs where bbs.bbs_del_flg = 'f' group by bbs.bbsthread_id) bbs_count on bbsthread.bbsthread_id = bbs_count.bbsthread_id";
$cond = "where bbsthread.bbsthread_del_flg = 'f' order by bbsthread.disp_order desc";
$sel_theme = select_from_table($con, $sql, $cond, $fname);
if ($sel_theme == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// カテゴリ・テーマを配列化、初期表示カテゴリ・テーマの決定
$categories = array();
while ($row = pg_fetch_array($sel_category)) {
    $tmp_category = array(
        "title" => $row["bbscategory_title"],
        "date" => substr($row["date"], 0, 8),
        "count" => $row["count"],
        "disp_order" => $row["disp_order"],
        "themes" => array()
    );
    $categories[$row["bbscategory_id"]] = $tmp_category;
    if ($category_id == "" && $theme_id == "") {
        $category_id = $row["bbscategory_id"];
    }
}
while ($row = pg_fetch_array($sel_theme)) {
    $tmp_theme = array(
        "title" => $row["bbsthread_title"],
        "date" => substr($row["date"], 0, 8),
        "time" => substr($row["date"], 8, 4),
        "count" => $row["count"],
        "disp_order" => $row["disp_order"],
        "emp_nm" => $row["emp_lt_nm"] . " " . $row["emp_ft_nm"],
        "content" => $row["bbsthread"]
    );
    $categories[$row["bbscategory_id"]]["themes"][$row["bbsthread_id"]] = $tmp_theme;
    if ($category_id == "" && $row["bbsthread_id"] == $theme_id) {
        $category_id = $row["bbscategory_id"];
    }
}

// for test
/*
$categories = array();
$category_id = "";
$theme_id = "";
*/

// 並び順の確認（重複・歯抜けを考慮）
$disp_order_reassigned = false;
$category_disp_order = count($categories);
foreach ($categories as $tmp_category_id => $tmp_category) {
    if ($categories[$tmp_category_id]["disp_order"] != $category_disp_order) {
        $categories[$tmp_category_id]["disp_order"] = $category_disp_order;
        $disp_order_reassigned = true;
    }
    $category_disp_order--;

    $tmp_themes = $tmp_category["themes"];
    $theme_disp_order = count($tmp_themes);
    foreach ($tmp_themes as $tmp_theme_id => $tmp_theme) {
        if ($categories[$tmp_category_id]["themes"][$tmp_theme_id]["disp_order"] != $theme_disp_order) {
            $categories[$tmp_category_id]["themes"][$tmp_theme_id]["disp_order"] = $theme_disp_order;
            $disp_order_reassigned = true;
        }
        $theme_disp_order--;
    }
}

// 並び順が再割り当てされた場合はDBに反映
if ($disp_order_reassigned) {
    pg_query($con, "begin");
    foreach ($categories as $tmp_category_id => $tmp_category) {
        $sql = "update bbscategory set";
        $set = array("disp_order");
        $setvalue = array($tmp_category["disp_order"]);
        $cond = "where bbscategory_id = $tmp_category_id";
        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        $tmp_themes = $tmp_category["themes"];
        foreach ($tmp_themes as $tmp_theme_id => $tmp_theme) {
            $sql = "update bbsthread set";
            $set = array("disp_order");
            $setvalue = array($tmp_theme["disp_order"]);
            $cond = "where bbsthread_id = $tmp_theme_id";
            $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
            if ($upd == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
        }
    }
    pg_query($con, "commit");
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/yui/build/yahoo/yahoo-min.js"></script>
<script type="text/javascript" src="js/yui/build/treeview/treeview-min.js"></script>
<link rel="stylesheet" type="text/css" href="js/yui/build/treeview/assets/skins/sam/treeview.css">
<script type="text/javascript">
function initPage() {
    var tree = new YAHOO.widget.TreeView('sam');
    var root = tree.getRoot();
<?php
    $comp_date = date("Ymd", strtotime("-" . ($newpost_days - 1) . " day", mktime(0, 0, 0, date("m"), date("d"), date("Y"))));

    reset($categories);
    $category_count = count($categories);
    $category_index = 1;
    while ($tmp_data = each($categories)) {
        $tmp_category_id = $tmp_data["key"];
        $tmp_category = $tmp_data["value"];
        $tmp_category_title = $tmp_category["title"];
        $tmp_category_date = $tmp_category["date"];
        $tmp_category_count = $tmp_category["count"];
        $tmp_themes = $tmp_category["themes"];
        if ($tmp_category_id == $category_id && count($tmp_themes) > 0) {
            $tmp_category_expanded = "true";
        } else {
            $tmp_category_expanded = "false";
        }

        $tmp_category_html = "<input type=\"checkbox\" name=\"category_ids[]\" value=\"$tmp_category_id\" style=\"vertical-align:middle;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
        if ($category_index == 1) {
            $tmp_category_html .= "<img src=\"img/up-g.gif\" alt=\"\" width=\"17\" height=\"17\" style=\"vertical-align:middle;\">";
        } else {
            $tmp_category_html .= "<a href=\"bbs_admin_menu.php?session=$session&category_id=$tmp_category_id&sort=$sort&show_content=$show_content&action=up\"><img src=\"img/up.gif\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\"></a>";
        }
        if ($category_index == $category_count) {
            $tmp_category_html .= "<img src=\"img/down-g.gif\" alt=\"\" width=\"17\" height=\"17\" style=\"vertical-align:middle;\">";
        } else {
            $tmp_category_html .= "<a href=\"bbs_admin_menu.php?session=$session&category_id=$tmp_category_id&sort=$sort&show_content=$show_content&action=down\"><img src=\"img/down.gif\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\"></a>";
        }
        if ($tmp_category_id == $category_id && $theme_id == "") {
            $tmp_category_html .= "<b>$tmp_category_title</b>";
        } else {
            $tmp_category_html .= "<a href=\"bbs_admin_menu.php?session=$session&category_id=$tmp_category_id&sort=$sort&show_content=$show_content\">$tmp_category_title</a>";
        }
        $tmp_category_html .= "</font>";
        if ($tmp_category_date >= $comp_date && $tmp_category_count > 0) {
            $tmp_category_html .= "<img src=\"img/icon/new.gif\" alt=\"NEW\" width=\"24\" height=\"10\" style=\"margin-left:4px;\">";
        }
?>
    var category<?php echo($tmp_category_id); ?> = new YAHOO.widget.HTMLNode('<?php echo($tmp_category_html); ?>', root, <?php echo($tmp_category_expanded); ?>, true);
<?php
        $theme_count = count($tmp_themes);
        $theme_index = 1;
        while ($tmp_data = each($tmp_themes)) {
            $tmp_theme_id = $tmp_data["key"];
            $tmp_theme = $tmp_data["value"];
            $tmp_theme_title = $tmp_theme["title"];
            $tmp_theme_date = $tmp_theme["date"];
            $tmp_theme_count = $tmp_theme["count"];

            $tmp_theme_html = "<input type=\"checkbox\" name=\"theme_ids[]\" value=\"$tmp_theme_id\" style=\"vertical-align:middle;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
            if ($theme_index == 1) {
                $tmp_theme_html .= "<img src=\"img/up-g.gif\" alt=\"\" width=\"17\" height=\"17\" style=\"vertical-align:middle;\">";
            } else {
                $tmp_theme_html .= "<a href=\"bbs_admin_menu.php?session=$session&category_id=$tmp_category_id&theme_id=$tmp_theme_id&sort=$sort&show_content=$show_content&action=up\"><img src=\"img/up.gif\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\"></a>";
            }
            if ($theme_index == $theme_count) {
                $tmp_theme_html .= "<img src=\"img/down-g.gif\" alt=\"\" width=\"17\" height=\"17\" style=\"vertical-align:middle;\">";
            } else {
                $tmp_theme_html .= "<a href=\"bbs_admin_menu.php?session=$session&category_id=$tmp_category_id&theme_id=$tmp_theme_id&sort=$sort&show_content=$show_content&action=down\"><img src=\"img/down.gif\" alt=\"\" width=\"17\" border=\"0\" height=\"17\" style=\"vertical-align:middle;\"></a>";
            }
            if ($tmp_theme_id == $theme_id) {
                $tmp_theme_html .= "<b>$tmp_theme_title</b>";
            } else {
                $tmp_theme_html .= "<a href=\"bbs_admin_menu.php?session=$session&category_id=$tmp_category_id&theme_id=$tmp_theme_id&sort=$sort&show_content=$show_content\">$tmp_theme_title</a>";
            }
            $tmp_theme_html .= "</font>";
            if ($tmp_theme_date >= $comp_date && $tmp_theme_count > 0) {
                $tmp_theme_html .= "<img src=\"img/icon/new.gif\" alt=\"NEW\" width=\"24\" height=\"10\" style=\"margin-left:4px;\" style=\"vertical-align:middle;\">";
            }
?>
    var theme<?php echo($tmp_theme_id); ?> = new YAHOO.widget.HTMLNode('<?php echo($tmp_theme_html); ?>', category<?php echo($tmp_category_id); ?>, false, true);
<?php
            $theme_index++;
        }
        $category_index++;
    }
?>
    tree.draw();
}

function registCategory() {
    location.href = 'bbscategory_register.php?session=<?php echo($session); ?>&sort=<?php echo($sort); ?>&show_content=<?php echo($show_content); ?>&path=2'
}

function registTheme() {
<?php if ($category_id == "") { ?>
    alert('カテゴリが選択されていません。');
<?php } else { ?>
    location.href = 'bbsthread_register.php?session=<?php echo($session); ?>&category_id=<?php echo($category_id); ?>&sort=<?php echo($sort); ?>&show_content=<?php echo($show_content); ?>&path=2';
<?php } ?>
}

function updateCategory() {
    location.href = 'bbscategory_update.php?session=<?php echo($session); ?>&category_id=<?php echo($category_id); ?>&sort=<?php echo($sort); ?>&show_content=<?php echo($show_content); ?>&path=2';
}

function updateTheme() {
    location.href = 'bbsthread_update.php?session=<?php echo($session); ?>&category_id=<?php echo($category_id); ?>&theme_id=<?php echo($theme_id); ?>&sort=<?php echo($sort); ?>&show_content=<?php echo($show_content); ?>&path=2';
}

function deleteCategoryTheme() {
    var category_ids = getCheckedIds(document.delform.elements['category_ids[]']);
    var theme_ids = getCheckedIds(document.delform.elements['theme_ids[]']);
    if (category_ids.length == 0 && theme_ids.length == 0) {
        alert('カテゴリ・テーマが選択されていません。');
        return;
    }

    if (confirm('選択されたカテゴリ・テーマを削除します。よろしいですか？')) {
        document.delform.submit();
    }
}

function changeSortType(sort) {
    location.href = 'bbs_admin_menu.php?session=<?php echo($session); ?>&category_id=<?php echo($category_id); ?>&theme_id=<?php echo($theme_id); ?>&sort=' + sort + '&show_content=<?php echo($show_content); ?>';
}

function changeContentView(checked) {
    var show_content = (checked) ? 't' : '';
    location.href = 'bbs_admin_menu.php?session=<?php echo($session); ?>&category_id=<?php echo($category_id); ?>&theme_id=<?php echo($theme_id); ?>&sort=<?php echo($sort); ?>&show_content=' + show_content;
}

function postComment() {
<?php if ($theme_id == "") { ?>
    alert('テーマが選択されていません。');
<?php } else { ?>
    location.href = 'bbs_post_register.php?session=<?php echo($session); ?>&theme_id=<?php echo($theme_id); ?>&sort=<?php echo($sort); ?>&show_content=<?php echo($show_content); ?>&path=2';
<?php } ?>
}

function getCheckedIds(boxes) {
    var checked_ids = new Array();
    if (boxes) {
        if (!boxes.length) {
            if (boxes.checked) {
                checked_ids.push(boxes.value);
            }
        } else {
            for (var i = 0, j = boxes.length; i < j; i++) {
                if (boxes[i].checked) {
                    checked_ids.push(boxes[i].value);
                }
            }
        }
    }
    return checked_ids;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bbsthread_menu.php?session=<?php echo($session); ?>"><img src="img/icon/b10.gif" width="32" height="32" border="0" alt="掲示板・電子会議室"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bbsthread_menu.php?session=<?php echo($session); ?>"><b>掲示板・電子会議室</b></a> &gt; <a href="bbs_admin_menu.php?session=<?php echo($session); ?>&sort=<?php echo($sort); ?>&show_content=<?php echo($show_content); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="bbsthread_menu.php?session=<?php echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="110" align="center" bgcolor="#5279a5"><a href="bbs_admin_menu.php?session=<?php echo($session); ?>&sort=<?php echo($sort); ?>&show_content=<?php echo($show_content); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>投稿一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="130" align="center" bgcolor="#bdd1e7"><a href="bbs_admin_option.php?session=<?php echo($session); ?>&sort=<?php echo($sort); ?>&show_content=<?php echo($show_content); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション設定</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="1" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td align="right">
<input type="button" value="カテゴリ作成" onclick="registCategory();">
<input type="button" value="テーマ作成" onclick="registTheme();">
</td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="2" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="30%" valign="top">
<!-- theme list start -->
<form name="delform" action="bbsthread_delete.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ＞テーマ</font></td>
<td align="right"><input type="button" value="削除" onclick="deleteCategoryTheme();"></td>
</tr>
</table>
</tr>
</td>
<tr height="400">
<td valign="top">
<?php if (count($categories) == 0)  { ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリが登録されていません。</font>
<?php } ?>
<div id="sam"></div>
</td>
</tr>
</table>
<input type="hidden" name="session" value="<?php echo($session); ?>">
<input type="hidden" name="sort" value="<?php echo($sort); ?>">
<input type="hidden" name="show_content" value="<?php echo($show_content); ?>">
<input type="hidden" name="path" value="2">
</form>
<!-- theme list end -->
</td>
<td><img src="img/spacer.gif" alt="" width="2" height="1"></td>
<td width="70%" valign="top">
<!-- theme detail start -->
<?php show_bbsthread_detail($con, $category_id, $theme_id, $fname); ?>
<!-- theme detail end -->
<!-- comment view start -->
<?php if ($category_id != "" && $theme_id == "") { ?>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td width="20%" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">テーマ一覧<input type="checkbox" name="show_content" value="t"<?php if ($show_content == "t") {echo(" checked");} ?> style="margin-left:10px;" onclick="changeContentView(this.checked);">内容を表示する</font></td>
</tr>
</table>
</td>
</tr>
<tr height="328">
<td valign="top" style="padding:0 2px;">
<?php show_theme_list($con, $category_id, $categories[$category_id]["themes"], $sort, $newpost_days, $show_content, $session, $fname); ?>
</td>
</tr>
</table>
<?php } ?>
<?php if ($theme_id != "") { ?>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td width="20%" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">投稿一覧</font></td>
<td>
<select name="sort" onchange="changeSortType(this.value);"><option value="1"<?php if ($sort == "1") {echo(" selected");} ?>>ツリー表示（新しい順）
<option value="2"<?php if ($sort == "2") {echo(" selected");} ?>>ツリー表示（古い順）
<option value="3"<?php if ($sort == "3") {echo(" selected");} ?>>投稿順表示（新しい順）
<option value="4"<?php if ($sort == "4") {echo(" selected");} ?>>投稿順表示（古い順）
</select>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="show_content" value="t"<?php if ($show_content == "t") {echo(" checked");} ?> onclick="changeContentView(this.checked);">内容を表示する</font></td>
<td align="right"><input type="button" value="新しい投稿" onclick="postComment();"></td>
</tr>
</table>
</td>
</tr>
<tr height="328">
<td valign="top" style="padding:0 2px;">
<?php show_post_list($con, $theme_id, $sort, $newpost_days, $show_content, $emp_id, $session, $fname); ?>
</td>
</tr>
</table>
<?php } ?>
<!-- comment view end -->
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>
<?php pg_close($con);