<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成 | 勤務実績入力</title>

<?
$arr_pos_value = array();
$arr_time_value = array();

$arr_pos_value[] = "開始";
$arr_time_value[] = time();
require_once("about_comedix.php");
require_once("duty_shift_common.ini");
require_once("duty_shift_user_tab_common.ini");
require_once("duty_shift_common_class.php");
require_once("duty_shift_menu_common_class.php");
//繰越公休残日数 start
require_once("duty_shift_hol_remain_common_class.php");
//繰越公休残日数 end
$arr_pos_value[] = "require_once";
$arr_time_value[] = time();

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$arr_pos_value[] = "qualify_session";
$arr_time_value[] = time();
///-----------------------------------------------------------------------------
//DBコネクション
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$arr_pos_value[] = "connect2db";
$arr_time_value[] = time();
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
$arr_pos_value[] = "new duty_shift_common_class";
$arr_time_value[] = time();
$obj_menu = new duty_shift_menu_common_class($con, $fname);
$arr_pos_value[] = "new duty_shift_menu_common_class";
$arr_time_value[] = time();
///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//ユーザ画面用
$chk_flg = $obj->check_authority_user($session, $fname);
if ($chk_flg == "") {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$arr_pos_value[] = "check_authority_user";
$arr_time_value[] = time();
//管理画面用
$section_admin_auth = $obj->check_authority_Management($session, $fname);
$arr_pos_value[] = "check_authority_Management";
$arr_time_value[] = time();
///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
$first_flg = ($duty_mm == "");//初期表示時、月が未指定の場合 20140529
///-----------------------------------------------------------------------------
//現在日付の取得
///-----------------------------------------------------------------------------
$date = getdate();
$now_yyyy = $date["year"];
$now_mm = $date["mon"];
///-----------------------------------------------------------------------------
//表示年／月設定
///-----------------------------------------------------------------------------
if ($duty_yyyy == "") { $duty_yyyy = $now_yyyy;}
if ($duty_mm == "") { $duty_mm = $now_mm; }


///-----------------------------------------------------------------------------
//日数
///-----------------------------------------------------------------------------
$day_cnt = $obj->days_in_month($duty_yyyy, $duty_mm);
$arr_pos_value[] = "days_in_month";
$arr_time_value[] = time();
$start_day = 1;
$end_day = $day_cnt;

//表示期間外の分を表示する 20130307
$extra_day =0;
$reappear_flg = false;
if ($week_index >= 0 && $week_index != null){
	$data_cnt = "";
	$reappear_flg = true;
	$extra_day =  $week_index * 7;
}

///-----------------------------------------------------------------------------
//各変数値の初期値設定
///-----------------------------------------------------------------------------
$plan_results_flg = "3";	//１：予定表示、２：予実績表示、３：予実績表示(実績用)
$plan_hope_flg = "";		//勤務希望表示（ダミー）
$plan_duty_flg = "";		//当直表示（ダミー）
$plan_comment_flg = "";		//コメント表示（ダミー）

$staff_del_flg = "";		//スタッフ削除フラグ（１：削除）
$highlight_flg = "1";		//強調表示フラグ（１：強調表示）
$rslt_flg = "1";			//実績入力フラグ（１：実績入力）
$check_flg = "";			//警告行表示フラグ（１：表示）
$individual_flg = "";		//希望ＤＢフラグ（１：希望）
$hope_get_flg = "";			//「希望取り込み」フラグ（１：取り込み）
$person_charge_id = "";		//シフト管理者（責任者）
$caretaker_id = "";			//シフト管理者（代行者）
$caretaker2_id = "";		//シフト管理者（代行者）
$caretaker3_id = "";		//シフト管理者（代行者）
$caretaker4_id = "";		//シフト管理者（代行者）
$caretaker5_id = "";
$caretaker6_id = "";
$caretaker7_id = "";
$caretaker8_id = "";
$caretaker9_id = "";
$caretaker10_id = "";
if ($data_cnt == "") { $data_cnt = 0; }

//入力形式で表示する日（開始／終了）
if ($edit_start_day == "") { $edit_start_day = 0; }
if ($edit_end_day == "") { $edit_end_day = 0; }

$err_msg_1 = "";
$err_msg_2 = "";

$show_data_flg = "1";		//２行目表示データフラグ（１：勤務実績、２：勤務希望、３：当直）
///-----------------------------------------------------------------------------
// ログインユーザの職員ID・氏名を取得
///-----------------------------------------------------------------------------
$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
$emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
$arr_pos_value[] = "select emp_id";
$arr_time_value[] = time();
///-----------------------------------------------------------------------------
//ＤＢ(stmst)より役職情報取得
//ＤＢ(jobmst)より職種情報取得
//ＤＢ(empmst)より職員情報を取得
//ＤＢ(wktmgrp)より勤務パターン情報を取得
///-----------------------------------------------------------------------------
$data_st = $obj->get_stmst_array();
$arr_pos_value[] = "get_stmst_array 役職";
$arr_time_value[] = time();
$data_job = $obj->get_jobmst_array();
$arr_pos_value[] = "get_jobmst_array 職種";
$arr_time_value[] = time();
// 有効なグループの職員を取得
$data_wktmgrp = $obj->get_wktmgrp_array();
$arr_pos_value[] = "get_wktmgrp_array 勤務パターングループ";
$arr_time_value[] = time();
$count_row = 0;
///-----------------------------------------------------------------------------
// 勤務シフトグループ情報を取得
///-----------------------------------------------------------------------------
$group_array = $obj->get_duty_shift_group_array("", "", $data_wktmgrp);
$arr_pos_value[] = "get_duty_shift_group_array シフトグループ";
$arr_time_value[] = time();
if (count($group_array) <= 0) {
	$err_msg_1 = "勤務シフトグループ情報が未設定です。管理画面で登録してください。";
} else {
    // 更新権限あるグループ取得 20140210 //職員設定、応援追加されていても含めない
    $group_array = $obj->get_valid_group_array($group_array, $emp_id, $duty_yyyy, $duty_mm);
$arr_pos_value[] = "get_valid_group_array 更新権限あるグループ取得";
$arr_time_value[] = time();
    // 1件もない場合
    if (count($group_array) <= 0) {
        $conf = new Cmx_SystemConfig();
        $pattern = $conf->get('duty_shift.use_pattern');
        if ($pattern == "B") {
            ?>
            <script type='text/javascript'>
            location.href = "duty_shift_diary_bld_manage.php?session=<?= $session ?>";</script>
            <?php
        }
        $err_msg_1 = "シフト管理者として登録されたシフトグループがありません。管理画面＞シフトグループ一覧＞シフトグループ登録画面で登録してください。";
    }
    else {
        if ($group_id == ""){
            $group_id = $group_array[0]["group_id"];
        } else {
            //職員設定から移動してきた場合の対応 20100827
            $group_id_flg = false;
            
            //権限、職員設定されているか確認
            for ($i=0; $i<count($group_array); $i++) {
                if ($group_id == $group_array[$i]["group_id"]) {
                    $group_id_flg = true;
                    break;
                }
            }
            //権限がないグループの場合、先頭のグループにする
            if (!$group_id_flg) {
                $group_id = $group_array[0]["group_id"];
            }
        }
    }
}
//対象グループのシフト管理者を設定
for ($i=0;$i<count($group_array);$i++) {
	if ($group_id == $group_array[$i]["group_id"]) {
		$person_charge_id = $group_array[$i]["person_charge_id"];	//シフト管理者ID（責任者）
		$caretaker_id = $group_array[$i]["caretaker_id"];			//シフト管理者ID（代行者）
		$caretaker2_id = $group_array[$i]["caretaker2_id"];
		$caretaker3_id = $group_array[$i]["caretaker3_id"];
		$caretaker4_id = $group_array[$i]["caretaker4_id"];
		$caretaker5_id = $group_array[$i]["caretaker5_id"];
		$caretaker6_id = $group_array[$i]["caretaker6_id"];
		$caretaker7_id = $group_array[$i]["caretaker7_id"];
		$caretaker8_id = $group_array[$i]["caretaker8_id"];
		$caretaker9_id = $group_array[$i]["caretaker9_id"];
		$caretaker10_id = $group_array[$i]["caretaker10_id"];
		$start_month_flg1 = $group_array[$i]["start_month_flg1"];
		$start_day1 = $group_array[$i]["start_day1"];
		$month_flg1 = $group_array[$i]["month_flg1"];
		$end_day1 = $group_array[$i]["end_day1"];

        // 集計行表示位置
        $count_row = $group_array[$i]['display']["result_row_position"] + 0;
		break;
	}
}
if ($start_month_flg1 == "") {$start_month_flg1 = "1";}
if ($start_day1 == "" || $start_day1 == "0") {$start_day1 = "1";}
if ($month_flg1 == "" || $end_day1 == "0") {$month_flg1 = "1";}
if ($end_day1 == "" || $end_day1 == "0") {	$end_day1 = "99";}

//権限チェック
$create_flg = "";
if (($emp_id == $person_charge_id) || ($emp_id == $caretaker_id) || ($emp_id == $caretaker2_id) || ($emp_id == $caretaker3_id) || ($emp_id == $caretaker4_id) || ($emp_id == $caretaker5_id) || ($emp_id == $caretaker6_id) || ($emp_id == $caretaker7_id) || ($emp_id == $caretaker8_id) || ($emp_id == $caretaker9_id) || ($emp_id == $caretaker10_id)) {
	$create_flg = "1";
}
///-----------------------------------------------------------------------------
// カレンダー(calendar)情報を取得
///-----------------------------------------------------------------------------
$arr_date = $obj->get_term_date($duty_yyyy, $duty_mm, $start_month_flg1, $start_day1, $month_flg1, $end_day1);
$arr_pos_value[] = "get_term_date 期間情報";
$arr_time_value[] = time();

//初回で年月が未指定の場合のみ、当日が範囲に入るように年月を調整する
if ($first_flg) {
	$today = date("Ymd");
	//当日が範囲より前の場合は、前月とする
	if ($today < $arr_date[0]) {
		$duty_mm--;
		if ($duty_mm < 1) {
			$duty_yyyy--;
			$duty_mm = 12;
		}
		$arr_date = $obj->get_term_date($duty_yyyy, $duty_mm, $start_month_flg1, $start_day1, $month_flg1, $end_day1);
	} else if ($today > $arr_date[1]) {
		//当日が範囲より後の場合は、翌月とする
		$duty_mm++;
		if ($duty_mm > 12) {
			$duty_yyyy++;
			$duty_mm = 1;
		}
		$arr_date = $obj->get_term_date($duty_yyyy, $duty_mm, $start_month_flg1, $start_day1, $month_flg1, $end_day1);
	}
$arr_pos_value[] = "get_term_date 当日が範囲に入るように年月を調整する";
$arr_time_value[] = time();
}

// 有効なグループの職員を取得、年月調整後に行う 20111020
//$data_emp = $obj->get_valid_empmst_array($emp_id, $duty_yyyy, $duty_mm);
// シフトグループに所属する職員を取得 20140210
$data_emp = $obj->get_empmst_by_group_id($group_id, $duty_yyyy, $duty_mm);
$arr_pos_value[] = "get_empmst_by_group_id シフトグループに所属する職員";
$arr_time_value[] = time();

$start_date = $arr_date[0];
$end_date = $arr_date[1];

$origin_start_date = $start_date;
if ($reappear_flg){		//表示期間の開始日 20130307
	$start_date= date("Ymd", strtotime("-$extra_day day",date_utils::to_timestamp_from_ymd($start_date) ));
}

$calendar_array = $obj->get_calendar_array($start_date, $end_date);
$arr_pos_value[] = "get_calendar_array カレンダー";
$arr_time_value[] = time();
$day_cnt=count($calendar_array);
///-----------------------------------------------------------------------------
//指定月の全曜日を設定
///-----------------------------------------------------------------------------
$week_array = array();
for ($k=1; $k<=$day_cnt; $k++) {
	$wk_date = $calendar_array[$k-1]["date"];
	$wk_yyyy = substr($wk_date, 0, 4);
	$wk_mm = substr($wk_date, 4, 2);
	$wk_dd = substr($wk_date, 6, 2);
	$tmp_date = mktime(0, 0, 0, $wk_mm, $wk_dd, $wk_yyyy);
	$week_array[$k]["name"] = $obj->get_weekday($tmp_date);
}
///-----------------------------------------------------------------------------
// 指定グループの出勤パターン(atdptn)情報を取得
///-----------------------------------------------------------------------------
//勤務シフトグループ情報ＤＢより情報取得
$wk_group = $obj->get_duty_shift_group_array($group_id, "", $data_wktmgrp);
$arr_pos_value[] = "get_duty_shift_group_array $group_id シフトグループ情報";
$arr_time_value[] = time();
$pattern_id = "";
if (count($wk_group) > 0) {
	$pattern_id = $wk_group[0]["pattern_id"];
	$reason_setting_flg = $wk_group[0]["reason_setting_flg"];
    $paid_hol_disp_flg = $wk_group[0]["paid_hol_disp_flg"];    //有給休暇残日数表示フラグ
}
///-----------------------------------------------------------------------------
//ＤＢ(atdptn)より出勤パターン情報取得
//ＤＢ(duty_shift_pattern)より勤務シフト記号情報を取得
///-----------------------------------------------------------------------------
$data_atdptn = $obj->get_atdptn_array($pattern_id);
$arr_pos_value[] = "get_atdptn_array 出勤パターン情報";
$arr_time_value[] = time();
//20140220 start
$data_pattern = $obj->get_duty_shift_pattern_array($pattern_id, $data_atdptn);
$arr_pos_value[] = "get_duty_shift_pattern_history_array 勤務シフト記号";
$arr_time_value[] = time();
//応援追加されている出勤パターングループ確認 
$arr_pattern_id = $obj->get_arr_pattern_id($group_id, $duty_yyyy, $duty_mm, array(), $pattern_id);
$pattern_id_cnt = count($arr_pattern_id);
//$group_emp_add_cnt = $obj->get_add_emp_cnt($group_id, $duty_yyyy, $duty_mm);
echo("<!-- 応援パターングループ $pattern_id_cnt -->\n");
if ($pattern_id_cnt > 0) {
    $data_atdptn_tmp = $obj->get_atdptn_array_2($arr_pattern_id); //配列対応
    $data_atdptn_all = array_merge($data_atdptn, $data_atdptn_tmp);
    $arr_pos_value[] = "get_atdptn_array 出勤パターン情報";
	$arr_time_value[] = time();
    $data_pattern_tmp = $obj->get_duty_shift_pattern_array("array", $data_atdptn_tmp, $arr_pattern_id); //配列対応
    $data_pattern_all = array_merge($data_pattern, $data_pattern_tmp);
    $arr_pos_value[] = "get_duty_shift_pattern_history_array 勤務シフト記号";
	$arr_time_value[] = time();
}
//応援なしの場合、同じパターングループのデータを設定
else {
	$data_atdptn_all = $data_atdptn;
	$data_pattern_all = $data_pattern;
}
//20140220 end
///-----------------------------------------------------------------------------
// 勤務シフト情報を取得（初期表示、再表示）
///-----------------------------------------------------------------------------
$set_data = array();
$wk_flg = "";
if (($data_cnt > 0) &&
	($group_id == $cause_group_id) &&
	($duty_yyyy == $cause_duty_yyyy) &&
	($duty_mm == $cause_duty_mm)) {
	$wk_flg = "1";
} else {
	$edit_start_day = 0;
	$edit_end_day = 0;
	$rslt_upd_flg = array(); //実績と希望の2段目用
}

///-----------------------------------------------------------------------------
//再表示時
///-----------------------------------------------------------------------------
if ($wk_flg == "1") {

	//前週を表示する場合 20130318
	$extra_day2 = 0;
	if ($week_index == "" && $before_week != 0) {
		$extra_day2 =  $before_week * 7;
	}

	for($i = 0; $i < $data_cnt; $i++) {
		//応援追加情報
		$assist_str = "assist_group_$i";
		$arr_assist_group = explode(",", $$assist_str);
		///-----------------------------------------------------------------------------
		//既存データの追加
		///-----------------------------------------------------------------------------
		for($k=1;$k<=$day_cnt;$k++) {
			$src_k =  $k + $extra_day2;

			$set_data[$i]["plan_rslt_flg"] = "1";			//２：予定は再取得→１に変更

			$wk2 = "rslt_pattern_id_$i" . "_" . $src_k;
			$wk3 = "rslt_id_$i" . "_" . $src_k;
			$wk6 = "rslt_reason_2_$i" . "_" . $src_k;

			$set_data[$i]["staff_id"] = $staff_id[$i];						//職員ＩＤ
			$set_data[$i]["rslt_pattern_id_$k"] = $$wk2;					//出勤グループＩＤ

			if ((int)substr($$wk3,0,2) > 0) {
				$set_data[$i]["rslt_id_$k"] = (int)substr($$wk3,0,2);		//出勤パターンＩＤ
			}
			if ((int)substr($$wk3,2,2) > 0) {
				$set_data[$i]["rslt_reason_$k"] = (int)substr($$wk3,2,2);	//事由
			}
			$set_data[$i]["rslt_reason_2_$k"] = $$wk6;	//事由（午前有給／午後有給）
			//応援情報
			$set_data[$i]["assist_group_$k"] = $arr_assist_group[$src_k - 1];
		}
	}
$arr_pos_value[] = "再表示時";
$arr_time_value[] = time();
}

///-----------------------------------------------------------------------------
// ユニット表示仕変
$unit_disp_level = $obj->get_emp_unitlevel($group_id);
///-----------------------------------------------------------------------------
;
// ユニット（３階層名）表示のため追加
$arr_class_name = get_class_name_array($con, $fname);
$unit_name = $arr_class_name[intval($unit_disp_level) - 1];

//-------------------------------------------------
// 当初予定表示状態を取得する START
//-------------------------------------------------
if (isset($_GET["yotei"])) {
    
    $disp_tosYotei_flg = $_GET["yotei"];
    
    // 当初予定表示状態を反転（反転はここのみで処理する）
    if ($disp_tosYotei_flg == "t") {
        $disp_tosYotei_flg = "f";
    } else {
        $disp_tosYotei_flg = "t";
    }
    $obj->set_disp_tosYotei_flg($group_id, $disp_tosYotei_flg);
} else {
    //	初期表示を「当初予定表示」ボタンにする
    $disp_tosYotei_flg = "f";	//初期表示
    $obj->set_disp_tosYotei_flg($group_id, $disp_tosYotei_flg);
}

// 取得テーブルを変更する
if ($disp_tosYotei_flg == "t") {
    $sw_sche = "_sche";
} else {
    $sw_sche = "";
}
//-------------------------------------------------
// 当初予定表示状態を取得する END
//-------------------------------------------------

///-----------------------------------------------------------------------------
// 勤務シフト情報を取得
///-----------------------------------------------------------------------------
$plan_array = $obj->get_duty_shift_plan_array(
								$group_id, $pattern_id,
								$duty_yyyy, $duty_mm, $day_cnt,
								$individual_flg,
								$hope_get_flg,
								$show_data_flg,
								$set_data, $week_array, $calendar_array,
								$data_atdptn, $data_pattern_all,
                                $data_st, $data_job, $data_emp,
                                "", null, $sw_sche);
$data_cnt = count($plan_array);
///-----------------------------------------------------------------------------
//予定コピー
///-----------------------------------------------------------------------------
//スタンプ無効クリック後の表示不具合対応 20100202
//※再計算クリック時も予定コピーと同様に表示する処理とする
if ($plan_copy_flg == "1" ||
		$stamp_comp_flg == "1" ||
		$edit_flg == "1") {	//編集時も予定コピーと同様に表示 20100216
	$wk_plan_copy_flg = "1";
} else {
	$wk_plan_copy_flg = "";
}
//	$wk_plan_copy_flg = $plan_copy_flg;	//予定コピー不具合対応 20100125
if ($plan_copy_flg == "1") {
	for ($i=0; $i<$data_cnt; $i++) {
		for ($m=1; $m<=$day_cnt; $m++) {
			$plan_array[$i]["rslt_pattern_id_$m"] = $plan_array[$i]["pattern_id_$m"];			//出勤パターンＩＤ

			if (($plan_array[$i]["rslt_id_$m"] == "") &&
				($plan_array[$i]["atdptn_ptn_id_$m"] != "") &&
				($plan_array[$i]["assist_group_$m"] == $group_id)) { //応援追加分はコピーしない 20090219
				$plan_array[$i]["rslt_id_$m"] = $plan_array[$i]["atdptn_ptn_id_$m"];				//勤務状況ＩＤ
				$plan_array[$i]["rslt_reason_$m"] = $plan_array[$i]["reason_$m"];					//事由
				$plan_array[$i]["rslt_reason_2_$m"] = $plan_array[$i]["reason_2_$m"];				//事由（午前有給／午後有給）
				$plan_array[$i]["rslt_name_$m"] = $plan_array[$i]["font_name_$m"];					//文字（勤務状況名）
				$plan_array[$i]["rslt_font_color_$m"] = $plan_array[$i]["font_color_$m"];			//文字（色）
				$plan_array[$i]["rslt_back_color_$m"] = $plan_array[$i]["back_color_$m"];			//文字（背景色）
				$plan_array[$i]["rslt_count_kbn_gyo_$m"] = $plan_array[$i]["count_kbn_gyo_$m"];		//集計行（勤務状況ID）
				$plan_array[$i]["rslt_count_kbn_retu_$m"] = $plan_array[$i]["count_kbn_retu_$m"];	//集計列（勤務状況ID）

				//更新時情報
				$rslt_upd_flg[$i] = 1;
			}
		}
	}
	$plan_copy_flg = "";
	$rslt_total_flg = "1";	//予定コピー不具合対応 20100125
}
else {	//予定コピー不具合対応 20100125
//スタンプ無効クリック後の表示不具合対応 20100202
	$rslt_total_flg = ($stamp_comp_flg == "1") ? "1" : "2";
}
///-----------------------------------------------------------------------------
// 行タイトル（行ごとの集計する勤務パターン）を取得
// 列タイトル（列ごとの集計する勤務パターン）を取得
///-----------------------------------------------------------------------------
$title_gyo_array = $obj->get_total_title_array($pattern_id,	"1", $data_pattern);
$arr_pos_value[] = "get_total_title_array 行タイトル";
$arr_time_value[] = time();
$title_retu_array = $obj->get_total_title_array($pattern_id, "2", $data_pattern);
$arr_pos_value[] = "get_total_title_array 列タイトル";
$arr_time_value[] = time();
if ((count($title_gyo_array) <= 0) || (count($title_retu_array) <= 0)) {
	$err_msg_2 = "勤務シフト記号情報が未設定です。管理画面で登録してください。";
}
///-----------------------------------------------------------------------------
//個人日数合計（行）を算出設定
//個人日数合計（列）を算出設定
///-----------------------------------------------------------------------------
//4番目の引数を"1"から"2"へ変更 20100112
//予定コピー不具合対応、4番目の引数を"2"から$rslt_total_flgへ変更 20100125
$gyo_array = $obj->get_total_cnt_array($group_id, $pattern_id,
	"1", $rslt_total_flg, $day_cnt, $plan_array, $title_gyo_array, $title_retu_array, $calendar_array, $data_atdptn);
$arr_pos_value[] = "get_total_cnt_array 個人日数合計（行）";
$arr_time_value[] = time();
//4番目の引数を"1"から"2"へ変更 20100112
//予定コピー不具合対応、4番目の引数を"2"から$rslt_total_flgへ変更 20100125
$retu_array = $obj->get_total_cnt_array($group_id, $pattern_id,
	"2", $rslt_total_flg, $day_cnt, $plan_array, $title_gyo_array, $title_retu_array, $calendar_array, $data_atdptn);
$arr_pos_value[] = "get_total_cnt_array 個人日数合計（列）";
$arr_time_value[] = time();

//繰越公休残日数 start
//公休数を位置を取得
$legal_hol_idx = -1;
for ($i=0; $i<count($title_gyo_array); $i++) {
    if ($title_gyo_array[$i]["name"] == "公休") {
        $legal_hol_idx = $i;
        break;
    }
}

//繰越公休残用クラス
$obj_hol_remain = new duty_shift_hol_remain_common_class($con, $fname, $group_id, $emp_id, $legal_hol_idx);

//前月残がない場合の警告画面を開くフラグ
$hol_remain_open_flg = false;
$hol_remain_total = 0;
if ($obj_hol_remain->transfer_holiday_days_flg == "t") {
    //データをまとめて取得しplan_arrayに設定する。公休残hol_remain、前月残prev_remain、公休使用数hol_cnt、所定公休数std_hol_cnt
    $obj_hol_remain->set_hol_remain_data($plan_array, $group_id, $duty_yyyy, $duty_mm, $origin_start_date, $end_date, $wk_plan_copy_flg, $gyo_array); //予定コピー、再計算時対応
    
    for ($i=0; $i<$data_cnt; $i++) {
        $hol_remain_total += $plan_array[$i]["hol_remain"];
    }
    //2ヶ月以前はチェックしない
    $check_yyyymm = date("Ym", strtotime("-1 month"));
    $duty_yyyymm = $duty_yyyy.sprintf("%02d", $duty_mm);
    //管理者・代行者
    if ($create_flg == "1" && $warning_flg == "" && $duty_yyyymm > $check_yyyymm) {
        //応援確認、職員設定に登録されているスタッフ
        $arr_main_staff_info = $obj->get_duty_shift_staff_array($group_id, array(), array(), array());
        $arr_main_staff_id = array();
        for ($i=0; $i<count($arr_main_staff_info); $i++) {
            $arr_main_staff_id[] = $arr_main_staff_info[$i]["emp_id"];
        }
        //今月残、前月残がない場合は、警告メッセージ表示
        for ($i=0; $i<$data_cnt; $i++) {
            $wk_staff_id = $plan_array[$i]["staff_id"];
            if (!in_array($wk_staff_id, $arr_main_staff_id)) {
                continue;
            }
            
            if ($plan_array[$i]["hol_remain"] == "" && $plan_array[$i]["prev_remain"] == "") {
                $hol_remain_open_flg = true;
                break;
            }
        }
        
    }
}
//情報をメニュー用クラスに設定する
$obj_menu->set_hol_remain_info($obj_hol_remain);
//繰越公休残日数 end

$title_hol_array = $obj->get_total_title_hol_array($pattern_id, $data_pattern, $data_atdptn);
$arr_pos_value[] = "get_total_title_hol_array 休暇タイトル";
$arr_time_value[] = time();

//休暇事由情報
$hol_cnt_array = $obj->get_hol_cnt_array($group_id, $pattern_id, "1", "1", $day_cnt, $plan_array, $title_gyo_array, $title_retu_array, $calendar_array, $title_hol_array, $data_atdptn);
$arr_pos_value[] = "get_hol_cnt_array 休暇集計";
$arr_time_value[] = time();

// 登録子画面の指定情報保存
$obj->update_last_ptn($emp_id, $last_ptn_id, $last_reason_2);
$arr_pos_value[] = "update_last_ptn 登録子画面の指定情報";
$arr_time_value[] = time();
//組み合わせ指定確認用配列
$seq_pattern_array = $obj->get_duty_shift_group_seq_pattern_array($group_id);
$arr_pos_value[] = "get_duty_shift_group_seq_pattern_array 組み合わせ指定";
$arr_time_value[] = time();
// 勤務記号の初期値
$def_ptn_info=$obj->get_auto_default_ptn_info($pattern_id);
if ($def_ptn_info["reason"] == " ") {
	$def_ptn_info["reason"] = "";
}
$arr_pos_value[] = "get_auto_default_ptn_info 勤務記号の初期値";
$arr_time_value[] = time();
//JavaScriptで使用するpatern_id
$wk_pattern_id = ($pattern_id == "") ? "0" : $pattern_id;

$wk_yyyymm = $duty_yyyy.sprintf("%02d",$duty_mm);
// 週の場合、スタンプフラグクリア
if ($edit_start_day != $edit_end_day) {
	$stamp_flg = "";
}

//集計表用
$staff_ids = "";
for ($i=0; $i<$data_cnt; $i++) {
	if ($i>0) {
		$staff_ids .= ",";
	}
	$staff_ids .= $plan_array[$i]["staff_id"];
}
//希望情報取得、予定背景色変更用 20120403
$plan_individual = $obj->get_plan_individual($group_id, $pattern_id, $start_date, $end_date);
$arr_pos_value[] = "get_plan_individual 希望情報取得";
$arr_time_value[] = time();

//-------------------------------------------------------------------------------
// 有給休暇残日数を取得
//-------------------------------------------------------------------------------
$paid_hol_all_array = array();
if ($paid_hol_disp_flg === "t") {
    $paid_hol_all_array = $obj->get_paid_hol_rest_array($plan_array, $calendar_array, $duty_yyyy, $duty_mm, "create");
$arr_pos_value[] = "get_paid_hol_rest_array 有給休暇残";
$arr_time_value[] = time();
}

//下書き状態取得 20150306
$draft_reg_flg = "";
$cond_add = "";
for ($i=0;$i<count($plan_array);$i++) {
    $wk_id = $plan_array[$i]["staff_id"];
    if ($cond_add == "") {
        $cond_add .= "and (emp_id = '$wk_id' ";
    } else {
        $cond_add .= "or emp_id = '$wk_id' ";
    }
}
if ($cond_add != "") {
    $cond_add .= ") ";
}
$sql = "select count(*) as cnt from duty_shift_plan_draft";
$cond = "where group_id = '$group_id' and duty_date >= '$origin_start_date' and duty_date <= '$end_date' ";
$cond .= $cond_add;
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$num_1 = pg_fetch_result($sel, 0, "cnt");
if ($num_1 > 0) {
    $draft_reg_flg = "1";
}

// <script type="text/javascript" src="....."></script>
include_js("js/fontsize.js");
include_js("js/jquery/jquery-1.7.2.min.js");
include_js("js/browser-support.js");
include_js("js/duty_shift/results.js");

//勤務パターン数によってフローティングウインドウ用の画像サイズを調整
$floatWinCnt=0;
$floatWinRow=1;
for ($k=0;$k<count($data_pattern_all);$k++) {
	if ($pattern_id == $data_pattern_all[$k]["pattern_id"] &&	$data_pattern_all[$k]["font_name"] != "" &&
			$data_pattern_all[$k]["stamp_disp_flg"] != "f") { //スタンプ非表示フラグ確認 20130219
		$floatWinCnt++;
	}
}

//スタンプ用ウインドウサイズ
//件数に応じた大きさにする 20130219 start
//ウィンドウの高さ＝上下の余白＋行数＊高さ22、件数が少ない場合でも最低限3行分とする、$floatWinCntへの+1は未設定分
$wk_row = ceil(($floatWinCnt+1) / 15);
if ($wk_row < 3) {
    $wk_row = 3;
}
$floatWinHeiStp = 44 + ($wk_row * 22);

if($floatWinCnt > 7){
    $floatWinWidStp=800;
}else{
	$floatWinWidStp=500;
}
//件数に応じた大きさにする 20130219 end

//スタンプ用ウインドウ位置
if ($stamp_flg != "1"){
	$floatWinTopStp=-1000;
	$floatWinLeftStp=-1000;
}else{
	$floatWinTopStp=5;
	$floatWinLeftStp=390;
	$floatWinDisStp='';
}

?>
<style type="text/css">
#floatWindowStp{
	display:<?=$floatWinDisStp?>;
	position:absolute;
	top:<?=$floatWinTopStp?>px;
	left:<?=$floatWinLeftStp?>px;
	width:<?=$floatWinWidStp?>px;
	height:<?=$floatWinHeiStp?>px;
	/*filter:alpha(opacity=90);*/ /*IE*/
    /*-moz-opacity:0.9;*/ /*FF*/
    /*opacity:0.9;*/ /*OTHER*/
}
#floatWindowStp dl{
	width:<?=$floatWinWidStp?>px;
	height:<?=$floatWinHeiStp?>px;
	background:url("./images/floatWindow_g.gif");
	margin:1;
}
</style>
<script type="text/javascript">
    var change_switch = <?=$obj->GetSumCustomSwith($pattern_id);?>;
    var count_row = <?=h($count_row); ?>;

	///-----------------------------------------------------------------------------
	//予定コピー
	///-----------------------------------------------------------------------------
	function planCopy() {
		if (confirm('「予定コピー」します。よろしいですか？')) {
			document.mainform.plan_copy_flg.value = "1";
			document.mainform.action="duty_shift_results.php";
			document.mainform.submit();
		}
	}
	///-----------------------------------------------------------------------------
	//登録
	///-----------------------------------------------------------------------------
	function editData() {
		if (document.mainform.regist_first_flg.value == '0') {
		    return false;
		}
		if (confirm('「登録」します。よろしいですか？')) {
            document.mainform.regist_first_flg.value = '0';
			document.mainform.action="duty_shift_results_update_exe.php";
			document.mainform.submit();
		}
	}

	///-----------------------------------------------------------------------------
    //Excel出力 20130124
    ///-----------------------------------------------------------------------------
    function makeExcel() {
        if (!document.mainform.data_exist_flg) {
            alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
            return false;
        }

		<?
			$phpVersionName=phpversion();
		?>
		var PHP_VER ;
		PHP_VER = "<? echo $phpVersionName; ?>";

		if ( PHP_VER >= "5.1.6" ){ // PHP5.1.6以降対応 2012.1.11
		<?
		$wkdat = "";
		$wkstart = array();
		$tmp_date = $origin_start_date;
		$wkcnt=0;
		foreach($week_array as $num=>$name){
			$tmp_date = strtotime($tmp_date);
			if( $name["name"] == "月" ){
				$buf = date("Ymd", $tmp_date);
				if ($buf <= $end_date) {				//表示期間のみを表示する 20130307
					$dispday = date("Y-m-d", $tmp_date);
					$wkdat .= $dispday.",";
				}
				$wkstart[$wkcnt] = $num;
				$wkcnt++;
			}
			$tmp_date = date("Ymd", strtotime("+1 day", $tmp_date));
		}
		$wkdat = rtrim($wkdat, ",");
		?>
		var wkdate = "<? echo $wkdat; ?>";
		var wkstart = "<? echo $wkstart[0]; ?>";
		        var url = 'duty_shift_menu_excel_5para.php';
	        	url += '?session=<?=$session?>';
		        url += '&emp_id=<?=$emp_id?>';
		        url += '&duty_yyyy=<?=$duty_yyyy?>';
		        url += '&duty_mm=<?=$duty_mm?>';
                url += '&group_id=<? echo $group_id; ?>';
		        url += '&duty_list='+wkdate;
		        url += '&duty_start='+wkstart;
			childwin_excel = window.open(url , 'CoMedixExcelParam', 'width=550,height=580,scrollbars=yes,resizable=yes');
		        childwin_excel.focus();
		} else{ // PHP5対応、PHP5ではないときの従来処理
		        document.mainform.action = "duty_shift_menu_excel.php";
		        document.mainform.target = "download";
		        document.mainform.submit();
		}
    }
    ///-----------------------------------------------------------------------------
    /// Excel出力、PHP4用 ( 前バージョン呼び出し用 ) 20130124
    ///-----------------------------------------------------------------------------
    function makeExcel4(){
	        document.mainform.action = "duty_shift_menu_excel.php";
	        document.mainform.target = "download";
	        document.mainform.submit();
    }
    ///-----------------------------------------------------------------------------
    // Excel出力、PHP5用 20130124
    ///-----------------------------------------------------------------------------
    function makeExcel5_1_6(_ratio,_paper_size,_setfitpage,_mtop,_mbottom,_mleft,_mright,_mhead,_mfoot,_row_count,_row_type,_blancrows,_weekcolor,_hopecolor,_yearmonth, _week4start, _event ){
		document.mainform.excel_ratio.value = _ratio;
		document.mainform.excel_paper_size.value = _paper_size;
		document.mainform.excel_setfitpage.value = _setfitpage
		document.mainform.excel_mtop.value = _mtop;
		document.mainform.excel_mbottom.value = _mbottom;
		document.mainform.excel_mleft.value = _mleft;
		document.mainform.excel_mright.value = _mright;
		document.mainform.excel_mhead.value = _mhead;
		document.mainform.excel_mfoot.value = _mfoot;
		document.mainform.excel_row_count.value = _row_count;
		document.mainform.excel_row_type.value = _row_type;
		document.mainform.excel_blancrows.value = _blancrows;
		document.mainform.excel_date.value = "";
		document.mainform.excel_weekcolor.value = _weekcolor;
        document.mainform.excel_hopecolor.value = _hopecolor;
		document.mainform.excel_yearmonth.value = _yearmonth;
        document.mainform.excel_week4start.value = _week4start;
        document.mainform.excel_event.value = _event;
        document.mainform.action = "duty_shift_menu_excel_5.php";
        document.mainform.target = "download";
        document.mainform.submit();
    }
    ///-----------------------------------------------------------------------------
    // Excel出力、PHP5用 週間表 20130124
    ///-----------------------------------------------------------------------------
    function makeExcel5_1_6week(_ratio,_paper_size,_setfitpage,_mtop,_mbottom,_mleft,_mright,_mhead,_mfoot,_row_count,_row_type,_blancrows,_date,_weekcolor,_hopecolor, _event ){
		document.mainform.excel_ratio.value = _ratio;
		document.mainform.excel_paper_size.value = _paper_size;
		document.mainform.excel_setfitpage.value = _setfitpage
		document.mainform.excel_mtop.value = _mtop;
		document.mainform.excel_mbottom.value = _mbottom;
		document.mainform.excel_mleft.value = _mleft;
		document.mainform.excel_mright.value = _mright;
		document.mainform.excel_mhead.value = _mhead;
		document.mainform.excel_mfoot.value = _mfoot;
		document.mainform.excel_row_count.value = _row_count;
		document.mainform.excel_row_type.value = _row_type;
		document.mainform.excel_blancrows.value = _blancrows;
		document.mainform.excel_date.value = _date;
        document.mainform.excel_weekcolor.value = _weekcolor;
        document.mainform.excel_hopecolor.value = _hopecolor;
		document.mainform.excel_yearmonth.value = "";
        document.mainform.excel_week4start.value = "";
        document.mainform.excel_event.value = _event;
        document.mainform.action = "duty_shift_menu_excel_5week.php";
        document.mainform.target = "download";
        document.mainform.submit();
    }
    ///-----------------------------------------------------------------------------
    //曜日別集計表 20130124
    ///-----------------------------------------------------------------------------
    function makeExcelDow() {
        if (!document.mainform.data_exist_flg) {
            alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
            return false;
        }
        document.mainform.action = "duty_shift_menu_dow.php";
        document.mainform.target = "";
        document.mainform.submit();
    }

  	///-----------------------------------------------------------------------------
	//表示期間に以前週表示
	///-----------------------------------------------------------------------------
    function show_before_week(wk_index){
        document.mainform.week_index.value = wk_index;
        document.mainform.action="duty_shift_results.php?session=<?=$session?>";
        document.mainform.target = "";
		document.mainform.submit();
    }
	///-----------------------------------------------------------------------------
	//指定日された選択入力可能にする
	///-----------------------------------------------------------------------------
	function showEdit(start_day, end_day) {
		try {
			//登録権限があるかチェック
			if (document.mainform.create_flg.value == "") {
				return;
			}
			document.mainform.edit_flg.value = '1';	//編集フラグ

			//指定日された選択入力可能にする
			if ((document.mainform.edit_start_day.value == start_day) &&
				(document.mainform.edit_end_day.value == end_day)) {
				//表示のみ
				document.mainform.edit_start_day.value = 0;
				document.mainform.edit_end_day.value = 0;
			} else {
				//選択入力可能
				document.mainform.edit_start_day.value = start_day;
				document.mainform.edit_end_day.value = end_day;
			}
			document.mainform.action="duty_shift_results.php?session=<?=$session?>";
			document.mainform.submit();
		}
		catch(err){
		}
	}

	///-----------------------------------------------------------------------------
	//行／列ハイライト
	///-----------------------------------------------------------------------------
	function highlightCells(class_name, gyo, retu) {
        if (document.mainform.stamp_flg && document.mainform.stamp_flg.value == '') {
			changeCellsColor(class_name, '#ffff66',gyo,retu);
		}
	}
	function dehighlightCells(class_name, gyo, retu) {
        if (document.mainform.stamp_flg && document.mainform.stamp_flg.value == '') {
			changeCellsColor(class_name, '',gyo,retu);
		}
	}
	function changeCellsColor(class_name, color, gyo, retu) {
		try {
			var data = document.getElementById("data");
			var data_cnt = data.rows.length;
			var summary = document.getElementById("summary");
			var summary_cnt = summary.rows.length;
			var day_cnt = document.mainform.day_cnt.value;
			var title_gyo_cnt = document.mainform.title_gyo_cnt.value;
			var td_name = '';
			///-----------------------------------------------------------------------------
			//実績時のみ表示
			///-----------------------------------------------------------------------------
			var wk = parseInt(gyo) / 2;
			var wk1 = "" + wk;
			//実績か判定
		    if (wk1.match(/[^0-9]/g)) {
				return;
			}
			///-----------------------------------------------------------------------------
			//カーソル表示
			///-----------------------------------------------------------------------------
			if ((parseInt(gyo) > 0) && (parseInt(retu) > 0)) {
				for (k=1;k<=day_cnt;k++){
					td_name = 'data' +  parseInt(gyo) + '_' + k;
					document.getElementById(td_name).style.backgroundColor = color;
				}
				for (i=1;i<=data_cnt;i++){
					td_name = 'data' + i + '_' + parseInt(retu);
					document.getElementById(td_name).style.backgroundColor = color;
				}
			}
			if (parseInt(gyo) > 0) {
				td_name = 'no' + (parseInt(gyo) - 1);
				document.getElementById(td_name).style.backgroundColor = color;
				td_name = 'name' + (parseInt(gyo) - 1);
				document.getElementById(td_name).style.backgroundColor = color;
				td_name = 'job' +  (parseInt(gyo) - 1);
				document.getElementById(td_name).style.backgroundColor = color;
				td_name = 'st' +  (parseInt(gyo) - 1);
				document.getElementById(td_name).style.backgroundColor = color;
				td_name = 'name' + (parseInt(gyo) - 1) + '_2';
				document.getElementById(td_name).style.backgroundColor = color;

				for (h=0;h<title_gyo_cnt;h++){
					td_name = 'sum_gyo' + (parseInt(gyo)) + '_' + h;
					document.getElementById(td_name).style.backgroundColor = color;
				}
			}
			if (parseInt(retu) > 0) {
				td_name = 'day' + parseInt(retu);
				document.getElementById(td_name).style.backgroundColor = color;
				td_name = 'day_of_week' +  parseInt(retu);
				document.getElementById(td_name).style.backgroundColor = color;

				for (i=1;i<=summary_cnt;i++) {
					td_name = 'summary' + i + '_' + parseInt(retu);
					document.getElementById(td_name).style.backgroundColor = color;
				}
			}
		}
		catch(err){
		}
	}

	///-----------------------------------------------------------------------------
	//十字ハイライト
	///-----------------------------------------------------------------------------
<? $obj_menu->createCrossHighlightJavaScript("1"); ?>

	///-----------------------------------------------------------------------------
	//メッセージ画面（表示／非表示）
	///-----------------------------------------------------------------------------
	function showMemo(day, memo) {
		try {
			if (memo == "") {
				return;
			}

		    //--このHTMLテンプレートを書き換えればヘルプのデザインが変わります。
			var msg1  = '<table bgcolor="#dddddd" border="1">\n';
				msg1 += '<tr><td>'+memo+'</td></tr>\n';
				msg1 += '</table>\n';
			showMsg(msg1);
		}
		catch(err){
		}
	}
	///-----------------------------------------------------------------------------
	//勤務パターン登録子画面
	///-----------------------------------------------------------------------------
	function ptnUpdate(plan_hope_flg, emp_idx, atdptn_ptn_id, reason_2, day) {
		//closeChildWin();
		if (!document.mainform.data_exist_flg) {
			alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
			return false;
		}

		//登録権限があるかチェック
		if (document.mainform.create_flg.value == "") {
			return;
		}

		if (document.mainform.stamp_flg.value == "1") {
			setStamp(emp_idx, day, plan_hope_flg);
			return;
		}

		wx = 380;
		wy = 270;
//ウィンドウ位置を調整
		if (navigator.userAgent.search("Firefox") != -1 ) {

			if (day < 16) {
				dx = 280 + (day * 20);
			} else {
				dx = 280 + (day * 20) - wx - 30;
			}
<?
if ($fullscreen_flg != "1") {
	echo("			wtop = 250;\n");
} else {
	echo("			wtop = 130;\n");
}
?>
			base = wtop + (emp_idx * 20);
			if (base > screen.height - wy) {
				base = screen.height - wy;
			}
		} else {

			midx = screen.width / 2;

			wleft = 0;
			wtop = 0;
			if (navigator.userAgent.search("MSIE") != -1 ) {
				wleft = window.screenLeft;
				wtop = window.screenTop;
			}
			dx =  wleft + getMouseX(event);

			if (day < 16) {
				dx = dx + 30;
			} else {
				dx = dx - wx - 30;
			}

			base = wtop + getMouseY(event);
			base = base - 20;

			wheight = screen.availHeight;
			if (base > wheight - wy) {
				base = wheight - wy;
			}

		}

		wk_id = 'staff_id['+emp_idx+']';
		emp_id = document.mainform.elements[wk_id].value;
		var url = 'duty_shift_menu_ptn_update.php';
		url += '?session=<?=$session?>';
		url += '&plan_hope_flg='+plan_hope_flg;
		url += '&group_id=<?=$group_id?>';
		url += '&pattern_id=<?=$pattern_id?>';
		url += '&emp_idx='+emp_idx;
		url += '&emp_id='+emp_id;
		url += '&atdptn_ptn_id='+atdptn_ptn_id;
		url += '&reason_2='+reason_2;
		url += '&duty_yyyy=<?=$duty_yyyy?>';
		url += '&duty_mm=<?=$duty_mm?>';
		url += '&day='+day;
		<? /* 期間変更対応のため配列の位置を示す */ ?>
		url += '&start_date=<?=$start_date?>';
		url += '&end_date=<?=$end_date?>';
		url += '&where_from=2';
		childwin_ptn_update = window.open(url, 'ptnUpdatePopup', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
		childwin_ptn_update.focus();
	}
	//タイムカード修正画面
	function timecardEdit(emp_idx, day_idx, date) {

		if (!document.mainform.data_exist_flg) {
			alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
			return false;
		}

		//登録権限があるかチェック
		if (document.mainform.create_flg.value == "") {
			return;
		}

		if (document.mainform.stamp_flg.value == "1") {
			setStamp(emp_idx, day_idx, '2');
			return;
		}

		dx = 0;
		base = 0;
		wx = 640;
		wy = 600;

		wk_id = 'staff_id['+emp_idx+']';
		emp_id = document.mainform.elements[wk_id].value;

		var wk1 = 'rslt_id_'+emp_idx+'_'+day_idx;
		var wk2 = 'rslt_reason_2_'+emp_idx+'_'+day_idx;

		atdptn_ptn_id = document.mainform.elements[wk1].value;
		reason_2 = document.mainform.elements[wk2].value;

		var url = 'atdbk_timecard_edit.php';
		url += '?session=<? echo($session); ?>';
		url += '&plan_hope_flg=4';
		url += '&emp_idx='+emp_idx;
		url += '&emp_id='+emp_id;
		url += '&day_idx='+day_idx;
		url += '&date='+date;
		url += '&atdptn_ptn_id='+atdptn_ptn_id;
		url += '&reason_2='+reason_2;
		url += '&wherefrom=4';
		url += '&yyyymm=<? echo($wk_yyyymm); ?>'; <? //締め処理後の登録ボタン無効化対応 20130128 ?>

		childwin_timecard = window.open(url, 'timecardPopup', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
		childwin_timecard.focus();
	}

//組み合わせ指定
var arr_today = new Array();
var arr_nextday = new Array();
<?
//組み合わせによる自動設定
for ($i=0; $i<count($seq_pattern_array); $i++) {

	$wk_today = sprintf("%02d%02d", $seq_pattern_array[$i]["today_atdptn_ptn_id"], $seq_pattern_array[$i]["today_reason"]);
	$wk_nextday = sprintf("%02d%02d", $seq_pattern_array[$i]["nextday_atdptn_ptn_id"], $seq_pattern_array[$i]["nextday_reason"]);
	echo("arr_today[$i] = '$wk_today';\n");
	echo("arr_nextday[$i] = '$wk_nextday';\n");
}

//月をまたがっている場合に自動設定されない不具合の対応 20100519
//$end_day = substr($end_date, 6, 2);
$end_day = $day_cnt;
?>
		var end_day = <?=$end_day?>;
	//子画面で更新ボタン押下
	function callbackPtnUpdate(plan_hope_flg, emp_idx, day, atdptn_ptn_id, reason_2, reload ) {

		//勤務パターンを設定
		if (plan_hope_flg == '0') {
			var wk1 = 'atdptn_ptn_id_'+emp_idx+'_'+day;
			var wk2 = 'reason_2_'+emp_idx+'_'+day;
			setUpdFlg(emp_idx);
		} else {
			var wk1 = 'rslt_id_'+emp_idx+'_'+day;
			var wk2 = 'rslt_reason_2_'+emp_idx+'_'+day;
			setRsltUpdFlg(emp_idx);
		}

		//勤務パターンID 4桁
        var wk_reason = (reason_2 != '' && reason_2 != ' ') ? reason_2 : '00';
        atdptn_ptn_id = (atdptn_ptn_id < 10) ? '0' + atdptn_ptn_id : atdptn_ptn_id;
        var ptn_id = atdptn_ptn_id + wk_reason;

        // data_idx
        var data_idx = (parseInt(emp_idx) * 2) + 2;

		document.mainform.elements[wk1].value = ptn_id;
		document.mainform.elements[wk2].value = reason_2;
        changeCell('data' + data_idx + '_' + day, emp_idx, day, plan_hope_flg);

        // 指定情報を保存
		document.mainform.elements['last_ptn_id'].value = ptn_id;
		document.mainform.elements['last_reason_2'].value = reason_2;

		var auto_set_flg = false;
		var auto_set_flg2 = false;
		if ((day + 1) <= end_day) {
			if (plan_hope_flg == '0') {
				var wk1 = 'atdptn_ptn_id_'+emp_idx+'_'+(day+1);
				var wk2 = 'reason_2_'+emp_idx+'_'+(day+1);
			} else {
				var wk1 = 'rslt_id_'+emp_idx+'_'+(day+1);
				var wk2 = 'rslt_reason_2_'+emp_idx+'_'+(day+1);
			}
			for (var i=0; i<arr_today.length; i++) {
				if (arr_today[i] == atdptn_ptn_id) {
					document.mainform.elements[wk1].value = arr_nextday[i];
					document.mainform.elements[wk2].value = '';
					var flg = (plan_hope_flg == '2') ? 2 : 1;
					setReason(emp_idx, day+1, <?=$wk_pattern_id?>, arr_nextday[i], flg);
					auto_set_flg = true;
					var wk_day = day + 1;
                    changeCell('data' + data_idx + '_' + wk_day, emp_idx, day, plan_hope_flg);
					break;
				}
			}
		}
		if (auto_set_flg == true && (day + 2) <= end_day) {
			if (plan_hope_flg == '0') {
				var wk2 = 'atdptn_ptn_id_'+emp_idx+'_'+(day+1);
				var wk3 = 'atdptn_ptn_id_'+emp_idx+'_'+(day+2);
				var wk4 = 'reason_2_'+emp_idx+'_'+(day+2);
			} else {
				var wk2 = 'rslt_id_'+emp_idx+'_'+(day+1);
				var wk3 = 'rslt_id_'+emp_idx+'_'+(day+2);
				var wk4 = 'rslt_reason_2_'+emp_idx+'_'+(day+2);
			}
			var yokuyoku_ptn_id = document.mainform.elements[wk2].value;

			for (var i=0; i<arr_today.length; i++) {
				if (arr_today[i] == yokuyoku_ptn_id) {
					document.mainform.elements[wk3].value = arr_nextday[i];
					document.mainform.elements[wk4].value = '';
					var flg = (plan_hope_flg == '2') ? 2 : 1;
					setReason(emp_idx, day+2, <?=$wk_pattern_id?>, arr_nextday[i], flg);
					auto_set_flg2 = true;
					var wk_day = day + 2;
                    changeCell('data' + data_idx + '_' + wk_day, emp_idx, day, plan_hope_flg);
					break;
				}
			}
		}
		if (auto_set_flg2 == true && (day + 3) <= end_day) {
			if (plan_hope_flg == '0') {
				var wk2 = 'atdptn_ptn_id_'+emp_idx+'_'+(day+2);
				var wk3 = 'atdptn_ptn_id_'+emp_idx+'_'+(day+3);
				var wk4 = 'reason_2_'+emp_idx+'_'+(day+3);
			} else {
				var wk2 = 'rslt_id_'+emp_idx+'_'+(day+2);
				var wk3 = 'rslt_id_'+emp_idx+'_'+(day+3);
				var wk4 = 'rslt_reason_2_'+emp_idx+'_'+(day+3);
			}
			var yokuyokuyoku_ptn_id = document.mainform.elements[wk2].value;

			for (var i=0; i<arr_today.length; i++) {
				if (arr_today[i] == yokuyokuyoku_ptn_id) {
					document.mainform.elements[wk3].value = arr_nextday[i];
					document.mainform.elements[wk4].value = '';
					var flg = (plan_hope_flg == '2') ? 2 : 1;
					setReason(emp_idx, day+3, <?=$wk_pattern_id?>, arr_nextday[i], flg);
					var wk_day = day + 3;
                    changeCell('data' + data_idx + '_' + wk_day, emp_idx, day, plan_hope_flg);
					break;
				}
			}
		}
		document.mainform.emp_idx.value = emp_idx;
		document.mainform.stamp_comp_flg.value = '1';	//再計算フラグ追加 20100202

		//再表示
		if (reload!='non') {
			showEdit(document.mainform.edit_start_day.value, document.mainform.edit_end_day.value);
		}
	}

	//再計算処理 追加 20100202
	function stampComp() {
		document.mainform.stamp_comp_flg.value = '1';
		showEdit(document.mainform.edit_start_day.value, document.mainform.edit_end_day.value);
	}

var arr_atdptn_reason = new Array();
<?
//事由情報を出力
$arr_group_pattern_reason = $obj->get_pattern_reason_array();
foreach ($arr_group_pattern_reason as $wk_tmcd_group_id => $arr_pattern_reason) {
	foreach ($arr_pattern_reason as $wk_atdptn_id => $reason) {
		if ($reason != "" && !($wk_atdptn_id == "10" && $reason == "24")) {
			echo("arr_atdptn_reason['{$wk_tmcd_group_id}_{$wk_atdptn_id}'] = '$reason';\n");
		}
	}
}
?>
	// 勤務パターンに組合わされた事由があれば設定し、値を返す
	// pos 職員の位置
	// day 日の位置
	// tmcd_group_id 勤務パターングループ
	// flg フラグ 1:予定 2:希望、実績の2段目の場合
	function setReason(pos, day, tmcd_group_id, atdptn_id, flg) {

		if (flg == 1) {
			item_name = 'reason_2_'+pos+'_'+day;
		} else {
			item_name = 'rslt_reason_2_'+pos+'_'+day;
		}
		if (atdptn_id == '0000') {
			document.mainform[item_name].value = '';
			return '';
		}
		wk_atdptn_id = parseInt(atdptn_id.substring(0, 2),10);
		wk_id = tmcd_group_id+'_'+wk_atdptn_id;
		if (arr_atdptn_reason[wk_id] == undefined) {
			document.mainform[item_name].value = '';
			return '';
		}
		document.mainform[item_name].value = arr_atdptn_reason[wk_id];
		return arr_atdptn_reason[wk_id];
	}
	function setRsltUpdFlg(emp_idx) {
		var wk = 'rslt_upd_flg['+emp_idx+']';
		document.mainform.elements[wk].value = 1;
	}
	//スタンプ有効無効設定
	function setStampFlg() {
		if (!document.mainform.data_exist_flg) {
			alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
			return false;
		}
		var flg = (document.mainform.stamp_flg.value == '') ? '1' : '';
		document.mainform.stamp_flg.value = flg;

		var btn = document.getElementById('stamp_img');
		if (flg == '') {
			btn.src = 'img/ico-illust-stamp.gif';
			//フローティング(スタンプ用)非表示
			floatWindowHide(0,'Stp');
		} else {
			btn.src = 'img/ico-illust-stamp-off.gif';
			//フローティング(スタンプ用)表示
			floatWindowHide(1,'Stp');
		}

		return;
	}

	var stamp_ptn = '<? $wk_stamp_ptn = ($stamp_ptn != "") ? $stamp_ptn :$def_ptn_info["atdptn_ptn_id"]; echo($wk_stamp_ptn); ?>';
	var stamp_reason = '<? $wk_stamp_reason = ($stamp_reason != "") ? $stamp_reason :$def_ptn_info["reason"]; echo($wk_stamp_reason); ?>';
	// スタンプパターン設定。スタンプの基準を設定する。
	// ptn 勤務パターン
	// reason 事由
	function setStampPtn(ptn, reason, targetid) {
		if (stamp_ptn != '') {
			wk_id = (stamp_ptn != '10') ? stamp_ptn : stamp_ptn+'_'+stamp_reason;
			cel = document.getElementById("stamp_"+wk_id);
		}
		wk_id = (ptn != '10') ? ptn : ptn+'_'+reason;

		cel = document.getElementById("stamp_"+wk_id);

		stamp_ptn = ptn;
		stamp_reason = reason;

		//cell = document.getElementById("stamp_cel");
		//cell2 = document.getElementById("stamp_cel2");
		//未設定
		if (stamp_ptn == '') {
			//cell.style.backgroundColor = '#ffffff';
			//cell2.style.backgroundColor = '#ffffff';
			var wk_back_color = '#ffffff';
			var wk_ptn_name = '未設定';
			var wk_font_color = '#000000';
		} else {
			wk_ptn_id = (stamp_ptn != '10') ? stamp_ptn : stamp_ptn+'_'+stamp_reason;
			//cell.style.backgroundColor = arr_bg_color[wk_ptn_id];
			//cell2.style.backgroundColor = arr_bg_color[wk_ptn_id];
			var wk_back_color = arr_bg_color[wk_ptn_id];
			var wk_ptn_name = arr_ptn_name[wk_ptn_id];
			var wk_font_color = arr_font_color[wk_ptn_id];
		}

		//------------------------------------------------------------------------------------------------------
		//スタンプ時
		//------------------------------------------------------------------------------------------------------
        //前回クリックされた勤務シフトは元の表示に戻す
        cell = document.getElementById('stmpPosition');
        var stmpPositionID=cell.value;
        //今回クリック情報を保持
        cell.value = targetid;

        //前回クリック元の表示に戻す
		if (stmpPositionID!=''){
			cell = document.getElementById(stmpPositionID);
			cell.style.fontWeight = 'normal';
			cell.style.fontStyle = 'normal';
		}

		//今回クリックされた箇所は太字
		cell = document.getElementById(targetid);
		cell.style.fontWeight = 'bold';
		cell.style.fontStyle = 'italic';

		//連続を1に設定
		document.getElementById("continuous_cnt").value = "1";
		cell = document.getElementById('stamp_cel');
		cell.style.backgroundColor = wk_back_color;
        cell.innerHTML = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="'+wk_font_color+'"><b>'+wk_ptn_name+'</b></font>';
		//下側スタンプへの設定
		//cell2.innerHTML = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="'+wk_font_color+'">'+wk_ptn_name+'</font>';
		//document.getElementById("continuous_cnt2").value = "1";
	}
	//連続変更時設定
	function setCnt(flg) {
		if (flg == 1) {
			document.getElementById("continuous_cnt2").value = document.getElementById("continuous_cnt").value;
		} else {
			document.getElementById("continuous_cnt").value = document.getElementById("continuous_cnt2").value;
		}
	}

var arr_ptn_name = new Array();
var arr_font_color = new Array();
var arr_bg_color = new Array();
var arr_reason_name = new Array();
<?
for ($k=0;$k<count($data_pattern_all);$k++) {
	if ($pattern_id == $data_pattern_all[$k]["pattern_id"] &&
			$data_pattern_all[$k]["font_name"] != "") {

		$wk_atdptn_ptn_id = $data_pattern_all[$k]["atdptn_ptn_id"];
		$wk_reason = $data_pattern_all[$k]["reason"];
		$wk_key = ($wk_atdptn_ptn_id != "10") ? $wk_atdptn_ptn_id : $wk_atdptn_ptn_id."_".$wk_reason;

		//シングルクォーテーションがある場合、Javascriptエラーになるためエスケープ 20110817
		$wk_font_name = str_replace("'", "\'", h($data_pattern_all[$k]["font_name"]));
		echo("arr_ptn_name['".$wk_key."'] = '".$wk_font_name."';\n");
		echo("arr_font_color['".$wk_key."'] = '".$data_pattern_all[$k]["font_color_id"]."';\n");
		echo("arr_bg_color['".$wk_key."'] = '".$data_pattern_all[$k]["back_color_id"]."';\n");

	}
}

$reason_2_array = $obj->get_reason_2("");
for ($m=0; $m<count($reason_2_array); $m++) {
	$wk_key = $reason_2_array[$m]["id"];
	echo("arr_reason_name['".$wk_key."'] = '".$reason_2_array[$m]["font_name"]."';\n");
}
?>
    //デフォルト背景色を取得する
    function getDefaultBackColor(day) {
        //曜日欄
        var cell = document.getElementById('day_of_week' + day);

        //曜日欄の現設定背景色情報を取得
        var bgcolor;
        if (navigator.userAgent.indexOf("MSIE") >= 0) {
           bgcolor = cell.currentStyle.backgroundColor;
        }
        else {
           bgcolor = document.defaultView.getComputedStyle(cell,null).getPropertyValue("background-color");
        }
        return bgcolor;
    }

	// スタンプ設定。日毎のセルへ設定する。
	// emp_idx 職員の行位置
	// day 日の位置
	// plan_hope_flg 希望フラグ（実績も同じフラグ）0:予定のみ 2:希望等2段目あり
	function setStamp(emp_idx, day, plan_hope_flg) {

		if (document.mainform.stamp_flg.value != "1") {
			showEdit(day, day);
			return;
		}

		var copy_cnt = document.mainform.elements['continuous_cnt'].value;
		var wk_day = parseInt(day);

		//連続回数分繰返し
        for (cnt = 0; cnt < copy_cnt; cnt++) {

		if (plan_hope_flg == '0') {
			if (document.mainform.elements['show_data_flg'].value != '') {
				var data_idx = (parseInt(emp_idx) * 2) + 1;
			} else {
				var data_idx = parseInt(emp_idx) + 1;
			}
			var wk0 = 'data' + data_idx + '_' + wk_day;
			var wk1 = 'atdptn_ptn_id_'+emp_idx+'_'+wk_day;
			var wk2 = 'reason_2_'+emp_idx+'_'+wk_day;
			setUpdFlg(emp_idx);
		} else {
			var data_idx = (parseInt(emp_idx) + 1) * 2;
			var wk0 = 'data' + data_idx + '_' + wk_day;
			var wk1 = 'rslt_id_'+emp_idx+'_'+wk_day;
			var wk2 = 'rslt_reason_2_'+emp_idx+'_'+wk_day;
			setRsltUpdFlg(emp_idx);
		}
		var cell = document.getElementById(wk0);
		//未設定にする
		if (stamp_ptn == '') {
			document.mainform.elements[wk1].value = '0000';
			document.mainform.elements[wk2].value = '';

            //日付欄
			cell2 = document.getElementById('day' + wk_day);

			//日付欄の背景色をセット
	        cell.style.backgroundColor = getDefaultBackColor(wk_day);
            cell.innerHTML = '&nbsp;';
            changeCell(wk0, emp_idx, wk_day, plan_hope_flg);

			wk_day++;
			if (wk_day > end_day) {//終了日確認
				break;
			}
			continue;
		}
		//組合せ確認
		var wk_reason = (stamp_reason != '' && stamp_reason != ' ') ? stamp_reason : '00';
		atdptn_ptn_id = (stamp_ptn < 10) ? '0' + stamp_ptn : stamp_ptn;
		atdptn_ptn_id = atdptn_ptn_id + wk_reason;

		document.mainform.elements[wk1].value = atdptn_ptn_id;

		//事由設定
		var flg = (plan_hope_flg == '2') ? 2 : 1;
		var wk_reason_2 = setReason(emp_idx, day, <?=$wk_pattern_id?>, atdptn_ptn_id, flg);
		wk_ptn_id = (stamp_ptn != '10') ? stamp_ptn : stamp_ptn+'_'+stamp_reason;

		//背景色[白:#FFFFFF]以外は適用
		if (arr_bg_color[wk_ptn_id].toUpperCase() != "#FFFFFF") {
            cell.style.backgroundColor = arr_bg_color[wk_ptn_id];

		} else {
			//背景色[白:#FFFFFF]の場合
			//日付欄の背景色をセット
			cell.style.backgroundColor = getDefaultBackColor(wk_day);

		}
		var wk_ptn_name = arr_ptn_name[wk_ptn_id];

<? //事由設定フラグ確認
if ($reason_setting_flg == "t") {
?>
		if (wk_reason_2 != '') {
			wk_ptn_name = wk_ptn_name + arr_reason_name[wk_reason_2];
		}
<? } ?>
		var wk_font_color = arr_font_color[wk_ptn_id];

		fsize = (wk_ptn_name.length > 1) ? 'j10' : 'j12';
        cell.innerHTML = '<font face="ＭＳ Ｐゴシック, Osaka" color="'+wk_font_color+'">'+wk_ptn_name+'</font>';
        changeCell(wk0, emp_idx, wk_day, plan_hope_flg);

		//組合せ設定
		var auto_set_flg = false;
		var auto_set_flg2 = false;
		var auto_set_flg3 = false;
		//2日目
	    wk_day++;
	    if (wk_day > end_day) {//終了日確認
	        break;
        }
		if ((wk_day) <= end_day) {
			var wk0 = 'data' + data_idx + '_' + (wk_day);
			if (plan_hope_flg == '0') {
				var wk1 = 'atdptn_ptn_id_'+emp_idx+'_'+(wk_day);
				var wk2 = 'reason_2_'+emp_idx+'_'+(wk_day);
			} else {
				var wk1 = 'rslt_id_'+emp_idx+'_'+(wk_day);
				var wk2 = 'rslt_reason_2_'+emp_idx+'_'+(wk_day);
			}

            for (var i=0; i<arr_today.length; i++) {
				if (arr_today[i] == atdptn_ptn_id) {
					document.mainform.elements[wk1].value = arr_nextday[i];
					document.mainform.elements[wk2].value = '';
					auto_set_flg = true;
					break;
				}
			}
			if (auto_set_flg == true) {
				var cell = document.getElementById(wk0);
				wk_atdptn_ptn_id = parseInt(arr_nextday[i].substring(0, 2),10);
				wk_reason = parseInt(arr_nextday[i].substring(2, 4),10);
				wk_ptn_id = (wk_atdptn_ptn_id != '10') ? wk_atdptn_ptn_id : wk_atdptn_ptn_id+'_'+wk_reason;
				wk_reason_2 = setReason(emp_idx, wk_day, <?=$wk_pattern_id?>, arr_nextday[i], flg);

				//背景色[白:#FFFFFF]以外は適用
				if (arr_bg_color[wk_ptn_id].toUpperCase() != "#FFFFFF") {
	                cell.style.backgroundColor = arr_bg_color[wk_ptn_id];

				} else {
					//背景色[白:#FFFFFF]の場合
					//日付欄の背景色をセット
					cell.style.backgroundColor = getDefaultBackColor(wk_day);

				}
				var wk_ptn_name = arr_ptn_name[wk_ptn_id];
<? //事由設定フラグ確認
if ($reason_setting_flg == "t") {
?>
				if (wk_reason_2 != '') {
					wk_ptn_name = wk_ptn_name + arr_reason_name[wk_reason_2];
				}
<? } ?>
				var wk_font_color = arr_font_color[wk_ptn_id];
				fsize = (wk_ptn_name.length > 1) ? 'j10' : 'j12';
		        cell.innerHTML = '<font face="ＭＳ Ｐゴシック, Osaka" color="'+wk_font_color+'">'+wk_ptn_name+'</font>';
		        changeCell(wk0, emp_idx, wk_day, plan_hope_flg);
				wk_day++;//組合せ設定できたら対象日を進める
				if (wk_day > end_day) {//終了日確認
					break;
				}
			}
		}
		//3日目
		if (auto_set_flg == true && (wk_day) <= end_day) {
			var wk0 = 'data' + data_idx + '_' + (wk_day);
			if (plan_hope_flg == '0') {
				var wk2 = 'atdptn_ptn_id_'+emp_idx+'_'+(wk_day-1);
				var wk3 = 'atdptn_ptn_id_'+emp_idx+'_'+(wk_day);
				var wk4 = 'reason_2_'+emp_idx+'_'+(wk_day);
			} else {
				var wk2 = 'rslt_id_'+emp_idx+'_'+(wk_day-1);
				var wk3 = 'rslt_id_'+emp_idx+'_'+(wk_day);
				var wk4 = 'rslt_reason_2_'+emp_idx+'_'+(wk_day);
			}
			var yokuyoku_ptn_id = document.mainform.elements[wk2].value;

			for (var i=0; i<arr_today.length; i++) {
				if (arr_today[i] == yokuyoku_ptn_id) {
					document.mainform.elements[wk3].value = arr_nextday[i];
					document.mainform.elements[wk4].value = '';
					auto_set_flg2 = true;
					break;
				}
			}
			if (auto_set_flg2 == true) {
				var cell = document.getElementById(wk0);
				wk_atdptn_ptn_id = parseInt(arr_nextday[i].substring(0, 2),10);
				wk_reason = parseInt(arr_nextday[i].substring(2, 4),10);
				wk_ptn_id = (wk_atdptn_ptn_id != 10) ? wk_atdptn_ptn_id : wk_atdptn_ptn_id+'_'+wk_reason;
				wk_reason_2 = setReason(emp_idx, wk_day, <?=$wk_pattern_id?>, arr_nextday[i], flg);

				//背景色[白:#FFFFFF]以外は適用
				if (arr_bg_color[wk_ptn_id].toUpperCase() != "#FFFFFF") {
	                cell.style.backgroundColor = arr_bg_color[wk_ptn_id];

				} else {
					//背景色[白:#FFFFFF]の場合
					//日付欄の背景色をセット
					cell.style.backgroundColor = getDefaultBackColor(wk_day);

				}
				var wk_ptn_name = arr_ptn_name[wk_ptn_id];
<? //事由設定フラグ確認
if ($reason_setting_flg == "t") {
?>
				if (wk_reason_2 != '') {
					wk_ptn_name = wk_ptn_name + arr_reason_name[wk_reason_2];
				}
<? } ?>
				var wk_font_color = arr_font_color[wk_ptn_id];

				fsize = (wk_ptn_name.length > 1) ? 'j10' : 'j12';
		        cell.innerHTML = '<font face="ＭＳ Ｐゴシック, Osaka" color="'+wk_font_color+'">'+wk_ptn_name+'</font>';
		        changeCell(wk0, emp_idx, wk_day, plan_hope_flg);
				wk_day++;//組合せ設定できたら対象日を進める
				if (wk_day > end_day) {//終了日確認
					break;
				}
			}
		}
        //4日目
		if (auto_set_flg2 == true && (wk_day) <= end_day) {
			var wk0 = 'data' + data_idx + '_' + (wk_day);
			if (plan_hope_flg == '0') {
				var wk2 = 'atdptn_ptn_id_'+emp_idx+'_'+(wk_day-1);
				var wk3 = 'atdptn_ptn_id_'+emp_idx+'_'+(wk_day);
				var wk4 = 'reason_2_'+emp_idx+'_'+(wk_day);
			} else {
				var wk2 = 'rslt_id_'+emp_idx+'_'+(wk_day-1);
				var wk3 = 'rslt_id_'+emp_idx+'_'+(wk_day);
				var wk4 = 'rslt_reason_2_'+emp_idx+'_'+(wk_day);
			}
			var yokuyokuyoku_ptn_id = document.mainform.elements[wk2].value;

			for (var i=0; i<arr_today.length; i++) {
				if (arr_today[i] == yokuyokuyoku_ptn_id) {
					document.mainform.elements[wk3].value = arr_nextday[i];
					document.mainform.elements[wk4].value = '';
					auto_set_flg3 = true;
					break;
				}
			}
			if (auto_set_flg3 == true) {
				var cell = document.getElementById(wk0);
				wk_atdptn_ptn_id = parseInt(arr_nextday[i].substring(0, 2),10);
				wk_reason = parseInt(arr_nextday[i].substring(2, 4),10);
				wk_ptn_id = (wk_atdptn_ptn_id != 10) ? wk_atdptn_ptn_id : wk_atdptn_ptn_id+'_'+wk_reason;
				wk_reason_2 = setReason(emp_idx, wk_day, <?=$wk_pattern_id?>, arr_nextday[i], flg);

				//背景色[白:#FFFFFF]以外は適用
				if (arr_bg_color[wk_ptn_id].toUpperCase() != "#FFFFFF") {
	                cell.style.backgroundColor = arr_bg_color[wk_ptn_id];

				} else {
					//背景色[白:#FFFFFF]の場合
					//日付欄の背景色をセット
					cell.style.backgroundColor = getDefaultBackColor(wk_day);

				}
				var wk_ptn_name = arr_ptn_name[wk_ptn_id];
<? //事由設定フラグ確認
if ($reason_setting_flg == "t") {
?>
				if (wk_reason_2 != '') {
					wk_ptn_name = wk_ptn_name + arr_reason_name[wk_reason_2];
				}
<? } ?>
				var wk_font_color = arr_font_color[wk_ptn_id];

				fsize = (wk_ptn_name.length > 1) ? 'j10' : 'j12';
		        cell.innerHTML = '<font face="ＭＳ Ｐゴシック, Osaka" color="'+wk_font_color+'">'+wk_ptn_name+'</font>';
		        changeCell(wk0, emp_idx, wk_day, plan_hope_flg);

				wk_day++;//組合せ設定できたら対象日を進める
				if (wk_day > end_day) {//終了日確認
					break;
				}
			}
		}

		}
	}
	//タイムカード一括修正画面
	//emp_id:職員ID
	function openTimecardAll(emp_id) {
		if (!document.mainform.data_exist_flg) {
			alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
			return false;
		}

		//登録権限があるかチェック
		if (document.mainform.create_flg.value == "") {
			return;
		}

		base_left = 0;
		base = 0;
		wx = window.screen.availWidth;
		wy = window.screen.availHeight;
		// work_admin_timecard.phpをwork_admin_timecard_shift.phpに変更 20130129
		url = 'work_admin_timecard_shift.php?session=<?=$session?>&emp_id='+emp_id+'&yyyymm=<? echo($wk_yyyymm); ?>&wherefrom=10&group_id=<?=$group_id?>&duty_yyyy=<? echo($duty_yyyy); ?>&duty_mm=<? echo($duty_mm); ?>&staff_ids=<? echo($staff_ids); ?>&shift_from_flg=t';
		window.open(url, 'TimecardAll', 'left='+base_left+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
	}
	//グループタイムカード一括修正画面
	function openTimecardGroup(group_id, duty_date) {
		if (!document.mainform.data_exist_flg) {
			alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
			return false;
		}

		//登録権限があるかチェック
		if (document.mainform.create_flg.value == "") {
			return;
		}

		base_left = 0;
		base = 0;
		wx = window.screen.availWidth;
		wy = window.screen.availHeight;
		//group emp_id
		url = 'duty_shift_timecard_group.php?session=<?=$session?>&yyyymm=<? echo($duty_yyyy.sprintf("%02d",$duty_mm)); ?>&duty_date='+duty_date+'&wherefrom=4&group_id=<?=$group_id?>';
		window.open(url, 'TimecardAll', 'left='+base_left+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
	}

	//勤務集計表
	function total() {
		if (!document.mainform.data_exist_flg) {
			alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
			return false;
		}

		//ＷＩＮＤＯＷをＯＰＥＮ
		dx = screen.width;
		dy = screen.top;
		base_left = 0;
		base = 0;
		wx = 1100;
		wy = 700;

		var url = 'duty_shift_total.php';
		url += '?session=<?=$session?>';
		url += '&duty_yyyy=<?=$duty_yyyy?>';
		url += '&duty_mm=<?=$duty_mm?>';
		url += '&group_id=<?=$group_id?>';
		url += '&pattern_id=<?=$pattern_id?>';
		url += '&staff_ids=<?=$staff_ids?>';
		childwin_total = window.open(url, 'totalFormChild', 'left='+base_left+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
		childwin_total.focus();
//		document.mainform.action = "duty_shift_total.php";
//		document.mainform.target = "totalFormChild";
//		document.mainform.submit();
	}

<? // 繰越公休残日数 start
$prev_mm = $duty_mm - 1;
$prev_yyyy = $duty_yyyy;
if ($prev_mm <= 0) {
    $prev_mm = 12;
    $prev_yyyy = $duty_yyyy - 1;
}
 ?>
	//繰越公休残日数
	function hol_remain(flg) {
		if (!document.mainform.data_exist_flg) {
			alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
			return false;
		}
        function getText(tag){
            return (tag.innerText || tag.textContent || "") ;
        }

        var holcnt_str = '';
<?
//画面の公休数を取得
if ($legal_hol_idx >= 0) {
?>
        for (i=0; i<<?echo($data_cnt);?>; i++) {
            var td_id = 'sum_gyo'+((i+1)*2)+'_'+<?echo($legal_hol_idx);?>;

            var su = getText(document.getElementById(td_id));
            if (i==0) {
                holcnt_str = su;
            }
            else {
                holcnt_str += ','+su;
            }
        }
        <? } ?>
		//ＷＩＮＤＯＷをＯＰＥＮ
		wx = 600;
		wy = 600;
        dx = screen.width / 2 - (wx / 2);
        dy = screen.top;
        base = screen.height / 2 - (wy / 2);

		var url = 'duty_shift_hol_remain.php';
		url += '?session=<? echo $session; ?>';
		url += '&group_id=<? echo $group_id; ?>';
        if (flg == 1) {
		url += '&duty_yyyy=<? echo $duty_yyyy; ?>';
		url += '&duty_mm=<? echo $duty_mm; ?>';
        }
        else {
		url += '&duty_yyyy=<? echo $prev_yyyy; ?>';
		url += '&duty_mm=<? echo $prev_mm; ?>';
		url += '&warning_flg=1';
        }
<?
//warning_flg:前月残がないため警告画面として開いた状態を示すフラグ
//週前表示をしている場合は、件数を渡さない
if ($extra_day == 0) {
?>
		url += '&hol_cnt='+holcnt_str;
<? } ?>
    	childwin_hol = window.open(url, 'holFormChild', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
		childwin_hol.focus();
	}
<? // 繰越公休残日数 end ?>
<? // 年次有給休暇簿 start ?>
function openStaffList() {
    dx = screen.width;
    dy = screen.top;
    base = 0;
    wx = 510;
    wy = 600;
    dx = screen.width / 2 - (wx / 2);
    dy = screen.top;
    base = screen.height / 2 - (wy / 2);

    var url = './duty_shift_paid_hol_staff_select.php';
    
    url += '?session=<? echo $session; ?>';
    url += '&group_id=<? echo $group_id; ?>';
    url += '&duty_yyyy=<? echo $duty_yyyy; ?>';
    url += '&duty_mm=<? echo $duty_mm; ?>';
    childwin = window.open(url, 'stafflistpopup', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
    childwin.focus();
}
<? // 年次有給休暇簿 end ?>

	//スクロール
	function scroll_region(flg) {

		switch(flg) {
		case 1: //left
<?
if ($fullscreen_flg != "1") {
	?>
			parent.frames['floatingpage'].document.body.scrollLeft = 0;
<? } else {?>
			window.scrollTo(0,0);
<? } ?>
			break;
		case 2: //up
			var wk_ht = document.getElementById('region').offsetHeight;
			var wk_pos = document.getElementById('region').scrollTop
			document.getElementById('region').scrollTop = wk_pos - wk_ht;
			break;
		case 3: //right
<?
if ($fullscreen_flg != "1") {
	?>
			parent.frames['floatingpage'].document.body.scrollLeft = 900;
<? } else {?>
			window.scrollTo(900,0);
<? } ?>
			break;
		case 4: //down
			var wk_ht = document.getElementById('region').offsetHeight;
			var wk_pos = document.getElementById('region').scrollTop
			document.getElementById('region').scrollTop = wk_pos + wk_ht;
			break;
		}
	}
	//出勤簿印刷（pdf）
	function print_pdf(btn_name) {
		if (!document.mainform.data_exist_flg) {
			alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
			return false;
		}
		window.open("",
			"pdf", "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=1000,height=721");

		// 出勤簿印刷を追加 20130129
		document.csv.action = (btn_name == 'pdf_shift') ? 'atdbk_timecard_shift_pdf.php' : 'work_admin_pdf.php';
		document.csv.wherefrom.value = (btn_name == 'pdf_shift') ? '10' : '7';
		document.csv.target = 'pdf';
		document.csv.submit();
	}
	//打刻エラーリスト
	function timecard_error_list() {
		if (!document.mainform.data_exist_flg) {
			alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
			return false;
		}

		wx = 1100;
		wy = 800;
        dx = screen.width / 2 - (wx / 2);
        dy = screen.top;
        base = screen.height / 2 - (wy / 2);
		
		var url = 'work_admin_timecard_check.php';
		url += '?session=<? echo $session; ?>';
		url += '&yyyymm=<? echo $wk_yyyymm; ?>';
		url += '&emp_id_list=<? echo $staff_ids; ?>';
		url += '&menuname=check_timecard';
		url += '&srch_flg=1';
		url += '&shift_admin_flg=t';

		error_list = window.open(url, 'timecard_error', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
		error_list.focus();
	}

	//(注1)

	//--HTML出力
	function outputLAYER(layName,html){
	if(document.getElementById){        //e5,e6,n6,n7,m1,o7,s1用
	  document.getElementById(layName).innerHTML=html;
	} else if(document.all){            //e4用
	  document.all(layName).innerHTML=html;
	} else if(document.layers) {        //n4用
	   with(document.layers[layName].document){
	     open();
	     write(html);
	     close();
	  }
	}
	}

	//--マウス追跡
	/*==================================================================
	followingLAYER()

	Syntax :
	 追跡レイヤー名 = new followingLAYER('レイヤー名'
	                            ,右方向位置,下方向位置,動作間隔,html)

	 レイヤー名 マウスを追跡させるレイヤー名
	 右方向位置 マウスから右方向へ何ピクセル離すか
	 下方向位置 マウスから下方向へ何ピクセル離すか
	 動作間隔   マウスを追跡する間隔(1/1000秒単位 何秒後に動くか?)
	 html       マウスを追跡するHTML

	------------------------------------------------------------------*/
	/*--/////////////ここから下は触らなくても動きます/////////////--*/

	//--追跡オブジェクト
	//e4,e5,e6,n4,n6,n7,m1,o6,o7,s1用
	function followingLAYER(layName,ofx,ofy,delay,html){
	this.layName = layName;   //マウスを追跡させるレイヤー名
	this.ofx     = ofx;       //マウスから右方向へ何ピクセル離すか
	this.ofy     = ofy;       //マウスから下方向へ何ピクセル離すか
	this.delay   = delay;     //マウスを追跡するタイミング
	if(document.layers)
	  this.div='<layer name="'+layName+'" left="-100" top="-100">\n'	          + html + '</layer>\n';
	else
	  this.div='<div id="'+layName+'"\n'	          +'style="position:absolute;left:-100px;top:-100px">\n'	          + html + '</div>\n';
	document.write(this.div);
	}

	//--メソッドmoveLAYER()を追加する
	followingLAYER.prototype.moveLAYER = moveLAYER; //メソッドを追加する
	function moveLAYER(layName,x,y){
	if(document.getElementById){        //e5,e6,n6,n7,m1,o6,o7,s1用
	    document.getElementById(layName).style.left = x;
	    document.getElementById(layName).style.top  = y;
	} else if(document.all){            //e4用
	    document.all(layName).style.pixelLeft = x;
	    document.all(layName).style.pixelTop  = y;
	} else if(document.layers)          //n4用
	    document.layers[layName].moveTo(x,y);
	}

	//--Eventをセットする(マウスを動かすとdofollow()を実行します)
	document.onmousemove = dofollow;
	//--n4マウスムーブイベント走査開始
	if(document.layers)document.captureEvents(Event.MOUSEMOVE);
	//--oの全画面のEventを拾えないことへの対策
	if(window.opera){
	op_dmydoc ='<div id="dmy" style="position:absolute;z-index:0'	          +'                     left:100%;top:100%"></div> ';
	document.write(op_dmydoc);
	}

	//--イベント発生時にマウス追跡実行
	function dofollow(e){
	for(var i=0 ; i < a.length ; i++ )
	  setTimeout("a["+i+"].moveLAYER(a["+i+"].layName,"	         +(getMouseX(e)+a[i].ofx)+","+(getMouseY(e)+a[i].ofy)+")"	    ,a[i].delay);
	}

	//--マウスX座標get
	function getMouseX(e){
	if(navigator.userAgent.search(
	         "Opera(\ |\/)6") != -1 )   //o6用
	    return e.clientX;
	else if(document.all)               //e4,e5,e6用
	    return document.body.scrollLeft+event.clientX;
	else if(document.layers ||
	        document.getElementById)    //n4,n6,n7,m1,o7,s1用
	    return e.pageX;
	}

	//--マウスY座標get
	function getMouseY(e){
	if(navigator.userAgent.search(
	         "Opera(\ |\/)6") != -1 )   //o6用
	    return e.clientY;
	else if(document.all)               //e4,e5,e6用
	    return document.body.scrollTop+event.clientY;
	else if(document.layers ||
	        document.getElementById)    //n4,n6,n7,m1,o7,s1用
	    return e.pageY;
	}

	/*////////////////////////////// マウスを追跡するレイヤーここまで */

	//テンプレートを作成しoutputLAYERへ渡す
	function showMsg(msg1){
		outputLAYER('test0',msg1);
	}

	//レイヤーの中身を消す
	function hideMsg(){
		var msg1 ='';
		outputLAYER('test0',msg1);
	}

	//レイヤーの数だけa[i]=…部分を増減して使ってください
	var a = new Array();
	a[0]  = new followingLAYER('test0',20,10,100,'');
//	a[1]  = new followingLAYER('test1',20,10,200,'');
//	a[2]  = new followingLAYER('test2',20,10,300,'');
</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/font.css">
<link rel="stylesheet" type="text/css" href="css/shift/shift.css">
<link rel="stylesheet" type="text/css" href="css/shift/results.css">
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="start_auto_session_update();">

<form name="session_update_form" action="session_update.php" method="post" target="session_update_frame">
<input type="hidden" name="session" value="<?=$session?>">
</form>
<iframe name="session_update_frame" width="0" height="0" frameborder="0"></iframe>

	<!-- ------------------------------------------------------------------------ -->
	<!-- 画面遷移／タブ -->
	<!-- ------------------------------------------------------------------------ -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
	<?
		// 画面遷移
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		show_user_title($session, $section_admin_auth);			//duty_shift_common.ini
		echo("</table>\n");

		// タブ
		$arr_option = "&group_id=" . $group_id;
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		show_user_tab_menuitem($session, $fname, $arr_option);	//duty_shift_user_tab_common.ini
		echo("</table>\n");

		// 下線
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td bgcolor=\"#5279a5\"><img src=\"img/spacer.gif\" width=\"1\" height=\"2\" alt=\"\"></td></tr></table><img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
	?>
	</td></tr></table>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 画面遷移／タブ -->
	<!-- ------------------------------------------------------------------------ -->
	<?
		if (($err_msg_1 != "") || ($err_msg_2 != "")) {
			echo($err_msg_1);
			if ($err_msg_1 != "") { echo("<br>\n"); }
			echo($err_msg_2);
			echo("<br>\n");
			echo("<form name=\"mainform\" method=\"post\">\n");
			echo("<table id=\"header\" class=\"list\"></table>\n");
			echo("<div id=\"region\">\n");
			echo("<table id=\"data\" class=\"list\"></table>\n");
			echo("</div>\n");
			echo("<table id=\"error\" class=\"list\"></table>\n");
			echo("<table id=\"summary\" class=\"list\"></table>\n");
			echo("</form>");
		} else {
	?>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 一覧表 -->
	<!-- ------------------------------------------------------------------------ -->
	<form name="mainform" method="post">
		<!-- ------------------------------------------------------------------------ -->
		<!-- ＨＩＤＤＥＮ -->
		<!-- ------------------------------------------------------------------------ -->
		<?
			echo($obj_menu->showHidden_1($data_cnt,
										$day_cnt,
										$edit_start_day,
										$edit_end_day,
										count($title_gyo_array)+2));
		    echo("<input type=\"hidden\" name=\"regist_first_flg\" value=\"1\">\n");    //登録ボタン初回フラグ
		?>
		<!-- ------------------------------------------------------------------------ -->
		<!--  シフトグループ（病棟）名 、年、月 -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<?
			$url = "duty_shift_results.php";
			$url_option = "";
			$wk_group_show_flg = "";				//シフトグループ表示フラグ（１：表示）
			echo($obj_menu->showHead($session, $fname, $wk_group_show_flg, $group_id,
									$group_array,
									$duty_yyyy, $duty_mm, $url, $url_option, "", "", $week_index));
		?>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 各種ボタン -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="100%" border="0" cellspacing="0" cellpadding="2">
		<?
			echo($obj_menu->showButtonRslt($create_flg
											,$edit_start_day
											,$edit_end_day
											,$stamp_flg
											,$session
											,$group_id
                                            ,$disp_tosYotei_flg));

		?>
		</table>


<?//■スタンプエリアの生成開始?>
<img src="./images/imgm.gif" id="flagimg" name="img" style="position:absolute;display:none;" width="15" height="15" />
<input type="hidden" name="listPosition" id="listPosition">
<input type="hidden" name="listPositionPlanHope" id="listPositionPlanHope">

<div id="floatWindowStp" style="position:absolute;">
<a href="" class="close" onclick="floatWindowHide(0,'Stp');setStampFlg();"><img src="./images/close.gif" alt="閉じる" /></a>
<dl id="dl_floatWindowStp">
<dt></dt>
<dd>
<table border="0" cellspacing="0" cellpadding="0" frame="void">
<tr>
<td>
	<?
	//スタンプの指定無しの場合、シフト記号登録の先頭のパターンを取得
	for ($k=0;$k<count($data_pattern_all);$k++) {
		if ($pattern_id == $data_pattern_all[$k]["pattern_id"] &&
				(($stamp_ptn != "" && $data_pattern_all[$k]["atdptn_ptn_id"] == $stamp_ptn) ||
					($stamp_ptn == "" && $data_pattern_all[$k]["font_name"] != "")) &&
				$data_pattern_all[$k]["stamp_disp_flg"] != "f") { //スタンプ非表示フラグ確認 20120306
			$wk_ptn_name = $data_pattern_all[$k]["font_name"];
			$wk_font_color = $data_pattern_all[$k]["font_color_id"];
			$wk_back_color = $data_pattern_all[$k]["back_color_id"];
			break;
		}
	}
?>
        <? $stamp_tbl_disp = ($stamp_flg == "1") ? "" : "none"; ?>
          <table border="0" cellspacing="0" cellpadding="1" class="list">
            <tr>
              <input type="hidden" name="stmpPosition" value="" id="stmpPosition">
              <td width="50" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連続</font>
              </td>
            </tr>
            <tr>
<!--              <td align="center" style="background-color:#ffffff;">-->
              <td align="center" style="background-color:#ffffff;">
                <select name="continuous_cnt" id="continuous_cnt">
	<?
	for ($c_idx=1; $c_idx<=31; $c_idx++) {
		echo("<option value=\"$c_idx\">$c_idx</option>\n");
	}
        ?>
                </select>
              </td>
            </tr>


            <tr>
              <td id="stamp_cel" bgcolor="<?echo($wk_back_color);?>" align="center">
		        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="<?echo($wk_font_color);?>"><?echo($wk_ptn_name);?></font>
		      </td>
            </tr>


        </table>
      </td>

      <td width="5">
        <table>
        </table>
      </td>

      <td>
        <table border="0" cellspacing="0" cellpadding="1" class="list" style="margin-left : auto ; margin-right : auto ; text-align : left ;">
          <tr valign="middle">
	<?

	$wdpa = $data_pattern_all;
	$data_ptn_cnt = count($data_pattern_all);
	$wdpa[$data_ptn_cnt]["pattern_id"] = $pattern_id;
	$wdpa[$data_ptn_cnt]["atdptn_ptn_id"] = "";
	$wdpa[$data_ptn_cnt]["reason"] = "";
	$wdpa[$data_ptn_cnt]["font_name"] = "未設";
	$wdpa[$data_ptn_cnt]["atdptn_ptn_name"] = "未設定";

	$col_max = 15;
	$wk_cnt = 0;
	for ($k=0;$k<count($wdpa);$k++) {
		if ($pattern_id == $wdpa[$k]["pattern_id"] &&
				$wdpa[$k]["font_name"] != "" &&
				$wdpa[$k]["stamp_disp_flg"] != "f") { //スタンプ非表示フラグ確認 20120306

			if ($wk_cnt % $col_max == 0 && $wk_cnt > 0) {
				echo("</tr><tr>\n");
			}
			$wk_atdptn_ptn_id = $wdpa[$k]["atdptn_ptn_id"];
			$wk_id = ($wk_atdptn_ptn_id != "10") ? $wk_atdptn_ptn_id : $wk_atdptn_ptn_id."_".$wdpa[$k]["reason"];

			if (!$wdpa[$k]["back_color_id"]){
				$style = "";
			}else{
				$style = " style=\"background-color:".$wdpa[$k]["back_color_id"]."\"";
			}
			echo("<td class=\"td03\" id=\"stamp_$wk_id\"".$style." onClick=\"setStampPtn('");
			echo($wdpa[$k]["atdptn_ptn_id"]);
			echo("','");
			echo($wdpa[$k]["reason"]);
			echo("','");
			echo("stamp_$wk_id");
			echo("');\">");

			if (!$wdpa[$k]["reason_name"]){
				echo("<div style=\"color: ".$wdpa[$k]["font_color_id"]."\" title=\"" . $wdpa[$k]["atdptn_ptn_name"] . "\">");
			}else{
				echo("<div style=\"color: ".$wdpa[$k]["font_color_id"]."\" title=\"" . $wdpa[$k]["atdptn_ptn_name"] ."/".$wdpa[$k]["reason_name"]. "\">");
			}
			$wk_font_name = h($wdpa[$k]["font_name"]);
			if (strpos($wk_font_name, "&") === false) {
				$wk_font_name = mb_substr($wk_font_name,0,3);
			}
			echo($wk_font_name);
			echo("</div>");
			echo("</td>\n");

			$wk_cnt++;
		}
	}

?>
        </table>
       </td>
      </tr>
     </table>
    </td>
   </tr>
  </table>
</dd>
</dl>
</div>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 見出し -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="250" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;" class="non_list">
		<?
			//週		使用停止 20130307
// 			echo($obj_menu->showTitleSub1($duty_yyyy, $duty_mm, $day_cnt, $edit_start_day, $edit_end_day,
// 									$week_array, $title_gyo_array, $check_flg, "", $wk_group));
		?>
		</table>
		<table width="250" id="header" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;" class="list">
		<?
        //日・曜日
        echo($obj_menu->showTitleSub2(
                $duty_yyyy, $duty_mm,
                $day_cnt,
                $edit_start_day,
                $edit_end_day,
                $week_array,
                &$title_gyo_array, //行合計タイトル情報 集計列幅対応 20140502
                $check_flg,
                $calendar_array,
                "",
                "",
                "",
                array(),
                "",
                "3",
                $paid_hol_disp_flg,
                $wk_group,
				"",
                $extra_day,
                $unit_name,					//ユニット（３階層）名
                $gyo_array, //行合計情報 集計列幅対応 20140502     
                ""          //勤務シフト希望フラグ（１:勤務シフト希望）
        ));
		?>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 勤務シフトデータ（職員（列）年月日（行） -->
		<!-- ------------------------------------------------------------------------ -->
		<div id="region" style="overflow-x:hidden; overflow-y:scroll; overflow:auto; border:#5279a5 solid 0px;">
		<table width="250" id="data" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;" class="list">
		<?
	//divのstyleからcursor:pointer; を除く 20110722
	$wk_emp_id = "";
        $obj_menu->showList(
                $plan_array,
                $data_pattern,
                $warning_gyo_array,
                $plan_results_flg,
                $plan_hope_flg,
                $plan_duty_flg,
                $plan_comment_flg,
                $day_cnt,
                $edit_start_day,
                $edit_end_day,
                $week_array,
                $title_gyo_array,
                $gyo_array,
                $check_show_flg,
                $wk_emp_id,
                $staff_del_flg,
                0,
                $data_cnt,
                $highlight_flg,
                $group_id,
                $pattern_id,
                "",
                "",
                "",
                0,
                $calendar_array,
                "",
                $title_hol_array,
                $hol_cnt_array,
                $paid_hol_all_array,
                $reason_setting_flg,
                $wk_plan_copy_flg,
                $wk_group,
                $plan_individual,
                $paid_hol_disp_flg,
				"",
				$extra_day
        ); //予定コピー不具合対応 20100125
$arr_pos_value[] = "showList 表示処理";
$arr_time_value[] = time();
		?>
		</table>
		</div>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 列計 -->
		<!-- ------------------------------------------------------------------------ -->
		<div id="outCountWindow">
        <div id="outCountWindowBar"></div>
        <div id="countWindow">
		<table width="250" id="summary" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;" class="list">
		<?
		$paid_hol_array[] = $paid_hol_all_array["plan"];      //1段目の合計
		$paid_hol_array[] = $paid_hol_all_array["result"];    //2段目の合計
		echo($obj_menu->showTitleRetu(
		        $day_cnt,
		        $edit_start_day,
		        $edit_end_day,
		        $week_array,
		        $title_gyo_array,
		        $gyo_array,
		        $title_retu_array,
		        $retu_array,
		        $check_flg,
		        $data_cnt,
		        0,
		        "2",
		        "",
		        "",
		        $title_hol_array,
		        $hol_cnt_array,
		        "",
		        $wk_group,
		        $calendar_array,
		        $paid_hol_disp_flg,
		        $paid_hol_array
		));
		?>
		</table>
		</div>
		</div>

		<!-- ------------------------------------------------------------------------ -->
		<!-- ＨＩＤＤＥＮ -->
		<!-- ------------------------------------------------------------------------ -->
		<input type="hidden" name="plan_copy_flg" value="<?=$plan_copy_flg?>">
		<?
			echo($obj_menu->showHidden_2($rslt_flg,
										$plan_array, $calendar_array,
										$session,
										$day_cnt,
										$plan_results_flg,
										$plan_hope_flg,
										$plan_duty_flg,
										$plan_comment_flg,
										$group_id,
										$duty_yyyy, $duty_mm,
										$edit_start_day, $edit_end_day,
										$add_staff_cnt, $add_staff_id, $del_staff_id,
										$pattern_id,
										$draft_flg,
										$check_flg,
										"",
										$reason_setting_flg));

			echo("<input type=\"hidden\" name=\"create_flg\" value=\"$create_flg\">\n");	//登録可能フラグ（１：可能）
			//更新時用情報 20090611
			for ($i=0; $i<$data_cnt; $i++) {
				echo("<input type=\"hidden\" name=\"rslt_upd_flg[$i]\" value=\"{$rslt_upd_flg[$i]}\">\n");
			}
			//登録子画面の指定情報
			echo("<input type=\"hidden\" name=\"last_ptn_id\" value=\"\">\n");
			echo("<input type=\"hidden\" name=\"last_reason_2\" value=\"\">\n");
			//選択された職員の位置。スクロール調整用
			echo("<input type=\"hidden\" name=\"emp_idx\" value=\"\">\n");
			//スタンプ有効 1:有効
			echo("<input type=\"hidden\" name=\"stamp_flg\" value=\"$stamp_flg\">\n");
			echo("<input type=\"hidden\" name=\"stamp_ptn\" value=\"$stamp_ptn\">\n");
			echo("<input type=\"hidden\" name=\"stamp_reason\" value=\"$stamp_reason\">\n");
			//再計算フラグ 1:再計算 20100202
			echo("<input type=\"hidden\" name=\"stamp_comp_flg\" value=\"\">\n");
			//編集フラグ 1:編集 20100216
			echo("<input type=\"hidden\" name=\"edit_flg\" value=\"\">\n");
			//登録時、データがそろっていることの確認用
			echo("<input type=\"hidden\" name=\"data_exist_flg\" value=\"1\">\n");

			//-------------------------------------------------------------------------------
			// PHP Excel 5.1.6対応 20130124
			//-------------------------------------------------------------------------------
			echo("<input type=\"hidden\" name=\"excel_ratio\" 	   value=\"$excel_ratio\">\n");
			echo("<input type=\"hidden\" name=\"excel_paper_size\" value=\"$excel_paper_size\">\n");
			echo("<input type=\"hidden\" name=\"excel_setfitpage\" value=\"$excel_setfitpage\">\n");
			echo("<input type=\"hidden\" name=\"excel_mtop\"	   value=\"$excel_mtop\">\n");
			echo("<input type=\"hidden\" name=\"excel_mbottom\"	   value=\"$excel_mbottom\">\n");
			echo("<input type=\"hidden\" name=\"excel_mleft\"	   value=\"$excel_mleft\">\n");
			echo("<input type=\"hidden\" name=\"excel_mright\"	   value=\"$excel_mright\">\n");
			echo("<input type=\"hidden\" name=\"excel_mhead\"	   value=\"$excel_mhead\">\n");
			echo("<input type=\"hidden\" name=\"excel_mfoot\"	   value=\"$excel_mfoot\">\n");
			echo("<input type=\"hidden\" name=\"excel_row_count\"  value=\"$excel_row_count\">\n");
			echo("<input type=\"hidden\" name=\"excel_row_type\"   value=\"$excel_row_type\">\n");
			echo("<input type=\"hidden\" name=\"excel_blancrows\">\n");
			echo("<input type=\"hidden\" name=\"excel_date\">\n");
			echo("<input type=\"hidden\" name=\"excel_weekcolor\">\n");
			echo("<input type=\"hidden\" name=\"excel_hopecolor\">\n");
			echo("<input type=\"hidden\" name=\"excel_yearmonth\">\n");// 20130219 追加
            echo("<input type=\"hidden\" name=\"excel_week4start\" 	   value=\"\">\n");// 20140507 追加
            echo("<input type=\"hidden\" name=\"extra_day\" value=\"$extra_day\">\n");
            echo("<input type=\"hidden\" name=\"excel_event\"          value=\"\">\n");// 20150709 追加

            //繰越公休残日数 start
            echo("<input type=\"hidden\" name=\"legal_hol_idx\" value=\"$legal_hol_idx\">\n");
            //繰越公休残日数 end
		?>
	</form>
	<form name="csv" method="post" target="download">
	<input type="hidden" name="session" value="<? echo($session); ?>">
	<input type="hidden" name="emp_id_list" value="<? echo($staff_ids); ?>">
	<input type="hidden" name="yyyymm" value="<? echo($wk_yyyymm); ?>">
	<input type="hidden" name="wherefrom" value="">
	</form>
	<iframe name="download" width="0" height="0" frameborder="0"></iframe>
    <?
		}
echo("<!-- \n");
for ($i=0; $i<count($arr_pos_value); $i++) {
	echo date("H:i:s", $arr_time_value[$i]);
	if ($i>0) {
		echo " ";
		echo $arr_time_value[$i] - $arr_time_value[$i-1];
		echo "秒";
	}
	echo " ";
	echo $arr_pos_value[$i];

	echo "\n";
}
echo("-->");
	?>


<? pg_close($con); ?>

<script type="text/javascript">
<? //シフトグループ変更後と表示中に無効化、表示後有効化 20111221 ?>
	var group_id = document.getElementById('group_id');
	var duty_yyyy = document.getElementById('duty_yyyy');
    if (group_id) group_id.disabled = false;
    if (duty_yyyy) duty_yyyy.disabled = false;
<?
//繰越公休残日数 start
if ($hol_remain_open_flg && $warning_flg == "") {
?>
    alert('前月分の繰越公休残日数調整を行ってください。');
    hol_remain(2);
    <?
}
//繰越公休残日数 end
?>

function switch_yotei() {

	document.mainform.action = "duty_shift_results.php?session=<?=$session?>&group_id=<?=$group_id?>&yotei=<?=$disp_tosYotei_flg?>";
    document.mainform.target = "";
	document.mainform.submit();

}
</script>
<?
//下書き状態時メッセージ対応 20150306
if ($draft_reg_flg == "1" && $origin_start_date <= date("Ymd")) {
?>
<script language="JavaScript">
alert('勤務予定が下書き状態になっています。\nシフト作成画面で登録ボタンを押して、登録状態にして下さい。');
</script>
<?
}
?>
</body>

</html>