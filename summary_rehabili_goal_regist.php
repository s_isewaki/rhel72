<?
ob_start();
$fname = $_SERVER["PHP_SELF"];
require_once("about_comedix.php");
require_once("get_values.php");
require_once("show_sinryo_top_header.ini");
require_once("label_by_profile_type.ini");
require_once("sot_util.php");
require_once("summary_common.ini");
ob_end_clean();

$session = qualify_session($session,$fname);
$fplusadm_auth = check_authority($session, 59, $fname);
$con = @connect2db($fname);
if(!$session || !$fplusadm_auth || !$con){
    echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
    exit;
}

$mode = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : '';
$therapy_type = isset($_REQUEST["type_dropdown"]) ? $_REQUEST["type_dropdown"] : '';
$caption = isset($_REQUEST["rehabili_goal_caption"]) ? $_REQUEST["rehabili_goal_caption"] : '';
$caption = trim(mb_convert_kana($caption, "s", "EUC-JP"));

//==========================================================================
// 新規登録、登録後は一覧表へ
//==========================================================================
if ($mode == "regist") {
    $sel = select_from_table($con, "select max(code) as sort_order from sum_med_rehabili_goal_mst where therapy_type = '$therapy_type'", "", $fname);
    $code = ((int)pg_fetch_result($sel, 0, 0)) + 1;

    $sql = "select max(sort_order) as sort_order from sum_med_rehabili_goal_mst where therapy_type = '$therapy_type'";
    $sel = select_from_table($con, $sql, "", $fname);
    $sort_order = ((int)pg_fetch_result($sel, 0, 0)) + 1;

    $sql  = " insert into sum_med_rehabili_goal_mst (";
    $sql .= " therapy_type";
    $sql .= ",code";
    $sql .= ",sort_order";
    $sql .= ",is_disabled";
    $sql .= ",caption";
    $sql .= " ) values (";
    $sql .= " '$therapy_type'";
    $sql .= ",$code";
    $sql .= ",$sort_order";
    $sql .= ",0";
    $sql .= ",'". pg_escape_string($caption)."'";
    $sql .= ")";
    $upd = update_set_table($con, $sql, array(), null, "", $fname);
    header("Location: summary_rehabili_goal_list.php?session=".$session);
    die;
}


//==========================================================================
// 以下、通常処理
//==========================================================================
$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';


// 診療科権限を取得
$entity_admin_auth = check_authority($session, 28, $fname);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];
// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = $_label_by_profile["MED_REPORT"][$profile_type];
// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <? echo($med_report_title); ?>管理 | マスタ管理</title>

<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list1 {border-collapse:collapse;}
.list1 td { border:#5279a5 solid 1px; text-align:left; white-space:nowrap; background-color:#eee }
.list1 th {  border:#5279a5 solid 1px; text-align:left; font-weight:normal; white-space:nowrap; background-color:#e3ecf4; padding:3px }

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

<? show_sinryo_top_header($session, $fname, "KANRI_MST"); ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">
<? echo summary_common_show_admin_left_menu($session, $entity_admin_auth, $section_title, $summary_kubun_title); ?>
</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->

<!-- right -->
<td valign="top">

<?
//==========================================================================
// マスタ管理メニュー
//==========================================================================
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<? summary_common_show_mst_tab_menu($session, 'リハ目標'); ?>
<td>&nbsp;</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr></table>


<?
//==========================================================================
// サブメニュー
//==========================================================================
?>
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:8px">
    <tr height="22">
        <td width="100" align="center" bgcolor="#e8e4bd">
            <a href="summary_rehabili_goal_list.php?session=<? echo $session; ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>一覧/編集</b></font></a>
        </td>
        <td width="5">&nbsp;</td>
        <td width="100" align="center" bgcolor="#958f28">
            <a href="summary_rehabili_goal_regist.php?session=<? echo $session; ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>新規登録</b></font></a>
        </td>
        <td width="5">&nbsp;</td>
        <td></td>
    </tr>
</table>


<?
//==========================================================================
// 入力領域
//==========================================================================
?>
<script type="text/javascript">
    function addErr(msg) { errmsg[errmsg.length] = msg; }
    errmsg = [];
    function checkAndSubmit() {
        errmsg = [];
        var type_dropdown = document.getElementById("type_dropdown").selectedIndex;
        var tc = document.getElementById("rehabili_goal_caption").value;

        if (type_dropdown == "") {
            addErr("療法を選択してください。");
        }
        
        if (tc == "") {
            addErr("表記文字を指定してください。");
        }

        if (errmsg.length){
            alert(errmsg.join("\n"));
            return;
        }
        document.frm.mode.value = "regist";
        document.frm.submit();
    }
</script>

<form action="summary_rehabili_goal_regist.php" method="get" name="frm" style="margin-top:10px">
    <input type="hidden" name="session" value="<? echo $session; ?>">
    <input type="hidden" name="mode" value="">

    <table border="0" cellspacing="0" cellpadding="2" style="margin-top:10px; border-collapse:collapse;" class="list1">
        <tr>
            <th><? echo $font; ?>療法</font></th>
            <td>
                <div id="bunrui1_dropdown_div"><? echo $font; ?>
                    <select name="type_dropdown" id="type_dropdown">
                        <option value=""></option>
                        <?
                            $therapy_types = array('PT' => '理学療法', 'OT' => '作業療法', 'ST' => '言語聴覚療法', 'PST' => '心理療法');
                            foreach ($therapy_types as $type => $caption) {
                                echo "<option value=\"{$type}\">{$caption}：{$type}</option>\n";
                            }
                        ?>
                    </select>
                </font></div>
            </td>
        </tr>
        <tr>
            <th><? echo $font; ?>表記文字</font></th>
            <td><input type="text" name="rehabili_goal_caption" id="rehabili_goal_caption" value="<? echo h(@$_REQUEST["rehabili_goal_caption"]); ?>" style="width:500px" maxlength="80" autocomplete="off"></td>
        </tr>
    </table>

    <div style="width:400px; text-align:right; margin-top:30px">
        <input type="button" onclick="checkAndSubmit();" value="登録">
    </div>
</form>




</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
