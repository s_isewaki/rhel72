<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("cas_application_workflow_common_class.php");
require_once("cas_common.ini");
require_once("get_cas_title_name.ini");


//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new cas_application_workflow_common_class($con, $fname);


//====================================
// 登録更新処理
//====================================
if($mode == "REGIST")
{
	// 削除処理
    $obj->delete_cas_achievement_period($emp_id);

    // 登録処理
    for($i=1; $i<=$line_cnt; $i++)
    {
        $s_date_y = "start_date_y".$i;
        $s_date_m = "start_date_m".$i;
        $e_date_y = "end_date_y".$i;
        $e_date_m = "end_date_m".$i;
        $approval_emp_ids = "approval_emp_ids_".$i;
        $status = "status".$i;
        $level = "level".$i;

        $arr = array(
                      "emp_id" => $emp_id,
                      "achievement_order" => $i,
                      "start_period" => $$s_date_y.$$s_date_m,
                      "end_period" => $$e_date_y.$$e_date_m,
                      "approval_emp_ids" => $$approval_emp_ids,
                      "status" => $$status,
                      "level" => $$level
                     );
		$obj->regist_cas_achievement_period($arr);
	}

	echo("<script type=\"text/javascript\">");
	echo("if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}");
	echo("window.close();</script>");
	exit;
}

//====================================
// 初期処理
//====================================
$arr_achievement_period = array();
if($mode == "")
{
    $tmp_arr_achievement_period = $obj->get_cas_achievement_period($emp_id, "", "", "");

    foreach($tmp_arr_achievement_period as $achievement_period)
    {
        $arr_approval_emp_ids = split(",", $achievement_period["approval_emp_ids"]);
        $tmp_emp_name = "";
        for($i=0; $i<count($arr_approval_emp_ids); $i++)
        {
            if($tmp_emp_name != "")
            {
                $tmp_emp_name .= ",";
            } 
            $arr_emp = $obj->get_empmst_detail($arr_approval_emp_ids[$i]);
            $tmp_emp_name .= $arr_emp[0]["emp_lt_nm"]." ".$arr_emp[0]["emp_ft_nm"];
        }

        array_push($arr_achievement_period, array(
                                                   "start_period" => $achievement_period["start_period"],
                                                   "end_period" => $achievement_period["end_period"],
                                                   "approval_emp_ids" => $achievement_period["approval_emp_ids"],
                                                   "approval_emp_names" => $tmp_emp_name,
                                                   "status" => $achievement_period["status"],
                                                   "level" => $achievement_period["level"]
                                                  ));
    }

    if(count($arr_achievement_period) == 0)
    {
		$arr_achievement_period[0] = array();
    }

}
else if($mode == "ADD_LINE")
{
	for($i=1; $i<=$line_cnt; $i++)
	{
		$s_date_y = "start_date_y".$i;
		$s_date_m = "start_date_m".$i;
		$e_date_y = "end_date_y".$i;
		$e_date_m = "end_date_m".$i;
        $approval_emp_ids = "approval_emp_ids_".$i;
        $approval_emp_names = "approval_emp_names_".$i;
        $status = "status".$i;
        $level = "level".$i;

		array_push($arr_achievement_period, array(
                                                   "start_period" => $$s_date_y.$$s_date_m,
                                                   "end_period" => $$e_date_y.$$e_date_m,
                                                   "approval_emp_ids" => $$approval_emp_ids,
                                                   "approval_emp_names" => $$approval_emp_names,
                                                   "status" => $$status,
                                                   "level" => $$level
                                                  ));

		if($i == $add_cnt)
		{
            array_push($arr_achievement_period, array("start_period" => "", "end_period" => ""));
		}	
	}
}
else if($mode == "DEL_LINE")
{
	for($i=1; $i<=$line_cnt; $i++)
	{
		if($i == $del_cnt)
		{
            continue;
		}

		$s_date_y = "start_date_y".$i;
		$s_date_m = "start_date_m".$i;
		$e_date_y = "end_date_y".$i;
		$e_date_m = "end_date_m".$i;
        $approval_emp_ids = "approval_emp_ids_".$i;
        $approval_emp_names = "approval_emp_names_".$i;
        $status = "status".$i;
        $level = "level".$i;

		array_push($arr_achievement_period, array(
                                                   "start_period" => $$s_date_y.$$s_date_m,
                                                   "end_period" => $$e_date_y.$$e_date_m,
                                                   "approval_emp_ids" => $$approval_emp_ids,
                                                   "approval_emp_names" => $$approval_emp_names,
                                                   "status" => $$status,
                                                   "level" => $$level                        
                                                  ));
	}
}

$line_cnt = count($arr_achievement_period);

$cas_title = get_cas_title_name();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cas_title?>｜病棟評価・達成期間登録</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<script type="text/javascript">

function initPage()
{
    <?
    $idx = 1;
    foreach($arr_achievement_period as $achievement_period)
    {

        if($achievement_period["approval_emp_ids"] != "")
        {
	        $arr_approval_emp_ids   = split(",", $achievement_period["approval_emp_ids"]);
	        $arr_approval_emp_names = split(",", $achievement_period["approval_emp_names"]);
	
	        $approval_area = "";
	        $approval_ids = "";
	        $approval_names = "";
	        for($i=0; $i<count($arr_approval_emp_ids); $i++)
	        {
	            if($approval_area != "")
			    {
				    $approval_area  .= ",";
				    $approval_ids    .= ",";
				    $approval_names  .= ",";
			    }
	
	            $id = $arr_approval_emp_ids[$i];
	            $name = $arr_approval_emp_names[$i];
			    $approval_area .= "<a href=\"javascript:delete_emp(\\'{$id}\\', \\'{$name}\\', \\'{$idx}\\');\">{$name}</a>";
	
			    $approval_ids   .= $id;
			    $approval_names .= $name;
	        }
    ?>
		document.getElementById('approval_area_<?=$idx?>').innerHTML   = '<?=$approval_area?>';
		document.getElementById('approval_emp_ids_<?=$idx?>').value   = '<?=$approval_ids?>';
		document.getElementById('approval_emp_names_<?=$idx?>').value = '<?=$approval_names?>';
    <?
	    }
        $idx++;
	}
	?>
}



function add_line(add_cnt)
{
    document.apply.action="cas_ward_achivement_regist.php?session=<?=$session?>&mode=ADD_LINE&add_cnt="+ add_cnt;
    document.apply.submit();
}

function del_line(del_cnt)
{
    document.apply.action="cas_ward_achivement_regist.php?session=<?=$session?>&mode=DEL_LINE&del_cnt="+ del_cnt;
    document.apply.submit();
}

function regist_period()
{

    if(!is_null_check())
    {
        alert("日付を入力してください。");
        return;
    }

    if(!day_check())
    {
        alert("日付の前後を正しく入力してください。");
        return;
    }

    // 承認者未入力チェック
<?
    for($i=1; $i<=$line_cnt; $i++)
    {
?>
        approval_emp_ids = document.getElementById('approval_emp_ids_<?=$i?>').value;
        if(approval_emp_ids == "")
        {
            alert("承認者を入力してください。");
            return;
        }
<?
    }
?>
    document.apply.action="cas_ward_achivement_regist.php?session=<?=$session?>&mode=REGIST";
    document.apply.submit();
}

// 未入力チェック
function is_null_check()
{
<?
    for($i=1; $i<=$line_cnt; $i++)
    {
?>
        start_date_y = document.getElementById('start_date_y<?=$i?>').value;
        start_date_m = document.getElementById('start_date_m<?=$i?>').value;
        end_date_y = document.getElementById('end_date_y<?=$i?>').value;
        end_date_m = document.getElementById('end_date_m<?=$i?>').value;

        if(start_date_y == '' || start_date_m == '' || end_date_y == '' || end_date_m == '')
        {
            return false;
        }
<?
    }
?>
        return true;
}

// 日付前後チェック
function day_check()
{
<?
    for($i=1; $i<=$line_cnt; $i++)
    {
?>
        start_date_y = document.getElementById('start_date_y<?=$i?>').value;
        start_date_m = document.getElementById('start_date_m<?=$i?>').value;
        end_date_y   = document.getElementById('end_date_y<?=$i?>').value;
        end_date_m   = document.getElementById('end_date_m<?=$i?>').value;

        start_date = start_date_y + start_date_m;
        end_date = end_date_y + end_date_m;
        if(start_date > end_date)
        {
            return false;
        }	       
<?
    }
?>
        return true;
}




var childwin = null;
function openEmployeeList(input_div) {

	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
	var url = 'cas_emp_list.php';
	url += '?session=<?=$session?>&input_div=' + input_div;
	childwin = window.open(url, 'emplist', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}


//子画面から呼ばれる関数
function add_target_list(input_div, input_emp_id, input_emp_nm)
{

	ids   = "approval_emp_ids_" + input_div;
	names = "approval_emp_names_" + input_div;

	//既存入力されている職員
	emp_ids = document.getElementById(ids).value;
	emp_nms = document.getElementById(names).value;

	//子画面から入力された職員
	arr_input_emp_id   = input_emp_id.split(",");
	arr_input_emp_name = input_emp_nm.split(",");

	tmp_ids   = "";
	tmp_names = "";
	if(emp_ids != "")
	{
		arr_emp_id = emp_ids.split(",");

　		for(i=0; i<arr_input_emp_id.length; i++)
		{
			dpl_flg = false;
			for(j=0; j<arr_emp_id.length; j++)
			{
				// 重複した場合
				if(arr_input_emp_id[i] == arr_emp_id[j])
				{
					dpl_flg = true;
				}
			}
			if(!dpl_flg)
			{
				if(tmp_ids != "")
				{
					tmp_ids   += ",";
					tmp_names += ",";
				}
				tmp_ids   += arr_input_emp_id[i];
				tmp_names += arr_input_emp_name[i];
			}
		}
	}
	else
	{
		for(i=0; i<arr_input_emp_id.length; i++)
		{
			if(tmp_ids != "")
			{
				tmp_ids   += ",";
				tmp_names += ",";
			}
			tmp_ids   += arr_input_emp_id[i];
			tmp_names += arr_input_emp_name[i];
		}
	}

	if(emp_ids != "" && tmp_ids != "")
	{
		emp_ids += ",";
	}
	emp_ids += tmp_ids;
	document.getElementById(ids).value = emp_ids;
	if(emp_nms != ""  && tmp_names != "") {
		emp_nms += ",";
	}
	emp_nms += tmp_names;
	document.getElementById(names).value = emp_nms;

	set_approval_area(emp_ids, emp_nms, input_div);	
}

function set_approval_area(emp_id, emp_nm, input_div) {

	disp_area = "approval_area_" + input_div;

	arr_emp_id = emp_id.split(",");
	arr_emp_nm = emp_nm.split(",");
	disp_area_content = "";
	for(i=0; i<arr_emp_id.length; i++)
	{
		if(i > 0)
		{
			disp_area_content += ',';
		}

		disp_area_content += "<a href=\"javascript:delete_emp('" + arr_emp_id[i] + "', '" + arr_emp_nm[i] + "', '" + input_div + "');\">";
		disp_area_content += arr_emp_nm[i];
		disp_area_content += "</a>";
	}
	document.getElementById(disp_area).innerHTML = disp_area_content;
}

function delete_emp(del_emp_id, del_emp_nm, input_div)
{
	if(confirm("「" + del_emp_nm + "」さんを削除します。よろしいですか？"))
	{
		ids   = "approval_emp_ids_" + input_div;
		names = "approval_emp_names_" + input_div;

		emp_ids = document.getElementById(ids).value;
		emp_nms = document.getElementById(names).value;	

		new_emp_ids = "";
		new_emp_nms = "";

		cnt = 0;
		if(emp_ids != "") {

			arr_emp_id = emp_ids.split(",");
			arr_emp_nm = emp_nms.split(",");

			for(i=0; i<arr_emp_id.length; i++)
			{
				if(arr_emp_id[i] == del_emp_id)
				{
					continue;
				}
				else
				{
					if(cnt > 0)
					{
					    new_emp_ids += ",";
						new_emp_nms += ",";
					}
				    new_emp_ids += arr_emp_id[i];
					new_emp_nms += arr_emp_nm[i];
					cnt++;
				}
			}
		}
		document.getElementById(ids).value = new_emp_ids;
		document.getElementById(names).value = new_emp_nms;
		set_approval_area(new_emp_ids, new_emp_nms, input_div);
	}
}




</script>
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#A0D25A solid 1px;}
.list td td {border-width:0;}

table.block {border-collapse:collapse;}
table.block td {border:#A0D25A solid 1px;padding:1px;}
table.block td td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#A0D25A solid 0px;}

.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initPage();">
<form name="apply" action="#" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="emp_id" value="<?=$emp_id?>">
<input type="hidden" id="line_cnt" name="line_cnt" value="<?=$line_cnt?>">

<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#A0D25A">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>病棟評価・達成期間登録</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#A0D25A"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">

<tr bgcolor="#E5F6CD" align="center">
<td width="40">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">順番</font>
</td>
<td width="150">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象</font>
</td>
<td width="300">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">達成期間</font>
</td>
<td width="350">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">指導者</font>
</td>
<td width="70">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">追加/削除</font>
</td>
</tr>



<?
$count = 1;
foreach($arr_achievement_period as $period)
{
    $start_date_y = substr($period["start_period"], 0, 4);
    $start_date_m = substr($period["start_period"], 4, 2);
    $end_date_y = substr($period["end_period"], 0, 4);
    $end_date_m = substr($period["end_period"], 4, 2);

    $status = $period["status"];
    $level = $period["level"];

?>
<tr height="50">
<td align="center">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$count?></font>
</td>

<td>
<select name="status<?=$count?>" id="status<?=$count?>">
<?show_status_options($status)?>
</select>
<select name="level<?=$count?>" id="level<?=$count?>">
<?show_level_options($level)?>
</select>


<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="start_date_y<?=$count?>" id="start_date_y<?=$count?>">
<? show_select_years_for_cas(3, $start_date_y ,true); ?>
</select>年
<select name="start_date_m<?=$count?>" id="start_date_m<?=$count?>">
<? show_select_months_for_cas($start_date_m ,true); ?>
</select>月
&nbsp;〜&nbsp;
<select name="end_date_y<?=$count?>" id="end_date_y<?=$count?>">
<? show_select_years_for_cas(3, $end_date_y ,true); ?>
</select>年
<select name="end_date_m<?=$count?>" id="end_date_m<?=$count?>">
<? show_select_months_for_cas($end_date_m ,true); ?>
</select>月
</font>
</td>

<td>
<table width="100%" height="100%" border="0" cellspacing="1" cellpadding="0" class="non_in_list">
<tr>
<td width="100%" style="border:#A0D25A solid 1px;">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="approval_area_<?=$count?>"></span></font>
</td>
<td>
<input type="button" name="emplist1" value="職員名簿" style="margin-left:0em;width=5.5em;" onclick="openEmployeeList('<?=$count?>');">
</td>
</tr>
</table>
</td>

<input type="hidden" name="approval_emp_ids_<?=$count?>" id="approval_emp_ids_<?=$count?>" value="">
<input type="hidden" name="approval_emp_names_<?=$count?>" id="approval_emp_names_<?=$count?>" value="">

<td align="center">
<input type="button" value="追加" onclick="add_line('<?=$count?>');"><br>
<input type="button" value="削除" onclick="del_line('<?=$count?>');">
</td>
</tr>

<?
$count++;
}
?>

</table>


</td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="block_in">
<td align="right">
<input type="button" value="登録" onclick="regist_period();">
</td>
</tr>	
</table>
<center>
</body>
</form>
</html>


<?
// 状況オプションを出力
function show_status_options($status) {

	$status_nm = array("急性期","回復期");
	$status_id = array("1","2");

	for($i=0;$i<count($status_nm);$i++) {

		echo("<option value=\"$status_id[$i]\"");
		if($status == $status_id[$i]) {
			echo(" selected");
		}	
		echo(">$status_nm[$i]\n");
	}
}	

// レベルオプションを出力
function show_level_options($level) {

	$level_nm = array("レベルI","レベルII","レベルIII","レベルIV");
	$level_id = array("1","2","3","4");

	for($i=0;$i<count($level_nm);$i++) {

		echo("<option value=\"$level_id[$i]\"");
		if($level == $level_id[$i]) {
			echo(" selected");
		}	
		echo(">$level_nm[$i]\n");
	}
}

pg_close($con);
?>