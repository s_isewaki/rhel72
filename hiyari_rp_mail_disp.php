<?php
require_once('Cmx.php');
require_once('Cmx/View/Smarty.php');
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("about_postgres.php");
require_once("hiyari_common.ini");
require_once("hiyari_mail.ini");
require_once("hiyari_report_class.php");
require_once("hiyari_report_auth_models.php");
require_once("hiyari_db_class.php");
require_once('Cmx/Model/SystemConfig.php');
require_once("hiyari_report_classification.php");


//==============================
//画面名
//==============================
$fname = $PHP_SELF;

$rep_obj = new hiyari_report_class($con, $fname);
//==============================
//セッションのチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//==============================
// 権限チェック
//==============================
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
    showLoginPage();
    exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//==============================
//ユーザーID取得
//==============================
$emp_id = get_emp_id($con,$session,$fname);

//==============================
//様式ID取得
//==============================
$eis_id = get_eis_id_from_mail_id($con,$fname,$mail_id);
$sel_job = get_job_id_from_mail_id($con,$fname,$mail_id);

//==================================================
//オプション
//==================================================
$sysConf = new Cmx_SystemConfig();
//デザイン（1=旧デザイン、2=新デザイン）
$design_mode = $sysConf->get('fantol.design.mode');

//==============================
// 画面遷移パラメータ取得
//==============================
$mail_id = $_GET['mail_id'];
$mail_id_mode = $_GET['mail_id_mode'];

//====================================
//件名・差出人・日付・あて先情報取得
//====================================
$where = "where mail_id = '$mail_id'";
$order = "order by recv_class_disp_index asc ";

$fields = array(
        'report_id',
        'registrant_id',
        'registrant_class',
        'registrant_attribute',
        'registrant_dept',
        'registrant_room',
        'registration_date',
        'mail_id',
        'subject',
        'send_emp_name',
        'send_date',
        'send_eis_id',
        'anonymous_send_flg',
        'marker',
        'send_emp_class',
        'send_emp_attribute',
        'send_emp_dept',
        'send_emp_room',
        'recv_emp_name',
        'recv_class',
        'recv_class_disp_index',
        'anonymous_recv_flg',
        'message',
        'original_mail_id'
    );

$recv_info_arr = get_recv_mail_list($con,$fname,$where,$order,$fields);
if(count($recv_info_arr) < 1) {
    //パラメータ異常
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

$subject = "";              // 件名
$send_emp_name = "";        // 送信者
$send_emp_class_name = "";  // 送信者部署
$send_date = "";            // 送信日
$recv_name = "";            // あて先
$report_id = "";            // レポートID
$send_eis_id = "";          // 送信時の様式ID
$registration_date = "";    // 登録日
$to_recv_emp_name_arr = array();
$cc_recv_emp_name_arr = array();


        
$cnt = 0;
foreach($recv_info_arr as $list_data) {

    if($cnt == 0) {
        $subject = $list_data["subject"];

        $anonymous_send_flg = $list_data["anonymous_send_flg"];
        if($anonymous_send_flg == "t")
        {
            $send_emp_name = get_anonymous_name($con,$fname);
        }
        else
        {
            $send_emp_name = $list_data["send_emp_name"];
        }

        $send_date = $list_data["send_date"];
        $report_id = $list_data["report_id"];
        $send_eis_id = $list_data["send_eis_id"];

        
        $registration_date = $list_data["registration_date"];
     
        
        $marker_style = get_style_by_marker($list_data["marker"]);

        //送信者部署
/*
        $send_emp_class = $list_data["send_emp_class"];
        $send_emp_attribute = $list_data["send_emp_attribute"];
        $send_emp_dept = $list_data["send_emp_dept"];
        $send_emp_room = $list_data["send_emp_room"];
*/
		$send_emp_class = $list_data["registrant_class"];
		$send_emp_attribute = $list_data["registrant_attribute"];
		$send_emp_dept = $list_data["registrant_dept"];
		$send_emp_room = $list_data["registrant_room"];

        $profile_use = get_inci_profile($fname, $con);
        if($profile_use["class_flg"] == "t" && $send_emp_class != "")
        {
            $send_emp_class_name .= get_class_nm($con,$send_emp_class,$fname);
        }
        if($profile_use["attribute_flg"] == "t" && $send_emp_attribute != "")
        {
            $send_emp_class_name .= get_atrb_nm($con,$send_emp_attribute,$fname);
        }
        if($profile_use["dept_flg"] == "t" && $send_emp_dept != "")
        {
            $send_emp_class_name .= get_dept_nm($con,$send_emp_dept,$fname);
        }
        if($profile_use["room_flg"] == "t" && $send_emp_room != "")
        {
            $send_emp_class_name .= get_room_nm($con,$send_emp_room,$fname);
        }
        //20130630
        //==================================================
        //ファイル添付
        //==================================================
        //添付ファイル情報を取得
        $ret_file = $rep_obj->get_inci_file($report_id);



    }

    $recv_class = $list_data["recv_class"];

    $anonymous_recv_flg = $list_data["anonymous_recv_flg"];
    if($anonymous_recv_flg == "t")
    {
        $recv_emp_name = get_anonymous_name($con,$fname);
    }
    else
    {
        $recv_emp_name = $list_data["recv_emp_name"];
    }

    if($recv_class == "TO") {
        array_push($to_recv_emp_name_arr, $recv_emp_name);
    } else if($recv_class == "CC") {
        array_push($cc_recv_emp_name_arr, $recv_emp_name);
    }

    $cnt++;
}

foreach($to_recv_emp_name_arr as $i => $value) {


    if ($i > 0) {
        $recv_name .= " , ";
    }
    $recv_name .= $to_recv_emp_name_arr[$i];
}

foreach($cc_recv_emp_name_arr as $i => $value) {

    if ($i == 0) {
        $recv_name .= " ";
        $recv_name .= "【CC】";
    } else {
        $recv_name .= " , ";
    }
    $recv_name .= $cc_recv_emp_name_arr[$i];
}

//==============================
// ログインユーザのレポート権限取得 (※審議者は権限が無い。)
//==============================
$arr_report_auth = $rep_obj->get_my_inci_auth_list_for_report_progress($report_id, $emp_id);
$sel_eis_no = $rep_obj->get_eis_no_from_eis_id($eis_id);


// GRM(SM)フラグ
$is_sm_emp_flg =  is_sm_emp($session, $fname, $con);

//

//==============================
//編集・進捗登録ボタン表示チェック
//レポート受付・確認日付・更新可能チェック
//==============================
if($rep_obj->is_deleted_report($report_id))
{
    //論理削除済みの場合は非表示(編集は参照のみ)
    $report_edit_flg = is_mail_report_editable($session,$fname,$con);
    $progres_edit_flg = false;
    $report_progress_updatable_flg = false;
    $label_edit_flg = false;
    $report_progress_use_flg = false;
}
else
{
    $report_edit_flg = is_mail_report_editable($session,$fname,$con);
    $progres_edit_flg = is_progres_editable($session,$fname,$con);

    $report_progress_updatable_flg = false;
    if(count($arr_report_auth) > 0)
    {
       for ( $i = 0; $i < count($arr_report_auth); $i++) {
           $confirm = 'fantol.progress.'.$arr_report_auth[$i][auth].'.confirm.flg';
           $confirm = mb_strtolower($confirm);
           $progress_conform = $sysConf->get($confirm);
           if( $sysConf->get($confirm) == 't'){
               $report_progress_updatable_flg = $rep_obj->is_report_progress_detail_kakunin_updatable($report_id,$emp_id);
           }else{
               $report_progress_updatable_flg = $rep_obj->is_report_progress_toray_kakunin_updatable($report_id, $arr_report_auth[$i][auth]);
           }
           
       }
               

        
    }

    $label_edit_flg = true;
}

$label_use_flg = is_report_db_usable($session,$fname,$con);

if(count($arr_report_auth) == 0)
{
    $progress_comment_use_flg = false;
}
else
{
    $progress_comment_use_flg = get_progress_comment_use($con,$fname);
}


//受信トレイにある他の部署の報告書を評価可能にするフラグを取得
//$is_progress_eval = get_progress_eval($con, $fname);
$is_progress_eval = $sysConf->get('fantol.evaluation.common.use.flg');


if($is_progress_eval=='t'){
//評価機能利用フラグ 20140421 R.C
    $emp_auth = get_emp_priority_auth($session,$fname,$con);
    $is_evaluation_use = $sysConf->get('fantol.evaluation.use.' . strtolower($emp_auth) . '.flg') == 't';
}else{
//====================================
// 評価機能利用フラグ　20140421 R.C
//====================================
    foreach ($arr_report_auth as $auth_item){
	$emp_auth = $auth_item["auth"];
	$auth_flag = $sysConf->get('fantol.evaluation.use.' . strtolower($emp_auth) . '.flg') == 't';

	if ($auth_flag){
		$is_evaluation_use = $auth_flag;
		break;
	}
    }
}
//==============================
// トレイの更新権限取得
//==============================
$report_torey_update_flg = getToreyUpdateAuth($con, $session, $fname, $report_id);

//==============================
// トランザクションの開始
//==============================
pg_query($con, "begin transaction");

//==============================
// 既読・更新処理
//==============================
update_read_flg($con,$fname,$mail_id,$emp_id, "t");

//==============================
// レポート進捗・受付日付更新処理
//==============================
if($arr_report_auth)
{
    for($i = 0; $i < count($arr_report_auth); $i++)
    {
        $auth = $arr_report_auth[$i]["auth"];
        $rep_obj->set_report_progress_start_date($report_id, $auth, $emp_id,$mail_id);
    }
}

//==============================
// レポート進捗・確認日付更新処理
//==============================
if ($mode == "update") {
    if ($arr_report_auth) {
        for($i = 0; $i < count($arr_report_auth); $i++) {
            $auth = $arr_report_auth[$i]["auth"];
            $rep_obj->set_report_progress_end_date($report_id, $auth, $emp_id,$mail_id);
        }
    }

	$report_progress_updatable_flg = false;

	if ( $is_evaluation_use == true ) {
		//評価画面へ移動
        redirectPage("hiyari_easyinput.php?session={$session}&gamen_mode=analysis&report_id={$report_id}&mail_id={$mail_id}&mail_id_mode={$mail_id_mode}&job_id={$sel_job}&eis_no={$sel_eis_no}&callerpage={$callerpage}");
	}
}

//==============================
// トランザクションをコミット
//==============================
pg_query($con, "commit");

//==============================
// 分類ラベル
//==============================
if ($label_edit_flg){
    $folder_info = get_all_classification_folder_info($con, $fname, $emp_id);
    $folders = array();
    foreach($folder_info as $info){
        $work['id'] = $info["folder_id"];
        $work['name'] = h($info["folder_name"]);
        $work['is_common'] = $info["common_flg"]=="t";
        $work['is_selected'] = is_report_in_folder($con, $fname, $report_id, $info["folder_id"]);
        $folders[] = $work;
    }
}

//==============================
//表示
//==============================
$smarty = new Cmx_View();

$smarty->assign("session", $session);
$smarty->assign("fname", $fname);
$smarty->assign("INCIDENT_TITLE", $INCIDENT_TITLE);
$smarty->assign("mode", $mode);

// GRM(SM)フラグ
$smarty->assign("is_sm_emp_flg", $is_sm_emp_flg);

//実行アイコン

$smarty->assign("registration_date", $registration_date);
$smarty->assign("is_evaluation_use", $is_evaluation_use);
$smarty->assign("progress_comment_use_flg", $progress_comment_use_flg);
$smarty->assign("report_id", $report_id);
$smarty->assign("send_eis_id", $send_eis_id);
$smarty->assign("mail_id_mode", $mail_id_mode);
$smarty->assign("mail_id", $mail_id);
$smarty->assign("report_progress_updatable_flg", $report_progress_updatable_flg);
$smarty->assign("label_edit_flg", $label_edit_flg);
$smarty->assign("label_use_flg", $label_use_flg);
$smarty->assign("eis_id", $eis_id);
$smarty->assign("sel_job", $sel_job );
$smarty->assign("sel_eis_no", $sel_eis_no);

$arr_btn_flg = get_inci_btn_show_flg($con, $fname, $session);
$smarty->assign("arr_btn_flg", $arr_btn_flg);

$smarty->assign("report_edit_flg", $report_edit_flg);
$smarty->assign("report_torey_update_flg", $report_torey_update_flg);

$smarty->assign("progres_edit_flg", $progres_edit_flg);

if($report_progress_updatable_flg || $progress_comment_use_flg)
{
    $sql = "select emp_id  from inci_report_progress_comment where report_id = $report_id and emp_id ='$emp_id'";
    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $comment_cnt = pg_num_rows($sel);

    if($comment_cnt > 0){
        $disp_comment = "コメント修正";
        $smarty->assign("has_comment", true);
    }
    else{
        $disp_comment = "確認";
        $smarty->assign("has_comment", false);
    }
    $smarty->assign("disp_comment", $disp_comment);
}

$smarty->assign('folders', $folders);

//メッセージヘッダー
$smarty->assign("marker_style", $marker_style);
$smarty->assign("subject", $subject);
$smarty->assign("send_emp_name", $send_emp_name);
$smarty->assign("send_emp_class_name", $send_emp_class_name);
$smarty->assign("send_date", $send_date);
$smarty->assign("recv_name", $recv_name);

//既に登録済みとなっているファイル一覧
$smarty->assign("file_list", $ret_file);

//------------------------------
//メッセージと報告書内容
//------------------------------
//送信情報を取得
$send_info = get_send_mail_info($con, $fname, $mail_id);

//新デザインで、DBから送信時の報告書データが取得できた場合（新デザインで送信したメール）
$send_mail = array();
if ($design_mode == 2 && count($send_info['rpt_data']) > 0){
    $send_mail = $send_info['send_mail'];
    $smarty->assign("rpt_data", $send_info['rpt_data']);
}

//旧デザイン、またはDBから送信時の報告書データが取得できない場合（旧デザインで送信したメール）
else{
    $mail_message = "";
    foreach($send_info['send_mail'] as $info){
        //テキストファイルに保存したデータがない分は、別途保持
        $send_mail[] = $info;

        //テキストファイルに保存したデータを取得（報告書データとメッセージ）
        $mail_message = get_mail_message($info['mail_id']);
        if ($mail_message != "メッセージを表示できません。"){
            break;
        }
    }
    if ($design_mode == 2){
        $smarty->assign("old_message", "※旧デザインで送信された報告書です。");
    }
    // 返信/転送画面で「<tr><br><td>」のようにタグの間にある<br>を消去する
    // メッセージ入力欄の改行は<br />なので消去されない
    $mail_message = preg_replace("/>(\n)*\s*<br>(\n)*\s*</", ">\n<", $mail_message);
    $smarty->assign("mail_message", $mail_message);
}

//関連する送信メール情報（メールID,送信者名,メッセージの配列）
if ($design_mode == 2){
    if (count($send_mail) > 0){
        //直近の送信メッセージを別枠に表示するため抜き出し
        $last_mail = array_shift ($send_mail);
        $smarty->assign("last_message", $last_mail['message']);

        if (count($send_mail) > 0){
            $smarty->assign("send_mail", $send_mail);
        }
    }
}

//
//-------------------------------------
//確認コメント情報
//-------------------------------------
    $progress_comment_use_flg = get_progress_comment_use($con,$fname);

    $arr_report_auth = array();
    if ($design_mode == 2){
        $arr_report_auth = $rep_obj->get_my_inci_auth_list_for_report_progress($report_id, $emp_id);
    }

    if($progress_comment_use_flg || $design_mode == 2){
        $sql  = " SELECT au.auth, au.auth_name, au.auth_rank, come.comment_date, come.emp_id, come.progress_comment";
        $sql .= " FROM (SELECT * FROM inci_report_progress_comment WHERE report_id = {$report_id}) come";
        $sql .= " LEFT JOIN inci_auth_mst au ON come.auth = au.auth";
        if(count($arr_report_auth) > 0){
            $date = date("Y/m/d");
            $sql .= " UNION";
            $sql .= " SELECT auth, auth_name, auth_rank, '{$date}', '{$emp_id}', ''";
            $sql .= " FROM inci_auth_mst WHERE";
            foreach($arr_report_auth as $key=> $value){
                $auth = $value['auth'];
                if ($key > 0){
                    $sql .= " OR";
                }
                $sql .= " (auth='{$auth}' AND '{$emp_id}' NOT IN (SELECT emp_id FROM inci_report_progress_comment WHERE report_id={$report_id} AND auth='{$auth}'))";
            }
        }
        $sql .= " ORDER BY auth_rank,comment_date";

        $sel = select_from_table($con,$sql,"",$fname);
        if($sel == 0){
            pg_close($con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        $tmp_db_data_arr = pg_fetch_all($sel);

        $progress_comment_data = "";
        $target_auth_list = "";
        foreach($tmp_db_data_arr as $tmp_db_data)
        {
            $auth = $tmp_db_data["auth"];
			
 
            // 評価権限フラグ 20140421 R.C
            //受信トレイにある他の部署の報告書を評価可能にするフラグを取得
            //$is_progress_eval = get_progress_eval($con, $fname);
            $is_progress_eval = $sysConf->get('fantol.evaluation.common.use.flg');
            if($is_progress_eval=='t'){
                $auth_flag = true;
            }else{
                $auth_flag = $sysConf->get('fantol.evaluation.use.' . strtolower($auth) . '.flg') == 't';
            }            

            if($auth_flag){	
                if($tmp_db_data["progress_comment"] !=""){
	            $progress_comment_data[$auth]['auth_name'] = $tmp_db_data["auth_name"];
	
	            $tmp_data["date"] = $tmp_db_data["comment_date"];
	            $tmp_data["emp_name"] = get_emp_kanji_name($con,$tmp_db_data["emp_id"],$fname);;
	            $tmp_data["comment"] = $tmp_db_data["progress_comment"];
	            if ($emp_id == $tmp_db_data["emp_id"]){
	                $tmp_data["is_editable"] = true;
	                if($target_auth_list != ""){
	                    $target_auth_list .= ",";
	                }
	                $target_auth_list .= $auth;
	            }
	            else{
	                $tmp_data["is_editable"] = false;
	            }
	
	            $progress_comment_data[$auth]['data'][] = $tmp_data;
	         }
            }
        }
    }

    //コメントがなければ表示しない。
    if ($progress_comment_data != ""){
        $smarty->assign("progress_comment_data", $progress_comment_data);
    }
    if ($target_auth_list != ""){
        $smarty->assign("target_auth_list", $target_auth_list);
    }
    
    //ここまで
    
    
//データ連携設定情報
$arr_data_connect = $rep_obj->get_inci_data_connect();
$smarty->assign("emr_flg", $arr_data_connect["emr_flg"]);

if ($design_mode == 1){
    $smarty->display("hiyari_rp_mail_disp1.tpl");
}
else{
    $smarty->display("hiyari_rp_mail_disp2.tpl");
}

//==============================
//DBコネクション終了
//==============================
pg_close($con);
