<!-- ************************************************************************ -->
<!-- 勤務シフト集計単位変更「更新」 -->
<!-- ************************************************************************ -->

<?
require_once("about_session.php");
require_once("about_authority.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
// トランザクションを開始
///-----------------------------------------------------------------------------
pg_query($con, "begin transaction");
///-----------------------------------------------------------------------------
// 入力データ収集、ＤＢ更新
///-----------------------------------------------------------------------------

$staff_array = array();

$sql = "DELETE FROM emp_pair_set  ";
for ($r=0; $r<$staff_count; $r++) {
	$rid = "rid_".$r;
	$rid = $$rid;
	$cond = "WHERE emp_id = '$rid'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 相性設定条件を追加する
//$sql = "INSERT INTO emp_pair_set (emp_id,emp_id_2,compatibility) VALUES (";
$sql = "insert into emp_pair_set (emp_id, emp_id_2, compatibility, atdptn_id) values (";
$rsts=1;
for ($r=0; $r<$staff_count; $r++) {
	$rid = "rid_".$r;
	$rid = $$rid;
	for($c=$rsts; $c<$staff_count; $c++){
		$cid = "cid_".$c;
		$cid = $$cid;
		$compa = "pair_set_".$c."_".$r;
		$compa = $$compa;

																		// 相性設定条件を追加する
		$compb = "shift_set_".$c."_".$r;								// 同じ座標の勤務シフトパターン
		$compb = $$compb;

		if($compb == "") {												// 項目型に合わせる
			$compb = 0;
		}
		if( $compa != "" || $compa != 0 ){								// 相性設定したものだけINSERT
			$content = array($rid, $cid, $compa, $compb);

			$ins = insert_into_table($con, $sql, $content, $fname);
			if ($ins == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}
	$rsts++;
}

// 戻りデータ、職員ID
$arg_data = "000000000000";
for($i=0; $i<$staff_count; $i++) {
	$staff_id="cid_".$i;
	$staff_id=$$staff_id;
	$arg_data = $arg_data.",".$staff_id;
}

///-----------------------------------------------------------------------------
// トランザクションをコミット
///-----------------------------------------------------------------------------
pg_query("commit");
///-----------------------------------------------------------------------------
// データベース接続を閉じる
///-----------------------------------------------------------------------------
pg_close($con);

// 相性設定条件を追加する パラメータ追加
echo("<script type=\"text/javascript\">location.href = 'duty_shift_staff_compatiblity.php?session=$session&group_id=$group_id&pattern_id=$pattern_id&arg_data=$arg_data';</script>");
?>

