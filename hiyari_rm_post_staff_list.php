<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("hiyari_common.ini");
require_once("show_class_name.ini");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
// 権限チェック
// ●インシデントレポート(47)
// ●リスクマネージャ(48)
//====================================
$inci_auth_chk = check_authority($session, 47, $fname);
$rm_auth_chk = check_authority($session, 48, $fname);
if ($inci_auth_chk == '0' || $rm_auth_chk == '0') { 
	showLoginPage();
	exit;
}

// データベースに接続
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//======================================
// 組織階層情報取得
//======================================
$arr_class_name = get_class_name_array($con, $fname);

$classes_nm = $arr_class_name["class_nm"]." > ".$arr_class_name["atrb_nm"]." > ".$arr_class_name["dept_nm"];
if($arr_class_name["class_cnt"] == 4) {
	$classes_nm .= " > ".$arr_class_name["room_nm"];
}

// イニシャルのデフォルトは「あ行」
if ($in_id == "") {
	$in_id = 1;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | 職員検索</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script language="javascript">
<!--

function add_emp(emp_id, emp_nm) {

	if(window.opener && !window.opener.closed && window.opener.add_emp_list){
		window.opener.add_emp_list(emp_id, emp_nm, '<?echo($element_id);?>');
	}
}


// -->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
<!--
.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}

// -->
</style>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<form name="mainform" action="#" method="post">

<?
show_hiyari_header_for_sub_window("職員検索");
?>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
    <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
  </tr>
  <tr>
    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    <td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			    <td bgcolor="#F5FFE5"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
			</tr>
		</table>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			    <td bgcolor="#F5FFE5">
				<?
			    show_initial_list($in_id, $element_id, $session, $fname);
				?>	
			    </td>
			</tr>
		</table>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			    <td bgcolor="#F5FFE5"><img src="img/spacer.gif" width="1" height="3" alt=""></td>
			</tr>
		</table>
		<?
			show_employee_list($con, $element_id, $in_id, $session, $fname, $classes_nm, $arr_class_name["class_cnt"]);
		?>
	</td>
    <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
  </tr>
  <tr>
    <td><img src="img/r_3.gif" width="10" height="10"></td>
    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td><img src="img/r_4.gif" width="10" height="10"></td>
  </tr>
</table>
</form>	
</body>
</html>
<? pg_close($con); ?>

<?

function show_initial_list($in_id, $element_id, $session, $fname) {
	$alphabet = array("あ行", "か行", "さ行", "た行", "な行", "は行", "ま行", "や行", "ら行", "わ行");
	for ($i = 0; $i < 10; $i++) {
		$index = $i + 1;
		echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		if ($in_id != $index) {
			echo("<a href=\"$fname?session=$session&in_id=$index&element_id=$element_id\">{$alphabet[$i]}</a>\n");
		} else {
			echo("{$alphabet[$i]}\n");
		}
		echo("</font>");
	}
}

function show_employee_list($con, $element_id, $in_id, $session, $fname, $classes_nm, $class_cnt) {

/*
	$arr_elment_id = split('-', $element_id);
	$element_cnt = count($arr_elment_id);
*/
	$sql = "select a.emp_id, a.emp_lt_nm, a.emp_ft_nm, c.st_nm, d.job_nm, e.class_nm, f.atrb_nm, g.dept_nm, h.room_nm , a.emp_kn_lt_nm, a.emp_kn_ft_nm from empmst a ";
	$sql .= "inner join authmst b on a.emp_id = b.emp_id ";
	$sql .= "left outer join stmst c on a.emp_st = c.st_id ";
	$sql .= "left outer join jobmst d on a.emp_job = d.job_id ";
	$sql .= "left outer join classmst e on a.emp_class = e.class_id ";
	$sql .= "left outer join atrbmst f on a.emp_attribute = f.atrb_id ";
	$sql .= "left outer join deptmst g on a.emp_dept = g.dept_id ";
	$sql .= "left outer join classroom h on a.emp_room = h.room_id ";

	switch ($in_id) {
	case "1":
		$keys = array("01", "02", "03", "04", "05");
		break;
	case "2":
		$keys = array("06", "07", "08", "09", "10", "11", "12", "13", "14", "15");
		break;
	case "3":
		$keys = array("16", "17", "18", "19", "20", "21", "22", "23", "24", "25");
		break;
	case "4":
		$keys = array("26", "27", "28", "29", "30", "31", "32", "33", "34", "35");
		break;
	case "5":
		$keys = array("36", "37", "38", "39", "40");
		break;
	case "6":
		$keys = array("41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55");
		break;
	case "7":
		$keys = array("56", "57", "58", "59", "60");
		break;
	case "8":
		$keys = array("61", "62", "63");
		break;
	case "9":
		$keys = array("64", "65", "66", "67", "68");
		break;
	case "10":
		$keys = array("69", "70", "71", "72", "73", "99");
		break;
	}
	$cond = "where (emp_keywd like '" . join("%' or emp_keywd like '", $keys) . "%') ";

/*
	if($element_cnt > 0) {
		$cond .= "and a.emp_class = $arr_elment_id[0] ";
	}
	if($element_cnt > 1) {
		$cond .= "and a.emp_attribute = $arr_elment_id[1] ";
	}
	if($element_cnt > 2) {
		$cond .= "and a.emp_dept = $arr_elment_id[2] ";
	}
	if($element_cnt > 3) {
		$cond .= "and a.emp_room = $arr_elment_id[3] ";
	}
*/
	$cond .= "and b.emp_del_flg = 'f' ";
	$cond .= "order by a.emp_kn_lt_nm, a.emp_kn_ft_nm ";



	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}


	if(pg_numrows($sel) == 0) {
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
		echo("<tr bgcolor=\"#FFDDFD\"><td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">該当者がおりません。</font></td></tr>");
		echo("</table>\n");

	} else {

		$classes_nm = h($classes_nm);

		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n");
		echo("<tr height=\"22\" bgcolor=\"#DFFFDC\">\n");
		echo("<td width=\"50\"></td>\n");
		echo("<td width=\"100\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">職員氏名</font></td>\n");
		echo("<td width=\"*\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">所属({$classes_nm})</font></td>\n");
		echo("<td width=\"70\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">役職</font></td>\n");
		echo("<td width=\"70\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">職種</font></td>\n");
		echo("</tr>\n");
	
		while ($row = pg_fetch_array($sel)) {
			$emp_id = $row["emp_id"];
			$emp_nm = $row["emp_lt_nm"] . " " . $row["emp_ft_nm"];
			$st_nm = $row["st_nm"];
			$job_nm = $row["job_nm"];

			$class_nm = (empty($row["class_nm"])) ? '(なし)':$row["class_nm"];
			$atrb_nm = (empty($row["atrb_nm"])) ? '(なし)':$row["atrb_nm"];
			$dept_nm = (empty($row["dept_nm"])) ? '(なし)':$row["dept_nm"];
			$room_nm = (empty($row["room_nm"])) ? '(なし)':$row["room_nm"];

			$classes = $class_nm." > ".$atrb_nm." > ".$dept_nm;
			if($class_cnt == 4) {
				$classes .= " > ".$room_nm;
			}

			$emp_nm  = h($emp_nm);
			$st_nm   = h($st_nm);
			$job_nm  = h($job_nm);
			$classes = h($classes);

			echo("<tr height=\"22\" bgcolor=\"#FFFFFF\">\n");
			echo("<td width=\"50\" align=\"center\"><input type=\"button\" value=\"追加\" onclick=\"add_emp('{$emp_id}','{$emp_nm}');\"></td>\n");
			echo("<td width=\"*\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$emp_nm}</font></td>\n");
			echo("<td width=\"*\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$classes}</font></td>\n");
			echo("<td width=\"*\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$st_nm}</font></td>\n");
			echo("<td width=\"*\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$job_nm}</font></td>\n");
			echo("</tr>\n");
		}
		echo("</table>\n");
	}
}


?>
