<?php
require_once("Cmx.php");
require_once("aclg_set.php");
require_once('about_comedix.php');
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_reporting_common.ini");
require_once("hiyari_mail.ini");
require_once("hiyari_report_auth_models.php");
require_once('class/Cmx/Model/SystemConfig.php');
require_once("hiyari_report_class.php");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
    showLoginPage();
    exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

//==================================================
//オプション
//==================================================
$sysConf = new Cmx_SystemConfig();

//デザイン（1=旧デザイン、2=新デザイン）
$design_mode = $sysConf->get('fantol.design.mode');

//==============================
//パラメータ精査
//==============================

//日付検索(年)初期値は今年
if($search_date_year == "")
{
    $search_date_year = date("Y");
}
//日付検索(月)初期値は今月
if($search_date_month == "")
{
    $search_date_month = date("m");
}

//==============================
//ユーザーID取得
//==============================
$sql = "SELECT * FROM empmst JOIN session USING (emp_id) WHERE session_id='{$session}'";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$emp = pg_fetch_assoc($sel);
$emp_id = $emp["emp_id"];

//更新権限(範囲無視)
$is_report_db_update_able = is_report_db_update_able($session,$fname,$con);

// 参照権限のみか、更新権限ありか
if (is_inci_auth_emp($session,$fname,$con) && $is_report_db_update_able) {
     //更新権限あり
    $is_update = true;
}
else{
    $is_update = false;
}

//==============================
//処理モードごとの処理
//==============================
if($is_postback == "true")
{
    switch ($mode)
    {
        case "delete":

            //==============================
            //受信削除
            //==============================
            if(!empty($list_select))
            {
                $select_count=count($list_select);
                for($i=0;$i<$select_count;$i++)
                {
                    if(!$list_select[$i]=="")
                    {
                        //メールID
                        $mail_id = $list_select[$i];

                        //受信者ID
                        $recv_emp_id = $emp_id;

                        //受信メールの論理削除
                        delete_recv_mail($con,$fname,$mail_id,$recv_emp_id);
                    }
                }
            }
            break;
        case "read_change":

            //==============================
            //未読・既読切り替え
            //==============================
            if(!empty($list_select))
            {
                $select_count=count($list_select);
                for($i=0;$i<$select_count;$i++)
                {
                    if(!$list_select[$i]=="")
                    {
                        //メールID
                        $mail_id = $list_select[$i];

                        //受信者ID
                        $recv_emp_id = $emp_id;

                        //既読フラグの取得
                        $where = "where recv_emp_id = '$recv_emp_id' and mail_id = $mail_id";
                        $order = "";
                        $wk1 = get_recv_mail_list($con,$fname,$where,$order);
                        if(count($wk1) < 1)
                        {
                            continue;
                        }
                        $recv_read_flg = $wk1[0]["recv_read_flg"];

                        //既読フラグの更新
                        $recv_read_flg = ($recv_read_flg != "t");//反転＆boolean化
                        update_read_flg($con,$fname,$mail_id,$recv_emp_id,$recv_read_flg == "t");
                    }
                }
            }
            break;
        default:
            break;
    }
}

//==============================
//受信メール一覧取得
//==============================
$where = "where recv_emp_id = '$emp_id' and not recv_del_flg = 't' and not kill_flg";

//日付による絞込み
if($search_date_mode == "month")
{
    $search_date_1 = $search_date_year."/".sprintf('%02d', $search_date_month);
    $where .= " and substr(send_date,0,11) like '$search_date_1%' ";
}
elseif($search_date_mode == "ymd_st_ed")
{
    if($search_date_ymd_st != "")
    {
        $where .= " and substr(send_date,0,11) >= '$search_date_ymd_st' ";
    }
    if($search_date_ymd_ed != "")
    {
        $where .= " and substr(send_date,0,11) <= '$search_date_ymd_ed' ";
    }
}

//検索条件による絞込み
$serch_emp_target = "send";
if($search_emp_class != "")
{
//     $where_class .= " emp_class = $search_emp_class ";
	$where_class .= " registrant_class = $search_emp_class ";
}
if($search_emp_attribute != "")
{
//     $where_class .= " and emp_attribute = $search_emp_attribute ";
    $where_class .= " and registrant_attribute = $search_emp_attribute ";
}
if($search_emp_dept != "")
{
//     $where_class .= " and emp_dept = $search_emp_dept ";
    $where_class .= " and registrant_dept = $search_emp_dept ";
}
if($search_emp_room != "")
{
//     $where_class .= " and emp_room = $search_emp_room ";
    $where_class .= " and registrant_room = $search_emp_room ";
}

if($where_class != "")
{
//     $where .= " and ".$serch_emp_target."_emp_id in (select emp_id from empmst where $where_class union select emp_id from concurrent where $where_class) ";
   //$where .= " and ".$serch_emp_target."_emp_id in (select registrant_id from inci_report where $where_class and del_flag = false and shitagaki_flg = false and kill_flg = false) ";

    $where .=  " and " . $where_class;
    
}

if($search_emp_job != "")
{
    $where .= " and ".$serch_emp_target."_emp_job = $search_emp_job ";
}
if($search_emp_name != "")
{
    $search_emp_name2 = mb_ereg_replace(" ", "", pg_escape_string($search_emp_name));
    $search_emp_name2 = mb_ereg_replace("　", "", $search_emp_name2);
    $where .= " and ";
    $where .= " ( ";
    $where .= "   ( ";
    $where .= "     anonymous_send_flg ";
    $where .= "     and ";
    $where .= "     '".get_anonymous_name($con,$fname)."' like '%$search_emp_name2%' ";
    $where .= "   ) ";
    $where .= "   or ";
    $where .= "   ( ";
    $where .= "     not anonymous_send_flg ";
    $where .= "     and  ";
    $where .= "     ( ";
    $where .= "          ".$serch_emp_target."_emp_lt_nm like '%$search_emp_name2%' ";
    $where .= "       or ".$serch_emp_target."_emp_ft_nm like '%$search_emp_name2%' ";
    $where .= "       or ".$serch_emp_target."_emp_kn_lt_nm like '%$search_emp_name2%' ";
    $where .= "       or ".$serch_emp_target."_emp_kn_ft_nm like '%$search_emp_name2%' ";
    $where .= "       or (".$serch_emp_target."_emp_lt_nm || ".$serch_emp_target."_emp_ft_nm) like '%$search_emp_name2%' ";
    $where .= "       or (".$serch_emp_target."_emp_kn_lt_nm || ".$serch_emp_target."_emp_kn_ft_nm) like '%$search_emp_name2%' ";
    $where .= "     ) ";
    $where .= "   ) ";
    $where .= " ) ";
}

if($search_patient_id != "")
{
    $search_patient_id2 = pg_escape_string($search_patient_id);
    $where .= " and input_item_210_10 = '$search_patient_id2' ";
}

if($search_patient_name != "")
{
    $search_patient_name2 = pg_escape_string($search_patient_name);
    $where .= " and input_item_210_20 like '%$search_patient_name2%' ";
}


if($search_patient_year_from == "900")
{
    $where .= " and input_item_210_40 = '$search_patient_year_from' ";
}
else
{
    if($search_patient_year_from != "" && $search_patient_year_to != "")
    {
        $where .= " and input_item_210_40 >= '$search_patient_year_from' ";
        $where .= " and input_item_210_40 <= '$search_patient_year_to' ";
    }
    else if($search_patient_year_from != "" && $search_patient_year_to == "")
    {
        $where .= " and input_item_210_40 >= '$search_patient_year_from' ";
        $where .= " and input_item_210_40 < '900' ";
    }
    else if($search_patient_year_from == "" && $search_patient_year_to != "")
    {
        $where .= " and input_item_210_40 <= '$search_patient_year_to' ";
    }
}

if($search_report_no != "")
{
    $search_report_no2 = pg_escape_string($search_report_no);
    $where .= " and report_no like '{$search_report_no2}%' ";
}

if($search_incident_date_start != "")
{
    $where .= " and incident_date >= '$search_incident_date_start'";
}
if($search_incident_date_end != "")
{
    $where .= " and incident_date <= '$search_incident_date_end'";
}

if($search_torey_date_in_search_area_start != "")
{
    $where .= " and substr(send_date,0,11) >= '$search_torey_date_in_search_area_start' ";
}
if($search_torey_date_in_search_area_end != "")
{
    $where .= " and substr(send_date,0,11) <= '$search_torey_date_in_search_area_end' ";
}


if($search_title != "") {
    $title_keyword = mb_ereg_replace("　"," ",$search_title); //全角スペース除去
    $title_keyword = trim($title_keyword); //前後スペース除去
    $title_keyword_list = mb_split(" ", $title_keyword); //スペース区切り

    $wk_str = "";
    //マルチワードについては常にOR結合
    for ($i=0; $i<count($title_keyword_list); $i++) {
        if ($i > 0) {
            if($title_and_or_div == 1 || $title_and_or_div == "") {
                $wk_str .= " and ";
            } else {
                $wk_str .= " or ";
            }
        }
        $wk_str .= " report_title like '%{$title_keyword_list[$i]}%' ";
    }
    $where .= " and ($wk_str) ";
}

if($search_incident != "") {
    $title_keyword = mb_ereg_replace("　"," ",$search_incident); //全角スペース除去
    $title_keyword = trim($title_keyword); //前後スペース除去
    $title_keyword_list = mb_split(" ", $title_keyword); //スペース区切り

    $wk_str = "";
    //マルチワードについては常にOR結合
    for ($i=0; $i<count($title_keyword_list); $i++) {
        if ($i > 0) {
            if($incident_and_or_div == 1 || $incident_and_or_div == "") {
                $wk_str .= " and ";
            } else {
                $wk_str .= " or ";
            }
        }
        $wk_str .= " input_item_520_30 like '%{$title_keyword_list[$i]}%' ";
    }
    $where .= " and ($wk_str) ";
}












if($sort_div == "0")
{
    $sort = "asc";
}
else
{
    $sort = "desc";
}


$order = " order by";
switch($sort_item)
{
    case "REPORT_NO":
        $order .= " report_no $sort ,";
        break;
    case "SUBJECT":
        $order .= " subject $sort ,";
        break;
    case "SEND_CLASS":
        //$order .= " send_emp_class $sort , send_emp_attribute $sort , send_emp_dept $sort , send_emp_room $sort ,";
		$order .= " registrant_class $sort , registrant_attribute $sort , registrant_dept $sort , registrant_room $sort ,";
		break;
    case "SEND_NAME":
        $order .= " anonymous_send_flg $sort, send_emp_kn_lt_nm || send_emp_kn_ft_nm $sort ,";
        break;
    case "SEND_DATE":
        $order .= " send_date $sort ,";
        break;
    case "READ_REPORT":
        $order .= " recv_read_flg $sort ,";
        break;
    case "LEVEL":
        $order .= " level_num $sort ,";
        break;
    case "SM_START":
        $order .= " sm_start_flg $sort ,";
        break;
    case "SM_END":
        $order .= " sm_end_flg $sort ,";
        break;
    case "SM_EVAL":
        $order .= " sm_evaluation_flg $sort ,";
        break;
    case "RM_START":
        $order .= " rm_start_flg $sort ,";
        break;
    case "RM_END":
        $order .= " rm_end_flg $sort ,";
        break;
    case "RM_EVAL":
        $order .= " rm_evaluation_flg $sort ,";
        break;
    case "RA_START":
        $order .= " ra_start_flg $sort ,";
        break;
    case "RA_END":
        $order .= " ra_end_flg $sort ,";
        break;
    case "RA_EVAL":
        $order .= " ra_evaluation_flg $sort ,";
        break;
    case "SD_START":
        $order .= " sd_start_flg $sort ,";
        break;
    case "SD_END":
        $order .= " sd_end_flg $sort ,";
        break;
    case "SD_EVAL":
        $order .= " sd_evaluation_flg $sort ,";
        break;
    case "MD_START":
        $order .= " md_start_flg $sort ,";
        break;
    case "MD_END":
        $order .= " md_end_flg $sort ,";
        break;
    case "MD_EVAL":
        $order .= " md_evaluation_flg $sort ,";
        break;
    case "HD_START":
        $order .= " hd_start_flg $sort ,";
        break;
    case "HD_END":
        $order .= " hd_end_flg $sort ,";
        break;
    case "HD_EVAL":
        $order .= " hd_evaluation_flg $sort ,";
        break;
    
    default:
        //何もしない。
        break;
}

$order .= " mail_id desc";



$fields = array(
            'report_id',
//          'report_title',
//          'job_id',
//          'registrant_id',
//          'registrant_name',
//          'registration_date',
//          'eis_id',
//          'eid_id',
//          'del_flag',
            'report_no',
//          'shitagaki_flg',
//          'report_comment',
//          'edit_lock_flg',
//          'anonymous_report_flg',
            'registrant_class',
            'registrant_attribute',
            'registrant_dept',
            'registrant_room',
            'kill_flg',

//          'report_emp_class',
//          'report_emp_attribute',
//          'report_emp_dept',
//          'report_emp_room',
//          'report_emp_job',
//          'report_emp_lt_nm',
//          'report_emp_ft_nm',
//          'report_emp_kn_lt_nm',
//          'report_emp_kn_ft_nm',

//          '[auth]_update_emp_id',
            '[auth]_start_flg',
            '[auth]_end_flg',
            '[auth]_evaluation_flg',
//          '[auth]_start_date',
//          '[auth]_end_date',
            '[auth]_use_flg',

            'mail_id',
            'subject',
            'send_emp_id',
            'send_emp_name',
            'send_date',
//          'send_del_flg',
            'send_job_id',
            'send_eis_id',
            'anonymous_send_flg',
            'marker',

            'send_emp_class',
            'send_emp_attribute',
            'send_emp_dept',
            'send_emp_room',
//          'send_emp_job',
//          'send_emp_lt_nm',
//          'send_emp_ft_nm',
            'send_emp_kn_lt_nm',
            'send_emp_kn_ft_nm',

            'recv_emp_id',
//          'recv_emp_name',
//          'recv_class',
//          'recv_class_disp_index',
            'recv_read_flg',
//          'recv_read_flg_update_date',
            'recv_del_flg',
//          'anonymous_recv_flg',

//          'recv_emp_class',
//          'recv_emp_attribute',
//          'recv_emp_dept',
//          'recv_emp_room',
//          'recv_emp_job',
//          'recv_emp_lt_nm',
//          'recv_emp_ft_nm',
//          'recv_emp_kn_lt_nm',
//          'recv_emp_kn_ft_nm',

//          'report_link_id',
//          'report_link_status',
//          'main_report_id',
            'level',
            'level_num',
            'reply_mail_id',
            'forward_mail_id',
            'eis_name',
            'eis_no',
    );
if($search_emp_class != "" || $search_emp_attribute != "" || $search_emp_dept != "" || $search_emp_room != "" || $search_emp_job != "" || $search_emp_name != "" ||
    $search_patient_id != "" || $search_patient_name != "" || $search_patient_year_from != "" || $search_patient_year_to != "" || $search_incident_date_start != "" || $search_incident_date_end != "" ||
    $search_title != "" || $search_incident != "")
{
    $fields[] = 'send_emp_class';
    $fields[] = 'send_emp_attribute';
    $fields[] = 'send_emp_dept';
    $fields[] = 'send_emp_room';
    $fields[] = 'send_emp_job';
    $fields[] = 'send_emp_lt_nm';
    $fields[] = 'send_emp_ft_nm';
    $fields[] = 'send_emp_kn_lt_nm';
    $fields[] = 'send_emp_kn_ft_nm';
    $fields[] = 'input_item_210_10';
    $fields[] = 'input_item_210_20';
    $fields[] = 'input_item_210_40';
    $fields[] = 'incident_date';

    $fields[] = 'report_title';
    $fields[] = 'input_item_520_30';

}

//１ページ分のデータを取得
if($page == ""){
    $page = 1;
}
$disp_count_max_in_page = 20;
$offset = $disp_count_max_in_page * ($page - 1);
$list_data_array = get_recv_mail_list($con,$fname,$where,$order,$fields,$offset,$disp_count_max_in_page);

//総件数取得
$rec_count = get_recv_mail_list_count($con,$fname,$where,$fields);

//最大ページ数
if($rec_count == 0){
    $page_max  = 1;
}
else{
    $page_max  = floor( ($rec_count-1) / $disp_count_max_in_page ) + 1;
}

//==============================
//インシデント権限マスタ取得
//==============================
$arr_inci = get_inci_mst($con, $fname);

//SM表示フラグ取得/SM除去
$sm_disp_flg = get_sm_disp_flg($fname, $con);
if($sm_disp_flg != "t")
{
    $wk_arr_inci = null;
    foreach($arr_inci as $auth => $auth_info)
    {
        if($auth != "SM")
        {
            $wk_arr_inci[$auth] = $auth_info;
        }
    }
    $arr_inci = $wk_arr_inci;
}

//==============================
//患者影響レベル略称情報を取得
//==============================
$level_disp_list = get_inci_level_short_name($con,$fname);

//==============================
//画面タイトル
//==============================
$page_title = "受信トレイ";

//------------------------------
//実行アイコン
//------------------------------
$arr_btn_flg = get_inci_btn_show_flg($con, $fname, $session);
$icon_disp = array();
$icon_disp['create'] = $arr_btn_flg["create_btn_show_flg"] == "t";
$icon_disp['return'] = $arr_btn_flg["return_btn_show_flg"] == "t";
$icon_disp['forward'] = $arr_btn_flg["forward_btn_show_flg"] == "t";
$icon_disp['delete1'] = true;
$icon_disp['edit'] = is_mail_report_editable($session,$fname,$con);
$icon_disp['read_flg'] = true;
$icon_disp['progress'] = is_progres_editable($session,$fname,$con);
$icon_disp['analysis'] = is_report_analysis_usable($session,$fname,$con);
$icon_disp['analysis_register'] = false;
$icon_disp['excel'] = false;
$icon_disp['delete2'] = false;
$icon_disp['undelete'] = false;
$icon_disp['kill'] = false;
$icon_disp['search'] = true;

//------------------------------
//進捗表示
//------------------------------
$auth_list = array();
$progress_use = array();
$evaluation_use = array();  //担当者毎の評価機能利用フラグ
$is_evaluation_use = false; //評価機能を利用する担当者がいるかどうかのフラグ
if ($design_mode == 1){
    foreach($arr_inci as $arr_key => $arr){
        $progress_use[$arr_key] = is_usable_auth($arr_key,$fname,$con);
    }
}
else{
    $arr_inci_progress = search_inci_report_progress_dflt_flg($con, $fname);
    $num = pg_numrows($arr_inci_progress);
    for($i=0; $i<$num; $i++) {
        $auth = pg_result($arr_inci_progress,$i,"auth");
        $progress_use[$auth] = (pg_result($arr_inci_progress,$i,"use_flg") == 't');
        $evaluation_use[$auth] = ($sysConf->get('fantol.evaluation.use.' . strtolower($auth) . '.flg') == 't');
                
        if ($progress_use[$auth] && ($auth != 'SM' || $sm_disp_flg == 't')){
            $auth_list[$auth]['auth_name'] = h(pg_result($arr_inci_progress,$i,"auth_short_name"));
            $auth_list[$auth]['eval_use'] = $evaluation_use[$auth];
  
        }
        if ($evaluation_use[$auth]){
            $is_evaluation_use = true;
        }
    }
}

//------------------------------
//一覧項目表示フラグ
//------------------------------
$disp_flag = array();
$disp_flag = get_recv_torey_list_disp($con,$fname);
$is_inci_auth_emp = is_inci_auth_emp($session,$fname);
if (!$is_inci_auth_emp){
    $disp_flag["read_count_disp"] = false;
}

//------------------------------
//ステータス
//------------------------------
$report_ids = '';
foreach($list_data_array as $i => $list_data){
    if ($i > 0){
        $report_ids.= ",";
    }
    $report_ids.= "'" . $list_data["report_id"] . "'";
}

if ($report_ids != '') {
    //評価ステータス
    $emp_auth = get_emp_priority_auth($session,$fname,$con);
    if ($design_mode == 2){
        if ($evaluation_use[$emp_auth]){
            $sql = "SELECT report_id, auth, evaluation_flg FROM inci_report_progress WHERE report_id IN ($report_ids)";
            $sel = select_from_table($con, $sql, "", $fname);
            $rows = pg_fetch_all($sel);
            foreach ($rows as $r){
                $evaluation_status[$r['report_id']][$r['auth']] = $r['evaluation_flg'];
            }
        }
    }

    //分析ステータス
    $sql = "SELECT analysis_problem_id FROM inci_analysis_regist WHERE available AND analysis_problem_id IN ($report_ids)";
    $sel = select_from_table($con, $sql, "", $fname);
    $rows = pg_fetch_all($sel);
    foreach ($rows as $r){
        $analysis_status[] = $r['analysis_problem_id'];
    }
}
//------------------------------
//一覧データ
//------------------------------
$rep_obj = new hiyari_report_class($con, $fname);

$profile_use = get_inci_profile($fname, $con);

//匿名表示
if ($design_mode == 1){
    $anonymous_name = h(get_anonymous_name($con,$fname));
}
else{
    $anonymous_name = '匿名';
}
$anonymous_setting = get_anonymous_setting($session, $con, $fname);
$mouseover_popup_base_flg = false;
// セーフティマネージャで一部匿名の場合 → 実名が見える
if(is_inci_auth_emp_of_auth($session,$fname,$con,"SM") and !empty($anonymous_setting)) {
    $mouseover_popup_base_flg = true;
}

$list = array();
foreach($list_data_array as $i => $list_data){
    $mail_id = $list_data["mail_id"];
    $list[$i]['mail_id'] = $mail_id;
    $list[$i]['list_id'] = $mail_id;

    $report_id = $list_data["report_id"];
    $list[$i]['report_id'] =  $report_id;

    $list[$i]['marker_style'] = get_style_by_marker($list_data["marker"]);

    //ステータス
    if ($design_mode == 2){
    $status[1]['flag'] = $list_data['reply_mail_id'] > 0;
    $status[1]['name'] = '返信';

    $status[2]['flag'] = $list_data['forward_mail_id'] > 0;
    $status[2]['name'] = '転送';

    $status[3]['flag'] = $rep_obj->get_sm_lock($report_id);
    $status[3]['name'] = 'ロック';

        if ($is_evaluation_use){
        if ($evaluation_use[$emp_auth]){
            $status[4]['flag'] = $evaluation_status[$report_id][$emp_auth] == 't';
        }
            else{
                $status[4]['flag'] = false;
            }
            $status[4]['name'] = '評価';
    }
    
        $status[5]['flag'] = in_array($report_id, $analysis_status);
    $status[5]['name'] = '分析';

    $list[$i]['status'] = $status;
    }
    
    //事案番号
    $list[$i]['report_no'] = $list_data["report_no"];

    //件名
    $list[$i]['subject'] = h($list_data["subject"]);
    
    //送信時報告者職種
    $list[$i]['send_job_id'] = h($list_data["send_job_id"]);
    
    //送信時様式ID
    $list[$i]['send_eis_id'] = h($list_data["send_eis_id"]);
    
    //送信時様式No
    $list[$i]['eis_no'] = h($list_data["eis_no"]);
    
    //報告部署
    $class_nm = "";
    if($profile_use["class_flg"] == "t" && $list_data["registrant_class"] != ""){
        $class_nm .= get_class_nm($con,$list_data["registrant_class"],$fname);
    }
    if($profile_use["attribute_flg"] == "t" && $list_data["registrant_attribute"] != ""){
        $class_nm .= get_atrb_nm($con,$list_data["registrant_attribute"],$fname);
    }
    if($profile_use["dept_flg"] == "t" && $list_data["registrant_dept"] != ""){
        $class_nm .= get_dept_nm($con,$list_data["registrant_dept"],$fname);
    }
    if($profile_use["room_flg"] == "t" && $list_data["registrant_room"] != ""){
        $class_nm .= get_room_nm($con,$list_data["registrant_room"],$fname);
    }
    $list[$i]['class_nm'] = $class_nm;

    //送信者
    $real_name = h($list_data["send_emp_name"]);
    $list[$i]['real_name'] = $real_name;

    $anonymous_flg = $list_data["anonymous_send_flg"] == "t";
    $list[$i]['anonymous_flg'] = $anonymous_flg;

    if($anonymous_flg){
        $list[$i]['disp_emp_name'] = $anonymous_name;
    }
    else{
        $list[$i]['disp_emp_name'] = $real_name;
    }

    $mouseover_popup_flg = false;
    if ($mouseover_popup_base_flg){
        $mouseover_popup_flg = true;
    }
    else{
        foreach ($anonymous_setting as $auth => $data) {
            if ($data["report_db_use_flg"] == 't' and $data["report_db_read_div"] == 0) {
                $mouseover_popup_flg = true;
            }
            else if ($data["report_db_use_flg"] == 't' and $data["report_db_read_div"] == 1) {
                if ($list_data["send_emp_class"] == $emp["emp_class"]) {
                    $mouseover_popup_flg = true;
                }
            }
            else if ($data["report_db_use_flg"] == 't' and $data["report_db_read_div"] == 2) {
                if ($list_data["send_emp_class"] == $emp["emp_class"] and $list_data["send_emp_attribute"] == $emp["emp_attribute"]) {
                    $mouseover_popup_flg = true;
                }
            }
            else if ($data["report_db_use_flg"] == 't' and $data["report_db_read_div"] == 3) {
                if ($list_data["send_emp_class"] == $emp["emp_class"] and $list_data["send_emp_attribute"] == $emp["emp_attribute"] and $list_data["send_emp_dept"] == $emp["emp_dept"]) {
                    $mouseover_popup_flg = true;
                }
            }
            if ($data["report_db_read_auth_flg"] == 't') {
                if ($list_data["send_emp_room"]) {
                    if ($data["rm"][ $list_data["send_emp_class"] ][ $list_data["send_emp_attribute"] ][ $list_data["send_emp_dept"] ][ $list_data["send_emp_room"] ]) {
                        $mouseover_popup_flg = true;
                    }
                }
                else {
                    if ($data["rm"][ $list_data["send_emp_class"] ][ $list_data["send_emp_attribute"] ][ $list_data["send_emp_dept"] ]) {
                        $mouseover_popup_flg = true;
                    }
                }
            }
        }
    }
    $list[$i]['mouseover_popup_flg'] = $mouseover_popup_flg;

    //送信日
    $list[$i]['send_date'] = substr($list_data["send_date"],0,10);

    //既読
    $recv_read_count = get_recv_count_for_send_mail($con,$fname,$mail_id,"TO","t");
    $recv_all_count = get_recv_count_for_send_mail($con,$fname,$mail_id,"TO");
    $list[$i]['read_count'] = $recv_read_count."/".$recv_all_count;

    //未読
    if ($design_mode == 1){
        $list[$i]['recv_not_read_mark'] = ($list_data["recv_read_flg"] == "t") ? "&nbsp":"未";
    }
    else{
        $list[$i]['is_read'] = ($list_data["recv_read_flg"] == "t");
    }

    //影響
    $level = $list_data["level"];
    if($level != ""){
        $level = $level_disp_list[$level];
    }
    $list[$i]['level'] = $level;

    //進捗
    $progress_comment_use_flg = get_progress_comment_use($con,$fname);

    //$uketsuke[$auth] = ($sysConf->get('fantol.progress.' . strtolower($auth) . '.uketsuke.flg') == 't');
    //$confirm[$auth] = ($sysConf->get('fantol.progress.' . strtolower($auth) . '.confirm.flg') == 't');
    //$eval[$auth] = ($sysConf->get('fantol.progress.' . strtolower($auth) . 'eval.flg') == 't');
    
    
    $auth_info = array();
    foreach($arr_inci as $arr_key => $arr_val) {
        $progress_array = array();
        if ($progress_use[$arr_key]){
            
            $sql = "select * from inci_report_progress_detail";
            $cond = "where report_id = '$report_id' and auth = '$arr_key' and use_flg = 't'";
            $result = select_from_table($con,$sql,$cond,$fname);
            $progress_array = pg_fetch_all($result);
              
            $auth_ = strtolower($arr_key) .'_start_flg';
            $start_flg = $list_data["$auth_"] ;
            
            $auth_ = strtolower($arr_key) .'_end_flg';
            $end_flg = $list_data["$auth_"] ;
                    
            $auth_ = strtolower($arr_key) .'_evaluation_flg';
            $eval_flg = $list_data["$auth_"] ;       
            
             
            foreach ($progress_array as $value) {
                if ($sysConf->get('fantol.progress.' . strtolower($arr_key) . '.uketsuke.flg') == 't'){
                    if($value['start_flg']=='t'){
                        $start_flg = 't';
                    }else{
                        $start_flg ='f';
                        break;
                    }
                }
            }
                       
            $start_flg_individual = 'f';
            if($start_flg =='f'){
                if ($sysConf->get('fantol.progress.' . strtolower($arr_key) . '.uketsuke.flg') == 't'){
                    $sql = "select start_flg from inci_report_progress_detail";
                    $cond = " where mail_id = '$mail_id' and auth = '$arr_key' and emp_id= '$emp_id' and use_flg = 't'";
            
                    $results = select_from_table($con,$sql,$cond,$fname);
                    $start_flg_individual_tmp=pg_fetch_result($results, 0, "start_flg");
                    //$result = pg_query($con,$sql);
                    //$row = pg_fetch_row($result);
                    
                    if($start_flg_individual_tmp=='t'){
                        $start_flg_individual = 't';
                    }else {
                        $start_flg_individual = 'f';
                    }
                }
            }
                        
            foreach ($progress_array as $value) {
                if ($sysConf->get('fantol.progress.' . strtolower($arr_key) . '.confirm.flg') == 't'){
                    if($value['end_flg']=='t'){
                        $end_flg = 't';
                    }else{
                        $end_flg ='f';
                        break;
                    }
                }
            }
            
            $end_flg_individual = 'f';
            if($end_flg =='f'){
                if ($sysConf->get('fantol.progress.' . strtolower($arr_key) . '.confirm.flg') == 't'){
                    $sql = "select end_flg from inci_report_progress_detail";
                    $cond = " where mail_id = '$mail_id' and auth = '$arr_key' and emp_id= '$emp_id' and use_flg = 't'";
            
                    $results = select_from_table($con,$sql,$cond,$fname);
                    $end_flg_individual_tmp=pg_fetch_result($results, 0, "end_flg");
                    
                    if($end_flg_individual_tmp=='t'){
                        $end_flg_individual = 't';
                    }else {
                        $end_flg_individual = 'f';
                    }
                }
            }
            
            foreach ($progress_array as $value) {
                if ($sysConf->get('fantol.progress.' . strtolower($arr_key) . '.eval.flg') == 't'){
                    if($value['evaluation_flg']=='t'){
                        $eval_flg = 't';
                    }else{
                        $eval_flg ='f';
                        break;
                    }
                }
            }
            
            $eval_flg_individual = 'f';
            if($eval_flg =='f'){
                if ($sysConf->get('fantol.progress.' . strtolower($arr_key) . '.eval.flg') == 't'){
                    $sql = "select evaluation_flg from inci_report_progress_detail";
                    $cond = " where mail_id = '$mail_id' and auth = '$arr_key' and emp_id= '$emp_id' and use_flg = 't'";
            
                    $results = select_from_table($con,$sql,$cond,$fname);
                    $eval_flg_individual_tmp=pg_fetch_result($results, 0, "evaluation_flg");
                    
                    if($eval_flg_individual_tmp=='t'){
                        $eval_flg_individual = 't';
                    }else {
                        $eval_flg_individual = 'f';
                    }
                }
            }
            
            
             
            if($start_flg  == "t"){
                $accepted  ="1";
            }else if($start_flg_individual == "t"){
               $accepted  ="2";
            }else{
                $accepted  ="0";
            }
            
            if($end_flg  == "t"){
                $confirmed  ="1";
            }else if($end_flg_individual == "t"){
               $confirmed  ="2";
            }else{
                $confirmed  ="0";
            }
            
            if($eval_flg  == "t"){
                $evaluated  ="1";
            }else if($eval_flg_individual == "t"){
               $evaluated  ="2";
            }else{
                $evaluated  ="0";
            }
                   
            //$accepted  = ($start_flg  == "t");
            //$confirmed = ($end_flg   == "t");
            //$evaluated = ($eval_flg   == "t");
            
            
            switch ($arr_key){
                case "SM":
                    $use_flag  = ($list_data["sm_use_flg"]        == "t");
                    //$accepted  = ($start_flg  == "t");
                    //$confirmed = ($list_data["sm_end_flg"]        == "t");
                    //$evaluated = ($list_data["sm_evaluation_flg"] == "t");
                    break;
                case "RM":
                    $use_flag  = ($list_data["rm_use_flg"]        == "t");
                   // $accepted  = ($list_data["rm_start_flg"]      == "t");
                    //$confirmed = ($list_data["rm_end_flg"]        == "t");
                    //$evaluated = ($list_data["rm_evaluation_flg"] == "t");
                    break;
                case "RA":
                    $use_flag  = ($list_data["ra_use_flg"]        == "t");
                   // $accepted  = ($list_data["ra_start_flg"]      == "t");
                    //$confirmed = ($list_data["ra_end_flg"]        == "t");
                    //$evaluated = ($list_data["ra_evaluation_flg"] == "t");
                    break;
                case "SD":
                    $use_flag  = ($list_data["sd_use_flg"]        == "t");
                    //$accepted  = ($list_data["sd_start_flg"]      == "t");
                    //$confirmed = ($list_data["sd_end_flg"]        == "t");
                    //$evaluated = ($list_data["sd_evaluation_flg"] == "t");
                    break;
                case "MD":
                    $use_flag  = ($list_data["md_use_flg"]        == "t");
                  //  $accepted  = ($list_data["md_start_flg"]      == "t");
                    //$confirmed = ($list_data["md_end_flg"]        == "t");
                    //$evaluated = ($list_data["md_evaluation_flg"] == "t");
                    break;
                case "HD":
                    $use_flag  = ($list_data["hd_use_flg"]        == "t");
                   // $accepted  = ($list_data["hd_start_flg"]      == "t");
                    //$confirmed = ($list_data["hd_end_flg"]        == "t");
                    //$evaluated = ($list_data["hd_evaluation_flg"] == "t");
                    break;
                default:
            }
            $auth_info[$arr_key]['use'] = $use_flag;
            $auth_info[$arr_key]['accepted'] = $accepted;
            $auth_info[$arr_key]['confirmed'] = $confirmed;
            if ($design_mode == 2){
                if ($evaluation_use[$arr_key]){
                    $auth_info[$arr_key]['evaluated'] = $evaluated;
                }
            }
        }
    }
    $list[$i]["auth_info"] = $auth_info;

    //更新権限
    $report_torey_update_flg = getToreyUpdateAuth($con, $session, $fname, $report_id);
    if ($report_torey_update_flg) {
        // HTML出力用更新権限フラグ ： 更新権限が有る場合は、1を代入
        $report_torey_update_flg_value = '1';
    }
    else {
        // HTML出力用更新権限フラグ ： 更新権限が無い場合は、0を代入
        $report_torey_update_flg_value = '0';
    }
    $list[$i]['report_torey_update_flg_value'] = $report_torey_update_flg_value;
}

include("hiyari_rpt_display.php");

//==============================
//新着受信メールのアクセス済み化
//==============================
require_once("hiyari_info_access.ini");
access_entry_all_new_recv_mail($con,$fname,$emp_id);

//==============================
//DBコネクション終了
//==============================
pg_close($con);
