<?php
/***********************************************************
 * 主に使用する権限取得のクラス化
 *
 * 使用するメソッドはget_analysis_disp_auth
 * 引数は$report_id
 * レポートに対してアクセスユーザが閲覧可能かBOOLEAN型で返す
 ***********************************************************/
class hiyari_auth_class { 
	// DBインスタンス
	var $_db;
	
	// セッション
	var $_session;
	
	// ユーザID
	var $_emp_id;
	
	// ユーザ権限
	var $_auth = 'GU';
	
	// 自分がなり得る権限の一覧を保持するプロパティ
	var $_my_inci_auth_list = false;
	
	// 上記のプロパティを取得済みかチェックするフラグ
	var $_is_created_inci_auth_list = false;

	// 
	var $_emp_auth_class_post = false;
	
	// 上記のプロパティを取得済みかチェックするフラグ
	var $_is_created_emp_auth_class_post = false;
	
	/*******************************************************
	 * コンストラクタ
	 *
	 * @param	RESOURCE	$con	DBコネクタ
	 * @param	STRING		$file	使用ファイル名
	 * @param	STRING		$session	セッション
	 *******************************************************/
	function hiyari_auth_class($con, $file, $session) { 
		// セッションを格納
		$this->_session = $session;
		
		require_once('./hiyari_db_class.php');
		
		// DBクラスのインスタンス化
		$this->_db = new hiyari_db_class($con, $file);
		
		// ユーザIDのセット
		$sql  = "SELECT emp_id FROM session ";
		$cond = "WHERE session_id = '{$this->_session}'";
		
		$this->_db->query($sql.$cond);
		// ユーザIDを返す
		$this->_emp_id = $this->_db->getOne();
		
		// 最高権限を取得する
		$this->_auth = $this->get_emp_priority_auth();
	}
	
	/*******************************************************
	 * ユーザIDを取得する
	 *
	 * @return	INTEGER		ユーザIDを返す
	 *******************************************************/
	function get_emp_id() { 
		// コンストラクタでセットしたユーザIDを返す
		return $this->_emp_id;
	}
	
	/*******************************************************
	 * ユーザ権限を取得する
	 *
	 * @return	STRING		ユーザ権限を返す
	 *******************************************************/
	function get_auth() { 
		// コンストラクタでセットしたユーザIDを返す
		return $this->_auth;
	}
	
	/*******************************************************
	 * 表示権限があるかチェックする(インターフェイス)
	 *
	 * @param	INTEGER		$report_id		レポートID
	 * @return	BOOLEAN		権限あり：true  無し：false
	 *******************************************************/
	function get_analysis_disp_auth($report_id) { 
		// レポートデータ取得(１レコード)
		$report_data = $this->get_stats_report_data($report_id);
		
		// 報告ファイル閲覧権限があるか調べる
		return $this->is_report_db_readable_report_post($report_data['registrant_class'], 
														$report_data['registrant_attribute'],
														$report_data['registrant_dept'],
														$report_data['registrant_room']);
	}
	
	
	/*******************************************************
	 * レポートデータ取得
	 *
	 * @param	INTEGER		$report_id		レポートID
	 * @return	ARRAY(MIXED)  レポートのデータ
	 *******************************************************/
	function get_stats_report_data($report_id)
	{
		$arr_ret = array();

		$sql  = "select ";
		$sql .= "a.report_id, ";
		$sql .= "a.report_no, ";
		$sql .= "a.report_title, ";
		$sql .= "a.registration_date, ";
		$sql .= "a.anonymous_report_flg, ";
		$sql .= "a.registrant_class, ";
		$sql .= "a.registrant_attribute, ";
		$sql .= "a.registrant_dept, ";
		$sql .= "a.registrant_room, ";
		$sql .= "b.emp_lt_nm  || ' ' || emp_ft_nm as registrant_nm, ";
		$sql .= "c.class_nm, ";
		$sql .= "d.atrb_nm, ";
		$sql .= "e.dept_nm, ";
		$sql .= "f.room_nm, ";
		$sql .= "g.input_item as happened_ymd ";
		$sql .= "from inci_report a ";
		$sql .= "left join empmst b on a.registrant_id = b.emp_id ";
		$sql .= "left join classmst c on a.registrant_class = c.class_id and not class_del_flg ";
		$sql .= "left join atrbmst d on a.registrant_attribute = d.atrb_id and not atrb_del_flg ";
		$sql .= "left join deptmst e on a.registrant_dept = e.dept_id and not dept_del_flg ";
		$sql .= "left join classroom f on a.registrant_room = f.room_id and not room_del_flg ";
		$sql .= "left join (select eid_id, input_item from inci_easyinput_data where grp_code = 100 and easy_item_code = 5) g on a.eid_id = g.eid_id ";

		$cond = "where report_id = {$report_id} order by report_id desc";

		$this->_db->query($sql.$cond);
		$row = $this->_db->getRow();
		
		//プロフィールの個人設定情報
		$profile_use = $this->get_inci_profile();

		//while($row = pg_fetch_array($sel))
		//{
			$class_nm = "";

			/*$cnt = 0;
			foreach($arr_report_id as $report_id)
			{
				if($report_id == $row["report_id"])
				{
					$cnt++;	
				}
			}*/

			if($profile_use["class_flg"] == "t" && $row["class_nm"] != "")
			{
				$class_nm .= $row["class_nm"];
			}
			if($profile_use["attribute_flg"] == "t" && $row["atrb_nm"] != "")
			{
				$class_nm .= $row["atrb_nm"];
			}
			if($profile_use["dept_flg"] == "t" && $row["dept_nm"] != "")
			{
				$class_nm .= $row["dept_nm"];
			}
			if($profile_use["room_flg"] == "t" && $row["room_nm"] != "")
			{
				$class_nm .= $row["room_nm"];
			}

			// $arr_ret[]  <= 2重配列でなくする
			$arr_ret = array("report_id" => $row["report_id"],
			                    "report_no" => $row["report_no"],
			                    "report_title" => $row["report_title"],
								"registrant_nm" => $row["registrant_nm"],
								"anonymous_report_flg" => $row["anonymous_report_flg"],
			                    "class_nm" => $class_nm,
			                    "happened_ymd" => $row["happened_ymd"],
			                    "registration_date" => $row["registration_date"],
								//"count" => $cnt,
			                    "registrant_class" => $row["registrant_class"],
			                    "registrant_attribute" => $row["registrant_attribute"],
			                    "registrant_dept" => $row["registrant_dept"],
			                    "registrant_room" => $row["registrant_room"],
							   );
		//}
		return $arr_ret;
	}

	/**
	 * 報告ファイルにて参照可能な報告部署であるかを判定します。
	 * 
	 * @param integer $class_id 報告書報告部署の部ID
	 * @param integer $attribute_id 報告書報告部署の課ID
	 * @param integer $dept_id 報告書報告部署の科ID
	 * @param integer $room_id 報告書報告部署の室ID
	 * @return boolean 利用可能な場合にtrue
	 */
	function is_report_db_readable_report_post($class_id,$attribute_id,$dept_id,$room_id)
	{
		//報告ファイルの所属参照設定を取得
		$report_db_read_div = $this->get_report_db_read_div();//"":利用不可、0:部署指定しない、1:部門、2:課、3:科、4:室
		
		if($report_db_read_div === "")
		{
			return false;
		}
		elseif($report_db_read_div === 0)
		{
			return true;
		}
		else
		{
			$emp_id = $this->get_emp_id();
			$priority_auth = $this->get_auth();//$this->get_emp_priority_auth();
			
			//最優先権限が担当者(一般ユーザー以外)の場合：担当する部署のみ可能
			if($priority_auth != 'GU')
			{
				$report_db_read_auth_flg = $this->get_report_db_read_auth_flg();

				//担当範囲
				if($report_db_read_auth_flg)
				{
					//システム全体に対する担当者の場合は全て可能
					$is_emp_auth_system_all = $this->is_emp_auth_system_all();
					if($is_emp_auth_system_all)
					{
						return true;
					}
					
					//ログインユーザーの担当部署を取得
					$emp_class_post = $this->get_emp_auth_class_post_2();
				}
				//所属範囲
				else
				{
					//ログインユーザーの所属を取得
					$emp_class_post = $this->get_emp_class_post_2();
				}
				
			}
			//最優先権限が一般ユーザーの場合：所属する部署のみ可能
			else
			{
				//ログインユーザーの所属を取得
				$emp_class_post = $this->get_emp_class_post_2();
			}

			//各階層にて、参照制限範囲外、もしくは報告部署不明、もしくは報告部署=ユーザーの部署の場合
			if(
			   ($report_db_read_div < 1 || $class_id     == "" || in_array($class_id,$emp_class_post["emp_class"]))
			&& ($report_db_read_div < 2 || $attribute_id == "" || in_array($attribute_id,$emp_class_post["emp_attribute"]))
			&& ($report_db_read_div < 3 || $dept_id      == "" || in_array($dept_id,$emp_class_post["emp_dept"]))
			&& ($report_db_read_div < 4 || $room_id      == "" || in_array($room_id,$emp_class_post["emp_room"]))
			)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	
	/**
	 * プロフィールの個人設定情報を取得します。
	 * 
	 * @return プロフィールの個人設定情報
	 */
	function get_inci_profile()
	{
		$sql  = "select * from inci_profile_use";

		$this->_db->query($sql);
		$row = $this->_db->getRow();
		
		//while ( $row = pg_fetch_array($result))
		//{
			$arr = array("use_flg" => $row["use_flg"], "class_flg" => $row["class_flg"], "attribute_flg" => $row["attribute_flg"], "dept_flg" => $row["dept_flg"], "room_flg" => $row["room_flg"]);
		//}
		return $arr;

	}
	
	/**
	 * 報告ファイルの所属参照範囲を取得します。
	 * 
	 * @return integer 報告ファイルの所属参照範囲＝"":利用不可、0:部署指定しない、1:部門、2:課、3:科、4:室
	 */
	function get_report_db_read_div()
	{
		//最優先権限において、報告ファイルの所属制限がかかっていない場合のみ利用可能。
		//※例えば、複数権限の場合にSM、病院長の場合は運用にて、病院長をSMと同じ設定にする。
		
		$priority_auth = $this->get_auth();

		if($priority_auth == "SM")
		{
			//SMの場合は無条件に使用可能
			return 0;
		}
		else
		{
			$sql = "select * from inci_auth_case where auth = '$priority_auth'";
			$this->_db->query($sql);
			
			//所属制限なし(=0)の場合のみ使用可能。
			$report_db_read_div = $this->_db->getOne('report_db_read_div');
			return $report_db_read_div;
		}
	}

	
	/**
	 * ログインユーザーの最優先権限を取得します。
	 * 一般ユーザーの場合は"GU"を返します。
	 * 
	 * @return string 最優先権限
	 */
	function get_emp_priority_auth()
	{
		//優先順位リスト
		$auth_priority_value_list = array();
		$auth_priority_value_list["SM"] = 6;
		$auth_priority_value_list["HD"] = 5;
		$auth_priority_value_list["MD"] = 4;
		$auth_priority_value_list["SD"] = 3;
		$auth_priority_value_list["RA"] = 2;
		$auth_priority_value_list["RM"] = 1;
		$auth_priority_value_list["GU"] = 0;
		
		//最優先権限を判定
		$max_auth = "GU";
		$arr = $this->get_my_inci_auth_list();
		if($arr)
		{
			for($i = 0; $i < count($arr); $i++)
			{
				$auth = $arr[$i]["auth"];
				$auth_priority_value = $auth_priority_value_list[$auth];
				$max_auth_priority_value = $auth_priority_value_list[$max_auth];
				if($auth_priority_value > $max_auth_priority_value)
				{
					$max_auth = $auth;
				}
			}
		}
		
		return $max_auth;
	}

	/**
	 * 指定ユーザーが、指定権限でシステム全体の担当者となるか判定します。
	 * 
	 * @return boolean システム全体の担当者となる場合にtrue
	 */
	function is_emp_auth_system_all()
	{
		$sql  = " select count(*) as cnt from (";
		$sql .= $this->get_sql_emp_auth_all_for_system_all();
		$sql .= " ) emp_auth_all";
			$sql .= " where auth='".$this->get_auth()."' and emp_id='".$this->get_emp_id()."'";

		$this->_db->query($sql);
		$cnt = $this->_db->getOne();
		return $cnt >= 1;
	}

	/**
	 * 職員の部署役職を取得します。（兼務所属用)
	 * 
	 * @return array $指定された職員の部署・役職コード配列
	 */
	function get_emp_class_post()
	{
		$sql  = "SELECT emp_class, emp_attribute, emp_dept, emp_room, emp_st from empmst where emp_id = '".$this->get_emp_id()."' ";
		$sql .= "union all ";
		$sql .= "select emp_class, emp_attribute, emp_dept, emp_room, emp_st from concurrent where emp_id = '".$this->get_emp_id()."' ";

		$this->_db->query($sql);
		
		return $this->_db->getAll();
	}

	/**
	 * 職員の部署役職を取得します。（兼務所属用)
	 * get_emp_class_post()で得られる情報を項目単位に集約します。
	 * 
	 * 戻り値の例
	 * array["emp_class"][0] = 1;
	 * array["emp_class"][1] = 2;
	 * array["emp_attribute"][0] = 1;
	 * array["emp_attribute"][1] = 2;
	 * array["emp_dept"][0] = 1;
	 * array["emp_dept"][1] = 2;
	 * array["emp_st"][0] = 1;※兼務も同じ役職なら1つだけ。
	 * 
	 * @param string $emp_id 職員ID
	 * @param object $con DBコネクション
	 * @param string $fname 画面名
	 * @return array $指定された職員の部署・役職コード配列
	 */
	function get_emp_class_post_2()
	{
		$result_arr = "";
		$emp_class_post = $this->get_emp_class_post();
		$key_arr = array("emp_class","emp_attribute","emp_dept","emp_room","emp_st");
		foreach($emp_class_post as $emp_class_post1)
		{
			foreach($key_arr as $key)
			{
				$val = $emp_class_post1[$key];
				if(!in_array($val,$result_arr[$key]))
				{
					$result_arr[$key][] = $val;
				}
			}
		}
		return $result_arr;
	}

	/*******************************************************
	 * 自分がなり得る権限の一覧を取得します。
	 * 得られる情報はリクエストスコープでキャッシュされます。
	 * 
	 * @return	ARRAY		inci_auth_mstのレコード構造。
	 *******************************************************/
	function get_my_inci_auth_list() { 
		// 自分がなり得る権限の一覧を取得
		if(! $this->_is_created_inci_auth_list) { 
			$sql = "select distinct inci_auth_mst.*, inci_auth_emp.auth_part ";
			$sql .= "from inci_auth_mst ";
			$sql .= "inner join (". $this->get_sql_emp_auth_all() .") inci_auth_emp on inci_auth_emp.auth = inci_auth_mst.auth ";
			$sql .= "and inci_auth_emp.emp_id = '".$this->get_emp_id()."' ";
			$sql .= "order by inci_auth_mst.auth_rank";
			$cond =  "";
			
			$this->_db->query($sql);
			$this->_my_inci_auth_list = $this->_db->getAll();
		}
		
		// リクエストをキャッシュしておく
		$this->_is_created_inci_auth_list = true;
		
		return $this->_my_inci_auth_list;
	}
	
	/**
	 * 報告ファイルの参照権限の所属/担当範囲判定フラグを取得します。
	 * 
	 * @return boolean trueの場合に担当範囲指定
	 */
	function get_report_db_read_auth_flg()
	{
		$priority_auth = $this->get_auth();//$this->get_emp_priority_auth();
		if($priority_auth == "SM")
		{
			//SMの場合はこの判定は無意味。ダミー値返却。
			return false;
		}
		else
		{
			$sql = "select report_db_read_auth_flg from inci_auth_case where auth = '$priority_auth'";

			$this->_db->query($sql);
			//所属制限なし(=0)の場合のみ使用可能。
			$report_db_read_auth_flg = $this->_db->getOne();
			
			return $report_db_read_auth_flg == "t";
		}
	}

	/**
	 * 役職指定を含め、権限ユーザーになり得る担当者の権限情報取得用ＳＱＬを返します。
	 * 
	 * @return string inci_auth_empテーブルと同等の構造となるemp_auth_allテーブルを得るSQL文。
	 */
	function get_sql_emp_auth_all()
	{
		$sql = ""

		." "
		."  select distinct auth, auth_part, emp_id"
		."  from"
		."  ("
	//	."      /* 個人指定の権限ユーザー一覧 */"
		."      ("
		."          select * from inci_auth_emp"
		."      )"
		."      union"
	//	."      /* 役職指定の権限ユーザー一覧 */"
		."      ("
		."          select "
		."              auth_info.auth, "
		."              auth_info.auth_part, "
		."              all_emp.emp_id"
		."          from "
	//	."              /* 役職権限情報一覧 */"
		."              ("
		."                  select inci_auth_emp_post.*, inci_auth_emp_st.emp_st"
		."                  from inci_auth_emp_post"
		."                  left join inci_auth_emp_st "
		."                      on not inci_auth_emp_post.all_st_flg"
		."                      and inci_auth_emp_post.auth = inci_auth_emp_st.auth"
		."                      and inci_auth_emp_post.auth_part = inci_auth_emp_st.auth_part"
		."                  where inci_auth_emp_post.use_flg"
		."              ) auth_info"
		."          "
		."          left join "
	//	."              /* 論理削除されていない全ユーザー一覧(所属兼務対応) */"
		."              ("
		."                  select empmst2.emp_id,empmst2.emp_st"
		."                  from (select emp_id,emp_st from empmst union select emp_id,emp_st from concurrent) empmst2"
		."                  left join authmst on empmst2.emp_id = authmst.emp_id"
		."                  where not authmst.emp_del_flg"
		."              ) all_emp"
	//	."              /* 権限条件を満たしているユーザーのみ結合(部署は無視) */"
		."              on "
		."              ("
	//	."                  /* 権限指定されている役職のユーザーに限定 */"
		."                  auth_info.all_st_flg"
		."                  or all_emp.emp_st = auth_info.emp_st"
		."              )"
		."          where not all_emp.emp_id is null"
		."      )"
		."      union"
	//	."      /* リスクマネージャ役職権限ユーザー一覧 */"
		."      ("
		."          select auth_info.auth, auth_info.auth_part ,rm_emp.emp_id"
		."          from"
	//	."              /* リスクマネージャ役職権限を対象とする役職権限情報一覧 */"
		."              ("
		."                  select auth, auth_part, use_flg, target_class "
		."                  from inci_auth_emp_post "
		."                  where use_flg and rm_flg"
		."              ) auth_info"
		."          left join "
	//	."              /* リスクマネージャ役職権限ユーザー一覧 */"
		."              ("
		."                  select * from inci_rm where post_id = 1"
		."              ) rm_emp"
	//	."              /* 権限条件を満たしているユーザーのみ結合 */"
		."              on"
		."              ("
		."                  auth_info.target_class = 1"
		."                  and not rm_emp.class_id is null"
		."                  and     rm_emp.attribute_id is null"
		."              )"
		."              or"
		."              ("
		."                  auth_info.target_class = 2"
		."                  and not rm_emp.attribute_id is null"
		."                  and     rm_emp.dept_id is null"
		."              )"
		."              or"
		."              ("
		."                  auth_info.target_class = 3"
		."                  and not rm_emp.dept_id is null"
		."                  and     rm_emp.room_id is null"
		."              )"
		."              or"
		."              ("
		."                  auth_info.target_class = 4"
		."                  and not rm_emp.room_id is null"
		."              )"
	// 最下層が無い場合、上位の権限マスタを閲覧する 20090916
		."              or"
		."              ("
		."                  auth_info.target_class = 4"
		."                  and not rm_emp.dept_id is null"
		."                  and     rm_emp.room_id is null"
		."              )"
	//	."              /* リスクマネージャ役職権限ユーザーが未設定の場合は担当者なし */"
		."          where not rm_emp.emp_id is null"
		."      )"
		."      union"
	//	."      /* リスク管理者役職権限ユーザー一覧 */"
		."      ("
		."          select auth_info.auth, auth_info.auth_part ,rm_emp.emp_id"
		."          from"
	//	."              /* リスク管理者役職権限を対象とする役職権限情報一覧 */"
		."              ("
		."                  select auth, auth_part, use_flg, target_class "
		."                  from inci_auth_emp_post "
		."                  where use_flg and ra_flg"
		."              ) auth_info"
		."          left join "
	//	."              /* リスク管理者役職権限ユーザー一覧 */"
		."              ("
		."                  select * from inci_rm where post_id = 2"
		."              ) rm_emp"
	//	."              /* 権限条件を満たしているユーザーのみ結合 */"
		."              on"
		."              ("
		."                  auth_info.target_class = 1"
		."                  and not rm_emp.class_id is null"
		."                  and     rm_emp.attribute_id is null"
		."              )"
		."              or"
		."              ("
		."                  auth_info.target_class = 2"
		."                  and not rm_emp.attribute_id is null"
		."                  and     rm_emp.dept_id is null"
		."              )"
		."              or"
		."              ("
		."                  auth_info.target_class = 3"
		."                  and not rm_emp.dept_id is null"
		."                  and     rm_emp.room_id is null"
		."              )"
		."              or"
		."              ("
		."                  auth_info.target_class = 4"
		."                  and not rm_emp.room_id is null"
		."              )"
	// 最下層が無い場合、上位の権限マスタを閲覧する 20090916
		."              or"
		."              ("
		."                  auth_info.target_class = 4"
		."                  and not rm_emp.dept_id is null"
		."                  and     rm_emp.room_id is null"
		."              )"
	//	."              /* リスク管理者役職権限ユーザーが未設定の場合は担当者なし */"
		."          where not rm_emp.emp_id is null"
		."      )"
		."      union"
	//	."      /* 部門長役職権限ユーザー一覧 */"
		."      ("
		."          select auth_info.auth, auth_info.auth_part ,rm_emp.emp_id"
		."          from"
	//	."              /* 部門長役職権限を対象とする役職権限情報一覧 */"
		."              ("
		."                  select auth, auth_part, use_flg, target_class "
		."                  from inci_auth_emp_post "
		."                  where use_flg and sd_flg"
		."              ) auth_info"
		."          left join "
	//	."              /* 部門長役職権限ユーザー一覧 */"
		."              ("
		."                  select * from inci_rm where post_id = 4"
		."              ) rm_emp"
	//	."              /* 権限条件を満たしているユーザーのみ結合 */"
		."              on"
		."              ("
		."                  auth_info.target_class = 1"
		."                  and not rm_emp.class_id is null"
		."                  and     rm_emp.attribute_id is null"
		."              )"
		."              or"
		."              ("
		."                  auth_info.target_class = 2"
		."                  and not rm_emp.attribute_id is null"
		."                  and     rm_emp.dept_id is null"
		."              )"
		."              or"
		."              ("
		."                  auth_info.target_class = 3"
		."                  and not rm_emp.dept_id is null"
		."                  and     rm_emp.room_id is null"
		."              )"
		."              or"
		."              ("
		."                  auth_info.target_class = 4"
		."                  and not rm_emp.room_id is null"
		."              )"
	// 最下層が無い場合、上位の権限マスタを閲覧する 20090916
		."              or"
		."              ("
		."                  auth_info.target_class = 4"
		."                  and not rm_emp.dept_id is null"
		."                  and     rm_emp.room_id is null"
		."              )"
	//	."              /* 部門長役職権限ユーザーが未設定の場合は担当者なし */"
		."          where not rm_emp.emp_id is null"
		."      )"
		."      union"
	//	."      /* メディカルディレクター役職権限ユーザー一覧 */"
		."      ("
		."          select auth_info.auth, auth_info.auth_part ,rm_emp.emp_id"
		."          from"
	//	."              /* メディカルディレクター役職権限を対象とする役職権限情報一覧 */"
		."              ("
		."                  select auth, auth_part, use_flg, target_class "
		."                  from inci_auth_emp_post "
		."                  where use_flg and md_flg"
		."              ) auth_info"
		."          left join "
	//	."              /* メディカルディレクター役職権限ユーザー一覧 */"
		."              ("
		."                  select * from inci_rm where post_id = 3"
		."              ) rm_emp"
	//	."              /* 権限条件を満たしているユーザーのみ結合 */"
		."              on"
		."              ("
		."                  auth_info.target_class = 1"
		."                  and not rm_emp.class_id is null"
		."                  and     rm_emp.attribute_id is null"
		."              )"
		."              or"
		."              ("
		."                  auth_info.target_class = 2"
		."                  and not rm_emp.attribute_id is null"
		."                  and     rm_emp.dept_id is null"
		."              )"
		."              or"
		."              ("
		."                  auth_info.target_class = 3"
		."                  and not rm_emp.dept_id is null"
		."                  and     rm_emp.room_id is null"
		."              )"
		."              or"
		."              ("
		."                  auth_info.target_class = 4"
		."                  and not rm_emp.room_id is null"
		."              )"
	// 最下層が無い場合、上位の権限マスタを閲覧する 20090916
		."              or"
		."              ("
		."                  auth_info.target_class = 4"
		."                  and not rm_emp.dept_id is null"
		."                  and     rm_emp.room_id is null"
		."              )"
	//	."              /* メディカルディレクター役職権限ユーザーが未設定の場合は担当者なし */"
		."          where not rm_emp.emp_id is null"
		."      )"
		."      union"
	//	."      /* 病院長役職権限ユーザー一覧 */"
		."      ("
		."          select auth_info.auth, auth_info.auth_part ,rm_emp.emp_id"
		."          from"
	//	."              /* 病院長役職権限を対象とする役職権限情報一覧 */"
		."              ("
		."                  select auth, auth_part, use_flg, target_class "
		."                  from inci_auth_emp_post "
		."                  where use_flg and hd_flg"
		."              ) auth_info"
		."          left join "
	//	."              /* 病院長役職権限ユーザー一覧 */"
		."              ("
		."                  select * from inci_rm where post_id = 5"
		."              ) rm_emp"
	//	."              /* 権限条件を満たしているユーザーのみ結合 */"
		."              on"
		."              ("
		."                  auth_info.target_class = 1"
		."                  and not rm_emp.class_id is null"
		."                  and     rm_emp.attribute_id is null"
		."              )"
		."              or"
		."              ("
		."                  auth_info.target_class = 2"
		."                  and not rm_emp.attribute_id is null"
		."                  and     rm_emp.dept_id is null"
		."              )"
		."              or"
		."              ("
		."                  auth_info.target_class = 3"
		."                  and not rm_emp.dept_id is null"
		."                  and     rm_emp.room_id is null"
		."              )"
		."              or"
		."              ("
		."                  auth_info.target_class = 4"
		."                  and not rm_emp.room_id is null"
		."              )"
	// 最下層が無い場合、上位の権限マスタを閲覧する 20090916
		."              or"
		."              ("
		."                  auth_info.target_class = 4"
		."                  and not rm_emp.dept_id is null"
		."                  and     rm_emp.room_id is null"
		."              )"
	//	."              /* 病院長役職権限ユーザーが未設定の場合は担当者なし */"
		."          where not rm_emp.emp_id is null"
		."      )"
		."  ) emp_auth_all"


		." ";
		
		//利用しない権限を除去
		$sql = $this->get_sql_emp_auth_use_only($sql);
		
		//代行者を追加
		$sql = $this->get_sql_emp_auth_add_agent($sql);
		
		//ファントルくん利用権限がない人を除去
		$sql = $this->get_sql_emp_auth_user_only($sql);
		
		//余分なスペース除去
		$sql = preg_replace('/\\s+/',' ',$sql);

		return $sql;
	}

	/**
	 * (private)get_sql_emp_auth()系のＳＱＬから使用可能な権限のみを抽出します。
	 * 
	 * @param string $sql get_sql_emp_auth()系のＳＱＬ文
	 * @return string inci_auth_empテーブルと同等の構造となるemp_auth_allテーブルを得るSQL文。
	 */
	function get_sql_emp_auth_use_only($sql)
	{
		$sql = ""

		."select * from"
		."("
		."    select inci_auth_all.* from "
		."        ( $sql ) inci_auth_all"
		."    inner join inci_auth_mst "
		."    on inci_auth_all.auth = inci_auth_mst.auth and inci_auth_mst.use_flg"
		.") inci_auth_all"

		." ";
		return $sql;
	}

	/**
	 * (private)get_sql_emp_auth()系のＳＱＬに代行者を追加します。
	 * 但し、代行者はSQL上、未追加であることを前提とします。
	 * 
	 * @param string $sql get_sql_emp_auth()系のＳＱＬ文
	 * @return string inci_auth_empテーブルと同等の構造となるemp_auth_allテーブルを得るSQL文。
	 */
	function get_sql_emp_auth_add_agent($sql)
	{
		$sql = ""

		."select * from"
		."("
		."    select * from "
		."        ( $sql ) inci_auth_all"
		."    union"
		."    ("
		."        select distinct"
		."            inci_auth_all.auth,"
		."            varchar(6) '代行者' as auth_part,"
		."            inci_auth_agent_sub.agent_emp_id as emp_id"
		."        from"
		."            ( $sql ) inci_auth_all"
		."        left join (select * from inci_auth_agent join authmst on agent_emp_id=emp_id where emp_del_flg='f') inci_auth_agent_sub"
		."            on  inci_auth_all.auth in(select auth from inci_agent_use where agent_use_flg)"
		."            and inci_auth_all.auth_part = '決裁者'"
		."            and inci_auth_all.emp_id = inci_auth_agent_sub.deciding_emp_id"
		."            and ("
		."                   inci_auth_agent_sub.start_date is null "
		."                or inci_auth_agent_sub.start_date <= translate('01235689', '0123456789', current_date::text) "
		."                )"
		."            and ("
		."                   inci_auth_agent_sub.end_date is null "
		."                or inci_auth_agent_sub.end_date   >= translate('01235689', '0123456789', current_date::text) "
		."                )"
		."        where not inci_auth_agent_sub.agent_emp_id is null"
		."    )"
		.") inci_auth_all"

		." ";
		return $sql;
	}
	
	/**
	 * (private)get_sql_emp_auth()系のＳＱＬからファントルくんユーザーのみを抽出します。
	 * 
	 * @param string $sql get_sql_emp_auth()系のＳＱＬ文
	 * @return string inci_auth_empテーブルと同等の構造となるemp_auth_allテーブルを得るSQL文。
	 */
	function get_sql_emp_auth_user_only($sql)
	{
		$sql = ""

		."select * from"
		."("
		."    select inci_auth_all.* from "
		."        ( $sql ) inci_auth_all"
		."    natural inner join (select emp_id from authmst where emp_inci_flg) usable "
		.") inci_auth_all"

		." ";
		return $sql;
	}
	
	/**
	 * 個人指定によるシステム全体に対する担当者の権限情報取得要ＳＱＬを返します。
	 * (つまり、部署によっては担当者になれない人は除外されます。)
	 * 
	 * @return string inci_auth_empテーブルと同等の構造となるemp_auth_allテーブルを得るSQL文。
	 */
	function get_sql_emp_auth_all_for_system_all()
	{
		//個人指定の権限ユーザー一覧
		$sql = "select * from inci_auth_emp";
		
		//利用しない権限を除去
		$sql = $this->get_sql_emp_auth_use_only($sql);
		
		//代行者を追加
		$sql = $this->get_sql_emp_auth_add_agent($sql);
		
		//ファントルくん利用権限がない人を除去
		$sql = $this->get_sql_emp_auth_user_only($sql);
		
		//余分なスペース除去
		$sql = preg_replace('/\\s+/',' ',$sql);
		
		return $sql;
	}
	
	//この関数は環境によってはコストがかかる可能性あり。
	//但し、パフォーマンスアップも可能と思われる。(権限系のメンテナンスに影響ありか。)
	/**
	 * 指定ユーザーが、指定権限で担当者となる部署の一覧を取得します。
	 * ※get_emp_class_post()と同様の形式の結果を返します。
	 * 　但し、emp_stは省きます。
	 * 得られる情報はリクエストスコープでキャッシュされます。
	 * 
	 * @return array $指定された職員の部署コード配列
	 */
	function get_emp_auth_class_post()
	{
		if(! $this->_is_created_emp_auth_class_post["emp_id=".$this->get_emp_id()]["auth=".$this->get_auth()])
		{
			$ret = null;
			
			//所属可能な全部署情報を取得
			$all_entryable_post_info = $this->get_all_entryable_post_info();
			
			//考慮すべきユーザーを特定(＝本人＋本人を代行者指定している担当者)
			$sql  = " select deciding_emp_id as emp_id from inci_auth_agent";
			$sql .= " where agent_emp_id = '".$this->get_emp_id()."'";
			$sql .= " and (";
			$sql .= " inci_auth_agent.start_date is null";
			$sql .= " or inci_auth_agent.start_date <= translate('01235689', '0123456789', current_date::text)";
			$sql .= " )";
			$sql .= " and (";
			$sql .= " inci_auth_agent.end_date is null";
			$sql .= " or inci_auth_agent.end_date   >= translate('01235689', '0123456789', current_date::text)";
			$sql .= " )";
			
			$this->_db->query($sql);
			$data = $this->_db->getAll();
			$emp_id_list = "'".$this->get_emp_id()."'";
			if($data != "")
			{
				foreach($data as $data1)
				{
					$emp_id_list .= ",'".$data1['emp_id']."'";
				}
			}

			//システム全体に対する担当者であるかの判定
			$sql  = " select count(*) as cnt from inci_auth_emp where auth='".$this->get_auth()."' and emp_id in($emp_id_list)";
			$this->_db->query($sql);
			$is_system_all_auth = $this->_db->getOne() != 0;
			
			//システム全体に対する担当者の場合は全部署が返却値となる。
			if($is_system_all_auth)
			{
				foreach($all_entryable_post_info as $all_entryable_post_info1)
				{
					$a_class_id = $all_entryable_post_info1['class_id'];
					$a_atrb_id  = $all_entryable_post_info1['atrb_id'];
					$a_dept_id  = $all_entryable_post_info1['dept_id'];
					$a_room_id  = $all_entryable_post_info1['room_id'];
					$ret[] = array("emp_class"=>$a_class_id,"emp_attribute"=>$a_atrb_id,"emp_dept"=>$a_dept_id,"emp_room"=>$a_room_id);
				}
			}
			//システム全体に対する担当者ではない場合
			else
			{
				//権限に対する役職指定の設定情報を取得
				$sql  = " select * from inci_auth_emp_post where auth='".$this->get_auth()."' and use_flg";
				$this->_db->query($sql);
				$auth_emp_post = $this->_db->getAll();
				
				//権限に対して役職指定が行われている場合
				if($auth_emp_post != "")
				{
					//考慮すべきユーザーのうち、誰か一人でも所属(兼務所属を含む)している部署役職情報を取得
					$sql  = " select distinct emp_class,emp_attribute,emp_dept,emp_room,emp_st from empmst";
					$sql .= " where emp_id in($emp_id_list)";
					$sql .= " union";
					$sql .= " select distinct emp_class,emp_attribute,emp_dept,emp_room,emp_st from concurrent";
					$sql .= " where emp_id in($emp_id_list)";

					$this->_db->query($sql);
					$emp_post_list = $this->_db->getAll();
					
					//各役職指定の情報に対して(最大で決裁者と審議者の２ループ発生する。)
					foreach($auth_emp_post as $auth_emp_post1)
					{
						$auth_part = $auth_emp_post1['auth_part'];
						$target_class = $auth_emp_post1['target_class'];
						$all_st_flg = $auth_emp_post1['all_st_flg'];
						$rm_flg = $auth_emp_post1['rm_flg'];
						$ra_flg = $auth_emp_post1['ra_flg'];
						$md_flg = $auth_emp_post1['md_flg'];
						$sd_flg = $auth_emp_post1['sd_flg'];
						$hd_flg = $auth_emp_post1['hd_flg'];

						//指定役職を取得
						$st_list = "";
						if($all_st_flg != "t")
						{
							$sql  = " select emp_st from inci_auth_emp_st where auth = '".$this->get_auth()."' and auth_part = '$auth_part'";
							$this->_db->query($sql);
							$data = $this->_db->getAll();
							
							$st_list = null;
							foreach($data as $data1)
							{
								$st_list[] = $data1['emp_st'];
							}
							if($st_list == "")
							{
								$st_list[] = "nothing";
							}
						}
						
						//通常の役職指定に対する処理
						//全部署ループ
						foreach($all_entryable_post_info as $all_entryable_post_info1)
						{
							$a_class_id = $all_entryable_post_info1['class_id'];
							$a_atrb_id  = $all_entryable_post_info1['atrb_id'];
							$a_dept_id  = $all_entryable_post_info1['dept_id'];
							$a_room_id  = $all_entryable_post_info1['room_id'];

							//考慮すべきユーザーの所属部署ループ
							$is_auth_post = false;
							foreach($emp_post_list as $emp_post_list1)
							{
								$e_class_id = $emp_post_list1['emp_class'];
								$e_atrb_id  = $emp_post_list1['emp_attribute'];
								$e_dept_id  = $emp_post_list1['emp_dept'];
								$e_room_id  = $emp_post_list1['emp_room'];
								$e_st_id    = $emp_post_list1['emp_st'];
								
								//全部署ループが、「指定階層レベル」で、「考慮すべきユーザーの所属部署と同じ」で、
								//「その部署におけるユーザーの役職が権限指定されている役職に含まれている」場合、
								//その部署を返却値追加対象とする。
								if($st_list == "" || in_array($e_st_id,$st_list))
								{
									if($target_class == 1 && $a_class_id == $e_class_id)
									{
										$is_auth_post = true;
									}
									elseif($target_class == 2 && $a_atrb_id == $e_atrb_id)
									{
										$is_auth_post = true;
									}
									elseif(($target_class == 3 || $target_class == 4) && $a_dept_id == $e_dept_id)
									{
										$is_auth_post = true;
									}
									elseif($target_class == 4 && $a_room_id == $e_room_id)
									{
										$is_auth_post = true;
									}
								}
							}
							
							//返却値追加対象の部署の場合は返却値に追加
							if($is_auth_post)
							{
								$ret[] = array("emp_class"=>$a_class_id,"emp_attribute"=>$a_atrb_id,"emp_dept"=>$a_dept_id,"emp_room"=>$a_room_id);
							}
						}
						
						//安全管理役職指定に対する処理
						if($this->get_auth() == 'RM' && $rm_flg == "t")
						{
							$post_id = 1;
						}
						elseif($this->get_auth() == 'RA' && $ra_flg == "t")
						{
							$post_id = 2;
						}
						elseif($this->get_auth() == 'MD' && $md_flg == "t")
						{
							$post_id = 3;
						}
						elseif($this->get_auth() == 'SD' && $sd_flg == "t")
						{
							$post_id = 4;
						}
						elseif($this->get_auth() == 'HD' && $hd_flg == "t")
						{
							$post_id = 5;
						}
						else
						{
							$post_id = 0;
						}
						//安全管理役職指定が行われている場合
						if($post_id != 0)
						{
							//考慮すべきユーザーが、対象の安全管理役職に指定されている情報を取得
							$sql  = " select class_id,attribute_id,dept_id,room_id from inci_rm where post_id = $post_id and emp_id in($emp_id_list)";
							$this->_db->query($sql);
							$anzenkanri_list = $this->_db->getAll();

							//考慮すべきユーザーが、対象の安全管理役職に指定されている場合
							if($anzenkanri_list != "")
							{
								//各安全管理役職指定において
								foreach($anzenkanri_list as $anzenkanri_list1)
								{
									$s_class_id = $anzenkanri_list1['class_id'];
									$s_atrb_id  = $anzenkanri_list1['attribute_id'];
									$s_dept_id  = $anzenkanri_list1['dept_id'];
									$s_room_id  = $anzenkanri_list1['room_id'];
								
									//指定階層レベルでその設定が有効な場合
									//その部署を返却値追加対象とする。
									$is_auth_post = false;
									if($target_class == 1 && $s_class_id != "" && $s_atrb_id == "")
									{
										$is_auth_post = true;
									}
									elseif($target_class == 2 && $s_atrb_id != "" && $s_dept_id == "")
									{
										$is_auth_post = true;
									}
									elseif(($target_class == 3 || $target_class == 4) && $s_dept_id != "" && $s_room_id == "")
									{
										$is_auth_post = true;
									}
									elseif($target_class == 4 && $s_room_id != "")
									{
										$is_auth_post = true;
									}
							
									//返却値追加対象の部署の場合は返却値に追加
									if($is_auth_post)
									{
										foreach($all_entryable_post_info as $all_entryable_post_info1)
										{
											$a_class_id = $all_entryable_post_info1['class_id'];
											$a_atrb_id  = $all_entryable_post_info1['atrb_id'];
											$a_dept_id  = $all_entryable_post_info1['dept_id'];
											$a_room_id  = $all_entryable_post_info1['room_id'];
											if(
												   ($s_class_id == $a_class_id)
												&& ($s_atrb_id == "" || $s_atrb_id == $a_atrb_id)
												&& ($s_dept_id == "" || $s_dept_id == $a_dept_id)
												&& ($s_room_id == "" || $s_room_id == $a_room_id)
											)
											{
												$ret[] = array("emp_class"=>$a_class_id,"emp_attribute"=>$a_atrb_id,"emp_dept"=>$a_dept_id,"emp_room"=>$a_room_id);
											}
										}
									}
								}
							}
						}
					}
					
					//決裁者、審議者の通常の役職、安全管理役職でエントリーした部署のうち、重複した部署を取り除く
					$ret_wk = array();
					foreach($ret as $ret1)
					{
						$is_entryed = false;
						foreach($ret_wk as $ret_wk1)
						{
							if( $ret1['emp_class'] == $ret_wk1['emp_class'] 
							 && $ret1['emp_attribute'] == $ret_wk1['emp_attribute'] 
							 && $ret1['emp_dept'] == $ret_wk1['emp_dept'] 
							 && $ret1['emp_room'] == $ret_wk1['emp_room'] )
							{
								$is_entryed = true;
								break;
							}
						}
						if(!$is_entryed)
						{
							$ret_wk[] = $ret1;
						}
					}
					$ret = $ret_wk;
					
				}
			}

			$this->_emp_auth_class_post["emp_id=".$this->get_emp_id()]["auth=".$this->get_auth()] = $ret;
			$this->_is_created_emp_auth_class_post["emp_id=".$this->get_emp_id()]["auth=".$this->get_auth()] = true;
		}
		return $this->_emp_auth_class_post["emp_id=".$this->get_emp_id()]["auth=".$this->get_auth()];
	}

	/**
	 * 職員が所属可能な全所属情報を取得します。
	 */
	function get_all_entryable_post_info()
	{
		/* 部は必須 */
		$sql  = " select * from";
		$sql .= " (select class_id,class_nm from classmst where not class_del_flg) c";
		/* 課は必須 */
		$sql .= " natural inner join";
		$sql .= " (select class_id,atrb_id,atrb_nm from atrbmst where not atrb_del_flg) a";
		/* 科は必須 */
		$sql .= " natural inner join";
		$sql .= " (select atrb_id,dept_id,dept_nm from deptmst where not dept_del_flg) d";
		/* 室は非必須／３階層ケースあり */
		$sql .= " natural left join";
		$sql .= " (";
		$sql .= " select dept_id,room_id,room_nm from classroom where not room_del_flg";
		$sql .= " union";
		$sql .= " select dept_id,null as room_id,null as room_nm from (select distinct(dept_id) from classroom where not room_del_flg) a";
		$sql .= " ) r";

		$sql .= " order by class_id,atrb_id,dept_id,room_id";

		$this->_db->query($sql);
		$all_post_info = $this->_db->getAll();
		return $all_post_info;
	}

	/**
	 * 指定ユーザーが、指定権限で担当者となる部署の一覧を取得します。
	 * ※get_emp_class_post_2()と同様の形式の結果を返します。
	 * 　但し、emp_stは省きます。
	 * 
	 * 戻り値の例
	 * array["emp_class"][0] = 1;
	 * array["emp_class"][1] = 2;
	 * array["emp_attribute"][0] = 1;
	 * array["emp_attribute"][1] = 2;
	 * array["emp_dept"][0] = 1;
	 * array["emp_dept"][1] = 2;
	 * 
	 * @param string $emp_id 職員ID
	 * @param string $auth 権限
	 * @param object $con DBコネクション
	 * @param string $fname 画面名
	 * @return array $指定された職員の部署コード配列
	 */
	function get_emp_auth_class_post_2()
	{
		$result_arr = array();
		$emp_class_post = $this->get_emp_auth_class_post();
		$key_arr = array("emp_class","emp_attribute","emp_dept","emp_room");
		foreach($emp_class_post as $emp_class_post1)
		{
			foreach($key_arr as $key)
			{
				$val = $emp_class_post1[$key];
				if(!in_array($val,$result_arr[$key]))
				{
					$result_arr[$key][] = $val;
				}
			}
		}
		return $result_arr;
	}

}
?>
