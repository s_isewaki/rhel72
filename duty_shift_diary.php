<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成｜勤務日誌</title>

<?
//ini_set("display_errors","1");
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");

require_once("duty_shift_common.ini");
require_once("duty_shift_user_tab_common.ini");
require_once("duty_shift_common_class.php");
require_once("duty_shift_diary_common.ini");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//ユーザ画面用
$chk_flg = $obj->check_authority_user($session, $fname);
if ($chk_flg == "") {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
//管理画面用
$section_admin_auth = $obj->check_authority_Management($session, $fname);
///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
	///-----------------------------------------------------------------------------
	//初期値設定
	///-----------------------------------------------------------------------------
	$err_flg_1 = "";
	$person_charge_id = "";		//シフト管理者ID（責任者）
	$caretaker_id = "";			//シフト管理者ID（代行者）
	$caretaker2_id = "";		//シフト管理者ID（代行者）
	$caretaker3_id = "";		//シフト管理者ID（代行者）
	$caretaker4_id = "";		//シフト管理者ID（代行者）
	$caretaker5_id = "";
	$caretaker6_id = "";
	$caretaker7_id = "";
	$caretaker8_id = "";
	$caretaker9_id = "";
	$caretaker10_id = "";

	// 期間
	if ($start_yr == "") {
		$start_yr = date('Y');
	}
	if ($start_mon == "") {
		$start_mon = date('m');
	}
	if ($start_day == "") {
		$start_day = date('d');
	}
	if ($end_yr == "") {
		$end_yr = date('Y');
	}
	if ($end_mon == "") {
		$end_mon = date('m');
	}
	if ($end_day == "") {
		$end_day = date('d');
	}

	$arr_title = array(
		"シフト<br>グループ",	//0
		"年月日",
		"職員ID",
		"職種",
		"役職",
		"氏名",					//5
		"勤務シフト<br>予定",
		"出勤予定",
		"事由予定",
		"勤務シフト<br>実績",
		"出勤実績",				//10
		"事由実績",
		"出勤<br>時刻",
		"退勤<br>時刻",
		"会議<br>研修<br>時間",
		"部署"					//15
	);

	///-----------------------------------------------------------------------------
	// ログインユーザの職員ID・氏名を取得
	///-----------------------------------------------------------------------------
	$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
	$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");
	$emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
	///-----------------------------------------------------------------------------
	//ＤＢ(empmst)より職員情報を取得
	//ＤＢ(wktmgrp)より勤務パターン情報を取得
	///-----------------------------------------------------------------------------
//	$data_emp = $obj->get_empmst_array("");
	$data_emp = array();	//以下の処理で未使用のため
	$data_wktmgrp = $obj->get_wktmgrp_array();

	// 更新権限のあるシフトグループ数
	$create_flg_cnt = 0;
	///-----------------------------------------------------------------------------
	// 勤務シフトグループ情報を取得
	///-----------------------------------------------------------------------------
	$group_array = $obj->get_duty_shift_group_array("", $data_emp, $data_wktmgrp);
	// 更新権限あり、職員設定されているグループ取得
	$group_array = $obj->get_valid_group_array($group_array, $emp_id, "", "");
	if (count($group_array) <= 0) {
		$err_flg_1 = "1";
	} else {

		//更新権限があるグループを設定

		for ($i=0; $i<count($group_array); $i++) {
			$person_charge_id = $group_array[$i]["person_charge_id"];			//シフト管理者ID（責任者）
			$caretaker_id = $group_array[$i]["caretaker_id"];					//シフト管理者ID（代行者）
			$caretaker2_id = $group_array[$i]["caretaker2_id"];
			$caretaker3_id = $group_array[$i]["caretaker3_id"];
			$caretaker4_id = $group_array[$i]["caretaker4_id"];
			$caretaker5_id = $group_array[$i]["caretaker5_id"];
			$caretaker6_id = $group_array[$i]["caretaker6_id"];
			$caretaker7_id = $group_array[$i]["caretaker7_id"];	
			$caretaker8_id = $group_array[$i]["caretaker8_id"];
			$caretaker9_id = $group_array[$i]["caretaker9_id"];
			$caretaker10_id = $group_array[$i]["caretaker10_id"];	
		
			if (($emp_id == $person_charge_id) || ($emp_id == $caretaker_id) || ($emp_id == $caretaker2_id) || ($emp_id == $caretaker3_id) || ($emp_id == $caretaker4_id) || ($emp_id == $caretaker5_id) || ($emp_id == $caretaker6_id) || ($emp_id == $caretaker7_id) || ($emp_id == $caretaker8_id) || ($emp_id == $caretaker9_id) || ($emp_id == $caretaker10_id)) {
				$group_array[$i]["create_flg"] = "1";
				$create_flg_cnt++;
			}
		}

		///-----------------------------------------------------------------------------
		//勤務シフトグループ情報ＤＢより情報取得
		///-----------------------------------------------------------------------------
		if ($group_id == ""){
			$group_id = $group_array[0]["group_id"];
		}
	}

	if ($action == "表示") {
		$start_date = $start_yr.$start_mon.$start_day;
		$end_date = $end_yr.$end_mon.$end_day;
		$data_atdptn_all = $obj->get_atdptn_array("");
		$data_pattern_all = $obj->get_duty_shift_pattern_array("", $data_atdptn_all);

		$reason_2_array = $obj->get_reason_2("1");

		$data = get_diary_data($con, $fname, $group_id, $group_array, $start_date, $end_date, $data_pattern_all, $reason_2_array, $data_atdptn_all);
	}
?>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function downloadCSV() {
	document.csv.action = 'duty_shift_diary_csv.php';
	document.csv.start_date.value = "" + document.mainform.start_yr.value + document.mainform.start_mon.value +document.mainform.start_day.value;
	document.csv.end_date.value = "" + document.mainform.end_yr.value + document.mainform.end_mon.value + document.mainform.end_day.value;
	document.csv.submit();
}
</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 画面遷移／タブ -->
	<!-- ------------------------------------------------------------------------ -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
	<?
		// 画面遷移
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		show_user_title($session, $section_admin_auth);			//duty_shift_common.ini
		echo("</table>\n");

		// タブ
		if ($group_id == "all") {
			$wk_group_id = "";
		} else {
			$wk_group_id = $group_id;
		}
		$arr_option = "&group_id=" . $wk_group_id;
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		show_user_tab_menuitem($session, $fname, $arr_option);	//duty_shift_user_tab_common.ini
		echo("</table>\n");

		// 下線
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td bgcolor=\"#5279a5\"><img src=\"img/spacer.gif\" width\"1\" height=\"2\" alt=\"\"></td></tr></table><img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
	?>
	</td></tr></table>
	<!-- ------------------------------------------------------------------------ -->
	<!-- データ不備時 -->
	<!-- ------------------------------------------------------------------------ -->
	<?
	if ($err_flg_1 == "1") {
		echo("勤務シフトグループ情報が未設定です。管理画面で登録してください。");
	} elseif ($create_flg_cnt == 0) {
		echo("更新権限のあるシフトグループがありません。");
	} else {
	?>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 入力エリア -->
	<!-- ------------------------------------------------------------------------ -->
	<form name="mainform" method="post">
		<table width="960" border="0" cellspacing="0" cellpadding="2" class="">
		<tr>
		<td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="duty_shift_diary_bld_manage.php?session=<?=$session?>">病棟管理日誌</a></font></td>
		<td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>勤務データ出力</b></font></td>
		<td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="duty_shift_diary_allotment.php?session=<?=$session?>">勤務分担表</a></font></td>
		<td align="left" width="140"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="duty_shift_diary_sfc.php?session=<?=$session?>">管理日誌（SFC形式）</a></font></td>
		<td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="duty_shift_diary_list.php?session=<?=$session?>">在院者一覧</a></font></td>
		<td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="duty_shift_diary_duty.php?session=<?=$session?>">当直者一覧</a></font></td>
	<td align="left" width=""></td>
		</tr>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 勤務シフトグループ名 -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="960" border="0" cellspacing="0" cellpadding="2">
		<tr height="22">
		<!-- 勤務シフトグループ名 -->
		<td width="250" align="left" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">シフトグループ</font>
		<select name="group_id">
		<option value="all">全て</option>
		<?
		for($i=0;$i<count($group_array);$i++) {
			//更新権限確認
			if ($group_array[$i]["create_flg"] != "1") {
				continue;
			}
			$wk_id= $group_array[$i]["group_id"];
			$wk_name = $group_array[$i]["group_name"];
			echo("<option value=\"$wk_id\"");
			if ($group_id == $wk_id) {
				echo(" selected");
			}
			echo(">$wk_name\n");
		}
		?>
		</select>&nbsp;</td>
		<td width="540" align="left" nowrap>
		<?
		// 期間
		?>
		<select id='start_yr' name='start_yr'><? 
		show_select_years(15, $start_yr, true, true);
		?></select>年
		<select id='start_mon' name='start_mon'><? 
		show_select_months($start_mon, true);
		?></select>月
		<select id='start_day' name='start_day'><? 
		show_select_days($start_day, true);
		?></select>日
		&nbsp;〜&nbsp;
		<select id='end_yr' name='end_yr'><? 
		show_select_years(15, $end_yr, true, true);
		?></select>年
		<select id='end_mon' name='end_mon'><? 
		show_select_months($end_mon, true);
		?></select>月
		<select id='end_day' name='end_day'><? 
		show_select_days($end_day, true);
		?></select>日
		<!-- 表示ボタン -->
		<input type="submit" name="action" value="表示"></td>
		<td align="right" width="170">
		<? //if (count($data) > 0) { ?>
		<input type="button" value="CSV出力" onclick="downloadCSV()">
		<? //} ?>
		</td>
		</tr>
		</table>
<?
// 年月日
// 
// 表示、検索後
if ($action == "表示") {
?>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 表 -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="960" border="0" cellspacing="0" cellpadding="2" class="list">
			<!-- ------------------------------------------------------------------------ -->
			<!-- パターン一覧（見出し） -->
			<!-- ------------------------------------------------------------------------ -->
			<tr height="22" bgcolor="#f6f9ff">
			<?
			for ($i=0; $i<count($arr_title); $i++) {
			// width=\"8%\"
				echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> \n");
				echo($arr_title[$i]);
				echo("</font></td> \n");
			}
			?>

				</font></td>
			</tr>
			<!-- ------------------------------------------------------------------------ -->
			<!-- パターン一覧（データ） -->
			<!-- ------------------------------------------------------------------------ -->
			<?
			$cnt=0;
				for ($i=0; $i<count($data); $i++) {
					echo("<tr height=\"22\"> \n");
					for ($j=0; $j<count($arr_title); $j++) {
					// width=\"8%\"
						echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> \n");
						echo($data[$i][$j]);
						echo("</font></td> \n");
					}
					echo("</tr>\n");
				}
			?>
		</table>
<? } ?>
		<!-- ------------------------------------------------------------------------ -->
		<!-- ＨＩＤＤＥＮ -->
		<!-- ------------------------------------------------------------------------ -->
		<input type="hidden" name="session" value="<? echo($session); ?>">

	</form>
	<?
	// target="download"
	?>
	<form name="csv" method="get" target="download">
	<input type="hidden" name="session" value="<? echo($session); ?>">
	<input type="hidden" name="group_id" value="<? echo($group_id); ?>">
	<input type="hidden" name="start_date" value="<? echo($start_date); ?>">
	<input type="hidden" name="end_date" value="<? echo($end_date); ?>">
	</form>
	<iframe name="download" width="0" height="0" frameborder="0"></iframe>

	<? } ?>

</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>


