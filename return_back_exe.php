<?
require("about_session.php");
require("get_values.ini");
require_once("about_postgres.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($reason_id == "other") {
	if ($reason == "") {
		echo("<script type=\"text/javascript\">alert('理由が入力されていません。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
	if (strlen($reason) > 200) {
		echo("<script type=\"text/javascript\">alert('理由が長すぎます。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
	$reason_id = null;
} else {
	$reason = "";
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");


// 申請情報を取得
$sql = "select emp_id, target_date, rtncfm from rtnapply";
$cond = "where apply_id = $apply_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
$date = pg_fetch_result($sel, 0, "target_date");
$rtncfm = pg_fetch_result($sel, 0, "rtncfm");


// 申請IDを採番
$sql = "select max(apply_id) from rtnapply";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$new_apply_id = intval(pg_fetch_result($sel, 0, 0)) + 1;


// 申請情報を登録
$sql = "insert into rtnapply (apply_id, emp_id, target_date, reason_id, reason, apply_time, apply_status, comment, rtncfm, delete_flg) values (";
$content = array($new_apply_id, $emp_id, $date, $reason_id, $reason, date("YmdHis"), "0", $comment, $rtncfm, "f");
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 承認者情報を登録
$sql  = "insert into rtnaprv ";
$sql .= "(select ";
$sql .= "$new_apply_id, ";
$sql .= "aprv_no, ";
$sql .= "aprv_emp_id, ";
$sql .= "null, ";
$sql .= "'0', ";
$sql .= "'f', ";
$sql .= "'t', ";
$sql .= "aprv_sub_no, ";
$sql .= "multi_apv_flg, ";
$sql .= "next_notice_div, ";
$sql .= "'f', ";
$sql .= "emp_st, ";
$sql .= "'' ";
$sql .= "from rtnaprv ";
$sql .= "where apply_id = $apply_id";

$ins = insert_into_table($con, $sql, "", $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
		
// 承認者候補登録
$sql  = "insert into rtnaprvemp ";
$sql .= "(select ";
$sql .= "$new_apply_id, ";
$sql .= "aprv_no, ";
$sql .= "person_no, ";
$sql .= "emp_id, ";
$sql .= "'f' ";
$sql .= "from rtnaprvemp ";
$sql .= "where apply_id = $apply_id";

$ins = insert_into_table($con, $sql, "", $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
		
// 非同期・同期受信登録
$sql  = "insert into rtn_async_recv ";
$sql .= "(select ";
$sql .= "$new_apply_id, ";
$sql .= "send_aprv_no, ";
$sql .= "send_aprv_sub_no, ";
$sql .= "recv_aprv_no, ";
$sql .= "recv_aprv_sub_no, ";
$sql .= "null, ";
$sql .= "'f', ";
$sql .= "'f' ";
$sql .= "from rtn_async_recv ";
$sql .= "where apply_id = $apply_id";

$ins = insert_into_table($con, $sql, "", $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 元の申請書に再申請ＩＤを更新
$sql = "update rtnapply set";
$set = array("re_apply_id");
$setvalue = array($new_apply_id);
$cond = "where apply_id = $apply_id";	

$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}



// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 親画面をリフレッシュして自画面を閉じる
if($pnt_url != "") {
	echo("<script type=\"text/javascript\">opener.location.href = '$pnt_url'; self.close();</script>");
} else {
	echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>");	
	echo("<script language=\"javascript\">window.close();</script>\n");
}	
?>
