<?php
include_once 'jnl_indicator_data.php';
include_once 'jnl_indicator_facility.php';
include_once 'jnl_indicator_nurse_home_data.php';
/**
 * �����̴�����ɸ�����ѥǡ�������
 */
class IndicatorFacilityData extends IndicatorData
{
    /**
     * @var IndicatorFacility
     */
    var $indicatorFacility;

    /**
     * @var IndicatorNurseHome
     */
    var $indicatorNurseHomeData;

    /**
     * @var JnlBillDiagnosisEnter
     */
    var $jnlBillDiagnosisEnter;

    /**
     * @var JnlBillDiagnosisVisit
     */
    var $jnlBillDiagnosisVisit;

    /**
     * @var JnlRewardSynthesisMedi
     */
    var $jnlRewardSynthesisMedi;

    /**
     * @var JnlRewardSynthesisDental
     */
    var $jnlRewardSynthesisDental;

    /**
     * @var JnlBillRecuperation
     */
    var $jnlBillRecuperation;

    /**
     * @var JnlBillRecuperationPrepay
     */
    var $jnlBillRecuperationPrepay;

    /**
     * ���󥹥ȥ饯��
     * @param  array $arr
     * @return void
     */
    function IndicatorFacilityData($arr)
    {
        parent::IndicatorData($arr);
    }

    /**
     * @return void
     */
    function setIndicatorFacility()
    {
        $this->indicatorFacility = new IndicatorFacility(array('fname' => $this->fname, 'con' => $this->con));
    }

    /**
     * @return IndicatorFacility
     */
    function getIndicatorFacility()
    {
        if (!is_object($this->indicatorFacility)) { $this->setIndicatorFacility(); }
        return $this->indicatorFacility;
    }

    /**
     * @return void
     */
    function setIndicatorNurseHomeData()
    {
        $this->indicatorNurseHomeData = new IndicatorNurseHomeData(array('fname' => $this->fname, 'con' => $this->con));
    }

    /**
     * @return IndicatorNurseHomeData
     */
    function getIndicatorNurseHomeData()
    {
        if (!is_object($this->indicatorNurseHomeData)) { $this->setIndicatorNurseHomeData(); }
        return $this->indicatorNurseHomeData;
    }

    /**
     * @return void
     */
    function setJnlBillDiagnosisEnter()
    {
        $this->jnlBillDiagnosisEnter = new JnlBillDiagnosisEnter(array('fname' => $this->fname, 'con' => $this->con));
    }

    /**
     * @return JnlBillDiagnosisEnter
     */
    function getJnlBillDiagnosisEnter()
    {
        if (!is_object($this->jnlBillDiagnosisEnter)) { $this->setJnlBillDiagnosisEnter(); }
        return $this->jnlBillDiagnosisEnter;
    }

    /**
     * @return void
     */
    function setJnlBillDiagnosisVisit()
    {
        $this->jnlBillDiagnosisVisit = new JnlBillDiagnosisVisit(array('fname' => $this->fname, 'con' => $this->con));
    }

    /**
     * @return JnlBillDiagnosisVisit
     */
    function getJnlBillDiagnosisVisit()
    {
        if (!is_object($this->jnlBillDiagnosisVisit)) { $this->setJnlBillDiagnosisVisit(); }
        return $this->jnlBillDiagnosisVisit;
    }

    /**
     * @return void
     */
    function setJnlRewardSynthesisMedi()
    {
        $this->jnlRewardSynthesisMedi = new JnlRewardSynthesisMedi(array('fname' => $this->fname, 'con' => $this->con));
    }

    /**
     * @return JnlRewardSynthesisMedi
     */
    function getJnlRewardSynthesisMedi()
    {
        if (!is_object($this->jnlRewardSynthesisMedi)) { $this->setJnlRewardSynthesisMedi(); }
        return $this->jnlRewardSynthesisMedi;
    }

    /**
     * @return void
     */
    function setJnlRewardSynthesisDental()
    {
        $this->jnlRewardSynthesisDental = new JnlRewardSynthesisDental(array('fname' => $this->fname, 'con' => $this->con));
    }

    /**
     * @return JnlRewardSynthesisDental
     */
    function getJnlRewardSynthesisDental()
    {
        if (!is_object($this->jnlRewardSynthesisDental)) { $this->setJnlRewardSynthesisDental(); }
        return $this->jnlRewardSynthesisDental;
    }

    /**
     * @return void
     */
    function setJnlBillRecuperation()
    {
        $this->jnlBillRecuperation = new JnlBillRecuperation(array('fname' => $this->fname, 'con' => $this->con));
    }

    /**
     * @return JnlBillRecuperation
     */
    function getJnlBillRecuperation()
    {
        if (!is_object($this->jnlBillRecuperation)) { $this->setJnlBillRecuperation(); }
        return $this->jnlBillRecuperation;
    }

    /**
     * @return void
     */
    function setJnlBillRecuperationPrepay()
    {
        $this->jnlBillRecuperationPrepay = new JnlBillRecuperationPrepay(array('fname' => $this->fname, 'con' => $this->con));
    }

    /**
     * @return JnlBillRecuperationPrepay
     */
    function getJnlBillRecuperationPrepay()
    {
        if (!is_object($this->jnlBillRecuperationPrepay)) { $this->setJnlBillRecuperationPrepay(); }
        return $this->jnlBillRecuperationPrepay;
    }

    /**
     * (non-PHPdoc)
     * @see Indicator#getNoForNo($no)
     */
    function getNoForNo($no)
    {
        /* @var $indicatorFacility IndicatorFacility */
        $indicatorFacility = $this->getIndicatorFacility();
        $data              = $this->getArrValueByKey($no, $indicatorFacility->noArr);
        if (is_null($data)) { $data = parent::getNoForNo($no); }
        return $data;
    }

    /**
     * (non-PHPdoc)
     * @see IndicatorData#getDisplayDataList($no, $lastYearMonth, $currentYearMonth, $dateArr)
     */
    function getDisplayDataList($no, $lastYearMonth, $currentYearMonth, $optionArr)
    {
        $data = false;
        switch ($no) {
            case '74': # [�±�] �±��̷�����񴵼Կ�����[������]
                $data = $this->getSummaryOfNumberOfMonthlyReportPatientsAccordingToHospital($optionArr);
                break;

            case '75': # [�±�] �±������(�԰ٷ��)[������]
                $data = $this->getHospitalMonthlyReportFacility($optionArr);
                break;

            case '76': # [�±�] ������Ź԰�������
            case '77': # [�±�] ����������
                $data = $this->getOutpatientCareActDetails($optionArr);
                break;

            case '78': # [�±�] �������Ź԰�������
            case '79': # [�±�] ����������
                $data = $this->getInpatientCareActDetails($optionArr);
                break;

            case '80': # [�±�] ����������ɽ[���]
                $data = $this->getMedicalTreatmentFeeGeneralizationTableMedical($optionArr);
                break;

            case '81': # [�±�] ����������ɽ[����]
                $data = $this->getMedicalTreatmentFeeGeneralizationTableDental($optionArr);
                break;

            case '103': # [Ϸ��] ���Է���(��������)
                $data = $this->getPatientDailyFacilities($optionArr);
                break;

            case '124': # [Ϸ��] Ϸ���ֲû����ܡ�������Ӱ���
                $data = $this->getCalculationResultsForFiscalYear($optionArr);
                break;

            case '125': # [Ϸ��] ���������ٽ�
                $data = $this->getMedicalExpenseDetail($optionArr);
                break;

            case '126': # [Ϸ��] ͽ�ɵ������������ٽ�
                $data = $this->getPreventiveSupplyMedicalExpenseDetail($optionArr);
                break;

            default:
                $data = parent::getDisplayDataList($no, $lastYearMonth, $currentYearMonth, $optionArr);
                break;
        }

        return $data;
    }

    /**
     * No.74 [�±�] �±��̷�����񴵼Կ�����[������]
     * @param  array $optionArr
     * @return array
     */
    function getSummaryOfNumberOfMonthlyReportPatientsAccordingToHospital($optionArr)
    {
        $fiscalYear = $this->getArrValueByKey('year', $optionArr);
        $facilityId = $this->getArrValueByKey('facility_id', $optionArr);
        $startMonth = $this->getFiscalYearStartMonth();

        for ($m=0; $m < 12; $m++) {
            $month   = ($startMonth + $m < 13)? $startMonth + $m : $startMonth + $m -12;
            $year    = ($month < $startMonth)? $fiscalYear + 1 : $fiscalYear;
            $tmpData = $this->getPatientMonthlyReportAccordingToHospital(array('year' => $year, 'month' => $month));

            if (is_array($tmpData)) {
                $setData = array();
                foreach ($tmpData as $tmpVal) {
                    if ($facilityId == $tmpVal['jnl_facility_id'] && "current" == $tmpVal['year_type']) {
                        foreach ($tmpVal as $meargKey => $meargData) {
                            if ($this->getArrValueByKey($meargKey, $setData)) {
                                switch ($meargKey) {
                                    default:
                                        $setData[$meargKey] = $setData[$meargKey] + $meargData;
                                        break;

                                    case 'status': case 'valid':
                                        if ($setData[$meargKey] > $meargData) { $setData[$meargKey] = $meargData; }
                                        break;

                                    case 'year': case 'month'; case 'jnl_facility_id';
                                        break;
                                }
                            }
                            else { $setData[$meargKey] = $meargData; }
                        }
                    }
                }
                $dailyData[] = $setData;
            }
        }

        $rtnData['daily_data'] = $dailyData;

        $sql = "SELECT SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month, jnl_status AS status, newest_apply_id, emp_id"
                   .", jnl_facility_id, jnl_status, regist_date, gnr_ave_enter AS dispnum_297, gnr_new_enter_gnr AS dispnum_298"
                   .", gnr_dpc_total AS dispnum_299, gnr_ave_need_care AS dispnum_300, gnr_one_fourteen AS dispnum_301"
                   .", gnr_fifteen_thirty AS dispnum_302, gnr_emgc_mng AS dispnum_303, gnr_over_ninety AS dispnum_304"
                   .", gnr_sp_ent_total AS dispnum_305, hdc_one_fourteen AS dispnum_306, hdc_fifteen_thirty AS dispnum_307"
                   .", hdc_over_ninety AS dispnum_308,hdc_sp_ent_total AS dispnum_309, rhb_ave_seriuos AS dispnum_310"
                   .", rhb_ave_return AS dispnum_311, trt_a AS dispnum_312, trt_b AS dispnum_313, trt_c AS dispnum_314, trt_d AS dispnum_315"
                   .", trt_e AS dispnum_316, trt_f AS dispnum_317, trt_g AS dispnum_318, trt_h AS dispnum_319, trt_i AS dispnum_320"
                   .", trt_one_fourteen AS dispnum_321, trt_care_five AS dispnum_322, trt_care_four AS dispnum_323, trt_care_three AS dispnum_324"
                   .", trt_care_two AS dispnum_325, trt_care_one AS dispnum_326, new_comer AS dispnum_327, first_comer AS dispnum_328"
                   .", intro_comer AS dispnum_329, f_aid_req AS dispnum_330, f_aid_rev AS dispnum_331, f_aid_enter AS dispnum_332"
                   .", patient_overtime AS dispnum_333, enter_overtime AS dispnum_334, empty_one AS dispnum_335, empty_two AS dispnum_336"
                   .", empty_thee AS dispnum_337, empty_four AS dispnum_338, empty_five AS dispnum_339, empty_six AS dispnum_340"
                   .", empty_seven AS dispnum_341, del_flg"
               ." FROM jnl_hospital_month_patient"
              ." WHERE jnl_status < 2 AND del_flg = 'f'"
                .sprintf(" AND regist_date >= '%04d%02d00'", $fiscalYear,     $startMonth)
                .sprintf(" AND regist_date < '%04d%02d00'", $fiscalYear + 1, $startMonth)
                .sprintf(" AND jnl_facility_id = %d",        $facilityId)
              ." ORDER BY SUBSTR(regist_date, 0, 5), SUBSTR(regist_date, 5, 2)";

        $rtnData['monthly_data'] = $this->getDateList($sql);
        return $rtnData;
    }

    /**
     * No.75 [�±�] �±������(�԰ٷ��)[������]
     * @param  array $dateArr
     * @return array
     */
    function getHospitalMonthlyReportFacility($dateArr)
    {
        $facilityId = $this->getArrValueByKey('facility_id', $dateArr);
        $cYear      = $this->getArrValueByKey('year',        $dateArr);
        $cData      = $this->getHospitalMonthlyReport(array('year' => $cYear,       'facility_id' => $facilityId));
        $nData      = $this->getHospitalMonthlyReport(array('year' => ($cYear + 1), 'facility_id' => $facilityId));
        return array_merge($cData, $nData);
    }

    /**
     * No.76 [�±�] ������Ź԰�������
     * No.77 [�±�] ����������
     * @param  array $optionArr
     * @return array $rtnData
     */
    function getOutpatientCareActDetails($optionArr)
    {
        /* @var $jnlBillDiagnosisVisit JnlBillDiagnosisVisit */
        $jnlBillDiagnosisVisit = $this->getJnlBillDiagnosisVisit();
        $rtnData = $jnlBillDiagnosisVisit->getJnlBillDiagnosisVisit($optionArr);
        $this->setSqlArr($jnlBillDiagnosisVisit->getSqlArr()); # for debug
        return $rtnData;
    }

    /**
     * No.78 [�±�] �������Ź԰�������
     * No.79 [�±�] ����������
     * @param  array $optionArr
     * @return array $rtnData
     */
    function getInpatientCareActDetails($optionArr)
    {
        /* @var $jnlBillDiagnosisEnter JnlBillDiagnosisEnter */
        $jnlBillDiagnosisEnter = $this->getJnlBillDiagnosisEnter();
        $rtnData = $jnlBillDiagnosisEnter->getJnlBillDiagnosisEnter($optionArr);
        $this->setSqlArr($jnlBillDiagnosisEnter->getSqlArr()); # for debug
        return $rtnData;
    }

    /**
     * No.80 [�±�] ����������ɽ[���]
     * @param  array $dataArr
     * @return array $rtnData
     */
    function getMedicalTreatmentFeeGeneralizationTableMedical($optionArr)
    {
        /* @var $jnlRewardSynthesisMedi JnlRewardSynthesisMedi */
        $jnlRewardSynthesisMedi = $this->getJnlRewardSynthesisMedi();
        $rtnData = $jnlRewardSynthesisMedi->getJnlRewardSynthesisMedi($optionArr);
        $this->setSqlArr($jnlRewardSynthesisMedi->getSqlArr()); # for debug
        return $rtnData;
    }

    /**
     * No.81 [�±�] ����������ɽ[���]
     * @param  array $dataArr
     * @return array $rtnData
     */
    function getMedicalTreatmentFeeGeneralizationTableDental($optionArr)
    {
        /* @var $jnlRewardSynthesisDental JnlRewardSynthesisDental */
        $jnlRewardSynthesisDental = $this->getJnlRewardSynthesisDental();
        $rtnData = $jnlRewardSynthesisDental->getJnlRewardSynthesisDental($optionArr);
        $this->setSqlArr($jnlRewardSynthesisDental->getSqlArr()); # for debug
        return $rtnData;
    }

    /**
     * No.103 [Ϸ��] ���Է���(��������)
     * @param  array $optionArr
     * @return array $rtnData
     */
    function getPatientDailyFacilities($optionArr)
    {
        if ($this->getArrValueByKey('day', $optionArr)) { $optionArr['day'] = null; }
        /* @var $indicatorNurseHomeData IndicatorNurseHomeData */
        $indicatorNurseHomeData = $this->getIndicatorNurseHomeData();
        $rtnData = $indicatorNurseHomeData->getPatientDailyFacilities($optionArr);
        $this->setSqlArr($indicatorNurseHomeData->getSqlArr()); # for debug
        return $rtnData;
    }

    /**
     * No.124 [Ϸ��] Ϸ���ֲû����ܡ�������Ӱ���
     * @param  array $optionArr
     * @return array $rtnData
     */
    function getCalculationResultsForFiscalYear($optionArr)
    {
        /* @var $indicatorNurseHomeData IndicatorNurseHomeData */
        $indicatorNurseHomeData = $this->getIndicatorNurseHomeData();
        $rtnData = $indicatorNurseHomeData->getCalculationResultsForFiscalYear($optionArr);
        $this->setSqlArr($indicatorNurseHomeData->getSqlArr()); # for debug
        return $rtnData;
    }

    /**
     * No.125 [Ϸ��] ���������ٽ�
     * @param  array $optionArr
     * @return array $rtnData
     */
    function getMedicalExpenseDetail($optionArr)
    {
        /* @var $jnlBillRecuperation JnlBillRecuperation */
        $jnlBillRecuperation = $this->getJnlBillRecuperation();
        $rtnData = $jnlBillRecuperation->getJnlBillRecuperation($optionArr);
        $this->setSqlArr($jnlBillRecuperation->getSqlArr()); #for debug
        return $rtnData;
    }

    /**
     * No.126 [Ϸ��] ͽ�ɵ������������ٽ�
     * @param  array $optionArr
     * @return array $rtnData
     */
    function getPreventiveSupplyMedicalExpenseDetail($optionArr)
    {
        /* @var $jnlBillRecuperationPrepay JnlBillRecuperationPrepay */
        $jnlBillRecuperationPrepay = $this->getJnlBillRecuperationPrepay();
        $rtnData = $jnlBillRecuperationPrepay->getJnlBillRecuperationPrepay($optionArr);
        $this->setSqlArr($jnlBillRecuperationPrepay->getSqlArr()); #for debug
        return $rtnData;
    }
}


class JnlBillDiagnosisEnter extends IndicatorData
{
    function JnlBillDiagnosisEnter($arr)
    {
        parent::IndicatorData($arr);
    }

    function getJnlBillDiagnosisEnter($arr)
    {
        $rtnArr    = array();
        $abcde     = $this->getJnlBillDiagnosisEnter_abcde($arr);
        $fghij     = $this->getJnlBillDiagnosisEnter_fghij($arr);
        $klmno     = $this->getJnlBillDiagnosisEnter_klmno($arr);
        $pqrst     = $this->getJnlBillDiagnosisEnter_pqrst($arr);
        $uvwxy     = $this->getJnlBillDiagnosisEnter_uvwxy($arr);
        $zabacadae = $this->getJnlBillDiagnosisEnter_zabacadae($arr);
        $afag      = $this->getJnlBillDiagnosisEnter_afag($arr);

        $rtnArr    = $this->arrayMerge($abcde,     $rtnArr);
        $rtnArr    = $this->arrayMerge($fghij,     $rtnArr);
        $rtnArr    = $this->arrayMerge($klmno,     $rtnArr);
        $rtnArr    = $this->arrayMerge($pqrst,     $rtnArr);
        $rtnArr    = $this->arrayMerge($uvwxy,     $rtnArr);
        $rtnArr    = $this->arrayMerge($zabacadae, $rtnArr);
        $rtnArr    = $this->arrayMerge($afag,      $rtnArr);

        return $rtnArr;
    }

    function getJnlBillDiagnosisEnter_abcde($arr)
    {
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);
        $facilityId = $this->getArrValueByKey('facility_id', $arr);

		$sql = "SELECT jbdem.newest_apply_id, jbdem.emp_id, jbdem.jnl_facility_id, jbdem.jnl_status"
			.", jbdem.regist_date, jbdem.del_flg, jbdem.year, jbdem.month"
			.", a_account, a_days, a_1, a_2, a_3, a_4, a_5, a_6, a_7, a_8, a_9, a_10, a_11, a_12, a_13, a_side_total"
			.", b_account, b_days, b_1, b_2, b_3, b_4, b_5, b_6, b_7, b_8, b_9, b_10, b_11, b_12, b_13, b_side_total"
			.", c_account, c_days, c_1, c_2, c_3, c_4, c_5, c_6, c_7, c_8, c_9, c_10, c_11, c_12, c_13, c_side_total"
			.", d_account, d_days, d_1, d_2, d_3, d_4, d_5, d_6, d_7, d_8, d_9, d_10, d_11, d_12, d_13, d_side_total"
			.", e_account, e_days, e_1, e_2, e_3, e_4, e_5, e_6, e_7, e_8, e_9, e_10, e_11, e_12, e_13, e_side_total"
			." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
			.", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
			." FROM jnl_bill_diagnosis_enter_main jbdem"
			." WHERE del_flg = 'f' AND jnl_status < 2 ";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'", $facilityId); }

        $sql .=       ") jbdem"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS a_account, days AS a_days, first_diagnosis AS a_1"
                                .", guidance AS a_2, medicine AS a_3, injection AS a_4, treatment AS a_5, anesthesia AS a_6"
                                .", inspection AS a_7, x_ray AS a_8, rehab AS a_9, fee_etc AS a_10, hospitalization AS a_11"
                                .", dpc_all_assess AS a_12, meal_treat AS a_13, fee_total AS a_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 0 AND del_flg = 'f'"
                          .") jbde_a"
                      ." ON jbdem.newest_apply_id = jbde_a.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS b_account, days AS b_days, first_diagnosis AS b_1"
                                .", guidance AS b_2, medicine AS b_3, injection AS b_4, treatment AS b_5, anesthesia AS b_6"
                                .", inspection AS b_7, x_ray AS b_8, rehab AS b_9, fee_etc AS b_10, hospitalization AS b_11"
                                .", dpc_all_assess AS b_12, meal_treat AS b_13, fee_total AS b_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 1 AND del_flg = 'f'"
                          .") jbde_b"
                      ." ON jbdem.newest_apply_id = jbde_b.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS c_account, days AS c_days, first_diagnosis AS c_1"
                                .", guidance AS c_2, medicine AS c_3, injection AS c_4, treatment AS c_5, anesthesia AS c_6"
                                .", inspection AS c_7, x_ray AS c_8, rehab AS c_9, fee_etc AS c_10, hospitalization AS c_11"
                                .", dpc_all_assess AS c_12, meal_treat AS c_13, fee_total AS c_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 2 AND del_flg = 'f'"
                          .") jbde_c"
                      ." ON jbdem.newest_apply_id = jbde_c.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS d_account, days AS d_days, first_diagnosis AS d_1"
                                .", guidance AS d_2, medicine AS d_3, injection AS d_4, treatment AS d_5, anesthesia AS d_6"
                                .", inspection AS d_7, x_ray AS d_8, rehab AS d_9, fee_etc AS d_10, hospitalization AS d_11"
                                .", dpc_all_assess AS d_12, meal_treat AS d_13, fee_total AS d_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 3 AND del_flg = 'f'"
                          .") jbde_d"
                      ." ON jbdem.newest_apply_id = jbde_d.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS e_account, days AS e_days, first_diagnosis AS e_1"
                                .", guidance AS e_2, medicine AS e_3, injection AS e_4, treatment AS e_5, anesthesia AS e_6"
                                .", inspection AS e_7, x_ray AS e_8, rehab AS e_9, fee_etc AS e_10, hospitalization AS e_11"
                                .", dpc_all_assess AS e_12, meal_treat AS e_13, fee_total AS e_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 4 AND del_flg = 'f'"
                          .") jbde_e"
                      ." ON jbdem.newest_apply_id = jbde_e.newest_apply_id ";

        return $this->getDataList($sql);
    }

    function getJnlBillDiagnosisEnter_fghij($arr)
    {
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);
        $facilityId = $this->getArrValueByKey('facility_id', $arr);

		$sql = "SELECT jbdem.newest_apply_id, jbdem.emp_id, jbdem.jnl_facility_id, jbdem.jnl_status"
			.", jbdem.regist_date, jbdem.del_flg, jbdem.year, jbdem.month"
			.", f_account, f_days, f_1, f_2, f_3, f_4, f_5, f_6, f_7, f_8, f_9, f_10, f_11, f_12, f_13, f_side_total"
			.", g_account, g_days, g_1, g_2, g_3, g_4, g_5, g_6, g_7, g_8, g_9, g_10, g_11, g_12, g_13, g_side_total"
			.", h_account, h_days, h_1, h_2, h_3, h_4, h_5, h_6, h_7, h_8, h_9, h_10, h_11, h_12, h_13, h_side_total"
			.", i_account, i_days, i_1, i_2, i_3, i_4, i_5, i_6, i_7, i_8, i_9, i_10, i_11, i_12, i_13, i_side_total"
			.", j_account, j_days, j_1, j_2, j_3, j_4, j_5, j_6, j_7, j_8, j_9, j_10, j_11, j_12, j_13, j_side_total"
			." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
			.", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
			." FROM jnl_bill_diagnosis_enter_main jbdem"
			." WHERE del_flg = 'f' AND jnl_status < 2 ";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'", $facilityId); }

        $sql .=       ") jbdem"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS f_account, days AS f_days, first_diagnosis AS f_1"
                                .", guidance AS f_2, medicine AS f_3, injection AS f_4, treatment AS f_5, anesthesia AS f_6"
                                .", inspection AS f_7, x_ray AS f_8, rehab AS f_9, fee_etc AS f_10, hospitalization AS f_11"
                                .", dpc_all_assess AS f_12, meal_treat AS f_13, fee_total AS f_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 5 AND del_flg = 'f'"
                          .") jbde_f"
                      ." ON jbdem.newest_apply_id = jbde_f.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS g_account, days AS g_days, first_diagnosis AS g_1"
                                .", guidance AS g_2, medicine AS g_3, injection AS g_4, treatment AS g_5, anesthesia AS g_6"
                                .", inspection AS g_7, x_ray AS g_8, rehab AS g_9, fee_etc AS g_10, hospitalization AS g_11"
                                .", dpc_all_assess AS g_12, meal_treat AS g_13, fee_total AS g_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 6 AND del_flg = 'f'"
                          .") jbde_g"
                      ." ON jbdem.newest_apply_id = jbde_g.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS h_account, days AS h_days, first_diagnosis AS h_1"
                                .", guidance AS h_2, medicine AS h_3, injection AS h_4, treatment AS h_5, anesthesia AS h_6"
                                .", inspection AS h_7, x_ray AS h_8, rehab AS h_9, fee_etc AS h_10, hospitalization AS h_11"
                                .", dpc_all_assess AS h_12, meal_treat AS h_13, fee_total AS h_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 7 AND del_flg = 'f'"
                          .") jbde_h"
                      ." ON jbdem.newest_apply_id = jbde_h.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS i_account, days AS i_days, first_diagnosis AS i_1"
                                .", guidance AS i_2, medicine AS i_3, injection AS i_4, treatment AS i_5, anesthesia AS i_6"
                                .", inspection AS i_7, x_ray AS i_8, rehab AS i_9, fee_etc AS i_10, hospitalization AS i_11"
                                .", dpc_all_assess AS i_12, meal_treat AS i_13, fee_total AS i_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 8 AND del_flg = 'f'"
                          .") jbde_i"
                      ." ON jbdem.newest_apply_id = jbde_i.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS j_account, days AS j_days, first_diagnosis AS j_1"
                                .", guidance AS j_2, medicine AS j_3, injection AS j_4, treatment AS j_5, anesthesia AS j_6"
                                .", inspection AS j_7, x_ray AS j_8, rehab AS j_9, fee_etc AS j_10, hospitalization AS j_11"
                                .", dpc_all_assess AS j_12, meal_treat AS j_13, fee_total AS j_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 9 AND del_flg = 'f'"
                          .") jbde_j"
                      ." ON jbdem.newest_apply_id = jbde_j.newest_apply_id ";
        return $this->getDataList($sql);
    }

    function getJnlBillDiagnosisEnter_klmno($arr)
    {
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);
        $facilityId = $this->getArrValueByKey('facility_id', $arr);

		$sql = "SELECT jbdem.newest_apply_id, jbdem.emp_id, jbdem.jnl_facility_id, jbdem.jnl_status"
			.", jbdem.regist_date, jbdem.del_flg, jbdem.year, jbdem.month"
			.", k_account, k_days, k_1, k_2, k_3, k_4, k_5, k_6, k_7, k_8, k_9, k_10, k_11, k_12, k_13, k_side_total"
			.", l_account, l_days, l_1, l_2, l_3, l_4, l_5, l_6, l_7, l_8, l_9, l_10, l_11, l_12, l_13, l_side_total"
			.", m_account, m_days, m_1, m_2, m_3, m_4, m_5, m_6, m_7, m_8, m_9, m_10, m_11, m_12, m_13, m_side_total"
			.", n_account, n_days, n_1, n_2, n_3, n_4, n_5, n_6, n_7, n_8, n_9, n_10, n_11, n_12, n_13, n_side_total"
			.", o_account, o_days, o_1, o_2, o_3, o_4, o_5, o_6, o_7, o_8, o_9, o_10, o_11, o_12, o_13, o_side_total"
			." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
			.", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
			." FROM jnl_bill_diagnosis_enter_main jbdem"
			." WHERE del_flg = 'f' AND jnl_status < 2 ";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'", $facilityId); }

        $sql .=       ") jbdem"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS k_account, days AS k_days, first_diagnosis AS k_1"
                                .", guidance AS k_2, medicine AS k_3, injection AS k_4, treatment AS k_5, anesthesia AS k_6"
                                .", inspection AS k_7, x_ray AS k_8, rehab AS k_9, fee_etc AS k_10, hospitalization AS k_11"
                                .", dpc_all_assess AS k_12, meal_treat AS k_13, fee_total AS k_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 10 AND del_flg = 'f'"
                          .") jbde_k"
                      ." ON jbdem.newest_apply_id = jbde_k.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS l_account, days AS l_days, first_diagnosis AS l_1"
                                .", guidance AS l_2, medicine AS l_3, injection AS l_4, treatment AS l_5, anesthesia AS l_6"
                                .", inspection AS l_7, x_ray AS l_8, rehab AS l_9, fee_etc AS l_10, hospitalization AS l_11"
                                .", dpc_all_assess AS l_12, meal_treat AS l_13, fee_total AS l_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 11 AND del_flg = 'f'"
                          .") jbde_l"
                      ." ON jbdem.newest_apply_id = jbde_l.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS m_account, days AS m_days, first_diagnosis AS m_1"
                                .", guidance AS m_2, medicine AS m_3, injection AS m_4, treatment AS m_5, anesthesia AS m_6"
                                .", inspection AS m_7, x_ray AS m_8, rehab AS m_9, fee_etc AS m_10, hospitalization AS m_11"
                                .", dpc_all_assess AS m_12, meal_treat AS m_13, fee_total AS m_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 12 AND del_flg = 'f'"
                          .") jbde_m"
                      ." ON jbdem.newest_apply_id = jbde_m.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS n_account, days AS n_days, first_diagnosis AS n_1"
                                .", guidance AS n_2, medicine AS n_3, injection AS n_4, treatment AS n_5, anesthesia AS n_6"
                                .", inspection AS n_7, x_ray AS n_8, rehab AS n_9, fee_etc AS n_10, hospitalization AS n_11"
                                .", dpc_all_assess AS n_12, meal_treat AS n_13, fee_total AS n_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 13 AND del_flg = 'f'"
                          .") jbde_n"
                      ." ON jbdem.newest_apply_id = jbde_n.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS o_account, days AS o_days, first_diagnosis AS o_1"
                                .", guidance AS o_2, medicine AS o_3, injection AS o_4, treatment AS o_5, anesthesia AS o_6"
                                .", inspection AS o_7, x_ray AS o_8, rehab AS o_9, fee_etc AS o_10, hospitalization AS o_11"
                                .", dpc_all_assess AS o_12, meal_treat AS o_13, fee_total AS o_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 14 AND del_flg = 'f'"
                          .") jbde_o"
                      ." ON jbdem.newest_apply_id = jbde_o.newest_apply_id ";
        return $this->getDataList($sql);
    }

    function getJnlBillDiagnosisEnter_pqrst($arr)
    {
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);
        $facilityId = $this->getArrValueByKey('facility_id', $arr);

		$sql = "SELECT jbdem.newest_apply_id, jbdem.emp_id, jbdem.jnl_facility_id, jbdem.jnl_status"
			.", jbdem.regist_date, jbdem.del_flg, jbdem.year, jbdem.month"
			.", p_account, p_days, p_1, p_2, p_3, p_4, p_5, p_6, p_7, p_8, p_9, p_10, p_11, p_12, p_13, p_side_total"
			.", q_account, q_days, q_1, q_2, q_3, q_4, q_5, q_6, q_7, q_8, q_9, q_10, q_11, q_12, q_13, q_side_total"
			.", r_account, r_days, r_1, r_2, r_3, r_4, r_5, r_6, r_7, r_8, r_9, r_10, r_11, r_12, r_13, r_side_total"
			.", s_account, s_days, s_1, s_2, s_3, s_4, s_5, s_6, s_7, s_8, s_9, s_10, s_11, s_12, s_13, s_side_total"
			.", t_account, t_days, t_1, t_2, t_3, t_4, t_5, t_6, t_7, t_8, t_9, t_10, t_11, t_12, t_13, t_side_total"
			." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
			.", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
			." FROM jnl_bill_diagnosis_enter_main jbdem"
			." WHERE del_flg = 'f' AND jnl_status < 2 ";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'", $facilityId); }

        $sql .=       ") jbdem"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS p_account, days AS p_days, first_diagnosis AS p_1"
                                .", guidance AS p_2, medicine AS p_3, injection AS p_4, treatment AS p_5, anesthesia AS p_6"
                                .", inspection AS p_7, x_ray AS p_8, rehab AS p_9, fee_etc AS p_10, hospitalization AS p_11"
                                .", dpc_all_assess AS p_12, meal_treat AS p_13, fee_total AS p_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 15 AND del_flg = 'f'"
                          .") jbde_p"
                      ." ON jbdem.newest_apply_id = jbde_p.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS q_account, days AS q_days, first_diagnosis AS q_1"
                                .", guidance AS q_2, medicine AS q_3, injection AS q_4, treatment AS q_5, anesthesia AS q_6"
                                .", inspection AS q_7, x_ray AS q_8, rehab AS q_9, fee_etc AS q_10, hospitalization AS q_11"
                                .", dpc_all_assess AS q_12, meal_treat AS q_13, fee_total AS q_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 16 AND del_flg = 'f'"
                          .") jbde_q"
                      ." ON jbdem.newest_apply_id = jbde_q.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS r_account, days AS r_days, first_diagnosis AS r_1"
                                .", guidance AS r_2, medicine AS r_3, injection AS r_4, treatment AS r_5, anesthesia AS r_6"
                                .", inspection AS r_7, x_ray AS r_8, rehab AS r_9, fee_etc AS r_10, hospitalization AS r_11"
                                .", dpc_all_assess AS r_12, meal_treat AS r_13, fee_total AS r_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 17 AND del_flg = 'f'"
                          .") jbde_r"
                      ." ON jbdem.newest_apply_id = jbde_r.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS s_account, days AS s_days, first_diagnosis AS s_1"
                                .", guidance AS s_2, medicine AS s_3, injection AS s_4, treatment AS s_5, anesthesia AS s_6"
                                .", inspection AS s_7, x_ray AS s_8, rehab AS s_9, fee_etc AS s_10, hospitalization AS s_11"
                                .", dpc_all_assess AS s_12, meal_treat AS s_13, fee_total AS s_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 18 AND del_flg = 'f'"
                          .") jbde_s"
                      ." ON jbdem.newest_apply_id = jbde_s.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS t_account, days AS t_days, first_diagnosis AS t_1"
                                .", guidance AS t_2, medicine AS t_3, injection AS t_4, treatment AS t_5, anesthesia AS t_6"
                                .", inspection AS t_7, x_ray AS t_8, rehab AS t_9, fee_etc AS t_10, hospitalization AS t_11"
                                .", dpc_all_assess AS t_12, meal_treat AS t_13, fee_total AS t_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 19 AND del_flg = 'f'"
                          .") jbde_t"
                      ." ON jbdem.newest_apply_id = jbde_t.newest_apply_id ";
        return $this->getDataList($sql);
    }

    function getJnlBillDiagnosisEnter_uvwxy($arr)
    {
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);
        $facilityId = $this->getArrValueByKey('facility_id', $arr);

		$sql = "SELECT jbdem.newest_apply_id, jbdem.emp_id, jbdem.jnl_facility_id, jbdem.jnl_status"
			.", jbdem.regist_date, jbdem.del_flg, jbdem.year, jbdem.month"
			.", u_account, u_days, u_1, u_2, u_3, u_4, u_5, u_6, u_7, u_8, u_9, u_10, u_11, u_12, u_13, u_side_total"
			.", v_account, v_days, v_1, v_2, v_3, v_4, v_5, v_6, v_7, v_8, v_9, v_10, v_11, v_12, v_13, v_side_total"
			.", w_account, w_days, w_1, w_2, w_3, w_4, w_5, w_6, w_7, w_8, w_9, w_10, w_11, w_12, w_13, w_side_total"
			.", x_account, x_days, x_1, x_2, x_3, x_4, x_5, x_6, x_7, x_8, x_9, x_10, x_11, x_12, x_13, x_side_total"
			.", y_account, y_days, y_1, y_2, y_3, y_4, y_5, y_6, y_7, y_8, y_9, y_10, y_11, y_12, y_13, y_side_total"
			." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
			.", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
			." FROM jnl_bill_diagnosis_enter_main jbdem"
			." WHERE del_flg = 'f' AND jnl_status < 2 ";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'", $facilityId); }

        $sql .=       ") jbdem"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS u_account, days AS u_days, first_diagnosis AS u_1"
                                .", guidance AS u_2, medicine AS u_3, injection AS u_4, treatment AS u_5, anesthesia AS u_6"
                                .", inspection AS u_7, x_ray AS u_8, rehab AS u_9, fee_etc AS u_10, hospitalization AS u_11"
                                .", dpc_all_assess AS u_12, meal_treat AS u_13, fee_total AS u_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 20 AND del_flg = 'f'"
                          .") jbde_u"
                      ." ON jbdem.newest_apply_id = jbde_u.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS v_account, days AS v_days, first_diagnosis AS v_1"
                                .", guidance AS v_2, medicine AS v_3, injection AS v_4, treatment AS v_5, anesthesia AS v_6"
                                .", inspection AS v_7, x_ray AS v_8, rehab AS v_9, fee_etc AS v_10, hospitalization AS v_11"
                                .", dpc_all_assess AS v_12, meal_treat AS v_13, fee_total AS v_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 21 AND del_flg = 'f'"
                          .") jbde_v"
                      ." ON jbdem.newest_apply_id = jbde_v.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS w_account, days AS w_days, first_diagnosis AS w_1"
                                .", guidance AS w_2, medicine AS w_3, injection AS w_4, treatment AS w_5, anesthesia AS w_6"
                                .", inspection AS w_7, x_ray AS w_8, rehab AS w_9, fee_etc AS w_10, hospitalization AS w_11"
                                .", dpc_all_assess AS w_12, meal_treat AS w_13, fee_total AS w_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 22 AND del_flg = 'f'"
                          .") jbde_w"
                      ." ON jbdem.newest_apply_id = jbde_w.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS x_account, days AS x_days, first_diagnosis AS x_1"
                                .", guidance AS x_2, medicine AS x_3, injection AS x_4, treatment AS x_5, anesthesia AS x_6"
                                .", inspection AS x_7, x_ray AS x_8, rehab AS x_9, fee_etc AS x_10, hospitalization AS x_11"
                                .", dpc_all_assess AS x_12, meal_treat AS x_13, fee_total AS x_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 23 AND del_flg = 'f'"
                          .") jbde_x"
                      ." ON jbdem.newest_apply_id = jbde_x.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS y_account, days AS y_days, first_diagnosis AS y_1"
                                .", guidance AS y_2, medicine AS y_3, injection AS y_4, treatment AS y_5, anesthesia AS y_6"
                                .", inspection AS y_7, x_ray AS y_8, rehab AS y_9, fee_etc AS y_10, hospitalization AS y_11"
                                .", dpc_all_assess AS y_12, meal_treat AS y_13, fee_total AS y_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 24 AND del_flg = 'f'"
                          .") jbde_y"
                      ." ON jbdem.newest_apply_id = jbde_y.newest_apply_id ";
        return $this->getDataList($sql);
    }

    function getJnlBillDiagnosisEnter_zabacadae($arr)
    {
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);
        $facilityId = $this->getArrValueByKey('facility_id', $arr);

		$sql = "SELECT jbdem.newest_apply_id, jbdem.emp_id, jbdem.jnl_facility_id, jbdem.jnl_status"
			.", jbdem.regist_date, jbdem.del_flg, jbdem.year, jbdem.month"
			.", z_account, z_days, z_1, z_2, z_3, z_4, z_5, z_6, z_7, z_8, z_9, z_10, z_11, z_12, z_13, z_side_total"
			.", ab_account, ab_days, ab_1, ab_2, ab_3, ab_4, ab_5, ab_6, ab_7, ab_8, ab_9, ab_10, ab_11, ab_12, ab_13, ab_side_total"
			.", ac_account, ac_days, ac_1, ac_2, ac_3, ac_4, ac_5, ac_6, ac_7, ac_8, ac_9, ac_10, ac_11, ac_12, ac_13, ac_side_total"
			.", ad_account, ad_days, ad_1, ad_2, ad_3, ad_4, ad_5, ad_6, ad_7, ad_8, ad_9, ad_10, ad_11, ad_12, ad_13, ad_side_total"
			.", ae_account, ae_days, ae_1, ae_2, ae_3, ae_4, ae_5, ae_6, ae_7, ae_8, ae_9, ae_10, ae_11, ae_12, ae_13, ae_side_total"
			." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
			.", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
			." FROM jnl_bill_diagnosis_enter_main jbdem"
			." WHERE del_flg = 'f' AND jnl_status < 2 ";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'", $facilityId); }

        $sql .=       ") jbdem"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS z_account, days AS z_days, first_diagnosis AS z_1"
                                .", guidance AS z_2, medicine AS z_3, injection AS z_4, treatment AS z_5, anesthesia AS z_6"
                                .", inspection AS z_7, x_ray AS z_8, rehab AS z_9, fee_etc AS z_10, hospitalization AS z_11"
                                .", dpc_all_assess AS z_12, meal_treat AS z_13, fee_total AS z_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 25 AND del_flg = 'f'"
                          .") jbde_z"
                      ." ON jbdem.newest_apply_id = jbde_z.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS ab_account, days AS ab_days, first_diagnosis AS ab_1"
                                .", guidance AS ab_2, medicine AS ab_3, injection AS ab_4, treatment AS ab_5, anesthesia AS ab_6"
                                .", inspection AS ab_7, x_ray AS ab_8, rehab AS ab_9, fee_etc AS ab_10, hospitalization AS ab_11"
                                .", dpc_all_assess AS ab_12, meal_treat AS ab_13, fee_total AS ab_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 26 AND del_flg = 'f'"
                          .") jbde_ab"
                      ." ON jbdem.newest_apply_id = jbde_ab.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS ac_account, days AS ac_days, first_diagnosis AS ac_1"
                                .", guidance AS ac_2, medicine AS ac_3, injection AS ac_4, treatment AS ac_5, anesthesia AS ac_6"
                                .", inspection AS ac_7, x_ray AS ac_8, rehab AS ac_9, fee_etc AS ac_10, hospitalization AS ac_11"
                                .", dpc_all_assess AS ac_12, meal_treat AS ac_13, fee_total AS ac_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 27 AND del_flg = 'f'"
                          .") jbde_ac"
                      ." ON jbdem.newest_apply_id = jbde_ac.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS ad_account, days AS ad_days, first_diagnosis AS ad_1"
                                .", guidance AS ad_2, medicine AS ad_3, injection AS ad_4, treatment AS ad_5, anesthesia AS ad_6"
                                .", inspection AS ad_7, x_ray AS ad_8, rehab AS ad_9, fee_etc AS ad_10, hospitalization AS ad_11"
                                .", dpc_all_assess AS ad_12, meal_treat AS ad_13, fee_total AS ad_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 28 AND del_flg = 'f'"
                          .") jbde_ad"
                      ." ON jbdem.newest_apply_id = jbde_ad.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS ae_account, days AS ae_days, first_diagnosis AS ae_1"
                                .", guidance AS ae_2, medicine AS ae_3, injection AS ae_4, treatment AS ae_5, anesthesia AS ae_6"
                                .", inspection AS ae_7, x_ray AS ae_8, rehab AS ae_9, fee_etc AS ae_10, hospitalization AS ae_11"
                                .", dpc_all_assess AS ae_12, meal_treat AS ae_13, fee_total AS ae_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 29 AND del_flg = 'f'"
                          .") jbde_ae"
                      ." ON jbdem.newest_apply_id = jbde_ae.newest_apply_id ";
        return $this->getDataList($sql);
    }

    function getJnlBillDiagnosisEnter_afag($arr)
    {
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);
        $facilityId = $this->getArrValueByKey('facility_id', $arr);

		$sql = "SELECT jbdem.newest_apply_id, jbdem.emp_id, jbdem.jnl_facility_id, jbdem.jnl_status"
			.", jbdem.regist_date, jbdem.del_flg, jbdem.year, jbdem.month"
			.", af_account, af_days, af_1, af_2, af_3, af_4, af_5, af_6, af_7, af_8, af_9, af_10, af_11, af_12, af_13, af_side_total"
			.", ag_account, ag_days, ag_1, ag_2, ag_3, ag_4, ag_5, ag_6, ag_7, ag_8, ag_9, ag_10, ag_11, ag_12, ag_13, ag_side_total"
			." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
			.", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
			." FROM jnl_bill_diagnosis_enter_main jbdem"
			." WHERE del_flg = 'f' AND jnl_status < 2 ";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'", $facilityId); }

        $sql .=       ") jbdem"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS af_account, days AS af_days, first_diagnosis AS af_1"
                                .", guidance AS af_2, medicine AS af_3, injection AS af_4, treatment AS af_5, anesthesia AS af_6"
                                .", inspection AS af_7, x_ray AS af_8, rehab AS af_9, fee_etc AS af_10, hospitalization AS af_11"
                                .", dpc_all_assess AS af_12, meal_treat AS af_13, fee_total AS af_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 30 AND del_flg = 'f'"
                          .") jbde_af"
                      ." ON jbdem.newest_apply_id = jbde_af.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, account AS ag_account, days AS ag_days, first_diagnosis AS ag_1"
                                .", guidance AS ag_2, medicine AS ag_3, injection AS ag_4, treatment AS ag_5, anesthesia AS ag_6"
                                .", inspection AS ag_7, x_ray AS ag_8, rehab AS ag_9, fee_etc AS ag_10, hospitalization AS ag_11"
                                .", dpc_all_assess AS ag_12, meal_treat AS ag_13, fee_total AS ag_side_total"
                            ." FROM jnl_bill_diagnosis_enter"
                           ." WHERE type_section = 31 AND del_flg = 'f'"
                          .") jbde_ag"
                      ." ON jbdem.newest_apply_id = jbde_ag.newest_apply_id ";
        return $this->getDataList($sql);
    }
}


class JnlBillDiagnosisVisit extends IndicatorData
{
    function JnlBillDiagnosisVisit($arr)
    {
        parent::IndicatorData($arr);
    }

    function getJnlBillDiagnosisVisit($arr)
    {
        $rtnArr = array();
        $abcde  = $this->getJnlBillDiagnosisVisit_abcde($arr);
        $fghij  = $this->getJnlBillDiagnosisVisit_fghij($arr);
        $klmno  = $this->getJnlBillDiagnosisVisit_klmno($arr);
        $pqrst  = $this->getJnlBillDiagnosisVisit_pqrst($arr);

        $rtnArr = $this->arrayMerge($abcde, $rtnArr);
        $rtnArr = $this->arrayMerge($fghij, $rtnArr);
        $rtnArr = $this->arrayMerge($klmno, $rtnArr);
        $rtnArr = $this->arrayMerge($pqrst, $rtnArr);

        return $rtnArr;
    }

    function getJnlBillDiagnosisVisit_abcde($arr)
    {
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);
        $facilityId = $this->getArrValueByKey('facility_id', $arr);

		$sql = "SELECT jbdvm.newest_apply_id, jbdvm.emp_id, jbdvm.jnl_facility_id, jbdvm.jnl_status"
			.", jbdvm.regist_date, jbdvm.del_flg, jbdvm.year, jbdvm.month"
			.", a_account, a_days, a_1, a_2, a_3, a_4, a_5, a_6, a_7, a_8, a_9, a_10, a_11, a_12, a_13, a_14, a_side_total"
			.", b_account, b_days, b_1, b_2, b_3, b_4, b_5, b_6, b_7, b_8, b_9, b_10, b_11, b_12, b_13, b_14, b_side_total"
			.", c_account, c_days, c_1, c_2, c_3, c_4, c_5, c_6, c_7, c_8, c_9, c_10, c_11, c_12, c_13, c_14, c_side_total"
			.", d_account, d_days, d_1, d_2, d_3, d_4, d_5, d_6, d_7, d_8, d_9, d_10, d_11, d_12, d_13, d_14, d_side_total"
			.", e_account, e_days, e_1, e_2, e_3, e_4, e_5, e_6, e_7, e_8, e_9, e_10, e_11, e_12, e_13, e_14, e_side_total"
			." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg, SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
			." FROM jnl_bill_diagnosis_visit_main jbdvm"
			." WHERE del_flg = 'f' AND jnl_status < 2 ";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'", $facilityId); }

        $sql .=       ") jbdvm"
              ." INNER JOIN (SELECT newest_apply_id, type_section"
                                .", account AS a_account, days AS a_days, first_diagnosis AS a_1, re_diagnosis AS a_2, guidance AS a_3"
                                .", being_in  AS a_4, in_prescription AS a_5, medication AS a_6, injection AS a_7, treatment AS a_8"
                                .", anesthesia AS a_9, inspection AS a_10, x_ray AS a_11, rehab AS a_12, prescription AS a_13"
                                .", fee_etc AS a_14, fee_total AS a_side_total"
                            ." FROM jnl_bill_diagnosis_visit"
                           ." WHERE type_section = 0 AND del_flg = 'f'"
                          .") jbdv_a"
                      ." ON jbdvm.newest_apply_id = jbdv_a.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section"
                                .", account AS b_account, days AS b_days, first_diagnosis AS b_1, re_diagnosis AS b_2, guidance AS b_3"
                                .", being_in  AS b_4, in_prescription AS b_5, medication AS b_6, injection AS b_7, treatment AS b_8"
                                .", anesthesia AS b_9, inspection AS b_10, x_ray AS b_11, rehab AS b_12, prescription AS b_13"
                                .", fee_etc AS b_14, fee_total AS b_side_total"
                            ." FROM jnl_bill_diagnosis_visit"
                           ." WHERE type_section = 1 AND del_flg = 'f'"
                          .") jbdv_b"
                      ." ON jbdvm.newest_apply_id = jbdv_b.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section"
                                .", account AS c_account, days AS c_days, first_diagnosis AS c_1, re_diagnosis AS c_2, guidance AS c_3"
                                .", being_in AS c_4, in_prescription AS c_5, medication AS c_6, injection AS c_7, treatment AS c_8"
                                .", anesthesia AS c_9, inspection AS c_10, x_ray AS c_11, rehab AS c_12, prescription AS c_13"
                                .", fee_etc AS c_14, fee_total AS c_side_total"
                            ." FROM jnl_bill_diagnosis_visit"
                           ." WHERE type_section = 2 AND del_flg = 'f'"
                          .") jbdv_c"
                      ." ON jbdvm.newest_apply_id = jbdv_c.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section"
                                .", account AS d_account, days AS d_days, first_diagnosis AS d_1, re_diagnosis AS d_2, guidance AS d_3"
                                .", being_in  AS d_4, in_prescription AS d_5, medication AS d_6, injection AS d_7, treatment AS d_8"
                                .", anesthesia AS d_9, inspection AS d_10, x_ray AS d_11, rehab AS d_12, prescription AS d_13"
                                .", fee_etc AS d_14, fee_total AS d_side_total"
                            ." FROM jnl_bill_diagnosis_visit"
                           ." WHERE type_section = 3 AND del_flg = 'f'"
                          .") jbdv_d"
                      ." ON jbdvm.newest_apply_id = jbdv_d.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section"
                                .", account AS e_account, days AS e_days, first_diagnosis AS e_1, re_diagnosis AS e_2, guidance AS e_3"
                                .", being_in AS e_4, in_prescription AS e_5, medication AS e_6, injection AS e_7, treatment AS e_8"
                                .", anesthesia AS e_9, inspection AS e_10, x_ray AS e_11, rehab AS e_12, prescription AS e_13"
                                .", fee_etc AS e_14, fee_total AS e_side_total"
                            ." FROM jnl_bill_diagnosis_visit"
                           ." WHERE type_section = 4 AND del_flg = 'f'"
                          .") jbdv_e"
                      ." ON jbdvm.newest_apply_id = jbdv_e.newest_apply_id ";

        return $this->getDataList($sql);
    }

    function getJnlBillDiagnosisVisit_fghij($arr)
    {
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);
        $facilityId = $this->getArrValueByKey('facility_id', $arr);

		$sql = "SELECT jbdvm.newest_apply_id, jbdvm.emp_id, jbdvm.jnl_facility_id, jbdvm.jnl_status"
			.", jbdvm.regist_date, jbdvm.del_flg, jbdvm.year, jbdvm.month"
			.", f_account, f_days, f_1, f_2, f_3, f_4, f_5, f_6, f_7, f_8, f_9, f_10, f_11, f_12, f_13, f_14, f_side_total"
			.", g_account, g_days, g_1, g_2, g_3, g_4, g_5, g_6, g_7, g_8, g_9, g_10, g_11, g_12, g_13, g_14, g_side_total"
			.", h_account, h_days, h_1, h_2, h_3, h_4, h_5, h_6, h_7, h_8, h_9, h_10, h_11, h_12, h_13, h_14, h_side_total"
			.", i_account, i_days, i_1, i_2, i_3, i_4, i_5, i_6, i_7, i_8, i_9, i_10, i_11, i_12, i_13, i_14, i_side_total"
			.", j_account, j_days, j_1, j_2, j_3, j_4, j_5, j_6, j_7, j_8, j_9, j_10, j_11, j_12, j_13, j_14, j_side_total"
			." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
			.", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
			." FROM jnl_bill_diagnosis_visit_main jbdvm"
			." WHERE del_flg = 'f' AND jnl_status < 2 ";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'", $facilityId); }

        $sql .=       ") jbdvm"
              ." INNER JOIN (SELECT newest_apply_id,         type_section,         account   AS f_account,   days      AS f_days"
                                .", first_diagnosis AS f_1,  re_diagnosis AS f_2,  guidance  AS f_3,         being_in  AS f_4"
                                .", in_prescription AS f_5,  medication   AS f_6,  injection AS f_7,         treatment AS f_8"
                                .", anesthesia      AS f_9,  inspection   AS f_10, x_ray     AS f_11,        rehab     AS f_12"
                                .", prescription    AS f_13, fee_etc      AS f_14, fee_total AS f_side_total"
                            ." FROM jnl_bill_diagnosis_visit"
                           ." WHERE type_section = 5 AND del_flg = 'f'"
                          .") jbdv_f"
                      ." ON jbdvm.newest_apply_id = jbdv_f.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id,         type_section,         account   AS g_account,   days      AS g_days"
                                .", first_diagnosis AS g_1,  re_diagnosis AS g_2,  guidance  AS g_3,         being_in  AS g_4"
                                .", in_prescription AS g_5,  medication   AS g_6,  injection AS g_7,         treatment AS g_8"
                                .", anesthesia      AS g_9,  inspection   AS g_10, x_ray     AS g_11,        rehab     AS g_12"
                                .", prescription    AS g_13, fee_etc      AS g_14, fee_total AS g_side_total"
                            ." FROM jnl_bill_diagnosis_visit"
                           ." WHERE type_section = 6 AND del_flg = 'f'"
                          .") jbdv_g"
                      ." ON jbdvm.newest_apply_id = jbdv_g.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id,         type_section,         account   AS h_account,   days      AS h_days"
                                .", first_diagnosis AS h_1,  re_diagnosis AS h_2,  guidance  AS h_3,         being_in  AS h_4"
                                .", in_prescription AS h_5,  medication   AS h_6,  injection AS h_7,         treatment AS h_8"
                                .", anesthesia      AS h_9,  inspection   AS h_10, x_ray     AS h_11,        rehab     AS h_12"
                                .", prescription    AS h_13, fee_etc      AS h_14, fee_total AS h_side_total"
                            ." FROM jnl_bill_diagnosis_visit"
                           ." WHERE type_section = 7 AND del_flg = 'f'"
                          .") jbdv_h"
                      ." ON jbdvm.newest_apply_id = jbdv_h.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id,         type_section,         account   AS i_account,   days      AS i_days"
                                .", first_diagnosis AS i_1,  re_diagnosis AS i_2,  guidance  AS i_3,         being_in  AS i_4"
                                .", in_prescription AS i_5,  medication   AS i_6,  injection AS i_7,         treatment AS i_8"
                                .", anesthesia      AS i_9,  inspection   AS i_10, x_ray     AS i_11,        rehab     AS i_12"
                                .", prescription    AS i_13, fee_etc      AS i_14, fee_total AS i_side_total"
                            ." FROM jnl_bill_diagnosis_visit"
                           ." WHERE type_section = 8 AND del_flg = 'f'"
                          .") jbdv_i"
                      ." ON jbdvm.newest_apply_id = jbdv_i.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id,         type_section,         account   AS j_account,   days      AS j_days"
                                .", first_diagnosis AS j_1,  re_diagnosis AS j_2,  guidance  AS j_3,         being_in  AS j_4"
                                .", in_prescription AS j_5,  medication   AS j_6,  injection AS j_7,         treatment AS j_8"
                                .", anesthesia      AS j_9,  inspection   AS j_10, x_ray     AS j_11,        rehab     AS j_12"
                                .", prescription    AS j_13, fee_etc      AS j_14, fee_total AS j_side_total"
                            ." FROM jnl_bill_diagnosis_visit"
                           ." WHERE type_section = 9 AND del_flg = 'f'"
                          .") jbdv_j"
                      ." ON jbdvm.newest_apply_id = jbdv_j.newest_apply_id ";
        return $this->getDataList($sql);
    }

    function getJnlBillDiagnosisVisit_klmno($arr)
    {
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);
        $facilityId = $this->getArrValueByKey('facility_id', $arr);

		$sql = "SELECT jbdvm.newest_apply_id, jbdvm.emp_id, jbdvm.jnl_facility_id, jbdvm.jnl_status"
			.", jbdvm.regist_date, jbdvm.del_flg, jbdvm.year, jbdvm.month"
			.", k_account, k_days, k_1, k_2, k_3, k_4, k_5, k_6, k_7, k_8, k_9, k_10, k_11, k_12, k_13, k_14, k_side_total"
			.", l_account, l_days, l_1, l_2, l_3, l_4, l_5, l_6, l_7, l_8, l_9, l_10, l_11, l_12, l_13, l_14, l_side_total"
			.", m_account, m_days, m_1, m_2, m_3, m_4, m_5, m_6, m_7, m_8, m_9, m_10, m_11, m_12, m_13, m_14, m_side_total"
			.", n_account, n_days, n_1, n_2, n_3, n_4, n_5, n_6, n_7, n_8, n_9, n_10, n_11, n_12, n_13, n_14, n_side_total"
			.", o_account, o_days, o_1, o_2, o_3, o_4, o_5, o_6, o_7, o_8, o_9, o_10, o_11, o_12, o_13, o_14, o_side_total"
			." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
			.", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
			." FROM jnl_bill_diagnosis_visit_main jbdvm"
			." WHERE del_flg = 'f' AND jnl_status < 2 ";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'", $facilityId); }

        $sql .=       ") jbdvm"
              ." INNER JOIN (SELECT newest_apply_id,         type_section,         account   AS k_account,   days      AS k_days"
                                .", first_diagnosis AS k_1,  re_diagnosis AS k_2,  guidance  AS k_3,         being_in  AS k_4"
                                .", in_prescription AS k_5,  medication   AS k_6,  injection AS k_7,         treatment AS k_8"
                                .", anesthesia      AS k_9,  inspection   AS k_10, x_ray     AS k_11,        rehab     AS k_12"
                                .", prescription    AS k_13, fee_etc      AS k_14, fee_total AS k_side_total"
                            ." FROM jnl_bill_diagnosis_visit"
                           ." WHERE type_section = 10 AND del_flg = 'f'"
                          .") jbdv_k"
                      ." ON jbdvm.newest_apply_id = jbdv_k.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id,         type_section,         account   AS l_account,   days      AS l_days"
                                .", first_diagnosis AS l_1,  re_diagnosis AS l_2,  guidance  AS l_3,         being_in  AS l_4"
                                .", in_prescription AS l_5,  medication   AS l_6,  injection AS l_7,         treatment AS l_8"
                                .", anesthesia      AS l_9,  inspection   AS l_10, x_ray     AS l_11,        rehab     AS l_12"
                                .", prescription    AS l_13, fee_etc      AS l_14, fee_total AS l_side_total"
                            ." FROM jnl_bill_diagnosis_visit"
                           ." WHERE type_section = 11 AND del_flg = 'f'"
                          .") jbdv_l"
                      ." ON jbdvm.newest_apply_id = jbdv_l.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id,         type_section,         account   AS m_account,   days      AS m_days"
                                .", first_diagnosis AS m_1,  re_diagnosis AS m_2,  guidance  AS m_3,         being_in  AS m_4"
                                .", in_prescription AS m_5,  medication   AS m_6,  injection AS m_7,         treatment AS m_8"
                                .", anesthesia      AS m_9,  inspection   AS m_10, x_ray     AS m_11,        rehab     AS m_12"
                                .", prescription    AS m_13, fee_etc      AS m_14, fee_total AS m_side_total"
                            ." FROM jnl_bill_diagnosis_visit"
                           ." WHERE type_section = 12 AND del_flg = 'f'"
                          .") jbdv_m"
                      ." ON jbdvm.newest_apply_id = jbdv_m.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id,         type_section,         account   AS n_account,   days      AS n_days"
                                .", first_diagnosis AS n_1,  re_diagnosis AS n_2,  guidance  AS n_3,         being_in  AS n_4"
                                .", in_prescription AS n_5,  medication   AS n_6,  injection AS n_7,         treatment AS n_8"
                                .", anesthesia      AS n_9,  inspection   AS n_10, x_ray     AS n_11,        rehab     AS n_12"
                                .", prescription    AS n_13, fee_etc      AS n_14, fee_total AS n_side_total"
                            ." FROM jnl_bill_diagnosis_visit"
                           ." WHERE type_section = 13 AND del_flg = 'f'"
                          .") jbdv_n"
                      ." ON jbdvm.newest_apply_id = jbdv_n.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id,         type_section,         account   AS o_account,   days      AS o_days"
                                .", first_diagnosis AS o_1,  re_diagnosis AS o_2,  guidance  AS o_3,         being_in  AS o_4"
                                .", in_prescription AS o_5,  medication   AS o_6,  injection AS o_7,         treatment AS o_8"
                                .", anesthesia      AS o_9,  inspection   AS o_10, x_ray     AS o_11,        rehab     AS o_12"
                                .", prescription    AS o_13, fee_etc      AS o_14, fee_total AS o_side_total"
                            ." FROM jnl_bill_diagnosis_visit"
                           ." WHERE type_section = 14 AND del_flg = 'f'"
                          .") jbdv_o"
                      ." ON jbdvm.newest_apply_id = jbdv_o.newest_apply_id ";
        return $this->getDataList($sql);
    }

    function getJnlBillDiagnosisVisit_pqrst($arr)
    {
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);
        $facilityId = $this->getArrValueByKey('facility_id', $arr);

		$sql = "SELECT jbdvm.newest_apply_id, jbdvm.emp_id, jbdvm.jnl_facility_id, jbdvm.jnl_status"
			.", jbdvm.regist_date, jbdvm.del_flg, jbdvm.year, jbdvm.month"
			.", p_account, p_days, p_1, p_2, p_3, p_4, p_5, p_6, p_7, p_8, p_9, p_10, p_11, p_12, p_13, p_14, p_side_total"
			.", q_account, q_days, q_1, q_2, q_3, q_4, q_5, q_6, q_7, q_8, q_9, q_10, q_11, q_12, q_13, q_14, q_side_total"
			.", r_account, r_days, r_1, r_2, r_3, r_4, r_5, r_6, r_7, r_8, r_9, r_10, r_11, r_12, r_13, r_14, r_side_total"
			.", s_account, s_days, s_1, s_2, s_3, s_4, s_5, s_6, s_7, s_8, s_9, s_10, s_11, s_12, s_13, s_14, s_side_total"
			.", t_account, t_days, t_1, t_2, t_3, t_4, t_5, t_6, t_7, t_8, t_9, t_10, t_11, t_12, t_13, t_14, t_side_total"
			." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
			.", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
			." FROM jnl_bill_diagnosis_visit_main jbdvm"
			." WHERE del_flg = 'f' AND jnl_status < 2 ";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'", $facilityId); }

        $sql .=       ") jbdvm"
              ." INNER JOIN (SELECT newest_apply_id,         type_section,         account   AS p_account,   days      AS p_days"
                                .", first_diagnosis AS p_1,  re_diagnosis AS p_2,  guidance  AS p_3,         being_in  AS p_4"
                                .", in_prescription AS p_5,  medication   AS p_6,  injection AS p_7,         treatment AS p_8"
                                .", anesthesia      AS p_9,  inspection   AS p_10, x_ray     AS p_11,        rehab     AS p_12"
                                .", prescription    AS p_13, fee_etc      AS p_14, fee_total AS p_side_total"
                            ." FROM jnl_bill_diagnosis_visit"
                           ." WHERE type_section = 15 AND del_flg = 'f'"
                          .") jbdv_p"
                      ." ON jbdvm.newest_apply_id = jbdv_p.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id,         type_section,         account   AS q_account,   days      AS q_days"
                                .", first_diagnosis AS q_1,  re_diagnosis AS q_2,  guidance  AS q_3,         being_in  AS q_4"
                                .", in_prescription AS q_5,  medication   AS q_6,  injection AS q_7,         treatment AS q_8"
                                .", anesthesia      AS q_9,  inspection   AS q_10, x_ray     AS q_11,        rehab     AS q_12"
                                .", prescription    AS q_13, fee_etc      AS q_14, fee_total AS q_side_total"
                            ." FROM jnl_bill_diagnosis_visit"
                           ." WHERE type_section = 16 AND del_flg = 'f'"
                          .") jbdv_q"
                      ." ON jbdvm.newest_apply_id = jbdv_q.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id,         type_section,         account   AS r_account,   days      AS r_days"
                                .", first_diagnosis AS r_1,  re_diagnosis AS r_2,  guidance  AS r_3,         being_in  AS r_4"
                                .", in_prescription AS r_5,  medication   AS r_6,  injection AS r_7,         treatment AS r_8"
                                .", anesthesia      AS r_9,  inspection   AS r_10, x_ray     AS r_11,        rehab     AS r_12"
                                .", prescription    AS r_13, fee_etc      AS r_14, fee_total AS r_side_total"
                            ." FROM jnl_bill_diagnosis_visit"
                           ." WHERE type_section = 17 AND del_flg = 'f'"
                          .") jbdv_r"
                      ." ON jbdvm.newest_apply_id = jbdv_r.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id,         type_section,         account   AS s_account,   days      AS s_days"
                                .", first_diagnosis AS s_1,  re_diagnosis AS s_2,  guidance  AS s_3,         being_in  AS s_4"
                                .", in_prescription AS s_5,  medication   AS s_6,  injection AS s_7,         treatment AS s_8"
                                .", anesthesia      AS s_9,  inspection   AS s_10, x_ray     AS s_11,        rehab     AS s_12"
                                .", prescription    AS s_13, fee_etc      AS s_14, fee_total AS s_side_total"
                            ." FROM jnl_bill_diagnosis_visit"
                           ." WHERE type_section = 18 AND del_flg = 'f'"
                          .") jbdv_s"
                      ." ON jbdvm.newest_apply_id = jbdv_s.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id,         type_section,         account   AS t_account,   days      AS t_days"
                                .", first_diagnosis AS t_1,  re_diagnosis AS t_2,  guidance  AS t_3,         being_in  AS t_4"
                                .", in_prescription AS t_5,  medication   AS t_6,  injection AS t_7,         treatment AS t_8"
                                .", anesthesia      AS t_9,  inspection   AS t_10, x_ray     AS t_11,        rehab     AS t_12"
                                .", prescription    AS t_13, fee_etc      AS t_14, fee_total AS t_side_total"
                            ." FROM jnl_bill_diagnosis_visit"
                           ." WHERE type_section = 19 AND del_flg = 'f'"
                          .") jbdv_t"
                      ." ON jbdvm.newest_apply_id = jbdv_t.newest_apply_id ";
        return $this->getDataList($sql);
    }
}


class JnlRewardSynthesisMedi extends IndicatorData
{
    /**
     * ���󥹥ȥ饯��
     * @param  array $arr
     * @return void
     */
    function JnlRewardSynthesisMedi($arr)
    {
        parent::IndicatorData($arr);
    }

    function getJnlRewardSynthesisMedi($arr)
    {
        $rtnArr    = array();
        $typeZero  = $this->getJnlRewardSynthesisMedi_type_zero($arr);
        $typeOne   = $this->getJnlRewardSynthesisMedi_type_one($arr);
        $typeTwo   = $this->getJnlRewardSynthesisMedi_type_two($arr);
        $typeThree = $this->getJnlRewardSynthesisMedi_type_three($arr);

        $rtnArr = $this->arrayMerge($typeZero,  $rtnArr);
        $rtnArr = $this->arrayMerge($typeOne,   $rtnArr);
        $rtnArr = $this->arrayMerge($typeTwo,   $rtnArr);
        $rtnArr = $this->arrayMerge($typeThree, $rtnArr);

        return $rtnArr;
    }

    function getJnlRewardSynthesisMedi_type_zero($arr)
    {
        $facilityId = $this->getArrayValueByKeyForEmptyIsNull('facility_id', $arr);
        $year       = $this->getArrayValueByKeyForEmptyIsNull('year',        $arr);
        $month      = $this->getArrayValueByKeyForEmptyIsNull('month',       $arr);

        $sql = "SELECT jrsmm.newest_apply_id, jrsmm.emp_id, jrsmm.jnl_facility_id , jrsmm.jnl_status, jrsmm.regist_date, jrsmm.del_flg"
                   .", SUBSTR(jrsmm.regist_date, 0, 5) AS year, SUBSTR(jrsmm.regist_date, 5, 2) AS month"
                   .", a_1, a_2, a_3, a_4, a_5, a_6, a_7, a_8, a_1_5, a_2_6, a_3_7, a_4_8"
                   .", b_1, b_2, b_3, b_4, b_5, b_6, b_7, b_8, b_1_5, b_2_6, b_3_7, b_4_8"
                   .", c_1, c_2, c_3, c_4, c_5, c_6, c_7, c_8, c_1_5, c_2_6, c_3_7, c_4_8"
                   .", d_1, d_2, d_3, d_4, d_5, d_6, d_7, d_8, d_1_5, d_2_6, d_3_7, d_4_8"
                   .", abc_1, abc_2, abc_3, abcd_4, abc_5, abc_6, abc_7, abcd_8, abc_1_5, abc_2_6, abc_3_7, abcd_4_8"
                   .", e_1, e_2, e_3, e_4, e_5, e_6, e_7, e_8, e_1_5, e_2_6, e_3_7, e_4_8"
                   .", f_1, f_2, f_3, f_4, f_5, f_6, f_7, f_8, f_1_5, f_2_6, f_3_7, f_4_8"
                   .", g_1, g_2, g_3, g_4, g_5, g_6, g_7, g_8, g_1_5, g_2_6, g_3_7, g_4_8"
                   .", h_1, h_2, h_3, h_4, h_5, h_6, h_7, h_8, h_1_5, h_2_6, h_3_7, h_4_8"
                   .", i_1, i_2, i_3, i_4, i_5, i_6, i_7, i_8, i_1_5, i_2_6, i_3_7, i_4_8"
                   .", efgh_1, efgh_2, efgh_3, efghi_4, efgh_5, efgh_6, efgh_7, efghi_8, efgh_1_5, efgh_2_6, efgh_3_7, efghi_4_8"
                   .", kana_a_1, kana_i_1, kana_u_1, kana_e_1, kana_a_2, kana_i_2, kana_u_2, kana_e_2, kana_a_1_2, kana_i_1_2, kana_u_1_2, kana_e_1_2"
                   .", kana_ka_1, kana_ki_1, kana_ku_1, kana_ke_1, kana_ka_2, kana_ki_2, kana_ku_2, kana_ke_2, kana_ka_1_2, kana_ki_1_2, kana_ku_1_2, kana_ke_1_2"
                   .", total_a_1, total_a_2, total_a_3, total_a_4, total_a_5, total_a_6, total_a_7, total_a_8, total_a_1_5, total_a_2_6, total_a_3_7, total_a_4_8"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                       ." FROM jnl_reward_synthesis_medi_main"
                      ." WHERE del_flg = 'f' AND jnl_status < 2";

        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = %d", $facilityId); }
        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }

        $sql .=       ") jrsmm"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_factor"
                                .", ent1_account   AS a_1,   ent1_days   AS a_2,   ent1_points   AS a_3,   ent1_money   AS a_4"
                                .", visit1_account AS a_5,   visit1_days AS a_6,   visit1_points AS a_7,   visit1_money AS a_8"
                                .", total1_account AS a_1_5, total1_days AS a_2_6, total1_points AS a_3_7, total1_money AS a_4_8"

                                .", ent2_account   AS b_1,   ent2_days   AS b_2,   ent2_points   AS b_3,   ent2_money   AS b_4"
                                .", visit2_account AS b_5,   visit2_days AS b_6,   visit2_points AS b_7,   visit2_money AS b_8"
                                .", total2_account AS b_1_5, total2_days AS b_2_6, total2_points AS b_3_7, total2_money AS b_4_8"

                                .", ent3_account   AS c_1,   ent3_days   AS c_2,   ent3_points   AS c_3,   ent3_money   AS c_4"
                                .", visit3_account AS c_5,   visit3_days AS c_6,   visit3_points AS c_7,   visit3_money AS c_8"
                                .", total3_account AS c_1_5, total3_days AS c_2_6, total3_points AS c_3_7, total3_money AS c_4_8"

                                .", ent4_account   AS d_1,   ent4_days   AS d_2,   ent4_points   AS d_3,   ent4_money   AS d_4"
                                .", visit4_account AS d_5,   visit4_days AS d_6,   visit4_points AS d_7,   visit4_money AS d_8"
                                .", total4_account AS d_1_5, total4_days AS d_2_6, total4_points AS d_3_7, total4_money AS d_4_8"

                                .", ent5_account   AS abc_1,   ent5_days   AS abc_2,   ent5_points   AS abc_3,   ent5_money   AS abcd_4"
                                .", visit5_account AS abc_5,   visit5_days AS abc_6,   visit5_points AS abc_7,   visit5_money AS abcd_8"
                                .", total5_account AS abc_1_5, total5_days AS abc_2_6, total5_points AS abc_3_7, total5_money AS abcd_4_8"
                            ." FROM jnl_reward_synthesis_medi jrsm00"
                           ." WHERE type_section = 0 AND type_factor = 0 AND del_flg = 'f'"
                          .") jrsm00"
                      ." ON jrsmm.newest_apply_id = jrsm00.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_factor"
                                .", ent1_account   AS e_1,   ent1_days   AS e_2,   ent1_points   AS e_3,   ent1_money   AS e_4"
                                .", visit1_account AS e_5,   visit1_days AS e_6,   visit1_points AS e_7,   visit1_money AS e_8"
                                .", total1_account AS e_1_5, total1_days AS e_2_6, total1_points AS e_3_7, total1_money AS e_4_8"

                                .", ent2_account   AS f_1,   ent2_days   AS f_2,   ent2_points   AS f_3,   ent2_money   AS f_4"
                                .", visit2_account AS f_5,   visit2_days AS f_6,   visit2_points AS f_7,   visit2_money AS f_8"
                                .", total2_account AS f_1_5, total2_days AS f_2_6, total2_points AS f_3_7, total2_money AS f_4_8"

                                .", ent3_account   AS g_1,   ent3_days   AS g_2,   ent3_points   AS g_3,   ent3_money   AS g_4"
                                .", visit3_account AS g_5,   visit3_days AS g_6,   visit3_points AS g_7,   visit3_money AS g_8"
                                .", total3_account AS g_1_5, total3_days AS g_2_6, total3_points AS g_3_7, total3_money AS g_4_8"

                                .", ent4_account   AS h_1,   ent4_days   AS h_2,   ent4_points   AS h_3,   ent4_money   AS h_4"
                                .", visit4_account AS h_5,   visit4_days AS h_6,   visit4_points AS h_7,   visit4_money AS h_8"
                                .", total4_account AS h_1_5, total4_days AS h_2_6, total4_points AS h_3_7, total4_money AS h_4_8"

                                .", ent5_account   AS i_1,   ent5_days   AS i_2,   ent5_points   AS i_3,   ent5_money   AS i_4"
                                .", visit5_account AS i_5,   visit5_days AS i_6,   visit5_points AS i_7,   visit5_money AS i_8"
                                .", total5_account AS i_1_5, total5_days AS i_2_6, total5_points AS i_3_7, total5_money AS i_4_8"

                                .", ent6_account   AS efgh_1,   ent6_days   AS efgh_2,   ent6_points   AS efgh_3,   ent6_money   AS efghi_4"
                                .", visit6_account AS efgh_5,   visit6_days AS efgh_6,   visit6_points AS efgh_7,   visit6_money AS efghi_8"
                                .", total6_account AS efgh_1_5, total6_days AS efgh_2_6, total6_points AS efgh_3_7, total6_money AS efghi_4_8"
                            ." FROM jnl_reward_synthesis_medi jrsm01"
                           ." WHERE type_section = 0 AND type_factor = 1 AND del_flg = 'f'"
                          .") jrsm01"
                      ." ON jrsmm.newest_apply_id = jrsm01.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_factor"
                                .", ent1_account   AS kana_a_1,   ent1_days   AS kana_i_1,   ent1_points   AS kana_u_1,   ent1_money   AS kana_e_1"
                                .", visit1_account AS kana_a_2,   visit1_days AS kana_i_2,   visit1_points AS kana_u_2,   visit1_money AS kana_e_2"
                                .", total1_account AS kana_a_1_2, total1_days AS kana_i_1_2, total1_points AS kana_u_1_2, total1_money AS kana_e_1_2"

                                .", ent2_account   AS kana_ka_1,   ent2_days   AS kana_ki_1,   ent2_points   AS kana_ku_1,   ent2_money   AS kana_ke_1"
                                .", visit2_account AS kana_ka_2,   visit2_days AS kana_ki_2,   visit2_points AS kana_ku_2,   visit2_money AS kana_ke_2"
                                .", total2_account AS kana_ka_1_2, total2_days AS kana_ki_1_2, total2_points AS kana_ku_1_2, total2_money AS kana_ke_1_2"

                                .", ent3_account   AS total_a_1,   ent3_days   AS total_a_2,   ent3_points   AS total_a_3,   ent3_money   AS total_a_4"
                                .", visit3_account AS total_a_5,   visit3_days AS total_a_6,   visit3_points AS total_a_7,   visit3_money AS total_a_8"
                                .", total3_account AS total_a_1_5, total3_days AS total_a_2_6, total3_points AS total_a_3_7, total3_money AS total_a_4_8"
                            ." FROM jnl_reward_synthesis_medi jrsm02"
                           ." WHERE type_section = 0 AND type_factor = 2 AND del_flg = 'f'"
                          .") jrsm02"
                      ." ON jrsmm.newest_apply_id = jrsm02.newest_apply_id";

        return $this->getDataList($sql);
    }

    function getJnlRewardSynthesisMedi_type_one($arr)
    {
        $facilityId = $this->getArrayValueByKeyForEmptyIsNull('facility_id', $arr);
        $year       = $this->getArrayValueByKeyForEmptyIsNull('year',        $arr);
        $month      = $this->getArrayValueByKeyForEmptyIsNull('month',       $arr);

        $sql = "SELECT jrsmm.newest_apply_id, jrsmm.emp_id, jrsmm.jnl_facility_id, jrsmm.jnl_status, jrsmm.regist_date, jrsmm.del_flg"
                   .", SUBSTR(jrsmm.regist_date, 0, 5) AS year, SUBSTR(jrsmm.regist_date, 5, 2) AS month"
                   .", j_1, j_2, j_3, j_4, j_5, j_6, j_7, j_8, j_1_5, j_2_6, j_3_7, j_4_8"
                   .", k_1, k_2, k_3, k_4, k_5, k_6, k_7, k_8, k_1_5, k_2_6, k_3_7, k_4_8"
                   .", l_1, l_2, l_3, l_4, l_5, l_6, l_7, l_8, l_1_5, l_2_6, l_3_7, l_4_8"
                   .", m_1, m_2, m_3, m_4, m_5, m_6, m_7, m_8, m_1_5, m_2_6, m_3_7, m_4_8"
                   .", jkl_1, jkl_2, jkl_3, jklm_4, jkl_5, jkl_6, jkl_7, jklm_8, jkl_1_5, jkl_2_6, jkl_3_7, jklm_4_8"
                   .", n_1, n_2, n_3, n_4, n_5, n_6, n_7, n_8, n_1_5, n_2_6, n_3_7, n_4_8"
                   .", o_1, o_2, o_3, o_4, o_5, o_6, o_7, o_8, o_1_5, o_2_6, o_3_7, o_4_8"
                   .", p_1, p_2, p_3, p_4, p_5, p_6, p_7, p_8, p_1_5, p_2_6, p_3_7, p_4_8"
                   .", q_1, q_2, q_3, q_4, q_5, q_6, q_7, q_8, q_1_5, q_2_6, q_3_7, q_4_8"
                   .", r_1, r_2, r_3, r_4, r_5, r_6, r_7, r_8, r_1_5, r_2_6, r_3_7, r_4_8"
                   .", nopq_1, nopq_2, nopq_3, nopqr_4, nopq_5, nopq_6, nopq_7, nopqr_8, nopq_1_5, nopq_2_6, nopq_3_7, nopqr_4_8"
                   .", kana_sa_1, kana_shi_1, kana_su_1, kana_se_1, kana_sa_2, kana_shi_2, kana_su_2, kana_se_2, kana_sa_1_2, kana_shi_1_2, kana_su_1_2, kana_se_1_2"
                   .", total_b_1, total_b_2, total_b_3, total_b_4, total_b_5, total_b_6, total_b_7, total_b_8, total_b_1_5, total_b_2_6, total_b_3_7, total_b_4_8"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                       ." FROM jnl_reward_synthesis_medi_main"
                      ." WHERE del_flg = 'f' AND jnl_status < 2";

        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = %d", $facilityId); }
        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }

        $sql .=       ") jrsmm"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_factor"
                                .", ent1_account   AS j_1,   ent1_days   AS j_2,   ent1_points   AS j_3,   ent1_money   AS j_4"
                                .", visit1_account AS j_5,   visit1_days AS j_6,   visit1_points AS j_7,   visit1_money AS j_8"
                                .", total1_account AS j_1_5, total1_days AS j_2_6, total1_points AS j_3_7, total1_money AS j_4_8"

                                .", ent2_account   AS k_1,   ent2_days   AS k_2,   ent2_points   AS k_3,   ent2_money AS k_4"
                                .", visit2_account AS k_5,   visit2_days AS k_6,   visit2_points AS k_7,   visit2_money AS k_8"
                                .", total2_account AS k_1_5, total2_days AS k_2_6, total2_points AS k_3_7, total2_money AS k_4_8"

                                .", ent3_account   AS l_1,   ent3_days   AS l_2,   ent3_points   AS l_3,   ent3_money   AS l_4"
                                .", visit3_account AS l_5,   visit3_days AS l_6,   visit3_points AS l_7,   visit3_money AS l_8"
                                .", total3_account AS l_1_5, total3_days AS l_2_6, total3_points AS l_3_7, total3_money AS l_4_8"

                                .", ent4_account   AS m_1,   ent4_days   AS m_2,   ent4_points   AS m_3,   ent4_money   AS m_4"
                                .", visit4_account AS m_5,   visit4_days AS m_6,   visit4_points AS m_7,   visit4_money AS m_8"
                                .", total4_account AS m_1_5, total4_days AS m_2_6, total4_points AS m_3_7, total4_money AS m_4_8"

                                .", ent5_account   AS jkl_1,   ent5_days   AS jkl_2,   ent5_points   AS jkl_3,  ent5_money   AS jklm_4"
                                .", visit5_account AS jkl_5,   visit5_days AS jkl_6,   visit5_points AS jkl_7,  visit5_money AS jklm_8"
                                .", total5_account AS jkl_1_5, total5_days AS jkl_2_6, total5_points AS jkl_3_7,total5_money AS jklm_4_8"
                            ." FROM jnl_reward_synthesis_medi jrsm10 WHERE type_section = 1 AND type_factor = 0 AND del_flg = 'f'"
                          .") jrsm10"
                      ." ON jrsmm.newest_apply_id = jrsm10.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_factor"
                                .", ent1_account   AS n_1,   ent1_days   AS n_2,   ent1_points   AS n_3,   ent1_money   AS n_4"
                                .", visit1_account AS n_5,   visit1_days AS n_6,   visit1_points AS n_7,   visit1_money AS n_8"
                                .", total1_account AS n_1_5, total1_days AS n_2_6, total1_points AS n_3_7, total1_money AS n_4_8"

                                .", ent2_account   AS o_1,   ent2_days   AS o_2,   ent2_points   AS o_3,   ent2_money   AS o_4"
                                .", visit2_account AS o_5,   visit2_days AS o_6,   visit2_points AS o_7,   visit2_money AS o_8"
                                .", total2_account AS o_1_5, total2_days AS o_2_6, total2_points AS o_3_7, total2_money AS o_4_8"

                                .", ent3_account   AS p_1,   ent3_days   AS p_2,   ent3_points   AS p_3,   ent3_money   AS p_4"
                                .", visit3_account AS p_5,   visit3_days AS p_6,   visit3_points AS p_7,   visit3_money AS p_8"
                                .", total3_account AS p_1_5, total3_days AS p_2_6, total3_points AS p_3_7, total3_money AS p_4_8"

                                .", ent4_account   AS q_1,   ent4_days   AS q_2,   ent4_points   AS q_3,   ent4_money   AS q_4"
                                .", visit4_account AS q_5,   visit4_days AS q_6,   visit4_points AS q_7,   visit4_money AS q_8"
                                .", total4_account AS q_1_5, total4_days AS q_2_6, total4_points AS q_3_7, total4_money AS q_4_8"

                                .", ent5_account   AS r_1,   ent5_days   AS r_2,   ent5_points   AS r_3,   ent5_money   AS r_4"
                                .", visit5_account AS r_5,   visit5_days AS r_6,   visit5_points AS r_7,   visit5_money AS r_8"
                                .", total5_account AS r_1_5, total5_days AS r_2_6, total5_points AS r_3_7, total5_money AS r_4_8"

                                .", ent6_account   AS nopq_1,   ent6_days   AS nopq_2,   ent6_points   AS nopq_3,   ent6_money   AS nopqr_4"
                                .", visit6_account AS nopq_5,   visit6_days AS nopq_6,   visit6_points AS nopq_7,   visit6_money AS nopqr_8"
                                .", total6_account AS nopq_1_5, total6_days AS nopq_2_6, total6_points AS nopq_3_7, total6_money AS nopqr_4_8"
                            ." FROM jnl_reward_synthesis_medi jrsm11 WHERE type_section = 1 AND type_factor = 1 AND del_flg = 'f'"
                          .") jrsm11"
                      ." ON jrsmm.newest_apply_id = jrsm11.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_factor"
                                .", ent1_account   AS kana_sa_1,   ent1_days   AS kana_shi_1,   ent1_points   AS kana_su_1,   ent1_money   AS kana_se_1"
                                .", visit1_account AS kana_sa_2,   visit1_days AS kana_shi_2,   visit1_points AS kana_su_2,   visit1_money AS kana_se_2"
                                .", total1_account AS kana_sa_1_2, total1_days AS kana_shi_1_2, total1_points AS kana_su_1_2, total1_money AS kana_se_1_2"

                                .", ent2_account   AS total_b_1,   ent2_days   AS total_b_2,   ent2_points   AS total_b_3,   ent2_money   AS total_b_4"
                                .", visit2_account AS total_b_5,   visit2_days AS total_b_6,   visit2_points AS total_b_7,   visit2_money AS total_b_8"
                                .", total2_account AS total_b_1_5, total2_days AS total_b_2_6, total2_points AS total_b_3_7, total2_money AS total_b_4_8"
                            ." FROM jnl_reward_synthesis_medi jrsm12 WHERE type_section = 1 AND type_factor = 2 AND del_flg = 'f'"
                          .") jrsm12"
                      ." ON jrsmm.newest_apply_id = jrsm12.newest_apply_id ";

        return $this->getDataList($sql);
    }

    function getJnlRewardSynthesisMedi_type_two($arr)
    {
        $facilityId = $this->getArrayValueByKeyForEmptyIsNull('facility_id', $arr);
        $year       = $this->getArrayValueByKeyForEmptyIsNull('year',        $arr);
        $month      = $this->getArrayValueByKeyForEmptyIsNull('month',       $arr);

        $sql = "SELECT jrsmm.newest_apply_id, jrsmm.emp_id, jrsmm.jnl_facility_id, jrsmm.jnl_status, jrsmm.regist_date, jrsmm.del_flg"
                   .", SUBSTR(jrsmm.regist_date, 0, 5) AS year, SUBSTR(jrsmm.regist_date, 5, 2) AS month"
                   .", s_1, s_2, s_3, s_4, s_5, s_6, s_7, s_8, s_1_5, s_2_6, s_3_7, s_4_8"
                   .", t_1, t_2, t_3, t_4, t_5, t_6, t_7, t_8, t_1_5, t_2_6, t_3_7, t_4_8"
                   .", u_1, u_2, u_3, u_4, u_5, u_6, u_7, u_8, u_1_5, u_2_6, u_3_7, u_4_8"
                   .", v_1, v_2, v_3, v_4, v_5, v_6, v_7, v_8, v_1_5, v_2_6, v_3_7, v_4_8"
                   .", stuv_1, stuv_2, stuv_3, stuv_4, stuv_5, stuv_6, stuv_7, stuv_8, stuv_1_5, stuv_2_6, stuv_3_7, stuv_4_8"
                   .", w_1, w_2, w_3, w_4, w_5, w_6, w_7, w_8, w_1_5, w_2_6, w_3_7, w_4_8"
                   .", x_1, x_2, x_3, x_4, x_5, x_6, x_7, x_8, x_1_5, x_2_6, x_3_7, x_4_8"
                   .", y_1, y_2, y_3, y_4, y_5, y_6, y_7, y_8, y_1_5, y_2_6, y_3_7, y_4_8"
                   .", z_1, z_2, z_3, z_4, z_5, z_6, z_7, z_8, z_1_5, z_2_6, z_3_7, z_4_8"
                   .", ab_1, ab_2, ab_3, ab_4, ab_5, ab_6, ab_7, ab_8, ab_1_5, ab_2_6, ab_3_7, ab_4_8"
                   .", ac_1, ac_2, ac_3, ac_4, ac_5, ac_6, ac_7, ac_8, ac_1_5, ac_2_6, ac_3_7, ac_4_8"
                   .", kana_ta_1, kana_chi_1, kana_te_1, kana_to_1, kana_ta_2, kana_chi_2, kana_te_2, kana_to_2, kana_ta_1_2, kana_chi_1_2, kana_te_1_2, kana_to_1_2"
                   .", total_c_1, total_c_2, total_c_3, total_c_4, total_c_5, total_c_6, total_c_7, total_c_8, total_c_1_5, total_c_2_6, total_c_3_7, total_c_4_8"
                   .", total_abc_1, total_abc_2, total_abc_3, total_abc_4, total_abc_5, total_abc_6, total_abc_7, total_abc_8, total_abc_1_5, total_abc_2_6, total_abc_3_7, total_abc_4_8"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                       ." FROM jnl_reward_synthesis_medi_main"
                      ." WHERE del_flg = 'f' AND jnl_status < 2";

        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = %d", $facilityId); }
        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }

        $sql .=       ") jrsmm"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_factor"
                                .", ent1_account   AS s_1,   ent1_days   AS s_2,   ent1_points   AS s_3,   ent1_money   AS s_4"
                                .", visit1_account AS s_5,   visit1_days AS s_6,   visit1_points AS s_7,   visit1_money AS s_8"
                                .", total1_account AS s_1_5, total1_days AS s_2_6, total1_points AS s_3_7, total1_money AS s_4_8"

                                .", ent2_account   AS t_1,   ent2_days   AS t_2,   ent2_points   AS t_3,   ent2_money   AS t_4"
                                .", visit2_account AS t_5,   visit2_days AS t_6,   visit2_points AS t_7,   visit2_money AS t_8"
                                .", total2_account AS t_1_5, total2_days AS t_2_6, total2_points AS t_3_7, total2_money AS t_4_8"

                                .", ent3_account   AS u_1,   ent3_days   AS u_2,   ent3_points   AS u_3,   ent3_money   AS u_4"
                                .", visit3_account AS u_5,   visit3_days AS u_6,   visit3_points AS u_7,   visit3_money AS u_8"
                                .", total3_account AS u_1_5, total3_days AS u_2_6, total3_points AS u_3_7, total3_money AS u_4_8"

                                .", ent4_account   AS v_1,   ent4_days   AS v_2,   ent4_points   AS v_3,   ent4_money   AS v_4"
                                .", visit4_account AS v_5,   visit4_days AS v_6,   visit4_points AS v_7,   visit4_money AS v_8"
                                .", total4_account AS v_1_5, total4_days AS v_2_6, total4_points AS v_3_7, total4_money AS v_4_8"

                                .", ent5_account   AS stuv_1,   ent5_days   AS stuv_2,   ent5_points   AS stuv_3,   ent5_money   AS stuv_4"
                                .", visit5_account AS stuv_5,   visit5_days AS stuv_6,   visit5_points AS stuv_7,   visit5_money AS stuv_8"
                                .", total5_account AS stuv_1_5, total5_days AS stuv_2_6, total5_points AS stuv_3_7, total5_money AS stuv_4_8"
                            ." FROM jnl_reward_synthesis_medi jrsm20"
                           ." WHERE type_section = 2 AND type_factor = 0 AND del_flg = 'f'"
                          .") jrsm20"
                      ." ON jrsmm.newest_apply_id = jrsm20.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_factor"
                                .", ent1_account   AS w_1,   ent1_days   AS w_2,   ent1_points   AS w_3,   ent1_money   AS w_4"
                                .", visit1_account AS w_5,   visit1_days AS w_6,   visit1_points AS w_7,   visit1_money AS w_8"
                                .", total1_account AS w_1_5, total1_days AS w_2_6, total1_points AS w_3_7, total1_money AS w_4_8"

                                .", ent2_account   AS x_1,   ent2_days   AS x_2,   ent2_points   AS x_3,   ent2_money   AS x_4"
                                .", visit2_account AS x_5,   visit2_days AS x_6,   visit2_points AS x_7,   visit2_money AS x_8"
                                .", total2_account AS x_1_5, total2_days AS x_2_6, total2_points AS x_3_7, total2_money AS x_4_8"

                                .", ent3_account   AS y_1,   ent3_days   AS y_2,   ent3_points   AS y_3,   ent3_money AS y_4"
                                .", visit3_account AS y_5,   visit3_days AS y_6,   visit3_points AS y_7,   visit3_money AS y_8"
                                .", total3_account AS y_1_5, total3_days AS y_2_6, total3_points AS y_3_7, total3_money AS y_4_8"

                                .", ent4_account   AS z_1,   ent4_days   AS z_2,   ent4_points   AS z_3,   ent4_money AS z_4"
                                .", visit4_account AS z_5,   visit4_days AS z_6,   visit4_points AS z_7,   visit4_money AS z_8"
                                .", total4_account AS z_1_5, total4_days AS z_2_6, total4_points AS z_3_7, total4_money AS z_4_8"

                                .", ent5_account   AS ab_1,   ent5_days   AS ab_2,   ent5_points   AS ab_3,   ent5_money   AS ab_4"
                                .", visit5_account AS ab_5,   visit5_days AS ab_6,   visit5_points AS ab_7,   visit5_money AS ab_8"
                                .", total5_account AS ab_1_5, total5_days AS ab_2_6, total5_points AS ab_3_7, total5_money AS ab_4_8"

                                .", ent6_account   AS ac_1,   ent6_days   AS ac_2,   ent6_points   AS ac_3,   ent6_money   AS ac_4"
                                .", visit6_account AS ac_5,   visit6_days AS ac_6,   visit6_points AS ac_7,   visit6_money AS ac_8"
                                .", total6_account AS ac_1_5, total6_days AS ac_2_6, total6_points AS ac_3_7, total6_money AS ac_4_8"
                            ." FROM jnl_reward_synthesis_medi jrsm21"
                           ." WHERE type_section = 2 AND type_factor = 1 AND del_flg = 'f'"
                          .") jrsm21"
                      ." ON jrsmm.newest_apply_id = jrsm21.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_factor"
                                .", ent1_account   AS kana_ta_1,   ent1_days   AS kana_chi_1,   ent1_points   AS kana_te_1,   ent1_money   AS kana_to_1"
                                .", visit1_account AS kana_ta_2,   visit1_days AS kana_chi_2,   visit1_points AS kana_te_2,   visit1_money AS kana_to_2"
                                .", total1_account AS kana_ta_1_2, total1_days AS kana_chi_1_2, total1_points AS kana_te_1_2, total1_money AS kana_to_1_2"

                                .", ent2_account   AS total_c_1,   ent2_days   AS total_c_2,   ent2_points   AS total_c_3,   ent2_money   AS total_c_4"
                                .", visit2_account AS total_c_5,   visit2_days AS total_c_6,   visit2_points AS total_c_7,   visit2_money AS total_c_8"
                                .", total2_account AS total_c_1_5, total2_days AS total_c_2_6, total2_points AS total_c_3_7, total2_money AS total_c_4_8"

                                .", ent3_account   AS total_abc_1,   ent3_days   AS total_abc_2,   ent3_points   AS total_abc_3,   ent3_money   AS total_abc_4"
                                .", visit3_account AS total_abc_5,   visit3_days AS total_abc_6,   visit3_points AS total_abc_7,   visit3_money AS total_abc_8"
                                .", total3_account AS total_abc_1_5, total3_days AS total_abc_2_6, total3_points AS total_abc_3_7, total3_money AS total_abc_4_8"
                            ." FROM jnl_reward_synthesis_medi jrsm22"
                           ." WHERE type_section = 2 AND type_factor = 2 AND del_flg = 'f'"
                          .") jrsm22"
                      ." ON jrsmm.newest_apply_id = jrsm22.newest_apply_id ";
        return $this->getDataList($sql);
    }

    function getJnlRewardSynthesisMedi_type_three($arr)
    {
        $facilityId = $this->getArrayValueByKeyForEmptyIsNull('facility_id', $arr);
        $year       = $this->getArrayValueByKeyForEmptyIsNull('year',        $arr);
        $month      = $this->getArrayValueByKeyForEmptyIsNull('month',       $arr);

        $sql = "SELECT jrsmm.newest_apply_id, jrsmm.emp_id, jrsmm.jnl_facility_id, jrsmm.jnl_status, jrsmm.regist_date, jrsmm.del_flg"
                   .", SUBSTR(jrsmm.regist_date, 0, 5) AS year, SUBSTR(jrsmm.regist_date, 5, 2) AS month"
                   .", claim_this_mon_1, claim_this_mon_2,  claim_this_mon_3,  claim_this_mon_4"
                   .", claim_this_mon_5, claim_this_mon_6,  claim_this_mon_7,  claim_this_mon_8"
                   .", claim_this_mon_9, claim_this_mon_10, claim_this_mon_11, claim_this_mon_12"

                   .", claim_prev_year_1, claim_prev_year_2,  claim_prev_year_3,  claim_prev_year_4"
                   .", claim_prev_year_5, claim_prev_year_6,  claim_prev_year_7,  claim_prev_year_8"
                   .", claim_prev_year_9, claim_prev_year_10, claim_prev_year_11, claim_prev_year_12"

                   .", difference_1, difference_2,  difference_3,  difference_4"
                   .", difference_5, difference_6,  difference_7,  difference_8"
                   .", difference_9, difference_10, difference_11, difference_12"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                       ." FROM jnl_reward_synthesis_medi_main"
                      ." WHERE del_flg = 'f' AND jnl_status < 2";

        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = %d", $facilityId); }
        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }

        $sql .=      ") jrsmm".
                     " INNER JOIN (SELECT newest_apply_id, type_section, type_factor"
                                      .", ent1_account   AS claim_this_mon_1, ent1_days   AS claim_this_mon_2,  ent1_points   AS claim_this_mon_3,  ent1_money   AS claim_this_mon_4"
                                      .", visit1_account AS claim_this_mon_5, visit1_days AS claim_this_mon_6,  visit1_points AS claim_this_mon_7,  visit1_money AS claim_this_mon_8"
                                      .", total1_account AS claim_this_mon_9, total1_days AS claim_this_mon_10, total1_points AS claim_this_mon_11, total1_money AS claim_this_mon_12"

                                      .", ent2_account   AS claim_prev_year_1, ent2_days   AS claim_prev_year_2,  ent2_points   AS claim_prev_year_3,  ent2_money   AS claim_prev_year_4"
                                      .", visit2_account AS claim_prev_year_5, visit2_days AS claim_prev_year_6,  visit2_points AS claim_prev_year_7,  visit2_money AS claim_prev_year_8"
                                      .", total2_account AS claim_prev_year_9, total2_days AS claim_prev_year_10, total2_points AS claim_prev_year_11, total2_money AS claim_prev_year_12"

                                      .", ent3_account   AS difference_1, ent3_days   AS difference_2,  ent3_points   AS difference_3,  ent3_money   AS difference_4"
                                      .", visit3_account AS difference_5, visit3_days AS difference_6,  visit3_points AS difference_7,  visit3_money AS difference_8"
                                      .", total3_account AS difference_9, total3_days AS difference_10, total3_points AS difference_11, total3_money AS difference_12"
                                  ." FROM jnl_reward_synthesis_medi jrsm32"
                                 ." WHERE type_section = 3 AND type_factor = 2 AND del_flg = 'f'"
                                .") jrsm32"
                            ." ON jrsmm.newest_apply_id = jrsm32.newest_apply_id ";
        return $this->getDataList($sql);
    }
}

class JnlRewardSynthesisDental extends IndicatorData
{
    /**
     * ���󥹥ȥ饯��
     * @param  array $arr
     * @return void
     */
    function JnlRewardSynthesisDental($arr)
    {
        parent::IndicatorData($arr);
    }

    function getJnlRewardSynthesisDental($arr)
    {
        $rtnArr    = array();
        $typeZero  = $this->getJnlRewardSynthesisDental_type_zero($arr);
        $typeOne   = $this->getJnlRewardSynthesisDental_type_one($arr);
        $typeTwo   = $this->getJnlRewardSynthesisDental_type_two($arr);
        $typeThree = $this->getJnlRewardSynthesisDental_type_three($arr);

        $rtnArr = $this->arrayMerge($typeZero,  $rtnArr);
        $rtnArr = $this->arrayMerge($typeOne,   $rtnArr);
        $rtnArr = $this->arrayMerge($typeTwo,   $rtnArr);
        $rtnArr = $this->arrayMerge($typeThree, $rtnArr);

        return $rtnArr;
    }

    function getJnlRewardSynthesisDental_type_zero($arr)
    {
        $facilityId = $this->getArrayValueByKeyForEmptyIsNull('facility_id', $arr);
        $year       = $this->getArrayValueByKeyForEmptyIsNull('year',        $arr);
        $month      = $this->getArrayValueByKeyForEmptyIsNull('month',       $arr);

        $sql = "SELECT jrsdm.newest_apply_id, jrsdm.emp_id, jrsdm.jnl_facility_id , jrsdm.jnl_status, jrsdm.regist_date, jrsdm.del_flg"
                   .", SUBSTR(jrsdm.regist_date, 0, 5) AS year, SUBSTR(jrsdm.regist_date, 5, 2) AS month"
                   .", a_1, a_2, a_3, a_4, a_5, a_6, a_7, a_8, a_1_5, a_2_6, a_3_7, a_4_8"
                   .", b_1, b_2, b_3, b_4, b_5, b_6, b_7, b_8, b_1_5, b_2_6, b_3_7, b_4_8"
                   .", c_1, c_2, c_3, c_4, c_5, c_6, c_7, c_8, c_1_5, c_2_6, c_3_7, c_4_8"
                   .", d_1, d_2, d_3, d_4, d_5, d_6, d_7, d_8, d_1_5, d_2_6, d_3_7, d_4_8"
                   .", abc_1, abc_2, abc_3, abcd_4, abc_5, abc_6, abc_7, abcd_8, abc_1_5, abc_2_6, abc_3_7, abcd_4_8"
                   .", e_1, e_2, e_3, e_4, e_5, e_6, e_7, e_8, e_1_5, e_2_6, e_3_7, e_4_8"
                   .", f_1, f_2, f_3, f_4, f_5, f_6, f_7, f_8, f_1_5, f_2_6, f_3_7, f_4_8"
                   .", g_1, g_2, g_3, g_4, g_5, g_6, g_7, g_8, g_1_5, g_2_6, g_3_7, g_4_8"
                   .", h_1, h_2, h_3, h_4, h_5, h_6, h_7, h_8, h_1_5, h_2_6, h_3_7, h_4_8"
                   .", i_1, i_2, i_3, i_4, i_5, i_6, i_7, i_8, i_1_5, i_2_6, i_3_7, i_4_8"
                   .", efgh_1, efgh_2, efgh_3, efghi_4, efgh_5, efgh_6, efgh_7, efghi_8, efgh_1_5, efgh_2_6, efgh_3_7, efghi_4_8"
                   .", kana_a_1, kana_i_1, kana_u_1, kana_e_1, kana_a_2, kana_i_2, kana_u_2, kana_e_2, kana_a_1_2, kana_i_1_2, kana_u_1_2, kana_e_1_2"
                   .", kana_ka_1, kana_ki_1, kana_ku_1, kana_ke_1, kana_ka_2, kana_ki_2, kana_ku_2, kana_ke_2, kana_ka_1_2, kana_ki_1_2, kana_ku_1_2, kana_ke_1_2"
                   .", total_a_1, total_a_2, total_a_3, total_a_4, total_a_5, total_a_6, total_a_7, total_a_8, total_a_1_5, total_a_2_6, total_a_3_7, total_a_4_8"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                       ." FROM jnl_reward_synthesis_dental_main"
                      ." WHERE del_flg = 'f' AND jnl_status < 2";

        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = %d", $facilityId); }
        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }

        $sql .=       ") jrsdm"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_factor"
                                .", ent1_account   AS a_1,   ent1_days   AS a_2,   ent1_points   AS a_3,   ent1_money   AS a_4"
                                .", visit1_account AS a_5,   visit1_days AS a_6,   visit1_points AS a_7,   visit1_money AS a_8"
                                .", total1_account AS a_1_5, total1_days AS a_2_6, total1_points AS a_3_7, total1_money AS a_4_8"

                                .", ent2_account   AS b_1,   ent2_days   AS b_2,   ent2_points   AS b_3,   ent2_money   AS b_4"
                                .", visit2_account AS b_5,   visit2_days AS b_6,   visit2_points AS b_7,   visit2_money AS b_8"
                                .", total2_account AS b_1_5, total2_days AS b_2_6, total2_points AS b_3_7, total2_money AS b_4_8"

                                .", ent3_account   AS c_1,   ent3_days   AS c_2,   ent3_points   AS c_3,   ent3_money   AS c_4"
                                .", visit3_account AS c_5,   visit3_days AS c_6,   visit3_points AS c_7,   visit3_money AS c_8"
                                .", total3_account AS c_1_5, total3_days AS c_2_6, total3_points AS c_3_7, total3_money AS c_4_8"

                                .", ent4_account   AS d_1,   ent4_days   AS d_2,   ent4_points   AS d_3,   ent4_money   AS d_4"
                                .", visit4_account AS d_5,   visit4_days AS d_6,   visit4_points AS d_7,   visit4_money AS d_8"
                                .", total4_account AS d_1_5, total4_days AS d_2_6, total4_points AS d_3_7, total4_money AS d_4_8"

                                .", ent5_account   AS abc_1,   ent5_days   AS abc_2,   ent5_points   AS abc_3,   ent5_money   AS abcd_4"
                                .", visit5_account AS abc_5,   visit5_days AS abc_6,   visit5_points AS abc_7,   visit5_money AS abcd_8"
                                .", total5_account AS abc_1_5, total5_days AS abc_2_6, total5_points AS abc_3_7, total5_money AS abcd_4_8"
                            ." FROM jnl_reward_synthesis_dental jrsd00"
                           ." WHERE type_section = 0 AND type_factor = 0 AND del_flg = 'f'"
                          .") jrsd00"
                      ." ON jrsdm.newest_apply_id = jrsd00.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_factor"
                                .", ent1_account   AS e_1,   ent1_days   AS e_2,   ent1_points   AS e_3,   ent1_money   AS e_4"
                                .", visit1_account AS e_5,   visit1_days AS e_6,   visit1_points AS e_7,   visit1_money AS e_8"
                                .", total1_account AS e_1_5, total1_days AS e_2_6, total1_points AS e_3_7, total1_money AS e_4_8"

                                .", ent2_account   AS f_1,   ent2_days   AS f_2,   ent2_points   AS f_3,   ent2_money   AS f_4"
                                .", visit2_account AS f_5,   visit2_days AS f_6,   visit2_points AS f_7,   visit2_money AS f_8"
                                .", total2_account AS f_1_5, total2_days AS f_2_6, total2_points AS f_3_7, total2_money AS f_4_8"

                                .", ent3_account   AS g_1,   ent3_days   AS g_2,   ent3_points   AS g_3,   ent3_money   AS g_4"
                                .", visit3_account AS g_5,   visit3_days AS g_6,   visit3_points AS g_7,   visit3_money AS g_8"
                                .", total3_account AS g_1_5, total3_days AS g_2_6, total3_points AS g_3_7, total3_money AS g_4_8"

                                .", ent4_account   AS h_1,   ent4_days   AS h_2,   ent4_points   AS h_3,   ent4_money   AS h_4"
                                .", visit4_account AS h_5,   visit4_days AS h_6,   visit4_points AS h_7,   visit4_money AS h_8"
                                .", total4_account AS h_1_5, total4_days AS h_2_6, total4_points AS h_3_7, total4_money AS h_4_8"

                                .", ent5_account   AS i_1,   ent5_days   AS i_2,   ent5_points   AS i_3,   ent5_money   AS i_4"
                                .", visit5_account AS i_5,   visit5_days AS i_6,   visit5_points AS i_7,   visit5_money AS i_8"
                                .", total5_account AS i_1_5, total5_days AS i_2_6, total5_points AS i_3_7, total5_money AS i_4_8"

                                .", ent6_account   AS efgh_1,   ent6_days   AS efgh_2,   ent6_points   AS efgh_3,   ent6_money   AS efghi_4"
                                .", visit6_account AS efgh_5,   visit6_days AS efgh_6,   visit6_points AS efgh_7,   visit6_money AS efghi_8"
                                .", total6_account AS efgh_1_5, total6_days AS efgh_2_6, total6_points AS efgh_3_7, total6_money AS efghi_4_8"
                            ." FROM jnl_reward_synthesis_dental jrsd01"
                           ." WHERE type_section = 0 AND type_factor = 1 AND del_flg = 'f'"
                          .") jrsd01"
                      ." ON jrsdm.newest_apply_id = jrsd01.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_factor"
                                .", ent1_account   AS kana_a_1,   ent1_days   AS kana_i_1,   ent1_points   AS kana_u_1,   ent1_money   AS kana_e_1"
                                .", visit1_account AS kana_a_2,   visit1_days AS kana_i_2,   visit1_points AS kana_u_2,   visit1_money AS kana_e_2"
                                .", total1_account AS kana_a_1_2, total1_days AS kana_i_1_2, total1_points AS kana_u_1_2, total1_money AS kana_e_1_2"

                                .", ent2_account   AS kana_ka_1,   ent2_days   AS kana_ki_1,   ent2_points   AS kana_ku_1,   ent2_money   AS kana_ke_1"
                                .", visit2_account AS kana_ka_2,   visit2_days AS kana_ki_2,   visit2_points AS kana_ku_2,   visit2_money AS kana_ke_2"
                                .", total2_account AS kana_ka_1_2, total2_days AS kana_ki_1_2, total2_points AS kana_ku_1_2, total2_money AS kana_ke_1_2"

                                .", ent3_account   AS total_a_1,   ent3_days   AS total_a_2,   ent3_points   AS total_a_3,   ent3_money   AS total_a_4"
                                .", visit3_account AS total_a_5,   visit3_days AS total_a_6,   visit3_points AS total_a_7,   visit3_money AS total_a_8"
                                .", total3_account AS total_a_1_5, total3_days AS total_a_2_6, total3_points AS total_a_3_7, total3_money AS total_a_4_8"
                            ." FROM jnl_reward_synthesis_dental jrsd02"
                           ." WHERE type_section = 0 AND type_factor = 2 AND del_flg = 'f'"
                          .") jrsd02"
                      ." ON jrsdm.newest_apply_id = jrsd02.newest_apply_id";

        return $this->getDataList($sql);
    }

    function getJnlRewardSynthesisDental_type_one($arr)
    {
        $facilityId = $this->getArrayValueByKeyForEmptyIsNull('facility_id', $arr);
        $year       = $this->getArrayValueByKeyForEmptyIsNull('year',        $arr);
        $month      = $this->getArrayValueByKeyForEmptyIsNull('month',       $arr);

        $sql = "SELECT jrsdm.newest_apply_id, jrsdm.emp_id, jrsdm.jnl_facility_id, jrsdm.jnl_status, jrsdm.regist_date, jrsdm.del_flg"
                   .", SUBSTR(jrsdm.regist_date, 0, 5) AS year, SUBSTR(jrsdm.regist_date, 5, 2) AS month"
                   .", j_1, j_2, j_3, j_4, j_5, j_6, j_7, j_8, j_1_5, j_2_6, j_3_7, j_4_8"
                   .", k_1, k_2, k_3, k_4, k_5, k_6, k_7, k_8, k_1_5, k_2_6, k_3_7, k_4_8"
                   .", l_1, l_2, l_3, l_4, l_5, l_6, l_7, l_8, l_1_5, l_2_6, l_3_7, l_4_8"
                   .", m_1, m_2, m_3, m_4, m_5, m_6, m_7, m_8, m_1_5, m_2_6, m_3_7, m_4_8"
                   .", jkl_1, jkl_2, jkl_3, jklm_4, jkl_5, jkl_6, jkl_7, jklm_8, jkl_1_5, jkl_2_6, jkl_3_7, jklm_4_8"
                   .", n_1, n_2, n_3, n_4, n_5, n_6, n_7, n_8, n_1_5, n_2_6, n_3_7, n_4_8"
                   .", o_1, o_2, o_3, o_4, o_5, o_6, o_7, o_8, o_1_5, o_2_6, o_3_7, o_4_8"
                   .", p_1, p_2, p_3, p_4, p_5, p_6, p_7, p_8, p_1_5, p_2_6, p_3_7, p_4_8"
                   .", q_1, q_2, q_3, q_4, q_5, q_6, q_7, q_8, q_1_5, q_2_6, q_3_7, q_4_8"
                   .", r_1, r_2, r_3, r_4, r_5, r_6, r_7, r_8, r_1_5, r_2_6, r_3_7, r_4_8"
                   .", nopq_1, nopq_2, nopq_3, nopqr_4, nopq_5, nopq_6, nopq_7, nopqr_8, nopq_1_5, nopq_2_6, nopq_3_7, nopqr_4_8"
                   .", kana_sa_1, kana_shi_1, kana_su_1, kana_se_1, kana_sa_2, kana_shi_2, kana_su_2, kana_se_2, kana_sa_1_2, kana_shi_1_2, kana_su_1_2, kana_se_1_2"
                   .", total_b_1, total_b_2, total_b_3, total_b_4, total_b_5, total_b_6, total_b_7, total_b_8, total_b_1_5, total_b_2_6, total_b_3_7, total_b_4_8"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                       ." FROM jnl_reward_synthesis_dental_main"
                      ." WHERE del_flg = 'f' AND jnl_status < 2";

        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = %d", $facilityId); }
        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }

        $sql .=       ") jrsdm"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_factor"
                                .", ent1_account   AS j_1,   ent1_days   AS j_2,   ent1_points   AS j_3,   ent1_money   AS j_4"
                                .", visit1_account AS j_5,   visit1_days AS j_6,   visit1_points AS j_7,   visit1_money AS j_8"
                                .", total1_account AS j_1_5, total1_days AS j_2_6, total1_points AS j_3_7, total1_money AS j_4_8"

                                .", ent2_account   AS k_1,   ent2_days   AS k_2,   ent2_points   AS k_3,   ent2_money AS k_4"
                                .", visit2_account AS k_5,   visit2_days AS k_6,   visit2_points AS k_7,   visit2_money AS k_8"
                                .", total2_account AS k_1_5, total2_days AS k_2_6, total2_points AS k_3_7, total2_money AS k_4_8"

                                .", ent3_account   AS l_1,   ent3_days   AS l_2,   ent3_points   AS l_3,   ent3_money   AS l_4"
                                .", visit3_account AS l_5,   visit3_days AS l_6,   visit3_points AS l_7,   visit3_money AS l_8"
                                .", total3_account AS l_1_5, total3_days AS l_2_6, total3_points AS l_3_7, total3_money AS l_4_8"

                                .", ent4_account   AS m_1,   ent4_days   AS m_2,   ent4_points   AS m_3,   ent4_money   AS m_4"
                                .", visit4_account AS m_5,   visit4_days AS m_6,   visit4_points AS m_7,   visit4_money AS m_8"
                                .", total4_account AS m_1_5, total4_days AS m_2_6, total4_points AS m_3_7, total4_money AS m_4_8"

                                .", ent5_account   AS jkl_1,   ent5_days   AS jkl_2,   ent5_points   AS jkl_3,  ent5_money   AS jklm_4"
                                .", visit5_account AS jkl_5,   visit5_days AS jkl_6,   visit5_points AS jkl_7,  visit5_money AS jklm_8"
                                .", total5_account AS jkl_1_5, total5_days AS jkl_2_6, total5_points AS jkl_3_7,total5_money AS jklm_4_8"
                            ." FROM jnl_reward_synthesis_dental jrsd10 WHERE type_section = 1 AND type_factor = 0 AND del_flg = 'f'"
                          .") jrsd10"
                      ." ON jrsdm.newest_apply_id = jrsd10.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_factor"
                                .", ent1_account   AS n_1,   ent1_days   AS n_2,   ent1_points   AS n_3,   ent1_money   AS n_4"
                                .", visit1_account AS n_5,   visit1_days AS n_6,   visit1_points AS n_7,   visit1_money AS n_8"
                                .", total1_account AS n_1_5, total1_days AS n_2_6, total1_points AS n_3_7, total1_money AS n_4_8"

                                .", ent2_account   AS o_1,   ent2_days   AS o_2,   ent2_points   AS o_3,   ent2_money   AS o_4"
                                .", visit2_account AS o_5,   visit2_days AS o_6,   visit2_points AS o_7,   visit2_money AS o_8"
                                .", total2_account AS o_1_5, total2_days AS o_2_6, total2_points AS o_3_7, total2_money AS o_4_8"

                                .", ent3_account   AS p_1,   ent3_days   AS p_2,   ent3_points   AS p_3,   ent3_money   AS p_4"
                                .", visit3_account AS p_5,   visit3_days AS p_6,   visit3_points AS p_7,   visit3_money AS p_8"
                                .", total3_account AS p_1_5, total3_days AS p_2_6, total3_points AS p_3_7, total3_money AS p_4_8"

                                .", ent4_account   AS q_1,   ent4_days   AS q_2,   ent4_points   AS q_3,   ent4_money   AS q_4"
                                .", visit4_account AS q_5,   visit4_days AS q_6,   visit4_points AS q_7,   visit4_money AS q_8"
                                .", total4_account AS q_1_5, total4_days AS q_2_6, total4_points AS q_3_7, total4_money AS q_4_8"

                                .", ent5_account   AS r_1,   ent5_days   AS r_2,   ent5_points   AS r_3,   ent5_money   AS r_4"
                                .", visit5_account AS r_5,   visit5_days AS r_6,   visit5_points AS r_7,   visit5_money AS r_8"
                                .", total5_account AS r_1_5, total5_days AS r_2_6, total5_points AS r_3_7, total5_money AS r_4_8"

                                .", ent6_account   AS nopq_1,   ent6_days   AS nopq_2,   ent6_points   AS nopq_3,   ent6_money   AS nopqr_4"
                                .", visit6_account AS nopq_5,   visit6_days AS nopq_6,   visit6_points AS nopq_7,   visit6_money AS nopqr_8"
                                .", total6_account AS nopq_1_5, total6_days AS nopq_2_6, total6_points AS nopq_3_7, total6_money AS nopqr_4_8"
                            ." FROM jnl_reward_synthesis_dental jrsd11 WHERE type_section = 1 AND type_factor = 1 AND del_flg = 'f'"
                          .") jrsd11"
                      ." ON jrsdm.newest_apply_id = jrsd11.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_factor"
                                .", ent1_account   AS kana_sa_1,   ent1_days   AS kana_shi_1,   ent1_points   AS kana_su_1,   ent1_money   AS kana_se_1"
                                .", visit1_account AS kana_sa_2,   visit1_days AS kana_shi_2,   visit1_points AS kana_su_2,   visit1_money AS kana_se_2"
                                .", total1_account AS kana_sa_1_2, total1_days AS kana_shi_1_2, total1_points AS kana_su_1_2, total1_money AS kana_se_1_2"

                                .", ent2_account   AS total_b_1,   ent2_days   AS total_b_2,   ent2_points   AS total_b_3,   ent2_money   AS total_b_4"
                                .", visit2_account AS total_b_5,   visit2_days AS total_b_6,   visit2_points AS total_b_7,   visit2_money AS total_b_8"
                                .", total2_account AS total_b_1_5, total2_days AS total_b_2_6, total2_points AS total_b_3_7, total2_money AS total_b_4_8"
                            ." FROM jnl_reward_synthesis_dental jrsd12 WHERE type_section = 1 AND type_factor = 2 AND del_flg = 'f'"
                          .") jrsd12"
                      ." ON jrsdm.newest_apply_id = jrsd12.newest_apply_id ";

        return $this->getDataList($sql);
    }

    function getJnlRewardSynthesisDental_type_two($arr)
    {
        $facilityId = $this->getArrayValueByKeyForEmptyIsNull('facility_id', $arr);
        $year       = $this->getArrayValueByKeyForEmptyIsNull('year',        $arr);
        $month      = $this->getArrayValueByKeyForEmptyIsNull('month',       $arr);

        $sql = "SELECT jrsdm.newest_apply_id, jrsdm.emp_id, jrsdm.jnl_facility_id, jrsdm.jnl_status, jrsdm.regist_date, jrsdm.del_flg"
                   .", SUBSTR(jrsdm.regist_date, 0, 5) AS year, SUBSTR(jrsdm.regist_date, 5, 2) AS month"
                   .", s_1, s_2, s_3, s_4, s_5, s_6, s_7, s_8, s_1_5, s_2_6, s_3_7, s_4_8"
                   .", t_1, t_2, t_3, t_4, t_5, t_6, t_7, t_8, t_1_5, t_2_6, t_3_7, t_4_8"
                   .", u_1, u_2, u_3, u_4, u_5, u_6, u_7, u_8, u_1_5, u_2_6, u_3_7, u_4_8"
                   .", v_1, v_2, v_3, v_4, v_5, v_6, v_7, v_8, v_1_5, v_2_6, v_3_7, v_4_8"
                   .", stuv_1, stuv_2, stuv_3, stuv_4, stuv_5, stuv_6, stuv_7, stuv_8, stuv_1_5, stuv_2_6, stuv_3_7, stuv_4_8"
                   .", w_1, w_2, w_3, w_4, w_5, w_6, w_7, w_8, w_1_5, w_2_6, w_3_7, w_4_8"
                   .", x_1, x_2, x_3, x_4, x_5, x_6, x_7, x_8, x_1_5, x_2_6, x_3_7, x_4_8"
                   .", y_1, y_2, y_3, y_4, y_5, y_6, y_7, y_8, y_1_5, y_2_6, y_3_7, y_4_8"
                   .", z_1, z_2, z_3, z_4, z_5, z_6, z_7, z_8, z_1_5, z_2_6, z_3_7, z_4_8"
                   .", ab_1, ab_2, ab_3, ab_4, ab_5, ab_6, ab_7, ab_8, ab_1_5, ab_2_6, ab_3_7, ab_4_8"
                   .", ac_1, ac_2, ac_3, ac_4, ac_5, ac_6, ac_7, ac_8, ac_1_5, ac_2_6, ac_3_7, ac_4_8"
                   .", kana_ta_1, kana_chi_1, kana_te_1, kana_to_1, kana_ta_2, kana_chi_2, kana_te_2, kana_to_2, kana_ta_1_2, kana_chi_1_2, kana_te_1_2, kana_to_1_2"
                   .", total_c_1, total_c_2, total_c_3, total_c_4, total_c_5, total_c_6, total_c_7, total_c_8, total_c_1_5, total_c_2_6, total_c_3_7, total_c_4_8"
                   .", total_abc_1, total_abc_2, total_abc_3, total_abc_4, total_abc_5, total_abc_6, total_abc_7, total_abc_8, total_abc_1_5, total_abc_2_6, total_abc_3_7, total_abc_4_8"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                       ." FROM jnl_reward_synthesis_dental_main"
                      ." WHERE del_flg = 'f' AND jnl_status < 2";

        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = %d", $facilityId); }
        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }

        $sql .=       ") jrsdm"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_factor"
                                .", ent1_account   AS s_1,   ent1_days   AS s_2,   ent1_points   AS s_3,   ent1_money   AS s_4"
                                .", visit1_account AS s_5,   visit1_days AS s_6,   visit1_points AS s_7,   visit1_money AS s_8"
                                .", total1_account AS s_1_5, total1_days AS s_2_6, total1_points AS s_3_7, total1_money AS s_4_8"

                                .", ent2_account   AS t_1,   ent2_days   AS t_2,   ent2_points   AS t_3,   ent2_money   AS t_4"
                                .", visit2_account AS t_5,   visit2_days AS t_6,   visit2_points AS t_7,   visit2_money AS t_8"
                                .", total2_account AS t_1_5, total2_days AS t_2_6, total2_points AS t_3_7, total2_money AS t_4_8"

                                .", ent3_account   AS u_1,   ent3_days   AS u_2,   ent3_points   AS u_3,   ent3_money   AS u_4"
                                .", visit3_account AS u_5,   visit3_days AS u_6,   visit3_points AS u_7,   visit3_money AS u_8"
                                .", total3_account AS u_1_5, total3_days AS u_2_6, total3_points AS u_3_7, total3_money AS u_4_8"

                                .", ent4_account   AS v_1,   ent4_days   AS v_2,   ent4_points   AS v_3,   ent4_money   AS v_4"
                                .", visit4_account AS v_5,   visit4_days AS v_6,   visit4_points AS v_7,   visit4_money AS v_8"
                                .", total4_account AS v_1_5, total4_days AS v_2_6, total4_points AS v_3_7, total4_money AS v_4_8"

                                .", ent5_account   AS stuv_1,   ent5_days   AS stuv_2,   ent5_points   AS stuv_3,   ent5_money   AS stuv_4"
                                .", visit5_account AS stuv_5,   visit5_days AS stuv_6,   visit5_points AS stuv_7,   visit5_money AS stuv_8"
                                .", total5_account AS stuv_1_5, total5_days AS stuv_2_6, total5_points AS stuv_3_7, total5_money AS stuv_4_8"
                            ." FROM jnl_reward_synthesis_dental jrsd20"
                           ." WHERE type_section = 2 AND type_factor = 0 AND del_flg = 'f'"
                          .") jrsd20"
                      ." ON jrsdm.newest_apply_id = jrsd20.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_factor"
                                .", ent1_account   AS w_1,   ent1_days   AS w_2,   ent1_points   AS w_3,   ent1_money   AS w_4"
                                .", visit1_account AS w_5,   visit1_days AS w_6,   visit1_points AS w_7,   visit1_money AS w_8"
                                .", total1_account AS w_1_5, total1_days AS w_2_6, total1_points AS w_3_7, total1_money AS w_4_8"

                                .", ent2_account   AS x_1,   ent2_days   AS x_2,   ent2_points   AS x_3,   ent2_money   AS x_4"
                                .", visit2_account AS x_5,   visit2_days AS x_6,   visit2_points AS x_7,   visit2_money AS x_8"
                                .", total2_account AS x_1_5, total2_days AS x_2_6, total2_points AS x_3_7, total2_money AS x_4_8"

                                .", ent3_account   AS y_1,   ent3_days   AS y_2,   ent3_points   AS y_3,   ent3_money AS y_4"
                                .", visit3_account AS y_5,   visit3_days AS y_6,   visit3_points AS y_7,   visit3_money AS y_8"
                                .", total3_account AS y_1_5, total3_days AS y_2_6, total3_points AS y_3_7, total3_money AS y_4_8"

                                .", ent4_account   AS z_1,   ent4_days   AS z_2,   ent4_points   AS z_3,   ent4_money AS z_4"
                                .", visit4_account AS z_5,   visit4_days AS z_6,   visit4_points AS z_7,   visit4_money AS z_8"
                                .", total4_account AS z_1_5, total4_days AS z_2_6, total4_points AS z_3_7, total4_money AS z_4_8"

                                .", ent5_account   AS ab_1,   ent5_days   AS ab_2,   ent5_points   AS ab_3,   ent5_money   AS ab_4"
                                .", visit5_account AS ab_5,   visit5_days AS ab_6,   visit5_points AS ab_7,   visit5_money AS ab_8"
                                .", total5_account AS ab_1_5, total5_days AS ab_2_6, total5_points AS ab_3_7, total5_money AS ab_4_8"

                                .", ent6_account   AS ac_1,   ent6_days   AS ac_2,   ent6_points   AS ac_3,   ent6_money   AS ac_4"
                                .", visit6_account AS ac_5,   visit6_days AS ac_6,   visit6_points AS ac_7,   visit6_money AS ac_8"
                                .", total6_account AS ac_1_5, total6_days AS ac_2_6, total6_points AS ac_3_7, total6_money AS ac_4_8"
                            ." FROM jnl_reward_synthesis_dental jrsd21"
                           ." WHERE type_section = 2 AND type_factor = 1 AND del_flg = 'f'"
                          .") jrsd21"
                      ." ON jrsdm.newest_apply_id = jrsd21.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_factor"
                                .", ent1_account   AS kana_ta_1,   ent1_days   AS kana_chi_1,   ent1_points   AS kana_te_1,   ent1_money   AS kana_to_1"
                                .", visit1_account AS kana_ta_2,   visit1_days AS kana_chi_2,   visit1_points AS kana_te_2,   visit1_money AS kana_to_2"
                                .", total1_account AS kana_ta_1_2, total1_days AS kana_chi_1_2, total1_points AS kana_te_1_2, total1_money AS kana_to_1_2"

                                .", ent2_account   AS total_c_1,   ent2_days   AS total_c_2,   ent2_points   AS total_c_3,   ent2_money   AS total_c_4"
                                .", visit2_account AS total_c_5,   visit2_days AS total_c_6,   visit2_points AS total_c_7,   visit2_money AS total_c_8"
                                .", total2_account AS total_c_1_5, total2_days AS total_c_2_6, total2_points AS total_c_3_7, total2_money AS total_c_4_8"

                                .", ent3_account   AS total_abc_1,   ent3_days   AS total_abc_2,   ent3_points   AS total_abc_3,   ent3_money   AS total_abc_4"
                                .", visit3_account AS total_abc_5,   visit3_days AS total_abc_6,   visit3_points AS total_abc_7,   visit3_money AS total_abc_8"
                                .", total3_account AS total_abc_1_5, total3_days AS total_abc_2_6, total3_points AS total_abc_3_7, total3_money AS total_abc_4_8"
                            ." FROM jnl_reward_synthesis_dental jrsd22"
                           ." WHERE type_section = 2 AND type_factor = 2 AND del_flg = 'f'"
                          .") jrsd22"
                      ." ON jrsdm.newest_apply_id = jrsd22.newest_apply_id ";
        return $this->getDataList($sql);
    }

    function getJnlRewardSynthesisDental_type_three($arr)
    {
        $facilityId = $this->getArrayValueByKeyForEmptyIsNull('facility_id', $arr);
        $year       = $this->getArrayValueByKeyForEmptyIsNull('year',        $arr);
        $month      = $this->getArrayValueByKeyForEmptyIsNull('month',       $arr);

        $sql = "SELECT jrsdm.newest_apply_id, jrsdm.emp_id, jrsdm.jnl_facility_id, jrsdm.jnl_status, jrsdm.regist_date, jrsdm.del_flg"
                   .", SUBSTR(jrsdm.regist_date, 0, 5) AS year, SUBSTR(jrsdm.regist_date, 5, 2) AS month"
                   .", claim_this_mon_1, claim_this_mon_2,  claim_this_mon_3,  claim_this_mon_4"
                   .", claim_this_mon_5, claim_this_mon_6,  claim_this_mon_7,  claim_this_mon_8"
                   .", claim_this_mon_9, claim_this_mon_10, claim_this_mon_11, claim_this_mon_12"

                   .", claim_prev_year_1, claim_prev_year_2,  claim_prev_year_3,  claim_prev_year_4"
                   .", claim_prev_year_5, claim_prev_year_6,  claim_prev_year_7,  claim_prev_year_8"
                   .", claim_prev_year_9, claim_prev_year_10, claim_prev_year_11, claim_prev_year_12"

                   .", difference_1, difference_2,  difference_3,  difference_4"
                   .", difference_5, difference_6,  difference_7,  difference_8"
                   .", difference_9, difference_10, difference_11, difference_12"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                       ." FROM jnl_reward_synthesis_dental_main"
                      ." WHERE del_flg = 'f' AND jnl_status < 2";

        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = %d", $facilityId); }
        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }

        $sql .=      ") jrsdm".
                     " INNER JOIN (SELECT newest_apply_id, type_section, type_factor"
                                      .", ent1_account   AS claim_this_mon_1, ent1_days   AS claim_this_mon_2,  ent1_points   AS claim_this_mon_3,  ent1_money   AS claim_this_mon_4"
                                      .", visit1_account AS claim_this_mon_5, visit1_days AS claim_this_mon_6,  visit1_points AS claim_this_mon_7,  visit1_money AS claim_this_mon_8"
                                      .", total1_account AS claim_this_mon_9, total1_days AS claim_this_mon_10, total1_points AS claim_this_mon_11, total1_money AS claim_this_mon_12"

                                      .", ent2_account   AS claim_prev_year_1, ent2_days   AS claim_prev_year_2,  ent2_points   AS claim_prev_year_3,  ent2_money   AS claim_prev_year_4"
                                      .", visit2_account AS claim_prev_year_5, visit2_days AS claim_prev_year_6,  visit2_points AS claim_prev_year_7,  visit2_money AS claim_prev_year_8"
                                      .", total2_account AS claim_prev_year_9, total2_days AS claim_prev_year_10, total2_points AS claim_prev_year_11, total2_money AS claim_prev_year_12"

                                      .", ent3_account   AS difference_1, ent3_days   AS difference_2,  ent3_points   AS difference_3,  ent3_money   AS difference_4"
                                      .", visit3_account AS difference_5, visit3_days AS difference_6,  visit3_points AS difference_7,  visit3_money AS difference_8"
                                      .", total3_account AS difference_9, total3_days AS difference_10, total3_points AS difference_11, total3_money AS difference_12"
                                  ." FROM jnl_reward_synthesis_dental jrsd32"
                                 ." WHERE type_section = 3 AND type_factor = 2 AND del_flg = 'f'"
                                .") jrsd32"
                            ." ON jrsdm.newest_apply_id = jrsd32.newest_apply_id ";
        return $this->getDataList($sql);
    }
}

class JnlBillRecuperation extends IndicatorData
{
    function JnlBillRecuperation($arr)
    {
        parent::IndicatorData($arr);
    }

    function getJnlBillRecuperation($arr)
    {
        $rtnArr      = array();
        $insureRehab = $this->getJnlBillRecuperationInsureRehab($arr);
        $service     = $this->getJnlBillRecuperationService($arr);
        $ownPayment  = $this->getJnlBillRecuperationOwnPayment($arr);
        $etc         = $this->getJnlBillRecuperationEtc($arr);

        $rtnArr = $this->arrayMerge($insureRehab, $rtnArr);
        $rtnArr = $this->arrayMerge($service,     $rtnArr);
        $rtnArr = $this->arrayMerge($ownPayment,  $rtnArr);
        $rtnArr = $this->arrayMerge($etc,         $rtnArr);

        return $rtnArr;
    }

    function getJnlBillRecuperationInsureRehab($arr)
    {
        $rtnArr = array();
        $type0  = $this->getJnlBillRecuperationInsureRehabType0($arr);
        $type1  = $this->getJnlBillRecuperationInsureRehabType1($arr);

        $rtnArr = $this->arrayMerge($type0, $rtnArr);
        $rtnArr = $this->arrayMerge($type1, $rtnArr);

        return $rtnArr;
    }

    function getJnlBillRecuperationInsureRehabType0($arr)
    {
        $rtnArr       = array();
        $presentMonth = $this->getJnlBillRecuperationInsureRehabPresentMonth($arr);
        $monthsDelay  = $this->getJnlBillRecuperationInsureRehabMonthsDelay($arr);
        $reclaim      = $this->getJnlBillRecuperationInsureRehabReclaim($arr);
        $total        = $this->getJnlBillRecuperationInsureRehabTotal($arr);

        $rtnArr = $this->arrayMerge($presentMonth, $rtnArr);
        $rtnArr = $this->arrayMerge($monthsDelay,  $rtnArr);
        $rtnArr = $this->arrayMerge($reclaim,      $rtnArr);
        $rtnArr = $this->arrayMerge($total,        $rtnArr);

        return $rtnArr;
    }

    function getJnlBillRecuperationInsureRehabType1($arr)
    {
        $facilityId = $this->getArrValueByKey('facility_id', $arr);
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);

        $sql = "SELECT jbrm.newest_apply_id, jbrm.emp_id, jbrm.jnl_facility_id, jbrm.jnl_status, jbrm.regist_date, jbrm.del_flg"
                   .", jbrm.year, jbrm.month"
                   .", orange_472, orange_473, orange_474, orange_475, orange_476, orange_477, orange_478, orange_479"
                   .", orange_480, orange_481, orange_482, orange_483, orange_484, orange_485, orange_486, orange_487, orange_488, orange_489"
                   .", orange_490, orange_491, orange_492, orange_493, orange_494, orange_495, orange_496, orange_497, orange_498, orange_499"
                   .", orange_500, orange_501, orange_502, orange_503, orange_504, orange_505, orange_506, orange_507, orange_508, orange_509"
                   .", orange_510, orange_511, orange_512, orange_513, orange_514, orange_515, orange_516, orange_517, orange_518, orange_519"
                   .", orange_520, orange_521, orange_522, orange_523, orange_524, orange_525, orange_526, orange_527, orange_528, orange_529"
                   .", orange_530, orange_531, orange_532, orange_533, orange_534, orange_535, orange_536, orange_537, orange_538, orange_539"
                   .", orange_540, orange_541, orange_542, orange_543, orange_544, orange_545, orange_546, orange_547, orange_548, orange_549"
                   .", orange_550, orange_551, orange_552, orange_553, orange_554, orange_555, orange_556, orange_557, orange_558, orange_559"
                   .", orange_560, orange_561, orange_562, orange_563, orange_564, orange_565, orange_566, orange_567, orange_568, orange_569"
                   .", orange_570"
                   .", red_327, red_328, red_329"
                   .", red_330, red_331, red_332, red_333, red_334, red_335, red_336, red_337, red_338, red_339"
                   .", red_340, red_341, red_342, red_343, red_344, red_345, red_346, red_347, red_348, red_349"
                   .", red_350, red_351, red_352, red_353, red_354, red_355, red_356, red_357, red_358, red_359"
                   .", green_65, green_66, green_67, green_68, green_69"
                   .", green_70, green_71, green_72, green_73, green_74, green_75, green_76, green_77, green_78, green_79"
                   .", green_80"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                           .", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_bill_recuperation_main jbrm"
                      ." WHERE jnl_status < 2";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'",             $facilityId); }

        $sql .=       ") jbrm"
              ." INNER JOIN (SELECT newest_apply_id, type_section, total_account AS orange_472, total_days AS orange_473"
                                .", total_price AS orange_474, total_ins_charge AS orange_475, total_pblc_charge AS orange_476"
                                .", total_own_charge AS orange_477, total_cost AS orange_478, one_account AS orange_479"
                                .", one_days AS orange_480, one_price AS orange_481, one_pblc_charge AS orange_482"
                                .", one_own_charge AS orange_483, one_cost AS orange_484, two_account AS orange_485"
                                .", two_days AS orange_486, two_price AS orange_487, two_pblc_charge AS orange_488"
                                .", two_own_charge AS orange_489, two_cost AS orange_490, empty_one_one AS green_65"
                                .", empty_one_two AS green_66, three_account AS orange_491, three_days AS orange_492"
                                .", three_price AS orange_493, three_ins_charge AS orange_494, three_pblc_charge AS orange_495"
                                .", three_own_charge AS orange_496, three_cost AS orange_497, empty_two_one AS green_67"
                                .", empty_two_two AS green_68, four_account AS orange_498, four_days AS orange_499"
                                .", four_price AS orange_500, four_ins_charge AS orange_501, four_pblc_charge AS orange_502"
                                .", four_own_charge AS orange_503, four_cost AS orange_504, del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab"
                           ." WHERE del_flg = 'f' AND type_section = 16"
                          .") jbrir16"
                      ." ON jbrm.newest_apply_id = jbrir16.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, total_account AS orange_505, total_days AS orange_506"
                                .", total_price AS orange_507, total_ins_charge AS orange_508, total_pblc_charge AS orange_509"
                                .", total_own_charge AS orange_510, total_cost AS orange_511, one_account AS orange_512"
                                .", one_days AS orange_513, one_price AS orange_514, one_pblc_charge AS orange_515"
                                .", one_own_charge AS orange_516, one_cost AS orange_517, two_account AS orange_518"
                                .", two_days AS orange_519, two_price AS orange_520, two_pblc_charge AS orange_521"
                                .", two_own_charge AS orange_522, two_cost AS orange_523, empty_one_one AS green_69"
                                .", empty_one_two AS green_70, three_account AS orange_524, three_days AS orange_525"
                                .", three_price AS orange_526, three_ins_charge AS orange_527, three_pblc_charge AS orange_528"
                                .", three_own_charge AS orange_529, three_cost AS orange_530, empty_two_one AS green_71"
                                .", empty_two_two AS green_72, four_account AS orange_531, four_days AS orange_532"
                                .", four_price AS orange_533, four_ins_charge AS orange_534, four_pblc_charge AS orange_535"
                                .", four_own_charge AS orange_536, four_cost AS orange_537, del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab"
                           ." WHERE del_flg = 'f' AND type_section = 17"
                          .") jbrir17"
                      ." ON jbrm.newest_apply_id = jbrir17.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, total_account AS orange_538, total_days AS orange_539"
                                .", total_price AS orange_540, total_ins_charge AS orange_541, total_pblc_charge AS orange_542"
                                .", total_own_charge AS orange_543, total_cost AS orange_544, one_account AS orange_545"
                                .", one_days AS orange_546, one_price AS orange_547, one_pblc_charge AS orange_548"
                                .", one_own_charge AS orange_549, one_cost AS orange_550, two_account AS orange_551"
                                .", two_days AS orange_552, two_price AS orange_553, two_pblc_charge AS orange_554"
                                .", two_own_charge AS orange_555, two_cost AS orange_556, empty_one_one AS green_73"
                                .", empty_one_two AS green_74, three_account AS orange_557, three_days AS orange_558"
                                .", three_price AS orange_559, three_ins_charge AS orange_560, three_pblc_charge AS orange_561"
                                .", three_own_charge AS orange_562, three_cost AS orange_563, empty_two_one AS green_75"
                                .", empty_two_two AS green_76, four_account AS orange_564, four_days AS orange_565"
                                .", four_price AS orange_566, four_ins_charge AS orange_567, four_pblc_charge AS orange_568"
                                .", four_own_charge AS orange_569, four_cost AS orange_570, del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab"
                           ." WHERE del_flg = 'f' AND type_section = 18"
                          .") jbrir18"
                      ." ON jbrm.newest_apply_id = jbrir18.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, total_account AS red_327, total_days AS red_328, total_price AS red_329"
                                .", total_ins_charge AS red_330, total_pblc_charge AS red_331, total_own_charge AS red_332"
                                .", total_cost AS red_333, one_account AS red_334, one_days AS red_335, one_price AS red_336"
                                .", one_pblc_charge AS red_337, one_own_charge AS red_338, one_cost AS red_339, two_account AS red_340"
                                .", two_days AS red_341, two_price AS red_342, two_pblc_charge AS red_343, two_own_charge AS red_344"
                                .", two_cost AS red_345, empty_one_one AS green_77, empty_one_two AS green_78, three_account AS red_346"
                                .", three_days AS red_347, three_price AS red_348, three_ins_charge AS red_349"
                                .", three_pblc_charge AS red_350, three_own_charge AS red_351, three_cost AS red_352"
                                .", empty_two_one AS green_79, empty_two_two AS green_80, four_account AS red_353, four_days AS red_354"
                                .", four_price AS red_355, four_ins_charge AS red_356, four_pblc_charge AS red_357"
                                .", four_own_charge AS red_358, four_cost AS red_359, del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab"
                           ." WHERE del_flg = 'f' AND type_section = 19"
                          .") jbrir19"
                      ." ON jbrm.newest_apply_id = jbrir19.newest_apply_id";
        return $this->getDataList($sql);
    }

    function getJnlBillRecuperationInsureRehabPresentMonth($arr)
    {
        $facilityId = $this->getArrValueByKey('facility_id', $arr);
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);

        $sql = "SELECT jbrm.newest_apply_id, jbrm.emp_id, jbrm.jnl_facility_id, jbrm.jnl_status, jbrm.regist_date, jbrm.del_flg"
                   .", jbrm.year, jbrm.month"
                   .", orange_1, orange_2, orange_3, orange_4, orange_5, orange_6, orange_7, orange_8, orange_9"
                   .", orange_10, orange_11, orange_12, orange_13, orange_14, orange_15, orange_16, orange_17, orange_18, orange_19"
                   .", orange_20, orange_21, orange_22, orange_23, orange_24, orange_25, orange_26, orange_27, orange_28, orange_29"
                   .", orange_30, orange_31, orange_32, orange_33, orange_34, orange_35, orange_36, orange_37, orange_38, orange_39"
                   .", orange_40, orange_41, orange_42, orange_43, orange_44, orange_45, orange_46, orange_47, orange_48, orange_49"
                   .", orange_50, orange_51, orange_52, orange_53, orange_54, orange_55, orange_56, orange_57, orange_58, orange_59"
                   .", orange_60, orange_61, orange_62, orange_63, orange_64, orange_65, orange_66, orange_67, orange_68, orange_69"
                   .", orange_70, orange_71, orange_72, orange_73, orange_74, orange_75, orange_76, orange_77, orange_78, orange_79"
                   .", orange_80, orange_81, orange_82, orange_83, orange_84, orange_85, orange_86, orange_87, orange_88, orange_89"
                   .", orange_90, orange_91, orange_92, orange_93"
                   .", red_3, red_4, red_5, red_6, red_7, red_8, red_9"
                   .", red_10, red_11, red_12, red_13, red_14, red_15, red_16, red_17, red_18, red_19"
                   .", red_20, red_21, red_22, red_23, red_24, red_25, red_26, red_27, red_28, red_29"
                   .", red_30, red_31, red_32, red_33"
                   .", green_1, green_2, green_3, green_4, green_5, green_6, green_7, green_8, green_9"
                   .", green_10, green_11, green_12, green_13, green_14, green_15, green_16"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                           .", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_bill_recuperation_main jbrm"
                      ." WHERE jnl_status < 2";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'",             $facilityId); }

        $sql .=       ") jbrm"
              ." INNER JOIN (SELECT newest_apply_id, type_section, total_account AS orange_1, total_days AS orange_2"
                                .", total_price AS orange_3, total_ins_charge AS orange_4, total_pblc_charge AS orange_5"
                                .", total_own_charge AS orange_6, total_cost AS orange_7, one_account AS orange_8"
                                .", one_days AS orange_9, one_price AS orange_10, one_pblc_charge AS orange_11"
                                .", one_own_charge AS orange_12, one_cost AS orange_13, two_account AS orange_14"
                                .", two_days AS orange_15, two_price AS orange_16, two_pblc_charge AS orange_17"
                                .", two_own_charge AS orange_18, two_cost AS orange_19, empty_one_one AS green_1"
                                .", empty_one_two AS green_2, three_account AS orange_20, three_days AS orange_21"
                                .", three_price AS orange_22, three_pblc_charge AS orange_23, three_own_charge AS orange_24"
                                .", three_cost AS orange_25, empty_two_one AS green_3, empty_two_two AS green_4"
                                .", four_account AS orange_26, four_days AS orange_27, four_price AS orange_28"
                                .", four_pblc_charge AS orange_29, four_own_charge AS orange_30, four_cost AS orange_31, del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab"
                           ." WHERE del_flg = 'f' AND type_section = 0"
                          .") jbrir00"
                      ." ON jbrm.newest_apply_id = jbrir00.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, total_account AS orange_32, total_days AS orange_33"
                                .", total_price AS orange_34, total_ins_charge AS orange_35, total_pblc_charge AS orange_36"
                                .", total_own_charge AS orange_37, total_cost AS orange_38, one_account AS orange_39"
                                .", one_days AS orange_40, one_price AS orange_41, one_pblc_charge AS orange_42"
                                .", one_own_charge AS orange_43, one_cost AS orange_44, two_account AS orange_45"
                                .", two_days AS orange_46, two_price AS orange_47, two_pblc_charge AS orange_48"
                                .", two_own_charge AS orange_49, two_cost AS orange_50, empty_one_one AS green_5"
                                .", empty_one_two AS green_6, three_account AS orange_51, three_days AS orange_52"
                                .", three_price AS orange_53, three_pblc_charge AS orange_54, three_own_charge AS orange_55"
                                .", three_cost AS orange_56, empty_two_one AS green_7, empty_two_two AS green_8"
                                .", four_account AS orange_57, four_days AS orange_58, four_price AS orange_59"
                                .", four_pblc_charge AS orange_60, four_own_charge AS orange_61, four_cost AS orange_62, del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab"
                           ." WHERE del_flg = 'f' AND type_section = 1"
                          .") jbrir01"
                      ." ON jbrm.newest_apply_id = jbrir01.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, total_account AS orange_63, total_days AS orange_64"
                                .", total_price AS orange_65, total_ins_charge AS orange_66, total_pblc_charge AS orange_67"
                                .", total_own_charge AS orange_68, total_cost AS orange_69, one_account AS orange_70"
                                .", one_days AS orange_71, one_price AS orange_72, one_pblc_charge AS orange_73"
                                .", one_own_charge AS orange_74, one_cost AS orange_75, two_account AS orange_76"
                                .", two_days AS orange_77, two_price AS orange_78, two_pblc_charge AS orange_79"
                                .", two_own_charge AS orange_80, two_cost AS orange_81, empty_one_one AS green_9"
                                .", empty_one_two AS green_10, three_account AS orange_82, three_days AS orange_83"
                                .", three_price AS orange_84, three_pblc_charge AS orange_85, three_own_charge AS orange_86"
                                .", three_cost AS orange_87, empty_two_one AS green_11, empty_two_two AS green_12"
                                .", four_account AS orange_88, four_days AS orange_89, four_price AS orange_90"
                                .", four_pblc_charge AS orange_91, four_own_charge AS orange_92, four_cost AS orange_93, del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab"
                           ." WHERE del_flg = 'f' AND type_section = 2"
                          .") jbrir02"
                      ." ON jbrm.newest_apply_id = jbrir02.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, total_account AS red_3, total_days AS red_4, total_price AS red_5"
                                .", total_ins_charge AS red_6, total_pblc_charge AS red_7, total_own_charge AS red_8, total_cost AS red_9"
                                .", one_account AS red_10, one_days AS red_11, one_price AS red_12, one_pblc_charge AS red_13"
                                .", one_own_charge AS red_14, one_cost AS red_15, two_account AS red_16, two_days AS red_17"
                                .", two_price AS red_18, two_pblc_charge AS red_19, two_own_charge AS red_20, two_cost AS red_21"
                                .", empty_one_one AS green_13, empty_one_two AS green_14, three_account AS red_22, three_days AS red_23"
                                .", three_price AS red_24, three_pblc_charge AS red_25, three_own_charge AS red_26, three_cost AS red_27"
                                .", empty_two_one AS green_15, empty_two_two AS green_16, four_account AS red_28, four_days AS red_29"
                                .", four_price AS red_30, four_pblc_charge AS red_31, four_own_charge AS red_32, four_cost AS red_33"
                                .", del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab"
                           ." WHERE del_flg = 'f' AND type_section = 3"
                          .") jbrir03"
                      ." ON jbrm.newest_apply_id = jbrir03.newest_apply_id ";
        return $this->getDataList($sql);
    }

    function getJnlBillRecuperationInsureRehabMonthsDelay($arr)
    {
        $facilityId = $this->getArrValueByKey('facility_id', $arr);
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);

        $sql = "SELECT jbrm.newest_apply_id, jbrm.emp_id, jbrm.jnl_facility_id, jbrm.jnl_status, jbrm.regist_date, jbrm.del_flg, jbrm.year"
                   .", jbrm.month"
                   .", orange_94, orange_95, orange_96, orange_97, orange_98, orange_99"
                   .", orange_100, orange_101, orange_102, orange_103, orange_104, orange_105, orange_106, orange_107, orange_108, orange_109"
                   .", orange_110, orange_111, orange_112, orange_113, orange_114, orange_115, orange_116, orange_117, orange_118, orange_119"
                   .", orange_120, orange_121, orange_122, orange_123, orange_124, orange_125, orange_126, orange_127, orange_128, orange_129"
                   .", orange_130, orange_131, orange_132, orange_133, orange_134, orange_135, orange_136, orange_137, orange_138, orange_139"
                   .", orange_140, orange_141, orange_142, orange_143, orange_144, orange_145, orange_146, orange_147, orange_148, orange_149"
                   .", orange_150, orange_151, orange_152, orange_153, orange_154, orange_155, orange_156, orange_157, orange_158, orange_159"
                   .", orange_160, orange_161, orange_162, orange_163, orange_164, orange_165, orange_166, orange_167, orange_168, orange_169"
                   .", orange_170, orange_171, orange_172, orange_173, orange_174, orange_175, orange_176, orange_177, orange_178, orange_179"
                   .", orange_180, orange_181, orange_182, orange_183, orange_184, orange_185, orange_186"
                   .", red_34, red_35, red_36, red_37, red_38, red_39"
                   .", red_40, red_41, red_42, red_43, red_44, red_45, red_46, red_47, red_48, red_49"
                   .", red_50, red_51, red_52, red_53, red_54, red_55, red_56, red_57, red_58, red_59"
                   .", red_60, red_61, red_62, red_63, red_64"
                   .", green_17, green_18, green_19"
                   .", green_20, green_21, green_22, green_23, green_24, green_25, green_26, green_27, green_28, green_29"
                   .", green_30, green_31, green_32"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                           .", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_bill_recuperation_main jbrm"
                      ." WHERE jnl_status < 2";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'",             $facilityId); }

        $sql .=       ") jbrm"
              ." INNER JOIN (SELECT newest_apply_id, type_section, total_account AS orange_94, total_days AS orange_95"
                                .", total_price AS orange_96, total_ins_charge AS orange_97, total_pblc_charge AS orange_98"
                                .", total_own_charge AS orange_99, total_cost AS orange_100, one_account AS orange_101"
                                .", one_days AS orange_102, one_price AS orange_103, one_pblc_charge AS orange_104"
                                .", one_own_charge AS orange_105, one_cost AS orange_106, two_account AS orange_107"
                                .", two_days AS orange_108, two_price AS orange_109, two_pblc_charge AS orange_110"
                                .", two_own_charge AS orange_111, two_cost AS orange_112, empty_one_one AS green_17"
                                .", empty_one_two AS green_18, three_account AS orange_113, three_days AS orange_114"
                                .", three_price AS orange_115, three_pblc_charge AS orange_116, three_own_charge AS orange_117"
                                .", three_cost AS orange_118, empty_two_one AS green_19, empty_two_two AS green_20"
                                .", four_account AS orange_119, four_days AS orange_120, four_price AS orange_121"
                                .", four_pblc_charge AS orange_122, four_own_charge AS orange_123, four_cost AS orange_124, del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab"
                           ." WHERE del_flg = 'f' AND type_section = 4"
                          .") jbrir04"
                      ." ON jbrm.newest_apply_id = jbrir04.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, total_account AS orange_125, total_days AS orange_126"
                                .", total_price AS orange_127, total_ins_charge AS orange_128, total_pblc_charge AS orange_129"
                                .", total_own_charge AS orange_130, total_cost AS orange_131, one_account AS orange_132"
                                .", one_days AS orange_133, one_price AS orange_134, one_pblc_charge AS orange_135"
                                .", one_own_charge AS orange_136, one_cost AS orange_137, two_account AS orange_138"
                                .", two_days AS orange_139, two_price AS orange_140, two_pblc_charge AS orange_141"
                                .", two_own_charge AS orange_142, two_cost AS orange_143, empty_one_one AS green_21"
                                .", empty_one_two AS green_22, three_account AS orange_144, three_days AS orange_145"
                                .", three_price AS orange_146, three_pblc_charge AS orange_147, three_own_charge AS orange_148"
                                .", three_cost AS orange_149, empty_two_one AS green_23, empty_two_two AS green_24"
                                .", four_account AS orange_150, four_days AS orange_151, four_price AS orange_152"
                                .", four_pblc_charge AS orange_153, four_own_charge AS orange_154, four_cost AS orange_155, del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab"
                           ." WHERE del_flg = 'f' AND type_section = 5"
                          .") jbrir05"
                      ." ON jbrm.newest_apply_id = jbrir05.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, total_account AS orange_156, total_days AS orange_157"
                                .", total_price AS orange_158, total_ins_charge AS orange_159, total_pblc_charge AS orange_160"
                                .", total_own_charge AS orange_161, total_cost AS orange_162, one_account AS orange_163"
                                .", one_days AS orange_164, one_price AS orange_165, one_pblc_charge AS orange_166"
                                .", one_own_charge AS orange_167, one_cost AS orange_168, two_account AS orange_169"
                                .", two_days AS orange_170, two_price AS orange_171, two_pblc_charge AS orange_172"
                                .", two_own_charge AS orange_173, two_cost AS orange_174, empty_one_one AS green_25"
                                .", empty_one_two AS green_26, three_account AS orange_175, three_days AS orange_176"
                                .", three_price AS orange_177, three_pblc_charge AS orange_178, three_own_charge AS orange_179"
                                .", three_cost AS orange_180, empty_two_one AS green_27, empty_two_two AS green_28"
                                .", four_account AS orange_181, four_days AS orange_182, four_price AS orange_183"
                                .", four_pblc_charge AS orange_184, four_own_charge AS orange_185, four_cost AS orange_186, del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab"
                           ." WHERE del_flg = 'f' AND type_section = 6"
                          .") jbrir06"
                      ." ON jbrm.newest_apply_id = jbrir06.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, total_account AS red_34, total_days AS red_35, total_price AS red_36"
                                .", total_ins_charge AS red_37, total_pblc_charge AS red_38, total_own_charge AS red_39"
                                .", total_cost AS red_40, one_account AS red_41, one_days AS red_42, one_price AS red_43"
                                .", one_pblc_charge AS red_44, one_own_charge AS red_45, one_cost AS red_46, two_account AS red_47"
                                .", two_days AS red_48, two_price AS red_49, two_pblc_charge AS red_50, two_own_charge AS red_51"
                                .", two_cost AS red_52, empty_one_one AS green_29, empty_one_two AS green_30, three_account AS red_53"
                                .", three_days AS red_54, three_price AS red_55, three_pblc_charge AS red_56, three_own_charge AS red_57"
                                .", three_cost AS red_58, empty_two_one AS green_31, empty_two_two AS green_32, four_account AS red_59"
                                .", four_days AS red_60, four_price AS red_61, four_pblc_charge AS red_62, four_own_charge AS red_63"
                                .", four_cost AS red_64, del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab"
                           ." WHERE del_flg = 'f' AND type_section = 7"
                          .") jbrir07"
                      ." ON jbrm.newest_apply_id = jbrir07.newest_apply_id ";
        return $this->getDataList($sql);
    }

    function getJnlBillRecuperationInsureRehabReclaim($arr)
    {
        $facilityId = $this->getArrValueByKey('facility_id', $arr);
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);

        $sql = "SELECT jbrm.newest_apply_id, jbrm.emp_id, jbrm.jnl_facility_id, jbrm.jnl_status, jbrm.regist_date, jbrm.del_flg, jbrm.year"
                   .", jbrm.month"
                   .", orange_187, orange_188, orange_189"
                   .", orange_190, orange_191, orange_192, orange_193, orange_194, orange_195, orange_196, orange_197, orange_198, orange_199"
                   .", orange_200, orange_201, orange_202, orange_203, orange_204, orange_205, orange_206, orange_207, orange_208, orange_209"
                   .", orange_210, orange_211, orange_212, orange_213, orange_214, orange_215, orange_216, orange_217, orange_218, orange_219"
                   .", orange_220, orange_221, orange_222, orange_223, orange_224, orange_225, orange_226, orange_227, orange_228, orange_229"
                   .", orange_230, orange_231, orange_232, orange_233, orange_234, orange_235, orange_236, orange_237, orange_238, orange_239"
                   .", orange_240, orange_241, orange_242, orange_243, orange_244, orange_245, orange_246, orange_247, orange_248, orange_249"
                   .", orange_250, orange_251, orange_252, orange_253, orange_254, orange_255, orange_256, orange_257, orange_258, orange_259"
                   .", orange_260, orange_261, orange_262, orange_263, orange_264, orange_265, orange_266, orange_267, orange_268, orange_269"
                   .", orange_270, orange_271, orange_272, orange_273, orange_274, orange_275, orange_276, orange_277, orange_278, orange_279"
                   .", red_65, red_66, red_67, red_68, red_69"
                   .", red_70, red_71, red_72, red_73, red_74, red_75, red_76, red_77, red_78, red_79"
                   .", red_80, red_81, red_82, red_83, red_84, red_85, red_86, red_87, red_88, red_89"
                   .", red_90, red_91, red_92, red_93, red_94, red_95"
                   .", green_34, green_35, green_36, green_37, green_38, green_39"
                   .", green_40, green_41, green_42, green_43, green_44, green_45, green_46, green_47, green_48"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                           .", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_bill_recuperation_main jbrm"
                      ." WHERE jnl_status < 2";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'",             $facilityId); }

        $sql .=       ") jbrm"
              ." INNER JOIN (SELECT newest_apply_id, type_section, total_account AS orange_187, total_days AS orange_188"
                                .", total_price AS orange_189, total_ins_charge AS orange_190, total_pblc_charge AS orange_191"
                                .", total_own_charge AS orange_192, total_cost AS orange_193, one_account AS orange_194"
                                .", one_days AS orange_195, one_price AS orange_196, one_pblc_charge AS orange_197"
                                .", one_own_charge AS orange_198, one_cost AS orange_199, two_account AS orange_200"
                                .", two_days AS orange_201, two_price AS orange_202, two_pblc_charge AS orange_203"
                                .", two_own_charge AS orange_204, two_cost AS orange_205, empty_one_one AS green_33"
                                .", empty_one_two AS green_34, three_account AS orange_206, three_days AS orange_207"
                                .", three_price AS orange_208, three_pblc_charge AS orange_209, three_own_charge AS orange_210"
                                .", three_cost AS orange_211, empty_two_one AS green_35, empty_two_two AS green_36"
                                .", four_account AS orange_212, four_days AS orange_213, four_price AS orange_214"
                                .", four_pblc_charge AS orange_215, four_own_charge AS orange_216, four_cost AS orange_217, del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab"
                           ." WHERE del_flg = 'f' AND type_section = 8"
                          .") jbrir08"
                      ." ON jbrm.newest_apply_id = jbrir08.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, total_account AS orange_218, total_days AS orange_219"
                                .", total_price AS orange_220, total_ins_charge AS orange_221, total_pblc_charge AS orange_222"
                                .", total_own_charge AS orange_223, total_cost AS orange_224, one_account AS orange_225"
                                .", one_days AS orange_226, one_price AS orange_227, one_pblc_charge AS orange_228"
                                .", one_own_charge AS orange_229, one_cost AS orange_230, two_account AS orange_231"
                                .", two_days AS orange_232, two_price AS orange_233, two_pblc_charge AS orange_234"
                                .", two_own_charge AS orange_235, two_cost AS orange_236, empty_one_one AS green_37"
                                .", empty_one_two AS green_38, three_account AS orange_237, three_days AS orange_238"
                                .", three_price AS orange_239, three_pblc_charge AS orange_240, three_own_charge AS orange_241"
                                .", three_cost AS orange_242, empty_two_one AS green_39, empty_two_two AS green_40"
                                .", four_account AS orange_243, four_days AS orange_244, four_price AS orange_245"
                                .", four_pblc_charge AS orange_246, four_own_charge AS orange_247, four_cost AS orange_248, del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab"
                           ." WHERE del_flg = 'f' AND type_section = 9"
                          .") jbrir09"
                      ." ON jbrm.newest_apply_id = jbrir09.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, total_account AS orange_249, total_days AS orange_250"
                                .", total_price AS orange_251, total_ins_charge AS orange_252, total_pblc_charge AS orange_253"
                                .", total_own_charge AS orange_254, total_cost AS orange_255, one_account AS orange_256"
                                .", one_days AS orange_257, one_price AS orange_258, one_pblc_charge AS orange_259"
                                .", one_own_charge AS orange_260, one_cost AS orange_261, two_account AS orange_262"
                                .", two_days AS orange_263, two_price AS orange_264, two_pblc_charge AS orange_265"
                                .", two_own_charge AS orange_266, two_cost AS orange_267, empty_one_one AS green_41"
                                .", empty_one_two AS green_42, three_account AS orange_268, three_days AS orange_269"
                                .", three_price AS orange_270, three_pblc_charge AS orange_271, three_own_charge AS orange_272"
                                .", three_cost AS orange_273, empty_two_one AS green_43, empty_two_two AS green_44"
                                .", four_account AS orange_274, four_days AS orange_275, four_price AS orange_276"
                                .", four_pblc_charge AS orange_277, four_own_charge AS orange_278, four_cost AS orange_279, del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab"
                           ." WHERE del_flg = 'f' AND type_section = 10"
                          .") jbrir10"
                      ." ON jbrm.newest_apply_id = jbrir10.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, total_account AS red_65, total_days AS red_66, total_price AS red_67"
                                .", total_ins_charge AS red_68, total_pblc_charge AS red_69, total_own_charge AS red_70"
                                .", total_cost AS red_71, one_account AS red_72, one_days AS red_73, one_price AS red_74"
                                .", one_pblc_charge AS red_75, one_own_charge AS red_76, one_cost AS red_77, two_account AS red_78"
                                .", two_days AS red_79, two_price AS red_80, two_pblc_charge AS red_81, two_own_charge AS red_82"
                                .", two_cost AS red_83, empty_one_one AS green_45, empty_one_two AS green_46, three_account AS red_84"
                                .", three_days AS red_85, three_price AS red_86, three_pblc_charge AS red_87, three_own_charge AS red_88"
                                .", three_cost AS red_89, empty_two_one AS green_47, empty_two_two AS green_48, four_account AS red_90"
                                .", four_days AS red_91, four_price AS red_92, four_pblc_charge AS red_93, four_own_charge AS red_94"
                                .", four_cost AS red_95, del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab"
                           ." WHERE del_flg = 'f' AND type_section = 11"
                          .") jbrir11"
                      ." ON jbrm.newest_apply_id = jbrir11.newest_apply_id ";
        return $this->getDataList($sql);
    }

    function getJnlBillRecuperationInsureRehabTotal($arr)
    {
        $facilityId = $this->getArrValueByKey('facility_id', $arr);
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);

        $sql = "SELECT jbrm.newest_apply_id, jbrm.emp_id, jbrm.jnl_facility_id, jbrm.jnl_status, jbrm.regist_date, jbrm.del_flg, jbrm.year"
                   .", jbrm.month"
                   .", red_96, red_97, red_98, red_99, red_100, red_101, red_102, red_103, red_104, red_105, red_106, red_107, red_108"
                   .", red_109, red_110, red_111, red_112, red_113, red_114, red_115, red_116, red_117, red_118, red_119, red_120, red_121"
                   .", red_122, red_123, red_124, red_125, red_126, red_127, red_128, red_129, red_130, red_131, red_132, red_133, red_134"
                   .", red_135, red_136, red_137, red_138, red_139, red_140, red_141, red_142, red_143, red_144, red_145, red_146, red_147"
                   .", red_148, red_149, red_150, red_151, red_152, red_153, red_154, red_155, red_156, red_157, red_158, red_159, red_160"
                   .", red_161, red_162, red_163, red_164, red_165, red_166, red_167, red_168, red_169, red_170, red_171, red_172, red_173"
                   .", red_174, red_175, red_176, red_177, red_178, red_179, red_180, red_181, red_182, red_183, red_184, red_185, red_186"
                   .", red_187, red_188, red_189, red_190, red_191, red_192, red_193, red_194, red_195, red_196, red_197, red_198, red_199"
                   .", red_200, red_201, red_202, red_203, red_204, red_205, red_206, red_207, red_208, red_209, red_210, red_211, red_212"
                   .", red_213, red_214, red_215, red_216, red_217, red_218, red_219"
                   .", green_49, green_50, green_51, green_52, green_53, green_54, green_55, green_56, green_57, green_58, green_59"
                   .", green_60, green_61, green_62, green_63, green_64"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                           .", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_bill_recuperation_main jbrm"
                      ." WHERE jnl_status < 2 AND del_flg = 'f'";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'",             $facilityId); }

        $sql .=       ") jbrm"
              ." INNER JOIN (SELECT newest_apply_id, type_section, total_account AS red_96, total_days AS red_97, total_price AS red_98"
                                .", total_ins_charge AS red_99, total_pblc_charge AS red_100, total_own_charge AS red_101"
                                .", total_cost AS red_102, one_account AS red_103, one_days AS red_104, one_price AS red_105"
                                .", one_pblc_charge AS red_106, one_own_charge AS red_107, one_cost AS red_108, two_account AS red_109"
                                .", two_days AS red_110, two_price AS red_111, two_pblc_charge AS red_112, two_own_charge AS red_113"
                                .", two_cost AS red_114, empty_one_one AS green_49, empty_one_two AS green_50, three_account AS red_115"
                                .", three_days AS red_116, three_price AS red_117, three_pblc_charge AS red_118"
                                .", three_own_charge AS red_119, three_cost AS red_120, empty_two_one AS green_51"
                                .", empty_two_two AS green_52, four_account AS red_121, four_days AS red_122, four_price AS red_123"
                                .", four_pblc_charge AS red_124, four_own_charge AS red_125, four_cost AS red_126, del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab"
                           ." WHERE del_flg = 'f' AND type_section = 12"
                          .") jbrir12"
                      ." ON jbrm.newest_apply_id = jbrir12.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, total_account AS red_127, total_days AS red_128, total_price AS red_129"
                                .", total_ins_charge AS red_130, total_pblc_charge AS red_131, total_own_charge AS red_132"
                                .", total_cost AS red_133, one_account AS red_134, one_days AS red_135, one_price AS red_136"
                                .", one_pblc_charge AS red_137, one_own_charge AS red_138, one_cost AS red_139, two_account AS red_140"
                                .", two_days AS red_141, two_price AS red_142, two_pblc_charge AS red_143, two_own_charge AS red_144"
                                .", two_cost AS red_145, empty_one_one AS green_53, empty_one_two AS green_54, three_account AS red_146"
                                .", three_days AS red_147, three_price AS red_148, three_pblc_charge AS red_149"
                                .", three_own_charge AS red_150, three_cost AS red_151, empty_two_one AS green_55"
                                .", empty_two_two AS green_56, four_account AS red_152, four_days AS red_153, four_price AS red_154"
                                .", four_pblc_charge AS red_155, four_own_charge AS red_156, four_cost AS red_157, del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab"
                           ." WHERE del_flg = 'f' AND type_section = 13"
                          .") jbrir13"
                      ." ON jbrm.newest_apply_id = jbrir13.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, total_account AS red_158, total_days AS red_159, total_price AS red_160"
                                .", total_ins_charge AS red_161, total_pblc_charge AS red_162, total_own_charge AS red_163"
                                .", total_cost AS red_164, one_account AS red_165, one_days AS red_166, one_price AS red_167"
                                .", one_pblc_charge AS red_168, one_own_charge AS red_169, one_cost AS red_170, two_account AS red_171"
                                .", two_days AS red_172, two_price AS red_173, two_pblc_charge AS red_174, two_own_charge AS red_175"
                                .", two_cost AS red_176, empty_one_one AS green_57, empty_one_two AS green_58, three_account AS red_177"
                                .", three_days AS red_178, three_price AS red_179, three_pblc_charge AS red_180"
                                .", three_own_charge AS red_181, three_cost AS red_182, empty_two_one AS green_59"
                                .", empty_two_two AS green_60, four_account AS red_183, four_days AS red_184, four_price AS red_185"
                                .", four_pblc_charge AS red_186, four_own_charge AS red_187, four_cost AS red_188, del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab"
                           ." WHERE del_flg = 'f' AND type_section = 14"
                          .") jbrir14"
                      ." ON jbrm.newest_apply_id = jbrir14.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, total_account AS red_189, total_days AS red_190, total_price AS red_191"
                                .", total_ins_charge AS red_192, total_pblc_charge AS red_193, total_own_charge AS red_194"
                                .", total_cost AS red_195, one_account AS red_196, one_days AS red_197, one_price AS red_198"
                                .", one_pblc_charge AS red_199, one_own_charge AS red_200, one_cost AS red_201, two_account AS red_202"
                                .", two_days AS red_203, two_price AS red_204, two_pblc_charge AS red_205, two_own_charge AS red_206"
                                .", two_cost AS red_207, empty_one_one AS green_61, empty_one_two AS green_62, three_account AS red_208"
                                .", three_days AS red_209, three_price AS red_210, three_pblc_charge AS red_211"
                                .", three_own_charge AS red_212, three_cost AS red_213, empty_two_one AS green_63"
                                .", empty_two_two AS green_64, four_account AS red_214, four_days AS red_215, four_price AS red_216"
                                .", four_pblc_charge AS red_217, four_own_charge AS red_218, four_cost AS red_219, del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab"
                           ." WHERE del_flg = 'f' AND type_section = 15"
                          .") jbrir15"
                      ." ON jbrm.newest_apply_id = jbrir15.newest_apply_id ";
        return $this->getDataList($sql);
    }

    function getJnlBillRecuperationService($arr)
    {
        $rtnArr = array();
        $type0  = $this->getJnlBillRecuperationServiceType0($arr);
        $type1  = $this->getJnlBillRecuperationServiceType1($arr);

        $rtnArr = $this->arrayMerge($type0, $rtnArr);
        $rtnArr = $this->arrayMerge($type1, $rtnArr);

        return $rtnArr;
    }

    function getJnlBillRecuperationServiceType0($arr)
    {
        $facilityId = $this->getArrValueByKey('facility_id', $arr);
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);

        $sql = "SELECT jbrm.newest_apply_id, jbrm.emp_id, jbrm.jnl_facility_id, jbrm.jnl_status, jbrm.regist_date, jbrm.del_flg"
                   .", orange_281, orange_282, orange_283, orange_284, orange_285, orange_286, orange_287, orange_288, orange_289"
                   .", orange_290, orange_291, orange_292, orange_293, orange_294, orange_295, orange_296, orange_297, orange_298, orange_299"
                   .", orange_300, orange_301, orange_302, orange_303, orange_304, orange_305, orange_306, orange_307, orange_308, orange_309"
                   .", orange_310, orange_311, orange_312, orange_313, orange_314, orange_315, orange_316, orange_317, orange_318, orange_319"
                   .", orange_320, orange_321, orange_322, orange_323, orange_324, orange_325, orange_326, orange_327, orange_328"
                   .", red_221, red_222, red_223, red_224, red_225, red_226, red_227, red_228, red_229"
                   .", red_230, red_231, red_232, red_233, red_234, red_235, red_236, red_237, red_238, red_239"
                   .", red_240, red_241, red_242, red_243, red_244, red_245, red_246, red_247, red_248, red_249"
                   .", red_250, red_251, red_252, red_253, red_254, red_255, red_256, red_257, red_258, red_259"
                   .", red_260, red_261, red_262, red_263, red_264, red_265, red_266, red_267, red_268"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                           .", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_bill_recuperation_main jbrm"
                      ." WHERE jnl_status < 2 AND del_flg = 'f'";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'",             $facilityId); }

        $sql .=       ") jbrm"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind, short_house_acount AS orange_281"
                                .", short_house_days AS orange_282, short_house_cost AS orange_283, short_house_ins AS orange_284"
                                .", short_meal_acount AS orange_285, short_meal_days AS orange_286, short_meal_cost AS orange_287"
                                .", short_meal_ins AS orange_288, enter_house_acount AS orange_289, enter_house_days AS orange_290"
                                .", enter_house_cost AS orange_291, enter_house_ins AS orange_292, enter_meal_acount AS orange_293"
                                .", enter_meal_days AS orange_294, enter_meal_cost AS orange_295, enter_meal_ins AS orange_296"
                                .", total_house_acount AS red_221, total_house_days AS red_222, total_house_cost AS red_223"
                                .", total_house_ins AS red_224, total_meal_acount AS red_225, total_meal_days AS red_226"
                                .", total_meal_cost AS red_227, total_meal_ins AS red_228, del_flg"
                            ." FROM jnl_bill_recuperation_service"
                           ." WHERE del_flg = 'f' AND type_section = 0 AND type_kind = 0"
                          .") jbrs00"
                      ." ON jbrm.newest_apply_id = jbrs00.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind, short_house_acount AS orange_297"
                                .", short_house_days AS orange_298, short_house_cost AS orange_299, short_house_ins AS orange_300"
                                .", short_meal_acount AS orange_301, short_meal_days AS orange_302, short_meal_cost AS orange_303"
                                .", short_meal_ins AS orange_304, enter_house_acount AS orange_305, enter_house_days AS orange_306"
                                .", enter_house_cost AS orange_307, enter_house_ins AS orange_308, enter_meal_acount AS orange_309"
                                .", enter_meal_days AS orange_310, enter_meal_cost AS orange_311, enter_meal_ins AS orange_312"
                                .", total_house_acount AS red_229, total_house_days AS red_230, total_house_cost AS red_231"
                                .", total_house_ins AS red_232, total_meal_acount AS red_233, total_meal_days AS red_234"
                                .", total_meal_cost AS red_235, total_meal_ins AS red_236, del_flg"
                            ." FROM jnl_bill_recuperation_service"
                           ." WHERE del_flg = 'f' AND type_section = 0 AND type_kind = 1"
                          .") jbrs01"
                      ." ON jbrm.newest_apply_id = jbrs01.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind, short_house_acount AS orange_313"
                                .", short_house_days AS orange_314, short_house_cost AS orange_315, short_house_ins AS orange_316"
                                .", short_meal_acount AS orange_317, short_meal_days AS orange_318, short_meal_cost AS orange_319"
                                .", short_meal_ins AS orange_320, enter_house_acount AS orange_321, enter_house_days AS orange_322"
                                .", enter_house_cost AS orange_323, enter_house_ins AS orange_324, enter_meal_acount AS orange_325"
                                .", enter_meal_days AS orange_326, enter_meal_cost AS orange_327, enter_meal_ins AS orange_328"
                                .", total_house_acount AS red_237, total_house_days AS red_238, total_house_cost AS red_239"
                                .", total_house_ins AS red_240, total_meal_acount AS red_241, total_meal_days AS red_242"
                                .", total_meal_cost AS red_243, total_meal_ins AS red_244, del_flg"
                            ." FROM jnl_bill_recuperation_service"
                           ." WHERE del_flg = 'f' AND type_section = 0 AND type_kind = 2"
                          .") jbrs02"
                      ." ON jbrm.newest_apply_id = jbrs02.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind, short_house_acount AS red_245, short_house_days AS red_246"
                                .", short_house_cost AS red_247, short_house_ins AS red_248, short_meal_acount AS red_249"
                                .", short_meal_days AS red_250, short_meal_cost AS red_251, short_meal_ins AS red_252"
                                .", enter_house_acount AS red_253, enter_house_days AS red_254, enter_house_cost AS red_255"
                                .", enter_house_ins AS red_256, enter_meal_acount AS red_257, enter_meal_days AS red_258"
                                .", enter_meal_cost AS red_259, enter_meal_ins AS red_260, total_house_acount AS red_261"
                                .", total_house_days AS red_262, total_house_cost AS red_263, total_house_ins AS red_264"
                                .", total_meal_acount AS red_265, total_meal_days AS red_266, total_meal_cost AS red_267"
                                .", total_meal_ins AS red_268, del_flg"
                            ." FROM jnl_bill_recuperation_service"
                           ." WHERE del_flg = 'f' AND type_section = 0 AND type_kind = 3"
                          .") jbrs03"
                      ." ON jbrm.newest_apply_id = jbrs03.newest_apply_id";
        return $this->getDataList($sql);
    }

    function getJnlBillRecuperationServiceType1($arr)
    {
        $facilityId = $this->getArrValueByKey('facility_id', $arr);
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);

        $sql = "SELECT jbrm.newest_apply_id, jbrm.emp_id, jbrm.jnl_facility_id, jbrm.jnl_status, jbrm.regist_date, jbrm.del_flg"
                   .", orange_329"
                   .", orange_330, orange_331, orange_332, orange_333, orange_334, orange_335, orange_336, orange_337, orange_338, orange_339"
                   .", orange_340, orange_341, orange_342, orange_343, orange_344, orange_345, orange_346, orange_347, orange_348, orange_349"
                   .", orange_350, orange_351, orange_352, orange_353, orange_354, orange_355, orange_356, orange_357, orange_358, orange_359"
                   .", orange_360, orange_361, orange_362, orange_363, orange_364, orange_365, orange_366, orange_367, orange_368, orange_369"
                   .", orange_370, orange_371, orange_372, orange_373, orange_374, orange_375, orange_376"
                   .", red_269"
                   .", red_270, red_271, red_272, red_273, red_274, red_275, red_276, red_277, red_278, red_279"
                   .", red_280, red_281, red_282, red_283, red_284, red_285, red_286, red_287, red_288, red_289"
                   .", red_290, red_291, red_292, red_293, red_294, red_295, red_296, red_297, red_298, red_299"
                   .", red_300, red_301, red_302, red_303, red_304, red_305, red_306, red_307, red_308, red_309"
                   .", red_310, red_311, red_312, red_313, red_314, red_315, red_316"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                           .", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_bill_recuperation_main jbrm"
                      ." WHERE jnl_status < 2 AND del_flg = 'f'";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'",             $facilityId); }

        $sql .=       ") jbrm"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind, short_house_acount AS orange_329"
                                .", short_house_days AS orange_330, short_house_cost AS orange_331, short_house_ins AS orange_332"
                                .", short_meal_acount AS orange_333, short_meal_days AS orange_334, short_meal_cost AS orange_335"
                                .", short_meal_ins AS orange_336, enter_house_acount AS orange_337, enter_house_days AS orange_338"
                                .", enter_house_cost AS orange_339, enter_house_ins AS orange_340, enter_meal_acount AS orange_341"
                                .", enter_meal_days AS orange_342, enter_meal_cost AS orange_343, enter_meal_ins AS orange_344"
                                .", total_house_acount AS red_269, total_house_days AS red_270, total_house_cost AS red_271"
                                .", total_house_ins AS red_272, total_meal_acount AS red_273, total_meal_days AS red_274"
                                .", total_meal_cost AS red_275, total_meal_ins AS red_276, del_flg"
                            ." FROM jnl_bill_recuperation_service"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 0"
                          .") jbrs10 on jbrm.newest_apply_id = jbrs10.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind, short_house_acount AS orange_345"
                                .", short_house_days AS orange_346, short_house_cost AS orange_347, short_house_ins AS orange_348"
                                .", short_meal_acount AS orange_349, short_meal_days AS orange_350, short_meal_cost AS orange_351"
                                .", short_meal_ins AS orange_352, enter_house_acount AS orange_353, enter_house_days AS orange_354"
                                .", enter_house_cost AS orange_355, enter_house_ins AS orange_356, enter_meal_acount AS orange_357"
                                .", enter_meal_days AS orange_358, enter_meal_cost AS orange_359, enter_meal_ins AS orange_360"
                                .", total_house_acount AS red_277, total_house_days AS red_278, total_house_cost AS red_279"
                                .", total_house_ins AS red_280, total_meal_acount AS red_281, total_meal_days AS red_282"
                                .", total_meal_cost AS red_283, total_meal_ins AS red_284, del_flg"
                            ." FROM jnl_bill_recuperation_service"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 1"
                          .") jbrs11 on jbrm.newest_apply_id = jbrs11.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind, short_house_acount AS orange_361"
                                .", short_house_days AS orange_362, short_house_cost AS orange_363, short_house_ins AS orange_364"
                                .", short_meal_acount AS orange_365, short_meal_days AS orange_366, short_meal_cost AS orange_367"
                                .", short_meal_ins AS orange_368, enter_house_acount AS orange_369, enter_house_days AS orange_370"
                                .", enter_house_cost AS orange_371, enter_house_ins AS orange_372, enter_meal_acount AS orange_373"
                                .", enter_meal_days AS orange_374, enter_meal_cost AS orange_375, enter_meal_ins AS orange_376"
                                .", total_house_acount AS red_285, total_house_days AS red_286, total_house_cost AS red_287"
                                .", total_house_ins AS red_288, total_meal_acount AS red_289, total_meal_days AS red_290"
                                .", total_meal_cost AS red_291, total_meal_ins AS red_292, del_flg"
                            ." FROM jnl_bill_recuperation_service"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 2"
                          .") jbrs12 on jbrm.newest_apply_id = jbrs12.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind, short_house_acount AS red_293, short_house_days AS red_294"
                                .", short_house_cost AS red_295, short_house_ins AS red_296, short_meal_acount AS red_297"
                                .", short_meal_days AS red_298, short_meal_cost AS red_299, short_meal_ins AS red_300"
                                .", enter_house_acount AS red_301, enter_house_days AS red_302, enter_house_cost AS red_303"
                                .", enter_house_ins AS red_304, enter_meal_acount AS red_305, enter_meal_days AS red_306"
                                .", enter_meal_cost AS red_307, enter_meal_ins AS red_308, total_house_acount AS red_309"
                                .", total_house_days AS red_310, total_house_cost AS red_311, total_house_ins AS red_312"
                                .", total_meal_acount AS red_313, total_meal_days AS red_314, total_meal_cost AS red_315"
                                .", total_meal_ins AS red_316, del_flg"
                            ." FROM jnl_bill_recuperation_service"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 3"
                          .") jbrs13 on jbrm.newest_apply_id = jbrs13.newest_apply_id";
        return $this->getDataList($sql);
    }

    function getJnlBillRecuperationOwnPayment($arr)
    {
        $rtnArr = array();
        $visit  = $this->getJnlBillRecuperationOwnPaymentVisit($arr);
        $short  = $this->getJnlBillRecuperationOwnPaymentShort($arr);
        $enter  = $this->getJnlBillRecuperationOwnPaymentEnter($arr);
        $other  = $this->getJnlBillRecuperationOwnPaymentOther($arr);

        $rtnArr = $this->arrayMerge($visit, $rtnArr);
        $rtnArr = $this->arrayMerge($short, $rtnArr);
        $rtnArr = $this->arrayMerge($enter, $rtnArr);
        $rtnArr = $this->arrayMerge($other, $rtnArr);

        return $rtnArr;
    }

    function getJnlBillRecuperationOwnPaymentVisit($arr)
    {
        $facilityId = $this->getArrValueByKey('facility_id', $arr);
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);

        $sql = "SELECT jbrm.newest_apply_id, jbrm.emp_id, jbrm.jnl_facility_id, jbrm.jnl_status, jbrm.regist_date, jbrm.del_flg"
                   .", jbrm.year, jbrm.month"
                   .", orange_395, orange_396, orange_397"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                           .", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_bill_recuperation_main jbrm"
                      ." WHERE jnl_status < 2 AND del_flg = 'f'";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'",             $facilityId); }

        $sql .=       ") jbrm"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_395, days AS orange_396, money AS orange_397, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 0 AND type_kind = 0"
                          .") jbrop000"
                      ." ON jbrm.newest_apply_id = jbrop000.newest_apply_id";
        return $this->getDataList($sql);
    }

    function getJnlBillRecuperationOwnPaymentShort($arr)
    {
        $facilityId = $this->getArrValueByKey('facility_id', $arr);
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);

        $sql = "SELECT jbrm.newest_apply_id, jbrm.emp_id, jbrm.jnl_facility_id, jbrm.jnl_status, jbrm.regist_date, jbrm.del_flg"
                   .", jbrm.year, jbrm.month"
                   .", orange_398, orange_399"
                   .", orange_400, orange_401, orange_402, orange_403, orange_404, orange_405, orange_406, orange_407, orange_408, orange_409"
                   .", orange_410, orange_411, orange_412, orange_413, orange_414, orange_415, orange_416, orange_417, orange_418, orange_419"
                   .", orange_420, orange_421, orange_422, orange_423, orange_424, orange_425, orange_426, orange_427, orange_428, orange_429"
                   .", orange_430, orange_431, orange_432, orange_433"
                   .", red_323"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                           .", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_bill_recuperation_main jbrm"
                      ." WHERE jnl_status < 2 AND del_flg = 'f'";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'",             $facilityId); }

        $sql .=       ") jbrm"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_398, days AS orange_399, money AS orange_400, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 1"
                          .") jbrop101"
                      ." ON jbrm.newest_apply_id = jbrop101.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_401, days AS orange_402, money AS orange_403, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 2"
                          .") jbrop102"
                      ." ON jbrm.newest_apply_id = jbrop102.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_404, days AS orange_405, money AS orange_406, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 3"
                          .") jbrop103"
                      ." ON jbrm.newest_apply_id = jbrop103.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_407, days AS orange_408, money AS orange_409, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 4"
                          .") jbrop104"
                      ." ON jbrm.newest_apply_id = jbrop104.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_410, days AS orange_411, money AS orange_412, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 5"
                          .") jbrop105"
                      ." ON jbrm.newest_apply_id = jbrop105.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_413, days AS orange_414, money AS orange_415, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 6"
                          .") jbrop106"
                      ." ON jbrm.newest_apply_id = jbrop106.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_416, days AS orange_417, money AS orange_418, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 7"
                          .") jbrop107"
                      ." ON jbrm.newest_apply_id = jbrop107.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_419, days AS orange_420, money AS orange_421, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 8"
                          .") jbrop108"
                      ." ON jbrm.newest_apply_id = jbrop108.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_422, days AS orange_423, money AS orange_424, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 9"
                          .") jbrop109"
                      ." ON jbrm.newest_apply_id = jbrop109.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_425, days AS orange_426, money AS orange_427, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 10"
                          .") jbrop110"
                      ." ON jbrm.newest_apply_id = jbrop110.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_428, days AS orange_429, money AS orange_430, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 11"
                          .") jbrop111"
                      ." ON jbrm.newest_apply_id = jbrop111.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_431, days AS orange_432, money AS orange_433, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 12"
                          .") jbrop112"
                    ." ON jbrm.newest_apply_id = jbrop112.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind, money AS red_323, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 13"
                          .") jbrop113"
                    ." ON jbrm.newest_apply_id = jbrop113.newest_apply_id";
        return $this->getDataList($sql);
    }

    function getJnlBillRecuperationOwnPaymentEnter($arr)
    {
        $facilityId = $this->getArrValueByKey('facility_id', $arr);
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);

        $sql = "SELECT jbrm.newest_apply_id, jbrm.emp_id, jbrm.jnl_facility_id, jbrm.jnl_status, jbrm.regist_date, jbrm.del_flg"
                   .", jbrm.year, jbrm.month"
                   .", orange_434, orange_435, orange_436, orange_437, orange_438, orange_439"
                   .", orange_440, orange_441, orange_442, orange_443, orange_444, orange_445, orange_446, orange_447, orange_448, orange_449"
                   .", orange_450, orange_451, orange_452, orange_453, orange_454, orange_455, orange_456, orange_457, orange_458, orange_459"
                   .", orange_460, orange_461, orange_462, orange_463, orange_464, orange_465, orange_466, orange_467, orange_468, orange_469"
                   .", red_324"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                           .", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_bill_recuperation_main jbrm"
                      ." WHERE jnl_status < 2 AND del_flg = 'f'";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'",             $facilityId); }

        $sql .=       ") jbrm"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_434, days AS orange_435, money AS orange_436, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 2 AND type_kind = 1"
                          .") jbrop201"
                      ." ON jbrm.newest_apply_id = jbrop201.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_437, days AS orange_438, money AS orange_439, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 2 AND type_kind = 2"
                          .") jbrop202"
                      ." ON jbrm.newest_apply_id = jbrop202.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_440, days AS orange_441, money AS orange_442, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 2 AND type_kind = 3"
                          .") jbrop203"
                      ." ON jbrm.newest_apply_id = jbrop203.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_443, days AS orange_444, money AS orange_445, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 2 AND type_kind = 4"
                          .") jbrop204"
                      ." ON jbrm.newest_apply_id = jbrop204.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_446, days AS orange_447, money AS orange_448, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 2 AND type_kind = 5"
                          .") jbrop205"
                      ." ON jbrm.newest_apply_id = jbrop205.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_449, days AS orange_450, money AS orange_451, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 2 AND type_kind = 6"
                          .") jbrop206"
                      ." ON jbrm.newest_apply_id = jbrop206.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_452, days AS orange_453, money AS orange_454, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 2 AND type_kind = 7"
                          .") jbrop207"
                      ." ON jbrm.newest_apply_id = jbrop207.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_455, days AS orange_456, money AS orange_457, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 2 AND type_kind = 8"
                          .") jbrop208"
                      ." ON jbrm.newest_apply_id = jbrop208.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_458, days AS orange_459, money AS orange_460, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 2 AND type_kind = 9"
                          .") jbrop209"
                      ." ON jbrm.newest_apply_id = jbrop209.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_461, days AS orange_462, money AS orange_463, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 2 AND type_kind = 10"
                          .") jbrop210"
                      ." ON jbrm.newest_apply_id = jbrop210.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_464, days AS orange_465, money AS orange_466, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 2 AND type_kind = 11"
                          .") jbrop211"
                      ." ON jbrm.newest_apply_id = jbrop211.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_467, days AS orange_468, money AS orange_469, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 2 AND type_kind = 12"
                          .") jbrop212"
                      ." ON jbrm.newest_apply_id = jbrop212.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind, money AS red_324, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 2 AND type_kind = 13"
                          .") jbrop213"
                      ." ON jbrm.newest_apply_id = jbrop213.newest_apply_id";
        return $this->getDataList($sql);
    }

    function getJnlBillRecuperationOwnPaymentOther($arr)
    {
        $facilityId = $this->getArrValueByKey('facility_id', $arr);
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);

        $sql = "SELECT jbrm.newest_apply_id, jbrm.emp_id, jbrm.jnl_facility_id, jbrm.jnl_status, jbrm.regist_date, jbrm.del_flg"
                   .", jbrm.year, jbrm.month"
                   .", orange_470, orange_471"
                   .", red_325"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                           .", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_bill_recuperation_main jbrm"
                      ." WHERE jnl_status < 2 AND del_flg = 'f'";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'",             $facilityId); }

        $sql .=       ") jbrm"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind, money AS orange_470, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 3 AND type_kind = 14"
                          .") jbrop314"
                      ." ON jbrm.newest_apply_id = jbrop314.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind, money AS orange_471, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 4 AND type_kind = 15"
                          .") jbrop415"
                      ." ON jbrm.newest_apply_id = jbrop415.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind, money AS red_325, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment"
                           ." WHERE del_flg = 'f' AND type_section = 5 AND type_kind = 16"
                          .") jbrop516"
                      ." ON jbrm.newest_apply_id = jbrop516.newest_apply_id";
        return $this->getDataList($sql);
    }

    function getJnlBillRecuperationEtc($arr)
    {
        $facilityId = $this->getArrValueByKey('facility_id', $arr);
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);

        $sql = "SELECT jbrm.newest_apply_id, jbrm.emp_id, jbrm.jnl_facility_id, jbrm.jnl_status, jbrm.regist_date, jbrm.del_flg"
                   .", jbrm.year, jbrm.month"
                   .", orange_280"
                   .", orange_377, orange_378, orange_379"
                   .", orange_380, orange_381, orange_382, orange_383, orange_384, orange_385, orange_386, orange_387, orange_388, orange_389"
                   .", orange_390, orange_391, orange_392, orange_393, orange_394"
                   .", red_1, red_2"
                   .", red_220"
                   .", red_317, red_318, red_319, red_320, red_321, red_322"
                   .", red_326"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                           .", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_bill_recuperation_main jbrm"
                      ." WHERE jnl_status < 2 AND del_flg = 'f'";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'",             $facilityId); }

        $sql .=       ") jbrm"
              ." INNER JOIN (SELECT newest_apply_id, support_total_acount AS red_317, support_total_price AS red_318"
                                .", support_total_amount AS red_319, support_now_acount AS orange_377, support_now_price AS orange_378"
                                .", support_now_amount AS orange_379, support_delay_acount AS orange_380, support_delay_price AS orange_381"
                                .", support_delay_amount AS orange_382, support_return_acount AS orange_383"
                                .", support_return_price AS orange_384, support_return_amount AS orange_385"
                                .", rate_date_total_low_acount AS orange_386, rate_date_total_low_days AS orange_387"
                                .", rate_date_total_total_days AS orange_388, rate_date_total_rate AS red_320"
                                .", rate_date_enter_low_acount AS orange_389, rate_date_enter_low_days AS orange_390"
                                .", rate_date_enter_total_days AS orange_391, rate_date_enter_rate AS red_321"
                                .", rate_date_short_low_acount AS orange_392, rate_date_short_low_days AS orange_393"
                                .", rate_date_short_total_days AS orange_394, rate_date_short_rate AS red_322, all_total_charge AS red_1"
                                .", ins_ins_total_charge AS red_2, specific_enter_total_charge AS red_220"
                                .", ins_rehab_total_charge AS red_326, improvement_grant AS orange_280"
                            ." FROM jnl_bill_recuperation_etc"
                           ." WHERE del_flg = 'f'"
                          .") jbre"
                      ." ON jbrm.newest_apply_id = jbre.newest_apply_id";
        return $this->getDataList($sql);
    }
}

class JnlBillRecuperationPrepay extends IndicatorData
{
    function JnlBillRecuperationPrepay($arr)
    {
        parent::IndicatorData($arr);
    }

    function getJnlBillRecuperationPrepay($arr)
    {
        $rtnArr      = array();
        $insureRehab = $this->getJnlBillRecuperationInsureRehabPrepay($arr);
        $service     = $this->getJnlBillRecuperationServicePrepay($arr);
        $ownPayment  = $this->getJnlBillRecuperationOwnPaymentPrepay($arr);
        $etc         = $this->getJnlBillRecuperationEtcPrepay($arr);

        $rtnArr = $this->arrayMerge($insureRehab, $rtnArr);
        $rtnArr = $this->arrayMerge($service,     $rtnArr);
        $rtnArr = $this->arrayMerge($ownPayment,  $rtnArr);
        $rtnArr = $this->arrayMerge($etc,         $rtnArr);

        return $rtnArr;
    }

    function getJnlBillRecuperationInsureRehabPrepay($arr)
    {
        $rtnArr = array();
        $type0  = $this->getJnlBillRecuperationInsureRehabPrepayType0($arr);
        $type1  = $this->getJnlBillRecuperationInsureRehabPrepayType1($arr);

        $rtnArr = $this->arrayMerge($type0, $rtnArr);
        $rtnArr = $this->arrayMerge($type1, $rtnArr);

        return $rtnArr;
    }

    function getJnlBillRecuperationInsureRehabPrepayType0($arr)
    {
        $rtnArr       = array();
        $presentMonth = $this->getJnlBillRecuperationInsureRehabPrepayPresentMonth($arr);
        $monthsDelay  = $this->getJnlBillRecuperationInsureRehabPrepayMonthsDelay($arr);
        $reclaim      = $this->getJnlBillRecuperationInsureRehabPrepayReclaim($arr);
        $total        = $this->getJnlBillRecuperationInsureRehabPrepayTotal($arr);

        $rtnArr = $this->arrayMerge($presentMonth, $rtnArr);
        $rtnArr = $this->arrayMerge($monthsDelay,  $rtnArr);
        $rtnArr = $this->arrayMerge($reclaim,      $rtnArr);
        $rtnArr = $this->arrayMerge($total,        $rtnArr);

        return $rtnArr;
    }

    function getJnlBillRecuperationInsureRehabPrepayType1($arr)
    {
        $facilityId = $this->getArrValueByKey('facility_id', $arr);
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);

        $sql = "SELECT jbrpm.newest_apply_id, jbrpm.emp_id, jbrpm.jnl_facility_id, jbrpm.jnl_status, jbrpm.regist_date, jbrpm.del_flg"
                   .", jbrpm.year, jbrpm.month"
                   .", orange_862, orange_863, orange_864, orange_865, orange_866, orange_867, orange_868, orange_869"
                   .", orange_870, orange_871, orange_872, orange_873, orange_874, orange_875, orange_876, orange_877, orange_878, orange_879"
                   .", orange_880, orange_881, orange_882, orange_883, orange_884, orange_885, orange_886, orange_887, orange_888, orange_889"
                   .", orange_890, orange_891, orange_892, orange_893, orange_894, orange_895, orange_896, orange_897, orange_898, orange_899"
                   .", orange_900, orange_901, orange_902, orange_903, orange_904, orange_905, orange_906, orange_907, orange_908, orange_909"
                   .", orange_910, orange_911, orange_912, orange_913, orange_914, orange_915, orange_916, orange_917, orange_918, orange_919"
                   .", orange_920, orange_921, orange_922, orange_923, orange_924, orange_925, orange_926, orange_927, orange_928, orange_929"
                   .", orange_930, orange_931, orange_932, orange_933, orange_934, orange_935, orange_936, orange_937, orange_938, orange_939"
                   .", orange_940, orange_941, orange_942, orange_943, orange_944, orange_945, orange_946, orange_947, orange_948, orange_949"
                   .", orange_950, orange_951, orange_952, orange_953, orange_954, orange_955, orange_956, orange_957, orange_958, orange_959"
                   .", orange_960"
                   .", red_573, red_574, red_575, red_576, red_577, red_578, red_579"
                   .", red_580, red_581, red_582, red_583, red_584, red_585, red_586, red_587, red_588, red_589"
                   .", red_590, red_591, red_592, red_593, red_594, red_595, red_596, red_597, red_598, red_599"
                   .", red_600, red_601, red_602, red_603, red_604, red_605"
                   .", green_129"
                   .", green_130, green_131, green_132, green_133, green_134, green_135, green_136, green_137, green_138, green_139"
                   .", green_140, green_141, green_142, green_143, green_144"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                           .", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_bill_recuperation_prepay_main jbrpm"
                      ." WHERE jnl_status < 2 AND del_flg = 'f'";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'",             $facilityId); }

        $sql .=       ") jbrpm"
              ." INNER JOIN (SELECT newest_apply_id, type_section"
                                .", total_account AS orange_862, total_days AS orange_863, total_price AS orange_864, total_ins_charge AS orange_865, total_pblc_charge AS orange_866, total_own_charge AS orange_867, total_cost AS orange_868"
                                .", one_account AS orange_869, one_days AS orange_870, one_price AS orange_871, one_pblc_charge AS orange_872, one_own_charge AS orange_873, one_cost AS orange_874"
                                .", two_account AS orange_875, two_days AS orange_876, two_price AS orange_877, two_pblc_charge AS orange_878, two_own_charge AS orange_879, two_cost AS orange_880"
                                .", empty_one_one AS green_129, empty_one_two AS green_130"
                                .", three_account AS orange_881, three_days AS orange_882, three_price AS orange_883, three_ins_charge AS orange_884, three_pblc_charge AS orange_885, three_own_charge AS orange_886, three_cost AS orange_887"
                                .", empty_two_one AS green_131, empty_two_two AS green_132"
                                .", four_account AS orange_888, four_days AS orange_889, four_price AS orange_890, four_ins_charge AS orange_891, four_pblc_charge AS orange_892, four_own_charge AS orange_893, four_cost AS orange_894"
                                .", del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 12"
                          .") jbrirp12"
                      ." ON jbrpm.newest_apply_id = jbrirp12.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section"
                                .", total_account AS orange_895, total_days AS orange_896, total_price AS orange_897, total_ins_charge AS orange_898, total_pblc_charge AS orange_899, total_own_charge AS orange_900, total_cost AS orange_901"
                                .", one_account AS orange_902, one_days AS orange_903, one_price AS orange_904, one_pblc_charge AS orange_905, one_own_charge AS orange_906, one_cost AS orange_907"
                                .", two_account AS orange_908, two_days AS orange_909, two_price AS orange_910, two_pblc_charge AS orange_911, two_own_charge AS orange_912, two_cost AS orange_913"
                                .", empty_one_one AS green_133, empty_one_two AS green_134"
                                .", three_account AS orange_914, three_days AS orange_915, three_price AS orange_916, three_ins_charge AS orange_917, three_pblc_charge AS orange_918, three_own_charge AS orange_919, three_cost AS orange_920"
                                .", empty_two_one AS green_135, empty_two_two AS green_136"
                                .", four_account AS orange_921, four_days AS orange_922, four_price AS orange_923, four_ins_charge AS orange_924, four_pblc_charge AS orange_925, four_own_charge AS orange_926, four_cost AS orange_927"
                                .", del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 13"
                          .") jbrirp13"
                      ." ON jbrpm.newest_apply_id = jbrirp13.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section"
                                .", total_account AS orange_928, total_days AS orange_929, total_price AS orange_930, total_ins_charge AS orange_931, total_pblc_charge AS orange_932, total_own_charge AS orange_933, total_cost AS orange_934"
                                .", one_account AS orange_935, one_days AS orange_936, one_price AS orange_937, one_pblc_charge AS orange_938, one_own_charge AS orange_939, one_cost AS orange_940"
                                .", two_account AS orange_941, two_days AS orange_942, two_price AS orange_943, two_pblc_charge AS orange_944, two_own_charge AS orange_945, two_cost AS orange_946"
                                .", empty_one_one AS green_137, empty_one_two AS green_138"
                                .", three_account AS orange_947, three_days AS orange_948, three_price AS orange_949, three_ins_charge AS orange_950, three_pblc_charge AS orange_951, three_own_charge AS orange_952, three_cost AS orange_953"
                                .", empty_two_one AS green_139, empty_two_two AS green_140"
                                .", four_account AS orange_954, four_days AS orange_955, four_price AS orange_956, four_ins_charge AS orange_957, four_pblc_charge AS orange_958, four_own_charge AS orange_959, four_cost AS orange_960"
                                .", del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 14"
                          .") jbrirp14"
                      ." ON jbrpm.newest_apply_id = jbrirp14.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section"
                                .", total_account AS red_573, total_days AS red_574, total_price AS red_575, total_ins_charge AS red_576, total_pblc_charge AS red_577, total_own_charge AS red_578, total_cost AS red_579"
                                .", one_account AS red_580, one_days AS red_581, one_price AS red_582, one_pblc_charge AS red_583, one_own_charge AS red_584, one_cost AS red_585"
                                .", two_account AS red_586, two_days AS red_587, two_price AS red_588, two_pblc_charge AS red_589, two_own_charge AS red_590, two_cost AS red_591"
                                .", empty_one_one AS green_141, empty_one_two AS green_142"
                                .", three_account AS red_592, three_days AS red_593, three_price AS red_594, three_ins_charge AS red_595, three_pblc_charge AS red_596, three_own_charge AS red_597, three_cost AS red_598"
                                .", empty_two_one AS green_143, empty_two_two AS green_144"
                                .", four_account AS red_599, four_days AS red_600, four_price AS red_601, four_ins_charge AS red_602, four_pblc_charge AS red_603, four_own_charge AS red_604, four_cost AS red_605"
                                .", del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 15"
                          .") jbrirp15"
                      ." ON jbrpm.newest_apply_id = jbrirp15.newest_apply_id";
        return $this->getDataList($sql);
    }

    function getJnlBillRecuperationInsureRehabPrepayPresentMonth($arr)
    {
        $facilityId = $this->getArrValueByKey('facility_id', $arr);
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);

        $sql = "SELECT jbrpm.newest_apply_id, jbrpm.emp_id, jbrpm.jnl_facility_id, jbrpm.jnl_status, jbrpm.regist_date, jbrpm.del_flg"
                   .", jbrpm.year, jbrpm.month"
                   .", orange_571, orange_572, orange_573, orange_574, orange_575, orange_576, orange_577, orange_578, orange_579"
                   .", orange_580, orange_581, orange_582, orange_583, orange_584, orange_585, orange_586, orange_587, orange_588, orange_589"
                   .", orange_590, orange_591, orange_592, orange_593, orange_594, orange_595, orange_596, orange_597, orange_598, orange_599"
                   .", orange_600, orange_601, orange_602, orange_603, orange_604, orange_605, orange_606, orange_607, orange_608, orange_609"
                   .", orange_610, orange_611, orange_612, orange_613, orange_614, orange_615, orange_616, orange_617, orange_618, orange_619"
                   .", orange_620, orange_621, orange_622, orange_623, orange_624, orange_625, orange_626, orange_627, orange_628, orange_629"
                   .", orange_630, orange_631, orange_632"
                   .", red_362, red_363, red_364, red_365, red_366, red_367, red_368, red_369"
                   .", red_370, red_371, red_372, red_373, red_374, red_375, red_376, red_377, red_378, red_379"
                   .", red_380, red_381, red_382, red_383, red_384, red_385, red_386, red_387, red_388, red_389"
                   .", red_390, red_391, red_392"
                   .", green_81, green_82, green_83, green_84, green_85, green_86, green_87, green_88, green_89"
                   .", green_90, green_91, green_92"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                           .", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_bill_recuperation_prepay_main jbrpm"
                      ." WHERE jnl_status < 2 AND del_flg = 'f'";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'",             $facilityId); }

        $sql .=       ") jbrpm"
              ." INNER JOIN (SELECT newest_apply_id, type_section"
                                .", total_account AS orange_571, total_days AS orange_572, total_price AS orange_573, total_ins_charge AS orange_574"
                                .", total_pblc_charge AS orange_575, total_own_charge AS orange_576, total_cost AS orange_577"
                                .", one_account AS orange_578, one_days AS orange_579, one_price AS orange_580, one_pblc_charge AS orange_581"
                                .", one_own_charge AS orange_582, one_cost AS orange_583"
                                .", two_account AS orange_584, two_days AS orange_585, two_price AS orange_586, two_pblc_charge AS orange_587"
                                .", two_own_charge AS orange_588, two_cost AS orange_589"
                                .", empty_one_one AS green_81, empty_one_two AS green_82"
                                .", three_account AS orange_590, three_days AS orange_591, three_price AS orange_592, three_pblc_charge AS orange_593"
                                .", three_own_charge AS orange_594, three_cost AS orange_595"
                                .", empty_two_one AS green_83, empty_two_two AS green_84"
                                .", four_account AS orange_596, four_days AS orange_597, four_price AS orange_598, four_pblc_charge AS orange_599"
                                .", four_own_charge AS orange_600, four_cost AS orange_601"
                                .", del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 0"
                          .") jbrirp00"
                      ." ON jbrpm.newest_apply_id = jbrirp00.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section"
                                .", total_account AS orange_602, total_days AS orange_603, total_price AS orange_604, total_ins_charge AS orange_605"
                                .", total_pblc_charge AS orange_606, total_own_charge AS orange_607, total_cost AS orange_608"
                                .", one_account AS orange_609, one_days AS orange_610, one_price AS orange_611, one_pblc_charge AS orange_612"
                                .", one_own_charge AS orange_613, one_cost AS orange_614"
                                .", two_account AS orange_615, two_days AS orange_616, two_price AS orange_617, two_pblc_charge AS orange_618"
                                .", two_own_charge AS orange_619, two_cost AS orange_620"
                                .", empty_one_one AS green_85, empty_one_two AS green_86"
                                .", three_account AS orange_621, three_days AS orange_622, three_price AS orange_623, three_pblc_charge AS orange_624"
                                .", three_own_charge AS orange_625, three_cost AS orange_626"
                                .", empty_two_one AS green_87, empty_two_two AS green_88"
                                .", four_account AS orange_627, four_days AS orange_628, four_price AS orange_629, four_pblc_charge AS orange_630"
                                .", four_own_charge AS orange_631, four_cost AS orange_632"
                                .", del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 1"
                          .") jbrirp01"
                      ." ON jbrpm.newest_apply_id = jbrirp01.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section"
                                .", total_account AS red_362, total_days AS red_363, total_price AS red_364, total_ins_charge AS red_365, total_pblc_charge AS red_366, total_own_charge AS red_367, total_cost AS red_368"
                                .", one_account AS red_369, one_days AS red_370, one_price AS red_371, one_pblc_charge AS red_372, one_own_charge AS red_373, one_cost AS red_374"
                                .", two_account AS red_375, two_days AS red_376, two_price AS red_377, two_pblc_charge AS red_378, two_own_charge AS red_379, two_cost AS red_380"
                                .", empty_one_one AS green_89, empty_one_two AS green_90"
                                .", three_account AS red_381, three_days AS red_382, three_price AS red_383, three_pblc_charge AS red_384, three_own_charge AS red_385, three_cost AS red_386"
                                .", empty_two_one AS green_91, empty_two_two AS green_92"
                                .", four_account AS red_387, four_days AS red_388, four_price AS red_389, four_pblc_charge AS red_390, four_own_charge AS red_391, four_cost AS red_392"
                                .", del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 2"
                          .") jbrirp02"
                      ." ON jbrpm.newest_apply_id = jbrirp02.newest_apply_id ";
        return $this->getDataList($sql);
    }

    function getJnlBillRecuperationInsureRehabPrepayMonthsDelay($arr)
    {
        $facilityId = $this->getArrValueByKey('facility_id', $arr);
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);

        $sql = "SELECT jbrpm.newest_apply_id, jbrpm.emp_id, jbrpm.jnl_facility_id, jbrpm.jnl_status, jbrpm.regist_date, jbrpm.del_flg, jbrpm.year"
                   .", jbrpm.month"
                   .", orange_633, orange_634, orange_635, orange_636, orange_637, orange_638, orange_639"
                   .", orange_640, orange_641, orange_642, orange_643, orange_644, orange_645, orange_646, orange_647, orange_648, orange_649"
                   .", orange_650, orange_651, orange_652, orange_653, orange_654, orange_655, orange_656, orange_657, orange_658, orange_659"
                   .", orange_660, orange_661, orange_662, orange_663, orange_664, orange_665, orange_666, orange_667, orange_668, orange_669"
                   .", orange_670, orange_671, orange_672, orange_673, orange_674, orange_675, orange_676, orange_677, orange_678, orange_679"
                   .", orange_680, orange_681, orange_682, orange_683, orange_684, orange_685, orange_686, orange_687, orange_688, orange_689"
                   .", orange_690, orange_691, orange_692, orange_693, orange_694"
                   .", red_393, red_394, red_395, red_396, red_397, red_398, red_399"
                   .", red_400, red_401, red_402, red_403, red_404, red_405, red_406, red_407, red_408, red_409"
                   .", red_410, red_411, red_412, red_413, red_414, red_415, red_416, red_417, red_418, red_419"
                   .", red_420, red_421, red_422, red_423"
                   .", green_93, green_94, green_95, green_96, green_97, green_98, green_99"
                   .", green_100, green_101, green_102, green_103, green_104"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                           .", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_bill_recuperation_prepay_main jbrpm"
                      ." WHERE jnl_status < 2 AND del_flg = 'f'";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'",             $facilityId); }

        $sql .=       ") jbrpm"
              ." INNER JOIN (SELECT newest_apply_id, type_section"
                                .", total_account AS orange_633, total_days AS orange_634, total_price AS orange_635, total_ins_charge AS orange_636, total_pblc_charge AS orange_637, total_own_charge AS orange_638, total_cost AS orange_639"
                                .", one_account AS orange_640, one_days AS orange_641, one_price AS orange_642, one_pblc_charge AS orange_643, one_own_charge AS orange_644, one_cost AS orange_645"
                                .", two_account AS orange_646, two_days AS orange_647, two_price AS orange_648, two_pblc_charge AS orange_649, two_own_charge AS orange_650, two_cost AS orange_651"
                                .", empty_one_one AS green_93, empty_one_two AS green_94"
                                .", three_account AS orange_652, three_days AS orange_653, three_price AS orange_654, three_pblc_charge AS orange_655, three_own_charge AS orange_656, three_cost AS orange_657"
                                .", empty_two_one AS green_95, empty_two_two AS green_96"
                                .", four_account AS orange_658, four_days AS orange_659, four_price AS orange_660, four_pblc_charge AS orange_661, four_own_charge AS orange_662, four_cost AS orange_663"
                                .", del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 3"
                          .") jbrirp03"
                      ." ON jbrpm.newest_apply_id = jbrirp03.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section"
                                .", total_account AS orange_664, total_days AS orange_665, total_price AS orange_666, total_ins_charge AS orange_667, total_pblc_charge AS orange_668, total_own_charge AS orange_669, total_cost AS orange_670"
                                .", one_account AS orange_671, one_days AS orange_672, one_price AS orange_673, one_pblc_charge AS orange_674, one_own_charge AS orange_675, one_cost AS orange_676"
                                .", two_account AS orange_677, two_days AS orange_678, two_price AS orange_679, two_pblc_charge AS orange_680, two_own_charge AS orange_681, two_cost AS orange_682"
                                .", empty_one_one AS green_97, empty_one_two AS green_98"
                                .", three_account AS orange_683, three_days AS orange_684, three_price AS orange_685, three_pblc_charge AS orange_686, three_own_charge AS orange_687, three_cost AS orange_688"
                                .", empty_two_one AS green_99, empty_two_two AS green_100"
                                .", four_account AS orange_689, four_days AS orange_690, four_price AS orange_691, four_pblc_charge AS orange_692, four_own_charge AS orange_693, four_cost AS orange_694"
                                .", del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 4"
                          .") jbrirp04"
                      ." ON jbrpm.newest_apply_id = jbrirp04.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section"
                                .", total_account AS red_393, total_days AS red_394, total_price AS red_395, total_ins_charge AS red_396, total_pblc_charge AS red_397, total_own_charge AS red_398, total_cost AS red_399"
                                .", one_account AS red_400, one_days AS red_401, one_price AS red_402, one_pblc_charge AS red_403, one_own_charge AS red_404, one_cost AS red_405"
                                .", two_account AS red_406, two_days AS red_407, two_price AS red_408, two_pblc_charge AS red_409, two_own_charge AS red_410, two_cost AS red_411"
                                .", empty_one_one AS green_101, empty_one_two AS green_102"
                                .", three_account AS red_412, three_days AS red_413, three_price AS red_414, three_pblc_charge AS red_415, three_own_charge AS red_416, three_cost AS red_417"
                                .", empty_two_one AS green_103, empty_two_two AS green_104"
                                .", four_account AS red_418, four_days AS red_419, four_price AS red_420, four_pblc_charge AS red_421, four_own_charge AS red_422, four_cost AS red_423"
                                .", del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 5"
                          .") jbrirp05"
                      ." ON jbrpm.newest_apply_id = jbrirp05.newest_apply_id ";
        return $this->getDataList($sql);
    }

    function getJnlBillRecuperationInsureRehabPrepayReclaim($arr)
    {
        $facilityId = $this->getArrValueByKey('facility_id', $arr);
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);

        $sql = "SELECT jbrpm.newest_apply_id, jbrpm.emp_id, jbrpm.jnl_facility_id, jbrpm.jnl_status, jbrpm.regist_date, jbrpm.del_flg, jbrpm.year"
                   .", jbrpm.month"
                   .", orange_695, orange_696, orange_697, orange_698, orange_699"
                   .", orange_700, orange_701, orange_702, orange_703, orange_704, orange_705, orange_706, orange_707, orange_708, orange_709"
                   .", orange_710, orange_711, orange_712, orange_713, orange_714, orange_715, orange_716, orange_717, orange_718, orange_719"
                   .", orange_720, orange_721, orange_722, orange_723, orange_724, orange_725, orange_726, orange_727, orange_728, orange_729"
                   .", orange_730, orange_731, orange_732, orange_733, orange_734, orange_735, orange_736, orange_737, orange_738, orange_739"
                   .", orange_740, orange_741, orange_742, orange_743, orange_744, orange_745, orange_746, orange_747, orange_748, orange_749"
                   .", orange_750, orange_751, orange_752, orange_753, orange_754, orange_755, orange_756"
                   .", red_424, red_425, red_426, red_427, red_428, red_429"
                   .", red_430, red_431, red_432, red_433, red_434, red_435, red_436, red_437, red_438, red_439"
                   .", red_440, red_441, red_442, red_443, red_444, red_445, red_446, red_447, red_448, red_449"
                   .", red_450, red_451, red_452, red_453, red_454"
                   .", green_105, green_106, green_107, green_108, green_109"
                   .", green_110, green_111, green_112, green_113, green_114, green_115, green_116"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                           .", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_bill_recuperation_prepay_main jbrpm"
                      ." WHERE jnl_status < 2 AND del_flg = 'f'";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'",             $facilityId); }

        $sql .=       ") jbrpm"
              ." INNER JOIN (SELECT newest_apply_id, type_section"
                                .", total_account AS orange_695, total_days AS orange_696, total_price AS orange_697, total_ins_charge AS orange_698, total_pblc_charge AS orange_699, total_own_charge AS orange_700, total_cost AS orange_701"
                                .", one_account AS orange_702, one_days AS orange_703, one_price AS orange_704, one_pblc_charge AS orange_705, one_own_charge AS orange_706, one_cost AS orange_707"
                                .", two_account AS orange_708, two_days AS orange_709, two_price AS orange_710, two_pblc_charge AS orange_711, two_own_charge AS orange_712, two_cost AS orange_713"
                                .", empty_one_one AS green_105, empty_one_two AS green_106"
                                .", three_account AS orange_714, three_days AS orange_715, three_price AS orange_716, three_pblc_charge AS orange_717, three_own_charge AS orange_718, three_cost AS orange_719"
                                .", empty_two_one AS green_107, empty_two_two AS green_108"
                                .", four_account AS orange_720, four_days AS orange_721, four_price AS orange_722, four_pblc_charge AS orange_723, four_own_charge AS orange_724, four_cost AS orange_725"
                                .", del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 6"
                          .") jbrirp06"
                      ." ON jbrpm.newest_apply_id = jbrirp06.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section"
                                .", total_account AS orange_726, total_days AS orange_727, total_price AS orange_728, total_ins_charge AS orange_729, total_pblc_charge AS orange_730, total_own_charge AS orange_731, total_cost AS orange_732"
                                .", one_account AS orange_733, one_days AS orange_734, one_price AS orange_735, one_pblc_charge AS orange_736, one_own_charge AS orange_737, one_cost AS orange_738"
                                .", two_account AS orange_739, two_days AS orange_740, two_price AS orange_741, two_pblc_charge AS orange_742, two_own_charge AS orange_743, two_cost AS orange_744"
                                .", empty_one_one AS green_109, empty_one_two AS green_110"
                                .", three_account AS orange_745, three_days AS orange_746, three_price AS orange_747, three_pblc_charge AS orange_748, three_own_charge AS orange_749, three_cost AS orange_750"
                                .", empty_two_one AS green_111, empty_two_two AS green_112"
                                .", four_account AS orange_751, four_days AS orange_752, four_price AS orange_753, four_pblc_charge AS orange_754, four_own_charge AS orange_755, four_cost AS orange_756"
                                .", del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 7"
                          .") jbrirp07"
                      ." ON jbrpm.newest_apply_id = jbrirp07.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section"
                                .", total_account AS red_424, total_days AS red_425, total_price AS red_426, total_ins_charge AS red_427, total_pblc_charge AS red_428, total_own_charge AS red_429, total_cost AS red_430"
                                .", one_account AS red_431, one_days AS red_432, one_price AS red_433, one_pblc_charge AS red_434, one_own_charge AS red_435, one_cost AS red_436"
                                .", two_account AS red_437, two_days AS red_438, two_price AS red_439, two_pblc_charge AS red_440, two_own_charge AS red_441, two_cost AS red_442"
                                .", empty_one_one AS green_113, empty_one_two AS green_114"
                                .", three_account AS red_443, three_days AS red_444, three_price AS red_445, three_pblc_charge AS red_446, three_own_charge AS red_447, three_cost AS red_448"
                                .", empty_two_one AS green_115, empty_two_two AS green_116"
                                .", four_account AS red_449, four_days AS red_450, four_price AS red_451, four_pblc_charge AS red_452, four_own_charge AS red_453, four_cost AS red_454"
                                .", del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 8"
                          .") jbrirp08"
                      ." ON jbrpm.newest_apply_id = jbrirp08.newest_apply_id ";
        return $this->getDataList($sql);
    }

    function getJnlBillRecuperationInsureRehabPrepayTotal($arr)
    {
        $facilityId = $this->getArrValueByKey('facility_id', $arr);
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);

        $sql = "SELECT jbrpm.newest_apply_id, jbrpm.emp_id, jbrpm.jnl_facility_id, jbrpm.jnl_status, jbrpm.regist_date, jbrpm.del_flg, jbrpm.year"
                   .", jbrpm.month"
                   .", red_455, red_456, red_457, red_458, red_459"
                   .", red_460, red_461, red_462, red_463, red_464, red_465, red_466, red_467, red_468, red_469"
                   .", red_470, red_471, red_472, red_473, red_474, red_475, red_476, red_477, red_478, red_479"
                   .", red_480, red_481, red_482, red_483, red_484, red_485, red_486, red_487, red_488, red_489"
                   .", red_490, red_491, red_492, red_493, red_494, red_495, red_496, red_497, red_498, red_499"
                   .", red_500, red_501, red_502, red_503, red_504, red_505, red_506, red_507, red_508, red_509"
                   .", red_510, red_511, red_512, red_513, red_514, red_515, red_516, red_517, red_518, red_519"
                   .", red_520, red_521, red_522, red_523, red_524, red_525, red_526, red_527, red_528, red_529"
                   .", red_530, red_531, red_532, red_533, red_534, red_535, red_536, red_537, red_538, red_539"
                   .", red_540, red_541, red_542, red_543, red_544, red_545, red_546, red_547"
                   .", green_117, green_118, green_119"
                   .", green_120, green_121, green_122, green_123, green_124, green_125, green_126, green_127, green_128"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                           .", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_bill_recuperation_prepay_main jbrpm"
                      ." WHERE jnl_status < 2 AND del_flg = 'f'";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'",             $facilityId); }

        $sql .=       ") jbrpm"
              ." INNER JOIN (SELECT newest_apply_id, type_section"
                                .", total_account AS red_455, total_days AS red_456, total_price AS red_457, total_ins_charge AS red_458"
                                .", total_pblc_charge AS red_459, total_own_charge AS red_460, total_cost AS red_461"
                                .", one_account AS red_462, one_days AS red_463, one_price AS red_464, one_pblc_charge AS red_465"
                                .", one_own_charge AS red_466, one_cost AS red_467"
                                .", two_account AS red_468, two_days AS red_469, two_price AS red_470, two_pblc_charge AS red_471"
                                .", two_own_charge AS red_472, two_cost AS red_473"
                                .", empty_one_one AS green_117, empty_one_two AS green_118"
                                .", three_account AS red_474, three_days AS red_475, three_price AS red_476, three_pblc_charge AS red_477"
                                .", three_own_charge AS red_478, three_cost AS red_479"
                                .", empty_two_one AS green_119, empty_two_two AS green_120"
                                .", four_account AS red_480, four_days AS red_481, four_price AS red_482, four_pblc_charge AS red_483"
                                .", four_own_charge AS red_484, four_cost AS red_485"
                                .", del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 9"
                          .") jbrirp09"
                      ." ON jbrpm.newest_apply_id = jbrirp09.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section"
                                .", total_account AS red_486, total_days AS red_487, total_price AS red_488, total_ins_charge AS red_489"
                                .", total_pblc_charge AS red_490, total_own_charge AS red_491, total_cost AS red_492"
                                .", one_account AS red_493, one_days AS red_494, one_price AS red_495, one_pblc_charge AS red_496"
                                .", one_own_charge AS red_497, one_cost AS red_498"
                                .", two_account AS red_499, two_days AS red_500, two_price AS red_501, two_pblc_charge AS red_502"
                                .", two_own_charge AS red_503, two_cost AS red_504"
                                .", empty_one_one AS green_121, empty_one_two AS green_122"
                                .", three_account AS red_505, three_days AS red_506, three_price AS red_507, three_pblc_charge AS red_508"
                                .", three_own_charge AS red_509, three_cost AS red_510"
                                .", empty_two_one AS green_123, empty_two_two AS green_124"
                                .", four_account AS red_511, four_days AS red_512, four_price AS red_513, four_pblc_charge AS red_514"
                                .", four_own_charge AS red_515, four_cost AS red_516"
                                .", del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 10"
                          .") jbrirp10"
                      ." ON jbrpm.newest_apply_id = jbrirp10.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section"
                                .", total_account AS red_517, total_days AS red_518, total_price AS red_519, total_ins_charge AS red_520"
                                .", total_pblc_charge AS red_521, total_own_charge AS red_522, total_cost AS red_523"
                                .", one_account AS red_524, one_days AS red_525, one_price AS red_526, one_pblc_charge AS red_527"
                                .", one_own_charge AS red_528, one_cost AS red_529"
                                .", two_account AS red_530, two_days AS red_531, two_price AS red_532, two_pblc_charge AS red_533"
                                .", two_own_charge AS red_534, two_cost AS red_535"
                                .", empty_one_one AS green_125, empty_one_two AS green_126"
                                .", three_account AS red_536, three_days AS red_537, three_price AS red_538, three_pblc_charge AS red_539"
                                .", three_own_charge AS red_540, three_cost AS red_541"
                                .", empty_two_one AS green_127, empty_two_two AS green_128"
                                .", four_account AS red_542, four_days AS red_543, four_price  AS red_544, four_pblc_charge  AS red_545"
                                .", four_own_charge AS red_546, four_cost AS red_547"
                                .", del_flg"
                            ." FROM jnl_bill_recuperation_insure_rehab_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 11"
                          .") jbrirp11"
                      ." ON jbrpm.newest_apply_id = jbrirp11.newest_apply_id";
        return $this->getDataList($sql);
    }

    function getJnlBillRecuperationServicePrepay($arr)
    {
        $rtnArr = array();
        $type0  = $this->getJnlBillRecuperationServicePrepayType0($arr);
        $type1  = $this->getJnlBillRecuperationServicePrepayType1($arr);

        $rtnArr = $this->arrayMerge($type0, $rtnArr);
        $rtnArr = $this->arrayMerge($type1, $rtnArr);

        return $rtnArr;
    }

    function getJnlBillRecuperationServicePrepayType0($arr)
    {
        $facilityId = $this->getArrValueByKey('facility_id', $arr);
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);

        $sql = "SELECT jbrpm.newest_apply_id, jbrpm.emp_id, jbrpm.jnl_facility_id, jbrpm.jnl_status, jbrpm.regist_date, jbrpm.del_flg"
                   .", orange_758, orange_759"
                   .", orange_760, orange_761, orange_762, orange_763, orange_764, orange_765, orange_766, orange_767, orange_768, orange_769"
                   .", orange_770, orange_771, orange_772, orange_773, orange_774, orange_775, orange_776, orange_777, orange_778, orange_779"
                   .", orange_780, orange_781"
                   .", red_549"
                   .", red_550, red_551, red_552, red_553, red_554, red_555, red_556"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                           .", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_bill_recuperation_prepay_main jbrpm"
                      ." WHERE jnl_status < 2 AND del_flg = 'f'";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'",             $facilityId); }

        $sql .=       ") jbrpm"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", house_acount AS orange_758, house_days AS orange_759, house_cost AS orange_760, house_ins AS orange_761"
                                .", meal_acount AS orange_762, meal_days AS orange_763, meal_cost AS orange_764, meal_ins AS orange_765"
                                .", del_flg"
                            ." FROM jnl_bill_recuperation_service_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 0 AND type_kind = 0"
                          .") jbrsp00"
                      ." ON jbrpm.newest_apply_id = jbrsp00.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", house_acount AS orange_766, house_days AS orange_767, house_cost AS orange_768, house_ins AS orange_769"
                                .", meal_acount AS orange_770, meal_days AS orange_771, meal_cost AS orange_772, meal_ins AS orange_773"
                                .", del_flg"
                            ." FROM jnl_bill_recuperation_service_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 0 AND type_kind = 1"
                          .") jbrsp01"
                      ." ON jbrpm.newest_apply_id = jbrsp01.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", house_acount AS orange_774, house_days AS orange_775, house_cost AS orange_776, house_ins AS orange_777"
                                .", meal_acount AS orange_778, meal_days AS orange_779, meal_cost AS orange_780, meal_ins AS orange_781"
                                .", del_flg"
                            ." FROM jnl_bill_recuperation_service_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 0 AND type_kind = 2"
                          .") jbrsp02"
                      ." ON jbrpm.newest_apply_id = jbrsp02.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", house_acount AS red_549, house_days AS red_550, house_cost AS red_551, house_ins AS red_552"
                                .", meal_acount AS red_553, meal_days AS red_554, meal_cost AS red_555, meal_ins AS red_556"
                                .", del_flg"
                            ." FROM jnl_bill_recuperation_service_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 0 AND type_kind = 3"
                          .") jbrsp03"
                      ." ON jbrpm.newest_apply_id = jbrsp03.newest_apply_id";
        return $this->getDataList($sql);
    }

    function getJnlBillRecuperationServicePrepayType1($arr)
    {
        $facilityId = $this->getArrValueByKey('facility_id', $arr);
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);

        $sql = "SELECT jbrpm.newest_apply_id, jbrpm.emp_id, jbrpm.jnl_facility_id, jbrpm.jnl_status, jbrpm.regist_date, jbrpm.del_flg"
                   .", orange_782, orange_783, orange_784, orange_785, orange_786, orange_787, orange_788, orange_789"
                   .", orange_790, orange_791, orange_792, orange_793, orange_794, orange_795, orange_796, orange_797, orange_798, orange_799"
                   .", orange_800, orange_801, orange_802, orange_803, orange_804, orange_805"
                   .", red_557, red_558, red_559"
                   .", red_560, red_561, red_562, red_563, red_564"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                           .", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_bill_recuperation_prepay_main jbrpm"
                      ." WHERE jnl_status < 2 AND del_flg = 'f'";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'",             $facilityId); }

        $sql .=       ") jbrpm"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", house_acount AS orange_782, house_days AS orange_783, house_cost AS orange_784, house_ins AS orange_785"
                                .", meal_acount AS orange_786, meal_days AS orange_787, meal_cost AS orange_788, meal_ins AS orange_789"
                                .", del_flg"
                            ." FROM jnl_bill_recuperation_service_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 0"
                          .") jbrsp10"
                      ." ON jbrpm.newest_apply_id = jbrsp10.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", house_acount AS orange_790, house_days AS orange_791, house_cost AS orange_792, house_ins AS orange_793"
                                .", meal_acount AS orange_794, meal_days AS orange_795, meal_cost AS orange_796, meal_ins AS orange_797"
                                .", del_flg"
                            ." FROM jnl_bill_recuperation_service_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 1"
                          .") jbrsp11"
                      ." ON jbrpm.newest_apply_id = jbrsp11.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", house_acount AS orange_798, house_days AS orange_799, house_cost AS orange_800, house_ins AS orange_801"
                                .", meal_acount AS orange_802, meal_days AS orange_803, meal_cost AS orange_804, meal_ins AS orange_805"
                                .", del_flg"
                            ." FROM jnl_bill_recuperation_service_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 2"
                          .") jbrs12"
                      ." ON jbrpm.newest_apply_id = jbrs12.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", house_acount AS red_557, house_days AS red_558, house_cost AS red_559, house_ins AS red_560"
                                .", meal_acount AS red_561, meal_days AS red_562, meal_cost AS red_563, meal_ins AS red_564"
                                .", del_flg"
                            ." FROM jnl_bill_recuperation_service_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 3"
                          .") jbrsp13"
                      ." ON jbrpm.newest_apply_id = jbrsp13.newest_apply_id";
        return $this->getDataList($sql);
    }

    function getJnlBillRecuperationOwnPaymentPrepay($arr)
    {
        $rtnArr = array();
        $visit  = $this->getJnlBillRecuperationOwnPaymentPrepayVisit($arr);
        $short  = $this->getJnlBillRecuperationOwnPaymentPrepayShort($arr);
        $other  = $this->getJnlBillRecuperationOwnPaymentPrepayOther($arr);

        $rtnArr = $this->arrayMerge($visit, $rtnArr);
        $rtnArr = $this->arrayMerge($short, $rtnArr);
        $rtnArr = $this->arrayMerge($other, $rtnArr);

        return $rtnArr;
    }

    function getJnlBillRecuperationOwnPaymentPrepayVisit($arr)
    {
        $facilityId = $this->getArrValueByKey('facility_id', $arr);
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);

        $sql = "SELECT jbrpm.newest_apply_id, jbrpm.emp_id, jbrpm.jnl_facility_id, jbrpm.jnl_status, jbrpm.regist_date, jbrpm.del_flg"
                   .", jbrpm.year, jbrpm.month"
                   .", orange_821, orange_822, orange_823"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                           .", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_bill_recuperation_prepay_main jbrpm"
                      ." WHERE jnl_status < 2 AND del_flg = 'f'";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'",             $facilityId); }

        $sql .=       ") jbrpm"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_821, days AS orange_822, money AS orange_823, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 0 AND type_kind = 0"
                          .") jbropp000"
                      ." ON jbrpm.newest_apply_id = jbropp000.newest_apply_id";
        return $this->getDataList($sql);
    }

    function getJnlBillRecuperationOwnPaymentPrepayShort($arr)
    {
        $facilityId = $this->getArrValueByKey('facility_id', $arr);
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);

        $sql = "SELECT jbrpm.newest_apply_id, jbrpm.emp_id, jbrpm.jnl_facility_id, jbrpm.jnl_status, jbrpm.regist_date, jbrpm.del_flg"
                   .", jbrpm.year, jbrpm.month"
                   .", orange_824, orange_825, orange_826, orange_827, orange_828, orange_829"
                   .", orange_830, orange_831, orange_832, orange_833, orange_834, orange_835, orange_836, orange_837, orange_838, orange_839"
                   .", orange_840, orange_841, orange_842, orange_843, orange_844, orange_845, orange_846, orange_847, orange_848, orange_849"
                   .", orange_850, orange_851, orange_852, orange_853, orange_854, orange_855, orange_856, orange_857, orange_858, orange_859"
                   .", red_570"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                           .", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_bill_recuperation_prepay_main jbrpm"
                      ." WHERE jnl_status < 2 AND del_flg = 'f'";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'",             $facilityId); }

        $sql .=       ") jbrpm"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_824, days AS orange_825, money AS orange_826, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 1"
                          .") jbropp101"
                      ." ON jbrpm.newest_apply_id = jbropp101.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_827, days AS orange_828, money AS orange_829, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 2"
                          .") jbropp102"
                      ." ON jbrpm.newest_apply_id = jbropp102.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_830, days AS orange_831, money AS orange_832, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 3"
                          .") jbropp103"
                      ." ON jbrpm.newest_apply_id = jbropp103.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_833, days AS orange_834, money AS orange_835, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 4"
                          .") jbropp104"
                      ." ON jbrpm.newest_apply_id = jbropp104.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_836, days AS orange_837, money AS orange_838, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 5"
                          .") jbropp105"
                      ." ON jbrpm.newest_apply_id = jbropp105.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_839, days AS orange_840, money AS orange_841, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 6"
                          .") jbropp106"
                      ." ON jbrpm.newest_apply_id = jbropp106.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_842, days AS orange_843, money AS orange_844, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 7"
                          .") jbropp107"
                      ." ON jbrpm.newest_apply_id = jbropp107.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_845, days AS orange_846, money AS orange_847, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 8"
                          .") jbropp108"
                      ." ON jbrpm.newest_apply_id = jbropp108.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_848, days AS orange_849, money AS orange_850, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 9"
                          .") jbropp109"
                      ." ON jbrpm.newest_apply_id = jbropp109.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_851, days AS orange_852, money AS orange_853, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 10"
                          .") jbropp110"
                      ." ON jbrpm.newest_apply_id = jbropp110.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_854, days AS orange_855, money AS orange_856, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 11"
                          .") jbropp111"
                      ." ON jbrpm.newest_apply_id = jbropp111.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind"
                                .", acount AS orange_857, days AS orange_858, money AS orange_859, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 12"
                          .") jbropp112"
                    ." ON jbrpm.newest_apply_id = jbropp112.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind, money AS red_570, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 1 AND type_kind = 13"
                          .") jbropp113"
                    ." ON jbrpm.newest_apply_id = jbropp113.newest_apply_id";
        return $this->getDataList($sql);
    }

    function getJnlBillRecuperationOwnPaymentPrepayOther($arr)
    {
        $facilityId = $this->getArrValueByKey('facility_id', $arr);
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);

        $sql = "SELECT jbrpm.newest_apply_id, jbrpm.emp_id, jbrpm.jnl_facility_id, jbrpm.jnl_status, jbrpm.regist_date, jbrpm.del_flg"
                   .", jbrpm.year, jbrpm.month"
                   .", orange_860, orange_861"
                   .", red_571"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                           .", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_bill_recuperation_prepay_main jbrpm"
                      ." WHERE jnl_status < 2 AND del_flg = 'f'";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'",             $facilityId); }

        $sql .=       ") jbrpm"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind, money AS orange_860, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 3 AND type_kind = 14"
                          .") jbropp314"
                      ." ON jbrpm.newest_apply_id = jbropp314.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind, money AS orange_861, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 4 AND type_kind = 15"
                          .") jbropp415"
                      ." ON jbrpm.newest_apply_id = jbropp415.newest_apply_id"
              ." INNER JOIN (SELECT newest_apply_id, type_section, type_kind, money AS red_571, del_flg"
                            ." FROM jnl_bill_recuperation_own_payment_prepay"
                           ." WHERE del_flg = 'f' AND type_section = 5 AND type_kind = 16"
                          .") jbropp516"
                      ." ON jbrpm.newest_apply_id = jbropp516.newest_apply_id";
        return $this->getDataList($sql);
    }

    function getJnlBillRecuperationEtcPrepay($arr)
    {
        $facilityId = $this->getArrValueByKey('facility_id', $arr);
        $year       = $this->getArrValueByKey('year',        $arr);
        $month      = $this->getArrValueByKey('month',       $arr);

        $sql = "SELECT jbrm.newest_apply_id, jbrm.emp_id, jbrm.jnl_facility_id, jbrm.jnl_status, jbrm.regist_date, jbrm.del_flg"
                   .", jbrm.year, jbrm.month"
                   .", orange_757"
                   .", orange_806, orange_807, orange_808, orange_809"
                   .", orange_810, orange_811, orange_812, orange_813, orange_814, orange_815, orange_816, orange_817, orange_818, orange_819"
                   .", orange_820"
                   .", red_360, red_361"
                   .", red_548, red_565, red_566, red_567, red_568, red_569"
                   .", red_572"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, del_flg"
                           .", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
                       ." FROM jnl_bill_recuperation_prepay_main jbrm"
                      ." WHERE jnl_status < 2 AND del_flg = 'f'";

        if (!is_null($year))       { $sql .= sprintf(" AND SUBSTR(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND SUBSTR(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%d'",             $facilityId); }

        $sql .=       ") jbrm"
              ." INNER JOIN (SELECT newest_apply_id"
                                .", support_total_acount AS red_565, support_total_price AS red_566, support_total_amount AS red_567"
                                .", support_now_acount AS orange_806, support_now_price AS orange_807, support_now_amount AS orange_808"
                                .", support_delay_acount AS orange_809, support_delay_price AS orange_810, support_delay_amount AS orange_811"
                                .", support_return_acount AS orange_812, support_return_price AS orange_813, support_return_amount AS orange_814"
                                .", rate_date_total_low_acount AS orange_815, rate_date_total_low_days AS orange_816, rate_date_total_total_days AS orange_817"
                                .", rate_date_total_rate AS red_568"
                                .", rate_date_short_low_acount AS orange_818, rate_date_short_low_days AS orange_819, rate_date_short_total_days AS orange_820"
                                .", rate_date_short_rate AS red_569"
                                .", all_total_charge AS red_360, ins_ins_total_charge AS red_361"
                                .", specific_enter_total_charge AS red_548"
                                .", ins_rehab_total_charge AS red_572"
                                .", improvement_grant AS orange_757"
                            ." FROM jnl_bill_recuperation_etc_prepay"
                           ." WHERE del_flg = 'f'"
                          .") jbrep"
                      ." ON jbrm.newest_apply_id = jbrep.newest_apply_id";
        return $this->getDataList($sql);
    }
}