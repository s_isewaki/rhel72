<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, 12, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック・登録値の編集
$year = date("Y", $date);
$month = date("m", $date);
$arr_content = array();
for ($i = 0; $i < $rows; $i++) {
	if ($day[$i] == "-" && (${"start_hrs$i"} == "--" || ${"start_hrs$i"} == "") && (${"start_min$i"} == "-" || ${"start_min$i"} == "") && (${"dur_hrs$i"} == "--" || ${"dur_hrs$i"} == "") && (${"dur_min$i"} == "-" || ${"dur_min$i"} == "") && $event_name[$i] == "" && $event_content[$i] == "") {
		continue;
	}

	if ($day[$i] == "-") {
		echo("<script type=\"text/javascript\">alert('日付が選択されていません。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
	if (${"time_flg$i"} == "" && (${"start_hrs$i"} == "--" || ${"start_min$i"} == "-" || ${"dur_hrs$i"} == "--" || ${"dur_min$i"} == "-")) {
		echo("<script type=\"text/javascript\">alert('時刻が不正です。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
	if (${"time_flg$i"} == "") {
		$tmp_start_time = "{${"start_hrs$i"}}:{${"start_min$i"}}";
		$tmp_end_time = "{${"dur_hrs$i"}}:{${"dur_min$i"}}";
		if ($tmp_start_time >= $tmp_end_time) {
			echo("<script type=\"text/javascript\">alert('終了時刻は開始時刻より後にしてください。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}
		$time_flg = "t";
	} else {
		$tmp_start_time = "08:00";
		$tmp_end_time = "20:00";
		$time_flg = "f";
	}
	if ($event_name[$i] == "") {
		echo("<script type=\"text/javascript\">alert('行事名が入力されていません。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
	if (strlen($event_name[$i]) > 100) {
		echo("<script type=\"text/javascript\">alert('行事名が長すぎます。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}

/* 2010/09/02 Tsuzuki Revised
	if ($event_content[$i] == "") {
		echo("<script type=\"text/javascript\">alert('詳細が入力されていません。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
*/

	if (strlen($event_content[$i]) > 400) {
		echo("<script type=\"text/javascript\">alert('詳細が長すぎます。');</script>\n");
		echo("<script type=\"text/javascript\">history.back();</script>\n");
		exit;
	}
	if (strlen($fcl_detail[$i]) > 50) {
		echo("<script type=\"text/javascript\">alert('施設詳細が長すぎます。');</script>\n");
		echo("<script type=\"text/javascript\">history.back();</script>\n");
		exit;
	}
	if (strlen($contact[$i]) > 100) {
		echo("<script type=\"text/javascript\">alert('連絡先が長すぎます。');</script>\n");
		echo("<script type=\"text/javascript\">history.back();</script>\n");
		exit;
	}

	$arr_content[] = array($event_name[$i], $event_content[$i], "$year$month{$day[$i]}", $tmp_start_time, $tmp_end_time, $time_flg, $fcl_detail[$i], $contact[$i]);
}
if (count($arr_content) == 0) {
	echo("<script type=\"text/javascript\">alert('登録対象がありません。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

if ($fcl_id == "-") {
	$fcl_id = null;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin transaction");

foreach ($arr_content as $content) {

	// 行事IDを採番
	$sql = "select max(event_id) from timegd";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$event_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

	// 行事情報を登録
	$sql = "insert into timegd (event_id, event_name, event_content, event_date, event_time, event_dur, time_flg, fcl_detail, contact, fcl_id) values (";
	array_unshift($content, $event_id);
	array_push($content, $fcl_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 行事一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'time_guide_list.php?session=$session&fcl_id=$fcl_id&date=$date&mode=display';</script>");
?>
