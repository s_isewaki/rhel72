<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<style type="text/css">
	th { text-align:left }
</style>
<?
require("about_session.php");
require("about_authority.php");
require("label_by_profile_type.ini");

$fname = $PHP_SELF;

$q1 = @$_REQUEST["q1"];
$q2 = @$_REQUEST["q2"];
$q3 = @$_REQUEST["q3"];
$q4_1 = @$_REQUEST["q4_1"];
$q4_2 = @$_REQUEST["q4_2"];
$q5 = @$_REQUEST["q5"];
$q6_1 = @$_REQUEST["q6_1"];
$q6_2 = @$_REQUEST["q6_2"];
$q7 = @$_REQUEST["q7"];
$q7_com = @$_REQUEST["q7_com"];
$q8_1 = @$_REQUEST["q8_1"];
$q8_2 = @$_REQUEST["q8_2"];



// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$checkauth = check_authority($session, 57, $fname);  // メドレポート
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
$checkauth = check_authority($session, 14, $fname);  // 病床管理
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 患者参照権限・入院来院履歴権限を取得
$pt_ref_auth = check_authority($session, 15, $fname);
$inout_auth = check_authority($session, 33, $fname);

// データベースに接続
$con = connect2db($fname);

//==============================
// ログ
//==============================
require_once("summary_common.ini");
summary_common_write_operate_log("access", $con, $fname, $session, "", "", $pt_id);


// 患者情報を取得
$sql = "select ptif_lt_kaj_nm, ptif_ft_kaj_nm from ptifmst";
$cond = "where ptif_id = '$pt_id' and ptif_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	require_once("summary_common.ini");
	summary_common_show_error_page_and_die();
}
$lt_kj_nm = pg_fetch_result($sel, 0, "ptif_lt_kaj_nm");
$ft_kj_nm = pg_fetch_result($sel, 0, "ptif_ft_kaj_nm");

// チェックリスト情報を取得
$sql = "select * from inptq";
$cond = "where ptif_id = '$pt_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if($sel == 0){
	pg_close($con);
	require_once("summary_common.ini");
	summary_common_show_error_page_and_die();
}
if (pg_num_rows($sel) > 0) {
	$q1 = pg_fetch_result($sel, 0, "inpt_q1");
	$q2 = pg_fetch_result($sel, 0, "inpt_q2");
	$q3 = pg_fetch_result($sel, 0, "inpt_q3");
	$q4_1 = pg_fetch_result($sel, 0, "inpt_q4_1");
	$q4_2 = pg_fetch_result($sel, 0, "inpt_q4_2");
	$q5 = pg_fetch_result($sel, 0, "inpt_q5");
	$q6_1 = pg_fetch_result($sel, 0, "inpt_q6_1");
	$q6_2 = pg_fetch_result($sel, 0, "inpt_q6_2");
	$q7 = pg_fetch_result($sel, 0, "inpt_q7");
	$q7_com = pg_fetch_result($sel, 0, "inpt_q7_com");
	$q8_1 = pg_fetch_result($sel, 0, "inpt_q8_1");
	$q8_2 = pg_fetch_result($sel, 0, "inpt_q8_2");
}
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 患者/利用者
$patient_title = $_label_by_profile["PATIENT"][$profile_type];

// 来院/来所
$come_title = $_label_by_profile["COME"][$profile_type];
// 入院/入所
$in_title = $_label_by_profile["IN"][$profile_type];
?>
<title>CoMedix <?
// メドレポート/ＰＯレポート(Problem Oriented)
echo ($_label_by_profile["MED_REPORT"][$profile_type]); ?> | <? echo($in_title); ?><? echo($patient_title); ?>チェックリスト</title>
</head>
<body>

<? $pager_param_addition = "&page=".@$page."&key1=".@$key1."&key2=".@$key2; ?>

<? summary_common_show_sinryo_top_header($session, $fname, "チェックリスト", "", $pt_id); ?>

<? summary_common_show_report_tab_menu($con, $fname, "チェックリスト", 0, $pt_id, $pager_param_addition); ?>


<table class="prop_table">
	<tr height="22">
		<th width="100px"><? echo ($patient_title); ?>ID</th>
		<td width="100px" style="text-align:center"><? echo($pt_id); ?></td>
		<th width="100px"><? echo ($patient_title); ?>氏名</th>
		<td width=""><? echo("$lt_kj_nm $ft_kj_nm"); ?></td>
	</tr>
</table>




<form name="inpt" action="inpatient_check_list_confirm.php" method="post">


<table class="prop_table" style="width:800px; margin-top:16px">
<tr>
<th width="50%">問1）&nbsp;<? echo($in_title); ?>時書類（<? echo($in_title); ?>案内・契約書・食事について・ご案内）</th>
<td width="50%"><label><input type="radio" name="q1" value="t" class="baseline" <? if($q1 == "t"){echo("checked");} ?>>渡した</label>&nbsp;<label><input type="radio" name="q1" value="f" class="baseline" <? if($q1 == "f"){echo("checked");} ?>>未</label></td>
</tr>
<tr>
<th>問2）&nbsp;プロフィール用紙</th>
<td><label><input type="radio" name="q2" value="t" class="baseline" <? if($q2 == "t"){echo("checked");} ?>>渡した</label>&nbsp;<label><input type="radio" name="q2" value="f" class="baseline" <? if($q2 == "f"){echo("checked");} ?>>未</label></td>
</tr>
<tr>
<th>問3）&nbsp;<? echo($in_title); ?>案内（手続き、携帯品）</th>
<td><label><input type="radio" name="q3" value="t" class="baseline" <? if($q3 == "t"){echo("checked");} ?>>説明済み</label>&nbsp;<label><input type="radio" name="q3" value="f" class="baseline" <? if($q3 == "f"){echo("checked");} ?>>未</label></td>
</tr>
<tr>
<th>問4）&nbsp;承諾書</th>
<td><label><input type="radio" name="q4_1" value="t" class="baseline" <? if($q4_1 == "t"){echo("checked");} ?>>有</label>&nbsp;<label><input type="radio" name="q4_1" value="f" class="baseline" <? if($q4_1 == "f"){echo("checked");} ?>>無</label>（<label><input type="radio" name="q4_2" value="t" class="baseline" <? if($q4_2 == "t"){echo("checked");} ?>>手渡し済</label>&nbsp;<label><input type="radio" name="q4_2" value="f" class="baseline" <? if($q4_2 == "f"){echo("checked");} ?>>未</label>）</td>
</tr>
<tr>
<th>問5）&nbsp;［手術・内視鏡］伝票の記入</th>
<td><label><input type="radio" name="q5" value="t" class="baseline" <? if($q5 == "t"){echo("checked");} ?>>済</label>&nbsp;<label><input type="radio" name="q5" value="f" class="baseline" <? if($q5 == "f"){echo("checked");} ?>>未</label></td>
</tr>
<tr>
<th>問6）&nbsp;麻酔科からのお願い</th>
<td><label><input type="radio" name="q6_1" value="t" class="baseline" <? if($q6_1 == "t"){echo("checked");} ?>>有</label>&nbsp;<label><input type="radio" name="q6_1" value="f" class="baseline" <? if($q6_1 == "f"){echo("checked");} ?>>無</label>（<label><input type="radio" name="q6_2" value="t" class="baseline" <? if($q6_2 == "t"){echo("checked");} ?>>手渡し済</label>&nbsp;<label><input type="radio" name="q6_2" value="f" class="baseline" <? if($q6_2 == "f"){echo("checked");} ?>>未</label>）</td>
</tr>
<tr>
<th>問7）&nbsp;Dr.より［入院・手術・検査］の説明</th>
<td><label><input type="radio" name="q7" value="t" class="baseline" <? if($q7 == "t"){echo("checked");} ?>>有</label>（本人・家族：<input type="text" name="q7_com" value="<? echo($q7_com); ?>">）&nbsp;<label><input type="radio" name="q7" value="f" class="baseline" <? if($q7 == "f"){echo("checked");} ?>>無</label></td>
</tr>
<tr>
<th>問8）&nbsp;HIV感染検査のお願い</th>
<td><label><input type="radio" name="q8_1" value="t" class="baseline" <? if($q8_1 == "t"){echo("checked");} ?>>有</label>&nbsp;<label><input type="radio" name="q8_1" value="f" class="baseline" <? if($q8_1 == "f"){echo("checked");} ?>>無</label>（<label><input type="radio" name="q8_2" value="t" class="baseline" <? if($q8_2 == "t"){echo("checked");} ?>>手渡し済</label>&nbsp;<label><input type="radio" name="q8_2" value="f" class="baseline" <? if($q8_2 == "f"){echo("checked");} ?>>未</label>）</td>
</tr>
</table>




<div class="search_button_div" style="width:800px">
	<input type="button" onclick="document.inpt.submit();" value="登録"/>
</div>


<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
<input type="hidden" name="path" value="M">
<input type="hidden" name="enti" value="1">
</form>
</body>
<? pg_close($con); ?>
</html>
