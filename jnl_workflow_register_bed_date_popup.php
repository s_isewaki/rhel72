<?
ob_start();  // META要素が出力されるのを防ぐ
require("./about_session.php");
require("./about_authority.php");
require_once("about_postgres.php");
require("./get_values.ini");
ob_clean();

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// セッションIDから職員IDを取得
$con = connect2db($fname);
$emp_id = get_emp_id($con, $session, $fname);

//初期化
$disp_title = "";

$record_date = "(".substr($date,0,4)."年".substr($date,4,2)."月".substr($date,6,2)."日付け)";


if($type=="1")
{
	//病院（病床数）の表示
	$sql = "select 	jnl_facility_id, assigned_date ,
			ip_pmt  ,ip_act  ,
			shog_pmt  ,shog_act  ,
			seip_pmt  ,seip_act  ,
			ir_pmt  ,ir_act  ,
			kaig_pmt  ,kaig_act  ,
			kaif_pmt  ,kaif_act  ,
			ak_pmt  ,ak_act  ,
			kan_pmt  ,kan_act  ,
			icu_pmt  ,icu_act  ,
			shoni_pmt  ,shoni_act  ,
			seir_pmt  ,seir_act  ,
			tok_pmt  ,tok_act  ,
			nin_pmt  ,nin_act  from jnl_hospital_bed ";
	$cond = "where jnl_facility_id='".$fc_id."' and assigned_date='".$date."'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0)
	{
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//初期表示用に取得したデータの最新日付のものを配列にセット
	$hospital_bed_data = array();

	$hos_bed['ip_pmt'] = pg_result($sel,0,"ip_pmt");
	$hos_bed['ip_act'] = pg_result($sel,0,"ip_act");
	$hos_bed['shog_pmt'] = pg_result($sel,0,"shog_pmt");
	$hos_bed['shog_act'] = pg_result($sel,0,"shog_act");
	$hos_bed['seip_pmt'] = pg_result($sel,0,"seip_pmt");
	$hos_bed['seip_act'] = pg_result($sel,0,"seip_act");
	$hos_bed['ir_pmt'] = pg_result($sel,0,"ir_pmt");
	$hos_bed['ir_act'] = pg_result($sel,0,"ir_act");
	$hos_bed['kaig_pmt'] = pg_result($sel,0,"kaig_pmt");
	$hos_bed['kaig_act'] = pg_result($sel,0,"kaig_act");
	$hos_bed['kaif_pmt'] = pg_result($sel,0,"kaif_pmt");
	$hos_bed['kaif_act'] = pg_result($sel,0,"kaif_act");
	$hos_bed['ak_pmt'] = pg_result($sel,0,"ak_pmt");
	$hos_bed['ak_act'] = pg_result($sel,0,"ak_act");
	$hos_bed['kan_pmt'] = pg_result($sel,0,"kan_pmt");
	$hos_bed['kan_act'] = pg_result($sel,0,"kan_act");
	$hos_bed['icu_pmt'] = pg_result($sel,0,"icu_pmt");
	$hos_bed['icu_act'] = pg_result($sel,0,"icu_act");
	$hos_bed['shoni_pmt'] = pg_result($sel,0,"shoni_pmt");
	$hos_bed['shoni_act'] = pg_result($sel,0,"shoni_act");
	$hos_bed['seir_pmt'] = pg_result($sel,0,"seir_pmt");
	$hos_bed['seir_act'] = pg_result($sel,0,"seir_act");
	$hos_bed['tok_pmt'] = pg_result($sel,0,"tok_pmt");
	$hos_bed['tok_act'] = pg_result($sel,0,"tok_act");
	$hos_bed['nin_pmt'] = pg_result($sel,0,"nin_pmt");
	$hos_bed['nin_act'] = pg_result($sel,0,"nin_act");

	$disp_title = "病院設定病床数";



}
else
{
	//老人保険施設（通所）の表示

	// 指定老人保健施設・日付の通所数を配列に格納
	$sql = "select 	jnl_facility_id, assigned_date ,wk_limit, st_limit, sn_limit, enter_limit, wk_regard, st_regard, sn_regard, ann_regard from jnl_nurse_home_bed ";
	$cond = "where jnl_facility_id='".$fc_id."' and assigned_date='".$date."'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0)
	{
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}



	//初期表示用に取得したデータの最新日付のものを配列にセット
	$nhome_bed = array();

	$nhome_bed['wk_limit'] = pg_result($sel,0,"wk_limit");
	$nhome_bed['st_limit'] = pg_result($sel,0,"st_limit");
	$nhome_bed['sn_limit'] = pg_result($sel,0,"sn_limit");

	$nhome_bed['enter_limit'] = pg_result($sel,0,"enter_limit");

	$nhome_bed['cfg_wk_flg'] = pg_result($sel,0,"wk_regard");
	$nhome_bed['cfg_sat_flg'] = pg_result($sel,0,"st_regard");
	$nhome_bed['cfg_sun_flg'] = pg_result($sel,0,"sn_regard");
	$nhome_bed['cfg_ann_flg'] = pg_result($sel,0,"ann_regard");

	$disp_title = "老人保健施設通所数";
}



pg_close($con);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<script type="text/javascript" src="js/fontsize.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
        <TITLE>CoMedix <?=$disp_title?></TITLE>
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<style type="text/css">
		.list {border-collapse:collapse;}
		.list td {border:#5279a5 solid 1px;}
		.list td td {border-style:none;}
		</style>
    </HEAD>



<?
if($type=="1")
{
	//病院用表示

?>


	<table border="0" cellspacing="0" cellpadding="2" class="list">
		<tr height="22">
			<td  bgcolor="C8D5FF" align="center" colspan="3">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>病床数設定<?=$record_date?></b></font>
			</td>
		</tr>

		<tr>
			<td bgcolor="C8D5FF" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>一般病棟</b></font>
			</td>
			<td bgcolor="#f6f9ff" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定床数（許可病床数）：</font>
				<input type="text" name="bed_pmt1" id="bed_pmt1" value="<?= $hos_bed['ip_pmt']?>" size="6"  maxlength=5 ReadOnly>
			</td>
			<td bgcolor="#f6f9ff" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">稼動病床数：</font>
				<input type="text" name="bed_act1" id="bed_act1" value="<?= $hos_bed['ip_act']?>" size="6"  maxlength=5 ReadOnly >
			</td>
		</tr>

		<tr height="22">
			<td bgcolor="C8D5FF" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>障害者病棟</b></font>
			</td>
			<td bgcolor="#f6f9ff" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定床数（許可病床数）：</font>
				<input type="text" name="bed_pmt2" id="bed_pmt2" value="<?= $hos_bed['shog_pmt']?>" size="6"  maxlength=5 ReadOnly>
			</td>
			<td bgcolor="#f6f9ff" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">稼動病床数：</font>
				<input type="text" name="bed_act2" id="bed_act2" value="<?= $hos_bed['shog_act']?>" size="6"  maxlength=5 ReadOnly >
			</td>
		</tr>

		<tr height="22">
			<td bgcolor="C8D5FF" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>精神一般病棟</b></font>
			</td>
			<td bgcolor="#f6f9ff" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定床数（許可病床数）：</font>
				<input type="text" name="bed_pmt3" id="bed_pmt3" value="<?= $hos_bed['seip_pmt']?>" size="6"  maxlength=5 ReadOnly >
			</td>
			<td bgcolor="#f6f9ff" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">稼動病床数：</font>
				<input type="text" name="bed_act3" id="bed_act3" value="<?= $hos_bed['seip_act']?>" size="6"  maxlength=5 ReadOnly >
			</td>
		</tr>

		<tr height="22">
			<td bgcolor="C8D5FF" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>医療療養病棟</b></font>
			</td>
			<td bgcolor="#f6f9ff" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定床数（許可病床数）：</font>
				<input type="text" name="bed_pmt4" id="bed_pmt4" value="<?= $hos_bed['ir_pmt']?>" size="6"  maxlength=5 ReadOnly >
			</td>
			<td bgcolor="#f6f9ff" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">稼動病床数：</font>
				<input type="text" name="bed_act4" id="bed_act4" value="<?= $hos_bed['ir_act']?>" size="6"  maxlength=5 ReadOnly >
			</td>
		</tr>

		<tr height="22">
			<td bgcolor="C8D5FF" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>介護療養病棟</b></font>
			</td>
			<td bgcolor="#f6f9ff" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定床数（許可病床数）：</font>
				<input type="text" name="bed_pmt5" id="bed_pmt5" value="<?= $hos_bed['kaig_pmt']?>" size="6"  maxlength=5 ReadOnly >
			</td>
			<td bgcolor="#f6f9ff" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">稼動病床数：</font>
				<input type="text" name="bed_act5" id="bed_act5" value="<?= $hos_bed['kaig_act']?>" size="6"  maxlength=5 ReadOnly >
			</td>
		</tr>

		<tr height="22">
			<td bgcolor="C8D5FF" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>回復期リハ病棟</b></font>
			</td>
			<td bgcolor="#f6f9ff" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定床数（許可病床数）：</font>
				<input type="text" name="bed_pmt6" id="bed_pmt6" value="<?= $hos_bed['kaif_pmt']?>" size="6"  maxlength=5 ReadOnly >
			</td>
			<td bgcolor="#f6f9ff" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">稼動病床数：</font>
				<input type="text" name="bed_act6" id="bed_act6" value="<?= $hos_bed['kaif_act']?>" size="6"  maxlength=5 ReadOnly >
			</td>
		</tr>


		<tr height="22">
			<td bgcolor="C8D5FF" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>亜急性期病棟</b></font>
			</td>
			<td bgcolor="#f6f9ff" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定床数（許可病床数）：</font>
				<input type="text" name="bed_pmt7" id="bed_pmt7" value="<?= $hos_bed['ak_pmt']?>" size="6"  maxlength=5 ReadOnly >
			</td>
			<td bgcolor="#f6f9ff" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">稼動病床数：</font>
				<input type="text" name="bed_act7" id="bed_act7" value="<?= $hos_bed['ak_act']?>" size="6"  maxlength=5 ReadOnly >
			</td>
		</tr>

		<tr height="22">
			<td bgcolor="C8D5FF" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>緩和ケア病棟</b></font>
			</td>
			<td bgcolor="#f6f9ff" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定床数（許可病床数）：</font>
				<input type="text" name="bed_pmt8" id="bed_pmt8" value="<?= $hos_bed['kan_pmt']?>" size="6"  maxlength=5 ReadOnly >
			</td>
			<td bgcolor="#f6f9ff" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">稼動病床数：</font>
				<input type="text" name="bed_act8" id="bed_act8" value="<?= $hos_bed['kan_act']?>" size="6"  maxlength=5 ReadOnly >
			</td>
		</tr>

		<tr height="22">
			<td bgcolor="C8D5FF" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>ICU及びハイケアユニット</b></font>
			</td>
			<td bgcolor="#f6f9ff"  align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定床数（許可病床数）：</font>
				<input type="text" name="bed_pmt9" id="bed_pmt9" value="<?= $hos_bed['icu_pmt']?>" size="6"  maxlength=5 ReadOnly >
			</td>
			<td bgcolor="#f6f9ff"  align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">稼動病床数：</font>
				<input type="text" name="bed_act9" id="bed_act9" value="<?= $hos_bed['icu_act']?>" size="6"  maxlength=5 ReadOnly >
			</td>
		</tr>

		<tr height="22">
			<td bgcolor="C8D5FF" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>小児管理料</b></font>
			</td>
			<td bgcolor="#f6f9ff"  align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定床数（許可病床数）：</font>
				<input type="text" name="bed_pmt10" id="bed_pmt10" value="<?= $hos_bed['shoni_pmt']?>" size="6"  maxlength=5 ReadOnly >
			</td>
			<td bgcolor="#f6f9ff"  align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">稼動病床数：</font>
				<input type="text" name="bed_act10" id="bed_act10" value="<?= $hos_bed['shoni_act']?>" size="6"  maxlength=5 ReadOnly >
			</td>
		</tr>

		<tr height="22">
			<td bgcolor="C8D5FF" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>精神医療病棟</b></font>
			</td>
			<td bgcolor="#f6f9ff"  align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定床数（許可病床数）：</font>
				<input type="text" name="bed_pmt11" id="bed_pmt11" value="<?= $hos_bed['seir_pmt']?>" size="6"  maxlength=5 ReadOnly >
			</td>
			<td bgcolor="#f6f9ff"  align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">稼動病床数：</font>
				<input type="text" name="bed_act11" id="bed_act11" value="<?= $hos_bed['seir_act']?>" size="6"  maxlength=5 ReadOnly >
			</td>
		</tr>

		<tr height="22">
			<td bgcolor="C8D5FF" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>特殊疾患(2)</b></font>
			</td>
			<td bgcolor="#f6f9ff" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定床数（許可病床数）：</font>
				<input type="text" name="bed_pmt12" id="bed_pmt12" value="<?= $hos_bed['tok_pmt']?>" size="6"  maxlength=5 ReadOnly >
			</td>
			<td bgcolor="#f6f9ff" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">稼動病床数：</font>
				<input type="text" name="bed_act12" id="bed_act12" value="<?= $hos_bed['tok_act']?>" size="6"  maxlength=5 ReadOnly >
			</td>
		</tr>

		<tr height="22">
			<td bgcolor="C8D5FF" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>認知症病棟</b></font>
			</td>
			<td bgcolor="#f6f9ff" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定床数（許可病床数）：</font>
				<input type="text" name="bed_pmt13" id="bed_pmt13" value="<?= $hos_bed['nin_pmt']?>" size="6"  maxlength=5 ReadOnly >
			</td>
			<td bgcolor="#f6f9ff" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">稼動病床数：</font>
				<input type="text" name="bed_act13" id="bed_act13" value="<?= $hos_bed['nin_act']?>" size="6"  maxlength=5 ReadOnly >
			</td>
		</tr>

		<tr height="22">
			<td bgcolor="#f6f9ff" align="right" colspan="3">
			<input type="button" value="閉じる" onClick="window.close()">
			</td>
		</tr>
	</table>


<?

}
else
{
	//老人保健施設の表示
?>

	<table border="0" cellspacing="0" cellpadding="2" class="list">
		<tr height="22">
			<td  bgcolor="C8D5FF" align="center" colspan="4">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b><?=$record_date?></b></font>
			</td>
		</tr>

		<tr>
			<td bgcolor="C8D5FF" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>通所設定</b></font>
			</td>
			<td bgcolor="FFE9C8" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平日：</font>
				<input type="text" name="wk_limit" id="wk_limit" value="<?= $nhome_bed['wk_limit']?>" size="6"  maxlength=5 ReadOnly>
			</td>
			<td bgcolor="C8FCFF" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">土曜：</font>
				<input type="text" name="st_limit" id="st_limit" value="<?= $nhome_bed['st_limit']?>" size="6"  maxlength=5 ReadOnly >
			</td>
			<td bgcolor="FFD7D7" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日曜：</font>
				<input type="text" name="sn_limit" id="sn_limit" value="<?= $nhome_bed['sn_limit']?>" size="6"  maxlength=5 ReadOnly >
			</td>
		</tr>

		<tr>
			<td  bgcolor="C8D5FF" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>入所定数</b></font>
			</td>
			<td bgcolor="B9FF8E" align="right">
				<input type="text" name="enter_limit" id="enter_limit" style="text-align:right;" ReadOnly value="<?= $nhome_bed['enter_limit']?>" size="12"  maxlength=5 >
			</td>
			<td align="right" colspan="2"></td>
		</tr>

		<tr height="22">
			<td  bgcolor="C8D5FF" align="center" colspan="4">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>業務曜日設定</b></font>
			</td>
		</tr>

		<tr>
			<td  bgcolor="C8D5FF" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>平日（月曜〜金曜）</b></font>
			</td>
			<td bgcolor="FFE9C8" align="right">
<input type="radio" name="cfg_wk_flg" id="cfg_wk_flg" value="0"<? if ($nhome_bed['cfg_wk_flg'] == "0") {echo(" checked");} ?>>
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平日扱い</font>
			</td>
			<td bgcolor="C8FCFF" align="right">
<input type="radio" name="cfg_wk_flg" id="cfg_wk_flg" value="1"<? if ($nhome_bed['cfg_wk_flg'] == "1") {echo(" checked");} ?>>
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">土曜扱い</font>
			</td>
			<td bgcolor="FFD7D7" align="right">
<input type="radio" name="cfg_wk_flg" id="cfg_wk_flg" value="2"<? if ($nhome_bed['cfg_wk_flg'] == "2") {echo(" checked");} ?>>
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日曜扱い</font>
			</td>
		</tr>

		<tr>
			<td  bgcolor="C8D5FF" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>土曜日</b></font>
			</td>
			<td bgcolor="FFE9C8" align="right">
<input type="radio" name="cfg_sat_flg" id="cfg_sat_flg" value="0"<? if ($nhome_bed['cfg_sat_flg'] == "0") {echo(" checked");} ?>>
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平日扱い</font>
			</td>
			<td bgcolor="C8FCFF" align="right">
<input type="radio" name="cfg_sat_flg" id="cfg_sat_flg" value="1"<? if ($nhome_bed['cfg_sat_flg'] == "1") {echo(" checked");} ?>>
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">土曜扱い</font>
			</td>
			<td bgcolor="FFD7D7" align="right">
<input type="radio" name="cfg_sat_flg" id="cfg_sat_flg" value="2"<? if ($nhome_bed['cfg_sat_flg'] == "2") {echo(" checked");} ?>>
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日曜扱い</font>
			</td>
		</tr>


		<tr>
			<td  bgcolor="C8D5FF" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>日曜日</b></font>
			</td>
			<td bgcolor="FFE9C8" align="right">
<input type="radio" name="cfg_sun_flg" id="cfg_sun_flg" value="0"<? if ($nhome_bed['cfg_sun_flg'] == "0") {echo(" checked");} ?>>
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平日扱い</font>
			</td>
			<td bgcolor="C8FCFF" align="right">
<input type="radio" name="cfg_sun_flg" id="cfg_sun_flg" value="1"<? if ($nhome_bed['cfg_sun_flg'] == "1") {echo(" checked");} ?>>
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">土曜扱い</font>
			</td>
			<td bgcolor="FFD7D7" align="right">
<input type="radio" name="cfg_sun_flg" id="cfg_sun_flg" value="2"<? if ($nhome_bed['cfg_sun_flg'] == "2") {echo(" checked");} ?>>
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日曜扱い</font>
			</td>
		</tr>

		<tr>
			<td  bgcolor="C8D5FF" align="right">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>祝日</b></font>
			</td>
			<td bgcolor="FFE9C8" align="right">
<input type="radio" name="cfg_ann_flg" id="cfg_ann_flg" value="0"<? if ($nhome_bed['cfg_ann_flg'] == "0") {echo(" checked");} ?>>
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平日扱い</font>
			</td>
			<td bgcolor="C8FCFF" align="right">
<input type="radio" name="cfg_ann_flg" id="cfg_ann_flg" value="1"<? if ($nhome_bed['cfg_ann_flg'] == "1") {echo(" checked");} ?>>
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">土曜扱い</font>
			</td>
			<td bgcolor="FFD7D7" align="right">
<input type="radio" name="cfg_ann_flg" id="cfg_ann_flg" value="2"<? if ($nhome_bed['cfg_ann_flg'] == "2") {echo(" checked");} ?>>
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日曜扱い</font>
			</td>
		</tr>



		<tr height="22">
			<td bgcolor="#f6f9ff" align="right" colspan="4">
			<input type="button" value="閉じる" onClick="window.close()">
			</td>
		</tr>
	</table>


<?

}
?>

</HTML>
