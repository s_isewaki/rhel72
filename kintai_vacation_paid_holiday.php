<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?php
require_once("settings_common.php"); // 年次有給休暇簿 20131126
$wk_this_title = get_settings_value("kintai_vacation_paid_hol_title", "年次休暇簿");
?>
<title>CoMedix 出勤表 | <?php echo $wk_this_title; ?></title>
<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_select_values.ini");
require_once("date_utils.php");
require_once("show_timecard_common.ini");
require_once("timecard_paid_hol_hour_class.php");
require_once("atdbk_menu_common.ini");
require_once("work_admin_menu_common.ini");
require_once("referer_common.ini");

require_once './common_log_class.ini';
require_once './kintai/common/mdb_wrapper.php'; // MDB本体をWrap
require_once './kintai/common/mdb.php'; 		// DBアクセスユーティリティ
require_once './kintai/common/user_info.php';	// ユーザ情報
require_once './kintai/common/master_util.php';	// マスタ
require_once './kintai/common/exception.php';
require_once './kintai/common/date_util.php';
require_once './kintai/common/code_util.php';

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}
//フォームデータ取得
$umode 		= $_REQUEST["umode"];
$selEmpID	= $_REQUEST["selEmpID"];
// DBコネクション作成（既存用）
$con = connect2db($fname);

if($umode == "usr" || $mmode == "usr"){
    // 権限のチェック
    $checkauth = check_authority($session, 5, $fname);
    if ($checkauth == "0") {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
        exit;
    }
}else if($umode == "adm" || $umode == "adm2"){
    // 勤務管理権限チェック
    $checkauth = check_authority($session, 42, $fname);
    if ($checkauth == "0") {
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showLoginPage(window);</script>");
        exit;
    }
}
elseif ($umode == "duty" || $umode == "duty2") {
    //シフト
    require_once("duty_shift_common_class.php");
    $obj = new duty_shift_common_class($con, $fname);
    //ユーザ画面用
    $chk_flg = $obj->check_authority_user($session, $fname);
    if ($chk_flg == "") {
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showLoginPage(window);</script>");
        exit;
    }
    //管理画面用
    $section_admin_auth = $obj->check_authority_Management($session, $fname);
}
elseif ($umode == "apply") {
    //決裁・申請
    ;
}
else{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

$work_admin_auth = check_authority($session, 42, $fname);

//データアクセス
try {

	MDBWrapper::init(); // おまじない
	$log = new common_log_class(basename(__FILE__));
	$db = new MDB($log); // ログはnullでも可。指定するとどのPHPからMDBにアクセスした際のエラーかわかる

	$db->beginTransaction(); // トランザクション開始

	//部署階層数
	$class_cnt = MasterUtil::get_organization_level_count();

	//職員ID・氏名を取得
	$ukey  = "";
	$uType = "";
    if($umode == "usr"){
        $ukey  = $session;
        $uType = "s";
    }else if($umode == "adm" || $umode == "duty" || $umode == "apply" || $umode == "duty2" || $umode == "adm2"){
		if($selEmpID != ""){
			$ukey  = $selEmpID;
			$uType = "u";
		}
	}
	$user_info  = UserInfo::get($ukey, $uType);
	$base_info  = $user_info->get_base_info();
	$job_info   = $user_info->get_job_info();
	$dept_info  = $user_info->get_dept_info();
	$croom_info = $user_info->get_class_room_info();
	$cond_info = $user_info->get_emp_condition();

	//所属
	if($class_cnt == 4 && $croom_info->room_nm != ""){
		$shozoku = $croom_info->room_nm;
	}else{
		$shozoku = $dept_info->dept_nm;
	}
    $db->endTransaction(); // トランザクション終了

} catch (BusinessException $bex) {
    $message = $bex->getMessage();
    echo("<script type='text/javascript'>alert('".$message."');</script>");
    echo("<script language='javascript'>history.back();</script>");
    exit;
} catch (FatalException $fex) {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showErrorPage(window);</script>");
    exit;
}

//時間有休
$obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
$obj_hol_hour->select();


$arr_data = array();
$wk_carry = "";
$wk_add = "";
$arr_emppaid = array();
$arr_emppaid["prev_idx"] = -1;
$arr_emppaid["next_idx"] = -1;
//

if ($base_info->emp_id != "") {
    $paid_hol_add_date = "";
    $arr_emppaid = get_emppaid_info($con, $fname, $base_info->emp_id, $add_date);

    $curr_idx = $arr_emppaid["curr_idx"];
    if ($curr_idx >= 0) {
        $paid_hol_add_date = $arr_emppaid[$curr_idx]["year"].$arr_emppaid[$curr_idx]["paid_hol_add_mmdd"];
        //繰越
        $wk_carry = $arr_emppaid[$curr_idx]["days1"];
        //有休付与
        $wk_add = $arr_emppaid[$curr_idx]["days2"];

        //調整日数
        $wk_adjust_day = $arr_emppaid[$curr_idx]["adjust_day"];

        $wk_total = $wk_carry + $wk_add + $wk_adjust_day;
        //繰越時間
        $wk_carry_time_minute = $arr_emppaid[$curr_idx]["carry_time_minute"];
        //$wk_carry_time = minute_to_hmm($wk_carry_time_minute);
        // レイアウト修正 20130913
        $wk_carry_time = minute_to_hmm($wk_carry_time_minute);
    }

    if ($paid_hol_add_date != "") {

        $date_y1 = substr($paid_hol_add_date, 0, 4);
        $date_m1 = substr($paid_hol_add_date, 4, 2);
        $date_d1 = substr($paid_hol_add_date, 6, 2);
        //終了年月日計算
        //
        $next_idx = $arr_emppaid["next_idx"];
        if ($next_idx >= 0) {
            $date_y2 = $arr_emppaid[$next_idx]["year"];
            $date_m2 = substr($arr_emppaid[$next_idx]["paid_hol_add_mmdd"], 0, 2);
            $date_d2 = substr($arr_emppaid[$next_idx]["paid_hol_add_mmdd"], 2, 2);
            $edYMD = mktime(0, 0, 0, intval($date_m2), intval($date_d2), (intval($date_y2)));	//
            $edYMD = mktime(0, 0, 0, date("n", $edYMD), date("j", $edYMD) - 1, date("Y", $edYMD));	//１日マイナス
            $date_y2 = date("Y", $edYMD);
            $date_m2 = date("m", $edYMD);
            $date_d2 = date("d", $edYMD);
        }
        else {
            $edYMD = mktime(0, 0, 0, intval($date_m1), intval($date_d1), (intval($date_y1) + 1));	//１年プラス
            $edYMD = mktime(0, 0, 0, date("n", $edYMD), date("j", $edYMD) - 1, date("Y", $edYMD));	//１日マイナス
            $date_y2 = date("Y", $edYMD);
            $date_m2 = date("m", $edYMD);
            $date_d2 = date("d", $edYMD);
        }

        $arr_data = $obj_hol_hour->get_paid_hol_list($base_info->emp_id, $paid_hol_add_date, $date_y2.$date_m2.$date_d2);

    }
}

//所定時間
$day1_time = $obj_hol_hour->get_specified_time_calendar();
$wk_minute = $obj_hol_hour->get_specified_time($base_info->emp_id, $day1_time);
$specified_time = minute_to_hmm($wk_minute);
?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
</style>
<script type="text/javascript">
var childwin;
function openEmployeeList(item_id) {
	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
<?php
//管理画面側の場合
if ($mmode != "usr") { ?>
	var url = './emplist_popup.php';
<? }
//ユーザ側で担当者設定がある場合
else { ?>
	var url = './timecard_emp_search.php';
<? } ?>
	url += '?session=' + document.frmKintai.session.value;
	url += '&emp_id=';
	url += '&item_id='+item_id;

	childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
	childwin.focus();
}
function add_target_list(item_id, select_emp_id, name) {
	var list = select_emp_id.split(",");

	if (list.length > 1) {
		alert("複数の職員が選択されています。");
		return false;
	}
	childwin.close();
	document.frmKintai.selEmpID.value = select_emp_id;
	document.frmKintai.action = "./kintai_vacation_paid_holiday.php";
	document.frmKintai.method="POST";
	document.frmKintai.submit();

}


function print_xls(){

	document.frmKintai.action = "./kintai_vacation_paid_holiday_xls.php";
	document.frmKintai.method="POST";
	document.frmKintai.submit();

}
</script>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<form name="frmKintai" method="post">
<?php
if ($umode == "duty") {
    require_once("duty_shift_common.ini");
    require_once("duty_shift_user_tab_common.ini");
?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
    <?
    // 画面遷移
    echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
    show_user_title($session, $section_admin_auth);			//duty_shift_common.ini
    echo("</table>\n");

    // タブ
    $arr_option = "&group_id=" . $group_id;
    echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
    show_user_tab_menuitem($session, $fname, $arr_option);	//duty_shift_user_tab_common.ini
    echo("</table>\n");

    // 下線
    echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td bgcolor=\"#5279a5\"><img src=\"img/spacer.gif\" width=\"1\" height=\"2\" alt=\"\"></td></tr></table><img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
	?>
	</td></tr></table>
    <?
}
//■■■■ユーザ画面ヘッダ■■■■■■■■■■■■
elseif($umode == "usr" || $mmode == "usr"){
    require_once("atdbk_menu_common.ini");
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr bgcolor="#f6f9ff">
					<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
					<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a></font></td>
    <?php
    if ($work_admin_auth == "1") {
?>
					<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<?php echo($session); ?>&work=1"><b>管理画面へ</b></a></font></td>
        <?php
    }
?>
				</tr>
			</table>
    <?php
    // メニュー表示
	$arr_option["mmode"] = $mmode;
    show_atdbk_menuitem($session, $fname, $arr_option);
?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
				</tr>
			</table>

<?
if ($mmode == "usr") {
?>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<?
		$arr_option["mmode"] = $mmode;
        show_work_admin_submenu($session,$fname,$arr_option);
}
else {
?>

			<table width="500" border="0" cellspacing="0" cellpadding="1">
				<tr>
					<td height="22" colspan="2">
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;&nbsp;
    <?php
    $wk_title = "有給（年次）休暇";
    $wk_url = "<a href=\"atdbk_vacation.php?session={$session}&year={$year}&flg=1\">{$wk_title}</a>";
    echo($wk_url);
    echo("&nbsp;&nbsp;");

    $wk_title = "休暇取得状況";
    $wk_url = "<a href=\"atdbk_vacation.php?session={$session}&year={$year}&flg=2\">{$wk_title}</a>";
    echo($wk_url);
    echo("&nbsp;&nbsp;");
    //$wk_title = "年次休暇簿";
    $wk_url = "<b>{$wk_this_title}</b>";
    echo($wk_url);
    echo("&nbsp;&nbsp;");

    //休暇簿　2012.04
    $wk_disp_flg = get_settings_value("kintai_vacation_disp_flg", "f"); // opt/settings.phpで変更
    if ($wk_disp_flg == "t") {
        $wk_title = "休暇簿";
        $wk_url = "<a href=\"kintai_vacation.php?session={$session}&umode=usr\">{$wk_title}</a>";
        echo($wk_url);
        echo("&nbsp;&nbsp;");
    }
?>
						</font>
					</td>
				</tr>
			</table>
			<br>
<? } //mmode ?> 
		</td>
	</tr>
</table>
    <?php
    //■■■■管理者画面ヘッダ■■■■■■■■■■■■
}else if($umode == "adm" || $umode == "adm2"){
    // 遷移元の設定
    if ($work != "") {
        set_referer($con, $session, "work", $work, $fname);
        $referer = $work;
    } else {
        $referer = get_referer($con, $session, "work", $fname);
    }
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
    <?php
    if ($referer == "2") {
?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr bgcolor="#f6f9ff">
					<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<?php echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
					<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<?php echo($session); ?>"><b>勤務管理</b></a></font></td>
				</tr>
			</table>
        <?php
    } else {
?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr bgcolor="#f6f9ff">
					<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
					<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
					<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>ユーザ画面へ</b></a></font></td>
				</tr>
			</table>
        <?php
    }
    // メニュータブ
    show_work_admin_menuitem($session, $fname, "");
?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
				</tr>
			</table>
    <?
    //サブメニュー表示
    if ($umode == "adm") {
        ?>
			<img src="img/spacer.gif" alt="" width="1" height="5"><br>
        <?
		$arr_option["mmode"] = $mmode;
        show_work_admin_submenu($session,$fname,$arr_option);
    }
    else {
        ?>
			<img src="img/spacer.gif" alt="" width="1" height="2"><br>
        <?
        require_once("timecard_hol_menu_common.php");
        show_timecard_hol_menuitem($session,$fname,$arr_option);
    }
?>
		</td>
	</tr>
</table>
    <?php
}
else {
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>
<?
    echo $wk_this_title;
?>
</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

    <?
}
?>


<table>
	<tr>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
//前年度
$prev_idx = $arr_emppaid["prev_idx"];
if ($prev_idx >= 0) {
    $prev_add_date = $arr_emppaid[$prev_idx]["year"].$arr_emppaid[$prev_idx]["paid_hol_add_mmdd"];
    echo("<a  href=\"kintai_vacation_paid_holiday.php?session=$session&umode=$umode&selEmpID=$selEmpID&add_date=$prev_add_date&mmode=$mmode\">←前年度</a>");
}
else {
    echo("　　　　　　");
}
echo("　　");
//次年度
$next_idx = $arr_emppaid["next_idx"];
if ($next_idx >= 0) {
    $next_add_date = $arr_emppaid[$next_idx]["year"].$arr_emppaid[$next_idx]["paid_hol_add_mmdd"];
    echo("<a  href=\"kintai_vacation_paid_holiday.php?session=$session&umode=$umode&selEmpID=$selEmpID&add_date=$next_add_date&mmode=$mmode\">次年度→</a>");
}
?>
</font></td>
	</tr>
	<tr>
		<td width="300"><font size="4" face="ＭＳ Ｐゴシック, Osaka" class="j16">
<?php
if ($paid_hol_add_date != "") {
    echo $date_y1."年".intval($date_m1)."月".intval($date_d1)."日 〜 ".$date_y2."年".intval($date_m2)."月".intval($date_d2)."日";
}
        ?>
        </font></td>
		<td><font size="4" face="ＭＳ Ｐゴシック, Osaka" class="j16">
        　<?php echo $wk_this_title; ?></font></td>
		<td>&nbsp;&nbsp;</td>
		<td>
<?php
if($umode == "adm" || $umode == "duty" || $umode == "adm2"){
?>
			<input type="button" value="職員選択" onclick="openEmployeeList(1);">&nbsp;&nbsp;
    <?php
}

?>
			<input type="button" value="印刷" onclick="print_xls();">
        </td>
	</tr>
</table>
<table>
	<tr>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID：</font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $base_info->emp_personal_id; ?></font></td>
		<td>&nbsp;&nbsp;</td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属：</font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $shozoku; ?></font></td>
		<td>&nbsp;&nbsp;</td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種：</font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $job_info->job_nm; ?></font></td>
		<td>&nbsp;&nbsp;</td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">氏名：</font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $base_info->emp_lt_nm."&nbsp;".$base_info->emp_ft_nm; ?></font></td>
		<td>&nbsp;&nbsp;</td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">採用日：</font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
        <?php
if ($base_info->emp_join != "") {
    echo substr($base_info->emp_join, 0, 4)."年".
        intval(substr($base_info->emp_join, 4, 2))."月".
        intval(substr($base_info->emp_join, 6, 2))."日";
}
         ?></font></td>
<?

        ?>
	</tr>
</table>
<table>
	<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所定労働時間：<?php
if ($specified_time != "") {
    echo "$specified_time";
}
?></font></td>
		<td>&nbsp;&nbsp;</td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間単位で取得できる限度：
<?php
//上限ありの場合
if ($obj_hol_hour->paid_hol_hour_max_flag == "t") {
//    $wk_hour  = minute_to_hmm($wk_minute * $obj_hol_hour->paid_hol_hour_max_day);
//    echo "{$obj_hol_hour->paid_hol_hour_max_day}日（{$wk_hour}時間）";
    //HM対応 20130917
    $wk_hour  = minute_to_hmm($wk_minute * $obj_hol_hour->paid_hol_hour_max_day);
    echo "{$obj_hol_hour->paid_hol_hour_max_day}日{$wk_hour}";
}
else {
    echo "上限なし";
}
?></font></td>
	</tr>
</table>
<?
    $zan_day = $wk_total;
    $zan_minute = 0;
    $zan_minute += $wk_carry_time_minute;
    //$zan_time = minute_to_hmm($zan_minute);
// レイアウト修正 20130913
    $zan_time = minute_to_hmm($zan_minute);
?>
<table>
	<tr>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">前年繰越： <?
    if ($wk_carry != "" || $wk_carry_time != "") {
    echo $wk_carry; ?>日 <? echo $wk_carry_time; ?>
    <?php } ?></font></td>
		<td>&nbsp;&nbsp;</td>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">当年付与： <?
    if ($date_y1 != "") {
        echo $wk_add; ?>日（ <? echo $date_y1."年".intval($date_m1)."月".intval($date_d1)."日"; ?>）
    <?php } ?></font></td>
		<td>&nbsp;&nbsp;</td>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">合計： <?
    if ($zan_day != "" || $zan_time != "") {
    if ($zan_day == "") {
        $zan_day = "0";
    }
     echo $zan_day; ?>日 <? echo $zan_time; ?>
    <?php } ?></font></td>
    </tr>
</table>
<br>
<table border="0" cellspacing="0" cellpadding="2" class="list">
	<tr>
		<td align="center" width="80" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
		<td align="center" width="40" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">曜日</font></td>
		<td align="center" width="90" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">承認</font></td>
		<td align="center" width="60" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日単位有休</font></td>
		<td align="center" width="" colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間単位有休</font></td>
		<td align="center" width="100" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">有給休暇残日数</font></td>
		<td align="center" width="50" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請日</font></td>
	</tr>
	<tr>
		<td align="center" width="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">取得</font></td>
		<td align="center" width="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">累計</font></td>
		<td align="center" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間帯</font></td>
		<td align="center" width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">取得</font></td>
		<td align="center" width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">累計</font></td>
<? /* レイアウト修正 20130913
<td align="center" width="60"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日数</font></td>
<td align="center" width="60"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間数</font></td>
        */?>
	</tr>
<?
//一覧表示
//

$total_minute = 0;
$total_day = 0; // 日数累計 20130913
$data = array();
for ($i=0; $i<count($arr_data); $i++) {
    //

    if ($arr_data[$i]["date"] == "") {
        continue;
    }
    $data[$i]["date"] = intval(substr($arr_data[$i]["date"], 4, 2))."/".intval(substr($arr_data[$i]["date"], 6, 2));
    $data[$i]["wday"] = get_weekday(date_utils::to_timestamp_from_ymd($arr_data[$i]["date"]));
    if ($arr_data[$i]["apply_stat"] == "0") {
        $data[$i]["status"] = "申請中";
    }
    elseif ($arr_data[$i]["apply_stat"] == "1") {
        $data[$i]["status"] = "承認済み";
    }
    $data[$i]["apply_date"] = ($arr_data[$i]["apply_date"] != "") ?intval(substr($arr_data[$i]["apply_date"], 4, 2))."/".intval(substr($arr_data[$i]["apply_date"], 6, 2)) : "";
    //移行データの確認
    if ($arr_data[$i]["curr_use"] != "") {
        $wk_hol_day = $arr_data[$i]["curr_use"];
        $data[$i]["curr_use"] = $arr_data[$i]["curr_use"];
    }
    //
    else
        if ($arr_data[$i]["reason"] != "") {
            $wk_hol_day = $arr_data[$i]["hol_day"];
        }
    else {
        $wk_hol_day = "";
    }
    $data[$i]["hol_day"] = ($wk_hol_day != "" ) ? sprintf("%02.1f", $wk_hol_day) : "";
    //日累計 20130913
    if ($wk_hol_day != "") {
        $total_day += $wk_hol_day;
        $data[$i]["total_day"] = sprintf("%02.1f", $total_day);
    }
    //移行データがある場合
    if ($arr_data[$i]["curr_use_hour"] != "") {
        $data[$i]["hol_hour"] = "";
        $use_minute_calc = $obj_hol_hour->hh_or_hhmm_to_minute($arr_data[$i]["curr_use_hour"]);//20141014 HH:MM形式対応
        $wk_hmm = minute_to_hmm($use_minute_calc);
        $data[$i]["hol_hour_use"] = $wk_hmm;//20141014 HH:MM形式対応
        $data[$i]["hol_hour_total"] = $wk_hmm;//20141014 HH:MM形式対応

        $total_minute += $use_minute_calc;
        $data[$i]["curr_use_hour"] = $arr_data[$i]["curr_use_hour"];
    }
    else
    if ($arr_data[$i]["start_time"] != "") {
        $wk_start_time = intval(substr($arr_data[$i]["start_time"], 0, 2)).":".substr($arr_data[$i]["start_time"], 2, 2);
        $wk_start_time_minute = date_utils::hi_to_minute($arr_data[$i]["start_time"], 0, 2);
        $wk_end_time_minute = $wk_start_time_minute + $arr_data[$i]["use_hour"] * 60 + intval($arr_data[$i]["use_minute"]);
        $data[$i]["hol_hour"] = "$wk_start_time 〜 ".minute_to_hmm($wk_end_time_minute);
            //$data[$i]["hol_hour_use"] = $arr_data[$i]["use_hour"].":".$arr_data[$i]["use_minute"];
            $wk_use_hour = $arr_data[$i]["use_hour"].":".sprintf("%02d", $arr_data[$i]["use_minute"]);
            $data[$i]["hol_hour_use"] = $wk_use_hour;

        $use_minute_calc = $arr_data[$i]["use_hour"] * 60 + intval($arr_data[$i]["use_minute"]);
        $total_minute += $use_minute_calc;
            $data[$i]["hol_hour_total"] = minute_to_hmm($total_minute); //minute_to_hmm HM 20130913
    }
    else {
        $data[$i]["hol_hour"] = "";
        $data[$i]["hol_hour_use"] = "";
        $data[$i]["hol_hour_total"] = "";
        $use_minute_calc = 0;
    }
    //残数計算
    //所定時間未満分の時間を表示
    //残数から引ける場合
    if ($use_minute_calc > 0) {
        if ($zan_minute >= $use_minute_calc) {
            $zan_minute -= $use_minute_calc;
        }
        //残が少ない場合、日から繰り下げ
        else {
            //残が半日で引ける場合
            if ($zan_day == 0.5 && ($zan_minute + ($wk_minute / 2) >= $use_minute_calc )) {
                $zan_day -= 0.5; //日から引く
                $zan_minute += ($wk_minute / 2); //所定時間
            }
            else {
                $zan_day--; //日から引く
                $zan_minute += $wk_minute; //所定時間
            }
            $zan_minute -= $use_minute_calc;
        }
    }
    //時間を合わせて1日を引ける場合
    if ($arr_data[$i]["hol_day"] == 1 && $zan_day == 0.5 && $zan_minute >= ($wk_minute / 2)) {
        $zan_day -= 0.5; //日から引く
        $zan_minute -= floor($wk_minute / 2);
        $wk_hol_day = "";
    }

    //時間から0.5日を引ける場合
    if ($arr_data[$i]["hol_day"] == 0.5 && $zan_day == 0 && $zan_minute >= ($wk_minute / 2)) {
        $zan_minute -= floor($wk_minute / 2);
        $wk_hol_day = "";
    }
    if ($wk_hol_day != "") {
        $zan_day -= $wk_hol_day;
    }
    $zan_day = sprintf("%02.1f", $zan_day);
    $data[$i]["remain_day"] = $zan_day."日";

    $remain_hour = minute_to_hmm($zan_minute);
    if ($remain_hour == "") {
        $remain_hour = "0:00";
    }
    $data[$i]["remain_hour"] = $remain_hour;

?>
    <tr>
    <td align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;<? echo($data[$i]["date"]);
    if ($data[$i]["curr_use"] != "" ||
            $data[$i]["curr_use_hour"] != ""
        ) {
        echo "移行";
    }
         ?></font></td>
		<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($data[$i]["wday"]); ?></font></td>
		<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($data[$i]["status"]); ?></font></td>
    <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($data[$i]["hol_day"]);
    //日を追加 20130917
    if ($data[$i]["hol_day"] != "") {
        echo("日");
    }
         ?></font></td>
    <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($data[$i]["total_day"]);
    //日を追加 20130917
    if ($data[$i]["hol_day"] != "") {
        echo("日");
    }
         ?></font></td>
		<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($data[$i]["hol_hour"]); ?></font></td>
		<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($data[$i]["hol_hour_use"]); ?></font></td>
		<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($data[$i]["hol_hour_total"]); ?></font></td>
		<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($data[$i]["remain_day"]); ?><? echo($data[$i]["remain_hour"]); ?></font></td>
		<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($data[$i]["apply_date"]); ?></font></td>
    <?
    //申請日
?>
    </tr>
<?
}
?>
</table>

<input type="hidden" name="session" value="<?php echo($session); ?>">
<input type="hidden" name="year" value="<?php echo($year); ?>">

<input type="hidden" name="umode" value="<?php echo($umode); ?>">
<input type="hidden" name="selEmpID" value="<?php echo($selEmpID); ?>">

<input type="hidden" name="p_emp_id" value="<?php echo($base_info->emp_id); ?>">
<input type="hidden" name="paid_hol_add_date" value="<?php echo($paid_hol_add_date); ?>">
<input type="hidden" name="end_date" value="<?php echo($date_y2.$date_m2.$date_d2); ?>">
<input type="hidden" name="mmode" value="<?php echo($mmode); ?>">

</form>

<?
//add_date 付与日、初回は未設定
//初回はシステム日を含む年度、
function get_emppaid_info($con, $fname, $emp_id, $add_date) {

    if ($add_date != "") {
        $curr_year = substr($add_date, 0, 4);
    }
    else {
        $curr_year = date("Y");
    }
    $prev_year = $curr_year - 2;
    $next_year = $curr_year + 1;

    //前後1年ずつ検索
    $sql = "select * from emppaid ";
    $cond = "where emp_id = '$emp_id' and year >= '$prev_year' and year <= '$next_year' order by year , paid_hol_add_mmdd ";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $arr_data = array();
    while ($row = pg_fetch_array($sel)) {
        $arr_data[] = $row;
    }

    if ($add_date != "") {
        $compare_date = $add_date;
    }
    else {
        $compare_date = date("Ymd");
    }
    $curr_idx = -1;
    $prev_idx = -1;
    $next_idx = -1;

    for ($i=0; $i<count($arr_data); $i++) {
        $wk_add_date = $arr_data[$i]["year"].$arr_data[$i]["paid_hol_add_mmdd"];
        //もし次レコードがある場合はそちらの年月日を使用
        if ($i < count($arr_data) - 1) {
            $wk_date = $arr_data[$i+1]["year"].$arr_data[$i+1]["paid_hol_add_mmdd"];
        }
        //ない場合は1年後
        else {
            $wk_date = date("Ymd", strtotime("+1 year", date_utils::to_timestamp_from_ymd($wk_add_date)));
        }
        $wk_end_date = date("Ymd", strtotime("-1 day", date_utils::to_timestamp_from_ymd($wk_date)));
        //範囲を確認
        if ($wk_add_date <= $compare_date &&
                $compare_date <= $wk_end_date) {
            $curr_year = substr($wk_add_date, 0, 4);
            $curr_idx = $i;
            break;
        }
    }
    //今年度が見つかった場合、見つからない場合
    if ($curr_idx >= 0) {
        //前年度
        if ($curr_idx > 0) {
            $prev_idx = $curr_idx - 1;
        }
        //次年度
        if ($curr_idx < count($arr_data) - 1) {
            $next_idx = $curr_idx + 1;
        }
    }
    //今年度が見つからない場合
    else {
        if (count($arr_data) > 0) {
            //最後のレコードを使用
            $curr_idx = count($arr_data) - 1;
            //2件以上ある場合は前年度IDX設定
            if (count($arr_data) > 1) {
                $prev_idx = $curr_idx - 1;
            }
        }
    }
    $arr_data["curr_idx"] = $curr_idx;
    $arr_data["prev_idx"] = $prev_idx;
    $arr_data["next_idx"] = $next_idx;

    return $arr_data;
}


