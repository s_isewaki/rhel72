<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix タイムカード | 残業申請承認</title>
<?
require_once("about_comedix.php");
require_once("get_values.ini");
require_once("show_clock_in_common.ini");
require_once("show_overtime_approve_detail.ini");
require_once("show_overtime_apply_history.ini");
require_once("application_imprint_common.ini");
require_once("atdbk_workflow_common_class.php");
require_once("show_timecard_common.ini");
require_once("timecard_bean.php");
require_once("timecard_common_class.php");
require_once("ovtm_class.php");
require_once("atdbk_close_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//総合届フラグ
$general_flag = file_exists("opt/general_flag");

// データベースに接続
$con = connect2db($fname);

$obj = new atdbk_workflow_common_class($con, $fname);
//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);

$ovtm_class = new ovtm_class($con, $fname);

$imprint_flg = get_imprint_flg($con, $emp_id, $fname);
$imgsrc="img/approved.gif";
if ($imprint_flg == "t") {
	$imgsrc = "application_imprint_image.php?session=".$session."&emp_id=".$emp_id."&apv_flg=1&imprint_flg=".$imprint_flg."&t=".date('YmdHis');
}

$dirname = "workflow/imprint/";
$img_file = "img/approved.gif";
if ($imprint_flg == "t") {
	$img_file1 = "$dirname$emp_id.gif";
	$img_file2 = "$dirname$emp_id.jpg";
	// 当該職員の画像が登録済みか確認
	if (is_file($img_file1)) {
		$img_file = $img_file1;
	} else if (is_file($img_file2)) {
		$img_file = $img_file2;
	}
}
// 画像サイズ取得
list($w_frame1, $h_frame1) = get_imprint_imagesize($img_file);
list($w_frame2, $h_frame2) = get_imprint_imagesize("img/approve_ng.gif");
list($w_frame3, $h_frame3) = get_imprint_imagesize("img/returned.gif");

// 履歴フラグ true:履歴なので登録ボタン等を無効化する
$history_flg = false;
if($target_apply_id == "") {
	$target_apply_id = $apply_id;
} else {
	// $target_apply_idが異なる場合は再申請された以前のデータ
	if ($target_apply_id != $apply_id) {
		$history_flg = true;
	}
}

// 承認画像イメージ表示・非表示
$approve_flg = false;
$arr_applyapv = $obj->get_applyapv($target_apply_id, "OVTM");
foreach($arr_applyapv as $applyapv)
{
	$apv_stat       = $applyapv["aprv_status"];
	$apv_order      = $applyapv["aprv_no"];
	$apv_sub_order  = $applyapv["aprv_sub_no"];

	if(($apv_order == $apv_order_fix && $apv_sub_order == $apv_sub_order_fix) && $apv_stat == 0)
	{
		$approve_flg = true;
		break;
	}
}

// 残業申請情報を取得
$sql = "select empmst.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, ovtmapply.target_date, ovtmrsn.reason, ovtmapply.reason as other_reason, ovtmapply.apply_time, ovtmapply.comment, ovtmapply.start_time, ovtmapply.previous_day_flag, ovtmapply.end_time, ovtmapply.next_day_flag, ovtmapply.start_time, ovtmapply.previous_day_flag, ovtmapply.over_start_time, ovtmapply.over_end_time, ovtmapply.over_start_next_day_flag, ovtmapply.over_end_next_day_flag, ovtmapply.reason_detail, ovtmapply.over_start_time2, ovtmapply.over_end_time2, ovtmapply.over_start_next_day_flag2, ovtmapply.over_end_next_day_flag2 ";
for ($i=3; $i<=5; $i++) {
    $sql .=	", ovtmapply.over_start_time{$i}";
    $sql .=	", ovtmapply.over_end_time{$i}";
    $sql .=	", ovtmapply.over_start_next_day_flag{$i}";
    $sql .=	", ovtmapply.over_end_next_day_flag{$i}";
}
for ($i=1; $i<=5; $i++) {
    $sql .=	", ovtmapply.rest_start_time{$i}";
    $sql .=	", ovtmapply.rest_end_time{$i}";
    $sql .=	", ovtmapply.rest_start_next_day_flag{$i}";
    $sql .=	", ovtmapply.rest_end_next_day_flag{$i}";
}
//理由追加 20150106
for ($i=2; $i<=5; $i++) {
    $sql .=	", ovtmapply.reason_id{$i}";
    $sql .=	", ovtmapply.reason{$i}";
}
$sql .= " from (ovtmapply inner join empmst on ovtmapply.emp_id = empmst.emp_id) left join ovtmrsn on ovtmapply.reason_id = ovtmrsn.reason_id";
$cond = "where ovtmapply.apply_id = $target_apply_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
$apply_emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
$target_date = pg_fetch_result($sel, 0, "target_date");
$reason = pg_fetch_result($sel, 0, "reason");
$other_reason = pg_fetch_result($sel, 0, "other_reason");
$apply_time = pg_fetch_result($sel, 0, "apply_time");
$comment = pg_fetch_result($sel, 0, "comment");
$start_time = pg_fetch_result($sel, 0, "start_time");
$previous_day_flag = pg_fetch_result($sel, 0, "previous_day_flag");
$end_time = pg_fetch_result($sel, 0, "end_time");
$next_day_flag = pg_fetch_result($sel, 0, "next_day_flag");
$over_start_time = pg_fetch_result($sel, 0, "over_start_time");
$over_end_time = pg_fetch_result($sel, 0, "over_end_time");
//残業終了が未設定時は退勤時刻
if ($over_end_time == "") {
	$over_end_time = $end_time;
}
$over_start_next_day_flag = pg_fetch_result($sel, 0, "over_start_next_day_flag");
$over_end_next_day_flag = pg_fetch_result($sel, 0, "over_end_next_day_flag");
$reason_detail = pg_fetch_result($sel, 0, "reason_detail");

$over_start_time2 = pg_fetch_result($sel, 0, "over_start_time2");
$over_end_time2 = pg_fetch_result($sel, 0, "over_end_time2");
$over_start_next_day_flag2 = pg_fetch_result($sel, 0, "over_start_next_day_flag2");
$over_end_next_day_flag2 = pg_fetch_result($sel, 0, "over_end_next_day_flag2");

//ovtmapplyをまとめて取得
$arr_ovtmapply = pg_fetch_array($sel, 0, PGSQL_ASSOC);

//個人別勤務時間帯履歴対応 20130220 start
$timecard_common_class = new timecard_common_class($con, $fname, $emp_id, $target_date, $target_date);
// 勤務実績情報を取得
$arr_result = $timecard_common_class->get_clock_in_atdbkrslt_array($con, $emp_id, $fname, "1", $target_date);
$emp_officehours = $timecard_common_class->get_officehours_emp($emp_id, $target_date, $arr_result["tmcd_group_id"], $arr_result["pattern"]);
if ($emp_officehours["office_start_time"] != "") {
	$arr_result["office_start_time"] = $emp_officehours["office_start_time"];
	$arr_result["office_end_time"] = $emp_officehours["office_end_time"];
}
//個人別勤務時間帯履歴対応 20130220 end

//残業開始が未設定時は所定終了時刻
if ($over_start_time == "") {
	$over_start_time = $arr_result["office_end_time"];
}
$update_emp_id = $arr_result["update_emp_id"];
//所属
$update_org_name = get_orgname($con, $fname, $update_emp_id, $timecard_bean);
if ($update_org_name != "") {
	$update_org_name = substr($update_org_name, 1);
	$update_org_name = str_replace(" ", " &gt; ", $update_org_name);
}
//更新者氏名
$update_emp_name = get_emp_kanji_name($con, $update_emp_id, $fname);

$update_time = preg_replace("/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/","$1年$2月$3日 $4:$5",$arr_result["update_time"]);

//前日か判定
$previous_day_value = "";
if($previous_day_flag == 1){
	$previous_day_value = "　前日";
}

//翌日か判定
$next_day_value = "";
if($next_day_flag == 1){
	$next_day_value = "　翌日";
}
$over_start_next_day_flag_value = "";
if ($over_start_next_day_flag == 1){
	$over_start_next_day_flag_value = "　翌日";
}
$over_end_next_day_flag_value = "";
if ($over_end_next_day_flag == 1){
	$over_end_next_day_flag_value = "　翌日";
}

$over_start_next_day_flag2_value = "";
if ($over_start_next_day_flag2 == 1){
	$over_start_next_day_flag2_value = "　翌日";
}
$over_end_next_day_flag2_value = "";
if ($over_end_next_day_flag2 == 1){
	$over_end_next_day_flag2_value = "　翌日";
}
$arr_ovtm_data = $timecard_common_class->get_ovtm_month_total($emp_id, $target_date, "refer");

//上側へのボタン表示
$btn_str = "";
if ($approve_flg == true && $history_flg == false) {
    $btn_str = "<input type=\"button\" value=\"登録\" onclick=\"approve_regist();\">\n";
}

//1階層目の場合のみチェック 20151026
$close_stat = "0";
//練馬総合病院様を除く 20160204
if (!$general_flag) {
	if ($apv_order_fix == 1) {
		//締め状態確認 20150311
		$atdbk_close_class = new atdbk_close_class($con, $fname);

		$close_stat = $atdbk_close_class->get_close_stat($emp_id, $target_date);
	}
}
?>
<script type="text/javascript" src="js/prototype/prototype-1.7.1.js"></script>
<script type="text/javascript" src="js/yui/3.10.3/build/yui/yui-min.js"></script>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
var dd1;
var dd2;
var dd3;
function initPage() {
	YUI().use('dd-drag', 'dd-drop', 'dd-scroll', function(Y) {
		if (document.getElementById('apply1')) {
			dd1 = new Y.DD.Drag({node: '#apply1'}).plug(Y.Plugin.DDWinScroll);
			dd1.set('startXY', dd1.get('node').getXY());
			dd1.on('drag:start', function(e) {
				this.get('node').setStyle('zIndex', 999);
			});
			dd1.on('drag:drophit', function(e) {
				set_target('1');
			});
			dd1.on('drag:dropmiss', function(e) {
				var node = this.get('node');
				node.setXY(this.get('startXY'));
				node.setStyle('backgroundColor', '');
				node.setStyle('zIndex', 0);
			});
		}

		if (document.getElementById('apply2')) {
			dd2 = new Y.DD.Drag({node: '#apply2'}).plug(Y.Plugin.DDWinScroll);
			dd2.set('startXY', dd2.get('node').getXY());
			dd2.on('drag:start', function(e) {
				this.get('node').setStyle('zIndex', 999);
			});
			dd2.on('drag:drophit', function(e) {
				set_target('2');
			});
			dd2.on('drag:dropmiss', function(e) {
				var node = this.get('node');
				node.setXY(this.get('startXY'));
				node.setStyle('backgroundColor', '');
				node.setStyle('zIndex', 0);
			});
		}

		if (document.getElementById('apply3')) {
			dd3 = new Y.DD.Drag({node: '#apply3'}).plug(Y.Plugin.DDWinScroll);
			dd3.set('startXY', dd3.get('node').getXY());
			dd3.on('drag:start', function(e) {
				this.get('node').setStyle('zIndex', 999);
			});
			dd3.on('drag:drophit', function(e) {
				set_target('3');
			});
			dd3.on('drag:dropmiss', function(e) {
				var node = this.get('node');
				node.setXY(this.get('startXY'));
				node.setStyle('backgroundColor', '');
				node.setStyle('zIndex', 0);
			});
		}

		if (document.getElementById('target1')) {
			new Y.DD.Drop({node: '#target1'});
		}
	});
}

function set_target(flg) {

// drag元を消す
	document.getElementById("apply"+flg).style.display = "none";
// 白い背景を表示
	document.getElementById("empty"+flg).style.display = "";

// ターゲットに画像を表示
	var imgsrc = "";
	if (flg == 1) {
		imgsrc = "<?=$imgsrc?>";
		document.getElementById("img1").width = <?=$w_frame1?>;
		document.getElementById("img1").height = <?=$h_frame1?>;
	} else if (flg == 2) {
		imgsrc = "img/approve_ng.gif";
		document.getElementById("img1").width = <?=$w_frame2?>;
		document.getElementById("img1").height = <?=$h_frame2?>;
	} else if (flg == 3) {
		imgsrc = "img/returned.gif";
		document.getElementById("img1").width = <?=$w_frame3?>;
		document.getElementById("img1").height = <?=$h_frame3?>;
	}
	document.getElementById("img1").src = imgsrc;
// 背景を白に
	document.getElementById("target1").style.backgroundColor = "#ffffff";
	document.getElementById("message1").style.backgroundColor = "#f6f9ff";
// プルダウンを設定
	document.apply.approve.value = flg;

	document.apply.drag_flg.value = "t";

}

function history_select(apply_id, target_apply_id) {

	document.apply.apply_id.value = apply_id;
	document.apply.target_apply_id.value = target_apply_id;
	document.apply.action="overtime_approve.php?session=<?=$session?>&apv_order_fix=<?=$apv_order_fix?>&apv_sub_order_fix=<?=$apv_sub_order_fix?>&send_apved_order_fix=<?=$send_apved_order_fix?>";
	document.apply.submit();
}

function reset_imprint() {

	document.getElementById("apply1").style.display = "";
	document.getElementById("empty1").style.display = "none";
	dd1.get('node').setXY(dd1.get('startXY'));
	document.getElementById("apply2").style.display = "";
	document.getElementById("empty2").style.display = "none";
	dd2.get('node').setXY(dd2.get('startXY'));
	document.getElementById("apply3").style.display = "";
	document.getElementById("empty3").style.display = "none";
	dd3.get('node').setXY(dd3.get('startXY'));
	document.getElementById("target1").style.backgroundColor = "#ffcccc";
	document.getElementById("img1").src = "img/spacer.gif";
	document.getElementById("message1").style.backgroundColor = "#ffcccc";

	document.apply.drag_flg.value = "f";

}

function approve_regist() {
<?
//1階層目の場合のみチェック 20151026
if ($apv_order_fix == 1 && $close_stat == "1") {
?>
	alert('出勤表の勤務時間集計画面で締め済みのため登録できません。');
	return false;
<?
}
?>

	if (confirm('登録します。よろしいですか？')) {
		document.apply.action="overtime_approve_exe.php?session=<?=$session?>";
		document.apply.submit();
	}
}


// テキストエリア行数の自動拡張
var ResizingTextArea = Class.create();

ResizingTextArea.prototype = {
    defaultRows: 1,

    initialize: function(field)
    {
        this.defaultRows = Math.max(field.rows, 1);
        this.resizeNeeded = this.resizeNeeded.bindAsEventListener(this);
        Event.observe(field, "click", this.resizeNeeded);
        Event.observe(field, "keyup", this.resizeNeeded);
    },

    resizeNeeded: function(event)
    {
        var t = Event.element(event);
        var lines = t.value.split('\n');
        var newRows = lines.length + 1;
        var oldRows = t.rows;
        for (var i = 0; i < lines.length; i++)
        {
            var line = lines[i];
            if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
        }
        if (newRows > t.rows) t.rows = newRows;
        if (newRows < t.rows) t.rows = Math.max(this.defaultRows, newRows);

        // 履歴表示table高さ調整
        flg = resize_history_tbl();
        if(flg)
        {
            // 承認画像のY軸座標位置の再設定
            var y_position;
            if (dd1 && dd1.get('node').getStyle('display') != 'none') {
                y_position = dd1.get('node').getY();
            }
            if (!y_position) {
                YUI().use('node', function(Y) {
                    var empty1 = Y.one('#empty1');
                    if (empty1 && empty1.getStyle('display') != 'none') {
                        y_position = empty1.getY();
                    }
                });
            }
            if (!y_position) {
                return;
            }
            if (dd1) {
                dd1.get('node').setY(y_position);
                dd1.set('startXY', [dd1.get('startXY')[0], y_position]);
            }
            if (dd2) {
                dd2.get('node').setY(y_position);
                dd2.set('startXY', [dd2.get('startXY')[0], y_position]);
            }
            if (dd3) {
                dd3.get('node').setY(y_position);
                dd3.set('startXY', [dd3.get('startXY')[0], y_position]);
            }
        }
    }
}

// 全テキストエリアの行数変更
function resizeAllTextArea() {
	var objs = document.getElementsByTagName('textarea');
	for (var i = 0, j = objs.length; i < j; i++) {
	    var t = objs[i];
		var defaultRows = Math.max(t.rows, 1);
	    var lines = t.value.split('\n');
	    var newRows = lines.length + 1;
	    var oldRows = t.rows;
	    for (var k = 0; k < lines.length; k++)
	    {
	        var line = lines[k];
	        if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
	    }
	    if (newRows > t.rows) t.rows = newRows;
	    if (newRows < t.rows) t.rows = Math.max(defaultRows, newRows);
	}
}


// 履歴表示tableの高さ調節
function resize_history_tbl()
{
    var flg = false;
    heigh1 = document.getElementById('history_tbl').style.height;
    document.getElementById('history_tbl').style.height = document.getElementById('dtl_tbl').offsetHeight;
    heigh2 = document.getElementById('history_tbl').style.height;

    if(heigh1 != heigh2)
    {
        flg = true;
    }
    return flg
}


</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}


table.block2 {border-collapse:collapse;}
table.block2 td {border:#5279a5 solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#5279a5 solid 0px;}
table.block3 td {border-width:0;}

p {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="resizeAllTextArea();resize_history_tbl();initPage();">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>残業申請承認</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="apply" action="overtime_approve_exe.php" method="post">
<? if ($btn_str != "") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="left" width="145">
</td>
<td align="left">
</td>
<td align="right">
<? echo $btn_str; ?>
</td>
</tr>
</table>
<? } ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top" width="25%">
<?
show_overtime_history_for_approve($con, $session, $fname, $apply_id, $target_apply_id, $approve_flg, $history_flg);
?>
</td>
<td><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td valign="top" align="center" width="75%">

<div id="dtl_tbl">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<? // width="110" ?>
<td width="90" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($apply_emp_nm); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請日時</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})\d{2}$/", "$1/$2/$3 $4:$5", $apply_time)); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $target_date)); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">始業時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $arr_result["office_start_time"])); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $start_time).$previous_day_value); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">終業時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $arr_result["office_end_time"])); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退勤時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $end_time).$next_day_value); ?></font></td>
</tr>
<?
$ovtm_class->disp_btn_time_data($arr_result, "refer");
?>
<?
// 追加 20140626
$maxline = ($ovtm_class->ovtm_tab_flg == "t") ? 5 : 2;
for ($i=1; $i<=$maxline; $i++) {
 ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">残業時刻<? echo $i; ?></font></td>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
    $idx = ($i == 1) ? "" : $i;
    $s_t = "over_start_time".$idx;
    $e_t = "over_end_time".$idx;
    $s_f = "over_start_next_day_flag".$idx;
    $e_f = "over_end_next_day_flag".$idx;
    $over_start_next_day_flag_value = "";
    if ($arr_ovtmapply[$s_f] == 1){
        $over_start_next_day_flag_value = "　翌日";
    }
    $over_end_next_day_flag_value = "";
    if ($arr_ovtmapply[$e_f] == 1){
        $over_end_next_day_flag_value = "　翌日";
    }
    
    $wk_str = preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $arr_ovtmapply[$s_t]).$over_start_next_day_flag_value."〜".preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $arr_ovtmapply[$e_t]).$over_end_next_day_flag_value;
    if ($arr_ovtmapply[$s_t] != "") {
        echo($wk_str);
        //休憩
        $s_t = "rest_start_time".$i;
        $e_t = "rest_end_time".$i;
        $s_f = "rest_start_next_day_flag".$i;
        $e_f = "rest_end_next_day_flag".$i;
        $rest_start_next_day_flag_value = "";
        if ($arr_ovtmapply[$s_f] == 1){
            $rest_start_next_day_flag_value = "　翌日";
        }
        $rest_end_next_day_flag_value = "";
        if ($arr_ovtmapply[$e_f] == 1){
            $rest_end_next_day_flag_value = "　翌日";
        }
        
        if ($arr_ovtmapply[$s_t] != "") {
            echo "　休憩（";
            $wk_str = preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $arr_ovtmapply[$s_t]).$rest_start_next_day_flag_value."〜".preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $arr_ovtmapply[$e_t]).$rest_end_next_day_flag_value;
            echo($wk_str);
            
            echo "）";
        }
        //残業理由 20150106
		if ($ovtm_class->ovtm_reason2_input_flg == "t") {
	        echo "&nbsp;";
	        if ($i == 1) {
	        	$reason_val = $reason;
		        $other_reason_val = $arr_ovtmapply["other_reason"];
	        }
	        else {
		        $other_reason_val = $arr_ovtmapply["reason".$idx];
		        if ($other_reason_val != "") {
			        $reason_val = "";
	        	}
	        	else {
		        	$wk_reason_id = $arr_ovtmapply["reason_id".$idx];
		        	$reason_val = $ovtm_class->get_ovtmrsn_reason($wk_reason_id);
	        	}
	        }
	        echo($reason_val . $other_reason_val);
        }
    }
 ?></font></td>
</tr>
<? } ?>
<?php
//時間数を追加 20150119
$overtime_total = 0;
for ($i=1; $i<=5; $i++) {
	$idx = ($i == 1) ? "" : $i;
    $s_t = "over_start_time".$idx;
    $e_t = "over_end_time".$idx;
    $s_f = "over_start_next_day_flag".$idx;
    $e_f = "over_end_next_day_flag".$idx;
    $over_start_time_value = $arr_ovtmapply[$s_t];
    $over_end_time_value = $arr_ovtmapply[$e_t];
    $over_start_next_day_flag_value = $arr_ovtmapply[$s_f];
    $over_end_next_day_flag_value = $arr_ovtmapply[$e_f];
    
    if ($over_start_time_value != "" && $over_end_time_value != "") {
		//開始日時
        $over_start_date_time_value = ($over_start_next_day_flag_value == 1) ? next_date($target_date).$over_start_time_value : $target_date.$over_start_time_value;
		//終了日時
        $over_end_date_time_value = ($over_end_next_day_flag_value == 1) ? next_date($target_date).$over_end_time_value : $target_date.$over_end_time_value;
$overtime_total += date_utils::get_diff_minute($over_end_date_time_value, $over_start_date_time_value);
        $rs_t = "rest_start_time".$i;
        $re_t = "rest_end_time".$i;
        $rs_f = "rest_start_next_day_flag".$i;
        $re_f = "rest_end_next_day_flag".$i;
        $rest_start_time_value = $arr_ovtmapply[$rs_t];
        $rest_end_time_value = $arr_ovtmapply[$re_t];
        if ($rest_start_time_value != "" && $rest_end_time_value != "") {
            $rest_start_date_value = ($arr_ovtmapply[$rs_f] == 1) ? next_date($target_date) :$target_date;
            $rest_end_date_value = ($arr_ovtmapply[$re_f] == 1) ? next_date($target_date) :$target_date;
            $rest_start_date_time_value = $rest_start_date_value.$rest_start_time_value;
            $rest_end_date_time_value = $rest_end_date_value.$rest_end_time_value;
			$overtime_total -= date_utils::get_diff_minute($rest_end_date_time_value, $rest_start_date_time_value);
        }
    }

}
$hour = intval($overtime_total / 60);
$min = $overtime_total - ($hour * 60);
$overtime_hhmm = sprintf("%d:%02d", $hour, $min);
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">残業時間計</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($overtime_hhmm); ?></font></td>
</tr>
<?
$ovtm_class->disp_ovtm_total_time($arr_ovtm_data);
//残業時刻2以降にも理由を入力する場合は既存の理由を表示しない
if ($ovtm_class->ovtm_reason2_input_flg != "t") {
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">理由</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($reason . $other_reason); ?></font></td>
</tr>
<?php } ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">理由詳細</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(str_replace("\n", "<br>", $reason_detail)); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者コメント</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(str_replace("\n", "<br>", $comment)); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">最終更新日時</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$update_time?>
</font>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">最終更新者所属</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$update_org_name?>
</font>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">最終更新者</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$update_emp_name?>
</font>
</td>
</tr>

<?
$mode = "";
show_overtime_approve_detail($con, $session, $fname, $target_apply_id, $mode, $apv_comment, $approve, $drag_flg, $changes_flg, $history_flg, $apv_order_fix, $apv_sub_order_fix, $send_apved_order_fix, $update_emp_id);
?>

<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="apply_id" value="<? echo($apply_id); ?>">
<input type="hidden" name="target_apply_id" value="<?=$target_apply_id?>">
<input type="hidden" name="pnt_url" value="<? echo($pnt_url); ?>">
<!--
<input type="hidden" name="rbt" value="<? echo($rbt); ?>">
-->
</td>
</tr>
</table>

</form>
</center>
</body>
<? pg_close($con); ?>
</html>
