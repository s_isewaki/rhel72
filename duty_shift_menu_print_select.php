
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成 | 印刷範囲設定</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
//require_once("show_select_values.ini");

//require_once("yui_calendar_util.ini");

require_once("duty_shift_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);

// 月単位のリストを作る 20130219
$month_list = "";
for($i = -5; $i < 8; $i++)
{
	$buf = date("Y-m",strtotime("$i month" ,strtotime($duty_yyyy. "/". $duty_mm. "/1")));
	if ($i == 0)
	{
		$month_list .= "<OPTION VALUE=\"".$buf."\" selected>". $buf. "</OPTION>\n";
	}else{
		$month_list .= "<OPTION VALUE=\"".$buf."\">". $buf. "</OPTION>\n";
	}
}

?>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
	///-----------------------------------------------------------------------------
	// 「ＯＫ」印刷範囲を指定して印刷を実行
	///-----------------------------------------------------------------------------
	function OkBtn() {
		var total_print_flg;
		if (document.mainform.elements['total_print_flg'][0].checked) {
			total_print_flg = '1';
		} else {
			total_print_flg = '0';
		}
	    if (document.mainform.elements['pdf_print_flg'][1].checked) {
			pdf_print_flg = '1';
		} else {
			pdf_print_flg = '0';
		}

	    var selectmonth = "";
	    <?php if ($atdbk_flag != TRUE) {?>
			//月単位の値 20130219
		    selectmonth =  (document.mainform.setMonth.checked) ? document.mainform.selectmonth.value : "";
		<?php }?>

		//印刷範囲を指定して印刷を実行
		if(parent.opener && !parent.opener.closed && parent.opener.dataPrint){
			parent.opener.dataPrint(total_print_flg, pdf_print_flg, selectmonth);
		}

		window.close();
	}
	///-----------------------------------------------------------------------------
	// 「キャンセル」
	///-----------------------------------------------------------------------------
	function CancelBtn() {
		window.close();
	}

	///-----------------------------------------------------------------------------
	// 月単位で出力する 20130219
	///-----------------------------------------------------------------------------
	function monthChange() {
		if( document.mainform.setMonth.checked){
			document.getElementById("012").removeAttribute("disabled");
		}
		if( !document.mainform.setMonth.checked){
			document.getElementById("012").setAttribute("disabled","enable");
		}
	}

</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<!-- <body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initcal();"> -->
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
		<tr height="32" bgcolor="#5279a5">
		<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>印刷範囲設定</b></font></td>
		<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
		</tr>
	</table>

	<form name="mainform" method="post">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr height="22">
		<td width="20%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>集計行列の印刷</b></font></td>
		</tr>
	<tr height="20">
		<td width="80%" align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<input type="radio" id="total_print_flg" name="total_print_flg" value="1">する　
		<input type="radio" id="total_print_flg" name="total_print_flg" value="0" checked>しない
		</font>
		</td>
	</tr>
	<tr height="16">
        <td width="80%" align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j10'> （PDF:A3）　　　（PDF:A4）</font></td>
	</tr>
	<tr height="22">
		<td width="80%" align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<input type="radio" id="pdf_print_flg" name="pdf_print_flg" value="0">WEBページ
		<input type="radio" id="pdf_print_flg" name="pdf_print_flg" value="1" checked>PDF
		</font>
		</td>
	</tr>
	<!-- 	PDFで月単位で出力する 20130219 -->
<?php if ($atdbk_flag != TRUE) {?>
	<tr height="22">
		<td width="80%" align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<INPUT type="CHECKBOX" name="setMonth" ID="011" onclick="monthChange()">月単位で出力する&nbsp;&nbsp;&nbsp;
 		<SELECT name="selectmonth" ID="012" disabled><? echo $month_list; ?></select>
		</font>
		</td>
	</tr>
<?php }?>
	</table>

		<br>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 「ＯＫ」「キャンセル」ボタン -->
		<!-- ------------------------------------------------------------------------ -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="100%" align="center">
			<input type="button" value="　　ＯＫ　　" onclick="OkBtn();">
			<input type="button" value="キャンセル" onclick="CancelBtn();">
		</td>
		</tr>
	</table>
	</form>
</center>
</body>
<? pg_close($con); ?>
</html>

