<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 出勤表 | 休暇管理</title>
<?
//ini_set("display_errors","1");
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_timecard_common.ini");
require_once("show_attendance_pattern.ini");
require_once("holiday.php");
require_once("atdbk_menu_common.ini");
require_once("timecard_common_class.php");
require_once("atdbk_common_class.php");
require_once("date_utils.php");
require_once("timecard_bean.php");
require_once("timecard_paid_hol_hour_class.php");
require_once("calendar_name_class.php");
require_once("settings_common.php"); // 年次有給休暇簿 20131126
require_once("atdbk_close_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 5, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 勤務管理権限の取得
$work_admin_auth = check_authority($session, 42, $fname);

if ($flg == "") { $flg = "1"; }

// データベースに接続
$con = connect2db($fname);

//時間有休
$obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
$obj_hol_hour->select();
//カレンダー名、所定労働時間履歴用
$calendar_name_class = new calendar_name_class($con, $fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// ログインユーザの出勤グループを取得
$tmcd_group_id = get_timecard_group_id($con, $emp_id, $fname);

// ************ oose add start *****************
// 勤務条件テーブルより勤務形態（常勤、非常勤）、残業管理を取得
$sql = "select duty_form, no_overtime from empcond";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$duty_form = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "duty_form") : "";
$no_overtime = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "no_overtime") : "";
// ************ oose add end *****************

// 「締め日」「端数処理」「その他」情報を取得
$sql = "select * from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	$fraction1 = pg_fetch_result($sel, 0, "fraction1");
	$fraction1_min = pg_fetch_result($sel, 0, "fraction1_min");
	$fraction2 = pg_fetch_result($sel, 0, "fraction2");
	$fraction2_min = pg_fetch_result($sel, 0, "fraction2_min");
	$fraction3 = pg_fetch_result($sel, 0, "fraction3");
	$fraction4 = pg_fetch_result($sel, 0, "fraction4");
	$fraction4_min = pg_fetch_result($sel, 0, "fraction4_min");
	$fraction5 = pg_fetch_result($sel, 0, "fraction5");
	$fraction5_min = pg_fetch_result($sel, 0, "fraction5_min");
	$others1 = pg_fetch_result($sel, 0, "others1");
	$others2 = pg_fetch_result($sel, 0, "others2");
	$others3 = pg_fetch_result($sel, 0, "others3");
	$others4 = pg_fetch_result($sel, 0, "others4");
	$others5 = pg_fetch_result($sel, 0, "others5");
	$rest1 = intval(pg_fetch_result($sel, 0, "rest1"));
	$rest2 = intval(pg_fetch_result($sel, 0, "rest2"));
	$rest3 = intval(pg_fetch_result($sel, 0, "rest3"));

	$closing_paid_holiday = pg_fetch_result($sel, 0, "closing_paid_holiday");
	$delay_overtime_flg = pg_fetch_result($sel, 0, "delay_overtime_flg");
	$criteria_months = pg_fetch_result($sel, 0, "criteria_months");
	$year_paid_hol_menu_flg = pg_fetch_result($sel, 0, "year_paid_hol_menu_flg");

} else {
	$closing = "";
	$fraction1 = "";
	$fraction1_min = "";
	$fraction2 = "";
	$fraction2_min = "";
	$fraction3 = "";
	$fraction4 = "";
	$fraction4_min = "";
	$fraction5 = "";
	$fraction5_min = "";
	$others1 = "";
	$others2 = "";
	$others3 = "";
	$others4 = "";
	$others5 = "";
	$rest1 = 0;
	$rest2 = 0;
	$rest3 = 0;
	$closing_paid_holiday = 1;
	$delay_overtime_flg = "1";
	$criteria_months = "2";
	$year_paid_hol_menu_flg = "";
}
$atdbk_close_class = new atdbk_close_class($con, $fname);
$closing = $atdbk_close_class->get_empcond_closing($emp_id);
$closing_month_flg = $atdbk_close_class->closing_month_flg;

$org_year = $year;
// 締め日が登録済みの場合
if ($closing != "") {

	// 処理対象年度の設定
	if ($year == "") {
		$year = date("Y");

		if ($closing_paid_holiday == 1) {
			switch ($closing) {
			case "1":  // 1日
				$check_date = "{$year}0401";
				break;
			case "2":  // 5日
				$check_date = "{$year}0405";
				break;
			case "3":  // 10日
				$check_date = "{$year}0410";
				break;
			case "4":  // 15日
				$check_date = "{$year}0415";
				break;
			case "5":  // 20日
				$check_date = "{$year}0420";
				break;
			case "6":  // 末日
				$check_date = "{$year}0331";
				break;
			}

			if (date("Ymd") <= $check_date) {
				$year--;
			}
		}
	}

	// 変則労働適用情報を配列で取得
	$arr_irrg_info = get_irrg_info_array($con, $emp_id, $fname);

	// 給与支給区分・祝日計算方法を取得
	$sql = "select wage, hol_minus from empcond";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$wage = pg_fetch_result($sel, 0, "wage");
		$hol_minus = pg_fetch_result($sel, 0, "hol_minus");
	} else {
		$wage = "";
		$hol_minus = "";
	}

	// 有給休暇情報を取得
	$default_start_year = 2010; //20110810 2006から2010に変更
	$arr_paid_hol = array();
	// $arr_paid_hol[0:繰越 1:付与 2:年初残 3:調整日数 4:時間有休繰越][0-9:年度]の２次元配列

	//グループ毎のテーブルから勤務時間を取得する
    $timecard_common_class = new timecard_common_class($con, $fname, $emp_id);
	$day1_time = $timecard_common_class->get_calendarname_day1_time();

	if ($flg == 1) {

		if ($start_year == "") {
			//当年を元に開始年を求める
			$cur_year = date("Y");
			//3月末締めの場合年度にする
			if ($closing_paid_holiday == 1) {
				$md = date("md");
				if($md >= "0101" and $md <= "0331") {
					$cur_year--;
				}
			}
			$start_year = $default_start_year + (intval(($cur_year-$default_start_year)/10) * 10);
		}

		$end_year = $start_year + 9;
        $sql = "select year, days1, days2, adjust_day, paid_hol_add_mmdd, carry_time_minute from emppaid";
		$cond = "where emp_id = '$emp_id' and year >= '$start_year' and year <= '$end_year' order by year, paid_hol_add_mmdd ";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$wk_paid_hol_add_mmdd = "";
		$arr_paid_hol_add_mmdd = array();
		$num = pg_num_rows($sel);
		for ($i=0; $i<$num; $i++) {
			$wk_year = pg_fetch_result($sel, $i, "year");
            $wk_days1 = pg_fetch_result($sel, $i, "days1"); //繰越
			$wk_days2 = pg_fetch_result($sel, $i, "days2"); //付与
			$wk_adjust_day = pg_fetch_result($sel, $i, "adjust_day"); //調整日数
			if ($wk_adjust_day == "") {
				$wk_adjust_day = 0;
			}
			if ($wk_year == $year) {
				$wk_paid_hol_add_mmdd = pg_fetch_result($sel, $i, "paid_hol_add_mmdd"); //付与日
			}
			$idx = $wk_year - $start_year;
			$arr_paid_hol[0][$idx] = ($wk_days1 != "") ? $wk_days1 : 0;
			$arr_paid_hol[1][$idx] = ($wk_days2 != "") ? $wk_days2 : 0;
			$arr_paid_hol[2][$idx] = $arr_paid_hol[0][$idx] + $arr_paid_hol[1][$idx] + $wk_adjust_day;
			$arr_paid_hol[3][$idx] = $wk_adjust_day;
			//登録データの付与日
			$arr_paid_hol_add_mmdd[$idx] = pg_fetch_result($sel, $i, "paid_hol_add_mmdd");
            //時間有休繰越、端数処理
            $wk_carry_time_minute = pg_fetch_result($sel, $i, "carry_time_minute");
            //繰越方法
            if ($wk_carry_time_minute > 0) {
                switch ($obj_hol_hour->paid_hol_hour_carry_flg) {
                    case "2": //そのまま繰越
                        break;
                    case "3": //切捨て
                        $wk_carry_time_minute = 0;
                        break;
                    case "1": //切上げ
                    default:
                        $arr_paid_hol[0][$idx] += 1; //前年繰越に繰越分加算
                        $arr_paid_hol[2][$idx] += 1; //当年に繰越分加算
                        $wk_carry_time_minute = 0;
                        break;

                }
            }
            $arr_paid_hol[4][$idx] = $wk_carry_time_minute;
		}
		if (pg_num_rows($sel) > 0) {
			$days1 = pg_fetch_result($sel, 0, "days1");
			$days2 = pg_fetch_result($sel, 0, "days2");
			$days3 = pg_fetch_result($sel, 0, "days3");
			$days4 = pg_fetch_result($sel, 0, "days4");
		} else {
			$days1 = "";
			$days2 = "";
			$days3 = "";
			$days4 = "";
		}
		//初期表示時、2年目以降で、付与日がない場合
		//当日が前年の付与日から1年以内なら、前年にする
		$idx = $year - $start_year;
		if ($org_year == "" &&
			$idx > 0 && $arr_paid_hol_add_mmdd[$idx] == "") {
			$today = date("Ymd");
			$wk_ymd = $year.$arr_paid_hol_add_mmdd[$idx-1];
			if ($today < $wk_ymd) {
				$year--;
				$wk_paid_hol_add_mmdd =$arr_paid_hol_add_mmdd[$idx-1];
			}
		}


		$wk_month = ($criteria_months == "1") ? "3" : "6";
		$emp_join = "";
		$data_migration_date = "";
		$curr_use = "";
		//移行データの前回付与日の範囲も確認する 20120710
		$sql = "select a.emp_join, b.data_migration_date, b.curr_use, b.last_add_date, to_char(to_timestamp(case when emppaid.paid_hol_start_date is not null and  emppaid.paid_hol_start_date != '' then emppaid.paid_hol_start_date when emppaid.paid_hol_emp_join is not null and emppaid.paid_hol_emp_join != '' then emppaid.paid_hol_emp_join when f.paid_hol_start_date != '' then f.paid_hol_start_date else emp_join end, 'yyyymmdd')  + '".$wk_month." months', 'yyyymmdd') as paid_hol_add_date, to_char(to_timestamp(case when emppaid.paid_hol_start_date is not null and  emppaid.paid_hol_start_date != '' then emppaid.paid_hol_start_date when emppaid.paid_hol_emp_join is not null and emppaid.paid_hol_emp_join != '' then emppaid.paid_hol_emp_join when f.paid_hol_start_date != '' then f.paid_hol_start_date else emp_join end, 'yyyymmdd')  + '12 months', 'yyyymmdd') as year_after_join, emppaid.paid_hol_start_date as reg_start_date,emppaid.paid_hol_emp_join as reg_emp_join, f.paid_hol_start_date   ";
        $sql .= " , m.curr_use as hol_hour_curr_use, m.data_migration_date as hol_hour_data_migration_date "; //時間有休 20120806
        $sql .= " from empmst a ";
        $sql .= " left join emppaid on emppaid.emp_id = a.emp_id and emppaid.year = '$year' ";
		$sql .= " left join timecard_paid_hol_import b on b.emp_personal_id = a.emp_personal_id ";
		$sql .= " left join empcond f on f.emp_id = a.emp_id ";
        //時間有休移行データ 20120806
        $sql .= " left join timecard_paid_hol_hour_import m on m.emp_personal_id = a.emp_personal_id ";
        $cond = " where a.emp_id = '$emp_id' ";
		$sel_emp = select_from_table($con, $sql, $cond, $fname);
		if ($sel_emp == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		if (pg_numrows($sel_emp) >0) {
			$reg_start_date = pg_fetch_result($sel_emp,0,"reg_start_date");
			$reg_emp_join = pg_fetch_result($sel_emp,0,"reg_emp_join");
			$paid_hol_start_date = pg_fetch_result($sel_emp,0,"paid_hol_start_date");
			if ($reg_start_date != "") {
				$emp_join = $reg_start_date;
			} elseif ($reg_emp_join != ""){
				$emp_join = $reg_emp_join;
			} elseif ($paid_hol_start_date != "") {
				$emp_join = $paid_hol_start_date;
			} else {
				$emp_join = pg_fetch_result($sel_emp,0,"emp_join");
			}
			$paid_hol_add_date = pg_fetch_result($sel_emp,0,"paid_hol_add_date");
			$data_migration_date = pg_fetch_result($sel_emp,0,"data_migration_date");
			$curr_use = pg_fetch_result($sel_emp,0,"curr_use");
			$last_add_date = pg_fetch_result($sel_emp,0,"last_add_date"); //移行データの前回付与日の範囲も確認する 20120710
            $hol_hour_curr_use = pg_fetch_result($sel_emp,0,"hol_hour_curr_use");
            $hol_hour_data_migration_date = pg_fetch_result($sel_emp,0,"hol_hour_data_migration_date");
        }
		//付与日
		switch ($closing_paid_holiday) {
			case "1":
				$wk_add_mmdd = "4/1";
				break;
			case "2":
				$wk_add_mmdd = "1/1";
				break;
			case "3":
				$wk_add_mmdd = "";
				//付与日が登録されている場合
				if ($wk_paid_hol_add_mmdd != "") {
					$wk_add_mmdd = intval(substr($wk_paid_hol_add_mmdd, 0, 2))."/".intval(substr($wk_paid_hol_add_mmdd, 2, 2));
				}
				else {
					if ($criteria_months == "2") {
						if ($emp_join > 0) {
							//採用日の6ヶ月後とする 20091214
							//					$wk_join_timestamp = date_utils::to_timestamp_from_ymd($emp_join);
							//					$wk_6month_later = date("Ymd", strtotime("+6 month", $wk_join_timestamp));
							$wk_add_mmdd = intval(substr($paid_hol_add_date, 4, 2))."/".intval(substr($paid_hol_add_date, 6, 2));
							if ($org_year == "") {
								$mmdd = date("md");
								if ($mmdd < substr($paid_hol_add_date, 4,4)) {
									$year = date("Y");
									$year--;
								}
							}
						}
					}
					//3ヶ月基準の場合、付与日、起算日、採用日の優先順
					else {
						//$year_after_join = $row["year_after_join"];
						//入職後1年以上か確認
						if (substr($year_after_join, 0, 4) <= $year) {
							$wk_add_mmdd = intval(substr($year_after_join, 4, 2))."/".intval(substr($year_after_join, 6, 2));

						} else {
							$wk_add_mmdd = intval(substr($paid_hol_add_date, 4, 2))."/".intval(substr($paid_hol_add_date, 6, 2));
						}
					}
				}
				$wk_three_month_after_join = intval(substr($paid_hol_add_date, 4, 2))."/".intval(substr($paid_hol_add_date, 6, 2));
				$wk_year_after_join = intval(substr($year_after_join, 4, 2))."/".intval(substr($year_after_join, 6, 2));
				break;
		}


	}
    //flgが"1"以外
    else {

		$sql = "select days1, days2, days3, days4 from emppaid";
		$cond = "where emp_id = '$emp_id' and year = '$year'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (pg_num_rows($sel) > 0) {
			$days1 = pg_fetch_result($sel, 0, "days1");
			$days2 = pg_fetch_result($sel, 0, "days2");
			$days3 = pg_fetch_result($sel, 0, "days3");
			$days4 = pg_fetch_result($sel, 0, "days4");
		} else {
			$days1 = "";
			$days2 = "";
			$days3 = "";
			$days4 = "";
		}

	}
	// YYYY年4月度〜YYYY＋1年3月度までの期間配列を作成
    $arr_term = create_term_array($year, $closing, $closing_paid_holiday, $closing_month_flg);

	// 締め状況チェック、年度開始日〜年度終了日の配列を作成
	$arr_date = array();
	//繰越日数登録以外の場合
	if ($flg != "1") {
		//出勤表関連共通クラス
		$atdbk_common_class = new atdbk_common_class($con, $fname);

		for ($i = 0; $i < count($arr_term); $i++) {
			$tmp_yyyymm = substr($arr_term[$i]["start"], 0, 6);
			$sql = "select count(*) from atdbkclose";
			$cond = "where emp_id = '$emp_id' and yyyymm = '$tmp_yyyymm'";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$arr_term[$i]["closed"] = (pg_fetch_result($sel, 0, 0) > 0);

			// 締め未済の場合
			if (!$arr_term[$i]["closed"]) {

				// 年度開始日〜年度終了日の配列を作成
				$tmp_date = $arr_term[$i]["start"];
				while ($tmp_date <= $arr_term[$i]["end"]) {
					$arr_date[$tmp_date] = array();
					$arr_date[$tmp_date]["type"] = "";
					$arr_date[$tmp_date]["prov_tmcd_group_id"] = "";
					$arr_date[$tmp_date]["prov_pattern"] = "";
					$arr_date[$tmp_date]["prov_reason"] = "";
					$arr_date[$tmp_date]["prov_start_time"] = "";
					$arr_date[$tmp_date]["prov_end_time"] = "";
					$arr_date[$tmp_date]["tmcd_group_id"] = "";
					$arr_date[$tmp_date]["pattern"] = "";
					$arr_date[$tmp_date]["reason"] = "";
					$arr_date[$tmp_date]["start_time"] = "";
					$arr_date[$tmp_date]["end_time"] = "";
					$arr_date[$tmp_date]["out_time"] = "";
					$arr_date[$tmp_date]["ret_time"] = "";
					for ($j = 1; $j <= 10; $j++) {
						$start_var = "o_start_time$j";
						$end_var = "o_end_time$j";
						$arr_date[$tmp_date][$start_var] = "";
						$arr_date[$tmp_date][$end_var] = "";
					}
					$arr_date[$tmp_date]["term_index"] = $i;
					$tmp_date = next_date($tmp_date);
				}

				// 勤務日種別を格納
				$sql = "select date, type from calendar";
				$cond = "where date between '{$arr_term[$i]["start"]}' and '{$arr_term[$i]["end"]}' order by date";
				$sel = select_from_table($con, $sql, $cond, $fname);
				if ($sel == 0) {
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
				while ($row = pg_fetch_array($sel)) {
					$arr_date[$row["date"]]["type"] = $row["type"];
				}

				// 勤務予定情報を格納
				$sql = "select date, tmcd_group_id, pattern, reason, prov_start_time, prov_end_time from atdbk";
				$cond = "where emp_id = '$emp_id' and date between '{$arr_term[$i]["start"]}' and '{$arr_term[$i]["end"]}' order by date";
				$sel = select_from_table($con, $sql, $cond, $fname);
				if ($sel == 0) {
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
				while ($row = pg_fetch_array($sel)) {
					$arr_date[$row["date"]]["prov_tmcd_group_id"] = $row["prov_tmcd_group_id"];
					$arr_date[$row["date"]]["prov_pattern"] = $row["prov_pattern"];
					$arr_date[$row["date"]]["prov_reason"] = $row["prov_reason"];
					$arr_date[$row["date"]]["prov_start_time"] = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $row["prov_start_time"]);
					$arr_date[$row["date"]]["prov_end_time"] = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $row["prov_end_time"]);
				}

				// 勤務実績を格納
				$sql = "select ".
							"atdbkrslt.*, ".
							"atdptn.base_time, ".
							"atdptn.over_24hour_flag, ".
							"atdptn.out_time_nosubtract_flag, ".
                            "atdptn.previous_day_flag as atdptn_previous_day_flag, ".
                            "COALESCE(atdbk_reason_mst.day_count, 0) AS reason_day_count ".
						"from ".
							"atdbkrslt ".
								"LEFT JOIN atdptn ON atdbkrslt.tmcd_group_id = atdptn.group_id AND atdbkrslt.pattern = CAST(atdptn.atdptn_id AS varchar) ".
								"LEFT JOIN atdbk_reason_mst ON atdbkrslt.reason = atdbk_reason_mst.reason_id";
				$cond = "where emp_id = '$emp_id' and date between '{$arr_term[$i]["start"]}' and '{$arr_term[$i]["end"]}' order by date";
				$sel = select_from_table($con, $sql, $cond, $fname);
				if ($sel == 0) {
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
				while ($row = pg_fetch_array($sel)) {
					$arr_date[$row["date"]]["tmcd_group_id"] = $row["tmcd_group_id"];
					$arr_date[$row["date"]]["pattern"] = $row["pattern"];
					$arr_date[$row["date"]]["reason"] = $row["reason"];
					$arr_date[$row["date"]]["start_time"] = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $row["start_time"]);
					$arr_date[$row["date"]]["end_time"] = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $row["end_time"]);
					$arr_date[$row["date"]]["out_time"] = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $row["out_time"]);
					$arr_date[$row["date"]]["ret_time"] = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $row["ret_time"]);

					for ($j = 1; $j <= 10; $j++) {
						$start_var = "o_start_time$j";
						$end_var = "o_end_time$j";
						$$start_var = $row["$start_var"];
						$$end_var = $row["$end_var"];

						$arr_date[$row["date"]][$start_var] = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $$start_var);
						$arr_date[$row["date"]][$end_var] = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $$end_var);
					}
					$arr_date[$row["date"]]["reason_day_count"] = $row["reason_day_count"];

					$arr_date[$row["date"]]["night_duty"]        = $row["night_duty"];
					$arr_date[$row["date"]]["previous_day_flag"] = $row["previous_day_flag"];
					$arr_date[$row["date"]]["next_day_flag"]     = $row["next_day_flag"];
					$arr_date[$row["date"]]["base_time"]     = $row["base_time"];
					//残業時刻追加 20100114
					$arr_date[$row["date"]]["over_start_time"] = $row["over_start_time"];
					$arr_date[$row["date"]]["over_end_time"] = $row["over_end_time"];
					$arr_date[$row["date"]]["over_start_next_day_flag"] = $row["over_start_next_day_flag"];
					$arr_date[$row["date"]]["over_end_next_day_flag"] = $row["over_end_next_day_flag"];
					//残業時刻2追加 20110628
					$arr_date[$row["date"]]["over_start_time2"] = $row["over_start_time2"];
					$arr_date[$row["date"]]["over_end_time2"] = $row["over_end_time2"];
					$arr_date[$row["date"]]["over_start_next_day_flag2"] = $row["over_start_next_day_flag2"];
					$arr_date[$row["date"]]["over_end_next_day_flag2"] = $row["over_end_next_day_flag2"];
					$arr_date[$row["date"]]["over_24hour_flag"] = $row["over_24hour_flag"];
					$arr_date[$row["date"]]["out_time_nosubtract_flag"] = $row["out_time_nosubtract_flag"];
                    $arr_date[$row["date"]]["atdptn_previous_day_flag"] = $row["atdptn_previous_day_flag"];
                }

			// 締め済みの場合
			} else {
				$sql = "select days4, days5, days8, days6, days7, days_ex1, days_ex2, days_ex3, days10, count1, time1, count2, time2, time3, time4, time5, time6, days_ex4, days_ex5, days11, days12, days13, days14, days15, days16, days17, days18, days19, days20, days21, days22, days_ex6, days_ex7, days_ex8 from wktotalm";
				$cond = "where emp_id = '$emp_id' and yyyymm = '$tmp_yyyymm'";
				$sel = select_from_table($con, $sql, $cond, $fname);
				if ($sel == 0) {
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
				$arr_term[$i]["days4"] = pg_fetch_result($sel, 0, "days4");
				$arr_term[$i]["days5"] = pg_fetch_result($sel, 0, "days5");
				$arr_term[$i]["days8"] = pg_fetch_result($sel, 0, "days8");
				$arr_term[$i]["days6"] = pg_fetch_result($sel, 0, "days6");
				$arr_term[$i]["days7"] = pg_fetch_result($sel, 0, "days7");
				$arr_term[$i]["days_ex1"] = pg_fetch_result($sel, 0, "days_ex1");
				$arr_term[$i]["days_ex2"] = pg_fetch_result($sel, 0, "days_ex2");
				$arr_term[$i]["days_ex3"] = pg_fetch_result($sel, 0, "days_ex3");
				$arr_term[$i]["days10"] = pg_fetch_result($sel, 0, "days10");
				$arr_term[$i]["count1"] = pg_fetch_result($sel, 0, "count1");
				$arr_term[$i]["time1"] = pg_fetch_result($sel, 0, "time1");
				$arr_term[$i]["count2"] = pg_fetch_result($sel, 0, "count2");
				$arr_term[$i]["time2"] = pg_fetch_result($sel, 0, "time2");
				$arr_term[$i]["time3"] = pg_fetch_result($sel, 0, "time3");
				$arr_term[$i]["time4"] = pg_fetch_result($sel, 0, "time4");
				$arr_term[$i]["time5"] = pg_fetch_result($sel, 0, "time5");
				$arr_term[$i]["time6"] = pg_fetch_result($sel, 0, "time6");
				$arr_term[$i]["days_ex4"] = pg_fetch_result($sel, 0, "days_ex4");
				$arr_term[$i]["days_ex5"] = pg_fetch_result($sel, 0, "days_ex5");
				if (pg_fetch_result($sel, 0, "days11") != "") {
					$arr_term[$i]["days11"] = pg_fetch_result($sel, 0, "days11");
					$arr_term[$i]["days12"] = pg_fetch_result($sel, 0, "days12");
					$arr_term[$i]["days13"] = pg_fetch_result($sel, 0, "days13");
					$arr_term[$i]["days14"] = pg_fetch_result($sel, 0, "days14");
					$arr_term[$i]["days15"] = pg_fetch_result($sel, 0, "days15");
					$arr_term[$i]["days16"] = pg_fetch_result($sel, 0, "days16");
					$arr_term[$i]["days17"] = pg_fetch_result($sel, 0, "days17");
					$arr_term[$i]["days18"] = pg_fetch_result($sel, 0, "days18");
					$arr_term[$i]["days19"] = pg_fetch_result($sel, 0, "days19");
					$arr_term[$i]["days20"] = pg_fetch_result($sel, 0, "days20");
					$arr_term[$i]["days21"] = pg_fetch_result($sel, 0, "days21");
					$arr_term[$i]["days22"] = pg_fetch_result($sel, 0, "days22");
					$arr_term[$i]["days_ex6"] = pg_fetch_result($sel, 0, "days_ex6");
					$arr_term[$i]["days_ex7"] = pg_fetch_result($sel, 0, "days_ex7");
					$arr_term[$i]["days_ex8"] = pg_fetch_result($sel, 0, "days_ex8");
				} else {
				// 追加の事由が未設定の場合の対処、atdbkrsltから取得
					$sql = "select ".
							"atdbkrslt.reason, ".
							"sum(COALESCE(atdbk_reason_mst.day_count, 0)) AS reason_day_count ".
						"from ".
							"atdbkrslt ".
								"LEFT JOIN atdbk_reason_mst ON atdbkrslt.reason = atdbk_reason_mst.reason_id";
					$cond = "where emp_id = '$emp_id' and date between '{$arr_term[$i]["start"]}' and '{$arr_term[$i]["end"]}' group by atdbkrslt.reason";
					$sel_cnt = select_from_table($con, $sql, $cond, $fname);
					if ($sel_cnt == 0) {
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
					while ($row = pg_fetch_array($sel_cnt)) {
						$tmp_reason = $row["reason"];
						$tmp_daycount = $row["reason_day_count"];
						switch ($tmp_reason) {
						case "24":  // 公休
						case "22":  // 法定休暇
						case "23":  // 所定休暇
							$arr_term[$i]["days11"] = 1;
							break;
						case "35":  // 午前公休
						case "36":  // 午後公休
							$arr_term[$i]["days11"] = 0.5;
							break;
						case "25":  // 産休
							$arr_term[$i]["days12"] = 1;
							break;
						case "5":  // 特別休暇
							$arr_term[$i]["days13"] = 1;
							break;
						case "26":  // 育児休業
							$arr_term[$i]["days14"] = 1;
							break;
						case "27":  // 介護休業
							$arr_term[$i]["days15"] = 1;
							break;
						case "28":  // 傷病休職
							$arr_term[$i]["days16"] = 1;
							break;
						case "29":  // 学業休職
							$arr_term[$i]["days17"] = 1;
							break;
						case "30":  // 忌引
							$arr_term[$i]["days18"] = 1;
							break;
						case "31":   // 夏休
							$arr_term[$i]["days19"] = 1;
							break;
						case "32":   // 結婚休
							$arr_term[$i]["days20"] = 1;
							break;
						case "40":   // リフレッシュ休暇
							$arr_term[$i]["days21"] = 1;
							break;
						case "41":  // 初盆休暇
							$arr_term[$i]["days22"] = 1;
							break;
						}
					}
					// 休日出勤通常勤務日
					$sql = "select ".
							"sum(COALESCE(atdbk_reason_mst.day_count, 0)) AS reason_day_count ".
						"from ".
							"atdbkrslt ".
								"LEFT JOIN atdbk_reason_mst ON atdbkrslt.reason = atdbk_reason_mst.reason_id LEFT JOIN calendar ON calendar.date = atdbkrslt.date";
					$cond = "where emp_id = '$emp_id' and atdbkrslt.date between '{$arr_term[$i]["start"]}' and '{$arr_term[$i]["end"]}' and atdbkrslt.reason = '16' and calendar.type = '1'";
					$sel_cnt = select_from_table($con, $sql, $cond, $fname);
					if ($sel_cnt == 0) {
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
					$arr_term[$i]["days_ex6"] = pg_fetch_result($sel_cnt, 0, "reason_day_count");
					//過去分は0とする
					$arr_term[$i]["days_ex7"] = 0;
					$arr_term[$i]["days_ex8"] = 0;

				}
			}
		}

		//追加表示する事由の表示フラグ
		$arr_disp_flg = array();
		//追加表示する事由ID、公休以降
		$arr_reason_id = array("24", "25", "26", "27", "28", "29", "30", "31", "32", "40", "41");
		for ($i=0; $i<count($arr_reason_id); $i++) {
			$tmp_display_flag = $atdbk_common_class->get_reason_display_flag($arr_reason_id[$i]);
			$arr_disp_flg[$i] = $tmp_display_flag;
		}
		// 休暇欠勤日数を配列に格納
		$header1 = array();
		$header1[0] = array("name" => "代替出勤", "align" => "right");
		$header1[1] = array("name" => "振替出勤", "align" => "right");
		$header1[2] = array("name" => "年休・有休", "align" => "right");
		$header1[3] = array("name" => "代替休暇", "align" => "right");
		$header1[4] = array("name" => "振替休暇", "align" => "right");
//特別休暇、名称をマスタから取得
//		$header1[5] = array("name" => "特別休暇", "align" => "right");
		$wk_name = $atdbk_common_class->get_reason_name("5");
		$header1[5] = array("name" => $wk_name, "align" => "right");
		$header1[6] = array("name" => "一般欠勤", "align" => "right");
		$header1[7] = array("name" => "病傷欠勤", "align" => "right");
		$header1[8] = array("name" => "その他休", "align" => "right");

//事由をマスタから取得
		for ($k=0;$k<count($arr_reason_id); $k++) {
			$wk_name = $atdbk_common_class->get_reason_name($arr_reason_id[$k]);
			$header1[$k+9] = array("name" => $wk_name, "align" => "right");
		}
/*
		$header1[9] = array("name" => "公休", "align" => "right");
		$header1[10] = array("name" => "産休", "align" => "right");
		$header1[11] = array("name" => "育児休業", "align" => "right");
		$header1[12] = array("name" => "介護休業", "align" => "right");
		$header1[13] = array("name" => "傷病休職", "align" => "right");
		$header1[14] = array("name" => "学業休職", "align" => "right");
		$header1[15] = array("name" => "忌引", "align" => "right");
		$header1[16] = array("name" => "夏休", "align" => "right");
		$header1[17] = array("name" => "結婚休", "align" => "right");
		$header1[18] = array("name" => "リフレッシュ", "align" => "right");
		$header1[19] = array("name" => "初盆休暇", "align" => "right");
*/
		$arr_data1 = array();
/* 年休の表示を行わない 20100707
		if ($flg == 2) {
			// 年休使用日数を取得
			$start_yyyymm = substr($arr_term[0]["start"], 0, 6);
			$end_yyyymm = substr($arr_term[11]["start"], 0, 6);
			$arr_paid_hol_cnt = get_paid_hol_cnt($con, $fname, $emp_id, $start_yyyymm);
			//有休を設定
			for ($i=0; $i<12; $i++) {
				$arr_data1[2][$i] += $arr_paid_hol_cnt[$i];
				$arr_data1[2][12] += $arr_paid_hol_cnt[$i];
			}
		}
*/
		//公休と判定する日設定 20100810
		$arr_timecard_holwk_day_std = get_timecard_holwk_day($con, $fname, "");

		// 処理日付の勤務日種別を取得 20100810
		$wk_start_date = $arr_term[0]["start"];
		$wk_end_date = $arr_term[11]["end"];
		$arr_type = array();
		$sql = "select date, type from calendar";
		$cond = "where date >= '$wk_start_date' and date <= '$wk_end_date'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		while ($row = pg_fetch_array($sel)) {
			$date = $row["date"];
			$arr_type[$date] = $row["type"];
		}
		//日数はマスタからではなく固定とする ※マスタの日数は有休用のため 20100809
		foreach ($arr_date as $tmp_date => $arr_tmp) {
			//$reason_day_count = $arr_tmp["reason_day_count"];
			switch ($arr_tmp["reason"]) {
			case "14":  // 代替出勤
				$arr_data1[0][$arr_tmp["term_index"]] += 1;
				$arr_data1[0][12] += 1;
				break;
			case "15":  // 振替出勤
				$arr_data1[1][$arr_tmp["term_index"]] += 1;
				$arr_data1[1][12] += 1;
				break;
//			case "1":  // 有給休暇
//			case "2":  // 午前有休
//			case "3":  // 午後有休
//			case "37": // 年休
//			case "38": // 午前年休
//			case "39": // 午後年休
//				$arr_data1[2][$arr_tmp["term_index"]] += $reason_day_count;
//				$arr_data1[2][12] += $reason_day_count;
//				break;
			case "4":  // 代替休暇
					$arr_data1[3][$arr_tmp["term_index"]] += 1;
					$arr_data1[3][12] += 1;
					break;
				case "18":  // 半前代替休
			case "19":  // 半後代替休
				$arr_data1[3][$arr_tmp["term_index"]] += 0.5;
				$arr_data1[3][12] += 0.5;
				break;
			case "17":  // 振替休暇
					$arr_data1[4][$arr_tmp["term_index"]] += 1;
					$arr_data1[4][12] += 1;
					break;
				case "20":  // 半前振替休
			case "21":  // 半後振替休
					$arr_data1[4][$arr_tmp["term_index"]] += 0.5;
					$arr_data1[4][12] += 0.5;
				break;
			case "5":  // 特別休暇
				$arr_data1[5][$arr_tmp["term_index"]] += 1;
				$arr_data1[5][12] += 1;
				break;
			case "6":  // 一般欠勤
				$arr_data1[6][$arr_tmp["term_index"]] += 1;
				$arr_data1[6][12] += 1;
				break;
			case "7":  // 病傷欠勤
				$arr_data1[7][$arr_tmp["term_index"]] += 1;
				$arr_data1[7][12] += 1;
				break;
			case "8":  // その他休
				$arr_data1[8][$arr_tmp["term_index"]] += 1;
				$arr_data1[8][12] += 1;
				break;
			case "24":  // 公休
//			case "22":  // 法定休暇
//			case "23":  // 所定休暇
			case "45":  // 希望(公休)
			case "46":  // 待機(公休)
			case "47":  // 管理当直前(公休)
				$arr_data1[9][$arr_tmp["term_index"]] += 1;
				$arr_data1[9][12] += 1;
				break;
				case "35":  // 午前公休
				case "36":  // 午後公休
					$arr_data1[9][$arr_tmp["term_index"]] += 0.5;
					$arr_data1[9][12] += 0.5;
					break;
				case "25":  // 産休
				$arr_data1[10][$arr_tmp["term_index"]] += 1;
				$arr_data1[10][12] += 1;
				break;
			case "26":  // 育児休業
				$arr_data1[11][$arr_tmp["term_index"]] += 1;
				$arr_data1[11][12] += 1;
				break;
			case "27":  // 介護休業
				$arr_data1[12][$arr_tmp["term_index"]] += 1;
				$arr_data1[12][12] += 1;
				break;
			case "28":  // 傷病休職
				$arr_data1[13][$arr_tmp["term_index"]] += 1;
				$arr_data1[13][12] += 1;
				break;
			case "29":  // 学業休職
				$arr_data1[14][$arr_tmp["term_index"]] += 1;
				$arr_data1[14][12] += 1;
				break;
			case "30":  // 忌引
				$arr_data1[15][$arr_tmp["term_index"]] += 1;
				$arr_data1[15][12] += 1;
				break;
			case "31":   // 夏休
				$arr_data1[16][$arr_tmp["term_index"]] += 1;
				$arr_data1[16][12] += 1;
				break;
			case "32":   // 結婚休
				$arr_data1[17][$arr_tmp["term_index"]] += 1;
				$arr_data1[17][12] += 1;
				break;
			case "40":   // リフレッシュ休暇
					$arr_data1[18][$arr_tmp["term_index"]] += 1;
					$arr_data1[18][12] += 1;
					break;
				case "42":   // 午前リフレッシュ休暇
			case "43":   // 午後リフレッシュ休暇
					$arr_data1[18][$arr_tmp["term_index"]] += 0.5;
					$arr_data1[18][12] += 0.5;
				break;
			case "41":  // 初盆休暇
				$arr_data1[19][$arr_tmp["term_index"]] += 1;
				$arr_data1[19][12] += 1;
				break;
			case "44":  // 半有半公
//				$arr_data1[2][$arr_tmp["term_index"]] += 0.5;
//				$arr_data1[2][12] += 0.5;
				$arr_data1[9][$arr_tmp["term_index"]] += 0.5;
				$arr_data1[9][12] += 0.5;
				break;
				case "54":  // 半夏半公
					$arr_data1[16][$arr_tmp["term_index"]] += 0.5;
					$arr_data1[16][12] += 0.5;
					$arr_data1[9][$arr_tmp["term_index"]] += 0.5;
					$arr_data1[9][12] += 0.5;
					break;
				case "55":  // 半夏半有
					$arr_data1[16][$arr_tmp["term_index"]] += 0.5;
					$arr_data1[16][12] += 0.5;
					break;
				case "56":  // 半夏半欠
					$arr_data1[16][$arr_tmp["term_index"]] += 0.5;
					$arr_data1[16][12] += 0.5;
					$arr_data1[6][$arr_tmp["term_index"]] += 0.5;
					$arr_data1[6][12] += 0.5;
					break;
				case "72":  // 半夏半特
					$arr_data1[16][$arr_tmp["term_index"]] += 0.5;
					$arr_data1[16][12] += 0.5;
					$arr_data1[5][$arr_tmp["term_index"]] += 0.5;
					$arr_data1[5][12] += 0.5;
					break;
				case "67":  // 午前特別
				case "68":  // 午後特別
				case "58":  // 半特半有
					$arr_data1[5][$arr_tmp["term_index"]] += 0.5;
					$arr_data1[5][12] += 0.5;
					break;
				case "59":  // 半特半公
					$arr_data1[5][$arr_tmp["term_index"]] += 0.5;
					$arr_data1[5][12] += 0.5;
					$arr_data1[9][$arr_tmp["term_index"]] += 0.5;
					$arr_data1[9][12] += 0.5;
					break;
				case "60":  // 半特半欠
					$arr_data1[5][$arr_tmp["term_index"]] += 0.5;
					$arr_data1[5][12] += 0.5;
					$arr_data1[6][$arr_tmp["term_index"]] += 0.5;
					$arr_data1[6][12] += 0.5;
					break;
				case "63":  // 半正半公
					//$arr_data1[][$arr_tmp["term_index"]] += 0.5;
					//$arr_data1[][12] += 0.5;
					$arr_data1[9][$arr_tmp["term_index"]] += 0.5;
					$arr_data1[9][12] += 0.5;
					break;
				case "64":  // 半正半欠
					$arr_data1[6][$arr_tmp["term_index"]] += 0.5;
					$arr_data1[6][12] += 0.5;
					break;
				case "57": //半有半欠
					$arr_data1[6][$arr_tmp["term_index"]] += 0.5;
					$arr_data1[6][12] += 0.5;
					break;
			}
			// 休日となる設定と事由があう場合に公休に計算する 20100810
			// 法定休日
			if ($arr_type[$tmp_date] == "4" && $arr_timecard_holwk_day_std[0]["legal_holiday"] == "t") {
				if ($arr_tmp["reason"] == "22") {
					$arr_data1[9][$arr_tmp["term_index"]] += 1;
					$arr_data1[9][12] += 1;
				}
			}
			// 所定休日
			if ($arr_type[$tmp_date] == "5" && $arr_timecard_holwk_day_std[0]["prescribed_holiday"] == "t") {
				if ($arr_tmp["reason"] == "23") {
					$arr_data1[9][$arr_tmp["term_index"]] += 1;
					$arr_data1[9][12] += 1;
				}
			}
			// 年末年始
			if ($arr_type[$tmp_date] == "7" && $arr_timecard_holwk_day_std[0]["newyear_holiday"] == "t") {
				if ($arr_tmp["reason"] == "61") {
					$arr_data1[9][$arr_tmp["term_index"]] += 1;
					$arr_data1[9][12] += 1;
				}
				if ($arr_tmp["reason"] == "62" || $arr_tmp["reason"] == "63" || $arr_tmp["reason"] == "64") {
					$arr_data1[9][$arr_tmp["term_index"]] += 0.5;
					$arr_data1[9][12] += 0.5;
				}
			}
		}
		for ($i = 0; $i < count($arr_term); $i++) {
			if ($arr_term[$i]["closed"]) {
				$arr_data1[0][$i] += $arr_term[$i]["days4"];
				$arr_data1[1][$i] += $arr_term[$i]["days5"];
//				$arr_data1[2][$i] += $arr_term[$i]["days8"];
				$arr_data1[3][$i] += $arr_term[$i]["days6"];
				$arr_data1[4][$i] += $arr_term[$i]["days7"];
				$arr_data1[5][$i] += $arr_term[$i]["days_ex1"];
				$arr_data1[6][$i] += $arr_term[$i]["days_ex2"];
				$arr_data1[7][$i] += $arr_term[$i]["days_ex3"];
				$arr_data1[8][$i] += $arr_term[$i]["days10"];
				// 追加事由分が未設定の場合の対処
				$arr_data1[9][$i] += $arr_term[$i]["days11"];
//				$arr_data1[8][$i] += $arr_term[$i]["days12"]; // 特別は[5]に既にある
				$arr_data1[10][$i] += $arr_term[$i]["days13"];
				$arr_data1[11][$i] += $arr_term[$i]["days14"];
				$arr_data1[12][$i] += $arr_term[$i]["days15"];
				$arr_data1[13][$i] += $arr_term[$i]["days16"];
				$arr_data1[14][$i] += $arr_term[$i]["days17"];
				$arr_data1[15][$i] += $arr_term[$i]["days18"];
				$arr_data1[16][$i] += $arr_term[$i]["days19"];
				$arr_data1[17][$i] += $arr_term[$i]["days20"];
				$arr_data1[18][$i] += $arr_term[$i]["days21"];
				$arr_data1[19][$i] += $arr_term[$i]["days22"];
				for ($j = 0; $j <= 19; $j++) {
					$arr_data1[$j][12] += $arr_data1[$j][$i];
				}
			}
		}
		$arr_data1[0][13] = "-";
		$arr_data1[1][13] = "-";
		$arr_data1[2][13] = $days1 + $days2 - $arr_data1[2][12];
//		$arr_data1[3][13] = $days3 + $arr_data1[0][12] - $arr_data1[3][12];//代替休暇,振替休暇 残日数は指定しない2009/02/05
//		$arr_data1[4][13] = $days4 + $arr_data1[1][12] - $arr_data1[4][12];
		$arr_data1[3][13] = "-";
		$arr_data1[4][13] = "-";
		$arr_data1[5][13] = "-";
		$arr_data1[6][13] = "-";
		$arr_data1[7][13] = "-";
		$arr_data1[8][13] = "-";
		for ($idx=9; $idx<=19; $idx++) {
			$arr_data1[$idx][13] = "-";
		}

		// 休日出勤日数を配列に格納
		$header4 = array();
		$header4[0] = array("name" => "所定休日", "align" => "right");
		$header4[1] = array("name" => "法定休日", "align" => "right");
		$header4[2] = array("name" => "通常勤務日", "align" => "right");
		$header4[3] = array("name" => "祝日", "align" => "right");
		$header4[4] = array("name" => "年末年始休暇", "align" => "right");
		$arr_data4 = array();
/*
		foreach ($arr_date as $arr_tmp) {
			if ($arr_tmp["reason"] == "16") {  // 休日出勤
				switch ($arr_tmp["type"]) {
				case "5":  // 所定休日
					$arr_data4[0][$arr_tmp["term_index"]]++;
					$arr_data4[0][12]++;
					break;
				case "4":  // 法定休日
					$arr_data4[1][$arr_tmp["term_index"]]++;
					$arr_data4[1][12]++;
					break;
				case "1":  // 通常勤務日
					$arr_data4[2][$arr_tmp["term_index"]]++;
					$arr_data4[2][12]++;
					break;
				}
			}

		}
*/
		for ($i = 0; $i < count($arr_term); $i++) {
			if ($arr_term[$i]["closed"]) {
				$arr_data4[0][$i] += $arr_term[$i]["days_ex4"];
				$arr_data4[1][$i] += $arr_term[$i]["days_ex5"];
				$arr_data4[2][$i] += $arr_term[$i]["days_ex6"];
				$arr_data4[3][$i] += $arr_term[$i]["days_ex7"];
				$arr_data4[4][$i] += $arr_term[$i]["days_ex8"];
				for ($j = 0; $j <= 4; $j++) {
					$arr_data4[$j][12] += $arr_data4[$j][$i];
				}
			}
		}

		// 遅刻・早退・勤務時間情報を配列に格納
		$header2 = array();
		$header2[0] = array("name" => "<b>遅刻</b>", "align" => "left");
		$header2[1] = array("name" => "回数", "align" => "right");
		$header2[2] = array("name" => "時間", "align" => "right");
		$header2[3] = array("name" => "<b>早退</b>", "align" => "left");
		$header2[4] = array("name" => "回数", "align" => "right");
		$header2[5] = array("name" => "時間", "align" => "right");
		$header3 = array();
		$header3[0] = array("name" => "基準時間", "align" => "right");
		$header3[1] = array("name" => "稼働時間", "align" => "right");
		$header3[2] = array("name" => "残業", "align" => "right");
		$header3[3] = array("name" => "深夜勤務", "align" => "right");
		$arr_data2 = array();
		$arr_data3 = array();
		$arr_you_kinmu_nissu = array();
        //個人別所定時間を優先するための対応 20130220
        $arr_empcond_officehours_ptn = $timecard_common_class->get_empcond_officehours_pattern();
        //時間有休情報取得 20130527
        if ($obj_hol_hour->paid_hol_hour_flag == "t") {
            $arr_hol_hour_dat = $obj_hol_hour->get_paid_hol_hour_month($emp_id, $arr_term[0]["start"], $arr_term[11]["end"]);
        }
        for ($i = 0; $i < count($arr_term); $i++) { //
			if ($arr_term[$i]["closed"]) {
				continue;
			}

			// 月内の日付をループ（変則労働時間・週の場合、週の開始曜日からスタート）
			$start_date = $arr_term[$i]["start"];
			$end_date = $arr_term[$i]["end"];
			$tmp_date = $start_date;
			if ($arr_irrg_info["irrg_type"] == "1") {
				$wd = date("w", to_timestamp($tmp_date));
				while (($arr_irrg_info["irrg_wd"] == "1" && $wd != 0) || ($arr_irrg_info["irrg_wd"] == "2" && $wd != 1)) {
					$tmp_date = last_date($tmp_date);
					$wd = date("w", to_timestamp($tmp_date));
				}
			}
			$counter_for_week = 1;
			$holiday_count = 0;

			// 処理日付の勤務日種別を取得 2008/10/23
			$arr_type = array();
			$sql = "select date, type from calendar";
			$cond = "where date >= '$tmp_date' and date <= '$end_date'";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			while ($row = pg_fetch_array($sel)) {
				$date = $row["date"];
				$arr_type[$date] = $row["type"];
			}

            $arr_weekday_flg = $timecard_common_class->get_arr_weekday_flag($start_date, $end_date); //20130220
            $arr_empcond_officehours = $timecard_common_class->get_arr_empcond_officehours($emp_id, $start_date); //個人別勤務時間帯履歴対応 20130220
            //休日出勤テーブル 2008/10/23
			$arr_hol_work_date = array();
			$arr_hol_work_date1 = array();
			$arr_hol_work_date2 = array();
			$arr_hol_work_date3 = array();
			while ($tmp_date <= $end_date) {
				if ($counter_for_week >= 8) {
					$counter_for_week = 1;
				}
				if ($counter_for_week == 1) {
					$work_time_for_week = 0;
					if ($arr_irrg_info["irrg_type"] == "1") {
						$holiday_count = 0;
					}
				}

				$holiday_name = get_holiday_name($tmp_date);
				if ($holiday_name != "") {
					$holiday_count++;
				}

				if ($tmp_date >= $start_date) {
					$arr_tmp = $arr_date[$tmp_date];
				} else {  // 週の開始曜日がスタート日より前
					$arr_tmp = get_work_result_array($con, $emp_id, $tmp_date, $fname);
				}
				$type = $arr_tmp["type"];
				$prov_tmcd_group_id = $arr_tmp["prov_tmcd_group_id"];
				$prov_pattern = $arr_tmp["prov_pattern"];
				$prov_reason = $arr_tmp["prov_reason"];
				$prov_start_time = $arr_tmp["prov_start_time"];
				$prov_end_time = $arr_tmp["prov_end_time"];
				$tmcd_group_id = $arr_tmp["tmcd_group_id"];
				$pattern = $arr_tmp["pattern"];
				$reason = $arr_tmp["reason"];
				$start_time = $arr_tmp["start_time"];
				$end_time = $arr_tmp["end_time"];
				$out_time = $arr_tmp["out_time"];
				$ret_time = $arr_tmp["ret_time"];
				for ($j = 1; $j <= 10; $j++) {
					$start_var = "o_start_time$j";
					$end_var = "o_end_time$j";
					$$start_var = $arr_tmp["$start_var"];
					$$end_var = $arr_tmp["$end_var"];
				}

				$night_duty        = $arr_tmp["night_duty"];
				$previous_day_flag = $arr_tmp["previous_day_flag"];
				$next_day_flag     = $arr_tmp["next_day_flag"];
				$base_time = $arr_tmp["base_time"];

				//残業時刻追加 20100114
				$over_start_time = $arr_tmp["over_start_time"];
				$over_end_time = $arr_tmp["over_end_time"];
				$over_start_next_day_flag = $arr_tmp["over_start_next_day_flag"];
				$over_end_next_day_flag = $arr_tmp["over_end_next_day_flag"];
				if ($over_start_time != "") {
					$over_start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_start_time);
				}
				if ($over_end_time != "") {
					$over_end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_end_time);
				}
				//残業時刻2追加 20110628
				$over_start_time2 = $arr_tmp["over_start_time2"];
				$over_end_time2 = $arr_tmp["over_end_time2"];
				$over_start_next_day_flag2 = $arr_tmp["over_start_next_day_flag2"];
				$over_end_next_day_fla2g = $arr_tmp["over_end_next_day_flag2"];
				if ($over_start_time2 != "") {
					$over_start_time2 = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_start_time2);
				}
				if ($over_end_time2 != "") {
					$over_end_time2 = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_end_time2);
				}
				$over_24hour_flag = $arr_tmp["over_24hour_flag"];
				//外出時間を差し引かないフラグ 20110727
				$out_time_nosubtract_flag = $arr_tmp["out_time_nosubtract_flag"];
                //時間帯画面の前日フラグ 20121120
                $atdptn_previous_day_flag = $arr_atdbkrslt[$tmp_date]["atdptn_previous_day_flag"];

				//残業終了時刻がある場合は、退勤時刻へ設定する 20100114
				if ($over_end_time != "") {
					//日またがり対応
					if ($over_end_next_day_flag == "1" ||
							($start_time > $over_end_time &&
								$previous_day_flag == "0")) {
						$next_day_flag = "1";
					}
					$end_time = $over_end_time;
				}

				//当直の有効判定
				$night_duty_flag = $timecard_common_class->is_valid_duty($night_duty, $tmp_date, $start_time, $previous_day_flag, $end_time, $next_day_flag, $tmcd_group_id);

				if ($night_duty_flag){
					//当直時間の取得
					$night_duty_time_array = $timecard_common_class->get_duty_time_array($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);

					//日数換算の数値ｘ通常勤務日の労働時間の時間
					$duty_work_time = date_utils::hi_to_minute($day1_time) * $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);
				}

				// 各時間帯の開始時刻・終了時刻を変数に格納
				$tmp_type = ($type != "2" && $type != "3") ? "1" : $type;
				$start2 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start2" );
				$end2 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end2" );
				$start4 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start4" );
				$end4 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end4" );
				$start5 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start5" );
				$end5 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end5" );

                //個人別勤務時間帯履歴対応 20130220 start
                if ($arr_empcond_officehours_ptn[$tmcd_group_id][$pattern] == 1) {
                    $wk_weekday = $arr_weekday_flg[$tmp_date];
                    $arr_empcond_officehours = $timecard_common_class->get_empcond_officehours($wk_weekday, $tmp_date);
                    if ($arr_empcond_officehours["officehours2_start"] != "") {
                        $start2 = $arr_empcond_officehours["officehours2_start"];
                        $end2 = $arr_empcond_officehours["officehours2_end"];
                        $start4 = $arr_empcond_officehours["officehours4_start"];
                        $end4 = $arr_empcond_officehours["officehours4_end"];
                    }
                }
                //個人別勤務時間帯履歴対応 20130220 end
                // 所定労働時間帯が確定できない場合、予定時刻を所定労働時間帯として使用
				if (($start2 == "" || $end2 == "") && $prov_start_time != "" && $prov_end_time != "") {
					$start2 = $prov_start_time;
					$end2 = $prov_end_time;
				}
				//残業時刻追加 20100114
				//残業開始が予定終了時刻より前の場合に設定
				if ($over_start_time != "") {
					//$tmp_end2_date_time = ($start2 > $end2 && $previous_day_flag == 0) ? next_date($tmp_date).$end2 : $tmp_date.$end2;
					//残業開始日時
					if ($over_start_next_day_flag == "1" ||
							($start_time > $over_start_time &&
								$previous_day_flag == 0)) {
						$over_start_date = next_date($tmp_date);
					} else {
						$over_start_date = $tmp_date;
					}

					$over_start_date_time = $over_start_date.$over_start_time;
					//残業開始が予定終了時刻より前の場合に設定
					//if ($over_start_date_time < $tmp_end2_date_time) {
					//	$end2   = $over_start_time;
					//}
				}
				// 実働時間をクリア、出勤時刻＜退勤時刻でない場合に前日の残業時間が表示されないようにする。
				$effective_time = 0;

				// 所定労働・休憩開始終了日時の取得
                // 所定開始時刻が前日の場合 20121120
                if ($atdptn_previous_day_flag == "1") {
                    $tmp_prev_date = last_date($tmp_date);
					$start2_date_time = $tmp_prev_date.$start2;
					//24時間以上勤務の場合 20100203
					if ($over_24hour_flag == "1") {
						$tmp_prev_date = $tmp_date;
					}
					$end2_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_prev_date, $start2, $end2);
				} else {
					$start2_date_time = $tmp_date.$start2;
					//24時間以上勤務の場合 20100203
					if ($over_24hour_flag == "1") {
						$tmp_end_date = next_date($tmp_date);
					} else {
						$tmp_end_date = $tmp_date;
					}
					$end2_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_end_date, $start2, $end2);
				}
				//$start2_date_time = $tmp_date.$start2;
				//$end2_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_date, $start2, $end2);

				$start4_date_time = $tmp_date.$start4;
				$end4_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_date, $start4, $end4);

				//外出復帰日時の取得
				$out_date_time = $tmp_date.$out_time;
				$ret_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_date, $out_time, $ret_time);

				// 時間帯データを配列に格納
				$arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end2_date_time);  // 所定労働
				// 前日出勤フラグ、翌日退勤フラグを考慮した休憩時間配列を取得
				$arr_time4 = $timecard_common_class->get_minute_time_array2($start4_date_time, $end4_date_time, $previous_day_flag, $next_day_flag);  // 休憩

				// 処理当日の所定労働時間を求める
				if ($holiday_name != "" && $hol_minus == "t") {  // 祝日かつ祝日減算設定
					$specified_time = 0;
				} else if ($wage == "4" && $others3 == "2") {  // 時間給者かつ休憩を実働加算
					$specified_time = count($arr_time2);
				} else {
					$specified_time = count(array_diff($arr_time2, $arr_time4));
				}

// 基準時間の計算を変更 2008/10/21
				// 変則労働期間が月でない場合、所定労働時間を基準時間としてカウント
/*
				if ($tmp_date >= $start_date && $reason != "16") {
					if ($arr_irrg_info["irrg_type"] != "2") {
						$arr_data3[0][$i] += $specified_time;
					}
					//当直の場合稼働時間に日数換算の数値ｘ通常勤務日の労働時間の時間を追加
					if ($night_duty_flag){
						$arr_data3[0][$i] += $duty_work_time;
					}
				}
*/
				// 要勤務日数をカウント
				if ($tmp_date >= $start_date) {

					// カレンダーの属性を元にカウントする 2008/10/21
					$arr_you_kinmu_nissu[$i] += $timecard_common_class->get_you_kinmu_nissu($type);
				}
				// 出退勤とも打刻済みの場合
				if ($start_time != "" && $end_time != "") {

					$start_date_time = $timecard_common_class->get_timecard_start_date_time($tmp_date, $start_time, $previous_day_flag);
					$end_date_time   = $timecard_common_class->get_timecard_end_date_time($tmp_date, $end_time, $next_day_flag);

					// 出勤〜退勤までの時間を配列に格納
					$arr_tmp_work_time = $timecard_common_class->get_minute_time_array($start_date_time, $end_date_time);

					// ここまでの処理で所定労働時間帯が未確定の場合、勤務実績で上書き
					if (empty($start2_date_time) || empty($end2_date_time)) {
						$start2_date_time   = $start_date_time;
						$end2_date_time     = $end_date_time;
					}

					// 時間帯データを配列に格納
					switch ($reason) {
					case "2":  // 午前有休
					case "38": // 午前年休
						$arr_time2 = $timecard_common_class->get_minute_time_array($start4_date_time, $end2_date_time);  // 休憩後の所定労働
						break;
					case "3":  // 午後有休
					case "39": // 午後年休
						$arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end4_date_time);  // 休憩前の所定労働
						break;
					default:
						$arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end2_date_time);  // 所定労働
						break;
					}
					//前日・翌日の深夜帯も追加する
					$arr_time5 = $timecard_common_class->get_late_night_shift_array($tmp_date, $start5, $end5);  // 深夜帯

					// 確定出勤時刻／遅刻回数／遅刻時間を取得
					$start_time_info = $timecard_common_class->get_start_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction1, $fraction1_min, $fraction3, $others4, $others5);
                    //時間有休がある場合は遅刻を計算しない 20130527
                    if ($obj_hol_hour->paid_hol_hour_flag == "t") {
                        $hol_hour_min = intval($arr_hol_hour_dat[$tmp_date]["use_hour"]) * 60 + intval($arr_hol_hour_dat[$tmp_date]["use_minute"]);
                        if ($hol_hour_min > 0) {
                            $start_time_info["diff_minutes"] = 0;
                            $start_time_info["diff_count"] = 0;
                        }
                    }

					$fixed_start_time = $start_time_info["fixed_time"];
					//前日出勤の場合、時刻は前日時刻を設定 2008/10/23
					if ($previous_day_flag) {
						$fixed_start_time = $start_date_time;
					}
					if ($tmp_date >= $start_date) {
						$arr_data2[1][$i] += $start_time_info["diff_count"];
						$arr_data2[1][12] += $start_time_info["diff_count"];
						$diff_minutes1 = $start_time_info["diff_minutes"];
					} else {
						$diff_minutes1 = 0;
					}

					// 確定退勤時刻／早退回数／早退時間を取得
					$end_time_info = $timecard_common_class->get_end_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction2, $fraction2_min, $fraction3, $others4);
                    //時間有休がある場合は早退を計算しない 20130527
                    if ($obj_hol_hour->paid_hol_hour_flag == "t" && $hol_hour_min > 0) {
                        $end_time_info["diff_minutes"] = 0;
                        $end_time_info["diff_count"] = 0;
                    }
                    $fixed_end_time = $end_time_info["fixed_time"];
					if ($tmp_date >= $start_date) {
						$arr_data2[4][$i] += $end_time_info["diff_count"];
						$arr_data2[4][12] += $end_time_info["diff_count"];
						$diff_minutes2 = $end_time_info["diff_minutes"];
					} else {
						$diff_minutes2 = 0;
					}

					// 実働時間・所定労働時間を配列に格納（休憩を除く）・有休時間を算出
					switch ($reason) {
					case "2":  // 午前有休
					case "38": // 午前年休
						if (in_array($fixed_start_time, $arr_time2)) {
							$tmp_start_time = $fixed_start_time;
						} else {
							$tmp_start_time = $end4_date_time;
						}
						$tmp_end_time = $fixed_end_time;
						$arr_effective_time = $timecard_common_class->get_minute_time_array($tmp_start_time, $tmp_end_time);

						$arr_specified_time = $arr_time2;

						$paid_time = date_utils::get_time_difference($start2_date_time, $start4_date_time);

						break;
					case "3":  // 午後有休
					case "39": // 午後年休
						$tmp_start_time = $fixed_start_time;
						if (in_array($fixed_end_time, $arr_time2)) {
							$tmp_end_time = $fixed_end_time;
						} else {
							$tmp_end_time = $start4_date_time;
						}
						$arr_effective_time = $timecard_common_class->get_minute_time_array($tmp_start_time, $tmp_end_time);

						$arr_specified_time = $arr_time2;

						$paid_time = date_utils::get_time_difference($end4_date_time, $end2_date_time);

						break;
					default:
						$tmp_start_time = $fixed_start_time;
						$tmp_end_time = $fixed_end_time;
						$arr_effective_time = $timecard_common_class->get_minute_time_array($tmp_start_time, $tmp_end_time);
						if ($wage != "4") {
							$arr_effective_time = array_diff($arr_effective_time, $arr_time4);
						}

						$arr_specified_time = array_diff($arr_time2, $arr_time4);

						$paid_time = 0;

						break;
					}

					//残業時刻追加対応 20100114
					//予定終了時刻から残業開始時刻の間を除外する
					if ($over_start_time != "") {
						$wk_end_date_time = $end2_date_time;

						$arr_jogai = array();
						//基準日時
						$tmp_office_start_time = substr($end2_date_time, 0, 10) . "00";
						if ($fraction1 > "1") {
							//端数処理する分
							$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
							$over_start_date_time = date_utils::move_time($tmp_office_start_time, $over_start_date_time, $moving_minutes, $fraction1);
						}
						if ($fraction2 > "1") {
							//端数処理する分
							$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
							$wk_end_date_time = date_utils::move_time($tmp_office_start_time, $wk_end_date_time, $moving_minutes, $fraction2);
						}
						//残業開始時刻があとの場合
						if (date_utils::to_timestamp_from_ymdhi($over_start_date_time) > date_utils::to_timestamp_from_ymdhi($wk_end_date_time)) {
							//時間配列を取得する（休憩扱い）
							$arr_jogai = $timecard_common_class->get_minute_time_array($wk_end_date_time, $over_start_date_time);
							//休憩扱いの時間配列を除外する
							$arr_effective_time = array_diff($arr_effective_time, $arr_jogai);
						}

					}

					//休日勤務時間（分）と休日出勤を取得 2008/10/23
					if ($tmp_date >= $start_date) {
						$arr_hol_time = $timecard_common_class->get_holiday_work($arr_type, $tmp_date, $arr_effective_time, $start_time, $previous_day_flag, $end_time, $next_day_flag, $reason);

						// 休日出勤した日付を配列へ設定
						for ($wk_idx=4; $wk_idx<7; $wk_idx++) {
							if ($arr_hol_time[$wk_idx] != "") {
								$wk_type = $arr_date[$arr_hol_time[$wk_idx]]["type"];
								$wk_term_index = $arr_date[$arr_hol_time[$wk_idx]]["term_index"];
//echo("<br>t=$wk_type d=".$arr_hol_time[$wk_idx]);
								$arr_hol_work_date[$arr_hol_time[$wk_idx]]["type"] = $wk_type;
								$arr_hol_work_date[$arr_hol_time[$wk_idx]]["term_index"] = $wk_term_index;
/*
								switch ($wk_type) {
								case "1":
									$arr_hol_work_date1[$arr_hol_time[$wk_idx]] = $wk_term_index;
									break;
								case "4":
								case "6":
									$arr_hol_work_date2[$arr_hol_time[$wk_idx]] = $wk_term_index;
									break;
								case "5":
								case "7":
									$arr_hol_work_date3[$arr_hol_time[$wk_idx]] = $wk_term_index;
									break;
								}
*/
							}
						}
					}

					//当直の場合当直時間を計算から除外する
					if ($night_duty_flag){
						//稼働 - (当直 - 所定労働)
						$arr_effective_time = array_diff($arr_effective_time, array_diff($night_duty_time_array, $arr_time2));
					}

					// 普外時間を算出（分単位）
					// 外出時間を差し引かないフラグ対応 20110727
					if ($out_time_nosubtract_flag != "1") {
						$arr_out_time = $timecard_common_class->get_minute_time_array($out_date_time, $ret_date_time);
						$time3 = count(array_intersect($arr_out_time, $arr_specified_time));

						// 残外時間を算出（分単位）
						$time4 = count(array_diff($arr_out_time, $arr_specified_time));
					}
					// 実働時間を算出（分単位）
					$effective_time = count($arr_effective_time);
					if ($wage == "4" && $others3 == "1") {  // 時間給かつ休憩除外
						if ($effective_time > 540) {  // 9時間を超える場合
							$effective_time -= $rest3;
						} else if ($effective_time > 480) {  // 8時間を超える場合
							$effective_time -= $rest2;
						} else if ($effective_time > 360) {  // 6時間を超える場合
							$effective_time -= $rest1;
						}
					}

					// 代替出勤または振替出勤の場合、実働時間から所定労働時間を減算
					if ($reason == "14" || $reason == "15") {
						$effective_time -= count($arr_specified_time);
					}

					// 稼働時間を算出（分単位）
					$work_time = $effective_time + $paid_time - $time3 - $time4;
					$work_time_for_week += $work_time;
					if ($tmp_date < $start_date) {
						$tmp_date = next_date($tmp_date);
						$counter_for_week++;
						continue;
					}
					$arr_data3[1][$i] += $work_time;

					//当直の場合稼働時間に日数換算の数値ｘ通常勤務日の労働時間の時間を追加（当直の時間を除外して計算している為)
					if ($night_duty_flag){
						$arr_data3[1][$i] += $duty_work_time;
					}

					// 残業時間を算出（分単位）
					if ($arr_irrg_info["irrg_type"] == "") {  // 変則労働適用外の場合
						$tmp_specified_time = $specified_time;
						// 遅刻時間を残業時間から減算しない場合（所定時間から引くことで合わせる）
						if ($delay_overtime_flg == "2") {
							$tmp_specified_time = $specified_time - $start_time_info["diff_minutes"];
						}
	//					$time1 = ($effective_time > 480) ? $effective_time - 480 : 0;
						//法定外残業の基準時間
						if ($base_time == "") {
							$wk_base_time = 8 * 60;
						} else {
							$base_hour = intval(substr($base_time, 0, 2));
							$base_min = intval(substr($base_time, 2, 2));
							$wk_base_time = $base_hour * 60 + $base_min;
						}
						//基準が少ない場合は、残業計算に使用する
						if ($wk_base_time < $tmp_specified_time) {
							$tmp_specified_time = $wk_base_time;
						}
						if ($effective_time > $tmp_specified_time) {
							$time1 = $effective_time - $tmp_specified_time;
						} else {
							$time1 = 0;
						}
						switch ($others1) {
						case "2":  // 遅刻時間に加算する
							$diff_minutes1 += $time3;
							break;
						case "3":  // 早退時間に加算する
							$diff_minutes2 += $time3;
							break;
						case "4":  // 残業時間から減算する
							$time1 -= $time3;
							break;
						}
						switch ($others2) {
						case "2":  // 遅刻時間に加算する
							$diff_minutes1 += $time4;
							break;
						case "3":  // 早退時間に加算する
							$diff_minutes2 += $time4;
							break;
						case "4":  // 残業時間から減算する
							$time1 -= $time4;
							break;
						}

					} else if ($arr_irrg_info["irrg_type"] == "1") {  // 変則労働期間・週の場合
						if ($counter_for_week == 7) {
							$tmp_irrg_minutes = $arr_irrg_info["irrg_minutes"];
							if ($hol_minus == "t") {
								$tmp_irrg_minutes -= (8 * 60 * $holiday_count);
							}
							$time1 = ($work_time_for_week > $tmp_irrg_minutes) ? $work_time_for_week - $tmp_irrg_minutes : 0;
						} else {
							$time1 = 0;
						}
					}

					// 深夜勤務時間を算出（分単位）
					$time2 = count(array_intersect($arr_effective_time, $arr_time5));

					// 退勤後復帰回数・時間を算出
					$return_count = 0;
					$return_time = 0;
					for ($j = 1; $j <= 10; $j++) {
						$start_var = "o_start_time$j";
						if ($$start_var == "") {
							break;
						}
						$return_count++;

						$end_var = "o_end_time$j";
						if ($$end_var == "") {
							break;
						}

						$arr_ret = $timecard_common_class->get_return_array($end_date_time, $$start_var, $$end_var);
						$return_time += count($arr_ret);

						// 月の深夜勤務時間に退勤後復帰の深夜分を追加
						$arr_data3[3][$i] += count(array_intersect($arr_ret, $arr_time5));
					}

					// 月の稼働時間・残業時間に退勤後復帰分の残業時間を追加
					$arr_data3[1][$i] += $return_time;
					$arr_data3[2][$i] += $return_time;

					// 配列に反映
					$arr_data2[2][$i] += $diff_minutes1;
					$arr_data2[5][$i] += $diff_minutes2;
					$arr_data3[2][$i] += $time1;
					$arr_data3[3][$i] += $time2;

					//残業時刻2追加 20110628
					if ($over_start_time2 != "" && $over_end_time2 != "") {
						//開始日時
						$over_start_date_time2 = ($over_start_next_day_flag2 == 1) ? next_date($tmp_date).$over_start_time2 : $tmp_date.$over_start_time2;
						//終了日時
						$over_end_date_time2 = ($over_end_next_day_flag2 == 1) ? next_date($tmp_date).$over_end_time2 : $tmp_date.$over_end_time2;

						$wk_zangyo2 = date_utils::get_time_difference($over_end_date_time2,$over_start_date_time2);
						$arr_data3[1][$i] += $wk_zangyo2;
						$arr_data3[2][$i] += $wk_zangyo2;

						//深夜時間帯の時間取得
						$work_times_info = $timecard_common_class->get_work_times_info($tmp_date, $start4, $end4, $start5, $end5);
						$over_time2_late_night = $timecard_common_class->get_late_night($work_times_info, $over_start_date_time2, $over_end_date_time2, "");
						$arr_data3[3][$i] += $over_time2_late_night;
					}

				} else {

					// 事由が「代替休暇」「振替休暇」の場合、
					// 所定労働時間を稼働時間にプラス
					// 「有給休暇」を除く $reason == "1" || 2008/10/14
					if ($reason == "4" || $reason == "17") {
						if ($tmp_date >= $start_date) {
							$arr_data3[1][$i] += $specified_time;
							$work_time_for_week += $specified_time;
						}
					}

					if ($arr_irrg_info["irrg_type"] == "1" && $counter_for_week == 7) {
						$tmp_irrg_minutes = $arr_irrg_info["irrg_minutes"];
						if ($hol_minus == "t") {
							$tmp_irrg_minutes -= (8 * 60 * $holiday_count);
						}
						$arr_data3[2][$i] += ($work_time_for_week > $tmp_irrg_minutes) ? $work_time_for_week - $tmp_irrg_minutes : 0;
					}

					if ($tmp_date < $start_date) {
						$tmp_date = next_date($tmp_date);
						$counter_for_week++;
						continue;
					}
				}

				$tmp_date = next_date($tmp_date);
				$counter_for_week++;
			}

			// 休日出勤 2008/10/23
//print_r($arr_hol_work_date1);
/*
			foreach ($arr_hol_work_date1 as $wk_date => $wk_term_index) {
				$arr_data4[2][$wk_term_index]++;
				$arr_data4[2][12]++;
			}
			foreach ($arr_hol_work_date2 as $wk_date => $wk_term_index) {
				$arr_data4[1][$wk_term_index]++;
				$arr_data4[1][12]++;
			}
			foreach ($arr_hol_work_date3 as $wk_date => $wk_term_index) {
				$arr_data4[0][$wk_term_index]++;
				$arr_data4[0][12]++;
			}
*/
			foreach ($arr_hol_work_date as $wk_date) {
				$wk_type = $wk_date["type"];
				$wk_term_index = $wk_date["term_index"];
				switch ($wk_type) {
				case "1":  // 通常勤務日
					$arr_data4[2][$wk_term_index]++;
					$arr_data4[2][12]++;
					break;
				case "4":  // 法定休日
					$arr_data4[1][$wk_term_index]++;
					$arr_data4[1][12]++;
					break;
				case "5":  // 所定休日
					$arr_data4[0][$wk_term_index]++;
					$arr_data4[0][12]++;
					break;
				case "6":  // 祝日
					$arr_data4[3][$wk_term_index]++;
					$arr_data4[3][12]++;
					break;
				case "7":  // 年末年始休暇
					$arr_data4[4][$wk_term_index]++;
					$arr_data4[4][12]++;
					break;
				}
			}

			// 変則労働期間が月の場合
			if ($arr_irrg_info["irrg_type"] == "2") {

				// 所定労働時間を基準時間とする
				$tmp_irrg_minutes = $arr_irrg_info["irrg_minutes"];
				if ($hol_minus == "t") {
					$tmp_irrg_minutes -= (8 * 60 * $holiday_count);
				}
				$arr_data3[0][$i] = ($tmp_irrg_minutes > 0) ? $tmp_irrg_minutes : 0;

				// 稼働時間−基準時間を残業時間とする
				$arr_data3[2][$i] = $arr_data3[1][$i] - $arr_data3[0][$i];
			}
			else {
			// 基準時間の計算を変更 2008/10/21
			// 要勤務日数 × 所定労働時間
				$arr_data3[0][$i] = $arr_you_kinmu_nissu[$i] * date_utils::hi_to_minute($day1_time);
			}
		}

		// 月単位に時間を丸める
		for ($i = 0; $i < 12; $i++) {

			// 遅刻
			if ($arr_data2[2][$i] < 0) {
				$arr_data2[2][$i] = 0;
			}
			$arr_data2[2][$i] = round_time($arr_data2[2][$i], $fraction5, $fraction5_min);
			$arr_data2[2][12] += $arr_data2[2][$i];

			// 早退
			if ($arr_data2[5][$i] < 0) {
				$arr_data2[5][$i] = 0;
			}
			$arr_data2[5][$i] = round_time($arr_data2[5][$i], $fraction5, $fraction5_min);
			$arr_data2[5][12] += $arr_data2[5][$i];

			// 基準時間
			if ($arr_data3[0][$i] < 0) {
				$arr_data3[0][$i] = 0;
			}
			$arr_data3[0][$i] = round_time($arr_data3[0][$i], $fraction4, $fraction4_min);
			$arr_data3[0][12] += $arr_data3[0][$i];

			// 稼働時間
			if ($arr_data3[1][$i] < 0) {
				$arr_data3[1][$i] = 0;
			}
			$arr_data3[1][$i] = round_time($arr_data3[1][$i], $fraction4, $fraction4_min);
			$arr_data3[1][12] += $arr_data3[1][$i];

			// 残業
			if ($arr_data3[2][$i] < 0) {
				$arr_data3[2][$i] = 0;
			}
			// 残業管理をしない場合
			if ($no_overtime == "t") {
				$arr_data3[2][$i] = 0;
			} else {
				$arr_data3[2][$i] = round_time($arr_data3[2][$i], $fraction4, $fraction4_min);
			}
			$arr_data3[2][12] += $arr_data3[2][$i];

			// 深夜勤務
			if ($arr_data3[3][$i] < 0) {
				$arr_data3[3][$i] = 0;
			}
			$arr_data3[3][$i] = round_time($arr_data3[3][$i], $fraction4, $fraction4_min);
			$arr_data3[3][12] += $arr_data3[3][$i];
		}

		for ($i = 0; $i < count($arr_term); $i++) {
			if ($arr_term[$i]["closed"]) {
				$arr_data2[1][$i] += $arr_term[$i]["count1"];
				$arr_data2[2][$i] += hmm_to_minute($arr_term[$i]["time1"]);
				$arr_data2[4][$i] += $arr_term[$i]["count2"];
				$arr_data2[5][$i] += hmm_to_minute($arr_term[$i]["time2"]);
				$arr_data2[1][12] += $arr_data2[1][$i];
				$arr_data2[2][12] += $arr_data2[2][$i];
				$arr_data2[4][12] += $arr_data2[4][$i];
				$arr_data2[5][12] += $arr_data2[5][$i];

				$arr_data3[0][$i] += hmm_to_minute($arr_term[$i]["time3"]);
				$arr_data3[1][$i] += hmm_to_minute($arr_term[$i]["time4"]);
				$arr_data3[2][$i] += hmm_to_minute($arr_term[$i]["time5"]);
				$arr_data3[3][$i] += hmm_to_minute($arr_term[$i]["time6"]);
				for ($j = 0; $j <= 3; $j++) {
					$arr_data3[$j][12] += $arr_data3[$j][$i];
				}
			}
		}
	}
}
?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a></font></td>
<? if ($work_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>&work=1"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>

<?
// メニュー表示
show_atdbk_menuitem($session, $fname, "");
?>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? if ($closing == "") {  // 締め日未登録の場合 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイムカード締め日が登録されていません。管理者に連絡してください。</font></td>
</tr>
</table>
<? } else { ?>
<form action="atdbk_vacation_update_exe.php" method="post">
<table width="500" border="0" cellspacing="0" cellpadding="1">
<tr>
<td height="22" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($flg != 1) { ?>
<a href="atdbk_vacation.php?session=<? echo($session); ?>&year=<? echo($year - 1); ?>&flg=<?=$flg?>">&lt;前年</a>&nbsp;<? echo($year); ?>年度&nbsp;<a href="atdbk_vacation.php?session=<? echo($session); ?>&year=<? echo($year + 1); ?>&flg=<?=$flg?>">翌年&gt;</a>
<? } ?>
&nbsp;&nbsp;
<?
	//$wk_title = "繰越日数参照";
	$wk_title = "有給（年次）休暇";
	if ($flg == 1) {
	$wk_url = "<b>{$wk_title}</b>";
} else {
	$wk_url = "<a href=\"atdbk_vacation.php?session={$session}&year={$year}&flg=1\">{$wk_title}</a>";
}
echo($wk_url);
echo("&nbsp;&nbsp;");

$wk_title = "休暇取得状況";
if ($flg == 2) {
	$wk_url = "<b>{$wk_title}</b>";
} else {
	$wk_url = "<a href=\"atdbk_vacation.php?session={$session}&year={$year}&flg=2\">{$wk_title}</a>";
}
echo($wk_url);
echo("&nbsp;&nbsp;");
    // 年次有給休暇簿 20131126 start
//    $opt_display_flag = file_exists("opt/flag");
//
//    if ( phpversion() >= "5.1" && $opt_display_flag ){
//        // ----- メディアテック Start -----
//        //年次休暇簿
//        $wk_title = "年次休暇簿";
//        $wk_url = "<a href=\"kintai_vacation_paid_holiday.php?session={$session}&umode=usr\">{$wk_title}</a>";
//        echo($wk_url);
//        echo("&nbsp;&nbsp;");
//        //休暇簿　2012.04
//        $wk_title = "休暇簿";
//        $wk_url = "<a href=\"kintai_vacation.php?session={$session}&umode=usr&year={$year}&flg=2\">{$wk_title}</a>";
//        echo($wk_url);
//        echo("&nbsp;&nbsp;");
//        // ----- メディアテック End -----
//    }
    if ( phpversion() >= "5.1") {

        // 年次有給休暇簿 20131112 YOKOBORI
        if ($year_paid_hol_menu_flg == "t") {
            //$wk_title = "年次有給休暇簿";
            $wk_title = get_settings_value("kintai_vacation_paid_hol_title", "年次休暇簿"); // 年次有給休暇簿 20131122 SEKIAI
            if ($flg == 3) {
                $wk_url = "<b>{$wk_title}</b>";
            } else {
                $wk_url = "<a href=\"kintai_vacation_paid_holiday.php?session={$session}&umode=usr\">{$wk_title}</a>";
            }
            echo($wk_url);
            echo("&nbsp;&nbsp;");
        }

        //休暇簿
        $wk_disp_flg = get_settings_value("kintai_vacation_disp_flg", "f"); // opt/settings.phpで変更
        if ($wk_disp_flg == "t") {
            $wk_title = "休暇簿";
            $wk_url = "<a href=\"kintai_vacation.php?session={$session}&umode=usr&year={$year}&flg=2\">{$wk_title}</a>";
            echo($wk_url);
            echo("&nbsp;&nbsp;");
        }
    }
    // 年次有給休暇簿 20131126 end
?>
</font></td>
</tr>
</table>
<?
if ($flg == "1") {
/*
?>

<table width="500" border="0" cellspacing="0" cellpadding="1">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>有給休暇（年休）</b></font></td>
</tr>
</table>
<?
*/
//改ページ用矢印表示
?>
<table width="640" border="0" cellspacing="0" cellpadding="0">
<tr>
<td height="22" align="left" width="10%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;</font></td>
<td height="22" align="left" width="45%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($start_year > $default_start_year) {
			echo("<a href=\"atdbk_vacation.php?session=$session&flg=1&start_year=".($start_year-10)."&year=".($start_year-10)."\">←</a>");
} else {
	echo("&nbsp;");
}
?>
</font></td>
<td height="22" align="right" width="45%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<a href="atdbk_vacation.php?session=<? echo($session); ?>&flg=1&start_year=<? echo($end_year+1); ?>&year=<? echo($end_year+1); ?>">→</a></font></td>
</td>
</tr>
</table>
<table width="640" border="0" cellspacing="0" cellpadding="0" class="list">
<tr>
<td height="22" align="center" width="10%" bgcolor="#f6f9ff">&nbsp;</td>
<?

		//年をリンク表示する
		for ($i=0; $i<10; $i++) {
			echo("<td height=\"22\" align=\"center\" width=\"9%\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			$wk_target_year = $start_year+$i;
			if ($wk_target_year == $year) {
				echo($wk_target_year);
			} else {
				echo("<a href=\"atdbk_vacation.php?session=$session&start_year=$start_year&year=$wk_target_year\">$wk_target_year</a>");
			}
			echo("</font></td>");
		}
		//付与日追加 20091207
		echo("<tr>");
		echo("<td height=\"22\" align=\"right\" width=\"10%\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">付与日</font></td>");
		list($wk_add_mm, $wk_add_day) = split("/", $wk_add_mmdd);
		for ($j=0; $j<10; $j++) {

			echo("<td height=\"22\" align=\"center\" width=\"9%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			//採用日以降の付与日を表示
			$wk_add_date = ($start_year+$j).sprintf("%02d%02d", $wk_add_mm,$wk_add_day);
			if ($wk_add_date > $emp_join) {
				// 登録データの付与日がある場合 20110810
				if ($arr_paid_hol_add_mmdd[$j] != "") {
					$wk_mmdd = intval(substr($arr_paid_hol_add_mmdd[$j], 0, 2))."/".intval(substr($arr_paid_hol_add_mmdd[$j], 2, 2));
					echo($wk_mmdd);
				}
				/*
				//3ヶ月基準の場合
				if ($closing_paid_holiday == "3" && $criteria_months == "1") {
					// 登録データの付与日がある場合
					if ($arr_paid_hol_add_mmdd[$j] != "") {
						$wk_mmdd = intval(substr($arr_paid_hol_add_mmdd[$j], 0, 2))."/".intval(substr($arr_paid_hol_add_mmdd[$j], 2, 2));
						echo($wk_mmdd);
					} else
					//表示列の年が入職後1年未満、3ヶ月後の付与日
					if (($start_year+$j) < substr($year_after_join, 0, 4)) {
						echo($wk_three_month_after_join);
					} else {
						echo($wk_year_after_join);
						//echo($wk_add_mmdd);
					}
				} else {
					echo($wk_add_mmdd);
				}
				*/
			}
			echo("</font></td>");
		}
		echo("</tr>");
		$cnt_title = array("繰越", "付与", "有休日数", "調整日数");

        //所定労働時間
        //$specified_time = $obj_hol_hour->get_specified_time($emp_id, $day1_time);
        for ($i=0; $i<4; $i++) {

	echo("<tr>");
	echo("<td height=\"22\" align=\"right\" width=\"10%\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$cnt_title[$i]}</font></td>");
	for ($j=0; $j<10; $j++) {

		echo("<td height=\"22\" align=\"right\" width=\"9%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
                if ($arr_paid_hol[$i][$j] != 0 || $arr_paid_hol[4][$j] != 0) {
                    $wk_target_year = $start_year+$j;
                    echo($arr_paid_hol[$i][$j]);
            //時間有休繰越
                    if ($obj_hol_hour->paid_hol_hour_flag == "t") {
                        if (($i == 0 || $i == 2) && $arr_paid_hol[4][$j] != 0) {
                            echo("<br>&nbsp; ");
                            echo(minute_to_hmm($arr_paid_hol[4][$j]));
                        }
                    }
                } else {
			echo("&nbsp;");
		}
		echo("</font></td>");
	}
	echo("</tr>");
}

?>
</table>
<br>
<?
$idx = $year - $start_year;
//付与データがある場合表示、ない場合は表示しない
if ($arr_paid_hol_add_mmdd[$idx] != "") {
?>
<table width="700" border="0" cellspacing="0" cellpadding="0" class="list">
<tr>
<?
		//付与日
		if ($wk_paid_hol_add_mmdd != "") {
			$wk_add_date = $year.$wk_paid_hol_add_mmdd;
		} else {
			//入職後1年以上か確認
			if (substr($year_after_join, 0, 4) <= $year) {
				$wk_add_date = $year_after_join;
			} else {
				$wk_add_date = $paid_hol_add_date;
			}
		}
		//移行データの前回付与日の範囲も確認する 20120710
		if ($last_add_date != "") {
        	list($wk_import_add_year, $wk_month, $wk_day) = split("/", $last_add_date);
        	$last_add_date = sprintf("%04d%02d%02d", $wk_import_add_year, $wk_month, $wk_day); //yyyymmdd形式
        }
        //年休と時間有休の配列を取得
            list($arr_cnt, $arr_hour) = get_paid_hol_cnt2($con, $fname, $emp_id, $year, $closing, $closing_paid_holiday, $closing_month_flg, $wk_add_date, $data_migration_date, $obj_hol_hour, $day1_time, $last_add_date, $hol_hour_data_migration_date);
		//年度開始月
		if ($closing_paid_holiday == "1") {
			$start_mm = 4;
		}
		elseif ($closing_paid_holiday == "2") {
			$start_mm = 1;
		} else {
			if ($wk_paid_hol_add_mmdd != "") {
				$start_mm = intval(substr($wk_paid_hol_add_mmdd, 0, 2));
			} else {
				//入職後1年以上か確認
				if ($criteria_months == "1" && substr($year_after_join, 0, 4) <= $year) {
					$start_mm = intval(substr($year_after_join, 4, 2));
				} else {
					$start_mm = intval(substr($paid_hol_add_date, 4, 2));
				}
			}
		}
		//20101125 有給休暇残日数移行データがある場合、移行日の月に有給取得日数を表示し、移行日の月より前の月は0とする（空白）。
            //処理年月範囲確認
            $chk_ymd1 = $year.sprintf("%02d",$start_mm)."01";
            $chk_ymd1_timestamp = date_utils::to_timestamp_from_ymd($chk_ymd1);
            $chk_ymd2 =  date("Ymd", strtotime("+12 month", $chk_ymd1_timestamp));
            if ($data_migration_date != "") {
                //処理年月範囲内の場合、有休取得日数を表示用配列に加算する
			//移行データの前回付与日の範囲も確認する 20120710
			if (($chk_ymd1 <= $data_migration_date && $data_migration_date < $chk_ymd2) &&
				($chk_ymd1 <= $last_add_date && $last_add_date < $chk_ymd2)) {
				$migration_month = substr($data_migration_date, 4, 2);
				$arr_cnt[$migration_month] += $curr_use;
			}
		}
		//時間有休移行データの使用数設定
            if ($hol_hour_data_migration_date != "") {
                //処理年月範囲内の場合、有休取得日数を表示用配列に加算する
                if (($chk_ymd1 <= $hol_hour_data_migration_date &&
                    $hol_hour_data_migration_date < $chk_ymd2)) {
                    $hol_hour_migration_month = substr($hol_hour_data_migration_date, 4, 2);
                    $arr_hour[$hol_hour_migration_month] += $obj_hol_hour->hh_or_hhmm_to_minute($hol_hour_curr_use); //20141016 HH:MM形式対応
                }
            }

		echo("<td height=\"22\" align=\"center\" width=\"16%\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">月</font></td>");
		for ($j=0; $j<12; $j++) {

			$wk_mm = $j + $start_mm;
			if ($wk_mm > 12) {
				$wk_mm -= 12;
			}
			echo("<td height=\"22\" align=\"center\" width=\"7%\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			//echo($j+1);
			echo($wk_mm);
			echo("月</font></td>");
		}
		echo("</tr>");
		echo("<tr>");
		echo("<td height=\"22\" align=\"center\" width=\"16%\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">有休取得日数</font></td>");
		for ($j=0; $j<12; $j++) {

			echo("<td height=\"22\" align=\"center\" width=\"7%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
	//		echo($j+1);
			$wk_j = $j + $start_mm;
			if ($wk_j > 12) {
				$wk_j -= 12;
			}
			$wk_mm = sprintf("%02d", $wk_j);
			echo($arr_cnt[$wk_mm]);
			echo("</font></td>");
		}

		echo("</tr>");
            //時間有休
            if ($obj_hol_hour->paid_hol_hour_flag == "t") {
                echo("<tr>");
                echo("<td height=\"22\" align=\"center\" width=\"16%\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">有休取得時間数</font></td>");
                for ($j=0; $j<12; $j++) {
                    $wk_j = $j + $start_mm;
                    if ($wk_j > 12) {
                        $wk_j -= 12;
                    }
                    echo("<td height=\"22\" align=\"center\" width=\"7%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
                    $wk_mm = sprintf("%02d", $wk_j);
                    echo(minute_to_hmm($arr_hour[$wk_mm]));
                    echo("</font></td>");
                }
                echo("</tr>");
            }

            echo("<tr>");
		echo("<td height=\"22\" align=\"center\" width=\"16%\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">月締時残日数</font></td>");

//20101125 有給休暇残日数移行データがある場合、移行日の月に有給取得日数を表示し、移行日の月より前の月は0とする（空白）。

		//zansu - siyosu 月別残数　開始月
		$idx = $year - $start_year;
		$wk_cur_paid_hol = $arr_paid_hol[2][$idx];
		$arr_mon_zan = array();
        //時間有休
            $wk_cur_paid_hol_hour = $arr_paid_hol[4][$idx];

            $arr_mon_zan_hour = array();

            $arr_timehist = $calendar_name_class->get_calendarname_time($wk_add_date);
            $day1_time = $arr_timehist["day1_time"];
            //所定労働時間
            $specified_time = $obj_hol_hour->get_specified_time($emp_id, $day1_time);

            //残日数の計算
            for ($j=0; $j<12; $j++) {
                //有休付与日1月1日
                if ($closing_paid_holiday == "2") {
                    $wk_mm = sprintf("%02d", $j+1); //月
                    $wk_mm_idx = $j;  //配列上の位置
                } else {
                    //年度開始月から順に引く
                    //4月1日
                    if ($closing_paid_holiday == "1") {
                        $start_mm = 4;
                    }
                    //採用日
                    else {
                        //有休付与データがある場合
                        if ($wk_paid_hol_add_mmdd != "") {
                            $start_mm = intval(substr($wk_paid_hol_add_mmdd, 0, 2));
                        } else {
                            //入職後1年以上か確認
                            if ($criteria_months == "1" && substr($year_after_join, 0, 4) <= $year) {
                                $start_mm = intval(substr($year_after_join, 4, 2)); //採用日の月
                            } else {
                                $start_mm = intval(substr($paid_hol_add_date, 4, 2)); //採用日の3ヶ月か6ヵ月後
                            }
                        }
                    }
                    $wk_mm_idx = $j + $start_mm - 1;
                    if ($j + $start_mm - 1 >= 12) {
                        $wk_mm_idx -= 12;
                    }
                    $wk_mm = sprintf("%02d", $wk_mm_idx+1);
                }
                //配列の先頭の場合
                if ($j==0) {
                    $arr_mon_zan[$wk_mm_idx] = $wk_cur_paid_hol - $arr_cnt[$wk_mm];
                    //時間有休分を減算する
                    if ($arr_hour[$wk_mm] > 0) {
                        if ($wk_cur_paid_hol_hour >= $arr_hour[$wk_mm]) {
                            $arr_mon_zan_hour[$wk_mm_idx] = $wk_cur_paid_hol_hour - $arr_hour[$wk_mm];

                        }
                        else {
                            $wk_day = ceil($arr_hour[$wk_mm] / $specified_time);
                            $arr_mon_zan[$j] -= $wk_day;
                            $arr_mon_zan_hour[$wk_mm_idx] = ($specified_time*$wk_day)+$wk_cur_paid_hol_hour - $arr_hour[$wk_mm];
                        }
                    }
                    else {
                        $arr_mon_zan_hour[$wk_mm_idx] = $wk_cur_paid_hol_hour;
                    }
                }
                //配列の2番目以降
                else {
                    $wk_prev_idx = ($wk_mm_idx == 0) ? 12 : $wk_mm_idx;
                    $arr_mon_zan[$wk_mm_idx] = $arr_mon_zan[$wk_prev_idx-1] - $arr_cnt[$wk_mm];

                    //時間有休分を減算する
                    if ($arr_hour[$wk_mm] > 0) {
                        if ($arr_mon_zan_hour[$wk_prev_idx-1] >= $arr_hour[$wk_mm]) {
                            $arr_mon_zan_hour[$wk_mm_idx] = $arr_mon_zan_hour[$wk_prev_idx-1] - $arr_hour[$wk_mm];

                        }
                        else {
                            //日数換算
                            $wk_day = ceil( $arr_hour[$wk_mm] / $specified_time);
                            $arr_mon_zan[$wk_mm_idx] -= $wk_day;
                            $arr_mon_zan_hour[$wk_mm_idx] = ($specified_time*$wk_day)+$arr_mon_zan_hour[$wk_prev_idx-1] - $arr_hour[$wk_mm];
                        }
                    }
                    else {
                        $arr_mon_zan_hour[$wk_mm_idx] = $arr_mon_zan_hour[$wk_prev_idx-1];
                    }
                }
            }
            for ($j=0; $j<12; $j++) {

                $wk_j = $j + $start_mm;
                if ($wk_j > 12) {
                    $wk_j -= 12;
                }
                echo("<td height=\"22\" align=\"center\" width=\"7%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
                echo($arr_mon_zan[$wk_j-1]);
                /*
                if ($obj_hol_hour->paid_hol_hour_flag == "t") {
                    echo("<br>");
                    if ($arr_mon_zan_hour[$wk_j-1] > 0) {
                        echo(minute_to_hmm($arr_mon_zan_hour[$wk_j-1]));
                    }
                    else {
                        echo("&nbsp;");
                    }
                }
                echo("</font></td>");
                */
            }
            echo("</tr>");
            if ($obj_hol_hour->paid_hol_hour_flag == "t") {

                echo("<tr>");
                echo("<td height=\"22\" align=\"center\" width=\"16%\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">月締時時間数</font></td>");
                for ($j=0; $j<12; $j++) {

                    $wk_j = $j + $start_mm;
                    if ($wk_j > 12) {
                        $wk_j -= 12;
                    }
                    echo("<td height=\"22\" align=\"center\" width=\"7%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
                    if ($arr_mon_zan_hour[$wk_j-1] > 0) {
                        echo(minute_to_hmm($arr_mon_zan_hour[$wk_j-1]));
                    }
                    else {
                        echo("&nbsp;");
                    }
                    echo("</font></td>");
                }
                echo("</tr>");
            }
//</tr>
?>
</table>
<? } ?>
<? } ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="year" value="<? echo($year); ?>">
</form>
<?
if ($flg == "2") {
?>
<table width="600" border="0" cellspacing="0" cellpadding="1">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>休暇欠勤</b></font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="1" cellpadding="1" class="list">
<?
	write_data_header(true, $closing_paid_holiday);
	for ($i = 0; $i <= 19; $i++) {
		//追加事由については表示フラグ確認
		if ($i >= 9 && $arr_disp_flg[$i - 9] == 'f') {
			continue;
		}
			//年休・有休を非表示とする 20100705
			if ($i == 2) {
				continue;
			}

		echo("<tr>\n");
		echo("<td align=\"{$header1[$i]["align"]}\" nowrap bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$header1[$i]["name"]}</font></td>\n");
		for ($j = 0; $j <= 12; $j++) {
			echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			show_num($arr_data1[$i][$j]);
			echo("</font></td>\n");
		}
		echo("</tr>\n");
	}
?>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="600" border="0" cellspacing="0" cellpadding="1">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>遅刻・早退</b></font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="1" cellpadding="1" class="list">
<?
	write_data_header(false, $closing_paid_holiday);
	for ($i = 0; $i <= 5; $i++) {
		echo("<tr>\n");
		echo("<td align=\"{$header2[$i]["align"]}\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$header2[$i]["name"]}</font></td>\n");
		for ($j = 0; $j <= 12; $j++) {
			echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			switch ($i) {
			case 0:
			case 3:
				echo("&nbsp;");
				break;
			case 1:
			case 4:
				show_num($arr_data2[$i][$j]);
				break;
			case 2:
			case 5:
				show_hmm($arr_data2[$i][$j]);
				break;
			}
			echo("</font></td>\n");
		}
		echo("</tr>\n");
	}
?>
</table>
<? } ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<?
if ($flg == "3") {
?>
<table width="600" border="0" cellspacing="0" cellpadding="1">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>勤務時間</b></font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="1" cellpadding="1" class="list">
<?
	write_data_header(false, $closing_paid_holiday);
	for ($i = 0; $i <= 3; $i++) {
		echo("<tr>\n");
		echo("<td align=\"{$header3[$i]["align"]}\" nowrap bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$header3[$i]["name"]}</font></td>\n");
		for ($j = 0; $j <= 12; $j++) {
			echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			show_hmm($arr_data3[$i][$j]);
			echo("</font></td>\n");
		}
		echo("</tr>\n");
	}
?>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="600" border="0" cellspacing="0" cellpadding="1">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>休日出勤日数</b></font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="1" cellpadding="1" class="list">
<?
	write_data_header(false, $closing_paid_holiday);
	for ($i = 0; $i <= 4; $i++) {
		echo("<tr>\n");
		echo("<td align=\"{$header4[$i]["align"]}\" nowrap bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$header4[$i]["name"]}</font></td>\n");
		for ($j = 0; $j <= 12; $j++) {
			echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			show_num($arr_data4[$i][$j]);
			echo("</font></td>\n");
		}
		echo("</tr>\n");
	}
?>
</table>
<? } ?>
<? } ?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
// 期間配列を作成
// @param $year 年
// @param $closing 月締め日（常勤）
// @param $closing_paid_holiday 年休締め日
// 1:YYYY年4月度〜YYYY＋1年3月度
// 2:YYYY年1月度〜YYYY年12月度
// @param mixed $closing_month_flg 月度 1:開始日の月とする 2:終了日の月とする
// @return 期間配列 月毎にstart、endをキーとした配列を返す
function create_term_array($year, $closing, $closing_paid_holiday, $closing_month_flg) {
	$ret = array();

	if ($closing_paid_holiday == 1) {
		// YYYY年4月度〜YYYY年12月度の期間配列を作成
		for ($i = 4; $i <= 12; $i++) {
			$tmp_yyyymm = sprintf("%04d%02d", $year, $i);
            array_push($ret, get_term($tmp_yyyymm, $closing, $closing_month_flg));
		}

		// YYYY＋1年1月度〜YYYY＋1年3月度の期間配列を作成
		$next_year = $year + 1;
		for ($i = 1; $i <= 3; $i++) {
			$tmp_yyyymm = sprintf("%04d%02d", $next_year, $i);
            array_push($ret, get_term($tmp_yyyymm, $closing, $closing_month_flg));
		}
	} else {
		for ($i = 1; $i <= 12; $i++) {
			$tmp_yyyymm = sprintf("%04d%02d", $year, $i);
            array_push($ret, get_term($tmp_yyyymm, $closing, $closing_month_flg));
		}
	}

	return $ret;
}

// 指定年月の期間を返す
// @param $yyyymm 年月
// @param $closing 月締め日（常勤）
// @param mixed $closing_month_flg 月度 1:開始日の月とする 2:終了日の月とする
function get_term($yyyymm, $closing, $closing_month_flg) {
	$yyyy = substr($yyyymm, 0, 4);
	$mm = substr($yyyymm, 4, 2);

    //末日以外
    if ($closing != "6") {
        //月度終了日
        if ($closing_month_flg == "2") {
            $next_yyyy = $yyyy;
            $next_mm = $mm;
            $mm--;
            if ($mm < 1) {
                $yyyy--;
                $mm = 12;
            }

        }
        //月度開始日
        else {
            $next_yyyy = $yyyy;
            $next_mm = $mm + 1;
            if ($next_mm > 12) {
                $next_yyyy++;
                $next_mm = 1;
            }
        }
    }

	switch ($closing) {
	case "1":  // 1日
		$start_date = sprintf("%04d%02d%02d", $yyyy, $mm, 2);
		$end_date = sprintf("%04d%02d%02d", $next_yyyy, $next_mm, 1);
		break;
	case "2":  // 5日
		$start_date = sprintf("%04d%02d%02d", $yyyy, $mm, 6);
		$end_date = sprintf("%04d%02d%02d", $next_yyyy, $next_mm, 5);
		break;
	case "3":  // 10日
		$start_date = sprintf("%04d%02d%02d", $yyyy, $mm, 11);
		$end_date = sprintf("%04d%02d%02d", $next_yyyy, $next_mm, 10);
		break;
	case "4":  // 15日
		$start_date = sprintf("%04d%02d%02d", $yyyy, $mm, 16);
		$end_date = sprintf("%04d%02d%02d", $next_yyyy, $next_mm, 15);
		break;
	case "5":  // 20日
		$start_date = sprintf("%04d%02d%02d", $yyyy, $mm, 21);
		$end_date = sprintf("%04d%02d%02d", $next_yyyy, $next_mm, 20);
		break;
	case "6":  // 末日
		$start_date = sprintf("%04d%02d%02d", $yyyy, $mm, 1);
		$end_date = sprintf("%04d%02d%02d", $yyyy, $mm, days_in_month($yyyy, $mm));
		break;
	}

	return array("start" => $start_date, "end" => $end_date);
}

// 各実績テーブルのヘッダ行を出力
function write_data_header($with_remain = false, $closing_paid_holiday) {

	if ($closing_paid_holiday == 1) {
		$arr_headers = array("4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月", "1月", "2月", "3月", "合計");
	} else {
		$arr_headers = array();
		for ($i=0; $i<12; $i++) {
			$arr_headers[$i] = ($i+1)."月";
		}
		$arr_headers[12] = "合計";
	}

	echo("<tr align=\"center\">");
	echo("<td width=\"67\" bgcolor=\"#f6f9ff\">&nbsp;</td>");
	if ($with_remain) {
		foreach ($arr_headers as $header) {
			echo("<td width=\"37\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$header</font></td>");
		}
		//残日数を非表示とする 20100705
		//echo("<td width=\"52\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">残日数</font></td>");
	} else {
		foreach ($arr_headers as $header) {
			echo("<td width=\"41\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$header</font></td>");
		}
	}
	echo("</tr>");
}

// 勤務予実績を配列で取得
function get_work_result_array($con, $emp_id, $date, $fname) {

	$ret = array();

	// 勤務日種別を格納
	$sql = "select type from calendar";
	$cond = "where date = '$date'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$ret["type"] = pg_fetch_result($sel, 0, "type");
	} else {
		$ret["type"] = "";
	}

	// 勤務予定情報を格納
	$sql = "select tmcd_group_id, pattern, reason, prov_start_time, prov_end_time from atdbk";
	$cond = "where emp_id = '$emp_id' and date = '$date'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$ret["prov_tmcd_group_id"] = pg_fetch_result($sel, 0, "tmcd_group_id");
		$ret["prov_pattern"] = pg_fetch_result($sel, 0, "prov_pattern");
		$ret["prov_reason"] = pg_fetch_result($sel, 0, "prov_reason");
		$ret["prov_start_time"] = pg_fetch_result($sel, 0, "prov_start_time");
		$ret["prov_end_time"] = pg_fetch_result($sel, 0, "prov_end_time");

		$ret["prov_start_time"] = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $ret["prov_start_time"]);
		$ret["prov_end_time"] = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $ret["prov_end_time"]);
	} else {
		$ret["prov_tmcd_group_id"] = "";
		$ret["prov_pattern"] = "";
		$ret["prov_reason"] = "";
		$ret["prov_start_time"] = "";
		$ret["prov_end_time"] = "";
	}

	// 勤務実績を格納
	$sql = "select * from atdbkrslt";
	$cond = "where emp_id = '$emp_id' and date = '$date'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$ret["tmcd_group_id"] = pg_fetch_result($sel, 0, "tmcd_group_id");
		$ret["pattern"] = pg_fetch_result($sel, 0, "pattern");
		$ret["reason"] = pg_fetch_result($sel, 0, "reason");
		$ret["start_time"] = pg_fetch_result($sel, 0, "start_time");
		$ret["end_time"] = pg_fetch_result($sel, 0, "end_time");
		$ret["out_time"] = pg_fetch_result($sel, 0, "out_time");
		$ret["ret_time"] = pg_fetch_result($sel, 0, "ret_time");

		$ret["start_time"] = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $ret["start_time"]);
		$ret["end_time"] = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $ret["end_time"]);
		$ret["out_time"] = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $ret["out_time"]);
		$ret["ret_time"] = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $ret["ret_time"]);

		for ($i = 1; $i <= 10; $i++) {
			$start_var = "o_start_time$i";
			$end_var = "o_end_time$i";
			$$start_var = pg_fetch_result($sel, 0, "$start_var");
			$$end_var = pg_fetch_result($sel, 0, "$end_var");

			$ret[$start_var] = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $$start_var);
			$ret[$end_var] = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $$end_var);
		}
	} else {
		$ret["tmcd_group_id"] = "";
		$ret["pattern"] = "";
		$ret["reason"] = "";
		$ret["start_time"] = "";
		$ret["end_time"] = "";
		$ret["out_time"] = "";
		$ret["ret_time"] = "";

		for ($i = 1; $i <= 10; $i++) {
			$start_var = "o_start_time$i";
			$end_var = "o_end_time$i";
			$ret[$start_var] = "";
			$ret[$end_var] = "";
		}
	}

	return $ret;
}

//月別の年休数を取得、※未使用
function get_paid_hol_cnt($con, $fname, $emp_id, $start_yyyymm) {

	$arr_cnt = array();

	//開始月確認、4月〜3月、1月〜12月
	$start_mm = substr($start_yyyymm, 4, 2);

	$start_date = $start_yyyymm."01";
	$end_date = date("Ymd");

	$sql = "select date, pattern, reason from atdbkrslt ";
	$cond = "where emp_id = '$emp_id' and date >= '$start_date' and date <= '$end_date' and reason in ('1','2','3','37','38','39','44','55','57','58','62') "; //半有追加 20091110
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	while ($row = pg_fetch_array($sel)) {
		$wk_date = $row["date"];
		$wk_reason = $row["reason"];

		//加算する月の位置
		$wk_mm = substr($wk_date, 4, 2);
		if ($start_mm == "04") {
			if ($wk_mm <= "03") {
				$wk_idx = intval($wk_mm)+8;
			} else {
				$wk_idx = intval($wk_mm)-4;
			}
		} else {
			$wk_idx = intval($wk_mm)-1;
		}

		// 年休1.0日、半年休0.5日加算
		switch ($wk_reason) {
			case "1":
			case "37":
				$wk_day_count = 1.0;
				break;
			case "2":
			case "3":
			case "38":
			case "39":
			case "44":
			case "55": //半夏半有
			case "57": //半有半欠
			case "58": //半特半有
			case "62": //半正半有
				$wk_day_count = 0.5;
				break;
		}
		$arr_cnt[$wk_idx] += $wk_day_count;
	}

	return $arr_cnt;
}

/**
 * 月別の年休数を取得
 *
 * @param mixed $con DBコネクション
 * @param mixed $fname 画面名
 * @param mixed $emp_id 職員ID
 * @param mixed $year 年
 * @param mixed $closing 給与締め日
 * @param mixed $closing_paid_holiday 有休付与日
 * @param mixed $closing_month_flg 月度
 * @param mixed $paid_hol_add_date 採用日6ヶ月後の有休付与日
 * @param mixed $data_migration_date データ移行日
 * @param mixed $obj_hol_hour 時間有休クラス
 * @param mixed $day1_time 所定労働時間（カレンダー）
 * @param mixed $last_add_date 移行データの前回付与日YYYYMMDD形式
 * @param mixed $hol_hour_data_migration_datee 時間有休移行データの移行日YYYYMMDD形式
 * @return mixed 年休数と時間有休の配列、array($arr_cnt, $arr_hour);
 *
 */
function get_paid_hol_cnt2($con, $fname, $emp_id, $year, $closing, $closing_paid_holiday, $closing_month_flg, $paid_hol_add_date, $data_migration_date, $obj_hol_hour, $day1_time, $last_add_date, $hol_hour_data_migration_date) {

	$arr_cnt = array();
	//年、職員ID、締め日、月度
	//4月付与は4月〜3月、採用日
	// 開始日・締め日を算出
	$calced = false;
	switch ($closing) {
	case "1":  // 1日
		$closing_day = 1;
		break;
	case "2":  // 5日
		$closing_day = 5;
		break;
	case "3":  // 10日
		$closing_day = 10;
		break;
	case "4":  // 15日
		$closing_day = 15;
		break;
	case "5":  // 20日
		$closing_day = 20;
		break;
	case "6":  // 末日
		$start_year = $year;
		$start_month = $month;
		$start_day = 1;
		$end_year = $start_year;
		$end_month = $start_month;
		$end_day = days_in_month($end_year, $end_month);
		$calced = true;
		break;
	}
	if (!$calced) {
		$day = date("j");
		$start_day = $closing_day + 1;
		$end_day = $closing_day;
	}

	switch ($closing_paid_holiday) {
		case "1":
			if ($closing == "6") {
				$start_date = $year."0401";
				$end_date = ($year+1)."0331";
			} else {
				$wk_month = ($closing_month_flg == "1") ? "04" : "03";
				$start_date = $year.$wk_month.$start_day;
				$wk_month = ($closing_month_flg == "1") ? "05" : "04";
				$end_date = ($year+1).$wk_month.$end_day;
			}
			break;
		case "2":
			if ($closing == "6") {
				$start_date = $year."0101";
				$end_date = $year."1231";
			} else {
				//月度調整
				if ($closing_month_flg == "1") {
					$start_date = $year."01".$start_day;;
					$end_date = ($year+1)."01".$end_day;
				} else {
					$start_date = ($year-1)."12".$start_day;;
					$end_date = $year."12".$end_day;
				}
			}
			break;
		//採用日
		case "3":
			$wk_month = substr($paid_hol_add_date, 4, 2);
            //範囲を付与日から1年とする 20120710
            $wk_start_day = substr($paid_hol_add_date, 6, 2);
            $start_date = $year.$wk_month.$wk_start_day;
            $wk_end_date = ($year+1).$wk_month.$wk_start_day;
            $end_date = last_date($wk_end_date);			/*
			if ($closing == "6") {
				if ($wk_month == "01") {
					$start_date = $year."0101";
					$end_date = $year."1231";
				} else {
					$start_date = $year.$wk_month."01";
					$wk_end_month = sprintf("%02d", intval($wk_month) - 1);
					$end_date = ($year+1).$wk_end_month."31";	//不具合対応 20100809
				}
			} else {
				//月度調整 当月〜翌月
				if ($closing_month_flg == "1") {
					$start_date = $year.$wk_month.$start_day;;
					$wk_end_month = sprintf("%02d", intval($wk_month) - 1);
					$end_date = ($year+1).$wk_end_month.$end_day;
				} else {
					//前月〜当月
					if ($wk_month == "01") {
						$wk_year = $year-1;
						$wk_month = "12";
						$wk_end_year = $year;
						$wk_end_month = "01";
					} else {
						$wk_year = $year;
						$wk_month = sprintf("%02d", intval($wk_month) - 1);
						$wk_end_year = $year+1;
					}
					$start_date = $wk_year.$wk_month.$start_day;;
					$end_date = $wk_end_year.$wk_month.$end_day;
				}
			}
			*/
			break;

	}
    $org_start_date = $start_date;
	//有給休暇残日数移行データがあり範囲内の場合、移行日より後からカウント
	//移行データの前回付与日の範囲も確認する 20120710
	if ($data_migration_date != "" &&
			($start_date <= $data_migration_date && $data_migration_date <= $end_date) &&
			($start_date <= $last_add_date && $last_add_date <= $end_date)
		) {
		$start_date = next_date($data_migration_date);
	}

	//mmdd、締め日、月判定
	//データ取得 reason:44半有半公は0.5日とする 55半夏半有 57半有半欠 58半特半有 62半正半有 2午前有休 3午後有休 38午前年休 39午後年休
	$sql = " select atdbkrslt.emp_id, date, reason, case when reason='2' or reason='3' or reason='38' or reason='39' or reason = '44' or reason='55' or reason='57' or reason='58' or reason='62' then 0.5 else 1 end as cnt from atdbkrslt ";
	$cond = " where (emp_id = '$emp_id' and date >= '$start_date' and date <= '$end_date') ";
    $cond .= " and ((reason='2' or reason='3' or reason='38' or reason ='39' or reason='44' or reason='55' or reason='57' or reason='58' or reason='62') or (pattern = '10' and reason='1') or (pattern = '10' and reason='37')) ";

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$num = pg_num_rows($sel);
	for ($i=0; $i<$num; $i++) {
		$wk_date = pg_fetch_result($sel, $i, "date");
		$wk_cnt = pg_fetch_result($sel, $i, "cnt");

		//締め日
		if ($closing == "6") {
			$wk_mm = substr($wk_date, 4, 2);
		} else {
			//月度と締め日による調整
			$wk_int_mm = intval(substr($wk_date, 4, 2));
			$wk_dd = intval(substr($wk_date, 6, 2));
			if ($closing_month_flg == "1") {
				// 16-15
				//当月〜翌月、締め日以前の場合、前月に集計
				if ($wk_dd <= $closing_day) {
					if ($wk_int_mm == 1) {
						$wk_int_mm = 12;
					} else {
						$wk_int_mm--;
					}
				}
			} else {
				//前月〜当月、締め日後の場合、翌月に集計
				if ($wk_dd > $closing_day) {
					if ($wk_int_mm == 12) {
						$wk_int_mm = 1;
					} else {
						$wk_int_mm++;
					}
				}
			}
			$wk_mm = sprintf("%02d", $wk_int_mm);
		}
		$arr_cnt[$wk_mm] += $wk_cnt;
	}
	//時間有休追加 20111207
    //月毎の集計
    $arr_hour = array();
    if ($obj_hol_hour->paid_hol_hour_flag == "t") {
        //時間有休移行データがある場合、データ取得用開始日を移行日の翌日とする
        if ($hol_hour_data_migration_date != "" &&
                ($org_start_date <= $hol_hour_data_migration_date &&
                 $hol_hour_data_migration_date <= $end_date)
            ) {
            $start_date = next_date($hol_hour_data_migration_date);
        }
        $arr_hol_hour_dat = $obj_hol_hour->get_paid_hol_hour_month($emp_id, $start_date, $end_date);

		foreach($arr_hol_hour_dat as $wk_date => $hol_hour_dat) {
			//時間を分単位で取得
            $wk_cnt = $hol_hour_dat["use_hour"] * 60 + $hol_hour_dat["use_minute"];
			//締め日
			if ($closing == "6") {
				$wk_mm = substr($wk_date, 4, 2);
			} else {
				//月度と締め日による調整
				$wk_int_mm = intval(substr($wk_date, 4, 2));
				$wk_dd = intval(substr($wk_date, 6, 2));
				if ($closing_month_flg == "1") {
					// 16-15
					//当月〜翌月、締め日以前の場合、前月に集計
					if ($wk_dd <= $closing_day) {
						if ($wk_int_mm == 1) {
							$wk_int_mm = 12;
						} else {
							$wk_int_mm--;
						}
					}
				} else {
					//前月〜当月、締め日後の場合、翌月に集計
					if ($wk_dd > $closing_day) {
						if ($wk_int_mm == 12) {
							$wk_int_mm = 1;
						} else {
							$wk_int_mm++;
						}
					}
				}
				$wk_mm = sprintf("%02d", $wk_int_mm);
			}
			$arr_hour[$wk_mm] += $wk_cnt;
		}
/*
		//所定労働時間
		$specified_time = $obj_hol_hour->get_specified_time($emp_id, $day1_time);
		//月毎に日数換算し加算
		for ($i=1; $i<=12; $i++) {
			$wk_mm = sprintf("%02d", $i);
			if ($mon_tbl[$wk_mm] > 0) {
				$wk_use = $mon_tbl[$wk_mm] / $specified_time;
				//小数点以下２位まで
				$wk_use = $obj_hol_hour->edit_nenkyu_zan($wk_use);
				$arr_cnt[$wk_mm] += $wk_use;
			}
		}
*/
	}
    return array($arr_cnt, $arr_hour);
}

?>
