<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");
require("label_by_profile_type.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 53, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースへ接続
$con = connect2db($fname);

// 施設一覧を取得
$sql = "select * from allotfcl";
$cond = "where del_flg = 'f' order by allotfcl_id";
$sel_fcl = select_from_table($con, $sql, $cond, $fname);
if ($sel_fcl == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 文書登録可能職員名を取得
$sql = "select allotemp.allotfcl_id, empmst.emp_lt_nm, empmst.emp_ft_nm from allotemp inner join empmst on allotemp.emp_id = empmst.emp_id";
$cond = "order by empmst.emp_id";
$sel_emp = select_from_table($con, $sql, $cond, $fname);
if ($sel_emp == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 職員名を配列に保存
$emps = array();
while ($row = pg_fetch_array($sel_emp)) {
	$tmp_fcl_id = $row["allotfcl_id"];
	$tmp_emp_nm = $row["emp_lt_nm"] . " " . $row["emp_ft_nm"];

	if (!array_key_exists($tmp_fcl_id, $emps)) {
		$emps[$tmp_fcl_id] = array();
	}
	$emps[$tmp_fcl_id][] = $tmp_emp_nm;
}
foreach ($emps as $tmp_fcl_id => $emp_nms) {
	$emps[$tmp_fcl_id] = implode(", ", $emp_nms);
}

// イントラメニュー情報を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$menu2 = pg_fetch_result($sel, 0, "menu2");
$menu2_4 = pg_fetch_result($sel, 0, "menu2_4");
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
// 社会福祉法人の場合、「・外来」を削除
if ($profile_type == PROFILE_TYPE_WELFARE) {
	$menu2_4 = mbereg_replace("・外来", "", $menu2_4);
}
?>
<title>イントラネット | <? echo($menu2_4); ?>設定 | 施設一覧</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function deleteItem() {
	if (confirm('削除してよろしいですか？')) {
		document.delform.submit();
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<? echo($session); ?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<? echo($session); ?>"><b>イントラネット</b></a> &gt; <a href="intra_info.php?session=<? echo($session); ?>"><b><? echo($menu2); ?></b></a> &gt; <a href="intra_info_allot.php?session=<? echo($session); ?>"><b><? echo($menu2_4); ?></b></a> &gt; <a href="allot_master_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="intra_info_allot.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="110" align="center" bgcolor="#5279a5"><a href="allot_master_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>施設一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="allot_master_fcl_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設登録</font></a></td>
<td align="right"><input type="button" value="削除" onclick="deleteItem();"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="delform" action="allot_master_fcl_delete.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td width="30" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">削除</font></td>
<td width="240"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設名</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書登録可能職員</font></td>
</tr>
<? show_fcl_list($sel_fcl, $emps, $session); ?>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function show_fcl_list($sel_fcl, $emps, $session) {
	while ($row = pg_fetch_array($sel_fcl)) {
		$id = $row["allotfcl_id"];
		$name = $row["name"];

		echo("<tr>\n");
		echo("<td align=\"center\"><input type=\"checkbox\" name=\"ids[]\" value=\"$id\"></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"allot_master_fcl_update.php?session=$session&id=$id\">$name</a></font></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$emps[$id]}</font></td>\n");
		echo("</tr>\n");
	}
}
?>
