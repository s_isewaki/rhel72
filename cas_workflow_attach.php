<?
require("./about_session.php");
require("./about_authority.php");
require_once("cas_common.ini");
require_once("get_cas_title_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// ワークフロー権限のチェック
$checkauth = check_authority($session, $CAS_MANAGE_AUTH, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$cas_title = get_cas_title_name();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cas_title?>｜ファイル添付</title>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#A0D25A solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#A0D25A">
<td width="520" height="22" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>ファイル添付</b></font></td>
<td width="80" align="center"><a href="javascript:window.close()"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">■閉じる</font></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form action="cas_workflow_attach_save.php" method="post" enctype="multipart/form-data">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="160" align="right" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォーマットファイル</font></td>
<td width="440">
<input type="file" name="file" size="50">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※<? echo(ini_get("upload_max_filesize")); ?>まで</font>
</td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="添付"></td>
</tr>
</table>
</center>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</body>
</html>
