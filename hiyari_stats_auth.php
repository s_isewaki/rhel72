<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("about_postgres.php");
require_once("hiyari_common.ini");
require_once("hiyari_report_class.php");
require_once("show_class_name.ini");
require_once('class/Cmx/Model/SystemConfig.php');


// CmxSystemConfigインスタンス生成
$sysConf = new Cmx_SystemConfig();
// ユーザ画面の新デザインフラグ


//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//====================================
// 権限チェック
//====================================
$auth_chk = check_authority($session, 48, $fname);
if ($auth_chk == '0') {
    showLoginPage();
    exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
//==============================
// 組織階層情報取得
//==============================
$arr_class_name = get_class_name_array($con, $fname);
// 報告書マスタの内容を取得
$rep_obj = new hiyari_report_class($con, $fname);
$grps = $rep_obj->get_all_easy_items();
//縦軸横軸の表示フラグ
$axises_array = $rep_obj->get_axises();

//ユーザー定義項目
$user_items = array();
foreach($grps as $grp_code => $grp){
    if ($grp_code < 10000){
        continue;
    }
    if (!$grp["easy_item_list"][10]){
        continue;
    }
    $key = "user_item_{$grp_code}_10";
    $item_type = $grp["easy_item_list"][10]["easy_item_type"];
    if ($item_type == 'select' || $item_type == 'radio' || $item_type == 'checkbox'){
        $user_items[$key] = $grp["easy_item_list"][10]["easy_item_name"];
    }
}

if($mode == "update") {

    // トランザクションの開始
    pg_query($con, "begin transaction");

    //ユーザ画面の新デザインフラグ

    $subreport_default_value = $_REQUEST['subreport_default_value'];
    $sysConf->set('fantol.aggregate.subreport', $subreport_default_value);


    $auth_class = "";
    $arr_auths = split(",", $inci_auths);
    foreach($arr_auths as $auth)
    {
        //当設定画面表示形式→DB形式(旧画面仕様)に変換
        $setting_name = "setting_".$auth;

        $stats_use_flg = "t";
        $stats_read_div = $$setting_name;
        $stats_read_auth_flg = "f";
        if($$setting_name == "non")
        {
            $stats_use_flg = "f";
            $stats_read_div = "";
        }
        elseif($$setting_name == "auth")
        {
            $stats_read_auth_flg = "t";
        }

        if($stats_read_auth_flg == "t")
        {
            $sql = "select max(target_class) as target_class from inci_auth_emp_post where auth = '$auth'";
            $sel = select_from_table($con, $sql, "", $fname);
            if($sel == 0){
                pg_close($con);
                echo("<script type='text/javascript' src='./js/showpage.js'></script>");
                echo("<script language='javascript'>showErrorPage(window);</script>");
                exit;
            }
            $target_class = pg_fetch_result($sel,0,"target_class");

            if($target_class == "")
            {
                //ダミー設定。このケースの場合、担当範囲=システム全体であるため効果はない。
                $target_class = "1";
            }

            $stats_read_div = $target_class;
        }

        //更新
        update_inci_auth_case($con, $fname, $auth, $stats_use_flg, $stats_read_div,$stats_read_auth_flg);
    }

    // 集計項目の登録
    $axises_keys = $_REQUEST["axises_key"];
    $checkbox = $_REQUEST["chk_flg"];
    $axises_titles = $_REQUEST["axises_title"];
    regist_inci_axises($con, $fname, $axises_keys, $checkbox, $axises_titles, $user_items);

    // トランザクションをコミット
    pg_query($con, "commit");

    $axises_array = $rep_obj->get_axises();

}

$subreport_default_value = $sysConf->get('fantol.aggregate.subreport');
//$subreport_default_value = $subreport_default_value === null ? '0' : $subreport_default_value;

//==============================
//インシデント権限取得
//==============================
$arr_inci = get_inci_auth_case($con, $fname);
$inci_auths = "";
for($i=0; $i<count($arr_inci); $i++)
{
    if($arr_inci[$i]["auth"] != 'GU' && !is_usable_auth($arr_inci[$i]["auth"],$fname,$con))
    {
        continue;
    }

    if($inci_auths != "") {
        $inci_auths .= ",";
    }
    $inci_auths .= $arr_inci[$i]["auth"];
}

//DB形式(旧画面仕様)→当設定画面表示形式に変換
for($i=0; $i<count($arr_inci); $i++)
{
    $setting_name = "setting_".$arr_inci[$i]["auth"];

    $$setting_name = "non";
    if($arr_inci[$i]["stats_use_flg"] == "t")
    {
        $$setting_name = $arr_inci[$i]["stats_read_div"];
        if($arr_inci[$i]["stats_read_auth_flg"] == "t")
        {
            $$setting_name = "auth";
        }
    }
}

$axises_flag_array = array();
$axises_index_array = array();
$axises_default_title_array = array();

$idx = 0;
foreach($axises_array as $axises){
    $$axises["key"] = $axises["title"];
    $axises_index_array[$idx] = $axises["key"];
    $axises_flag_array[$axises["key"]] = $axises["axises_flg"];
    $axises_default_title_array[$axises["key"]] = $axises["default_title"];
    $idx++;
}

$user_items_js = '';
foreach($user_items as $key => $item){
    if ($user_items_js != ''){
        $user_items_js .= ',';
    }
    $user_items_js .= "'$key':'$item'";
}


//登録済みユーザー定義項目数
$used_user_item_count = 0;
foreach($axises_index_array as $key){
    if ($user_items[$key]){
        $used_user_item_count++;
    }
}

/*
if(!empty($eis_id) && $eis_item_title[240]) {
    $patient_division_2_title = $eis_item_title[240];
}

else {
    $patient_division_2_title = $grps[240]["easy_item_list"][70]["easy_item_name"];
}
*/

/*
$level =  $grps[90]["easy_item_list"][10]["easy_item_name"];
$place = $grps[110]["easy_item_list"][60]["easy_item_name"];
$generating_post = $grps[116]["easy_item_list"][10]["easy_item_name"];
$month  = $grps[100]["easy_item_list"][10]["easy_item_name"];
$weekday  = $grps[100]["easy_item_list"][20]["easy_item_name"];
$time   = $grps[100]["easy_item_list"][40]["easy_item_name"];
//$time2                 = "発生時間帯：1時間毎";
$time3  =  $grps[105]["easy_item_list"][40]["easy_item_name"];
//$time4                 = "発見時間帯：1時間毎";
$report_month   = "報告月";
$registrant_job   = "報告者職種";

$d_job                 = $grps[3020]["easy_item_list"][10]["easy_item_name"];
$d_job_year            = $grps[3036]["easy_item_list"][10]["easy_item_name"];
$d_class_year          = $grps[3038]["easy_item_list"][10]["easy_item_name"];

$t_job                 = $grps[3050]["easy_item_list"][30]["easy_item_name"];
$job_year              = $grps[3250]["easy_item_list"][70]["easy_item_name"];
$t_night_count         = $grps[3350]["easy_item_list"][110]["easy_item_name"];
$class_year          = $grps[3300]["easy_item_list"][90]["easy_item_name"];

$patient_sex           = $grps[210]["easy_item_list"][30]["easy_item_name"];
$patient_year          = $grps[210]["easy_item_list"][40]["easy_item_name"];
//$patient_division_2    = $patient_division_2_title;

$assessment_score      = $grps[150]["easy_item_list"][10]["easy_item_name"];
$asses_150_6           = $grps[150]["easy_item_list"][6]["easy_item_name"];
$restrict              = $grps[150]["easy_item_list"][7]["easy_item_name"];
$asses_150_8           = $grps[150]["easy_item_list"][8]["easy_item_name"];
$risyou_sensor         = $grps[150]["easy_item_list"][5]["easy_item_name"];
$asses_150_20          = $grps[150]["easy_item_list"][20]["easy_item_name"];
$life_system           = $grps[150]["easy_item_list"][30]["easy_item_name"];
$asses_150_40          = $grps[150]["easy_item_list"][40]["easy_item_name"];
$asses_150_50          = $grps[150]["easy_item_list"][50]["easy_item_name"];
$asses_150_60          = $grps[150]["easy_item_list"][60]["easy_item_name"];
$asses_150_70          = $grps[150]["easy_item_list"][70]["easy_item_name"];
$asses_150_80          = $grps[150]["easy_item_list"][80]["easy_item_name"];

$ic                    = "影響と対応";

$patient_state         = $grps[290]["easy_item_list"][120]["easy_item_name"];
//$summary               = $summary_title;
//$hiyari_summary        = $hiyari_summary_title;
$scene_content         = "発生場面と内容";
//$scene_content_2010    = $scene_content_2010_title;

$factor                = $grps[600]["easy_item_list"][10]["easy_item_name"];
$clinic               = $grps[130]["easy_item_list"][90]["easy_item_name"];

$risk_juudai           = $grps[1500]["easy_item_list"][10]["easy_item_name"]; // リスクの評価：重大性
$risk_kinkyu           = $grps[1502]["easy_item_list"][10]["easy_item_name"]; // リスクの評価：緊急性
$risk_hindo            = $grps[1504]["easy_item_list"][10]["easy_item_name"]; // リスクの評価：頻度
$risk_yosoku           = $grps[1510]["easy_item_list"][10]["easy_item_name"]; // リスクの予測
$system_kaizen         = $grps[1520]["easy_item_list"][10]["easy_item_name"]; // システム改善の必要性
$kyouiku_kenshu        = $grps[1530]["easy_item_list"][10]["easy_item_name"]; // 教育研修への活用

$timelag_class = '発見から報告までの時間';
$post = $arr_class_name[0];
$post_dept = '組織階層3';
*/
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$INCIDENT_TITLE?> | 集計統計分析権限設定</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tablednd.js"></script>
<script type="text/javascript">

//ユーザー定義項目
var user_items = {<?=$user_items_js?>};
var empty_row = null;

$(function() {
    //軸名の並び替えを行うスクリプト
    $('#eislist tbody').tableDnD({
        dragHandle: "dragHandle"
    });

    $('#eislist tbody tr').hover(hover_on, hover_off);

    $('#eislist select').each(function(){
        var val = $(this).val();
        if (val == ''){
            empty_row = $(this).closest('tr').clone();
            return false;
        }
    });
    // ユーザー定義項目変更イベント
    $('#eislist select').change(chenage_user_item);
});

//表の行に乗った時に、カーソルのアイコンをmoveに変更する。
function hover_on(){
    $(this.cells).css("cursor","move");
    $(this.cells[0]).addClass('showDragHandle');
    $(this.cells).css('background-color', 'gold');//カーソルを載った場所の色を変更する
}

//カーソルが行から移動したときに色を変更する
function hover_off(){
    $(this.cells[0]).removeClass('showDragHandle');//
    $(this.cells).css('background-color', '#F5FFE5');
}

// ユーザー定義項目変更
var max_no = <?=count($axises_array)?>;
function chenage_user_item(){
    var val = $(this).val();
    var row = $(this).closest('tr');

    $(row).find("input[type='hidden']").val(val);

    if (empty_row == null && val == ''){
        empty_row = $(row).clone();
    }

    // 選択済みユーザー定義項目
    var exist_empty = false;
    var selected_user_items = {};
    $('#eislist select').each(function(){
        var val = $(this).val();
        if (val != ''){
            selected_user_items[val]=val;
        } else {
            exist_empty = true;
        }
    });

    // 行追加
    if (!exist_empty && Object.keys(selected_user_items).length < Object.keys(user_items).length){
        var newrow = $(empty_row).clone();
        max_no++;
        $(newrow).find("input[type='hidden']").attr('name', 'axises_key['+max_no+']').val('');
        $(newrow).find("input[type='checkbox']").attr('name', 'chk_flg['+max_no+']').val('t');
        $(newrow).find("select").attr('name', 'user_item['+max_no+']').val('').change(chenage_user_item);
        $(newrow).find("input[type='text']").attr('name', 'axises_title['+max_no+']').val('');
        $(newrow)
            .hover(hover_on, hover_off)
            .appendTo('#eislist tbody');

        $('#eislist tbody').tableDnD({
            dragHandle: "dragHandle"
        });
    }

    // 選択肢再設定
    $('#eislist select').each(function(){
        var val = $(this).val();
        $(this).find("option").remove();
        $(this).append($('<option>'));
        for (var key in user_items){
            if (key == val || selected_user_items[key] == null){
                $(this).append($('<option>').val(key).text(user_items[key]));
            }
        }
        $(this).val(val);
    });

}

function init_page()
{
}

function update()
{
    document.mainform.mode.value="update";
    document.mainform.submit();
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="init_page();">
<form name="mainform" action="hiyari_stats_auth.php" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="mode" value="">
<input type="hidden" name="inci_auths" value="<?=$inci_auths?>">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー -->
<?
show_hiyari_header($session,$fname,true);
?>
<!-- ヘッダー -->

<img src="img/spacer.gif" width="1" height="5" alt=""><br>



<table width="800" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td valign="top" align="left" bgcolor="#F5FFE5">


<!-- 受信一覧 START-->
<table border="0" cellspacing="1" cellpadding="0" class="list">
<tr>
<td width="180" bgcolor="#DFFFDC" align="center" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><B>集計・統計分析権限設定</B></font>
</td>
</tr>
</table>



<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="10" height="5" alt=""></td>
</tr>
</table>





<!-- 参照 START -->
<?
if($arr_class_name["class_cnt"] == 4)
{
    $input_col_count = 7;
    $input_col_width = 80*7;
}
else
{
    $input_col_count = 6;
    $input_col_width = 80*6;
}

?>
<table border="0" cellspacing="1" cellpadding="0" class="list">

<tr>
<td width="200" bgcolor="#DFFFDC" align="center" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;</font>
</td>
<td width="80" bgcolor="#DFFFDC" align="center" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">全て</font>
</td>
<?
for($j=0; $j<$arr_class_name["class_cnt"]; $j++)
{
?>
<td width="80" bgcolor="#DFFFDC" align="center" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$arr_class_name[$j]?></font>
</td>
<?
}
?>
<td width="80" bgcolor="#DFFFDC" align="center" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">担当範囲</font>
</td>
<td width="80" bgcolor="#DFFFDC" align="center" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">不可</font>
</td>
</tr>


<?
for($i=0; $i<count($arr_inci); $i++)
{
    $setting_name = "setting_".$arr_inci[$i]["auth"];
?>




<tr>
<td width="200" bgcolor="#DFFFDC" align="center" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$arr_inci[$i]["auth_name"]?></font>
</td>
<td width="80" bgcolor="#FFFFFF" align="center" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="<?=$setting_name?>" value="0" <?   if($$setting_name == "0"){?>checked<?}   ?> ></font>
</td>
<?
for($j=0; $j<$arr_class_name["class_cnt"]; $j++)
{
?>
<td width="80" bgcolor="#FFFFFF" align="center" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="<?=$setting_name?>" value="<?=$j+1?>" <?   if($$setting_name == $j+1){?>checked<?}   ?> ></font>
</td>
<?
}
?>
<td width="80" bgcolor="#FFFFFF" align="center" nowrap>
<?
if($arr_inci[$i]["auth"] == 'GU')
{
?>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">-</font>
<?
}
else
{
?>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="<?=$setting_name?>" value="auth" <?   if($$setting_name == "auth"){?>checked<?}   ?> ></font>
<?
}
?>
</td>
<td width="80" bgcolor="#FFFFFF" align="center" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="<?=$setting_name?>" value="non" <?   if($$setting_name == "non"){?>checked<?}   ?> ></font>
</td>
</tr>


<?
}
?>


<tr>
<td bgcolor="#FFFFFF" align="left" colspan="8" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">担当範囲をチェックした場合は、担当者設定の条件に準じます。</font>
</td>
</tr>



</table>
<!-- 参照 END -->


<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="10" height="5" alt=""></td>
</tr>
</table>



<table width="100%" border="0" cellspacing="1" cellpadding="0">
<tr>
<td align="right">
<input type="button" value="登録" onclick="update();">
</td>
</tr>
</table>


<!-- 集計 START-->
<table border="0" cellspacing="1" cellpadding="0" class="list">
<tr>
<td width="180" bgcolor="#DFFFDC" align="center" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><B>集計</B></font>
</td>
</tr>
</table>

<table width="300" border="0" cellspacing="0" cellpadding="1" class="list" id="eislist">
<thead>

<tr height="22">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#FF0000">※ ドラッグ アンド ドロップで並び替えを行えます。並び替え後は「登録」ボタンをクリックしてください。</font>
<td align="center" bgcolor="#DFFFDC" width="10" style="white-space: nowrap;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">並替</font></td>
<td align="center" bgcolor="#DFFFDC" width="40" style="white-space: nowrap;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">表示</font></td>
<td align="center" bgcolor="#DFFFDC" width="125" style="white-space: nowrap;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">デフォルト名称</font></td>
<td align="center" bgcolor="#DFFFDC" width="125" style="white-space: nowrap;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">軸名</font></td>
</tr>
</thead>
<tbody>
<? for($i=0;$i<count($axises_index_array);$i++){ ?>
<tr>
<td class="dragHandle" align="left" bgcolor="#F5FFE5">
    <input type="hidden" name="axises_key[<?=$i?>]" value="<?=$axises_index_array[$i]?>">
</td>
<td width="40" class="nodrop nodrag" align="center" bgcolor="#F5FFE5" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
        <input type="checkbox" name="chk_flg[<?=$i?>]" value="t"<?=($axises_flag_array[$axises_index_array[$i]] == "t")?" checked":""?>>
    </font>
</td>
<td width="225" bgcolor="#F5FFE5" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
        <?if (strpos($axises_index_array[$i], 'user_item_')===0){?>
            <select name="user_item[<?=$i?>]" width="210">
                <option></option>
                <? foreach($user_items as $key => $item) { ?>
                    <?if ($axises_index_array[$i]==$key || !in_array($key, $axises_index_array)){?>
                        <option value="<?=$key?>"<?=$axises_index_array[$i]==$key?' selected':''?>><?=$item?></option>
                    <?}?>
                <?}?>
            </select>
        <?}else{?>
            <?=$axises_default_title_array[$axises_index_array[$i]]?>
        <?}?>
    </font>
</td>
<td width="125" class="nodrop nodrag" bgcolor="#F5FFE5" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
        <input type="text" size="40" name="axises_title[<?=$i?>]" value="<?=$$axises_index_array[$i]?>">
    </font>
</td>
</tr>
<? }?>

<? if($used_user_item_count < count($user_items)){?>
<tr>
<td class="dragHandle" align="left" bgcolor="#F5FFE5">
    <input type="hidden" name="axises_key[<?=$i?>]" value="">
</td>
<td width="40" class="nodrop nodrag" align="center" bgcolor="#F5FFE5" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
        <input type="checkbox" name="chk_flg[<?=$i?>]" value="t" checked>
     </font>
</td>
<td width="225" bgcolor="#F5FFE5" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
        <select name="user_item[<?=$i?>]" width="210">
            <option></option>
            <? foreach($user_items as $key => $item) { ?>
                <?if (!in_array($key, $axises_index_array)){?>
                    <option value="<?=$key?>"><?=$item?></option>
                <?}?>
            <?}?>
        </select>
    </font>
</td>
<td width="125" class="nodrop nodrag" bgcolor="#F5FFE5" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
        <input type="text" size="40" name="axises_title[<?=$i?>]">
    </font>
</td>
</tr>
<? }?>



</tbody>
</table>


<!-- 受信一覧 START-->
<br>
<table border="0" cellspacing="1" cellpadding="0" class="list">
<tr>
<td width="180" bgcolor="#DFFFDC" align="center" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><B>集計条件の初期設定</B></font>
</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td width="100%" bgcolor="#F5FFE5" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">

<input type="hidden" name="subreport_default_value" value="f">
<input type="checkbox" name="subreport_default_value" value="t" <?if($subreport_default_value == 't'){?>checked<?}?>>集計・統計画面において、「副報告を分析に含める」に初期チェックを入れる
</font>
</td>
</tr>
</table>




<table width="100%" border="0" cellspacing="1" cellpadding="0">
<tr>
<td align="right">
<input type="button" value="登録" onclick="update();">
</td>
</tr>
</table>



</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>


<!-- フッター START -->
<?
show_hiyari_footer($session,$fname,true);
?>
<!-- フッター END -->

</td>
</tr>


</table>

</form>
</body>
</html>

<?
function get_inci_auth_case($con, $fname)
{

    $sql  = "select a.*, b.auth_rank, case when a.auth = 'GU' then '一般ユーザー' else b.auth_name end ";
    $sql .= "from inci_auth_case a left join inci_auth_mst b on a.auth = b.auth ";
    $cond = "order by auth_rank is not null, auth_rank asc";
    $sel = select_from_table($con, $sql, $cond, $fname);

    if ($sel == 0)
    {
        pg_close($con);
        echo("<script type='text/javascript' src='js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    $array = pg_fetch_all($sel);
    return $array;
}

function update_inci_auth_case($con, $fname, $auth, $stats_use_flg, $stats_read_div,$stats_read_auth_flg)
{

    $sql = "update inci_auth_case set";
    $set = array("stats_use_flg","stats_read_div","stats_read_auth_flg");
    $setvalue = array("$stats_use_flg", "$stats_read_div","$stats_read_auth_flg");
    $cond = "where auth = '$auth'";

    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
    if ($upd == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}


//    function update_agreement_target_class($con, $fname, $auth_class)
//    {
//        $inci_auth_case_report_db_update_2 = false;
//        $inci_auth_emp_post_update_2 = false;
//        foreach($auth_class as $auth=>$agreement_target_class)
//        {
//            $sql = "select * from inci_auth_case where auth = '$auth';";
//            $sel = select_from_table($con, $sql, "", $fname);
//            if($sel == 0){
//                pg_close($con);
//                echo("<script type='text/javascript' src='./js/showpage.js'></script>");
//                echo("<script language='javascript'>showErrorPage(window);</script>");
//                exit;
//            }
//            $report_db_use_flg = pg_fetch_result($sel,0,"report_db_use_flg");
//            $report_db_read_div = pg_fetch_result($sel,0,"report_db_read_div");
//            $inci_auth_case_report_db_update = false;
//            if($report_db_use_flg == "t" && $report_db_read_div != 0 && $report_db_read_div != $agreement_target_class)
//            {
//                $inci_auth_case_report_db_update = true;
//            }
//
//            if($inci_auth_case_report_db_update)
//            {
//                $sql = "update inci_auth_case set";
//                $set = array("report_db_read_div");
//                $setvalue = array($agreement_target_class);
//                $cond = "where auth = '$auth'";
//                $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
//                if ($upd == 0) {
//                    pg_exec($con, "rollback");
//                    pg_close($con);
//                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
//                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
//                    exit;
//                }
//            }
//
//            $sql = "select * from inci_auth_emp_post where auth = '$auth' and auth_part = '決裁者'";
//            $sel = select_from_table($con, $sql, "", $fname);
//            if($sel == 0){
//                pg_close($con);
//                echo("<script type='text/javascript' src='./js/showpage.js'></script>");
//                echo("<script language='javascript'>showErrorPage(window);</script>");
//                exit;
//            }
//            if(pg_num_rows($sel) == 1)
//            {
//                $use_flg = pg_fetch_result($sel,0,"use_flg");
//                $target_class = pg_fetch_result($sel,0,"target_class");
//                $inci_auth_emp_post1_update = false;
//                if($use_flg == "t" && $target_class != $agreement_target_class)
//                {
//                    $inci_auth_emp_post1_update = true;
//                }
//            }
//
//            $sql = "select * from inci_auth_emp_post where auth = '$auth' and auth_part = '審議者'";
//            $sel = select_from_table($con, $sql, "", $fname);
//            if($sel == 0){
//                pg_close($con);
//                echo("<script type='text/javascript' src='./js/showpage.js'></script>");
//                echo("<script language='javascript'>showErrorPage(window);</script>");
//                exit;
//            }
//            if(pg_num_rows($sel) == 1)
//            {
//                $use_flg = pg_fetch_result($sel,0,"use_flg");
//                $target_class = pg_fetch_result($sel,0,"target_class");
//                $inci_auth_emp_post2_update = false;
//                if($use_flg == "t" && $target_class != $agreement_target_class)
//                {
//                    $inci_auth_emp_post2_update = true;
//                }
//            }
//
//            if($inci_auth_emp_post1_update || $inci_auth_emp_post2_update)
//            {
//                $sql = "update inci_auth_emp_post set";
//                $set = array("target_class");
//                $setvalue = array($agreement_target_class);
//                $cond = "where auth = '$auth' and use_flg";
//                $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
//                if ($upd == 0) {
//                    pg_exec($con, "rollback");
//                    pg_close($con);
//                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
//                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
//                    exit;
//                }
//            }
//
//            if($inci_auth_case_report_db_update)
//            {
//                $inci_auth_case_report_db_update_2 = true;
//            }
//            if($inci_auth_emp_post1_update || $inci_auth_emp_post2_update)
//            {
//                $inci_auth_emp_post_update_2 = true;
//            }
//        }
//
//        if($inci_auth_case_report_db_update_2 || $inci_auth_emp_post_update_2)
//        {
//            if($inci_auth_case_report_db_update_2 && !$inci_auth_emp_post_update_2)
//            {
//                $update_message = "報告ファイル権限設定に部署指定を反映しました。";
//            }
//            elseif(!$inci_auth_case_report_db_update_2 && $inci_auth_emp_post_update_2)
//            {
//                $update_message = "担当者設定に部署指定を反映しました。";
//            }
//            else
//            {
//                $update_message = "担当者設定と報告ファイル権限設定に部署指定を反映しました。";
//            }
//            echo("<script type=\"text/javascript\">alert(\"$update_message\");</script>");
//        }
//    }


function regist_inci_axises($con, $fname, $axises_keys, $disp_flgs, $titles, $user_items)
{
    $sql = "delete from inci_axises_title";
    $cond = "where key like 'user#_item#_%' escape '#'";
    $del = delete_from_table($con, $sql, $cond, $fname);
    if ($del == 0) {
        pg_exec($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $order_no = 1;
    foreach ($axises_keys as $idx => $key) {
        $disp_flg = $disp_flgs[$idx];
        $title = pg_escape_string($titles[$idx]);
        $axises_flg = ($disp_flg == 't') ? 't' : 'f';
        if (strpos($key, 'user_item_')===0){
            // ユーザー定義項目
            $default_title = pg_escape_string($user_items[$key]);
            $sql = "insert into inci_axises_title (key, axises_flg, default_title, title, order_no) values (";
            $content = array($key, $axises_flg, $default_title, $title, $order_no);
            $ins = insert_into_table($con, $sql, $content, $fname);
            if ($ins == 0) {
                pg_exec($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
        } else {
            $sql = 'update inci_axises_title set';
            $set = array('axises_flg', 'title', 'order_no');
            $setvalue = array($axises_flg, $title, $order_no);
            $cond = "where key = '$key'";
            $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
            if ($upd == 0) {
                pg_exec($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
        }
        $order_no++;
    }
}

//==============================
//DBコネクション終了
//==============================
pg_close($con);
?>