<?
ob_start();
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<?
require_once("about_session.php");
require_once("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

if ($year == "") {$year = date("Y");}
if ($month == "") {$month = date("m");}
if ($day == "") {$day = date("d");}

// データベースに接続
$con = connect2db($fname);

// 移動状況リストを作成
$sql  = "select ";
$sql .= "m.ptif_id, ";
$sql .= "fw.ward_name as from_ward_name, ";
$sql .= "fr.ptrm_name as from_ptrm_name, ";
$sql .= "m.from_bed_no, ";
$sql .= "fd.dr_nm as from_dr_nm, ";
$sql .= "tw.ward_name as to_ward_name, ";
$sql .= "tr.ptrm_name as to_ptrm_name, ";
$sql .= "m.to_bed_no, ";
$sql .= "td.dr_nm as to_dr_nm, ";
$sql .= "p.ptif_lt_kaj_nm, ";
$sql .= "p.ptif_ft_kaj_nm, ";
$sql .= "p.ptif_sex, ";
$sql .= "m.move_dt, ";
$sql .= "m.move_tm, ";
$sql .= "m.move_reason, ";
$sql .= "m.move_cfm_flg, ";
$sql .= "m.updated ";
$sql .= "from inptmove m ";
$sql .= "inner join ptifmst p on p.ptif_id = m.ptif_id ";
$sql .= "inner join wdmst fw on fw.bldg_cd = m.from_bldg_cd and fw.ward_cd = m.from_ward_cd ";
$sql .= "inner join ptrmmst fr on fr.bldg_cd = m.from_bldg_cd and fr.ward_cd = m.from_ward_cd and fr.ptrm_room_no = m.from_ptrm_room_no ";
$sql .= "inner join wdmst tw on tw.bldg_cd = m.to_bldg_cd and tw.ward_cd = m.to_ward_cd ";
$sql .= "inner join ptrmmst tr on tr.bldg_cd = m.to_bldg_cd and tr.ward_cd = m.to_ward_cd and tr.ptrm_room_no = m.to_ptrm_room_no ";
$sql .= "left join drmst fd on fd.sect_id = m.from_sect_id and fd.dr_id = m.from_dr_id ";
$sql .= "left join drmst td on td.sect_id = m.to_sect_id and td.dr_id = m.to_dr_id ";
$cond  = "where m.move_dt >= '$year$month$day' and m.move_del_flg = 'f' ";
$cond .= "order by m.move_dt, m.move_tm";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$moves = array();
while ($row = pg_fetch_array($sel)) {
	$moves[] = array(
		"from_ward_name" => $row["from_ward_name"],
		"from_ptrm_name" => $row["from_ptrm_name"],
		"from_bed_no" => $row["from_bed_no"],
		"from_dr_nm" => $row["from_dr_nm"],
		"to_ward_name" => $row["to_ward_name"],
		"to_ptrm_name" => $row["to_ptrm_name"],
		"to_bed_no" => $row["to_bed_no"],
		"to_dr_nm" => $row["to_dr_nm"],
		"pt_nm" => $row["ptif_lt_kaj_nm"] . " " . $row["ptif_ft_kaj_nm"],
		"pt_sex" => $row["ptif_sex"],
		"move_dt" => $row["move_dt"],
		"move_tm" => $row["move_tm"],
		"move_reason" => $row["move_reason"],
		"operators" => get_operators($con, $row["ptif_id"], $fname),
		"cfm_flg" => $row["move_cfm_flg"],
		"updated" => $row["updated"]
	);
}
unset($sel);
?>
</head>
<body>
<table border="1">
<tr bgcolor="#CCFFFF">
<td nowrap align="center">NEW</td>
<td nowrap align="center">現病棟</td>
<td nowrap align="center">病室</td>
<td nowrap align="center">ベッドNo</td>
<td nowrap align="center">氏名</td>
<td nowrap align="center">性別</td>
<td nowrap align="center">主治医</td>
<td nowrap align="center">担当</td>
<td nowrap align="center">→</td>
<td nowrap align="center">移動日</td>
<td nowrap align="center">曜日</td>
<td nowrap align="center">新病棟</td>
<td nowrap align="center">病室</td>
<td nowrap align="center">ベッドNo</td>
<td nowrap align="center">主治医</td>
<td nowrap align="center">担当</td>
<td nowrap align="center">ステータス</td>
<td nowrap align="center">備考</td>
</tr>
<?
$new_start = date("YmdHis", strtotime("-1 day"));
foreach ($moves as $move) {
?>
<tr>
<td nowrap align="center"><? echo(format_new($new_start, $move["updated"])); ?></td>
<td nowrap><? echo($move["from_ward_name"]); ?></td>
<td nowrap><? echo($move["from_ptrm_name"]); ?></td>
<td nowrap align="center"><? echo($move["from_bed_no"]); ?></td>
<td nowrap><? echo($move["pt_nm"]); ?></td>
<td nowrap><? echo(format_sex($move["pt_sex"])); ?></td>
<td nowrap align="center"><? echo($move["from_dr_nm"]); ?></td>
<td nowrap><? echo(format_operators($move["operators"])); ?></td>
<td nowrap align="center">→</td>
<td nowrap align="center"><? echo(format_datetime($move["move_dt"], $move["move_tm"])); ?></td>
<td nowrap align="center"><? echo(format_weekday($move["move_dt"])); ?></td>
<td nowrap><? echo($move["to_ward_name"]); ?></td>
<td nowrap><? echo($move["to_ptrm_name"]); ?></td>
<td nowrap align="center"><? echo($move["to_bed_no"]); ?></td>
<td nowrap><? echo($move["to_dr_nm"]); ?></td>
<td nowrap><? echo(format_operators($move["operators"])); ?></td>
<td nowrap><? echo(format_status($move["cfm_flg"])); ?></td>
<td nowrap><? echo(format_text($move["move_reason"])); ?></td>
</tr>
<?
}
?>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
$excel_data = mb_convert_encoding(ob_get_clean(), "Shift_JIS", 'EUC-JP');
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=move_report.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
header("Pragma: public");
echo($excel_data);

function get_operators($con, $ptif_id, $fname) {
	$sql = "select e.emp_lt_nm, e.emp_ft_nm from inptop i inner join empmst e on e.emp_id = i.emp_id";
	$cond = "where i.ptif_id = '$ptif_id' and exists (select * from jobmst j where j.job_id = e.emp_job and j.bed_op_flg) order by i.order_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$operators = array();
	while ($row = pg_fetch_array($sel)) {
		$operators[] = $row["emp_lt_nm"] . " " . $row["emp_ft_nm"];
	}
	return $operators;
}

function format_new($new_start, $updated) {
	if ($updated >= $new_start) {
		return "NEW";
	}
}

function format_sex($sex) {
	switch ($sex) {
	case "0":
		return "不明";
	case "1":
		return "男性";
	case "2":
		return "女性";
	default:
		return "";
	}
}

function format_datetime($ymd, $hm) {
	return preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $ymd) . " " . preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $hm);
}

function format_weekday($ymd) {
	$year = substr($ymd, 0, 4);
	$month = substr($ymd, 4, 2);
	$day = substr($ymd, 6, 2);

	switch (date("w", mktime(0, 0, 0, $month, $day, $year))) {
	case "0":
		return "日";
	case "1":
		return "月";
	case "2":
		return "火";
	case "3":
		return "水";
	case "4":
		return "木";
	case "5":
		return "金";
	case "6":
		return "土";
	}
}

function format_operators($operators) {
	return join("<br>", $operators);
}

function format_text($text) {
	return str_replace("\r\n", "<br>", $text);
}

function format_status($cfm_flg) {
	return ($cfm_flg == "t") ? "移動済み" : "移動予定";
}
?>
