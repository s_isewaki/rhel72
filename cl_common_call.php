<?
//ini_set( 'display_errors', 1 );
require_once( "Cmx.php");
require_once( "cl_common_log_class.inc");
$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

foreach( $_REQUEST as $key => $value ) {
	$log->debug('$_REQUEST['.$key.']:'.$_REQUEST[$key],__FILE__,__LINE__);
}

/*
共通モジュールの呼び出しを行います。

共通モジュールはcl/common配下に配置されていますが、
このモジュールを経由することで、
共通モジュール側の実装を、ルート直下で実行さる場合と同じロジックで記述することができます。
*/
require("cl/common/".$common_module.".php");

$log->info(basename(__FILE__)." END");
