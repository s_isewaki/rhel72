<?
require("about_session.php");
require("about_authority.php");
require_once("atdbk_workflow_common_class.php");
require("show_attendance_pattern.ini");
require("show_clock_in_common.ini");
require("timecard_common_class.php");
require_once("get_values.ini");
require_once("timecard_bean.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
// 勤務シフト作成より 
$auth_id = 69;
$checkauth = check_authority($session, $auth_id, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
//ログインユーザID
$login_emp_id = get_emp_id($con,$session,$fname);
//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);

$ret1_str = ($timecard_bean->return_icon_flg != "2") ? "退勤復帰" : "呼出出勤";
$ret2_str = ($timecard_bean->return_icon_flg != "2") ? "復帰退勤" : "呼出退勤";

// 入力チェック
// 職員をループ
for ($i = 0; $i < count($emp_ids); $i++) {
	$wk_emp_id = $emp_ids[$i];
	$wk_emp_name = get_emp_kanji_name($con, $wk_emp_id, $fname);
	
	//給与支給区分が月給制か確認
	$is_monthly_emp = is_monthly_emp($con, $wk_emp_id, $fname);
	// 月給者は出勤予定なしでの打刻を不可とする
	if ($is_monthly_emp && $pattern[$i] == "--" &&
			($start_hours[$i] != "" || $start_mins[$i] != "" || $end_hours[$i] != "" || $end_mins[$i] != "") ) {
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の勤務実績を選択してください。');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	} 
	//出勤時刻の時分チェック
	if (($start_hours[$i] == "" && $start_mins[$i] != "") || ($start_hours[$i] != "" && $start_mins[$i] == "")) {
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の出勤時刻の時と分の一方のみは登録できません。');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
	if (($start_hours[$i] != "" && !preg_match("/^[0-9]*$/", $start_hours[$i])) || ($start_mins[$i] != "" && !preg_match("/^[0-9]*$/", $start_mins[$i]))) {	
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の出勤時刻の時と分には数値を入力してください。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
	
	//退勤時刻の時分チェック
	if (($end_hours[$i] != "" && $end_mins[$i] == "") || ($end_hours[$i] == "" && $end_mins[$i] != "")) {
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の退勤時刻の時と分の一方のみは登録できません。');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
	if (($end_hours[$i] != "" && !preg_match("/^[0-9]*$/", $end_hours[$i])) || ($end_mins[$i] != "" && !preg_match("/^[0-9]*$/", $end_mins[$i]))) {	
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の退勤時刻の時と分には数値を入力してください。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
	//残業開始時刻1の時分チェック
	if (($over_start_hours[$i] == "" && $over_start_mins[$i] != "") || ($over_start_hours[$i] != "" && $over_start_mins[$i] == "")) {
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の残業開始時刻１の時と分の一方のみは登録できません。');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
	if (($over_start_hours[$i] != "" && !preg_match("/^[0-9]*$/", $over_start_hours[$i])) || ($over_start_mins[$i] != "" && !preg_match("/^[0-9]*$/", $over_start_mins[$i]))) {	
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の残業開始時刻１の時と分には数値を入力してください。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
	//残業終了時刻の時分チェック
	if (($over_end_hours[$i] != "" && $over_end_mins[$i] == "") || ($over_end_hours[$i] == "" && $over_end_mins[$i] != "")) {
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の残業終了時刻１の時と分の一方のみは登録できません。');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
	if (($over_end_hours[$i] != "" && !preg_match("/^[0-9]*$/", $over_end_hours[$i])) || ($over_end_mins[$i] != "" && !preg_match("/^[0-9]*$/", $over_end_mins[$i]))) {	
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の残業終了時刻１の時と分には数値を入力してください。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
	//残業開始時刻２の時分チェック
	if (($over_start_hours2[$i] == "" && $over_start_mins2[$i] != "") || ($over_start_hours2[$i] != "" && $over_start_mins2[$i] == "")) {
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の残業開始時刻２の時と分の一方のみは登録できません。');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
	if (($over_start_hours2[$i] != "" && !preg_match("/^[0-9]*$/", $over_start_hours2[$i])) || ($over_start_mins2[$i] != "" && !preg_match("/^[0-9]*$/", $over_start_mins2[$i]))) {	
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の残業開始時刻２の時と分には数値を入力してください。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
	//残業終了時刻の時分チェック
	if (($over_end_hours2[$i] != "" && $over_end_mins2[$i] == "") || ($over_end_hours2[$i] == "" && $over_end_mins2[$i] != "")) {
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の残業終了時刻２の時と分の一方のみは登録できません。');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
	if (($over_end_hours2[$i] != "" && !preg_match("/^[0-9]*$/", $over_end_hours2[$i])) || ($over_end_mins2[$i] != "" && !preg_match("/^[0-9]*$/", $over_end_mins2[$i]))) {	
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の残業終了時刻２の時と分には数値を入力してください。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
	//法定内残業の時分チェック
	if (($legal_in_over_hours[$i] != "" && $legal_in_over_mins[$i] == "") || ($legal_in_over_hours[$i] == "" && $legal_in_over_mins[$i] != "")) {
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の法定内残業の時と分の一方のみは登録できません。');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
	//外出時刻の時分チェック
	if (($out_hours[$i] == "" && $out_mins[$i] != "") || ($out_hours[$i] != "" && $out_mins[$i] == "")) {
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の外出時刻の時と分の一方のみは登録できません。');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
	if (($out_hours[$i] != "" && !preg_match("/^[0-9]*$/", $out_hours[$i])) || ($out_mins[$i] != "" && !preg_match("/^[0-9]*$/", $out_mins[$i]))) {	
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の外出時刻の時と分には数値を入力してください。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
	//戻り時刻の時分チェック
	if (($ret_hours[$i] != "" && $ret_mins[$i] == "") || ($ret_hours[$i] == "" && $ret_mins[$i] != "")) {
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の戻り時刻の時と分の一方のみは登録できません。');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
	if (($ret_hours[$i] != "" && !preg_match("/^[0-9]*$/", $ret_hours[$i])) || ($ret_mins[$i] != "" && !preg_match("/^[0-9]*$/", $ret_mins[$i]))) {	
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の戻り時刻の時と分には数値を入力してください。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
	//休憩開始時刻の時分チェック 20100921
	if (($rest_start_hours[$i] == "" && $rest_start_mins[$i] != "") || ($rest_start_hours[$i] != "" && $rest_start_mins[$i] == "")) {
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の休憩開始時刻の時と分の一方のみは登録できません。');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
	if (($rest_start_hours[$i] != "" && !preg_match("/^[0-9]*$/", $rest_start_hours[$i])) || ($rest_start_mins[$i] != "" && !preg_match("/^[0-9]*$/", $rest_start_mins[$i]))) {	
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の休憩開始時刻の時と分には数値を入力してください。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
	//休憩終了時刻の時分チェック 20100921
	if (($rest_end_hours[$i] != "" && $rest_end_mins[$i] == "") || ($rest_end_hours[$i] == "" && $rest_end_mins[$i] != "")) {
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の休憩終了時刻の時と分の一方のみは登録できません。');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
	if (($rest_end_hours[$i] != "" && !preg_match("/^[0-9]*$/", $rest_end_hours[$i])) || ($rest_end_mins[$i] != "" && !preg_match("/^[0-9]*$/", $rest_end_mins[$i]))) {	
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の休憩終了時刻の時と分には数値を入力してください。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
	//退勤復帰時刻の時分チェック
	if (($o_start_hour1s[$i] == "" && $o_end_hour1s[$i] != "")) {
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の{$ret1_str}時刻１を入力してください。');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
	if (($o_start_hour1s[$i] == "" && $o_start_min1s[$i] != "") || ($o_start_hour1s[$i] != "" && $o_start_min1s[$i] == "")) {
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の{$ret1_str}時刻１の時と分の一方のみは登録できません。');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
	if (($o_start_hour1s[$i] != "" && !preg_match("/^[0-9]*$/", $o_start_hour1s[$i])) || ($o_start_min1s[$i] != "" && !preg_match("/^[0-9]*$/", $o_start_min1s[$i]))) {	
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の{$ret1_str}時刻１の時と分には数値を入力してください。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
	//復帰退勤時刻の時分チェック
	if (($o_end_hour1s[$i] != "" && $o_end_min1s[$i] == "") || ($o_end_hour1s[$i] == "" && $o_end_min1s[$i] != "")) {
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の{$ret2_str}時刻１の時と分の一方のみは登録できません。');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
	if (($o_end_hour1s[$i] != "" && !preg_match("/^[0-9]*$/", $o_end_hour1s[$i])) || ($o_end_min1s[$i] != "" && !preg_match("/^[0-9]*$/", $o_end_min1s[$i]))) {	
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の{$ret2_str}時刻１の時と分には数値を入力してください。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
	//退勤復帰時刻の時分チェック
	if (($o_start_hour2s[$i] == "" && $o_end_hour2s[$i] != "")) {
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の{$ret1_str}時刻２を入力してください。');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
	if (($o_start_hour2s[$i] == "" && $o_start_min2s[$i] != "") || ($o_start_hour2s[$i] != "" && $o_start_min2s[$i] == "")) {
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の{$ret1_str}時刻２の時と分の一方のみは登録できません。');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
	if (($o_start_hour2s[$i] != "" && !preg_match("/^[0-9]*$/", $o_start_hour2s[$i])) || ($o_start_min2s[$i] != "" && !preg_match("/^[0-9]*$/", $o_start_min2s[$i]))) {	
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の{$ret1_str}時刻２の時と分には数値を入力してください。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
	//復帰退勤時刻の時分チェック
	if (($o_end_hour2s[$i] != "" && $o_end_min2s[$i] == "") || ($o_end_hour2s[$i] == "" && $o_end_min2s[$i] != "")) {
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の{$ret2_str}時刻２の時と分の一方のみは登録できません。');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
	if (($o_end_hour2s[$i] != "" && !preg_match("/^[0-9]*$/", $o_end_hour2s[$i])) || ($o_end_min2s[$i] != "" && !preg_match("/^[0-9]*$/", $o_end_min2s[$i]))) {	
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の{$ret2_str}時刻２の時と分には数値を入力してください。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
	//退勤復帰時刻の時分チェック
	if (($o_start_hour3s[$i] == "" && $o_end_hour3s[$i] != "")) {
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の{$ret1_str}時刻３を入力してください。');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
	if (($o_start_hour3s[$i] == "" && $o_start_min3s[$i] != "") || ($o_start_hour3s[$i] != "" && $o_start_min3s[$i] == "")) {
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の{$ret1_str}時刻３の時と分の一方のみは登録できません。');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
	if (($o_start_hour3s[$i] != "" && !preg_match("/^[0-9]*$/", $o_start_hour3s[$i])) || ($o_start_min3s[$i] != "" && !preg_match("/^[0-9]*$/", $o_start_min3s[$i]))) {	
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の{$ret1_str}時刻３の時と分には数値を入力してください。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
	//復帰退勤時刻の時分チェック
	if (($o_end_hour3s[$i] != "" && $o_end_min3s[$i] == "") || ($o_end_hour3s[$i] == "" && $o_end_min3s[$i] != "")) {
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の{$ret2_str}時刻３の時と分の一方のみは登録できません。');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
	if (($o_end_hour3s[$i] != "" && !preg_match("/^[0-9]*$/", $o_end_hour3s[$i])) || ($o_end_min3s[$i] != "" && !preg_match("/^[0-9]*$/", $o_end_min3s[$i]))) {	
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の{$ret2_str}時刻３の時と分には数値を入力してください。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
	if (($o_start_hour3s[$i] != "" && $o_start_hour2s[$i] == "") || ($o_start_hour2s[$i] != "" && $o_start_hour1s[$i] == "")) {
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の{$ret1_str}時刻は１から順番に入力してください。');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
	//病棟外開始時刻の時分チェック
	if (($m_start_hours[$i] == "" && $m_start_mins[$i] != "") || ($m_start_hours[$i] != "" && $m_start_mins[$i] == "")) {
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の病棟外開始時刻の時と分の一方のみは登録できません。');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
	if (($m_start_hours[$i] != "" && !preg_match("/^[0-9]*$/", $m_start_hours[$i])) || ($m_start_mins[$i] != "" && !preg_match("/^[0-9]*$/", $m_start_mins[$i]))) {	
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の病棟外開始時刻の時と分には数値を入力してください。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
	//病棟外終了時刻の時分チェック
	if (($m_end_hours[$i] != "" && $m_end_mins[$i] == "") || ($m_end_hours[$i] == "" && $m_end_mins[$i] != "")) {
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の病棟外終了時刻の時と分の一方のみは登録できません。');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
	if (($m_end_hours[$i] != "" && !preg_match("/^[0-9]*$/", $m_end_hours[$i])) || ($m_end_mins[$i] != "" && !preg_match("/^[0-9]*$/", $m_end_mins[$i]))) {	
		echo("<script type=\"text/javascript\">alert('{$wk_emp_name}の病棟外終了時刻の時と分には数値を入力してください。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
}

$obj = new atdbk_workflow_common_class($con, $fname);
$timecard_common_class = new timecard_common_class($con, $fname, $emp_id, date("Ymd", strtotime("-1 day")), date("Ymd", strtotime("1 day")));
// タイムカード設定情報を取得
$sql = "select modify_flg from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	$modify_flg = pg_fetch_result($sel, 0, "modify_flg");
}
if ($modify_flg == "") {$modify_flg = "t";}

// トランザクションを開始
pg_query($con, "begin transaction");

// 承認者ワークフロー情報取得
$sql = "select * from atdbk_wkfw";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$ovtm_wkfw_div = pg_fetch_result($sel, 0, "ovtm_wkfw_div");
$wkfw_approve_num = pg_fetch_result($sel, 0, "ovtmcfm");

$wkfw_flg = ($ovtm_wkfw_div != "1") ? false : true;

// 処理日付の勤務実績レコード数を取得
$tmp_start_date = $date[0];
$tmp_cnt = count($date);
$tmp_end_date = $date[$tmp_cnt - 1];

$duty_yyyy = substr($yyyymm, 0, 4);
$duty_mm = intval(substr($yyyymm, 4, 2));

$arr_rec_count = array();
$arr_data_idx = array();
$arr_old_data = array();

$staff_cnt = count($emp_ids);

if ($staff_cnt > 0) {
	$sql = "select atdbkrslt.*, ".
		"ovtmapply.apply_status AS ovtm_apply_status, ".
		"ovtmapply.reason_id AS ovtm_reason_id, ".
		"ovtmapply.reason AS ovtm_reason, ".
		"rtnapply.apply_status  AS rtn_apply_status ".
		"from atdbkrslt ".
		"LEFT JOIN ovtmapply ON atdbkrslt.emp_id = ovtmapply.emp_id AND CAST(atdbkrslt.date AS varchar) = ovtmapply.target_date AND ovtmapply.delete_flg = false AND ovtmapply.re_apply_id IS NULL ".
		"LEFT JOIN rtnapply  ON atdbkrslt.emp_id = rtnapply.emp_id  AND CAST(atdbkrslt.date AS varchar) = rtnapply.target_date AND rtnapply.delete_flg = false AND rtnapply.re_apply_id IS NULL ";
	//$cond = "where exists (select b.emp_id from duty_shift_plan_staff b where b.group_id = '$group_id' and atdbkrslt.emp_id = b.emp_id and b.duty_yyyy = $duty_yyyy and b.duty_mm = $duty_mm) and ";
	//職員IDの条件
	$cond_staff = "";
	for ($i=0; $i<$staff_cnt; $i++) {
		if ($i > 0) {
			$cond_staff .= " or ";
		}
		$cond_staff .= " atdbkrslt.emp_id = '". $emp_ids[$i] . "' ";
	}
	$cond = "where ($cond_staff) and ";
	$cond .= " atdbkrslt.date = '$duty_date'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	
	$num = pg_numrows($sel);
	for ($i = 0; $i < $num; $i++) {
		$tmp_emp_id = pg_fetch_result($sel, $i, "emp_id");
		$arr_rec_count[$tmp_emp_id] = 1;
		$arr_data_idx[$tmp_emp_id] = $i;
	}
	$arr_old_data = pg_fetch_all($sel);
}
$tmp_date = $duty_date;
// 職員をループ
for ($i = 0; $i < count($emp_ids); $i++) {
	// 処理日付の勤務実績レコード数を取得
	$wk_emp_id = $emp_ids[$i];
	
	$rec_count = ($arr_rec_count[$wk_emp_id] == 1) ? 1 : 0;
	// 勤務実績レコードがなければ作成、あれば更新
	// ユーザの出勤グループを取得
	$user_tmcd_group_id = get_timecard_group_id($con, $wk_emp_id, $fname);
	
	//グループ
	$tmcd_group_id = $list_tmcd_group_id[$i];
	if ($tmcd_group_id == "--" || $tmcd_group_id == "") {
		$tmcd_group_id = $user_tmcd_group_id;
	}
	//勤務実績
	$tmp_pattern = $pattern[$i];
	if ($tmp_pattern == "--") {
		$tmp_pattern = "";
	}
	//事由
	$tmp_reason = $reason[$i];
	if ($tmp_reason == "--") {
		$tmp_reason = "";
	}
	//当直
	$tmp_night_duty = $night_duty[$i];
	if ($tmp_night_duty == "--") {
		$tmp_night_duty = "";
	}
	//手当
	$tmp_allow_id = ($allow_ids[$i] == "--") ? NULL : $allow_ids[$i];
	//回数
	$tmp_allow_count = ($tmp_allow_id != NULL) ? $allow_counts[$i] : "";
	//前日フラグ
	$varname = "prev_day_flag_".$i;
	$tmp_previous_day_flag = ($$varname == 1) ? 1 : 0;
	//出勤
	$tmp_start_time = ($start_hours[$i] != "") ? sprintf("%02d%02d", $start_hours[$i], $start_mins[$i]) : "";
	//翌日フラグ
	$varname = "next_day_flag_".$i;
	$tmp_next_day_flag = ($$varname == 1) ? 1 : 0;
	//退勤
	$tmp_end_time = ($end_hours[$i] != "") ? sprintf("%02d%02d", $end_hours[$i], $end_mins[$i]) : "";
	//外出
	$tmp_out_time = ($out_hours[$i] != "") ? sprintf("%02d%02d", $out_hours[$i], $out_mins[$i]) : "";
	//戻り
	$tmp_ret_time = ($ret_hours[$i] != "") ? sprintf("%02d%02d", $ret_hours[$i], $ret_mins[$i]) : "";
	//翌日フラグ
	$varname = "o_s_next_day_flag_".$i;
	$tmp_o_s_next_day_flag = ($$varname == 1) ? 1 : 0;
	//残業開始
	$tmp_over_start_time = ($over_start_hours[$i] != "") ? sprintf("%02d%02d", $over_start_hours[$i], $over_start_mins[$i]) : "";
	
	//翌日フラグ
	$varname = "o_e_next_day_flag_".$i;
	$tmp_o_e_next_day_flag = ($$varname == 1) ? 1 : 0;
	//残業終了
	$tmp_over_end_time = ($over_end_hours[$i] != "") ? sprintf("%02d%02d", $over_end_hours[$i], $over_end_mins[$i]) : "";

	//残業時刻２
	//翌日フラグ
	$varname = "o_s_next_day_flag2_".$i;
	$tmp_o_s_next_day_flag2 = ($$varname == 1) ? 1 : 0;
	//残業開始
	$tmp_over_start_time2 = ($over_start_hours2[$i] != "") ? sprintf("%02d%02d", $over_start_hours2[$i], $over_start_mins2[$i]) : "";
	
	//翌日フラグ
	$varname = "o_e_next_day_flag2_".$i;
	$tmp_o_e_next_day_flag2 = ($$varname == 1) ? 1 : 0;
	//残業終了
	$tmp_over_end_time2 = ($over_end_hours2[$i] != "") ? sprintf("%02d%02d", $over_end_hours2[$i], $over_end_mins2[$i]) : "";
	
	
	//退復
	$tmp_o_start_time1 = ($o_start_hour1s[$i] != "") ? sprintf("%02d%02d", $o_start_hour1s[$i], $o_start_min1s[$i]) : "";
	//復退
	$tmp_o_end_time1 = ($o_end_hour1s[$i] != "") ? sprintf("%02d%02d", $o_end_hour1s[$i], $o_end_min1s[$i]) : "";
	//退復
	$tmp_o_start_time2 = ($o_start_hour2s[$i] != "") ? sprintf("%02d%02d", $o_start_hour2s[$i], $o_start_min2s[$i]) : "";
	//復退
	$tmp_o_end_time2 = ($o_end_hour2s[$i] != "") ? sprintf("%02d%02d", $o_end_hour2s[$i], $o_end_min2s[$i]) : "";
	//退復
	$tmp_o_start_time3 = ($o_start_hour3s[$i] != "") ? sprintf("%02d%02d", $o_start_hour3s[$i], $o_start_min3s[$i]) : "";
	//復退
	$tmp_o_end_time3 = ($o_end_hour3s[$i] != "") ? sprintf("%02d%02d", $o_end_hour3s[$i], $o_end_min3s[$i]) : "";
	//病棟外開始
	$tmp_m_start_time = ($m_start_hours[$i] != "") ? sprintf("%02d%02d", $m_start_hours[$i], $m_start_mins[$i]) : "";
	//病棟外終了
	$tmp_m_end_time = ($m_end_hours[$i] != "") ? sprintf("%02d%02d", $m_end_hours[$i], $m_end_mins[$i]) : "";
	//法定内残業
	$tmp_legal_in_over_time = ($legal_in_over_hours[$i] != "") ? sprintf("%02d%02d", $legal_in_over_hours[$i], $legal_in_over_mins[$i]) : "";
	//早出残業
	$tmp_early_over_time = ($early_over_hours[$i] != "") ? sprintf("%02d%02d", $early_over_hours[$i], $early_over_mins[$i]) : "";
	//休憩開始 20100921
	$tmp_rest_start_time = ($rest_start_hours[$i] != "") ? sprintf("%02d%02d", $rest_start_hours[$i], $rest_start_mins[$i]) : "";
	//休憩終了 20100921
	$tmp_rest_end_time = ($rest_end_hours[$i] != "") ? sprintf("%02d%02d", $rest_end_hours[$i], $rest_end_mins[$i]) : "";
	
	//理由
	$tmp_reason_id = ($reason_ids[$i] == "other") ? "0" : $reason_ids[$i];
	if ($tmp_reason_id == "") {
		$tmp_reason_id = null;
	}
	
	//残業申請不要フラグ
	if ($wherefrom == "1") {
		//ユーザ画面の場合
		$varname = "over_no_apply_flag_".$i;
		$tmp_over_no_apply_flag = ($$varname == 1) ? 1 : 0;
	} else {
		//管理画面の場合
		$tmp_over_no_apply_flag = ($ovtm_status[$i] == "4") ? 1 : 0;
	}
	//入力項目の変更があったか確認
	//申請不要
	$wk_idx = $arr_data_idx[$wk_emp_id];
	$old_over_no_apply_flag = ($arr_old_data[$wk_idx]["ovtm_apply_status"] == "4") ? 1 : 0;
	$old_reason_id = ($arr_old_data[$wk_idx]["ovtm_reason"] != "") ? "0" : $arr_old_data[$wk_idx]["ovtm_reason_id"];
	//グループIDが未設定時はデフォルト設定を使用
	if ($arr_old_data[$wk_idx]["tmcd_group_id"] == "") {
		$arr_old_data[$wk_idx]["tmcd_group_id"] = $user_tmcd_group_id;
	}
	//手当がない場合、手当回数もなしとする
	if ($arr_old_data[$wk_idx]["allow_id"] == "") {
		$arr_old_data[$wk_idx]["allow_count"] = "";
	}
	if ($tmcd_group_id == $arr_old_data[$wk_idx]["tmcd_group_id"] &&
			$tmp_pattern == $arr_old_data[$wk_idx]["pattern"] &&
			$tmp_reason == $arr_old_data[$wk_idx]["reason"] &&
			$tmp_night_duty == $arr_old_data[$wk_idx]["night_duty"] &&
			$tmp_allow_id == $arr_old_data[$wk_idx]["allow_id"] &&
			$tmp_allow_count == $arr_old_data[$wk_idx]["allow_count"] &&
			$tmp_previous_day_flag == $arr_old_data[$wk_idx]["previous_day_flag"] &&
			$tmp_start_time == $arr_old_data[$wk_idx]["start_time"] &&
			$tmp_next_day_flag == $arr_old_data[$wk_idx]["next_day_flag"] &&
			$tmp_end_time == $arr_old_data[$wk_idx]["end_time"] &&
			$tmp_out_time == $arr_old_data[$wk_idx]["out_time"] &&
			$tmp_ret_time == $arr_old_data[$wk_idx]["ret_time"] &&
			$tmp_o_s_next_day_flag == $arr_old_data[$wk_idx]["over_start_next_day_flag"] &&
			$tmp_over_start_time == $arr_old_data[$wk_idx]["over_start_time"] &&
			$tmp_o_e_next_day_flag == $arr_old_data[$wk_idx]["over_end_next_day_flag"] &&
			$tmp_over_end_time == $arr_old_data[$wk_idx]["over_end_time"] &&
			$tmp_o_s_next_day_flag2 == $arr_old_data[$wk_idx]["over_start_next_day_flag2"] &&
			$tmp_over_start_time2 == $arr_old_data[$wk_idx]["over_start_time2"] &&
			$tmp_o_e_next_day_flag2 == $arr_old_data[$wk_idx]["over_end_next_day_flag2"] &&
			$tmp_over_end_time2 == $arr_old_data[$wk_idx]["over_end_time2"] &&
			$tmp_reason_id == $old_reason_id &&
			$tmp_o_start_time1 == $arr_old_data[$wk_idx]["o_start_time1"] &&
			$tmp_o_end_time1 == $arr_old_data[$wk_idx]["o_end_time1"] &&
			$tmp_o_start_time2 == $arr_old_data[$wk_idx]["o_start_time2"] &&
			$tmp_o_end_time2 == $arr_old_data[$wk_idx]["o_end_time2"] &&
			$tmp_o_start_time3 == $arr_old_data[$wk_idx]["o_start_time3"] &&
			$tmp_o_end_time3 == $arr_old_data[$wk_idx]["o_end_time3"] &&
			$tmp_m_start_time == $arr_old_data[$wk_idx]["meeting_start_time"] &&
			$tmp_m_end_time == $arr_old_data[$wk_idx]["meeting_end_time"] &&
			$tmp_legal_in_over_time == $arr_old_data[$wk_idx]["legal_in_over_time"] &&
			$tmp_early_over_time == $arr_old_data[$wk_idx]["early_over_time"] &&
			$tmp_rest_start_time == $arr_old_data[$wk_idx]["rest_start_time"] &&
			$tmp_rest_end_time == $arr_old_data[$wk_idx]["rest_end_time"] 
		) {
		$change_flg = false;
	} else {
		$change_flg = true;
	}
	//管理画面の申請状態
	$tmp_ovtm_status = $ovtm_status[$i];
	//状態の確認
	$ovtm_apply_status = $arr_old_data[$wk_idx]["ovtm_apply_status"];
	//管理画面、申請中は承認済みにする 20100514
		if ($ovtm_apply_status == "0" && //申請中で残業時刻ありの場合
			(($tmp_over_start_time != "" && $tmp_over_end_time != "") ||
				($tmp_over_start_time2 != "" && $tmp_over_end_time2 != "")) &&
				$tmp_ovtm_status == "") {
			$tmp_ovtm_status = "1";
		}
	//入力項目、申請不要フラグが変更されていない場合、管理画面の申請状態がない場合はスキップ
	if ($change_flg == false &&
			$tmp_over_no_apply_flag == $old_over_no_apply_flag &&
			$tmp_ovtm_status == "" //管理画面の申請状態
		) {
		continue;	
	}	
	//時刻が未入力、DBデータの時刻も未設定、申請状態だけ変更している場合はスキップ 20100514
	if ($tmp_start_time == "" && $tmp_end_time == "" &&
			$arr_old_data[$wk_idx]["start_time"] == "" &&
			$arr_old_data[$wk_idx]["end_time"] == "" &&
			$tmp_ovtm_status != "") {
		//休暇で、残業時刻が入力されている場合を除く
		if (!($tmp_pattern == "10" && (($tmp_over_start_time != "" && $tmp_over_end_time != "") ||
						($tmp_over_start_time2 != "" && $tmp_over_end_time2 != "")))) {
			continue;
		}
	}
	
	if ($tmp_over_no_apply_flag == 1) {
		$tmp_o_s_next_day_flag = 0;
		$tmp_over_start_time = "";
		$tmp_o_e_next_day_flag = 0;
		$tmp_over_end_time = "";
	}
	
	if ($rec_count == 0) {
		$sql = "insert into atdbkrslt (emp_id, date, pattern, reason, night_duty, allow_id, tmcd_group_id, allow_count, previous_day_flag, start_time, next_day_flag, end_time, out_time, ret_time, over_start_next_day_flag, over_start_time, over_end_next_day_flag, over_end_time, o_start_time1, o_end_time1, meeting_start_time, meeting_end_time, update_emp_id, update_time, legal_in_over_time, early_over_time, rest_start_time, rest_end_time, over_start_next_day_flag2, over_start_time2, over_end_next_day_flag2, over_end_time2, o_start_time2, o_end_time2, o_start_time3, o_end_time3) values (";
		$content = array($wk_emp_id, $tmp_date, $tmp_pattern, $tmp_reason, $tmp_night_duty, $tmp_allow_id, $tmcd_group_id, $tmp_allow_count, $tmp_previous_day_flag, $tmp_start_time, $tmp_next_day_flag, $tmp_end_time, $tmp_out_time, $tmp_ret_time, $tmp_o_s_next_day_flag, $tmp_over_start_time, $tmp_o_e_next_day_flag, $tmp_over_end_time, $tmp_o_start_time1, $tmp_o_end_time1, $tmp_m_start_time, $tmp_m_end_time, $login_emp_id, date("YmdHis"), $tmp_legal_in_over_time, $tmp_early_over_time, $tmp_rest_start_time, $tmp_rest_end_time, $tmp_o_s_next_day_flag2, $tmp_over_start_time2, $tmp_o_e_next_day_flag2, $tmp_over_end_time2, $tmp_o_start_time2, $tmp_o_end_time2, $tmp_o_start_time3, $tmp_o_end_time3);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	} else {
		$sql = "update atdbkrslt set";
		$set = array("pattern", "reason", "night_duty", "allow_id", "tmcd_group_id", "allow_count", "previous_day_flag", "start_time", "next_day_flag", "end_time", "out_time", "ret_time", "over_start_next_day_flag", "over_start_time", "over_end_next_day_flag", "over_end_time", "o_start_time1", "o_end_time1", "meeting_start_time", "meeting_end_time", "update_emp_id", "update_time", "legal_in_over_time", "early_over_time", "rest_start_time", "rest_end_time", "over_start_next_day_flag2", "over_start_time2", "over_end_next_day_flag2", "over_end_time2", "o_start_time2", "o_end_time2", "o_start_time3", "o_end_time3");
		$setvalue = array($tmp_pattern, $tmp_reason, $tmp_night_duty, $tmp_allow_id, $tmcd_group_id, $tmp_allow_count, $tmp_previous_day_flag, $tmp_start_time, $tmp_next_day_flag, $tmp_end_time, $tmp_out_time, $tmp_ret_time, $tmp_o_s_next_day_flag, $tmp_over_start_time, $tmp_o_e_next_day_flag, $tmp_over_end_time, $tmp_o_start_time1, $tmp_o_end_time1, $tmp_m_start_time, $tmp_m_end_time, $login_emp_id, date("YmdHis"), $tmp_legal_in_over_time, $tmp_early_over_time, $tmp_rest_start_time, $tmp_rest_end_time, $tmp_o_s_next_day_flag2, $tmp_over_start_time2, $tmp_o_e_next_day_flag2, $tmp_over_end_time2, $tmp_o_start_time2, $tmp_o_end_time2, $tmp_o_start_time3, $tmp_o_end_time3);
		$cond = "where emp_id = '$wk_emp_id' and date = '$tmp_date'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}
	// 承認者管理情報取得
	$arr_wkfwapv = $obj->get_wkfwapv_info($wk_emp_id, "1", $wkfw_flg);
	$approve_num = count($arr_wkfwapv);   // 承認者数
	
	
	for ($j=0; $j<$approve_num; $j++) {
		$l = $j + 1;
		$wkfwapv         = $arr_wkfwapv[$j];
		$emp_infos       = $wkfwapv["emp_infos"];
		
		$multi_apv_flg   = $wkfwapv["multi_apv_flg"];	//複数承認者
		$apv_order       = $wkfwapv["apv_order"];		//承認階層
		$apv_sub_order   = $wkfwapv["apv_sub_order"];	//承認順
		$next_notice_div = $wkfwapv["next_notice_div"]; //1:非同期、2同期
		
		$varname = "regist_emp_id$l";
		if (count($emp_infos)> 0) {
			$$varname = $emp_infos[0]["emp_id"];
		} else {
			$$varname = "";
		}
		$varname = "apv_order$l";
		$$varname = $apv_order;
		$varname = "apv_sub_order$l";
		$$varname = $apv_sub_order;
		$varname = "multi_apv_flg$l";
		$$varname = $multi_apv_flg;
		$varname = "next_notice_div$l";
		$$varname = $next_notice_div;
		
		$varname = "pst_approve_num$apv_order";
		$$varname = count($emp_infos);
		
		for($k=0; $k<count($emp_infos); $k++) {
			$varname = "pst_emp_id$apv_order";
			$varname .= "_".($k+1);
			$$varname = $emp_infos[$k]["emp_id"];
		}
		
	}
	// 申請確定情報取得
	if($wkfw_approve_num != "")
	{
		// 申請確定階層取得用配列
		$inputs = array();
		$inputs["wkfw_approve_num"] = $wkfw_approve_num;
		$inputs["approve_num"] = $approve_num;
		for ($k=1; $k<=$approve_num; $k++) {
			$varname = "regist_emp_id$k";
			$inputs["regist_emp_id$k"] = $$varname;
			$varname = "apv_order$k";
			$inputs["apv_order$k"] = $$varname;
		}
		$tmp_cfm = $obj->get_apply_confirm_hierarchy($inputs);
	}
	else
	{
		$tmp_cfm = null;
	}
	
	//残業申請
	$ovtm_apply_id = "";
	//残業申請不要フラグがONの場合
	if ($tmp_over_no_apply_flag == 1) {
		//残業申請不要に更新
		$ovtm_apply_id = $obj->check_ovtm_apply_id($wk_emp_id, $tmp_date);
		if ($ovtm_apply_id != "") {
			// ステータスを更新
			$sql = "update ovtmapply set";
			$set = array("apply_status", "delete_flg");
			$setvalue = array('4', 'f');
			$cond = "where apply_id = $ovtm_apply_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			//管理画面からの場合
			if ($wherefrom == "4") {
				//承認情報を論理削除する
				$obj->delete_ovtm_aprv($ovtm_apply_id);
			}
		}
		//データなしのため登録する
		else {
			// ステータスapply_statusを4:申請不要とする
			$apply_id = $obj->get_ovtm_apply_id();
			$wk_reason = "";
			$sql = "insert into ovtmapply (apply_id, emp_id, target_date, reason_id, reason, apply_time, apply_status, ovtmcfm, delete_flg, end_time, next_day_flag, start_time, previous_day_flag, over_start_time, over_end_time, over_start_next_day_flag, over_end_next_day_flag, over_start_time2, over_end_time2, over_start_next_day_flag2, over_end_next_day_flag2) values (";
			$content = array($apply_id, $wk_emp_id, $tmp_date, $tmp_reason_id, $wk_reason, date("YmdHis"), '4', $tmp_cfm, 'f', $tmp_end_time, $tmp_next_day_flag, $tmp_start_time, $tmp_previous_day_flag, $tmp_over_start_time, $tmp_over_end_time, $tmp_o_s_next_day_flag, $tmp_o_e_next_day_flag, $tmp_over_start_time2, $tmp_over_end_time2, $tmp_o_s_next_day_flag2, $tmp_o_e_next_day_flag2);
			$ins = insert_into_table($con, $sql, $content, $fname);
			if ($ins == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			// 承認登録
			for($k=1; $k<=$approve_num; $k++)
			{
				$varname = "regist_emp_id$k";
				$aprv_emp_id = ($$varname == "") ? null : $$varname;
				
				$varname = "apv_order$k";
				$apv_order = ($$varname == "") ? null : $$varname;
				
				$varname = "apv_sub_order$k";
				$apv_sub_order = ($$varname == "") ? null : $$varname;
				
				$varname = "multi_apv_flg$k";
				$multi_apv_flg = ($$varname == "") ? "f" : $$varname;
				
				$varname = "next_notice_div$k";
				$next_notice_div = ($$varname == "") ? null : $$varname;
				
				if($aprv_emp_id != "")
				{
					// 役職も登録する
					$sql = "select emp_st from empmst ";
					$cond = "where emp_id = '$aprv_emp_id'";
					$sel = select_from_table($con, $sql, $cond, $fname);
					if ($sel == 0) {
						pg_query($con, "rollback");
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
					$emp_st = pg_fetch_result($sel, 0, 0);
					
					// 承認者情報を登録
					$sql = "insert into ovtmaprv (apply_id, aprv_no, aprv_emp_id, aprv_status, aprv_sub_no, multi_apv_flg, next_notice_div, emp_st) values (";
					$content = array($apply_id, $apv_order, $aprv_emp_id, '0', $apv_sub_order, $multi_apv_flg, $next_notice_div, $emp_st);
					$ins = insert_into_table($con, $sql, $content, $fname);
					if ($ins == 0) {
						pg_query($con, "rollback");
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
				}
			}
		}
	} else {
		//登録済か確認
		$ovtm_apply_id = $obj->check_ovtm_apply_id($wk_emp_id, $tmp_date);
		//変更の有無確認
		$wk_idx = $arr_data_idx[$wk_emp_id];
		$old_over_start_time = $arr_old_data[$wk_idx]["over_start_time"];
		$old_over_end_time = $arr_old_data[$wk_idx]["over_end_time"];
		if ($tmp_o_s_next_day_flag != $arr_old_data[$wk_idx]["over_start_next_day_flag"] ||
				$tmp_over_start_time != $arr_old_data[$wk_idx]["over_start_time"] ||
				$tmp_o_e_next_day_flag != $arr_old_data[$wk_idx]["over_end_next_day_flag"] ||
				$tmp_over_end_time != $arr_old_data[$wk_idx]["over_end_time"] ||
				$tmp_o_s_next_day_flag2 != $arr_old_data[$wk_idx]["over_start_next_day_flag2"] ||
				$tmp_over_start_time2 != $arr_old_data[$wk_idx]["over_start_time2"] ||
				$tmp_o_e_next_day_flag2 != $arr_old_data[$wk_idx]["over_end_next_day_flag2"] ||
				$tmp_over_end_time2 != $arr_old_data[$wk_idx]["over_end_time2"] ||
				$tmp_early_over_time != $arr_old_data[$wk_idx]["early_over_time"] ||
				$ovtm_status[$i] == "1" || //承認済
				$ovtm_status[$i] == "2" || //否認済
				($ovtm_apply_status == "0" && //申請中で残業時刻ありの場合
					(($tmp_over_start_time != "" && $tmp_over_end_time != "") ||
						($tmp_over_start_time2 != "" && $tmp_over_end_time2 != "") ||
						($tmp_early_over_time != ""))) 
			) {
			
			//状態の確認
			$ovtm_apply_status = $arr_old_data[$wk_idx]["ovtm_apply_status"];
			
			//残業時間確認用の翌日フラグ  20100811
			if ($tmp_over_end_time != "") {
				$tmp_chk_next_day_flag = $tmp_o_e_next_day_flag;
			} else {
				//翌日退勤の場合設定
				if ($tmp_start_time > $tmp_end_time && 	$tmp_previous_day_flag == "0") {
					$tmp_chk_next_day_flag = "1";
				} else {
					$tmp_chk_next_day_flag = $tmp_next_day_flag;
				}
			}
			
			//管理画面からの場合、事後申請時もtrueとするためのフラグの設定をする
			$link_type_flg = true;
            //残業時刻入力時に確認 20130220
            if (($tmp_over_start_time != "" && $tmp_over_end_time != "") ||
                    ($tmp_over_start_time2 != "" && $tmp_over_end_time2 != "")) {
                //残業時間があるか確認
                $arr_result = get_atdbkrslt($con, $wk_emp_id, $tmp_date, $fname);//所定時間のため取得
                $ovtm_flg = $timecard_common_class->is_overtime_apply($tmp_date, $tmp_start_time, $tmp_previous_day_flag, $tmp_end_time, $tmp_chk_next_day_flag, $tmp_night_duty, $arr_result["office_start_time"], $arr_result["office_end_time"], $link_type_flg, $tmcd_group_id, $tmp_over_start_time, $tmp_over_end_time, $tmp_early_over_time, $arr_result["over_24hour_flag"], $tmp_over_start_time2, $tmp_over_end_time2, $tmp_o_s_next_day_flag2, $tmp_o_e_next_day_flag2);
            }
            else {
                $ovtm_flg = false;
            }
			// 申請中の場合、または、申請のデータがある場合
			if ($ovtm_apply_status == "0" ||
					$ovtm_apply_id != "") {
				//残業時間なしになる場合は、削除
				if ($tmp_over_start_time == "" && $tmp_over_end_time == "" && $old_over_start_time != "" && $old_over_end_time != "") {
					$ovtm_flg = false;
				}				

				//残業時間がある場合、更新
				if ($ovtm_flg == true) {
					$sql = "update ovtmapply set";
					$set = array("end_time", "next_day_flag", "start_time", "previous_day_flag", "over_start_time", "over_end_time", "over_start_next_day_flag", "over_end_next_day_flag", "reason_id", "over_start_time2", "over_end_time2", "over_start_next_day_flag2", "over_end_next_day_flag2");
					$setvalue = array($tmp_end_time, $tmp_next_day_flag, $tmp_start_time, $tmp_previous_day_flag, $tmp_over_start_time, $tmp_over_end_time, $tmp_o_s_next_day_flag, $tmp_o_e_next_day_flag, $tmp_reason_id, $tmp_over_start_time2, $tmp_over_end_time2, $tmp_o_s_next_day_flag2, $tmp_o_e_next_day_flag2);
					$cond = "where apply_id = $ovtm_apply_id";
					$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
					if ($upd == 0) {
						pg_query($con, "rollback");
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
					//否認の場合 20100513
					if ($ovtm_status[$i] == "2") {
						$apply_status = '2';
					} else {
						//承認済
						$apply_status = '1';
					}
					$obj->update_ovtm_status_all($ovtm_apply_id, $apply_status);	
				} else {
					$obj->delete_ovtm_apply_id($ovtm_apply_id);
				}
			}
			else {
				//残業時間がある場合、追加
				if ($ovtm_flg == true) {
					//申請、承認済で追加
					$obj->insert_ovtm_apply($wk_emp_id, $tmp_date, $tmp_reason_id, "", $tmp_end_time, $tmp_next_day_flag, $tmp_start_time, $tmp_previous_day_flag, $tmp_over_start_time, $tmp_over_end_time, $tmp_o_s_next_day_flag, $tmp_o_e_next_day_flag, "1", $tmp_over_start_time2, $tmp_over_end_time2, $tmp_o_s_next_day_flag2, $tmp_o_e_next_day_flag2);
				}
			}
			
		}
		//理由だけが変わった場合
		if ($tmp_reason_id != $old_reason_id ) {
			if ($ovtm_apply_id != "") {
				$sql = "update ovtmapply set";
				$set = array("reason_id");
				$setvalue = array($tmp_reason_id);
				$cond = "where apply_id = $ovtm_apply_id";
				$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
				if ($upd == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
			}
		}
		
	}
	
	//管理画面からの場合、申請状態更新
	if ($wherefrom == "4" &&
			($ovtm_status[$i] == "1" ||
				$ovtm_status[$i] == "2" ||
				$ovtm_status[$i] == "3" )
		) {
		
		//申請IDが上記の処理で取得されていない場合	
		if ($ovtm_apply_id == "") {
			$ovtm_apply_id = $obj->check_ovtm_apply_id($wk_emp_id, $tmp_date);
		}
		//申請IDがある場合に状態更新 20100514
		if ($ovtm_apply_id != "") {
			$obj->update_ovtm_status_all($ovtm_apply_id, $ovtm_status[$i]);
		}
		//否認の場合の時刻設定
		if ($ovtm_status[$i] == "2") {
			// 勤務実績情報を取得
			$arr_result = get_atdbkrslt($con, $wk_emp_id, $tmp_date, $fname);
			// 勤務実績の退勤時刻を終業時刻で上書き
			$office_start_time = $arr_result["office_start_time"];
			$office_end_time   = $arr_result["office_end_time"];
			$over_24hour_flag = $arr_result["over_24hour_flag"];
			$office_next_day_flag = 0;
			//開始時刻・終業時刻から終業時刻が翌日か判定を行う
			if ($office_start_time > $office_end_time || $over_24hour_flag == 1){
				$office_next_day_flag = 1;
			}
			$sql = "update atdbkrslt set";
			$set = array("end_time", "next_day_flag", "over_start_time", "over_end_time", "over_start_next_day_flag", "over_end_next_day_flag", "over_start_time2", "over_end_time2", "over_start_next_day_flag2", "over_end_next_day_flag2");
			$setvalue = array($office_end_time, $office_next_day_flag, "", "", 0, 0, "", "", 0, 0);
			$cond = "where emp_id = '$wk_emp_id' and date = '$tmp_date'";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}
	//	$wk_idx = $arr_data_idx[$tmp_date];
	// 退復
	//状態の確認
	$rtn_apply_status = $arr_old_data[$wk_idx]["rtn_apply_status"];
	if ($rtn_apply_status == "0" ||
			$tmp_o_start_time1 != $arr_old_data[$wk_idx]["o_start_time1"] ||
			$tmp_o_end_time1 != $arr_old_data[$wk_idx]["o_end_time1"] ||
			$tmp_o_start_time2 != $arr_old_data[$wk_idx]["o_start_time2"] ||
			$tmp_o_end_time2 != $arr_old_data[$wk_idx]["o_end_time2"] ||
			$tmp_o_start_time3 != $arr_old_data[$wk_idx]["o_start_time3"] ||
			$tmp_o_end_time3 != $arr_old_data[$wk_idx]["o_end_time3"]) {
		//登録済か確認
		$rtn_apply_id = $obj->check_rtn_apply_id($wk_emp_id, $tmp_date);
		
		//申請中の場合
		if ($rtn_apply_status == "0") {
			//時間がなしになる場合、削除
			if ($tmp_o_start_time1 == "" && $tmp_o_end_time1 == "" &&
					$tmp_o_start_time2 == "" && $tmp_o_end_time2 == "" &&
					$tmp_o_start_time3 == "" && $tmp_o_end_time3 == "") {
				$obj->delete_rtn_apply_id($rtn_apply_id);
			}
			else {
				//更新
				$sql = "update rtnapply set";
				$set = array("o_start_time1", "o_end_time1", "o_start_time2", "o_end_time2", "o_start_time3", "o_end_time3");
				$setvalue = array($tmp_o_start_time1, $tmp_o_end_time1, $tmp_o_start_time2, $tmp_o_end_time2, $tmp_o_start_time3, $tmp_o_end_time3);
				$cond = "where apply_id = $rtn_apply_id";
				$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
				if ($upd == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
				//管理画面からの場合は、承認済に更新する
				if ($wherefrom == "2" || $wherefrom == "4") {
					$obj->update_rtn_status_all($rtn_apply_id, "1");	
				}
			}
		}
		//申請中以外
		else {
			//時間がある場合、追加
			if ($tmp_o_start_time1 != "" || $tmp_o_end_time1 != "" ||
					$tmp_o_start_time2 != "" || $tmp_o_end_time2 != "" ||
					$tmp_o_start_time3 != "" || $tmp_o_end_time3 != "") {
				//申請、承認済で追加
				$obj->insert_rtn_apply($wk_emp_id, $tmp_date, "1", $tmp_o_start_time1, $tmp_o_end_time1, $tmp_o_start_time2, $tmp_o_end_time2, $tmp_o_start_time3, $tmp_o_end_time3);
			}
			//時間がない場合、削除
			else {
				$obj->delete_rtn_apply_id($rtn_apply_id);
			}
			
		}
	}
}
//修正申請	
//修正不可の場合
if ($modify_flg != "t") {
	// 職員をループ
	for ($i = 0; $i < count($emp_ids); $i++) {
		// 職員ID
		$wk_emp_id = $emp_ids[$i];
		
		//一括修正申請情報
		$arr_tmmdapplyall_status = $obj->get_tmmdapplyall_status($wk_emp_id, $tmp_date, $tmp_date);
		
		//一括修正のID別の申請状態
		$arr_id_status = array();
		foreach ($arr_tmmdapplyall_status as $tmp_date => $tmp_id_status) {
			$tmp_apply_id = $tmp_id_status["apply_id"];
			$arr_id_status["$tmp_apply_id"] = $tmp_id_status["apply_status"];
		}
		
		// 期間をループ
		//for ($i = 0; $i < count($date); $i++) {
			// 処理日付の勤務実績レコード数を取得
			//$tmp_date = $date[$i];
			$rec_count = ($arr_rec_count[$tmp_date] == 1) ? 1 : 0;
		$wk_idx = $arr_data_idx[$wk_emp_id];
			//出勤
			$tmp_start_time = ($start_hours[$i] != "") ? sprintf("%02d%02d", $start_hours[$i], $start_mins[$i]) : "";
			//退勤
			$tmp_end_time = ($end_hours[$i] != "") ? sprintf("%02d%02d", $end_hours[$i], $end_mins[$i]) : "";
			//一括
			if ($arr_tmmdapplyall_status[$tmp_date]["apply_id"] != "") {
				$tmp_apply_id = $arr_tmmdapplyall_status[$tmp_date]["apply_id"];
				//申請中の場合、承認済にする
				if ($arr_id_status["$tmp_apply_id"] == "0") {
					$arr_id_status["$tmp_apply_id"] = "1";
					//DB更新
					$obj->update_tmmd_status_all($tmp_apply_id, "1");	
				}
				//時刻を更新する(tmmdapplyall)a_end_time
				if ($arr_tmmdapplyall_status[$tmp_date]["a_start_time"] != $tmp_start_time ||
						$arr_tmmdapplyall_status[$tmp_date]["a_end_time"] != $tmp_end_time) {
					$sql = "update tmmdapplyall set";
					$set = array("a_start_time", "a_end_time");
					$setvalue = array($tmp_start_time, $tmp_end_time);
					$cond = "where apply_id = $tmp_apply_id and target_date = '$tmp_date'";
					$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
					if ($upd == 0) {
						pg_query($con, "rollback");
						pg_close($con);
						echo("<script type='text/javascript' src='./js/showpage.js'></script>");
						echo("<script language='javascript'>showErrorPage(window);</script>");
						exit;
					}
				}
			}
			//一括以外
			else {
				//状態の確認
				$tmmd_apply_status = $arr_old_data[$wk_idx]["tmmd_apply_status"];
				$tmp_apply_id = $arr_old_data[$wk_idx]["tmmd_apply_id"];
				//なし
				//申請中は承認済にする
				if ($tmmd_apply_status == "0") {
					//DB更新
					$obj->update_tmmd_status_all($tmp_apply_id, "1");	
				} 
				//クリア
				else if ($tmmd_apply_status >= "1") {
					if ($tmp_start_time == "" && 
							$tmp_end_time == "") {
						//承認情報を論理削除する
						$obj->delete_tmmd_apply_id($tmp_apply_id);
					}		
				}
			}
		//}
		
	}
}


// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

// 画面を再表示
$duty_yyyy = substr($yyyymm, 0, 4);
$duty_mm = intval(substr($yyyymm, 4, 2));
echo("<script type=\"text/javascript\">opener.location.href = 'duty_shift_results.php?session=$session&group_id=$group_id&duty_yyyy=$duty_yyyy&duty_mm=$duty_mm';</script>");
echo("<script type=\"text/javascript\">location.href = 'duty_shift_timecard_group.php?session=$session&yyyymm=$yyyymm&view=$view&duty_date=$duty_date&wherefrom=4&group_id=$group_id';</script>");
// 自画面を閉じる
//echo("<script type=\"text/javascript\">self.close();</script>");

?>
