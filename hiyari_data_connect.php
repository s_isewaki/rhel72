<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("hiyari_common.ini");
require_once("hiyari_report_class.php");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$summary = check_authority($session, 48, $fname);
if ($summary == "0") {
	showLoginPage();
	exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$rep_obj = new hiyari_report_class($con, $fname);

if($mode == "update")
{
	$rep_obj->set_inci_data_connect($patient_profile_flg, $hospital_ymd_flg, $patient_search_url, $byoumei_search_flg, $emr_flg, $emr_path);
}


$arr_inci_data_connect = $rep_obj->get_inci_data_connect();

//==============================
//HTML出力
//==============================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$INCIDENT_TITLE?> | データ連携設定</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">

function update()
{
	document.mainform.action="hiyari_data_connect.php?session=<?=$session?>&mode=update";
	document.mainform.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
<!--
.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}
// -->
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<form name="mainform" action="#" method="post">


<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー START -->
<?
show_hiyari_header($session,$fname,true);
?>
<!-- ヘッダー END -->
<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table border="0" cellspacing="0" cellpadding="0" width="800">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td bgcolor="#F5FFE5">

<!-- ここから -->


<table border="0" cellspacing="1" cellpadding="0" class="list">
<tr>
<td width="200" bgcolor="#DFFFDC" align="center" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><B>患者プロフィール連携</B></font>
</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td width="250" bgcolor="#F5FFE5" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="patient_profile_flg" value="f" <?if($arr_inci_data_connect["patient_profile_flg"] == "f") echo(" checked");?>>データ連携しない
</font>
</td>
</tr>
<tr>
<td width="100%" bgcolor="#F5FFE5" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="patient_profile_flg" value="t" <?if($arr_inci_data_connect["patient_profile_flg"] != "f") echo(" checked");?>>患者マスタ検索
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;患者検索URL<input type="text" name="patient_search_url" size="120" value="<? echo($arr_inci_data_connect["patient_search_url"]); ?>" style="ime-mode:inactive;">
</font>
</td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table border="0" cellspacing="1" cellpadding="0" class="list">
<tr>
<td width="200" bgcolor="#DFFFDC" align="center" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><B>入院年月日連携</B></font>
</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td width="250" bgcolor="#F5FFE5" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="hospital_ymd_flg" value="f" <?if($arr_inci_data_connect["hospital_ymd_flg"] == "f") echo(" checked");?>>データ連携しない
</font>
</td>
</tr>
<tr>
<td width="250" bgcolor="#F5FFE5" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="hospital_ymd_flg" value="t" <?if($arr_inci_data_connect["hospital_ymd_flg"] != "f") echo(" checked");?>>患者マスタ検索
</font>
</td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table border="0" cellspacing="1" cellpadding="0" class="list">
<tr>
<td width="200" bgcolor="#DFFFDC" align="center" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><B>病名マスタ連携</B></font>
</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td width="250" bgcolor="#F5FFE5" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="byoumei_search_flg" value="f" <?if($arr_inci_data_connect["byoumei_search_flg"] == "f") echo(" checked");?>>検索しない
</font>
</td>
</tr>
<tr>
<td width="250" bgcolor="#F5FFE5" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="byoumei_search_flg" value="t" <?if($arr_inci_data_connect["byoumei_search_flg"] != "f") echo(" checked");?>>検索する
</font>
</td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table border="0" cellspacing="1" cellpadding="0" class="list">
<tr>
<td width="200" bgcolor="#DFFFDC" align="center" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><B>電子カルテ連携</B></font>
</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td width="250" bgcolor="#F5FFE5" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="emr_flg" value="f" <?if($arr_inci_data_connect["emr_flg"] != "t") echo(" checked");?>>連携しない
</font>
</td>
</tr>
<tr>
<td width="100%" bgcolor="#F5FFE5" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="emr_flg" value="t" <?if($arr_inci_data_connect["emr_flg"] == "t") echo(" checked");?>>連携する
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;電子カルテパス<input type="text" name="emr_path" size="120" value="<? echo($arr_inci_data_connect["emr_path"]); ?>" style="ime-mode:inactive;">
</font>
</td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td align="right">
<input type="button" value="更新" onclick="update();">
</td>
</tr>
</table>

<!-- ここまで -->

</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<!-- フッター START -->
<?
show_hiyari_footer($session,$fname,true);
?>
<!-- フッター END -->

</tr>
</table>
</form>
</body>
</html>


<?
//==============================
//DBコネクション終了
//==============================
pg_close($con);
?>