<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
//ファイルの読み込み
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("about_validation.php");
require("./conf/sql.inf");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//権限チェック
$dept = check_authority($session,28,$fname);
if($dept == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//ＤＢのコネクション作成
$con = connect2db($fname);

//正規表現チェック
if($entity_name ==""){
	echo("<script language=\"javascript\">alert(\"事業所名を入力してください\");</script>\n");
	echo("<script language=\"javascript\">location.href=\"./entity_menu.php?session=$session\";</script>\n");
	exit;
}


//ＤＢへ役職名を登録する

$cond = "";
$sel = select_from_table($con,$SQL37,$cond,$fname);	//事業所ＩＤの最大値を取得
if($sel == 0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$max = pg_result($sel,0,"max");
if($max == ""){
	$next_id = 1;
}else{
	$next_id = $max + 1;
}

$content = array("$next_id","$entity_name","f");

$in = insert_into_table($con,$SQL38,$content,$fname);	//事業所情報を挿入
pg_close($con);
if($in == 0){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}else{
	echo("<script language=\"javascript\">location.href=\"./entity_menu.php?session=$session\";</script>\n");
	exit;
}

?>