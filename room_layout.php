<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 病床管理 | 表示設定 | 病棟レイアウト</title>
<?
require("about_authority.php");
require("about_session.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkuath = check_authority($session, 21, $fname);
if ($checkuath == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

$default_url = ($section_admin_auth == "1") ? "entity_menu.php" : "building_list.php";

// レイアウトデータを配列で定義
$layout_detail = array(
	  2 => layout_detail(2),             // 入り口（上） 1〜2床
	  4 => layout_detail(4),             // 入り口（上） 3〜4床
	  6 => layout_detail(6),             // 入り口（上） 5〜6床
	  8 => layout_detail(8),             // 入り口（上） 7〜8床
	  0 => layout_detail_vertical(5),    // 入り口（上） 2〜5床、縦置き
	102 => layout_detail(2),             // 入り口（下） 1〜2床
	104 => layout_detail(4),             // 入り口（下） 3〜4床
	106 => layout_detail(6),             // 入り口（下） 5〜6床
	108 => layout_detail(8),             // 入り口（下） 7〜8床
	100 => layout_detail_vertical(5),    // 入り口（下） 2〜5床、縦置き
	202 => layout_detail_side(2),        // 入り口（右） 1〜2床
	204 => layout_detail_side(4),        // 入り口（右） 3〜4床
	206 => layout_detail_side(6),        // 入り口（右） 5〜6床
	200 => layout_detail_horizontal(5),  // 入り口（右） 2〜5床、横置き
	302 => layout_detail_side(2),        // 入り口（右） 1〜2床
	304 => layout_detail_side(4),        // 入り口（右） 3〜4床
	306 => layout_detail_side(6),        // 入り口（右） 5〜6床
	300 => layout_detail_horizontal(5)   // 入り口（左） 2〜5床、横置き
);

// ベッド表示用数値の定義
$bed_margin = 5;
$bed_height = 80;
$bed_width  = 55;

// データベースに接続
$con = connect2db($fname);

// 初期表示時
if ($back != "t") {

	// レイアウト情報を取得
	$sql = "select * from roomlayout";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$room_count1 = pg_fetch_result($sel, 0, "room_count1");
		for ($i = 1; $i <= 24; $i++) {
			$var_name = "pt_info$i";
			$$var_name = pg_fetch_result($sel, 0, $var_name);
		}
		$warn_time = pg_fetch_result($sel, 0, "warn_time");
		$refresh_sec = pg_fetch_result($sel, 0, "refresh_sec");
	} else {
		$room_count1 = "8";
		for ($i = 1; $i <= 24; $i++) {
			$var_name = "pt_info$i";
			$$var_name = ($i == 1 || $i == 2 || $i == 4) ? "t" : "f";
		}
		$warn_time = "24";
		$refresh_sec = 300;
	}

	// ベッド表示位置情報を取得
	$sql = "select * from bedlayout";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$bed_layouts[$row["layout_key"]][$row["position"]] = $row["bed_no"];
	}

// 入力エラー時はデフォルトレイアウトを設定
} else {
	foreach ($layout_detail as $key => $layout) {
		set_default_layout($bed_layouts, $key, $layout["count"]);
	}
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/yui/3.10.3/build/yui/yui-min.js"></script>
<script type="text/javascript">
function initPage() {
	YUI().use('dd-drop', 'dd-proxy', function(Y){
<?
	foreach ($layout_detail as $key => $layout) {
		for ($i = 1; $i <= $layout["count"]; $i++) {
?>
		new Y.DD.Drop({
			node: '<? echo("#bed_{$key}_{$i}"); ?>',
			groups: ['<? echo("bed_{$key}"); ?>']
		});

		var dragStyle = document.getElementById('<? echo("bed_{$key}_{$i}"); ?>').style;
		var drag = new Y.DD.Drag({
			node: '<? echo("#bed_{$key}_{$i}"); ?>',
			groups: ['<? echo("bed_{$key}"); ?>'],
			dragMode: 'point',
			data: {left: dragStyle.left, top: dragStyle.top}
		}).plug(Y.Plugin.DDProxy, {
			moveOnEnd: false
		});

		// 他のマスに乗っているとき
		drag.on('drag:over', function(e){
			e.drop.get('node').replaceClass('bed', 'bed_hover');
		});

		// 他のマスから出たとき
		drag.on('drag:exit', function(e){
			e.drop.get('node').replaceClass('bed_hover', 'bed');
		});

		// 他のマスにドロップされたとき！！
		drag.on('drag:drophit', function(e){
			var dragStyle = document.getElementById(e.drag.get('node').get('id')).style;
			var dropStyle = document.getElementById(e.drop.get('node').get('id')).style;
			dragStyle.left = dropStyle.left;
			dragStyle.top = dropStyle.top;
			dropStyle.left = e.drag.get('data').left;
			dropStyle.top = e.drag.get('data').top;
			e.drag.set('data', {left: dragStyle.left, top: dragStyle.top});
			Y.DD.DDM.getDrag(e.drop.get('node')).set('data', {left: dropStyle.left, top: dropStyle.top});
			e.drop.get('node').replaceClass('bed_hover', 'bed');
		});
<?
		}
	}
?>
	});
}

function submitForm() {
<?
	foreach ($layout_detail as $key => $layout) {
		for ($i = 1; $i <= $layout["count"]; $i++) {
			if ($layout["vertical"]) {
				echo("\tsetPositionVertical('bed_{$key}_{$i}');\n");
			} else if ($layout["side"]) {
				echo("\tsetPositionSide('bed_{$key}_{$i}');\n");
			} else if ($layout["horizontal"]) {
				echo("\tsetPositionHorizontal('bed_{$key}_{$i}');\n");
			} else {
				echo("\tsetPosition('bed_{$key}_{$i}');\n");
			}
		}
	}
?>
	document.mainform.submit();
}

function setPosition(bedId) {
	var bed = document.getElementById(bedId);
	var top = bed.style.top;
	var left = bed.style.left;

<?
	for ($i = 1; $i <= 4; $i++) {
		$pos = ($bed_margin * $i) + ($bed_height * ($i - 1));
		echo("\tvar row{$i}top = '{$pos}px';\n");
	}

	for ($i = 1; $i <= 2; $i++) {
		$pos = ($bed_margin * $i) + ($bed_width * ($i - 1));
		echo("\tvar col{$i}left = '{$pos}px';\n");
	}
?>

	var position;
	if (top == row1top && left == col1left) {
		position = 1;
	} else if (top == row1top && left == col2left) {
		position = 2;
	} else if (top == row2top && left == col1left) {
		position = 3;
	} else if (top == row2top && left == col2left) {
		position = 4;
	} else if (top == row3top && left == col1left) {
		position = 5;
	} else if (top == row3top && left == col2left) {
		position = 6;
	} else if (top == row4top && left == col1left) {
		position = 7;
	} else if (top == row4top && left == col2left) {
		position = 8;
	}

	document.mainform.elements[bedId].value = position;
}

function setPositionVertical(bedId) {
	var bed = document.getElementById(bedId);
	var top = bed.style.top;

<?
	for ($i = 1; $i <= 5; $i++) {
		$pos = ($bed_margin * $i) + ($bed_height * ($i - 1));
		echo("\tvar row{$i}top = '{$pos}px';\n");
	}
?>

	var position;
	switch (top) {
	case row1top:
		position = 1;
		break;
	case row2top:
		position = 2;
		break;
	case row3top:
		position = 3;
		break;
	case row4top:
		position = 4;
		break;
	case row5top:
		position = 5;
		break;
	}

	document.mainform.elements[bedId].value = position;
}

function setPositionSide(bedId) {
	var bed = document.getElementById(bedId);
	var top = bed.style.top;
	var left = bed.style.left;

<?
	for ($i = 1; $i <= 2; $i++) {
		$pos = ($bed_margin * $i) + ($bed_height * ($i - 1));
		echo("\tvar row{$i}top = '{$pos}px';\n");
	}

	for ($i = 1; $i <= 3; $i++) {
		$pos = ($bed_margin * $i) + ($bed_width * ($i - 1));
		echo("\tvar col{$i}left = '{$pos}px';\n");
	}
?>

	var position;
	if (top == row1top && left == col1left) {
		position = 1;
	} else if (top == row2top && left == col1left) {
		position = 2;
	} else if (top == row1top && left == col2left) {
		position = 3;
	} else if (top == row2top && left == col2left) {
		position = 4;
	} else if (top == row1top && left == col3left) {
		position = 5;
	} else if (top == row2top && left == col3left) {
		position = 6;
	}

	document.mainform.elements[bedId].value = position;
}

function setPositionHorizontal(bedId) {
	var bed = document.getElementById(bedId);
	var left = bed.style.left;

<?
	for ($i = 1; $i <= 5; $i++) {
		$pos = ($bed_margin * $i) + ($bed_width * ($i - 1));
		echo("\tvar col{$i}left = '{$pos}px';\n");
	}
?>

	var position;
	switch (left) {
	case col1left:
		position = 1;
		break;
	case col2left:
		position = 2;
		break;
	case col3left:
		position = 3;
		break;
	case col4left:
		position = 4;
		break;
	case col5left:
		position = 5;
		break;
	}

	document.mainform.elements[bedId].value = position;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
.bed {border:#5279a5 solid 1px;}
.bed_hover {border:purple solid 2px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a> &gt; <a href="<? echo($default_url); ?>?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; <a href="bed_display_setting.php?session=<? echo($session); ?>"><b>表示設定</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="bed_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">
<table width="118" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279A5">
<td height="22" class="spacing" colspan="2"><b><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">管理項目</font></b></td>
</tr>
<? if ($section_admin_auth == "1") { ?>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="entity_menu.php?session=<? echo($session); ?>">診療科</a></font></td>
</tr>
<? } ?>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="building_list.php?session=<? echo($session); ?>">病棟</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_display_setting.php?session=<? echo($session); ?>">表示設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_check_setting.php?session=<? echo($session); ?>">管理項目設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_bulk_inpatient_register.php?session=<? echo($session); ?>">一括登録</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_master_menu.php?session=<? echo($session); ?>">マスタ管理</a></font></td>
</tr>
</table>
</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="90" align="center" bgcolor="#bdd1e7"><a href="bed_display_setting.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="room_layout.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>病棟レイアウト</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="room_map.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床マップ</font></a></td>
<td width="5">&nbsp;</td>
<td width="140" align="center" bgcolor="#bdd1e7"><a href="bed_display_calendar.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入退院カレンダー</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="4" alt=""><br>
<form name="mainform" action="room_layout_update_exe.php" method="post">
<table width="650" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="180" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">1行に表示する病室数</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="room_count1" value="<? echo($room_count1); ?>" size="3" maxlength="2" style="ime-mode:disabled">部屋</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示する患者情報</font></td>
<td>
<table width="98%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="pt_info1" value="t"<? if ($pt_info1 == "t") {echo(" checked");} ?> disabled>患者氏名</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="pt_info13" value="t"<? if ($pt_info13 == "t") {echo(" checked");} ?>>患者ID</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="pt_info2" value="t"<? if ($pt_info2 == "t") {echo(" checked");} ?>>保険</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="pt_info3" value="t"<? if ($pt_info3 == "t") {echo(" checked");} ?>>性別</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="pt_info4" value="t"<? if ($pt_info4 == "t") {echo(" checked");} ?>>年齢</font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="pt_info5" value="t"<? if ($pt_info5 == "t") {echo(" checked");} ?>>診療科</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="pt_info6" value="t"<? if ($pt_info6 == "t") {echo(" checked");} ?>>主治医</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="pt_info7" value="t"<? if ($pt_info7 == "t") {echo(" checked");} ?>>移動</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="pt_info8" value="t"<? if ($pt_info8 == "t") {echo(" checked");} ?>>観察</font><td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="pt_info9" value="t"<? if ($pt_info9 == "t") {echo(" checked");} ?>>面会</font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="pt_info10" value="t"<? if ($pt_info10 == "t") {echo(" checked");} ?>>外泊中</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="pt_info11" value="t"<? if ($pt_info11 == "t") {echo(" checked");} ?>>外出中</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="pt_info14" value="t"<? if ($pt_info14 == "t") {echo(" checked");} ?>>入院日</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="pt_info15" value="t"<? if ($pt_info15 == "t") {echo(" checked");} ?>>在院日数</font></td>
</tr>
<tr>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="pt_info22" value="t"<? if ($pt_info22 == "t") {echo(" checked");} ?>>回復期リハビリ算定期限分類</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="pt_info23" value="t"<? if ($pt_info23 == "t") {echo(" checked");} ?>>回復期リハビリ算定期限日</font></td>
</tr>
<tr>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="pt_info20" value="t"<? if ($pt_info20 == "t") {echo(" checked");} ?>>リハビリ算定期限分類</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="pt_info21" value="t"<? if ($pt_info21 == "t") {echo(" checked");} ?>>リハビリ算定期限日</font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="pt_info12" value="t"<? if ($pt_info12 == "t") {echo(" checked");} ?>>退院予定日</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="pt_info16" value="t"<? if ($pt_info16 == "t") {echo(" checked");} ?>>医療・ADL</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="pt_info19" value="t"<? if ($pt_info19 == "t") {echo(" checked");} ?>>看護度分類</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="pt_info24" value="t"<? if ($pt_info24 == "t") {echo(" checked");} ?>>差額室料免除（減免）</font></td>
</tr>
<tr height="22">
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="pt_info18" value="t"<? if ($pt_info18 == "t") {echo(" checked");} ?>>個人情報に関する要望</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="pt_info17" value="t"<? if ($pt_info17 == "t") {echo(" checked");} ?>>特記事項</font></td>
</tr>
</table>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">待機患者警告時間</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="warn_time" value="<? echo($warn_time); ?>" size="3" maxlength="3" style="ime-mode:disabled">時間</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">画面更新間隔</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="refresh_sec" value="<? echo($refresh_sec); ?>" size="5" maxlength="4" style="ime-mode:disabled">秒</font></td>
</tr>
</table>
<table width="650" border="0" cellspacing="0" cellpadding="2">
<tr height="40" valign="bottom">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>ベッドの表示位置　入り口（上）</b></font></td>
</tr>
</table>
<table width="650" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">2床</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">3〜4床</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">5〜6床</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">7〜8床</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">縦置き<br>2〜5床</font></td>
</tr>
<tr valign="top" height="436">
<td align="center">
<? show_bed_layout($bed_layouts, 2, $bed_margin, $bed_height, $bed_width); ?>
</td>
<td align="center">
<? show_bed_layout($bed_layouts, 4, $bed_margin, $bed_height, $bed_width); ?>
</td>
<td align="center">
<? show_bed_layout($bed_layouts, 6, $bed_margin, $bed_height, $bed_width); ?>
</td>
<td align="center">
<? show_bed_layout($bed_layouts, 8, $bed_margin, $bed_height, $bed_width); ?>
</td>
<td align="center">
<? show_bed_layout_vertical($bed_layouts, 0, $bed_margin, $bed_height, $bed_width); ?>
</td>
</tr>
</table>
<table width="650" border="0" cellspacing="0" cellpadding="2">
<tr height="40" valign="bottom">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>ベッドの表示位置　入り口（下）</b></font></td>
</tr>
</table>
<table width="650" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">2床</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">3〜4床</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">5〜6床</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">7〜8床</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">縦置き<br>2〜5床</font></td>
</tr>
<tr valign="top" height="436">
<td align="center">
<? show_bed_layout($bed_layouts, 102, $bed_margin, $bed_height, $bed_width); ?>
</td>
<td align="center">
<? show_bed_layout($bed_layouts, 104, $bed_margin, $bed_height, $bed_width); ?>
</td>
<td align="center">
<? show_bed_layout($bed_layouts, 106, $bed_margin, $bed_height, $bed_width); ?>
</td>
<td align="center">
<? show_bed_layout($bed_layouts, 108, $bed_margin, $bed_height, $bed_width); ?>
</td>
<td align="center">
<? show_bed_layout_vertical($bed_layouts, 100, $bed_margin, $bed_height, $bed_width); ?>
</td>
</tr>
</table>
<table width="650" border="0" cellspacing="0" cellpadding="2">
<tr height="40" valign="bottom">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>ベッドの表示位置　入り口（右）</b></font></td>
</tr>
</table>
<table width="650" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">2床</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">3〜4床</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">5〜6床</font></td>
</tr>
<tr valign="top" height="181">
<td align="center">
<? show_bed_layout_side($bed_layouts, 202, $bed_margin, $bed_height, $bed_width); ?>
</td>
<td align="center">
<? show_bed_layout_side($bed_layouts, 204, $bed_margin, $bed_height, $bed_width); ?>
</td>
<td align="center">
<? show_bed_layout_side($bed_layouts, 206, $bed_margin, $bed_height, $bed_width); ?>
</td>
</tr>
<tr height="22" bgcolor="#f6f9ff">
<td colspan="3" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">横置き2〜5床</font></td>
</tr>
<tr valign="top" height="96">
<td colspan="3" align="center">
<? show_bed_layout_horizontal($bed_layouts, 200, $bed_margin, $bed_height, $bed_width); ?>
</td>
</tr>
</table>
<table width="650" border="0" cellspacing="0" cellpadding="2">
<tr height="40" valign="bottom">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>ベッドの表示位置　入り口（左）</b></font></td>
</tr>
</table>
<table width="650" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">2床</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">3〜4床</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">5〜6床</font></td>
</tr>
<tr valign="top" height="181">
<td align="center">
<? show_bed_layout_side($bed_layouts, 302, $bed_margin, $bed_height, $bed_width); ?>
</td>
<td align="center">
<? show_bed_layout_side($bed_layouts, 304, $bed_margin, $bed_height, $bed_width); ?>
</td>
<td align="center">
<? show_bed_layout_side($bed_layouts, 306, $bed_margin, $bed_height, $bed_width); ?>
</td>
</tr>
<tr height="22" bgcolor="#f6f9ff">
<td colspan="3" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">横置き2〜5床</font></td>
</tr>
<tr valign="top" height="96">
<td colspan="3" align="center">
<? show_bed_layout_horizontal($bed_layouts, 300, $bed_margin, $bed_height, $bed_width); ?>
</td>
</tr>
</table>
<table width="650" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="button" value="更新" onclick="submitForm();"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<?
	foreach ($layout_detail as $key => $layout) {
		for ($i = 1; $i <= $layout["count"]; $i++) {
			echo("<input type=\"hidden\" name=\"bed_{$key}_{$i}\" value=\"\">\n");
		}
	}
?>
</form>
</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function set_default_layout(&$bed_layouts, $key, $bed_count) {
	for ($i = 1; $i <= $bed_count; $i++) {
		$bed_layouts[$key][$_POST["bed_{$key}_{$i}"]] = $i;
	}
	ksort($bed_layouts[$key]);
}

function show_bed_layout($bed_layouts, $key, $bed_margin, $bed_height, $bed_width) {
	$div_width = ($bed_margin * 3) + ($bed_width * 2);
	echo("<div style=\"position:relative;width:{$div_width}px\">\n");
	foreach ($bed_layouts[$key] as $pos => $bed_no) {
		$row = ceil($pos / 2);
		$top = $bed_margin + (($row - 1) * ($bed_height + $bed_margin));
		$col = ($pos % 2 == 1) ? 1 : 2;
		$left = $bed_margin + (($col - 1) * ($bed_width + $bed_margin));
		echo("<div id=\"bed_{$key}_{$bed_no}\" class=\"bed\" style=\"width:{$bed_width}px;height:{$bed_height}px;text-align:center;cursor:move;position:absolute;top:{$top}px;left:{$left}px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><span style=\"margin-top:2px;\">No.{$bed_no}</span></font></div>\n");
	}
	echo("</div>\n");
}

function show_bed_layout_vertical($bed_layouts, $key, $bed_margin, $bed_height, $bed_width) {
	$div_width = ($bed_margin * 2) + ($bed_width);
	echo("<div style=\"position:relative;width:{$div_width}px\">\n");
	foreach ($bed_layouts[$key] as $pos => $bed_no) {
		$top = $bed_margin + (($pos - 1) * ($bed_height + $bed_margin));
		echo("<div id=\"bed_{$key}_{$bed_no}\" class=\"bed\" style=\"width:{$bed_width}px;height:{$bed_height}px;text-align:center;cursor:move;position:absolute;top:{$top}px;left:{$bed_margin}px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><span style=\"margin-top:2px;\">No.{$bed_no}</span></font></div>\n");
	}
	echo("</div>\n");
}

function show_bed_layout_side($bed_layouts, $key, $bed_margin, $bed_height, $bed_width) {
	$col_count =  ceil(count($bed_layouts[$key]) / 2);
	$div_width = ($bed_margin * ($col_count + 1)) + ($bed_width * $col_count);
	echo("<div style=\"position:relative;width:{$div_width}px\">\n");
	foreach ($bed_layouts[$key] as $pos => $bed_no) {
		$row = ($pos % 2 == 1) ? 1 : 2;
		$top = $bed_margin + (($row - 1) * ($bed_height + $bed_margin));
		$col = ceil($pos / 2);
		$left = $bed_margin + (($col - 1) * ($bed_width + $bed_margin));
		echo("<div id=\"bed_{$key}_{$bed_no}\" class=\"bed\" style=\"width:{$bed_width}px;height:{$bed_height}px;text-align:center;cursor:move;position:absolute;top:{$top}px;left:{$left}px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><span style=\"margin-top:2px;\">No.{$bed_no}</span></font></div>\n");
	}
	echo("</div>\n");
}

function show_bed_layout_horizontal($bed_layouts, $key, $bed_margin, $bed_height, $bed_width) {
	$div_width = ($bed_margin * 6) + ($bed_width * 5);
	echo("<div style=\"position:relative;width:{$div_width}px\">\n");
	foreach ($bed_layouts[$key] as $pos => $bed_no) {
		$left = $bed_margin + (($pos - 1) * ($bed_width + $bed_margin));
		echo("<div id=\"bed_{$key}_{$bed_no}\" class=\"bed\" style=\"width:{$bed_width}px;height:{$bed_height}px;text-align:center;cursor:move;position:absolute;top:{$bed_margin}px;left:{$left}px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><span style=\"margin-top:2px;\">No.{$bed_no}</span></font></div>\n");
	}
	echo("</div>\n");
}

function layout_detail($count, $vertical = false, $side = false, $horizontal = false) {
	return array(
		"count"      => $count,
		"vertical"   => $vertical,
		"side"       => $side,
		"horizontal" => $horizontal
	);
}

function layout_detail_vertical($count) {
	return layout_detail($count, true);
}

function layout_detail_side($count) {
	return layout_detail($count, false, true);
}

function layout_detail_horizontal($count) {
	return layout_detail($count, false, false, true);
}
