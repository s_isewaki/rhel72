<?
$fname = $PHP_SELF;
ob_start();
require_once("about_comedix.php");
require_once("fplusadm_common.ini");
require_once("Cmx.php");
require_once("./class/Cmx/Model/SystemConfig.php");

ob_end_clean();

$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';

$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$checkauth = check_authority($session, 79, $fname);
$con = @connect2db($fname);
if(!$session || !$checkauth || !$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}

$conf = new Cmx_SystemConfig();

if($reg_flg == "reg")
{

	// 「医療事故報告管理表を利用する」にチェックがある場合
	if($medical_accident_report_list_use[0] == "")
	{
		$conf->set("medical_accident_report_list_use",0);
	}else{
		$conf->set("medical_accident_report_list_use",1);
	}

	// 「本部集計表（医療事故情報集計表）を利用する」にチェックがある場合
	if($headquarters_total_list_use[0] == "")
	{
		$conf->set("headquarters_total_list_use",0);
	}else{
		$conf->set("headquarters_total_list_use",1);
	}
}



//==============================
//利用する画面のチェック一覧を取得
//==============================

//「医療事故報告管理表を利用する」チェック
if($conf->get("medical_accident_report_list_use") == "1")
{
	$ck_management = "checked";
}
else
{
	$ck_management = "";
}

//「本部集計表（医療事故情報集計表）を利用する」チェック
if($conf->get("headquarters_total_list_use") == "1")
{
	$ck_hq_summary = "checked";
}
else
{
	$ck_hq_summary = "";
}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ファントルくん＋ | 管理画面 | オプション設定</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
function registOption()
{

	ret_mes = confirm("表示状態で登録しますが、よろしいですか？");

	if(ret_mes == false)
	{
		return false;
	}

	document.regOption.reg_flg.value = "reg";
	document.regOption.submit();
}
</script>

<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#E6B3D4 solid 1px;}
.list td td {border-width:0;}

.list2 {border-collapse:collapse;}
.list2 td {border:#E6B3D4 solid 1px;}
.list2_in1 {border-collapse:collapse;}
.list2_in1 td {border-width:0;}
.list2_in2 {border-collapse:collapse;}
.list2_in2 td {border:#E6B3D4 solid 1px;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#E6B3D4 solid 0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">


<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="32" height="32" class="spacing"><a href="fplus_menu.php?session=<? echo($session); ?>"><img src="img/icon/b47.gif" width="32" height="32" border="0" alt="ファントルくん＋"></a></td>
					<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="fplus_menu.php?session=<? echo($session); ?>"><b>ファントルくん＋</b></a> &gt; <a href="fplusadm_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; <a href="fplusadm_option.php?session=<? echo($session); ?>"><b>本部機能設定</b></a></font></td>
					<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="fplus_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table style="width:100%; margin-top:4px">
				<tr>
					<td style="vertical-align:top; width:120px"><?= fplusadm_common_show_admin_left_menu($session); ?></td>
					<td style="vertical-align:top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:2px solid #c488ae; margin-bottom:2px">
							<tr>
								<td width="3" align="right"><img src="img/menu-fplus-left-on.gif" alt="" width="3" height="22"></td>
								<td width="120" align="center" bgcolor="#FF99CC"><a href="fplusadm_option.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><nobr>本部機能設定</nobr></font></a></td>
								<td width="3" align="left"><img src="img/menu-fplus-right-on.gif" alt="" width="3" height="22"></td>
								<td width="10">&nbsp;</td>
								<td width="3" align="right"><img src="img/menu-fplus-left.gif" alt="" width="3" height="22"></td>
								<td width="120" align="center" bgcolor="#FFDFFF"><a href="fplusadm_headquarters_auth_emp.php?session=<?=$session?>&auth_part=1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>医療事故管理表担当者</nobr></font></a></td>
								<td width="3" align="left"><img src="img/menu-fplus-right.gif" alt="" width="3" height="22"></td>
								<td width="10">&nbsp;</td>
								<td width="3" align="right"><img src="img/menu-fplus-left.gif" alt="" width="3" height="22"></td>
								<td width="120" align="center" bgcolor="#FFDFFF"><a href="fplusadm_headquarters_auth_emp.php?session=<?=$session?>&auth_part=2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>本部集計表担当者</nobr></font></a></td>
								<td width="3" align="left"><img src="img/menu-fplus-right.gif" alt="" width="3" height="22"></td>
								<td>&nbsp;</td>
							</tr>
						</table>
						<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:6px">
							<tr>
								<td width="30%" valign="top">
									<form name="regOption" action="fplusadm_option.php" method="post">
										<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
											<tr>
												<td>
													<table width="100%" border="0" cellspacing="0" cellpadding="1">
														<tr bgcolor="#feeae9">
															<td class="spacing" height="24"><?=$font?>本部機能利用選択</font></td>
															<td align="right">
																<input type="button" value="登録" onclick="registOption();">
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td valign="top">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="medical_accident_report_list_use[]" id="medical_accident_report_list_use[]" value="01" <?=$ck_management?> >医療事故報告管理表を利用する</font>
												</td>
											</tr>
											<tr>
												<td valign="top">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="headquarters_total_list_use[]" id="headquarters_total_list_use[]" value="01" <?=$ck_hq_summary?> >本部集計表（医療事故情報集計表）を利用する</font>
												</td>
											</tr>
										</table>
										<input type="hidden" name="session" value="<? echo($session); ?>">
										<input type="hidden" name="reg_flg" id="reg_flg" value="">
									</form>
								
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

</body>
</html>


