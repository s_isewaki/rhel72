<?php
require_once('fpdf153/mbfpdf.php');

/**
 * PDF作成拡張クラス
 */
class CustomMBFPDF extends MBFPDF
{
    /**
     * 患者ID
     * 
     * @var string
     */
    var $_ptifID = null;
    
    /**
     * 楕円のレート
     * 
     * @var float
     */
    var $_circleRate = 2.5;
    
    /**
     * 円を表示する位置のX方向オフセット値
     * 
     * @var float
     */
    var $_circleOffsetX = 1.0;
    
    /**
     * 円を表示する位置のY方向オフセット値
     * 
     * @var float
     */
    var $_circleOffsetY = 0.0;
    
    /**
     * コンストラクタ
     * 
     * @param string $ptifId 患者ID
     * @param array  $params 設定パラメータ
     *
     * @return void
     */
    function CustomMBFPDF($ptifId = null, $params = array(), $orientation = 'P', $unit = 'mm', $format = 'A4')
    {
        $this->_ptifID = $ptifId;
        $this->_circleRate = isset($params['circleRate']) ? $params['circleRate'] : $this->_circleRate;
        $this->_circleOffsetX = isset($params['circleOffsetX']) ? $params['circleOffsetX'] : $this->_circleOffsetX;
        $this->_circleOffsetY = isset($params['circleOffsetY']) ? $params['circleOffsetY'] : $this->_circleOffsetY;
        
        parent::MBFPDF($orientation, $unit, $format);
    }
    
    /**
     * 円の描画
     * 
     * @param float $x0 横位置
     * @param float $y0 縦位置
     * @param float $w0 円の大きさ
     * 
     * @return void
     */
    function Circle($x0, $y0, $w0) 
    {
        $w1 = $this->k * $w0;
        $y1 = ($this->h-($y0)) * $this->k;
        $x1 = $x0 * $this->k + $w0;
        $lw = $this->LineWidth;
        $dc = $this->DrawColor;
        $this->_out('1.0 w'); // 線幅
        $this->_out('0.4 0.4 0.4 RG'); // 灰色
        $r = $this->_circleRate; // 楕円のレート
        $this->_out(sprintf('%.2f %.2f m', $x1, $y1)); // 開始点に移動
        $this->_out(sprintf('%.2f %.2f %.2f %.2f %.2f %.2f c', ($x1+$w1*$r), $y1, ($x1+$w1*$r), ($y1-$w1), $x1, $y1-$w1)); // ベジエ曲線で半円描画
        $this->_out('S'); // セット
        $this->_out(sprintf('%.2f %.2f m', $x1, $y1)); // 開始点に移動
        $this->_out(sprintf('%.2f %.2f %.2f %.2f %.2f %.2f c', ($x1-$w1*$r), $y1, ($x1-$w1*$r), ($y1-$w1), $x1, $y1-$w1)); // ベジエ曲線で半円描画
        $this->_out('S'); // セット
        $this->_out(sprintf('%.2f w', $lw)); // 線幅を戻す
        $this->_out($dc); // 色を戻す
    }

    function Circle_On($obj, $size, $X = 0.0)
    {
        if($obj == 'true') {
            $x = $X ? $X : $this->GetX() + $this->_circleOffsetX;
            $y = $this->GetY() + $this->_circleOffsetY;
            $this->Circle($x, $y, $size);
        }
    }

    /**
     * ｢EUC-JP → SJIS｣変換のためのオーバーライド
     * 
     * MultiCell()もWrite()もこのCell()で表示しているので、オーバーライドはCell()だけでOK
     * 
     * @param float   $w      幅
     * @param float   $h      高さ
     * @param string  $txt    テキスト
     * @param mixed   $border ボーダー
     * @param integer $ln     改行
     * @param string  $align  配置
     * @param integer $fill   塗りつぶし
     * @param mixed   $link   リンク
     * 
     * @return void
     */
    function Cell($w, $h = 0, $txt = '', $border = 0, $ln = 0, $align = '', $fill = 0, $link = '')
    {
        $enc = mb_internal_encoding();
        if($enc != "sjis-win"){
            $txt = mb_convert_encoding($txt, "sjis-win", $enc) ;
            parent::Cell($w, $h, $txt, $border, $ln, $align, $fill, $link) ;
        }
    }
    
    /**
     * フッター出力(オーバーライド)
     * この出力以外のフッターにしたい場合は、インスタンス生成時に患者IDへNULLをセットするか、このクラスの継承先でオーバーライドする。
     * 
     * @return void
     */
    function Footer()
    {
        if (is_null($this->_ptifID)) {
            // 患者IDがNULLの場合は、フッター出力しない
            return;
        }
        
        $this->setY(-15);
        $this->SetFont(GOTHIC, '', 8);
        $this->Cell(90, 5.0, '患者ID : ' . $this->_ptifID, 0, 0, 'L');
        $this->Cell(90, 5.0, $this->PageNo() . 'ページ', 0, 1, 'R');
    }
    
    /**
     * 入力値の高さチェック 1
     * 
     * @param string  $txt    文字列
     * @param integer $char   1行に入る文字数
     * @param integer $h_size 1行のサイズ
     * 
     * @return integer 入力値の高さ
     */
    function hi_check1($txt, $char, $h_size)
    {
        $s = str_replace("\r",'~',$txt);
        $ary = explode('~',$s);
        $int = 0;
        $c = $char * 2;
        for ($i = 0; $i < count($ary); ++$i) {
            if(strlen($ary[$i]) > $c) {
                $int = $int + floor(strlen($ary[$i]) / $c);
            }
            ++$int;
        }
        $cnt = $int;
        $height = $cnt * $h_size;
        return $height;
    }
    
    /**
     * 入力値の高さチェック 2
     * 
     * @param string  $txt  文字列
     * @param integer $char 1行に入る文字数
     * @param integer $Row  1行のサイズ
     * 
     * @return integer 入力値の高さ
     */
    function hi_check2($txt, $char, $Row)
    {
        $s = str_replace("\r",'~',$txt);
        $ary = explode('~',$s);
        $int = 0;
        $c = $char * 2;
        for($i = 0; $i < count($ary); ++$i) {
            if(strlen($ary[$i]) > $c) {
                $int = $int + floor(strlen($ary[$i]) / $c);
            }
            ++$int;
        }
        $cnt = $int + $Row;
        $height = $cnt * 4;
        return $height;
    }

    /**
     * 線種を指定して直線を描画する
     * 
     * @param float $x1     始点のx座標
     * @param float $y1     始点のy座標
     * @param float $x2     終点のx座標
     * @param float $y2     終点のy座標
     * @param string $style 線種(dash:破線)
     * 
     * @return void
     */
    function Line($x1, $y1, $x2, $y2, $style = '')
    {
        if ($style === 'dash') {    // 破線
            if ($x1 != $x2) {
                $x = $x2 - $x1;
                $y = $y2 - $y1;
                $rad = atan2($y, $x);
            } else {
                if ($y1 <= $y2) {
                    $rad = M_PI / 2.0;
                } else {
                    $rad = (M_PI / 2.0) * (-1.0);
                }
            }
            $dash_gap = 0.5;
            $dash_start_x = $x1;
            $dash_start_y = $y1;
            $dash_end_x = $dash_start_x + $dash_gap * cos($rad);
            $dash_end_y = $dash_start_y + $dash_gap * sin($rad);
            $dash_goal_x = $x2;
            $dash_goal_y = $y2;
            while (1) {
                if ($x1 <= $x2) {
                    if (($dash_goal_x < $dash_start_x) || ($dash_goal_x < $dash_end_x)) {
                        break;
                    }
                } elseif ($x1 > $x2) {
                    if (($dash_goal_x > $dash_start_x) || ($dash_goal_x > $dash_end_x)) {
                        break;
                    }
                }

                if ($y1 <= $y2) {
                    if (($dash_goal_y < $dash_start_y) || ($dash_goal_y < $dash_end_y)) {
                        break;
                    }
                } else {
                    if (($dash_goal_y > $dash_start_y) || ($dash_goal_y > $dash_end_y)) {
                        break;
                    }
                }

                $this->Line($dash_start_x, $dash_start_y, $dash_end_x, $dash_end_y);

                // 次の開始・終了位置
                $dash_start_x = $dash_end_x + $dash_gap * cos($rad);
                $dash_end_x = $dash_start_x + $dash_gap * cos($rad);
                $dash_start_y = $dash_end_y + $dash_gap * sin($rad);
                $dash_end_y = $dash_start_y + $dash_gap * sin($rad);
            }
        } else {
            parent::Line($x1, $y1, $x2, $y2);
        }
    }
}