<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix スケジュール | 公開スケジュール（年）</title>
<script LANGUAGE="JavaScript">
<!--
function goLastMonth(month, year,set_date){
<? echo("var today=".$date.";"); ?>
--today;
var set_date = set_date;
document.location.href = "schedule_other_year_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date="+today+"&s_date="+set_date;
}
function goNextMonth(month, year){
<? echo("var today=".$date.";"); ?>
var set_date = set_date;
++today;
document.location.href = "schedule_other_year_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date="+today+"&s_date="+set_date;
}
//-->
</script>
<?
$fname=$PHP_SELF;
require("conf/sql.inf");                        //sqlの一覧ファイルを読み込む
require("about_postgres.php");          //db処理のためのファイルを読み込む
require("show_select_values.ini");
require("about_session.php");
require("about_authority.php");
require("get_values.ini");
require("time_check2.ini");
require("show_schedule_to_do2.ini");
require("project_check2.ini");
require("show_date_navigation_year.ini");
require("show_schedule_common.ini");

$session = qualify_session($session,$fname);
if($session == "0"){
echo("<script type='text/javascript' src='js/showpage.js'></script>");
echo("<script language='javascript'>showLoginPage(window);</script>");
exit;
}
$empif = check_authority($session,2,$fname);
if($empif == "0"){
echo("<script type='text/javascript' src='js/showpage.js'></script>");
echo("<script language='javascript'>showLoginPage(window);</script>");
exit;
}

// タスク権限を取得
$task = check_authority($session, 30, $fname);
$task_flg = ($task != "0");

if(!isset($_REQUEST['s_date'])){
$date1 = mktime(0,0,0,s_date('m'), s_date('d'), s_date('Y'));
}else{
$date1 = $_REQUEST['s_date'];
}
$dayx = date('d', $date1);
$monthx = date('m', $date1);
$yearx = date('Y', $date1);


$day = "01";
$month = date('m');
$year =$date;
$todayday = date('d');
$todaymonth = date('m');
$todayyear = date('Y');
$con = connect2db($fname);
//--0219追加ここから---
$cond = " where session_id='$session'";
$sel = select_from_table($con,$SQL1,$cond,$fname);

if($sel == 0){
pg_close($con);
echo("<script type='text/javascript' src='js/showpage.js'></script>");
echo("<script language='javascript'>showErrorPage(window);</script>");
exit;
}
$e_id = pg_result($sel,0,"emp_id");

if($timegd == "on" || $timegd == "off"){
time_update($con,$e_id,$timegd,$fname);
}

if($timegd == ""){
$timegd = time_check($con,$e_id,$fname);
}

if($pjtgd == "on" || $pjtgd == "off"){
project_update($con,$e_id,$pjtgd,$fname);
}

if($pjtgd == ""){
$pjtgd = project_check($con,$emp_id,$e_id,$fname);
}

echo("<script language=\"javascript\">\n");
echo("function showGuide(){\n");
echo("var ses = \"".$session."\";\n");

$sd =mktime(0,0,0,$monthx,$dayx,$yearx);

echo("var dt  = ".$year.";\n");
echo("var sdt  = ".$sd.";\n");
echo("var emp_id =\"".$emp_id."\";\n");
echo("if(document.timegd.timeguide.checked == true){\n");
echo("var url = \"schedule_other_year_menu.php?session=\"+ses+\"&emp_id=\"+emp_id+\"&date=\"+dt+\"&s_date=\"+sdt+\"&timegd=on\";\n");
echo("location.href=url;\n");
echo("}else{\n");
echo("var url = \"schedule_other_year_menu.php?session=\"+ses+\"&emp_id=\"+emp_id+\"&date=\"+dt+\"&s_date=\"+sdt+\"&timegd=off\";\n");
echo("location.href=url;\n");
echo("}\n");
echo("}\n");

echo("function showProject(){\n");
echo("var ses = \"".$session."\";\n");
echo("var emp_id = \"".$emp_id."\";");
$dt = mktime(0,0,0,$month,$day,$year);
echo("var dt  = ".$dt.";\n");
echo("if(document.timegd.pjtguide.checked == true){\n");
echo("var url = \"schedule_other_year_menu.php?session=\"+ses+\"&emp_id=\"+emp_id+\"&date=\"+dt+\"&pjtgd=on\";\n");
echo("location.href=url;\n");
echo("}else{\n");
echo("var url = \"schedule_other_year_menu.php?session=\"+ses+\"&emp_id=\"+emp_id+\"&date=\"+dt+\"&pjtgd=off\";\n");
echo("location.href=url;\n");
echo("}\n");
echo("}\n");

echo("</script>\n");

// 「前年」「翌年」のタイムスタンプを取得
$last_year = get_last_year($date1);
$next_year = get_next_year($date1);

// オプション設定情報を取得
$sql = "select calendar_start1 from option";
$cond = "where emp_id = '$e_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$calendar_start = pg_fetch_result($sel, 0, "calendar_start1");
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function changeDate(dt) {
    var arr_dt = dt.split(',');
    location.href = 'schedule_other_year_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=' + arr_dt[0] + '&s_date=' + arr_dt[1];
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>公開スケジュール（年）</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<form name="timegd">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="80" height="22" align="center" bgcolor="#bdd1e7"><a href="schedule_other_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo(mktime(0,0,0,$monthx,$dayx,$yearx)); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_other_week_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo(mktime(0,0,0,$monthx,$dayx,$yearx)); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_other_month_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo(mktime(0,0,0,$monthx,$dayx,$yearx)); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#5279a5"><a href="schedule_other_year_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($year); ?>&s_date=<? echo(mktime(0,0,0,$monthx,$dayx,$yearx)); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>年</b></font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_other_list_upcoming.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo(mktime(0,0,0,$monthx,$dayx,$yearx)); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<? if ($task_flg) { ?>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_other_task.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo(mktime(0,0,0,$monthx,$dayx,$yearx)); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">公開タスク</font></a></td>
<? } ?>
<td width="43"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b><? echo(get_emp_kanji_name($con,$emp_id,$fname)); ?></b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5" colspan="10"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<?
error_reporting('0');
ini_set('display_errors', '0');

$day = "01";
$month = "01";
$year =$date;
$set_date = mktime(0,0,0,$month,$day,$year);

// 年間の日ごとの予定件数を取得
$arr_schedule_count = get_arr_schedule_count($con, $fname, $emp_id, $year, $e_id);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="schedule_other_year_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo(date("Y", $last_year)); ?>&s_date=<? echo($last_year); ?>">&lt;前年</a>&nbsp;<select onchange="changeDate(this.options[this.selectedIndex].value);"><? show_date_options_y($last_year, $date1, $next_year); ?></select>&nbsp;<a href="schedule_other_year_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo(date("Y", $next_year)); ?>&s_date=<? echo($next_year); ?>">翌年&gt;</a></font></td>
</tr>
</table>
<?
echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");

$tp = 0;
for($p=0; $p<4; $p++){
$tp = $tp+1;
echo("<tr valign=\"top\">\n");

for($z=0; $z<3; $z++){

$month_start = mktime(0,0,0,$month, 01, $year);

$month_name = date('m', $month_start);

$month_start_day = date('D', $month_start);

switch($month_start_day){
    case "Sun": $offset = ($calendar_start == 1) ? 0 : 6; break;
    case "Mon": $offset = ($calendar_start == 1) ? 1 : 0; break;
    case "Tue": $offset = ($calendar_start == 1) ? 2 : 1; break;
    case "Wed": $offset = ($calendar_start == 1) ? 3 : 2; break;
    case "Thu": $offset = ($calendar_start == 1) ? 4 : 3; break;
    case "Fri": $offset = ($calendar_start == 1) ? 5 : 4; break;
    case "Sat": $offset = ($calendar_start == 1) ? 6 : 5; break;
}

if($month == 1){
$num_days_last = 0;
$num_days_last = cal_days_in_month(0, 12, ($year -1));
}else{
$num_days_last = 0;
$num_days_last = cal_days_in_month(0, ($month -1), $year);
}

$num_days_current = 0;
$num_days_current = cal_days_in_month(0, $month, $year);

$num_days_array = array();
for($i=1; $i<=$num_days_current; $i++){
$num_days_array[] = $i;
}

$num_days_last_array = array();
for($i=1; $i<=$num_days_last; $i++){
$num_days_last_array[] = $i;
}

if($offset > 0){
$offset_correction = array_slice($num_days_last_array, -$offset, $offset);
$new_count = array_merge($offset_correction, $num_days_array);
$offset_count = count($offset_correction);
}else{
$offset_count = 0;
$new_count = $num_days_array;
}

$current_num = count($new_count);

if($current_num > 35){
$num_weeks = 6;
$outset = (42 - $current_num);
}elseif($current_num < 35){
$num_weeks = 5;
$outset = (35 - $current_num);
}

if($current_num == 35){
$num_weeks = 5;
$outset = 0;
}

for($i=1; $i<=$outset; $i++){
$new_count[] = $i;
}

$weeks = array_chunk($new_count, 7);

echo("<td colspan=\"7\">\n");
echo("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"0\" style=\"border-color: #5279a5;border-width:1px;border-style:solid;\">\n");
echo("<tr>\n");
 echo("<td height=\"22\" colspan=\"7\" bgcolor=\"f6f9ff\">\n");
echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
echo($year);
echo("/");
echo($month_name);
echo("</font>");
echo("</td>\n");
echo("</tr>\n");    //追加
echo("<tr>\n");
if ($calendar_start == 1) {
    echo("<td align=\"center\" bgcolor=\"#fadede\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">日</font></td>\n");
}
echo("<td align=\"center\" bgcolor=\"#bdd1e7\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">月</font></td>\n");
echo("<td align=\"center\" bgcolor=\"#bdd1e7\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">火</font></td>\n");
echo("<td align=\"center\" bgcolor=\"#bdd1e7\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">水</font></td>\n");
echo("<td align=\"center\" bgcolor=\"#bdd1e7\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">木</font></td>\n");
echo("<td align=\"center\" bgcolor=\"#bdd1e7\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">金</font></td>\n");
echo("<td align=\"center\" bgcolor=\"#defafa\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">土</font></td>\n");
if ($calendar_start == 2) {
    echo("<td align=\"center\" bgcolor=\"#fadede\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">日</font></td>\n");
}
echo("</tr>\n");

$i = 0;
foreach($weeks AS $week){
    echo("<tr>\n");
    foreach($week as $d){
        if($i < $offset_count){
            $day_link = "<a href=\"schedule_other_menu.php?session=".$session."&emp_id=".$emp_id."&date=".mktime(0,0,0,$month -1,$d,$year)."\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$d</font></a>";
            echo("<td>&nbsp;</td>\n");
        }

        if(($i >= $offset_count) && ($i < ($num_weeks * 7) - $outset)){

            // スケジュールが登録されていたら背景色をピンクに
            $day = ($d < 10) ? "0".$d : $d;
            $schedule_count = $arr_schedule_count["$year-$month-$day"];
            $bgcolor = ($schedule_count > 0) ? "#fbdade" : "#ffffff";

            $day_link = "<a href=\"schedule_other_menu.php?session=$session&emp_id=".$emp_id."&date=".mktime(0,0,0,$month,$d,$year)."\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$d</font></a>";
            echo("<td align=\"center\" bgcolor=\"$bgcolor\">$day_link</td>\n");

        }elseif(($outset > 0)){
            if(($i >= ($num_weeks * 7) - $outset)){

                $day_link = "<a href=\"schedule_other_menu.php?session=$session&emp_id=".$emp_id."&date=".mktime(0,0,0,$month,$d,$year)."\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$d</font></a>";
                echo("<td>&nbsp;</td>\n");
            }
        }
        $i++;
    }
    echo("</tr>\n");
}

$month=$month+1;
if(strlen($month)==1){
    $month="0".$month;
}

echo("</table></td>");
if ($z < 2) {
    echo("<td width=\"5\"><img src=\"img/spacer.gif\" width=\"5\" height=\"1\" alt=\"\"></td>");
}
$tp=$tp+1;
}
echo("</tr>\n");
echo("<tr>\n");
echo("<td colspan=\"3\">\n");
echo("<img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
echo("</td>\n");
echo("</tr>\n");
}
echo("</table>\n");

?>
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
// 年間の日ごとの予定件数をまとめて取得する
function get_arr_schedule_count($con, $fname, $emp_id, $year, $login_emp_id) {

    // 日付範囲を設定
    $start_day = $year."-01-01";
    $end_day = $year."-12-31";

    $sql = "select count(*) as cnt, s.schd_start_date from schdmst s";
    $cond = get_other_schedule_sql_base($emp_id, $login_emp_id) . " and s.schd_start_date >= '$start_day' and s.schd_start_date <= '$end_day' group by s.schd_start_date order by s.schd_start_date";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $arr_schedule_count = array();
    while ($row = pg_fetch_array($sel)) {
        $arr_schedule_count[$row["schd_start_date"]] = $row["cnt"];
    }

    $sql = "select count(*) as cnt, s.schd_start_date from schdmst2 s";
    $cond = get_other_schedule_sql_base($emp_id, $login_emp_id) . " and s.schd_start_date >= '$start_day' and s.schd_start_date <= '$end_day' group by s.schd_start_date order by s.schd_start_date";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_array($sel)) {
        $arr_schedule_count[$row["schd_start_date"]] += $row["cnt"];
    }
    return $arr_schedule_count;
}
?>
