<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 52, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースへ接続
$con = connect2db($fname);

// 初期表示時は補助項目情報を取得
if ($back != "t") {
	$sql = "select statassist_name, statitem_id, assist_type, calc_var1_type, calc_var1_const_id, calc_var1_item_id, calc_var1_assist_id, calc_operator, calc_var2_type, calc_var2_const_id, calc_var2_item_id, calc_var2_assist_id, calc_format from statassist";
	$cond = "where statassist_id = $id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$name = pg_fetch_result($sel, 0, "statassist_name");
	$item_id = pg_fetch_result($sel, 0, "statitem_id");
	$assist_type = pg_fetch_result($sel, 0, "assist_type");
	$var1_type = pg_fetch_result($sel, 0, "calc_var1_type");
	$var1_const_id = pg_fetch_result($sel, 0, "calc_var1_const_id");
	$var1_item_id = pg_fetch_result($sel, 0, "calc_var1_item_id");
	$var1_assist_id = pg_fetch_result($sel, 0, "calc_var1_assist_id");
	$operator = pg_fetch_result($sel, 0, "calc_operator");
	$var2_type = pg_fetch_result($sel, 0, "calc_var2_type");
	$var2_const_id = pg_fetch_result($sel, 0, "calc_var2_const_id");
	$var2_item_id = pg_fetch_result($sel, 0, "calc_var2_item_id");
	$var2_assist_id = pg_fetch_result($sel, 0, "calc_var2_assist_id");
	$format = pg_fetch_result($sel, 0, "calc_format");
}

// 管理項目一覧を取得
$sql = "select statitem_id, name from statitem";
$cond = "where del_flg = 'f' order by statitem_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$items = array();
while ($row = pg_fetch_array($sel)) {
	$items[$row["statitem_id"]] = array("name" => $row["name"], "assists" => array());
}

// 補助項目一覧を取得（自分は含めない）
$sql = "select statassist_id, statassist_name, statitem_id from statassist";
$cond = "where assist_type = '1' and del_flg = 'f' and statassist_id <> $id order by statassist_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	$items[$row["statitem_id"]]["assists"][$row["statassist_id"]] = array(
		"name" => $row["statassist_name"]
	);
}

// 定数一覧を取得
$sql = "select statconst_id, statconst_name from statconst";
$cond = "where del_flg = 'f' order by statconst_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$consts = array();
while ($row = pg_fetch_array($sel)) {
	$consts[$row["statconst_id"]] = array("name" => $row["statconst_name"]);
}

// イントラメニュー名を取得
$sql = "select menu1 from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$intra_menu1 = pg_fetch_result($sel, 0, "menu1");
?>
<title>マスターメンテナンス | <? echo($intra_menu1); ?>設定 | 補助項目更新</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function initPage() {
	setItemOptions(document.mainform.var1_item_id, document.mainform.var1_assist_id, '<? echo($var1_item_id); ?>', '<? echo($var1_assist_id); ?>');
	setItemOptions(document.mainform.var2_item_id, document.mainform.var2_assist_id, '<? echo($var2_item_id); ?>', '<? echo($var2_assist_id); ?>');
	setEquationDisabled();
}

function setItemOptions(item_box, assist_box, default_item_id, default_assist_id) {
	deleteAllOptions(item_box);

	var parent_item_id = document.mainform.item_id.value;
<?
foreach ($items as $tmp_item_id => $tmp_item) {
	echo("\tif (parent_item_id != '$tmp_item_id') {\n");
	echo("\t\taddOption(item_box, '$tmp_item_id', '{$tmp_item["name"]}', default_item_id);\n");
	echo("\t}\n");
}
?>
	if (item_box.options.length == 0) {
		addOption(item_box, '', '（未登録）');
	}

	setAssistOptions(item_box, assist_box, default_assist_id);
}

function setAssistOptions(item_box, assist_box, default_assist_id) {
	deleteAllOptions(assist_box);

	var item_id = item_box.value;
<?
foreach ($items as $tmp_item_id => $tmp_item) {
	echo("\tif (item_id == '$tmp_item_id') {\n");
	foreach ($tmp_item["assists"] as $tmp_assist_id => $tmp_assist) {
		echo("\t\taddOption(assist_box, '$tmp_assist_id', '{$tmp_assist["name"]}', default_assist_id);\n");
	}
	echo("\t}\n");
}
?>
	if (assist_box.options.length == 0) {
		addOption(assist_box, '', '（未登録）');
	}
}

function setEquationDisabled() {
	var disabled = document.mainform.assist_type[0].checked;
	document.mainform.var1_type[0].disabled = disabled;
	document.mainform.var1_type[1].disabled = disabled;
	document.mainform.var1_const_id.disabled = disabled;
	document.mainform.var1_item_id.disabled = disabled;
	document.mainform.var1_assist_id.disabled = disabled;
	document.mainform.operator.disabled = disabled;
	document.mainform.var2_type[0].disabled = disabled;
	document.mainform.var2_type[1].disabled = disabled;
	document.mainform.var2_const_id.disabled = disabled;
	document.mainform.var2_item_id.disabled = disabled;
	document.mainform.var2_assist_id.disabled = disabled;
	document.mainform.format[0].disabled = disabled;
	document.mainform.format[1].disabled = disabled;

	setVar1Disabled();
	setVar2Disabled();
}

function setVar1Disabled() {
	var const_disabled = (document.mainform.assist_type[0].checked || document.mainform.var1_type[1].checked);
	var item_disabled = (document.mainform.assist_type[0].checked || document.mainform.var1_type[0].checked);
	document.mainform.var1_const_id.disabled = const_disabled;
	document.mainform.var1_item_id.disabled = item_disabled;
	document.mainform.var1_assist_id.disabled = item_disabled;
}

function setVar2Disabled() {
	var const_disabled = (document.mainform.assist_type[0].checked || document.mainform.var2_type[1].checked);
	var item_disabled = (document.mainform.assist_type[0].checked || document.mainform.var2_type[0].checked);
	document.mainform.var2_const_id.disabled = const_disabled;
	document.mainform.var2_item_id.disabled = item_disabled;
	document.mainform.var2_assist_id.disabled = item_disabled;
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}

function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.layout td {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<? echo($session); ?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<? echo($session); ?>"><b>イントラネット</b></a> &gt; <a href="intra_stat.php?session=<? echo($session); ?>"><b><? echo($intra_menu1); ?></b></a> &gt; <a href="stat_master_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="intra_stat.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="80" align="center" bgcolor="#bdd1e7"><a href="stat_master_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">プレビュー</font></a></td>
<td width="5">&nbsp;</td>
<td width="105" align="center" bgcolor="#bdd1e7"><a href="stat_master_item_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理項目一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="105" align="center" bgcolor="#bdd1e7"><a href="stat_master_item_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理項目登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="105" align="center" bgcolor="#5279a5"><a href="stat_master_assist_update.php?session=<? echo($session); ?>&id=<? echo($id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>補助項目更新</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="stat_master_fcl_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="stat_master_fcl_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="stat_master_const_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定数一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="stat_master_const_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定数登録</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="mainform" action="stat_master_assist_update_exe.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">補助項目名</font></td>
<td><input type="text" name="name" value="<? echo($name); ?>" size="30" maxlength="80" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理項目</font></td>
<td><select name="item_id" onchange="setItemOptions(this.form.var1_item_id, this.form.var1_assist_id); setItemOptions(this.form.var2_item_id, this.form.var2_assist_id);">
<?
if (count($items) > 0) {
	reset($items);
	foreach ($items as $tmp_item_id => $tmp_item) {
		echo("<option value=\"$tmp_item_id\"");
		if ($tmp_item_id == $item_id) {
			echo(" selected");
		}
		echo(">{$tmp_item["name"]}");
	}
} else {
	echo("<option value=\"\">（未登録）\n");
}
?>
</select></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">項目属性</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="assist_type" value="1"<? if ($assist_type == "1") {echo(" checked");} ?> onclick="setEquationDisabled();">入力
<input type="radio" name="assist_type" value="2"<? if ($assist_type == "2") {echo(" checked");} ?> onclick="setEquationDisabled();">自動計算
</font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td valign="bottom"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>計算式（自動計算のときに設定）</b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">項目1</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">演算記号</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">項目2</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">答え（表示項目）</font></td>
</tr>
<tr height="22" valign="top">
<td width="37%" style="padding:4px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<table border="0" cellspacing="0" cellpadding="0" class="layout">
<tr valign="top">
<td width="90"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="var1_type" value="1"<? if ($var1_type == "1") {echo(" checked");} ?> onclick="setVar1Disabled();">定数
</font></td>
<td>
<select name="var1_const_id">
<?
if (count($consts) > 0) {
	foreach ($consts as $tmp_const_id => $tmp_const) {
		echo("<option value=\"$tmp_const_id\"");
		if ($tmp_const_id == $var1_const_id) {
			echo(" selected");
		}
		echo(">{$tmp_const["name"]}");
	}
} else {
	echo("<option value=\"\">（未登録）\n");
}
?>
</select>
</td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="12"><br>
<table border="0" cellspacing="0" cellpadding="0" class="layout">
<tr valign="top">
<td width="90"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="var1_type" value="2"<? if ($var1_type == "2") {echo(" checked");} ?> onclick="setVar1Disabled();">項目指定
</font></td>
<td>
<select name="var1_item_id" onchange="setAssistOptions(this, this.form.var1_assist_id);">
</select>
<select name="var1_assist_id">
</select>
</td>
</tr>
</table>
</td>
<td width="10%" valign="middle" style="padding:4px;">
<select name="operator">
<option value="1"<? if ($operator == "1") {echo(" selected");} ?>>＋
<option value="2"<? if ($operator == "2") {echo(" selected");} ?>>−
<option value="3"<? if ($operator == "3") {echo(" selected");} ?>>×
<option value="4"<? if ($operator == "4") {echo(" selected");} ?>>÷
</select>
</td>
<td width="37%" style="padding:4px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<table border="0" cellspacing="0" cellpadding="0" class="layout">
<tr valign="top">
<td width="90"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="var2_type" value="1"<? if ($var2_type == "1") {echo(" checked");} ?> onclick="setVar2Disabled();">定数
</font></td>
<td>
<select name="var2_const_id">
<?
if (count($consts) > 0) {
	reset($consts);
	foreach ($consts as $tmp_const_id => $tmp_const) {
		echo("<option value=\"$tmp_const_id\"");
		if ($tmp_const_id == $var2_const_id) {
			echo(" selected");
		}
		echo(">{$tmp_const["name"]}");
	}
} else {
	echo("<option value=\"\">（未登録）\n");
}
?>
</select>
</td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="12"><br>
<table border="0" cellspacing="0" cellpadding="0" class="layout">
<tr valign="top">
<td width="90"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="var2_type" value="2"<? if ($var2_type == "2") {echo(" checked");} ?> onclick="setVar2Disabled();">項目指定
</font></td>
<td>
<select name="var2_item_id" onchange="setAssistOptions(this, this.form.var2_assist_id);">
</select>
<select name="var2_assist_id">
</select>
</td>
</tr>
</table>
</td>
<td width="16%" style="padding:4px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="format" value="1"<? if ($format == "1") {echo(" checked");} ?>>実数表示<br>
<img src="img/spacer.gif" alt="" width="1" height="12"><br>
<input type="radio" name="format" value="2"<? if ($format == "2") {echo(" checked");} ?>>%表示
</font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="id" value="<? echo($id); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
