<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="cas_application_apply_detail.php" method="post">
<input type="hidden" name="apply_title" value="<? echo($apply_title); ?>">
<input type="hidden" name="content" value="<? echo($content); ?>">
<?
foreach ($filename as $tmp_filename) {
	echo("<input type=\"hidden\" name=\"filename[]\" value=\"$tmp_filename\">\n");
}
foreach ($file_id as $tmp_file_id) {
	echo("<input type=\"hidden\" name=\"file_id[]\" value=\"$tmp_file_id\">\n");
}

for ($i = 1; $i <= $approve_num; $i++) {
	$varname = "regist_emp_id$i";
	echo("<input type=\"hidden\" name=\"regist_emp_id$i\" value=\"{$$varname}\">\n");
}

for ($i = 1; $i <= $precond_num; $i++) {
	$varname = "precond_apply_id$i";
	echo("<input type=\"hidden\" name=\"precond_apply_id$i\" value=\"{$$varname}\">\n");
}

for ($i = 1; $i <= $precond_num; $i++) {
	$varname = "precond_wkfw_id$i";
	echo("<input type=\"hidden\" name=\"precond_wkfw_id$i\" value=\"{$$varname}\">\n");
}

/*
for ($i = 1; $i <= $approve_num; $i++) {
	$varname = "approve_name$i";
	echo("<input type=\"hidden\" name=\"approve_name$i\" value=\"{$$varname}\">\n");
}
*/

?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="apply_id" value="<? echo($apply_id); ?>">
<input type="hidden" name="approve_num" value="<? echo($approve_num); ?>">
<input type="hidden" name="precond_num" value="<? echo($precond_num); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="notice_emp_id" id="notice_emp_id" value="<?=$notice_emp_id?>">
<input type="hidden" name="notice_emp_nm" id="notice_emp_nm" value="<?=$notice_emp_nm?>">
<input type="hidden" name="wkfw_history_no" value="<?=$wkfw_history_no?>">

</form>


<?

require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.ini");
require_once("cas_application_workflow_common_class.php");
require_once("cas_common.ini");

$fname=$PHP_SELF;


// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 決裁・申請権限のチェック
$checkauth = check_authority($session, $CAS_MENU_AUTH, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (strlen($apply_title) > 80) {
	echo("<script type=\"text/javascript\">alert('表題が長すぎます。');</script>");
//	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	echo("<script language='javascript'>history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$obj = new cas_application_workflow_common_class($con, $fname);

// トランザクションを開始
pg_query($con, "begin");

// ログインユーザ取得
$arr_empmst = $obj->get_empmst($session);
$login_emp_id = $arr_empmst[0]["emp_id"];


// 再申請チェック
$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
$apply_stat = $arr_apply_wkfwmst[0]["apply_stat"];
if($apply_stat != "3") {
	echo("<script type=\"text/javascript\">alert(\"申請状況が変更されたため、再申請できません。\");</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// 新規申請(apply_id)採番
$sql = "select max(apply_id) from cas_apply";
$cond = "";
$apply_max_sel = select_from_table($con,$sql,$cond,$fname);
$max = pg_result($apply_max_sel,0,"max");
if($max == ""){
	$new_apply_id = "1";
}else{
	$new_apply_id = $max + 1;
}


//テンプレートの場合XML形式のテキスト$contentを作成
if ($wkfw_content_type == "2") {
	$ext = ".php";
	$savexmlfilename = "cas_workflow/tmp/{$session}_x{$ext}";
//	if (is_file($savexmlfilename)) {
//		include( $savexmlfilename );
//	}

	// ワークフロー情報取得
//	$sel_wkfwmst = search_wkfwmst($con, $fname, $wkfw_id);
//	$wkfw_content = pg_fetch_result($sel_wkfwmst, 0, "wkfw_content");

	$arr_wkfw_template_history = $obj->get_wkfw_template_history($wkfw_id, $wkfw_history_no);
	$wkfw_content = $arr_wkfw_template_history["wkfw_content"];

	$wkfw_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $wkfw_content);
	$fp = fopen($savexmlfilename, "w");
	if (!$fp) {
		echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、申請してください。$savexmlfilename');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	if(!fwrite($fp, $wkfw_content_xml, 2000000)) {
		fclose($fp);
		echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、申請してください。');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	fclose($fp);

	include( $savexmlfilename );

}

// 'エスケープ
$apply_title   = pg_escape_string($apply_title);
$apply_content = pg_escape_string($content);

// 申請登録
$obj->regist_re_apply($new_apply_id, $apply_id, $apply_content, $apply_title, "DETAIL");


// 承認登録
$obj->regist_re_applyapv($new_apply_id, $apply_id);


// 承認者候補登録
$obj->regist_re_applyapvemp($new_apply_id, $apply_id);


// 添付ファイル登録
$no = 1;
foreach ($filename as $tmp_filename)
{
	$obj->regist_applyfile($new_apply_id, $no, $tmp_filename);
	$no++;
}

// 非同期・同期受信登録
$obj->regist_re_applyasyncrecv($new_apply_id, $apply_id);


// 申請結果通知登録
if($notice_emp_id != "")
{
	$arr_notice_emp_id = split(",", $notice_emp_id);
    $arr_rslt_ntc_div  = split(",", $rslt_ntc_div);
	for($i=0; $i<count($arr_notice_emp_id); $i++)
	{
		$obj->regist_applynotice($new_apply_id, $arr_notice_emp_id[$i], $arr_rslt_ntc_div[$i]);
	}
}

// 前提とする申請書(申請用)登録
for($i=0; $i<$precond_num; $i++)
{
	$order = $i + 1;
	$varname = "precond_wkfw_id$order";
	$precond_wkfw_id = $$varname;

	$varname = "precond_apply_id$order";
	$precond_apply_id = $$varname;

	$obj->regist_applyprecond($new_apply_id, $precond_wkfw_id, $order, $precond_apply_id);
}

// 元の申請書に再申請ＩＤを更新
$obj->update_re_apply_id($apply_id, $new_apply_id);


// 評価申請の場合
$short_wkfw_name = $arr_apply_wkfwmst[0]["short_wkfw_name"];
if($obj->is_eval_template($short_wkfw_name))
{
	if($obj->is_eval_level2_template($short_wkfw_name))
	{
        $level = "2";
    }
    else if($obj->is_eval_level3_template($short_wkfw_name))
    {
        $level = "3";
    }

    $application_book_apply_id = $obj->get_cas_recognize_schedule_apply_id($apply_id, $short_wkfw_name);
    if($application_book_apply_id > 0)
    {
        $arr_eval_apply_id = $obj->create_arr_eval_apply_id($application_book_apply_id, $new_apply_id, $short_wkfw_name, "ADD");
	    $obj->update_cas_recognize_schedule_for_apply_id($arr_eval_apply_id);
    }
}

// レベルアップ申請の場合
if($obj->is_levelup_apply_template($short_wkfw_name))
{
    // 念のため
    if($levelup_application_book_id != "")
    {
	    $apply_content = $arr_apply_wkfwmst[0]["apply_content"];
		$levelup_application_book_id = $obj->get_levelup_application_book_id($apply_content);
    }
    $obj->update_levelup_apply_id($levelup_application_book_id, $new_apply_id);
}


// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);


if (!is_dir("cas_apply")) {
	mkdir("cas_apply", 0755);
}

// 添付ファイルの移動
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$tmp_filename = $filename[$i];
	$tmp_fileno = $i + 1;
	$ext = strrchr($tmp_filename, ".");

	$tmp_filename = "cas_apply/tmp/{$session}_{$tmp_file_id}{$ext}";
	copy($tmp_filename, "cas_apply/{$new_apply_id}_{$tmp_fileno}{$ext}");

}
foreach (glob("cas_apply/tmp/{$session}_*.*") as $tmpfile) {
	unlink($tmpfile);
}





// 一覧画面に遷移
echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>");
echo("<script language=\"javascript\">close();</script>\n");

?>
</body>
<?
// ワークフロー情報取得
/*
function search_wkfwmst($con, $fname, $wkfw_id) {

	$sql = "select * from wkfwmst";
	$cond="where wkfw_id='$wkfw_id'";
	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}
*/
?>
