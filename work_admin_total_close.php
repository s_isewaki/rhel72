<?
ini_set("max_execution_time", "0");

require_once("about_comedix.php");
require_once("show_timecard_common.ini");
require_once("show_attendance_pattern.ini");
require_once("holiday.php");
require_once("timecard_common_class.php");
require_once("date_utils.php");
require_once("atdbk_common_class.php");
require_once("atdbk_workflow_common_class.php");
require_once("timecard_bean.php");
require_once("timecard_paid_hol_hour_class.php");
require_once("calendar_name_class.php");
require_once("atdbk_info_common.php");
require_once("ovtm_class.php");
require_once("atdbk_close_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 勤務管理権限チェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0" && $mmode != "usr") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

//時間有休
$obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
$obj_hol_hour->select();

//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);

//ワークフロー関連共通クラス
$atdbk_workflow_common_class = new atdbk_workflow_common_class($con, $fname);
//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);
//カレンダー名、所定労働時間履歴用
$calendar_name_class = new calendar_name_class($con, $fname);
//残業申請関連クラス
$ovtm_class = new ovtm_class($con, $fname);

$atdbk_close_class = new atdbk_close_class($con, $fname);

// 残業申請画面を開く時間を取得
$sql = "select ovtmscr_tm from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
if (pg_num_rows($sel) > 0) {
    $ovtmscr_tm = pg_fetch_result($sel, 0, "ovtmscr_tm");
} else {
    $ovtmscr_tm = 0;
}

// 端数処理情報を取得
$sql = "select * from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
if (pg_num_rows($sel) > 0) {
    for ($i = 1; $i <= 5; $i++) {
        $var_name = "fraction$i";
        $var_name_min = "fraction{$i}_min";
        $$var_name = pg_fetch_result($sel, 0, "fraction$i");
        $$var_name_min = pg_fetch_result($sel, 0, "fraction{$i}_min");
    }
    for ($i = 1; $i <= 5; $i++) {
        $var_name = "others$i";
        $$var_name = pg_fetch_result($sel, 0, "others$i");
    }
    $rest1 = intval(pg_fetch_result($sel, 0, "rest1"));
    $rest2 = intval(pg_fetch_result($sel, 0, "rest2"));
    $rest3 = intval(pg_fetch_result($sel, 0, "rest3"));
    //時間給者の休憩時間追加 20100929
    $rest4 = intval(pg_fetch_result($sel, 0, "rest4"));
    $rest5 = intval(pg_fetch_result($sel, 0, "rest5"));
    $rest6 = intval(pg_fetch_result($sel, 0, "rest6"));
    $rest7 = intval(pg_fetch_result($sel, 0, "rest7"));

    $night_workday_count = pg_fetch_result($sel, 0, "night_workday_count");
    $delay_overtime_flg = pg_fetch_result($sel, 0, "delay_overtime_flg");
    $early_leave_time_flg = pg_fetch_result($sel, 0, "early_leave_time_flg");
    //$overtime_40_flg = pg_fetch_result($sel, 0, "overtime_40_flg");
    $modify_flg = pg_fetch_result($sel, 0, "modify_flg");
    $closing = pg_fetch_result($sel, 0, "closing");
    $closing_parttime = pg_fetch_result($sel, 0, "closing_parttime");
    $closing_month_flg = pg_fetch_result($sel, 0, "closing_month_flg");
} else {
    for ($i = 1; $i <= 5; $i++) {
        $var_name = "fraction$i";
        $var_name_min = "fraction{$i}_min";
        $$var_name = "";
        $$var_name_min = "";
    }
    for ($i = 1; $i <= 5; $i++) {
        $var_name = "others$i";
        $$var_name = "";
    }
    $rest1 = 0;
    $rest2 = 0;
    $rest3 = 0;
    //時間給者の休憩時間追加 20100929
    $rest4 = 0;
    $rest5 = 0;
    $rest6 = 0;
    $rest7 = 0;

    $night_workday_count = "";
    $delay_overtime_flg = "1";
    $early_leave_time_flg = "2";
    $modify_flg = "t";
    $closing = "6";
    $closing_parttime = "6";
    $closing_month_flg = "1";
}
//週40時間超過分は計算しないを固定とする 20100525
$overtime_40_flg = "2";

//集計処理の設定を「集計しない」の固定とする 20100910
$others1 = "1";	//勤務時間内に外出した場合
$others2 = "1";	//勤務時間外に外出した場合
$others3 = "1";	//時間給者の休憩の集計
$others4 = "1";	//遅刻・早退に休憩が含まれた場合
$others5 = "2";	//早出残業時間の集計

// 「退勤後復帰」ボタン表示フラグを取得
$sql = "select ret_btn_flg from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query($con, "rolllback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$ret_btn_flg = pg_fetch_result($sel, 0, "ret_btn_flg");

//公休と判定する日設定 20100810
$arr_timecard_holwk_day_std = get_timecard_holwk_day($con, $fname, "");

//グループ毎のテーブルから勤務時間を取得する
$timecard_common_class = new timecard_common_class($con, $fname, $emp_id, $start_date_fulltime, "");

//個人別所定時間を優先するための対応 20120309
$arr_empcond_officehours_ptn = $timecard_common_class->get_empcond_officehours_pattern();
//所定労働時間履歴対応 20121129
$arr_timehist = $calendar_name_class->get_calendarname_time($start_date_fulltime);
$day1_time_fulltime = $arr_timehist["day1_time"];
$arr_timehist = $calendar_name_class->get_calendarname_time($start_date_parttime);
$day1_time_parttime = $arr_timehist["day1_time"];

// チェックされた職員をループ
foreach ($c_emp_id as $tmp_emp_id) {

    // トランザクションの開始
    pg_query($con, "begin");

    //休日出勤と判定する日設定取得 20091222
    $arr_timecard_holwk_day = get_timecard_holwk_day($con, $fname, $tmp_emp_id);

    // ************ oose add start *****************
    // 勤務条件テーブルより勤務形態（常勤、非常勤）、残業管理を取得
    $sql = "select duty_form, no_overtime from empcond";
    $cond = "where emp_id = '$tmp_emp_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    $duty_form = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "duty_form") : "";
    if ($duty_form == 2) {
        //非常勤
        //$start_date = $start_date_parttime;
        //$end_date = $end_date_parttime;
        //非常勤の締め日が設定済なら使用、未設定なら常勤の締め日を使用
        if ($closing_parttime != "") {
            $closing = $closing_parttime;
        }
    } else {
        //常勤
        //$start_date = $start_date_fulltime;
        //$end_date = $end_date_fulltime;
    }
	$closing = $atdbk_close_class->get_empcond_closing($tmp_emp_id);
	$arr_date = $atdbk_close_class->get_term($c_yyyymm, $closing, $closing_month_flg);
	$start_date = $arr_date["start"];
	$end_date = $arr_date["end"];

    //echo "start_date=$start_date end_data=$end_date <br>\n";
    //所属履歴
    $arr_class_hist = get_class_history($con, $fname, $tmp_emp_id, $start_date, $end_date);

    //print_r($arr_class_hist);
    $arr_term_data = array();
    $arr_current_class_info = get_current_class_info($con, $fname, $tmp_emp_id);
    //print_r($arr_current_class_info);

    $cnt_hist = count($arr_class_hist);
    //異動データなしの場合
    if ($cnt_hist == 0) {
        //通常の開始終了期間
        $arr_term_data[0]["start_date"] = $start_date;
        $arr_term_data[0]["end_date"] = $end_date;
        $arr_term_data[0]["class_id"] = $arr_current_class_info["emp_class"];
        $arr_term_data[0]["atrb_id"] = $arr_current_class_info["emp_attribute"];
        $arr_term_data[0]["dept_id"] = $arr_current_class_info["emp_dept"];
        $arr_term_data[0]["room_id"] = $arr_current_class_info["emp_room"];

    }
    else {
        //繰り返す
        $cnt_before = 0;
        $cnt_after = 0;
        //終了日以前の件数
        //終了日より後の件数
        $last_class = 0;

        for ($idx=0; $idx<$cnt_hist; $idx++) {
            $histdate = $arr_class_hist[$idx]["histdate"];
            $wk_date = substr($histdate, 0, 4).substr($histdate, 5, 2).substr($histdate, 8, 2);
            if ($wk_date <= $end_date) {
                $cnt_before++;
            }
            else {
                $cnt_after++;
                break;
            }
        }
        //最新の所属

        //終了日以前の件数がない場合
        if ($cnt_before == 0) {
            $arr_term_data[0]["start_date"] = $start_date;
            $arr_term_data[0]["end_date"] = $end_date;
            $arr_term_data[0]["class_id"] = $arr_current_class_info["emp_class"];
            $arr_term_data[0]["atrb_id"] = $arr_current_class_info["emp_attribute"];
            $arr_term_data[0]["dept_id"] = $arr_current_class_info["emp_dept"];
            $arr_term_data[0]["room_id"] = $arr_current_class_info["emp_room"];
        }
        else {
            //終了日以前の件数がある場合
            //繰り返す
            for ($idx=0; $idx<=$cnt_before; $idx++) {
                //$arr_term_data[$idx]["dept"] = "";
                if ($idx == 0) {
                    $wk_start_date = $start_date;
                }
                else {
                    //履歴データの登録日
                    $histdate = $arr_class_hist[$idx-1]["histdate"];
                    $wk_start_date =  substr($histdate, 0, 4).substr($histdate, 5, 2).substr($histdate, 8, 2);
                }
                //終了日以前のデータの最後で、終了日以降に履歴がある場合
                if ($cnt_before == $idx && $cnt_after == 1) {
                    $wk_end_date = $end_date;
                }
                //履歴がある場合
                else if ($cnt_before >= $idx+1) {
                    //履歴データの前日
                    $histdate = $arr_class_hist[$idx]["histdate"];
                    //echo "histdate=$histdate <br>\n";
                    $wk_next_start_date =  substr($histdate, 0, 4).substr($histdate, 5, 2).substr($histdate, 8, 2);
                    $wk_end_date = last_date($wk_next_start_date);
                }
                //上記以外
                else {
                    $wk_end_date = $end_date;
                }
                //部署情報
                if ($idx < $cnt_before) {
                    $arr_term_data[$idx]["class_id"] = $arr_class_hist[$idx]["class_id"];
                    $arr_term_data[$idx]["atrb_id"] = $arr_class_hist[$idx]["atrb_id"];
                    $arr_term_data[$idx]["dept_id"] = $arr_class_hist[$idx]["dept_id"];
                    $arr_term_data[$idx]["room_id"] = $arr_class_hist[$idx]["room_id"];
                }
                //最後
                else {
                    //終了日以降に履歴あり
                    if ($cnt_after == 1) {
                        $arr_term_data[$idx]["class_id"] = $arr_class_hist[$idx]["class_id"];
                        $arr_term_data[$idx]["atrb_id"] = $arr_class_hist[$idx]["atrb_id"];
                        $arr_term_data[$idx]["dept_id"] = $arr_class_hist[$idx]["dept_id"];
                        $arr_term_data[$idx]["room_id"] = $arr_class_hist[$idx]["room_id"];
                    }
                    else {
                        $arr_term_data[$idx]["class_id"] = $arr_current_class_info["emp_class"];
                        $arr_term_data[$idx]["atrb_id"] = $arr_current_class_info["emp_attribute"];
                        $arr_term_data[$idx]["dept_id"] = $arr_current_class_info["emp_dept"];
                        $arr_term_data[$idx]["room_id"] = $arr_current_class_info["emp_room"];
                    }
                }
                $arr_term_data[$idx]["start_date"] = $wk_start_date;
                $arr_term_data[$idx]["end_date"] = $wk_end_date;

            }
        }
    }
    //print_r($arr_term_data);

    $cnt_hist = count($arr_term_data);

    //2件より多い場合、同じ部署があればまとめる
    //対象日を配列に入れる ["arr_target_date"]
    if ($cnt_hist > 2) {
        $arr_class_chk = array();
        $wk_term_data = array();
        for ($i=0; $i<$cnt_hist; $i++) {
            $chk_key = $arr_term_data[$i]["class_id"]."_".
                $arr_term_data[$i]["atrb_id"]."_".
                $arr_term_data[$i]["dept_id"]."_".
                $arr_term_data[$i]["room_id"];
            //既にある場合
            if ($arr_class_chk[$chk_key] >= "0") {
                $target_idx = $arr_class_chk[$chk_key];
                $arr_target_date = $wk_term_data[$target_idx]["arr_target_date"];
            }
            //最初の場合
            else {
                $target_idx = $i;
                $wk_term_data[$target_idx] = $arr_term_data[$i];
                $arr_class_chk[$chk_key] = $i;
                $arr_target_date = array();
            }
            $tmp_date = $arr_term_data[$i]["start_date"];
            $wk_term_end_date = $arr_term_data[$i]["end_date"];
            while ($tmp_date <= $wk_term_end_date) {
                $arr_target_date[] = $tmp_date;
                $tmp_date = next_date($tmp_date);
            }
            $wk_term_data[$target_idx]["arr_target_date"] = $arr_target_date;
        }
        $arr_term_data = $wk_term_data;
    }
    else {
        for ($i=0; $i<$cnt_hist; $i++) {
            $tmp_date = $arr_term_data[$i]["start_date"];
            $wk_end = $arr_term_data[$i]["end_date"];
            $arr_target_date = array();
            while ($tmp_date <= $wk_end) {
                $arr_target_date[] = $tmp_date;
                $tmp_date = next_date($tmp_date);
            }
            $arr_term_data[$i]["arr_target_date"] = $arr_target_date;
        }
    }

    //for ($i=0; $i<$cnt_hist; $i++) {
    //$arr_ovtm_data = get_ovtm_data($con, $fname, $emp_id, $start_date, $end_date);
    //}
    //debug $arr_class_chk
    //print_r($arr_term_data);
    //print_r($arr_class_chk);
    //exit;

    $no_overtime = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "no_overtime") : "";
    // ************ oose add end *****************
    //一括修正申請情報
    $arr_tmmdapplyall_status = $atdbk_workflow_common_class->get_tmmdapplyall_status($tmp_emp_id, $start_date, $end_date);

    // 締めデータ（日）を削除
    $sql = "delete from wktotalp";
    $cond = "where emp_id = '$tmp_emp_id' and yyyymm = '$c_yyyymm'";
    $del = delete_from_table($con, $sql, $cond, $fname);
    if ($del == 0) {
        pg_query($con, "rolllback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $sql = "delete from wktotald";
    $cond = "where emp_id = '$tmp_emp_id' and yyyymm = '$c_yyyymm'";
    $del = delete_from_table($con, $sql, $cond, $fname);
    if ($del == 0) {
        pg_query($con, "rolllback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    // 締めデータ（月）を削除
    $sql = "delete from atdbkclose";
    $cond = "where emp_id = '$tmp_emp_id' and yyyymm = '$c_yyyymm'";
    $del = delete_from_table($con, $sql, $cond, $fname);
    if ($del == 0) {
        pg_query($con, "rolllback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $sql = "delete from wktotalm";
    $cond = "where emp_id = '$tmp_emp_id' and yyyymm = '$c_yyyymm'";
    $del = delete_from_table($con, $sql, $cond, $fname);
    if ($del == 0) {
        pg_query($con, "rolllback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    // 変則労働適用情報を配列で取得
    $arr_irrg_info = get_irrg_info_array($con, $tmp_emp_id, $fname);

    // 給与支給区分・祝日計算方法を取得
    $sql = "select wage, hol_minus, specified_time from empcond";
    $cond = "where emp_id = '$tmp_emp_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if (pg_num_rows($sel) > 0) {
        $wage = pg_fetch_result($sel, 0, "wage");
        $hol_minus = pg_fetch_result($sel, 0, "hol_minus");
        $paid_specified_time = pg_fetch_result($sel, 0, "specified_time");
    } else {
        $wage = "";
        $hol_minus = "";
        $paid_specified_time = "";
    }

    // 集計結果格納用配列を初期化
    $sums = array_fill(0, 45, 0);
    $paid_sums = array_fill(0, 1, 0);

    // 出勤グループを取得
    $tmcd_group_id = get_timecard_group_id($con, $tmp_emp_id, $fname);

    // 全日付をループ（変則労働時間・週の場合、週の開始曜日からスタート）
    $tmp_date = $start_date;
    $wd = date("w", to_timestamp($tmp_date));
    if ($arr_irrg_info["irrg_type"] == "1") {
        while (($arr_irrg_info["irrg_wd"] == "1" && $wd != 0) || ($arr_irrg_info["irrg_wd"] == "2" && $wd != 1)) {
            $tmp_date = last_date($tmp_date);
            $wd = date("w", to_timestamp($tmp_date));
        }
    } else {
        //変則労働時間・週以外の場合、日曜から
        while ($wd != 0) {
            $tmp_date = last_date($tmp_date);
            $wd = date("w", to_timestamp($tmp_date));
        }
    }
    //データ検索用開始日
    $wk_start_date = $tmp_date;

    //グループ毎のテーブルから勤務時間を取得する
    $timecard_common_class = new timecard_common_class($con, $fname, $tmp_emp_id, $tmp_date, $end_date);

    //個人別所定時間を優先するための対応 20120309
    $arr_weekday_flg = $timecard_common_class->get_arr_weekday_flag($start_date, $end_date);
    $arr_empcond_officehours = $timecard_common_class->get_arr_empcond_officehours($tmp_emp_id, $wk_start_date); //個人別勤務時間帯履歴対応 20130220

    // 勤務時間を取得
    //所定労働時間履歴対応 20121129
    if ($paid_specified_time != "") {
        $day1_time = $paid_specified_time;
    }
    else {
        if ($duty_form == 2) {
            //非常勤
            $day1_time = $day1_time_parttime;
        }
        else {
            //常勤
            $day1_time = $day1_time_fulltime;
        }
    }

    $counter_for_week = 1;
    $holiday_count = 0;

    //翌月初日までデータを取得 20130308
    $wk_end_date_nextone = next_date($end_date);
    // 処理日付の勤務日種別を取得
    $arr_type = array();
    $sql = "select date, type from calendar";
    $cond = "where date >= '$wk_start_date' and date <= '$wk_end_date_nextone'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_array($sel)) {
        $date = $row["date"];
        $arr_type[$date] = $row["type"];
    }
    // 予定データをまとめて取得
    $arr_atdbk = array();
    $sql =	"SELECT ".
        "atdbk.date, ".
        "atdbk.pattern, ".
        "atdbk.reason, ".
        "atdbk.night_duty, ".
        "atdbk.allow_id, ".
        "atdbk.prov_start_time, ".
        "atdbk.prov_end_time, ".
        "atdbk.tmcd_group_id, ".
        "atdptn.workday_count ".
        "FROM ".
        "atdbk ".
        "LEFT JOIN atdptn ON atdbk.tmcd_group_id = atdptn.group_id AND atdbk.pattern = CAST(atdptn.atdptn_id AS varchar) ";
    $cond = "where emp_id = '$tmp_emp_id' and ";
    $cond .= " date >= '$wk_start_date' and date <= '$end_date'";

    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_array($sel)) {
        $date = $row["date"];
        $arr_atdbk[$date]["tmcd_group_id"] = $row["tmcd_group_id"];
        $arr_atdbk[$date]["pattern"] = $row["pattern"];
        $arr_atdbk[$date]["reason"] = $row["reason"];
        $arr_atdbk[$date]["night_duty"] = $row["night_duty"];
        $arr_atdbk[$date]["allow_id"] = $row["allow_id"];
        $arr_atdbk[$date]["prov_start_time"] = $row["prov_start_time"];
        $arr_atdbk[$date]["prov_end_time"] = $row["prov_end_time"];
        $arr_atdbk[$date]["workday_count"] = $row["workday_count"];
    }
    // 実績データをまとめて取得
    $arr_atdbkrslt = array();
    $sql =	"SELECT ".
        "atdbkrslt.*, ".
        "atdptn.workday_count, ".
        "atdptn.base_time, ".
        "atdptn.over_24hour_flag, ".
        "atdptn.out_time_nosubtract_flag, ".
        "atdptn.after_night_duty_flag, ".
        "atdptn.previous_day_flag as atdptn_previous_day_flag, ".
        "atdptn.no_count_flag, ".
        "tmmdapply.apply_id     AS tmmd_apply_id, ".
        "tmmdapply.apply_status AS tmmd_apply_status, ".
        "ovtmapply.apply_id     AS ovtm_apply_id, ".
        "ovtmapply.apply_status AS ovtm_apply_status, ".
        "rtnapply.apply_id      AS rtn_apply_id, ".
        "rtnapply.apply_status  AS rtn_apply_status, ".
        "COALESCE(atdbk_reason_mst.day_count, 0) AS reason_day_count ".
        "FROM ".
        "atdbkrslt ".
        "LEFT JOIN atdptn ON atdbkrslt.tmcd_group_id = atdptn.group_id AND atdbkrslt.pattern = CAST(atdptn.atdptn_id AS varchar) ".
        "LEFT JOIN tmmdapply ON atdbkrslt.emp_id = tmmdapply.emp_id AND CAST(atdbkrslt.date AS varchar) = tmmdapply.target_date AND tmmdapply.delete_flg = false AND tmmdapply.re_apply_id IS NULL ".
        "LEFT JOIN ovtmapply ON atdbkrslt.emp_id = ovtmapply.emp_id AND CAST(atdbkrslt.date AS varchar) = ovtmapply.target_date AND ovtmapply.delete_flg = false AND ovtmapply.re_apply_id IS NULL ".
        "LEFT JOIN rtnapply  ON atdbkrslt.emp_id = rtnapply.emp_id  AND CAST(atdbkrslt.date AS varchar) = rtnapply.target_date AND rtnapply.delete_flg = false AND rtnapply.re_apply_id IS NULL ".
        "LEFT JOIN atdbk_reason_mst ON atdbkrslt.reason = atdbk_reason_mst.reason_id ";
    $cond = "where atdbkrslt.emp_id = '$tmp_emp_id' and ";
    $cond .= " atdbkrslt.date >= '$wk_start_date' and atdbkrslt.date <= '$wk_end_date_nextone'"; //20130308
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    //勤務パターンが1件もない場合の対処 20091214
    $ptn_day_cnt = 0;
    while ($row = pg_fetch_array($sel)) {
        $date = $row["date"];
        //まとめて設定
        $arr_atdbkrslt[$date] = $row;
        /*
        $arr_atdbkrslt[$date]["pattern"] = $row["pattern"];
        $arr_atdbkrslt[$date]["reason"] = $row["reason"];
        $arr_atdbkrslt[$date]["night_duty"] = $row["night_duty"];
        $arr_atdbkrslt[$date]["allow_id"] = $row["allow_id"];
        $arr_atdbkrslt[$date]["start_time"] = $row["start_time"];
        $arr_atdbkrslt[$date]["end_time"] = $row["end_time"];
        $arr_atdbkrslt[$date]["out_time"] = $row["out_time"];
        $arr_atdbkrslt[$date]["ret_time"] = $row["ret_time"];
        for ($i = 1; $i <= 10; $i++) {
        $start_var = "o_start_time$i";
        $end_var = "o_end_time$i";
        $arr_atdbkrslt[$date][$start_var] = $row[$start_var];
        $arr_atdbkrslt[$date][$end_var] = $row[$end_var];
        		}
        $arr_atdbkrslt[$date]["tmcd_group_id"] = $row["tmcd_group_id"];
        $arr_atdbkrslt[$date]["meeting_time"] = $row["meeting_time"];
        $arr_atdbkrslt[$date]["workday_count"] = $row["workday_count"];
        $arr_atdbkrslt[$date]["previous_day_flag"] = $row["previous_day_flag"];
        $arr_atdbkrslt[$date]["next_day_flag"] = $row["next_day_flag"];
        $arr_atdbkrslt[$date]["tmmd_apply_id"] = $row["tmmd_apply_id"];
        $arr_atdbkrslt[$date]["tmmd_apply_status"] = $row["tmmd_apply_status"];
        $arr_atdbkrslt[$date]["ovtm_apply_id"] = $row["ovtm_apply_id"];
        $arr_atdbkrslt[$date]["ovtm_apply_status"] = $row["ovtm_apply_status"];
        $arr_atdbkrslt[$date]["rtn_apply_id"] = $row["rtn_apply_id"];
        $arr_atdbkrslt[$date]["rtn_apply_status"] = $row["rtn_apply_status"];
        $arr_atdbkrslt[$date]["reason_day_count"] = $row["reason_day_count"];
        $arr_atdbkrslt[$date]["base_time"] = $row["base_time"];
        //残業時刻追加 20100114
        $arr_atdbkrslt[$date]["allow_count"] = $row["allow_count"];
        $arr_atdbkrslt[$date]["over_start_time"] = $row["over_start_time"];
        $arr_atdbkrslt[$date]["over_end_time"] = $row["over_end_time"];
        $arr_atdbkrslt[$date]["over_start_next_day_flag"] = $row["over_start_next_day_flag"];
        $arr_atdbkrslt[$date]["over_end_next_day_flag"] = $row["over_end_next_day_flag"];
        //残業時刻2追加 20110616
        $arr_atdbkrslt[$date]["over_start_time2"] = $row["over_start_time2"];
        $arr_atdbkrslt[$date]["over_end_time2"] = $row["over_end_time2"];
        $arr_atdbkrslt[$date]["over_start_next_day_flag2"] = $row["over_start_next_day_flag2"];
        $arr_atdbkrslt[$date]["over_end_next_day_flag2"] = $row["over_end_next_day_flag2"];
        $arr_atdbkrslt[$date]["over_24hour_flag"] = $row["over_24hour_flag"];
        //早出残業 20100601
        $arr_atdbkrslt[$date]["early_over_time"] = $row["early_over_time"];
        //休憩時刻追加 20100921
        $arr_atdbkrslt[$date]["rest_start_time"] = $row["rest_start_time"];
        $arr_atdbkrslt[$date]["rest_end_time"] = $row["rest_end_time"];
        //外出時間を差し引かないフラグ 20110727
        $arr_atdbkrslt[$date]["out_time_nosubtract_flag"] = $row["out_time_nosubtract_flag"];
        //明け追加 20110819
        $arr_atdbkrslt[$date]["after_night_duty_flag"] = $row["after_night_duty_flag"];
        //時間帯画面の前日フラグ 20121120
        $arr_atdbkrslt[$date]["atdptn_previous_day_flag"] = $row["atdptn_previous_day_flag"];
        //勤務時間を計算しないフラグ 20130225
        $arr_atdbkrslt[$date]["no_count_flag"] = $row["no_count_flag"];
        */
        //勤務パターンのある日数をカウント
        if ($arr_atdbkrslt[$date]["pattern"] != "" || $arr_atdbk[$date]["pattern"]) {
            $ptn_day_cnt++;
        }
    }

    //時間有休追加 20111207
    if ($obj_hol_hour->paid_hol_hour_flag == "t") {
        //時間有休データをまとめて取得
        $arr_hol_hour_dat = $obj_hol_hour->get_paid_hol_hour_month($tmp_emp_id, $wk_start_date, $end_date);

    }

    //勤務パターンが1件もない場合、ダイアログ表示
    if ($ptn_day_cnt == 0) {
        // 職員名を取得
        $sql = "select emp_lt_nm, emp_ft_nm from empmst";
        $cond = "where emp_id = '$tmp_emp_id'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
        pg_query($con, "rollback");
        pg_close($con);

        echo("<script type=\"text/javascript\">alert('下記の職員の勤務パターンが1件も登録されていないため、処理を中断します。\\n\\n【職員：{$emp_nm}】');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }

    //法定内勤務、法定内残業、法定外残業
    $hoteinai = 0;
    $hoteinai_zan = 0;
    $hoteigai = 0;

    //支給換算日数 20090908
    $paid_day_count = 0;

    //休日出勤テーブル 2008/10/23
    $arr_hol_work_date = array();

    //日別残業時間20140714
    $arr_zangyo_day = array();
    $arr_normal_over = array(); //普通残業
    $arr_normal_night = array(); //深夜残業
    $arr_hol_over = array(); //休日残業
    $arr_hol_night = array(); //休日深夜残業

    while ($tmp_date <= $end_date) {

        if ($counter_for_week >= 8) {
            $counter_for_week = 1;
        }
        if ($counter_for_week == 1) {
            //前日までの計
            $last_day_total = 0;
            $work_time_for_week = 0;
            if ($arr_irrg_info["irrg_type"] == "1") {
                $holiday_count = 0;
            }
            //1日毎の法定外残を週単位に集計 20090708
            $wk_week_hoteigai = 0;
        }
        $work_time = 0;
        $wk_return_time = 0;
        //内訳
        $normal_over = 0;
        $normal_night = 0;
        $hol_over = 0;
        $hol_night = 0;

        $start_time_info = array();
        $end_time_info = array(); //退勤時刻情報 20100910

        $time3 = 0; //外出時間（所定時間内） 20100910
        $time4 = 0; //外出時間（所定時間外） 20100910
        $tmp_rest2 = 0; //休憩時間 20100910
        $time3_rest = 0; //休憩時間（所定時間内）20100910
        $time4_rest = 0; //休憩時間（所定時間外）20100910
        $arr_effective_time_wk = array(); //休憩を引く前の実働時間 20100915
        $arr_hol_time = array(); // 休日勤務情報 20101224

        $holiday_name = get_holiday_name($tmp_date);

        if ($holiday_name != "") {
            $holiday_count++;
        }

        // 処理日付の勤務日種別を取得
        $type = $arr_type[$tmp_date];

        // 処理日付の勤務予定情報を取得
        $prov_tmcd_group_id = $arr_atdbk[$tmp_date]["tmcd_group_id"];
        $prov_pattern = $arr_atdbk[$tmp_date]["pattern"];
        $prov_reason = $arr_atdbk[$tmp_date]["reason"];
        $prov_night_duty = $arr_atdbk[$tmp_date]["night_duty"];
        $prov_allow_id = $arr_atdbk[$tmp_date]["allow_id"];

        $prov_start_time = $arr_atdbk[$tmp_date]["prov_start_time"];
        $prov_end_time = $arr_atdbk[$tmp_date]["prov_end_time"];
        $prov_start_time = ($prov_start_time != "") ? preg_replace("/(\d{2})(\d{2})/", "$1:$2", $prov_start_time) : "";
        $prov_end_time =  ($prov_end_time != "") ? preg_replace("/(\d{2})(\d{2})/", "$1:$2", $prov_end_time) : "";
        $prov_workday_count = $arr_atdbk[$tmp_date]["workday_count"];

        if ($prov_tmcd_group_id == "") {
            $prov_tmcd_group_id = null;
        }
        if ($prov_allow_id == "") {
            $prov_allow_id = null;
        }
        if ($prov_workday_count == "") {
            $prov_workday_count = null;
        }

        // 処理日付の勤務実績を取得
        $pattern = $arr_atdbkrslt[$tmp_date]["pattern"];
        $reason = $arr_atdbkrslt[$tmp_date]["reason"];
        $night_duty = $arr_atdbkrslt[$tmp_date]["night_duty"];
        $allow_id = $arr_atdbkrslt[$tmp_date]["allow_id"];
        $start_time = $arr_atdbkrslt[$tmp_date]["start_time"];
        $end_time = $arr_atdbkrslt[$tmp_date]["end_time"];
        $out_time = $arr_atdbkrslt[$tmp_date]["out_time"];
        $ret_time = $arr_atdbkrslt[$tmp_date]["ret_time"];

        for ($i = 1; $i <= 10; $i++) {
            $start_var = "o_start_time$i";
            $end_var = "o_end_time$i";
            $$start_var = $arr_atdbkrslt[$tmp_date]["$start_var"];
            $$end_var = $arr_atdbkrslt[$tmp_date]["$end_var"];

            if ($$start_var != "") {
                $$start_var = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $$start_var);
            }
            if ($$end_var != "") {
                $$end_var = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $$end_var);
            }
        }

        $tmcd_group_id = $arr_atdbkrslt[$tmp_date]["tmcd_group_id"];
        $meeting_time = $arr_atdbkrslt[$tmp_date]["meeting_time"];

        $workday_count = $arr_atdbkrslt[$tmp_date]["workday_count"];

        $previous_day_flag = $arr_atdbkrslt[$tmp_date]["previous_day_flag"];
        $next_day_flag = $arr_atdbkrslt[$tmp_date]["next_day_flag"];

        $tmmd_apply_id = $arr_atdbkrslt[$tmp_date]["tmmd_apply_id"];
        $tmmd_apply_status = $arr_atdbkrslt[$tmp_date]["tmmd_apply_status"];
        $ovtm_apply_id = $arr_atdbkrslt[$tmp_date]["ovtm_apply_id"];
        $ovtm_apply_status = $arr_atdbkrslt[$tmp_date]["ovtm_apply_status"];
        $rtn_apply_id = $arr_atdbkrslt[$tmp_date]["rtn_apply_id"];
        $rtn_apply_status = $arr_atdbkrslt[$tmp_date]["rtn_apply_status"];
        $reason_day_count = $arr_atdbkrslt[$tmp_date]["reason_day_count"];
        $base_time = $arr_atdbkrslt[$tmp_date]["base_time"];

        //換算日数
        $wk_workday_count = $workday_count;
        if ($wk_workday_count == "") {
            $wk_workday_count = $prov_workday_count;
        }
        //hhmmをhh:mmに変換している
        if ($start_time != "") {
            $start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $start_time);
        }
        if ($end_time != "") {
            $end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $end_time);
        }
        if ($out_time != "") {
            $out_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $out_time);
        }
        if ($ret_time != "") {
            $ret_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $ret_time);
        }
        if ($tmcd_group_id == "") {
            $tmcd_group_id = null;
        }
        if ($allow_id == "") {
            $allow_id = null;
        }
        if ($workday_count == "") {
            $workday_count = null;
        }
        if ($previous_day_flag == "") {
            $previous_day_flag = 0;
        }
        if ($next_day_flag == "") {
            $next_day_flag = 0;
        }
        //残業時刻追加 20100114
        $over_start_time = $arr_atdbkrslt[$tmp_date]["over_start_time"];
        $over_end_time = $arr_atdbkrslt[$tmp_date]["over_end_time"];
        $over_start_next_day_flag = $arr_atdbkrslt[$tmp_date]["over_start_next_day_flag"];
        $over_end_next_day_flag = $arr_atdbkrslt[$tmp_date]["over_end_next_day_flag"];
        if ($over_start_time != "") {
            $over_start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_start_time);
        }
        if ($over_end_time != "") {
            $over_end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_end_time);
        }
        //残業時刻2追加 20110616
        $over_start_time2 = $arr_atdbkrslt[$tmp_date]["over_start_time2"];
        $over_end_time2 = $arr_atdbkrslt[$tmp_date]["over_end_time2"];
        $over_start_next_day_flag2 = $arr_atdbkrslt[$tmp_date]["over_start_next_day_flag2"];
        $over_end_next_day_flag2 = $arr_atdbkrslt[$tmp_date]["over_end_next_day_flag2"];
        if ($over_start_time2 != "") {
            $over_start_time2 = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_start_time2);
        }
        if ($over_end_time2 != "") {
            $over_end_time2 = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_end_time2);
        }
        $over_24hour_flag = $arr_atdbkrslt[$tmp_date]["over_24hour_flag"];
        //早出残業 20100601
        $early_over_time = $arr_atdbkrslt[$tmp_date]["early_over_time"];
        if ($early_over_time != "") {
            $early_over_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $early_over_time);
        }
        //休憩時刻追加 20100921
        $rest_start_time = $arr_atdbkrslt[$tmp_date]["rest_start_time"];
        $rest_end_time = $arr_atdbkrslt[$tmp_date]["rest_end_time"];
        if ($rest_start_time != "") {
            $rest_start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $rest_start_time);
        }
        if ($rest_end_time != "") {
            $rest_end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $rest_end_time);
        }
        //外出時間を差し引かないフラグ 20110727
        $out_time_nosubtract_flag = $arr_atdbkrslt[$tmp_date]["out_time_nosubtract_flag"];
        //明け追加 20110819
        $after_night_duty_flag = $arr_atdbkrslt[$tmp_date]["after_night_duty_flag"];
        //時間帯画面の前日フラグ 20121120
        $atdptn_previous_day_flag = $arr_atdbkrslt[$tmp_date]["atdptn_previous_day_flag"];
        //勤務時間を計算しないフラグ 20130225
        $no_count_flag = $arr_atdbkrslt[$tmp_date]["no_count_flag"];

        //当直の有効判定
        $night_duty_flag = $timecard_common_class->is_valid_duty($night_duty, $tmp_date, $start_time, $previous_day_flag, $end_time, $next_day_flag, $tmcd_group_id);

        $rslt_workday_count = "";

        if ($tmp_date >= $start_date) {

            /* エラーチェックなしで締めるように変更(再変更に備えコメントとして残します) 20130117
                		// 休暇以外で打刻無しをエラーとする
            			$wk_pattern = $pattern;
            			if ($wk_pattern == "") {
            				$wk_pattern = $prov_pattern;
            			}
            			//換算日数０以外の判断追加 20091222
            			if ($wk_pattern != "" && $wk_pattern != "10" && $wk_workday_count > 0) {
            				if ($start_time == "" || $end_time == "") {
            					// 職員名を取得
            					$sql = "select emp_lt_nm, emp_ft_nm from empmst";
            					$cond = "where emp_id = '$tmp_emp_id'";
            					$sel = select_from_table($con, $sql, $cond, $fname);
            					if ($sel == 0) {
            						pg_query($con, "rollback");
            						pg_close($con);
            						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            						exit;
            					}
            					$emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");

            					pg_query($con, "rollback");
            					pg_close($con);

            					echo("<script type=\"text/javascript\">alert('下記の勤務時間が未設定のため、処理を中断します。\\n\\n【職員：{$emp_nm}、日付：" . preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $tmp_date) . "】');</script>");
            					echo("<script type=\"text/javascript\">history.back();</script>");
            					exit;
            				}
            			}
            */
            //公休か事由なし休暇で残業時刻がありの場合 20100802
            //事由に関わらず休暇で残業時刻がありの場合へ変更 20100916
            if (($pattern == "10" || $after_night_duty_flag == "1") && //明けを追加 20110819
                    $over_start_time != "" && $over_end_time != "") {
                $legal_hol_over_flg = true;
                //残業時刻を設定
                $start_time = $over_start_time;
                $end_time = $over_end_time;

                //法定外残業の基準
                $wk_base_time = 0;
            } else {
                $legal_hol_over_flg = false;
            }

            // 出勤日数をカウント（条件：出勤と退勤の打刻あり、または、「明け」の場合）
            if (($start_time != "" && $end_time != "") || $after_night_duty_flag == "1") {

                //日またがり対応 20101027
                if ($start_time > $end_time &&
                        $previous_day_flag == "0") {
                    $next_day_flag = "1";
                }

                //休暇時は計算しない 20090806
                if ($pattern != "10") {
                    $rslt_workday_count = $workday_count;
                }
                if($night_duty_flag){
                    //曜日に対応したworkday_countを取得する
                    $rslt_workday_count = $rslt_workday_count + $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);
                }
                $sums[1] = $sums[1] + $rslt_workday_count;
                //休日出勤のカウント方法変更 20091222
                if (check_timecard_holwk(
                            $arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason, "", $timecard_bean) == true) {
                    $sums[2] += 1;
                }
            }

            // 事由ベースの日数計算
            $reason_day_count = $atdbk_common_class->get_reason_day_count($reason);
            //reason_day_countは支給換算日数となる 20090908
            if ($reason != "") {
                $paid_day_count += $reason_day_count;
            }
            //時間有休追加 20111207
            $paid_time_hour = 0;
            if ($obj_hol_hour->paid_hol_hour_flag == "t") {
                $paid_hol_min =  $arr_hol_hour_dat[$tmp_date]["calc_minute"];
                $paid_time_hour = $paid_hol_min;
                $paid_sums[0] += $paid_hol_min; //時間有休計
            }

            switch ($reason) {
                case "14":  // 代替出勤
                    $sums[3] += 1;
                    break;
                case "15":  // 振替出勤
                    $sums[4] += 1;
                    break;
                case "65":  // 午前振出
                case "66":  // 午後振出
                    $sums[4] += 0.5;
                    break;
                case "4":  // 代替休暇
                    $sums[5] += 1;
                    break;
                case "18":  // 半前代替休
                case "19":  // 半後代替休
                    $sums[5] += 0.5;
                    break;
                case "17":  // 振替休暇
                    $sums[6] += 1;
                    break;
                case "20":  // 半前振替休
                case "21":  // 半後振替休
                    $sums[6] += 0.5;
                    break;
                case "5":  // 特別休暇
                    $sums[18] += 1;
                    $sums[24] += 1;
                    break;
                case "67":  // 午前特別
                case "68":  // 午後特別
                    $sums[18] += 0.5;
                    $sums[24] += 0.5;
                    break;
                case "58":  // 半特半有
                    $sums[18] += 0.5;
                    $sums[24] += 0.5;
                    $sums[7] += 0.5;
                    break;
                case "59":  // 半特半公
                    $sums[18] += 0.5;
                    $sums[24] += 0.5;
                    $sums[23] += 0.5;
                    break;
                case "60":  // 半特半欠
                    $sums[18] += 0.5;
                    $sums[24] += 0.5;
                    $sums[8] += 0.5;
                    break;
                case "1":  // 有休休暇
                case "37": // 年休
                    if ($pattern == "10") { //20140106 パターンが休暇の場合
                        $sums[7] += 1;
                    }
                    break;
                case "2":  // 午前有休
                case "3":  // 午後有休
                case "38": // 午前年休
                case "39": // 午後年休
                    $sums[7] += 0.5;
                    break;
                case "57":  // 半有半欠
                    $sums[7] += 0.5;
                    $sums[8] += 0.5;
                    break;
                case "6":  // 一般欠勤
                    $sums[8] += 1;
                    $sums[19] += 1;
                    break;
                case "7":  // 病傷欠勤
                    $sums[8] += 1;
                    $sums[20] += 1;
                    break;
                case "48": // 午前欠勤
                case "49": // 午後欠勤
                    $sums[8] += 0.5;
                    $sums[19] += 0.5;
                    break;
                case "8":   // その他休
                    $sums[9] += 1;
                    break;
                case "52":  // 午前その他休
                case "53":  // 午後その他休
                    $sums[9] += 0.5;
                    break;
                case "24":  // 公休
                case "45":  // 希望(公休)
                case "46":  // 待機(公休)
                case "47":  // 管理当直前(公休)
                    $sums[23] += 1;
                    break;
                case "35":  // 午前公休
                case "36":  // 午後公休
                    $sums[23] += 0.5;
                    break;
                case "25":  // 産休
                    $sums[25] += 1;
                    break;
                case "26":  // 育児休業
                    $sums[26] += 1;
                    break;
                case "27":  // 介護休業
                    $sums[27] += 1;
                    break;
                case "28":  // 傷病休職
                    $sums[28] += 1;
                    break;
                case "29":  // 学業休職
                    $sums[29] += 1;
                    break;
                case "30":  // 忌引
                    $sums[30] += 1;
                    break;
                case "31":  // 夏休
                    $sums[31] += 1;
                    break;
                case "50":  // 午前夏休
                case "51":  // 午後夏休
                    $sums[31] += 0.5;
                    break;
                case "54":  // 半夏半公
                    $sums[31] += 0.5;
                    $sums[23] += 0.5;
                    break;
                case "55":  // 半夏半有
                    $sums[31] += 0.5;
                    $sums[7] += 0.5;
                    break;
                case "56":  // 半夏半欠
                    $sums[31] += 0.5;
                    $sums[8] += 0.5;
                    break;
                case "72":  // 半夏半特
                    $sums[31] += 0.5;
                    $sums[18] += 0.5;
                    $sums[24] += 0.5;
                    break;
                case "61":  // 年末年始
                    $sums[42] += 1;	//後で項目追加
                    break;
                case "69":  // 午前正休
                case "70":  // 午後正休
                    $sums[42] += 0.5;
                    break;
                case "62":  // 半正半有
                    $sums[42] += 0.5;
                    $sums[7] += 0.5;
                    break;
                case "63":  // 半正半公
                    $sums[42] += 0.5;
                    $sums[23] += 0.5;
                    break;
                case "64":  // 半正半欠
                    $sums[42] += 0.5;
                    $sums[8] += 0.5;
                    break;
                case "32":   // 結婚休
                    $sums[32] += 1;
                    break;
                case "40":   // リフレッシュ休暇
                    $sums[33] += 1;
                    break;
                case "42":   // 午前リフレッシュ休暇
                case "43":   // 午後リフレッシュ休暇
                    $sums[33] += 0.5;
                    break;
                case "41":  // 初盆休暇
                    $sums[34] += 1;
                    break;
                case "44":  // 半有半公
                    $sums[7] += 0.5;
                    $sums[23] += 0.5;
                    break;
            }
            // 休日となる設定と事由があう場合に公休に計算する 20100810
            // 法定休日
            if ($arr_type[$tmp_date] == "4" && $arr_timecard_holwk_day_std[0]["legal_holiday"] == "t") {
                if ($reason == "22") {
                    $sums[23] += 1;
                }
            }
            // 所定休日
            if ($arr_type[$tmp_date] == "5" && $arr_timecard_holwk_day_std[0]["prescribed_holiday"] == "t") {
                if ($reason == "23") {
                    $sums[23] += 1;
                }
            }
            // 年末年始
            if ($arr_type[$tmp_date] == "7" && $arr_timecard_holwk_day_std[0]["newyear_holiday"] == "t") {
                if ($reason == "61") {
                    $sums[23] += 1;
                }
                if ($reason == "62" || $reason == "63" || $reason == "64") {
                    $sums[23] += 0.5;
                }
            }
        }

        // 各時間帯の開始時刻・終了時刻を変数に格納
        //type:"2勤務日1,3勤務日2"以外は"1通常"とする。"4法定休日,5所定休日,6祝日,7年末年始休暇"
        $tmp_type = ($type != "2" && $type != "3") ? "1" : $type;
        $start2 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start2" );
        $end2 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end2" );
        $start4 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start4" );
        $end4 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end4" );
        $start5 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start5" );
        $end5 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end5" );

        //個人別所定時間を優先する場合 20120309
        if ($arr_empcond_officehours_ptn[$tmcd_group_id][$pattern] == 1) {
            $wk_weekday = $arr_weekday_flg[$tmp_date];
            $arr_empcond_officehours = $timecard_common_class->get_empcond_officehours($wk_weekday, $tmp_date); //個人別勤務時間帯履歴対応 20130220
            if ($arr_empcond_officehours["officehours2_start"] != "") {
                $start2 = $arr_empcond_officehours["officehours2_start"];
                $end2 = $arr_empcond_officehours["officehours2_end"];
                $start4 = $arr_empcond_officehours["officehours4_start"];
                $end4 = $arr_empcond_officehours["officehours4_end"];
            }
        }

        // 所定労働時間帯が確定できない場合、予定時刻を所定労働時間帯として使用
        if (($start2 == "" || $end2 == "") && $prov_start_time != "" && $prov_end_time != "") {
            $start2 = $prov_start_time;
            $end2 = $prov_end_time;
        }
        $over_calc_flg = false;	//残業時刻 20100525
        //残業時刻追加 20100114
        //残業開始が予定終了時刻より前の場合に設定
        if ($over_start_time != "") {
            //残業開始日時
            if ($over_start_next_day_flag == "1") {
                $over_start_date = next_date($tmp_date);
            } else {
                $over_start_date = $tmp_date;
            }

            $over_start_date_time = $over_start_date.$over_start_time;
            $over_calc_flg = true;
            //残業終了時刻 20100705
            $over_end_date = ($over_end_next_day_flag == "1") ? next_date($tmp_date) : $tmp_date;
            $over_end_date_time = $over_end_date.$over_end_time;
        }

        //一括修正のステータスがある場合は設定
        $tmp_status = $arr_tmmdapplyall_status[$tmp_date]["apply_status"];
        if ($tmmd_apply_status == "" && $tmp_status != "") {
            $tmmd_apply_status = $tmp_status;
            $tmmd_apply_id = $arr_tmmdapplyall_status[$tmp_date]["apply_id"];
        }
        // 勤務時間修正情報を取得
        //tmmd_apply_statusからlink_type取得
        $modify_link_type = $timecard_common_class->get_link_type_modify($tmmd_apply_status);
        $modify_apply_id  = $tmmd_apply_id;

        /* エラーチェックなしで締めるように変更 20130117
        		// 申請中・差戻済の場合
        		if ($modify_link_type == "2" || $modify_link_type == "5") {

        			// 職員名を取得
        			$sql = "select emp_lt_nm, emp_ft_nm from empmst";
        			$cond = "where emp_id = '$tmp_emp_id'";
        			$sel = select_from_table($con, $sql, $cond, $fname);
        			if ($sel == 0) {
        				pg_query($con, "rollback");
        				pg_close($con);
        				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        				exit;
        			}
        			$emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");

        			pg_query($con, "rollback");
        			pg_close($con);

        			echo("<script type=\"text/javascript\">alert('下記の勤務時間修正申請が未決裁のため、処理を中断します。\\n\\n【職員：{$emp_nm}、日付：" . preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $tmp_date) . "】');</script>");
        			echo("<script type=\"text/javascript\">history.back();</script>");
        			exit;
        		}
        */
        // 残業情報を取得
        //ovtm_apply_statusからlink_type取得
        $night_duty_time_array  = array();
        $duty_work_time         = 0;
        if ($night_duty_flag){
            //当直時間の取得
            $night_duty_time_array = $timecard_common_class->get_duty_time_array($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);

            //日数換算の数値ｘ通常勤務日の労働時間の時間
            $duty_work_time = date_utils::hi_to_minute($day1_time) * $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);
        }
        $overtime_link_type = $timecard_common_class->get_link_type_overtime($tmp_date, $start_time, $previous_day_flag, $end_time, $next_day_flag, $night_duty, $start2, $end2, $ovtm_apply_status, $tmcd_group_id, $over_start_time, $over_end_time, $early_over_time, $over_24hour_flag, $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2);
        $overtime_apply_id  = $ovtm_apply_id;

        $overtime_approve_flg = $timecard_common_class->get_overtime_approve_flg($overtime_link_type, $modify_flg, $modify_link_type);
        /* エラーチェックなしで締めるように変更 20130117
        		// 残業管理をする場合
        		if ($no_overtime != "t") {
        			// 未申請・申請中・差戻済の場合
        			if ($overtime_link_type == "1" || $overtime_link_type == "2" || $overtime_link_type == "5") {

        				// 職員名を取得
        				$sql = "select emp_lt_nm, emp_ft_nm from empmst";
        				$cond = "where emp_id = '$tmp_emp_id'";
        				$sel = select_from_table($con, $sql, $cond, $fname);
        				if ($sel == 0) {
        					pg_query($con, "rollback");
        					pg_close($con);
        					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        					exit;
        				}
        				$emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");

        				pg_query($con, "rollback");
        				pg_close($con);

        				switch ($overtime_link_type) {
        				case "1":  // 未申請
        					$apply_status_str = "残業が未申請";
        					break;
        				case "2":  // 申請中
        				case "5":  // 差戻済
        					$apply_status_str = "残業申請が未決裁";
        					break;
        				}

        				echo("<script type=\"text/javascript\">alert('下記の{$apply_status_str}のため、処理を中断します。\\n\\n【職員：{$emp_nm}、日付：" . preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $tmp_date) . "】');</script>");
        				echo("<script type=\"text/javascript\">history.back();</script>");
        				exit;
        			}
        		}
        */
        // 退勤後復帰情報を取得
        //rtn_apply_statusからlink_type取得
        $return_link_type = $timecard_common_class->get_link_type_return($ret_btn_flg, $o_start_time1, $rtn_apply_status);
        $return_apply_id  = $rtn_apply_id;

        /* エラーチェックなしで締めるように変更 20130117
        		// 未申請・申請中・差戻済の場合
        		if ($return_link_type == "1" || $return_link_type == "2" || $return_link_type == "5") {

        			// 職員名を取得
        			$sql = "select emp_lt_nm, emp_ft_nm from empmst";
        			$cond = "where emp_id = '$tmp_emp_id'";
        			$sel = select_from_table($con, $sql, $cond, $fname);
        			if ($sel == 0) {
        				pg_query($con, "rollback");
        				pg_close($con);
        				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        				exit;
        			}
        			$emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");

        			pg_query($con, "rollback");
        			pg_close($con);

        			switch ($return_link_type) {
        			case "1":  // 未申請
        				$apply_status_str = "退勤後復帰が未申請";
        				break;
        			case "2":  // 申請中
        			case "5":  // 差戻済
        				$apply_status_str = "退勤後復帰申請が未決裁";
        				break;
        			}

        			echo("<script type=\"text/javascript\">alert('下記の{$apply_status_str}のため、処理を中断します。\\n\\n【職員：{$emp_nm}、日付：" . preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $tmp_date) . "】');</script>");
        			echo("<script type=\"text/javascript\">history.back();</script>");
        			exit;
        		}
        */
        // 所定労働・休憩開始終了日時の取得
        // 所定開始時刻が前日の場合 20121120
        if ($atdptn_previous_day_flag == "1") {
            $tmp_prev_date = last_date($tmp_date);
            $start2_date_time = $tmp_prev_date.$start2;
            //24時間以上勤務の場合 20100203
            if ($over_24hour_flag == "1") {
                $tmp_prev_date = $tmp_date;
            }
            $end2_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_prev_date, $start2, $end2);
        } else {
            $start2_date_time = $tmp_date.$start2;
            //24時間以上勤務の場合 20100203
            if ($over_24hour_flag == "1") {
                $tmp_end_date = next_date($tmp_date);
            } else {
                $tmp_end_date = $tmp_date;
            }
            $end2_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_end_date, $start2, $end2);
        }

        //休憩時刻追加、入力された場合時間帯の設定の代わりに使用 20100921
        if ($rest_start_time != "" && $rest_end_time != "") {
            $start4 = $rest_start_time;
            $end4 = $rest_end_time;
        }
        $start4_date_time = $tmp_date.$start4;
        $end4_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_date, $start4, $end4);

        //外出復帰日時の取得
        $out_date_time = $tmp_date.$out_time;
        $ret_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_date, $out_time, $ret_time);

        // 時間帯データを配列に格納
        $arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end2_date_time);  // 所定労働
        // 前日出勤フラグ、翌日退勤フラグを考慮した休憩時間配列を取得
        // 24時間超える場合の翌日の休憩は配列にいれない 20101027
        if ($start2 < $start4) {
            $arr_time4 = $timecard_common_class->get_minute_time_array2($start4_date_time, $end4_date_time, $previous_day_flag, "");  // 休憩
        } else {
            $arr_time4 = $timecard_common_class->get_minute_time_array2($start4_date_time, $end4_date_time, $previous_day_flag, $next_day_flag);  // 休憩
        }

        //時間帯情報 休憩、深夜、前日当日翌日分の開始終了時刻を保持 20110126
        if ($tmp_date >= $start_date) {
            $work_times_info = $timecard_common_class->get_work_times_info($tmp_date, $start4, $end4, $start5, $end5);
        }
        // 処理当日の所定労働時間を求める
        if ($holiday_name != "" && $hol_minus == "t") {  // 祝日かつ祝日減算設定
            $specified_time = 0;
            //		} else if ($wage == "4" && $others3 == "2") {  // 時間給者かつ休憩を実働加算 集計処理の設定を「集計しない」の固定とする 20100910
            //			$specified_time = count($arr_time2);
        } else {
            $specified_time = count(array_diff($arr_time2, $arr_time4));
        }

		//出勤退勤時刻がない場合でも残業時刻がある場合は所定時刻で勤務時間を計算する 20141027 start
		if ($over_start_time != "" && $over_end_time != "") {
			if ($start_time == "") {
				$start_time = $start2;
				$previous_day_flag = $atdptn_previous_day_flag; //時間帯設定の前日フラグ
			}
			if ($end_time == "") {
				$end_time = $end2;
				//日またがり対応
				if ($start_time > $end_time &&
						$previous_day_flag == "0") {
					$next_day_flag = "1";
				}
			}
		}
		//出勤退勤時刻がない場合でも残業時刻がある場合は所定時刻で勤務時間を計算する 20141027 end
        // 出退勤とも打刻済みの場合
        //勤務時間を計算しないフラグが"1"以外 20130225
        if ($start_time != "" && $end_time != "" && $no_count_flag != "1") {

            $start_date_time = $timecard_common_class->get_timecard_start_date_time($tmp_date, $start_time, $previous_day_flag);
            $end_date_time   = $timecard_common_class->get_timecard_end_date_time($tmp_date, $end_time, $next_day_flag);

            // 出勤〜退勤までの時間を配列に格納
            $arr_tmp_work_time = $timecard_common_class->get_minute_time_array($start_date_time, $end_date_time);

            // ここまでの処理で所定労働時間帯が未確定の場合、勤務実績で上書き
            if (empty($start2_date_time) || empty($end2_date_time)) {
                $start2_date_time = $start_date_time;
                $end2_date_time    = $end_date_time;
            }

            //出勤同時刻退勤または、出勤時間＞退勤時間の場合は処理を行わない
            if (count($arr_tmp_work_time) > 0){
                // 時間帯データを配列に格納
                $arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end2_date_time);  // 所定労働
                //休日で残業がある場合 20101028
                if ($pattern == "10" && $over_start_time != "" && $over_end_time != "") {
                    //深夜残業の開始終了時間
                    $start5 = "2200";
                    $end5 = "0500";
                }
                //前日・翌日の深夜帯も追加する
                $arr_time5 = $timecard_common_class->get_late_night_shift_array($tmp_date, $start5, $end5);  // 深夜帯

                // 確定出勤時刻／遅刻回数を取得
                // 勤務パターンで時刻指定がない場合でも、端数処理する
                if ($start2 == "" && $end2 == "" && $fraction1 > "1") {
                    //端数処理する分
                    $moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
                    //基準日時
                    $tmp_office_start_time = substr($start_date_time, 0, 10) . "00";
                    $fixed_start_time = date_utils::move_time($tmp_office_start_time, $start_date_time, $moving_minutes, $fraction1);
                    $start_time_info = array();
                } else {
                    $start_time_info = $timecard_common_class->get_start_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction1, $fraction1_min, $fraction3, $others4, $others5);
                    $fixed_start_time = $start_time_info["fixed_time"];
                }

                // 確定退勤時刻／早退回数を取得
                // 勤務パターンで時刻指定がない場合でも、端数処理する
                if ($start2 == "" && $end2 == "" && $fraction2 > "1") {
                    //端数処理する分
                    $moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
                    //基準日時
                    $tmp_office_start_time = substr($start_date_time, 0, 10) . "00";
                    $fixed_end_time = date_utils::move_time($tmp_office_start_time, $end_date_time, $moving_minutes, $fraction2);
                    $end_time_info = array();
                } else {
                    //残業時刻が入っている場合は、端数処理しない 20100806
                    $wk_over_end_date_time = str_replace(":", "", $over_end_date_time); //20111128
                    if ($over_end_time != "" && $wk_over_end_date_time > $end2_date_time && $end_date_time >= $end2_date_time) { //時間が所定終了よりあとか確認20111128 //早退の場合を除く条件追加 20140902
                        $fixed_end_time = $wk_over_end_date_time;
                    } else {
                        $end_time_info = $timecard_common_class->get_end_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction2, $fraction2_min, $fraction3, $others4);
                        $fixed_end_time = $end_time_info["fixed_time"];
                    }
                    //※残業申請未承認の場合と（残業申請不要かつ残業管理をする場合）に退勤時刻を所定にする 20100721
                    //残業申請画面を表示しないは除く 20100811
                    if ($timecard_bean->over_time_apply_type != "0") {
                        if ($overtime_approve_flg == "1" || ($overtime_approve_flg == "0" && $no_overtime != "t")) {
                            //退勤時刻が所定よりあとの場合設定 20101224
                            if ($fixed_end_time > $end2_date_time) {
                                $fixed_end_time = $end2_date_time;
                            }
                        }
                    }
                }

                //時間有休がある場合は、遅刻早退を計算しない 20111207
                if ($obj_hol_hour->paid_hol_hour_flag == "t" && $paid_time_hour > 0) {
                    //時間有休と相殺する場合
                    if ($ovtm_class->sousai_sinai_flg != "t") {
                        //遅刻
                        $tikoku_time = 0;
                        $start_time_info["diff_minutes"] = 0;
                        $start_time_info["diff_count"] = 0;
                        //早退
                        $sotai_time = 0;
                        $end_time_info["diff_minutes"] = 0;
                        $end_time_info["diff_count"] = 0;
                    }
                }
                if ($tmp_date >= $start_date) {
                    //遅刻
                    $sums[10] += $start_time_info["diff_count"];
                    $sums[12] += $start_time_info["diff_minutes"];
                }
                if ($tmp_date >= $start_date) {
                    //早退
                    $sums[11] += $end_time_info["diff_count"];
                    $sums[13] += $end_time_info["diff_minutes"];
                }

                // 実働時間・所定労働時間を配列に格納（休憩を除く）・有休時間を算出
                switch ($reason) {
                    case "2":  // 午前有休
                    case "38": // 午前年休
                        // 残業時間が正しく計算されないため、休憩終了時間設定をしない 20100330
                        //if (in_array($fixed_start_time, $arr_time2)) {
                        $tmp_start_time = $fixed_start_time;
                        //} else {
                        //	$tmp_start_time = $end4_date_time;
                        //}
                        $tmp_end_time = $fixed_end_time;
                        $arr_effective_time = $timecard_common_class->get_minute_time_array($tmp_start_time, $tmp_end_time);

                        $arr_specified_time = $arr_time2;

                        //$paid_time = date_utils::get_time_difference($start2_date_time, $start4_date_time);
                        //休憩を引く前の実働時間
                        $arr_effective_time_wk = $arr_effective_time;

                        break;
                    case "3":  // 午後有休
                    case "39": // 午後年休
                        $tmp_start_time = $fixed_start_time;
                        // 残業時間が正しく計算されないため、休憩開始時間設定をしない 20100330
                        //if (in_array($fixed_end_time, $arr_time2)) {
                        $tmp_end_time = $fixed_end_time;
                        //} else {
                        //	$tmp_end_time = $start4_date_time;
                        //}
                        $arr_effective_time = $timecard_common_class->get_minute_time_array($tmp_start_time, $tmp_end_time);

                        $arr_specified_time = $arr_time2;

                        //$paid_time = date_utils::get_time_difference($end4_date_time, $end2_date_time);
                        //休憩を引く前の実働時間
                        $arr_effective_time_wk = $arr_effective_time;

                        break;
                    default:
                        $tmp_start_time = $fixed_start_time;
                        $tmp_end_time = $fixed_end_time;
                        $arr_effective_time = $timecard_common_class->get_minute_time_array($tmp_start_time, $tmp_end_time);
                        //休憩を引く前の実働時間
                        $arr_effective_time_wk = $arr_effective_time;
                        if ($wage != "4" || ($start4 != "" && $end4 != "")) { //休憩時刻がある場合を追加 20100925
                            $arr_effective_time = array_diff($arr_effective_time, $arr_time4);
                        }

                        $arr_specified_time = array_diff($arr_time2, $arr_time4);

                        //$paid_time = 0;

                        break;
                }

                //残業時刻追加対応 20100114
                //予定終了時刻から残業開始時刻の間を除外する
                if ($over_start_time != "") {
                    $wk_end_date_time = $end2_date_time;

                    $arr_jogai = array();
                    /* 20100713
                    //基準日時
                    $tmp_office_start_time = substr($end2_date_time, 0, 10) . "00";
                    if ($fraction1 > "1") {
                    	//端数処理する分
                    	$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
                    	$over_start_date_time = date_utils::move_time($tmp_office_start_time, $over_start_date_time, $moving_minutes, $fraction1);
                    }
                    if ($fraction2 > "1") {
                    	//端数処理する分
                    	$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
                    	$wk_end_date_time = date_utils::move_time($tmp_office_start_time, $wk_end_date_time, $moving_minutes, $fraction2);
                    }
                    */
                    //残業開始時刻があとの場合
                    if (date_utils::to_timestamp_from_ymdhi($over_start_date_time) > date_utils::to_timestamp_from_ymdhi($wk_end_date_time)) {
                        //時間配列を取得する（休憩扱い）
                        $arr_jogai = $timecard_common_class->get_minute_time_array($wk_end_date_time, $over_start_date_time);
                        //休憩扱いの時間配列を除外する
                        $arr_effective_time = array_diff($arr_effective_time, $arr_jogai);
                    }

                }

                //休日勤務時間（分）と休日出勤を取得 2008/10/23
                if ($tmp_date >= $start_date) {
                    //休暇時は計算しない 20090806
                    //if ($pattern != "10") {
                    //休日出勤の判定方法変更 20100721
                    if (check_timecard_holwk(
                                $arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason) == true) {
                        $arr_hol_time = $timecard_common_class->get_holiday_work($arr_type, $tmp_date, $arr_effective_time, $start_time, $previous_day_flag, $end_time, $next_day_flag, $reason);
                        //$sums[39] += $arr_hol_time[3]; //20110126

                        // 休日出勤した日付を配列へ設定
                        for ($wk_idx=4; $wk_idx<7; $wk_idx++) {
                            if ($arr_hol_time[$wk_idx] != "") {
                                $wk_type = $arr_type[$arr_hol_time[$wk_idx]]["type"];
                                //							$wk_term_index = $arr_date[$arr_hol_time[$wk_idx]]["term_index"];

                                //echo("<br>t=$wk_type d=".$arr_hol_time[$wk_idx]);
                                $arr_hol_work_date[$arr_hol_time[$wk_idx]]["type"] = $wk_type;
                                //							$arr_hol_work_date[$arr_hol_time[$wk_idx]]["term_index"] = $wk_term_index;
                            }
                        }
                    }
                }
                //当直の場合当直時間を計算から除外する
                if ($night_duty_flag){
                    //稼働 - (当直 - 所定労働)
                    $arr_effective_time = array_diff($arr_effective_time, array_diff($night_duty_time_array, $arr_time2));
                }

                // 普外時間を算出（分単位）
                // 外出時間を差し引かないフラグ対応 20110727
                if ($out_time_nosubtract_flag != "1") {
                    $arr_out_time = $timecard_common_class->get_minute_time_array($out_date_time, $ret_date_time);
                    $time3 = count(array_intersect($arr_out_time, $arr_specified_time));
                    //echo("time3=$time3");

                    // 残外時間を算出（分単位）
                    $time4 = count(array_diff($arr_out_time, $arr_time2)); //$arr_specified_timeから変更、休憩も除くため 20100910
                }
                // 実働時間を算出（分単位）
                $effective_time = count($arr_effective_time);
                if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
                    //休日残業で残業申請中は休憩を表示しない 20100916
                    if (!($legal_hol_over_flg /*&& $overtime_approve_flg == "1"*/)) {
                        if ($effective_time > 1200) {  // 20時間を超える場合 20100929
                            $effective_time -= $rest7;
                            $time3_rest = 0;
                            $time4_rest = $rest7;
                        } else if ($effective_time > 720) {  // 12時間を超える場合
                            $effective_time -= $rest6;
                            $time3_rest = 0;
                            $time4_rest = $rest6;
                        } else if ($effective_time > 540) {  // 9時間を超える場合
                            $effective_time -= $rest3;
                            $time3_rest = 0;
                            $time4_rest = $rest3;
                        } else if ($effective_time > 480) {  // 8時間を超える場合
                            $effective_time -= $rest2;
                            $time3_rest = $rest2;
                            $time4_rest = 0;
                        } else if ($effective_time > 360) {  // 6時間を超える場合
                            $effective_time -= $rest1;
                            $time3_rest = $rest1;
                            $time4_rest = 0;
                        } else if ($effective_time > 240 && $start_time < "12:00") {  // 4時間超え午前開始の場合
                            $effective_time -= $rest4;
                            $time3_rest = $rest4;
                            $time4_rest = 0;
                        } else if ($effective_time > 240 && $start_time >= "12:00") {  // 4時間超え午後開始の場合
                            $effective_time -= $rest5;
                            $time3_rest = $rest5;
                            $time4_rest = 0;
                        }
                    }
                    //休日で残業がない場合,出勤退勤がある場合 20100917
                    if ($pattern == "10" && $over_start_time == "" && $over_end_time == "") {
                        $time3_rest = 0;
                        $time4_rest = 0;
                    }
                    //開始した日が休日の場合に休日勤務から休憩を減算 2008/10/23
                    //if (($previous_day_flag == true && $arr_hol_time[0] > 0) ||
                    //	($previous_day_flag == false && $arr_hol_time[1] > 0)) {
                    //	$sums[39] = $sums[39] - $time_rest;
                    //}
                } else {
                    // 所定労働内の休憩時間
                    $tmp_rest1 = count(array_intersect($arr_time2, $arr_time4));
                    // 実働時間内の休憩時間
                    $tmp_rest2 = count(array_intersect($arr_effective_time_wk, $arr_time4));
                    //休憩時間前後の遅刻早退対応 20100925
                    if ($tmp_rest1 > $tmp_rest2) {
                        $tmp_rest2 = $tmp_rest1;
                    }
                }

                //古い仕様のため廃止 20100330
                /*
                // 代替出勤または振替出勤の場合、実働時間から所定労働時間を減算
                if ($reason == "14" || $reason == "15") {
                	$effective_time -= count($arr_specified_time);
                }
                */
                // 稼働時間を算出（分単位）
                //$work_time = $effective_time + $paid_time - $time3 - $time4;
                // 半年休の時間をプラスしない 20100330
                $work_time = $effective_time - $time3 - $time4;

                //休暇時は計算しない 20090806 公休か事由なし休暇で残業時刻が設定済を条件追加 20100802
                if ($pattern == "10" && !$legal_hol_over_flg) {
                    $work_time = 0;
                }

                // 退勤後復帰回数・時間を算出
                $return_count = 0;
                $return_time = 0;
                $return_late_time = 0; //20100715
                for ($i = 1; $i <= 10; $i++) {
                    $start_var = "o_start_time$i";
                    if ($$start_var == "") {
                        break;
                    }
                    $return_count++;

                    $end_var = "o_end_time$i";
                    if ($$end_var == "") {
                        break;
                    }

                    //端数処理 20090709
                    if ($fraction1 > "1") {
                        //端数処理する分
                        $moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
                        //開始日時
                        $tmp_start_date_time = $timecard_common_class->get_schedule_end_date_time(substr($end_date_time,0,8), substr($end_date_time,8,4), $$start_var);
                        //基準日時
                        $tmp_base_date_time = substr($tmp_start_date_time,0,10)."00";
                        $tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_start_date_time, $moving_minutes, $fraction1);
                        $tmp_ret_start_time = substr($tmp_date_time, 8, 4);
                    } else {
                        $tmp_ret_start_time = str_replace(":", "", $$start_var);
                    }
                    if ($fraction2 > "1") {
                        //端数処理する分
                        $moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
                        //終了日時
                        $tmp_end_date_time = $timecard_common_class->get_schedule_end_date_time(substr($end_date_time,0,8), substr($end_date_time,8,4), $$end_var);
                        //基準日時
                        $tmp_base_date_time = substr($tmp_end_date_time,0,10)."00";
                        $tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_end_date_time, $moving_minutes, $fraction2);

                        $tmp_ret_end_time = substr($tmp_date_time, 8, 4);
                    } else {
                        $tmp_ret_end_time = str_replace(":", "", $$end_var);
                    }
                    $arr_ret = $timecard_common_class->get_return_array($end_date_time, $tmp_ret_start_time, $tmp_ret_end_time);

                    $return_time += count($arr_ret);
                    /* //内訳、使用する場合はコメント解除
                    $next_date = next_date($tmp_date);
                    $tmp_ret_end_date = ($tmp_ret_start_time > $tmp_ret_end_time) ? $next_date : $tmp_date;
                    $arr_overtime_data2 = get_overtime_data($work_times_info, $tmp_date, $pattern, $reason, $arr_timecard_holwk_day, $arr_type, $timecard_common_class, $tmp_date.$tmp_ret_start_time, $tmp_ret_end_date.$tmp_ret_end_time, $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], $timecard_bean, $ovtm_class);
                    $normal_over += $arr_overtime_data2["normal_over"];
                    $normal_night += $arr_overtime_data2["late_night"];
                    $hol_over += $arr_overtime_data2["hol_over"];
                    $hol_night += $arr_overtime_data2["hol_late_night"];
                    */
                    // 月の深夜勤務時間に退勤後復帰の深夜分を追加
                    //$return_late_time += count(array_intersect($arr_ret, $arr_time5)); //20100715
                    //計算方法変更 20110126
                    //深夜残業
                    $wk_ret_start = $tmp_date.$tmp_ret_start_time;
                    $wk_ret_end = $timecard_common_class->get_schedule_end_date_time($tmp_date, $tmp_ret_start_time, $tmp_ret_end_time);
                    $wk_late_night = $timecard_common_class->get_late_night($work_times_info, $wk_ret_start,$wk_ret_end, "");
                    $return_late_time += $wk_late_night;
                    //当日が休日の場合、休日に合計。
                    if (($pattern != "10" && check_timecard_holwk(
                                    $arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason, "") == true) ||
                            $reason == "16" ||
                            $pattern == "10") {
                        //当日の残業
                        $today_over = $timecard_common_class->get_day_time($work_times_info, $wk_ret_start,$wk_ret_end, "1");
                        //事由が普通残業の場合を除く 20110128
                        if ($reason != "71") {
                            $sums[44] += $today_over;
                        }
                    }
                    //翌日の残業
                    $next_date = next_date($tmp_date);
                    if (check_timecard_holwk(
                                $arr_timecard_holwk_day, $arr_type[$next_date], $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], "1") == true) { //20130308
                        $nextday_over = $timecard_common_class->get_day_time($work_times_info, $wk_ret_start,$wk_ret_end, "2");
                        //事由が普通残業の場合を除く 20110128
                        if ($reason != "71") {
                            $sums[44] += $nextday_over;
                        }
                    }
                }
                $wk_return_time = $return_time;

                // 前日までの計 = 週計
                $last_day_total = $work_time_for_week;
                $work_time_for_week += $work_time;
                $work_time_for_week += $wk_return_time;
                if ($tmp_date < $start_date) {
                    $tmp_date = next_date($tmp_date);
                    $counter_for_week++;
                    continue;
                }

                //当直の場合稼働時間に日数換算の数値ｘ通常勤務日の労働時間の時間を追加（当直の時間を除外して計算している為)
                if ($night_duty_flag){
                    $sums[15] += $duty_work_time;
                }

                // 深夜勤務時間を算出（分単位）
                $time2 = 0;
                //計算方法変更 20110126
                //残業承認時に計算、申請画面を表示しない場合は無条件に計算
                if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
                        $timecard_bean->over_time_apply_type == "0") {
                    //残業時刻がない場合、計算しない（以前は所定終了を使用していた）
                    if ($over_start_time == "" || $over_end_time == "") {
                        $over_start_date_time = "";
                        $over_end_date_time = "";
                    }
                    //深夜残業
                    $time2 = $timecard_common_class->get_late_night($work_times_info, $over_start_date_time,$over_end_date_time, "");
                    //休憩対応 20130827
                    if ($time2 > 0 && $start4 != "" && $end4 != "") {
                        $wk_rest_time = $timecard_common_class->get_intersect_times($over_start_date_time, $over_end_date_time, $start4_date_time, $end4_date_time);
                        if ($wk_rest_time > 0) {
                            $late_night_info = $timecard_common_class->get_late_night_shift_info($tmp_date, $start5, $end5);  // 深夜帯情報
                            //深夜時間帯か再確認
                            $wk_s1 = max($over_start_date_time, $start4_date_time);
                            $wk_e1 = min($over_end_date_time, $end4_date_time);
                            $wk_rest_time = $timecard_common_class->get_times_info_calc($late_night_info, $wk_s1, $wk_e1);
                            $time2 -= $wk_rest_time;
                        }
                    }
                    //休憩対応 20140710
                    $rest_start_time1 = $arr_atdbkrslt[$tmp_date]["rest_start_time1"];
                    $rest_end_time1 = $arr_atdbkrslt[$tmp_date]["rest_end_time1"];
                    if ($rest_start_time1 != "" && $rest_end_time1 != "") {
                        $rest_start_date1 = ($arr_atdbkrslt[$tmp_date]["rest_start_next_day_flag1"] == 1) ? next_date($tmp_date) :$tmp_date;
                        $rest_end_date1 = ($arr_atdbkrslt[$tmp_date]["rest_end_next_day_flag1"] == 1) ? next_date($tmp_date) :$tmp_date;
                        $rest_start_date_time1 = $rest_start_date1.$rest_start_time1;
                        $rest_end_date_time1 = $rest_end_date1.$rest_end_time1;
                    }
                    else {
                        $rest_start_date_time1 = "";
                        $rest_end_date_time1 = "";
                    }
                    if ($time2 > 0 && $rest_start_time1 != "" && $rest_end_time1 != "") {
                        $next_date = next_date($tmp_date);
                        $arr_resttime_data = get_overtime_data($work_times_info, $tmp_date, $pattern, $reason, $arr_timecard_holwk_day, $arr_type, $timecard_common_class, $rest_start_date_time1, $rest_end_date_time1, $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], $timecard_bean, $ovtm_class);
                        //深夜内の休憩
                        $overtime_rest_night = $arr_resttime_data["late_night"] + $arr_resttime_data["hol_late_night"];
                        $time2 -= $overtime_rest_night;
                        //echo "$tmp_date $rest_start_date_time1 $rest_end_date_time1 $time2 $overtime_rest_night<br>\n";
                    }
                    //休日残業
                    //当日
                    if (($pattern != "10" && check_timecard_holwk(
                                    $arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason, "") == true) ||
                            $reason == "16" ||
                            ($pattern == "10" && $over_start_time != "" && $over_end_time != "" &&
                                $overtime_approve_flg != "1") //休暇で残業時刻がある場合
                        ) {
                        $today_over = $timecard_common_class->get_day_time($work_times_info, $over_start_date_time,$over_end_date_time, "1");
                        //休憩時間
                        if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合
                            //休憩を減算
                            //$today_over = $today_over - $time3_rest - $time4_rest; //コメント化20130822
                            ;
                        } else {
                            //残業開始時刻が所定開始時刻と同じ場合で休憩がある場合は減算
                            if ($start2 != "" && $start2 == $over_start_time && $over_start_next_day_flag != "1"
                                    && $tmp_rest2 > 0) {
                                $today_over -= $tmp_rest2;
                            }
                            //休憩時間確認
                            else if ($start4 != "" && $end4 != "") {
                                $wk_kyukei = $timecard_common_class->get_intersect_times($over_start_date_time, $over_end_date_time, $start4_date_time, $end4_date_time);
                                $today_over -= $wk_kyukei;

                            }
                        }
                        //休憩対応 20140715
                        if ($rest_start_time1 != "" && $rest_end_time1 != "") {
                            $wk_kyukei = $timecard_common_class->get_day_time($work_times_info, $rest_start_date_time1, $rest_end_date_time1, "1");
                            $today_over -= $wk_kyukei;
                        }
                        //事由が普通残業の場合を除く 20110128
                        if ($reason != "71") {
                            $sums[44] += $today_over;
                        }
                    }
                    //翌日フラグ
                    if ($next_day_flag == "1" || $over_end_next_day_flag == "1") {
                        $next_date = next_date($tmp_date);
                        if (check_timecard_holwk(
                                    $arr_timecard_holwk_day, $arr_type[$next_date], $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], "1") == true) { //20130308
                            $nextday_over = $timecard_common_class->get_day_time($work_times_info, $over_start_date_time,$over_end_date_time, "2");
                            //休憩対応 20140715
                            if ($rest_start_time1 != "" && $rest_end_time1 != "") {
                                $wk_kyukei = $timecard_common_class->get_day_time($work_times_info, $rest_start_date_time1, $rest_end_date_time1, "2");
                                $nextday_over -= $wk_kyukei;
                            }
                            //事由が普通残業の場合を除く 20110128
                            if ($reason != "71") {
                                $sums[44] += $nextday_over;
                            }
                        }
                    }

                    //残業３−５を追加、２−５の繰り返し処理とする
                    //残業２
                    for ($idx=2; $idx<=5; $idx++) {
                        $s_t = "over_start_time".$idx;
                        $e_t = "over_end_time".$idx;
                        $s_f = "over_start_next_day_flag".$idx;
                        $e_f = "over_end_next_day_flag".$idx;
                        $over_start_time_value = $arr_atdbkrslt[$tmp_date][$s_t];
                        $over_end_time_value = $arr_atdbkrslt[$tmp_date][$e_t];
                        $over_start_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$s_f];
                        $over_end_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$e_f];
                        if ($over_start_time_value != "" && $over_end_time_value != "") {
                            //開始日時
                            $over_start_date_time_value = ($over_start_next_day_flag_value == 1) ? next_date($tmp_date).$over_start_time_value : $tmp_date.$over_start_time_value;
                            //終了日時
                            $over_end_date_time_value = ($over_end_next_day_flag_value == 1) ? next_date($tmp_date).$over_end_time_value : $tmp_date.$over_end_time_value;
                            //深夜時間帯の時間取得
                            $over_time2_late_night = $timecard_common_class->get_late_night($work_times_info, $over_start_date_time_value, $over_end_date_time_value, "");

                            //休憩除外
                            $rs_t = "rest_start_time".$idx;
                            $re_t = "rest_end_time".$idx;
                            $rs_f = "rest_start_next_day_flag".$idx;
                            $re_f = "rest_end_next_day_flag".$idx;
                            $rest_start_time_value = $arr_atdbkrslt[$tmp_date][$rs_t];
                            $rest_end_time_value = $arr_atdbkrslt[$tmp_date][$re_t];
                            $rest_start_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$rs_f];
                            $rest_end_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$re_f];

                            //残業時間深夜内休憩
                            $overtime_rest_night = 0;

                            if ($rest_start_time_value != "" && $rest_end_time_value != "") {
                                $rest_start_date_value = ($arr_atdbkrslt[$tmp_date][$rs_f] == 1) ? next_date($tmp_date) :$tmp_date;
                                $rest_end_date_value = ($arr_atdbkrslt[$tmp_date][$re_f] == 1) ? next_date($tmp_date) :$tmp_date;
                                $rest_start_date_time_value = $rest_start_date_value.$rest_start_time_value;
                                $rest_end_date_time_value = $rest_end_date_value.$rest_end_time_value;
                                $arr_resttime_data = get_overtime_data($work_times_info, $tmp_date, $pattern, $reason, $arr_timecard_holwk_day, $arr_type, $timecard_common_class, $rest_start_date_time_value, $rest_end_date_time_value, $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], $timecard_bean, $ovtm_class);

                                //深夜内の休憩
                                $overtime_rest_night =
                                    $arr_resttime_data["late_night"]
                                    + $arr_resttime_data["hol_late_night"];
                                $over_time2_late_night -= $overtime_rest_night;
                            }
                            $time2 += $over_time2_late_night;
                        }
                    }
                }
                $time2 += $return_late_time; //20100715
                $sums[43] += $time2;

                // 登録値の編集
                $time1 = minute_to_hmm($time1);
                $time2 = minute_to_hmm($time2);
                $wk_time3 = minute_to_hmm($time3); // wk_time3:登録用
                $wk_time4 = minute_to_hmm($time4);
                if ($return_count == 0) {$return_count = "";}
                $return_time = minute_to_hmm($return_time);

                // 遅刻・早退フラグの設定
                $late_flg = ($start_time_info["diff_count"] == 0) ? "f" : "t";
                $early_flg = ($end_time_info["diff_count"] == 0) ? "f" : "t";
            } else {
                // 出勤同時刻退勤または、出勤時間＞退勤時間の場合で
                // 月の開始日前は処理しない
                if ($tmp_date < $start_date) {
                    $tmp_date = next_date($tmp_date);
                    $counter_for_week++;
                    continue;
                }
            }
        } else {
            $time1 = "";
            $time2 = "";
            $time3 = "";
            $time4 = "";
            $wk_time3 = "";
            $wk_time4 = "";
            $return_count = "";
            $return_time = "";

            // 遅刻・早退フラグの設定
            if ($start_time != "") {
                if ($end2 == "") {
                    $late_flg = "f";
                } else {
                    $arr_tmp_work_time = create_time_array($start_time, $end2);
                    switch ($reason) {
                        case "2":
                            $arr_time2 = create_time_array($end4, $end2);
                            break;
                        case "3":
                            $arr_time2 = create_time_array($start2, $start4);
                            break;
                        default:
                            $arr_time2 = create_time_array($start2, $end2);
                            break;
                    }
                    $start_time_info = get_start_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction1, $fraction1_min, $fraction3, $others4, $others5);
                    $late_flg = ($start_time_info["diff_count"] == 0) ? "f" : "t";
                }
            } else if ($end_time != "") {
                if ($start2 == "") {
                    $early_flg = "f";
                } else {
                    $arr_tmp_work_time = create_time_array($start2, $end_time);
                    switch ($reason) {
                        case "2":
                            $arr_time2 = create_time_array($end4, $end2);
                            break;
                        case "3":
                            $arr_time2 = create_time_array($start2, $start4);
                            break;
                        default:
                            $arr_time2 = create_time_array($start2, $end2);
                            break;
                    }
                    $end_time_info = get_end_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction2, $fraction2_min, $fraction3, $others4);
                    $early_flg = ($end_time_info["diff_count"] == 0) ? "f" : "t";
                }
            } else {
                $late_flg = "f";
                $early_flg = "f";
            }

            // 事由が「代替休暇」「振替休暇」の場合、
            // 所定労働時間を稼働時間にプラス
            // 「有給休暇」を除く $reason == "1" || 2008/10/14
            if ($reason == "4" || $reason == "17") {
                if ($tmp_date >= $start_date) {
                    $sums[15] += $specified_time;
                    $work_time_for_week += $specified_time;
                }
            }

            // 退勤後復帰回数を算出
            $return_count = 0;
            // 退勤後復帰時間を算出 20100413
            $return_time = 0;
            //申請状態 "0":申請不要 "3":承認済 ※未承認時は計算しない //勤務時間を計算しないフラグ確認 20130225
            if (($return_link_type == "0" || $return_link_type == "3") && $no_count_flag != "1") {
                for ($i = 1; $i <= 10; $i++) {
                    $start_var = "o_start_time$i";
                    if ($$start_var == "") {
                        break;
                    }
                    $return_count++;

                    $end_var = "o_end_time$i";
                    if ($$end_var == "") {
                        break;
                    }
                    //端数処理
                    if ($fraction1 > "1") {
                        //端数処理する分
                        $moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
                        //開始日時
                        $tmp_start_date_time = $tmp_date.$$start_var;
                        //基準日時
                        $tmp_base_date_time = substr($tmp_start_date_time,0,10)."00";
                        $tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_start_date_time, $moving_minutes, $fraction1);
                        $tmp_ret_start_time = str_replace(":", "",substr($tmp_date_time, 8, 5));
                    } else {
                        $tmp_ret_start_time = str_replace(":", "", $$start_var);
                    }
                    if ($fraction2 > "1") {
                        //端数処理する分
                        $moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
                        //終了日時
                        $tmp_end_date_time = $tmp_date.$$end_var;
                        //基準日時
                        $tmp_base_date_time = substr($tmp_end_date_time,0,10)."00";
                        $tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_end_date_time, $moving_minutes, $fraction2);

                        $tmp_ret_end_time = str_replace(":", "",substr($tmp_date_time, 8, 5));
                    } else {
                        $tmp_ret_end_time = str_replace(":", "", $$end_var);
                    }
                    $arr_ret = $timecard_common_class->get_return_array($tmp_date.$tmp_ret_start_time, $tmp_ret_start_time, $tmp_ret_end_time);

                    $return_time += count($arr_ret);
                    //todo:内訳
                    $next_date = next_date($tmp_date);
                    $tmp_ret_end_date = ($tmp_ret_start_time > $tmp_ret_end_time) ? $next_date : $tmp_date;
                    $arr_overtime_data2 = get_overtime_data($work_times_info, $tmp_date, $pattern, $reason, $arr_timecard_holwk_day, $arr_type, $timecard_common_class, $tmp_date.$tmp_ret_start_time, $tmp_ret_end_date.$tmp_ret_end_time, $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], $timecard_bean, $ovtm_class);
                    $normal_over += $arr_overtime_data2["normal_over"];
                    $normal_night += $arr_overtime_data2["late_night"];
                    $hol_over += $arr_overtime_data2["hol_over"];
                    $hol_night += $arr_overtime_data2["hol_late_night"];

                }
            }
            $wk_return_time = $return_time;
            //分を時分に変換
            $return_time = minute_to_hmm($return_time);

            // 前日までの計 = 週計
            $last_day_total = $work_time_for_week;

            $work_time_for_week += $work_time;
            $work_time_for_week += $wk_return_time;

            if ($tmp_date < $start_date) {
                $tmp_date = next_date($tmp_date);
                $counter_for_week++;
                continue;
            }
        }

        // 日数
        $total_workday_count = "";
        if (($start_time != "" && $end_time != "") || $after_night_duty_flag == "1") //明けを追加 20110819
        {
            //休暇時は計算しない 20090806
            if ($pattern != "10") {
                if($workday_count != "")
                {
                    $total_workday_count = $workday_count;
                }
            }

            if($night_duty_flag)
            {
                //曜日に対応したworkday_countを取得する
                $total_workday_count = $total_workday_count + $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);
            }
        }
        else if ($workday_count == 0){
            //日数換算に0が指定された場合のみ0を入れる
            $total_workday_count = $workday_count;
        }


        // 勤務日種別と出勤パターン（予定）から所定労働時間帯を取得
        $prov_start2 = $timecard_common_class->get_officehours_info( $prov_tmcd_group_id, $prov_pattern, $tmp_type, "start2" );
        $prov_end2   = $timecard_common_class->get_officehours_info( $prov_tmcd_group_id, $prov_pattern, $tmp_type, "end2" );

        // 所定労働時間が確定できたら予定時刻として使用
        if ($prov_start2 != "" && $prov_end2 != "") {
            $prov_start_time = $prov_start2;
            $prov_end_time = $prov_end2;
        }

        // 締めデータ（日）を作成
        // ************ oose update start 2008/02/05（手当追加）*****************
        $sql = "insert into wktotalp (emp_id, yyyymm, date, pattern, reason, night_duty, allow_id, prov_start_time, prov_end_time, tmcd_group_id) values (";
        $content = array($tmp_emp_id, $c_yyyymm, $tmp_date, $prov_pattern, $prov_reason, $prov_night_duty, $prov_allow_id, $prov_start_time, $prov_end_time, $prov_tmcd_group_id);
        // ************ oose update end 2008/02/05（手当追加）*****************
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con, "rolllback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        // 残業管理をしない場合 2008/10/16
        if ($no_overtime == "t") {
            $time1 = "";
        }
        // ************ oose update start 2008/02/01（手当追加）*****************
        $sql = "insert into wktotald (emp_id, yyyymm, date, type, pattern, reason, night_duty, allow_id, start_time, end_time, over_time, night_time, goout_time1, goout_time2, return_count, return_time, office_end_time, late_flg, early_flg, workday_count, tmcd_group_id, meeting_time, previous_day_flag, next_day_flag) values (";
        $content = array($tmp_emp_id, $c_yyyymm, $tmp_date, $type, $pattern, $reason, $night_duty, $allow_id, $start_time, $end_time, $time1, $time2, $wk_time3, $wk_time4, $return_count, $return_time, str_replace(":", "", $end2), $late_flg, $early_flg, $total_workday_count, $tmcd_group_id, $meeting_time, $previous_day_flag, $next_day_flag);
        // ************ oose update end 2008/02/01（手当追加）*****************
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con, "rolllback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        // 残業時間の処理を変更 20090629
        $wk_hoteigai = 0;
        $wk_hoteinai_zan = 0;
        $wk_hoteinai = 0;

        //日数換算
        $total_workday_count = "";
        if($start_time != "" && $end_time != "")
        {
            //休暇時は計算しない 20090806
            if ($pattern != "10") {
                if($workday_count != "")
                {
                    $total_workday_count = $workday_count;
                }
            }

            if($night_duty_flag)
            {
                //曜日に対応したworkday_countを取得する
                $total_workday_count = $total_workday_count + $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);

            }
        }
        else if ($workday_count == 0){
            //日数換算に0が指定された場合のみ0を入れる
            $total_workday_count = $workday_count;
        }
        //法定外残業の基準時間
        if ($base_time == "") {
            $wk_base_time = 8 * 60;
        } else {
            $base_hour = intval(substr($base_time, 0, 2));
            $base_min = intval(substr($base_time, 2, 2));
            $wk_base_time = $base_hour * 60 + $base_min;
        }
        //公休か事由なし休暇で残業時刻がありの場合 20100802
        if ($legal_hol_over_flg) {
            $wk_base_time = 0;
        }
        //残業時間 20100209 変更 20100910
        $wk_zangyo = 0;

        $kinmu_time = 0;
        //出勤時刻、退勤時刻が両方入っている場合に計算する 20101004
        //勤務時間を計算しないフラグが"1"以外 20130225
        if ($start_time != "" && $end_time != "" && $no_count_flag != "1") {
            //残業承認時に計算、申請画面を表示しない場合は無条件に計算
            if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
                    $timecard_bean->over_time_apply_type == "0"
                ) {

                //残業開始、終了時刻がある場合
                if ($over_start_time != "" && $over_end_time != "") {
                    $wk_zangyo = date_utils::get_time_difference($over_end_date_time,$over_start_date_time);
                    //todo:内訳
                    $arr_overtime_data = get_overtime_data($work_times_info, $tmp_date, $pattern, $reason, $arr_timecard_holwk_day, $arr_type, $timecard_common_class, $over_start_date_time, $over_end_date_time, $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], $timecard_bean, $ovtm_class);
                    $normal_over += $arr_overtime_data["normal_over"];
                    $normal_night += $arr_overtime_data["late_night"];
                    $hol_over += $arr_overtime_data["hol_over"];
                    $hol_night += $arr_overtime_data["hol_late_night"];


                    //休憩除外
                    $rest_start_time1 = $arr_atdbkrslt[$tmp_date]["rest_start_time1"];
                    $rest_end_time1 = $arr_atdbkrslt[$tmp_date]["rest_end_time1"];
                    if ($rest_start_time1 != "" && $rest_end_time1 != "") {
                        $rest_start_date1 = ($arr_atdbkrslt[$tmp_date]["rest_start_next_day_flag1"] == 1) ? next_date($tmp_date) :$tmp_date;
                        $rest_end_date1 = ($arr_atdbkrslt[$tmp_date]["rest_end_next_day_flag1"] == 1) ? next_date($tmp_date) :$tmp_date;
                        $wk_rest_time1 = date_utils::get_time_difference($rest_end_date1.$rest_end_time1,$rest_start_date1.$rest_start_time1);
                        $wk_zangyo -= $wk_rest_time1;
                        //todo:休憩内訳
                        $arr_resttime_data = get_overtime_data($work_times_info, $tmp_date, $pattern, $reason, $arr_timecard_holwk_day, $arr_type, $timecard_common_class, $rest_end_date1.$rest_end_time1, $rest_start_date1.$rest_start_time1, $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], $timecard_bean, $ovtm_class);
                        $normal_over -= $arr_resttime_data["normal_over"];
                        $normal_night -= $arr_resttime_data["late_night"];
                        $hol_over -= $arr_resttime_data["hol_over"];
                        $hol_night -= $arr_resttime_data["hol_late_night"];
                    }
                }
                //残業３−５を追加、２−５の繰り返し処理とする
                //残業２
                for ($idx=2; $idx<=5; $idx++) {
                    $s_t = "over_start_time".$idx;
                    $e_t = "over_end_time".$idx;
                    $s_f = "over_start_next_day_flag".$idx;
                    $e_f = "over_end_next_day_flag".$idx;
                    $over_start_time_value = $arr_atdbkrslt[$tmp_date][$s_t];
                    $over_end_time_value = $arr_atdbkrslt[$tmp_date][$e_t];
                    $over_start_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$s_f];
                    $over_end_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$e_f];
                    if ($over_start_time_value != "" && $over_end_time_value != "") {
                        //開始日時
                        $over_start_date_time_value = ($over_start_next_day_flag_value == 1) ? next_date($tmp_date).$over_start_time_value : $tmp_date.$over_start_time_value;
                        //終了日時
                        $over_end_date_time_value = ($over_end_next_day_flag_value == 1) ? next_date($tmp_date).$over_end_time_value : $tmp_date.$over_end_time_value;

                        $wk_zangyo2 = date_utils::get_time_difference($over_end_date_time_value,$over_start_date_time_value);
                        //todo:内訳
                        $arr_overtime_data2 = get_overtime_data($work_times_info, $tmp_date, $pattern, $reason, $arr_timecard_holwk_day, $arr_type, $timecard_common_class, $over_start_date_time_value, $over_end_date_time_value, $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], $timecard_bean, $ovtm_class);
                        $normal_over += $arr_overtime_data2["normal_over"];
                        $normal_night += $arr_overtime_data2["late_night"];
                        $hol_over += $arr_overtime_data2["hol_over"];
                        $hol_night += $arr_overtime_data2["hol_late_night"];
                        //休憩除外
                        $rs_t = "rest_start_time".$idx;
                        $re_t = "rest_end_time".$idx;
                        $rs_f = "rest_start_next_day_flag".$idx;
                        $re_f = "rest_end_next_day_flag".$idx;
                        $rest_start_time_value = $arr_atdbkrslt[$tmp_date][$rs_t];
                        $rest_end_time_value = $arr_atdbkrslt[$tmp_date][$re_t];
                        $rest_start_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$rs_f];
                        $rest_end_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$re_f];

                        //残業時間深夜内休憩
                        $overtime_rest = 0;

                        if ($rest_start_time_value != "" && $rest_end_time_value != "") {
                            $rest_start_date_value = ($arr_atdbkrslt[$tmp_date][$rs_f] == 1) ? next_date($tmp_date) :$tmp_date;
                            $rest_end_date_value = ($arr_atdbkrslt[$tmp_date][$re_f] == 1) ? next_date($tmp_date) :$tmp_date;
                            $rest_start_date_time_value = $rest_start_date_value.$rest_start_time_value;
                            $rest_end_date_time_value = $rest_end_date_value.$rest_end_time_value;
                            $arr_resttime_data = get_overtime_data($work_times_info, $tmp_date, $pattern, $reason, $arr_timecard_holwk_day, $arr_type, $timecard_common_class, $rest_start_date_time_value, $rest_end_date_time_value, $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], $timecard_bean, $ovtm_class);

                            //休憩
                            $overtime_rest =
                                $arr_resttime_data["normal_over"]
                                + $arr_resttime_data["late_night"]
                                + $arr_resttime_data["hol_over"]
                                + $arr_resttime_data["hol_late_night"];
                            $wk_zangyo2 -= $overtime_rest;
                            //todo:休憩内訳
                            $normal_over -= $arr_resttime_data["normal_over"];
                            $normal_night -= $arr_resttime_data["late_night"];
                            $hol_over -= $arr_resttime_data["hol_over"];
                            $hol_night -= $arr_resttime_data["hol_late_night"];
                        }

                        $wk_zangyo += $wk_zangyo2;
                    }
                }
                //早出残業
                if ($early_over_time != "") {
                    $wk_early_over_time = hmm_to_minute($early_over_time);
                    $wk_zangyo += $wk_early_over_time;
                }
            }
            //呼出
            $wk_zangyo += $wk_return_time;

            //コメント化20130822
            //if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
            //	//休日残業時は、休憩減算 20100916
            //	if ($legal_hol_over_flg == true) {
            //		if ($wk_zangyo >= ($time3_rest + $time4_rest)) {
            //			$wk_zangyo -= ($time3_rest + $time4_rest);
            //		}
            //	}
            //}
            //休日残業で休憩時刻がある場合 20100921
            if ($legal_hol_over_flg == true && $tmp_rest2 > 0) {
                $wk_zangyo -= $tmp_rest2;
            }
            //出勤時刻と残業開始時刻が一致している場合、所定、遅刻、早退を計算せず、残業、休憩のみ計算する 20100917
            if ($start2 != "" && $start2 == $over_start_time && $over_start_next_day_flag != "1") {//残業開始が翌日でないことを追加 20101027
                $overtime_only_flg = true;
                //遅刻
                $tikoku_time = 0;
                //早退
                $sotai_time = 0;
                //残業承認時に計算、申請画面を表示しない場合は無条件に計算 20100922
                if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
                        $timecard_bean->over_time_apply_type == "0"
                    ) {
                    $wk_zangyo -= $tmp_rest2;
                } else {
                    $tmp_rest2 = 0;
                    $time_rest = "";
                }
            } else {
                $overtime_only_flg = false;
                $paid_hol_min =  $arr_hol_hour_dat[$tmp_date]["calc_minute"];
                //時間有休と相殺しない場合で時間有休ある場合 20140807
                if ($ovtm_class->sousai_sinai_flg == "t" && $paid_hol_min != "") {
                    //遅刻
                    $tikoku_time = 0;
                    //早退
                    $sotai_time = 0;
                }
                else {
                    //遅刻
                    $tikoku_time = $start_time_info["diff_minutes"];
                    //早退
                    $sotai_time = $end_time_info["diff_minutes"];
                }
            }
            $tikoku_time2 = 0;
            // 遅刻時間を残業時間から減算する場合
            if ($timecard_bean->delay_overtime_flg == "1") {
                if ($wk_zangyo >= $tikoku_time) { //20100913
                    $wk_zangyo -= $tikoku_time;
                } else { // 残業時間より多い遅刻時間対応 20100925
                    $tikoku_time2 = $tikoku_time - $wk_zangyo;
                    $wk_zangyo = 0;
                }
            }

            //勤務時間＝所定労働時間−休憩時間−外出時間−早退−遅刻＋残業時間 20100907
            //　残業時間＝承認済み残業時間＋承認済み呼出し勤務時間
            //　承認済み残業時間＝早出残業時間＋勤務終了後残業時間
            //所定
            if ($pattern == "10" || //休暇は所定を0とする 20100916
                    $overtime_only_flg == true ||	//残業のみ計算する 20100917
                    $after_night_duty_flag == "1"  //明けの場合 20110819
                ) {
                $shotei_time = 0;
            } elseif ($start2 == "" && $end2 == "") {  // 所定時刻指定がない場合、端数処理不具合対応 20101005
                $shotei_time = count($arr_effective_time_wk); //休憩を引く前の実働時間
            } else {
                $shotei_time = count($arr_time2);
            }
            $paid_hol_min =  $arr_hol_hour_dat[$tmp_date]["calc_minute"];
            if ($obj_hol_hour->paid_hol_hour_flag == "t" && $paid_hol_min != "") {
                //時間有休と相殺しない場合
                if ($ovtm_class->sousai_sinai_flg == "t") {
                    //遅刻早退が時間有休より多い場合、その分は勤務時間から減算されるようにする
                    if ($start_time_info["diff_minutes"] != "") {
                        if ($start_time_info["diff_minutes"] >= $paid_hol_min) {
                            $shotei_time -= $start_time_info["diff_minutes"];
                        }
                        else {
                            $shotei_time -= $paid_hol_min;
                        }
                    }
                    if ($end_time_info["diff_minutes"] != "") {
                        if ($end_time_info["diff_minutes"] >= $paid_hol_min) {
                            $shotei_time -= $end_time_info["diff_minutes"];
                        }
                        else {
                            $shotei_time -= $paid_hol_min;
                        }
                    }
                }
                else {
                    $shotei_time -= $paid_hol_min;
                }
            }
            //時間有休追加 20111207
            //if ($obj_hol_hour->paid_hol_hour_flag == "t") {
            //	$shotei_time -= $paid_time_hour;
            //}
            //休憩 $tmp_rest2 $time3_rest $time4_rest
            //外出 $time3 $time4
            $kinmu_time = $shotei_time - $time3 - $time4 + $wk_zangyo;
            // 遅刻時間を残業時間から減算しない場合
            if ($timecard_bean->delay_overtime_flg == "2") {
                $kinmu_time -= $tikoku_time;
            }
            // 遅刻時間を残業時間から減算する場合で、残業未承認の場合で遅刻がある場合は引く 20100914
            else {
                if ($overtime_approve_flg == "1") {
                    $kinmu_time -= $tikoku_time;
                }
            }
            // 残業時間より多い遅刻時間対応 20100925
            $kinmu_time -= $tikoku_time2;
            // 早退時間を残業時間から減算しない場合
            if ($timecard_bean->early_leave_time_flg == "2") {
                $kinmu_time -= $sotai_time;
            }
            //時間給者 20100915
            if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
                //休日残業時は、休憩を減算しない 20100916
                //if ($legal_hol_over_flg != true) { //コメント化20130822
                $kinmu_time -= ($time3_rest + $time4_rest);
                //}
            } else {
                if ($overtime_only_flg != true && $legal_hol_over_flg != true) {	//残業のみ計算する場合と休日残業の場合、残業から引くためここでは引かない 20100921
                    //月給制日給制、実働時間内の休憩時間
                    $kinmu_time -= $tmp_rest2;
                }
            }

            //法定内、法定外残業の計算
            if ($wk_zangyo > 0) {
                //法定内入力有無確認
                if ($legal_in_over_time != "") {
                    $wk_hoteinai_zan = hmm_to_minute($legal_in_over_time);
                    if ($wk_hoteinai_zan <= $wk_zangyo) {
                        $wk_hoteigai = $wk_zangyo - $wk_hoteinai_zan;
                    } else {
                        $wk_hoteigai = 0;
                    }

                } else {
                    //法定外残業の基準時間を超える分を法定外残業とする
                    if ($kinmu_time > $wk_base_time) {
                        $wk_hoteigai = $kinmu_time - $wk_base_time;
                    }
                    //
                    if ($wk_zangyo - $wk_hoteigai > 0) {
                        $wk_hoteinai_zan = $wk_zangyo - $wk_hoteigai;
                    } else {
                        //マイナスになる場合
                        $wk_hoteinai_zan = 0;
                        $wk_hoteigai = $wk_zangyo;
                    }
                }
				//遅刻早退で法定内残業がある場合、法定外残業とする 20141024 start
				if ($ovtm_class->delay_overtime_inout_flg == "t") {
					//時間有休、外出を追加 20141203
					if ($wk_hoteinai_zan > 0 &&
						(($start_time_info["diff_minutes"] + $end_time_info["diff_minutes"]) > 0 ||
						 $paid_hol_min > 0 ||
						 $time3 > 0)) {
						$wk_furikae_kouho = max(($start_time_info["diff_minutes"] + $end_time_info["diff_minutes"] + $time3), $paid_hol_min);
						$wk_furikae = min($wk_hoteinai_zan, $wk_furikae_kouho);
						$wk_hoteinai_zan -= $wk_furikae;
						$wk_hoteigai += $wk_furikae;
					}
				}
				//遅刻早退で法定内残業がある場合、法定外残業とする 20141024 end
            }
            //合計
            //法定内
            $wk_hoteinai = $kinmu_time - $wk_zangyo;
            $hoteinai += $wk_hoteinai;
            //法定内残業
            $hoteinai_zan += $wk_hoteinai_zan;
            //法定外残
            $hoteigai += $wk_hoteigai;
            //勤務時間
            $sums[15] += $kinmu_time;
            //残業時間
            $sums[16] += $wk_zangyo;

            $arr_zangyo_day[$tmp_date] = $wk_zangyo;
            //内訳
            $arr_normal_over[$tmp_date] = $normal_over;
            $arr_normal_night[$tmp_date] = $normal_night;
            $arr_hol_over[$tmp_date] = $hol_over;
            $arr_hol_night[$tmp_date] = $hol_night;
        }

        $tmp_date = next_date($tmp_date);
        $counter_for_week++;

    } //end of while

    //期間別集計
    $cnt_hist = count($arr_term_data);
    for ($i=0; $i<$cnt_hist; $i++) {
        $t_zangyo_min = 0;
        $t_normal_over_min = 0;
        $t_normal_night_min = 0;
        $t_hol_over_min = 0;
        $t_hol_night_min = 0;

        $arr_target_date = $arr_term_data[$i]["arr_target_date"];
        $tmp_date = $start_date;
        while ($tmp_date <= $end_date) {
            if (in_array($tmp_date, $arr_target_date)) {
                $t_zangyo_min += $arr_zangyo_day[$tmp_date];
                //内訳
                $t_normal_over_min += $arr_normal_over[$tmp_date];
                $t_normal_night_min += $arr_normal_night[$tmp_date];
                $t_hol_over_min += $arr_hol_over[$tmp_date];
                $t_hol_night_min += $arr_hol_night[$tmp_date];
            }
            $tmp_date = next_date($tmp_date);
        }
        $t_zangyo_fmt = ($t_zangyo_min == 0)  ? "0:00" : minute_to_hmm($t_zangyo_min);
        list($overtime_hour, $overtime_min) = split( ":", $t_zangyo_fmt);
        $normal_over_fmt = ($t_normal_over_min == 0)  ? "0:00" : minute_to_hmm($t_normal_over_min);
        list($normal_over_hour, $normal_over_min) = split( ":", $normal_over_fmt);
        $normal_night_fmt = ($t_normal_night_min == 0)  ? "0:00" : minute_to_hmm($t_normal_night_min);
        list($normal_night_hour, $normal_night_min) = split( ":", $normal_night_fmt);
        $hol_over_fmt = ($t_hol_over_min == 0)  ? "0:00" : minute_to_hmm($t_hol_over_min);
        list($hol_over_hour, $hol_over_min) = split( ":", $hol_over_fmt);
        $hol_night_fmt = ($t_hol_night_min == 0)  ? "0:00" : minute_to_hmm($t_hol_night_min);
        list($hol_night_hour, $hol_night_min) = split( ":", $hol_night_fmt);
        //echo "t_zangyo_min=$t_zangyo_min fmt=$t_zangyo_fmt";
        //echo "$i {$arr_term_data[$i]["start_date"]} {$arr_term_data[$i]["end_date"]} t_zangyo_min=$t_zangyo_min fmt=$t_zangyo_fmt";
        //echo " {$arr_term_data[$i]["class_id"]} {$arr_term_data[$i]["atrb_id"]} {$arr_term_data[$i]["dept_id"]} <br>\n";
        /*
        残業締めデータ登録
        */
        $wk_room_id = ($arr_term_data[$i]["room_id"] != "") ? $arr_term_data[$i]["room_id"] : null;
        $sql = "insert into wktotalovtm (class_id, atrb_id, dept_id, room_id, emp_id, yyyymm, overtime_hour, overtime_min, normal_over_hour, normal_over_min, normal_night_hour, normal_night_min, hol_over_hour, hol_over_min, hol_night_hour, hol_night_min) values (";
        $content = array($arr_term_data[$i]["class_id"], $arr_term_data[$i]["atrb_id"], $arr_term_data[$i]["dept_id"], $wk_room_id, $tmp_emp_id, $c_yyyymm, $overtime_hour, $overtime_min, $normal_over_hour, $normal_over_min, $normal_night_hour, $normal_night_min, $hol_over_hour, $hol_over_min, $hol_night_hour, $hol_night_min);
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con, "rolllback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

    }
    //exit;

    // 要勤務日数を取得 20091222
    $wk_year = substr($c_yyyymm, 0, 4);
    $wk_mon = substr($c_yyyymm, 4, 2);
    //当月日数
    if ($closing_month_flg == "2") { //締め日の月度が終了日の月の場合
        $wk_year2 = substr($start_date, 0, 4);
        $wk_mon2 = substr($start_date, 4, 2);
    } else {
        $wk_year2 = $wk_year;
        $wk_mon2 = $wk_mon;
    }
    $days_in_month = days_in_month($wk_year2, $wk_mon2);
    //対象年月の公休数を取得 20100615
    $legal_hol_cnt = get_legal_hol_cnt_month($con, $fname, $wk_year, $wk_mon, $closing, $closing_month_flg, $tmcd_group_id);
    $sums[0] = $days_in_month - $legal_hol_cnt;

    // 休日出勤 2008/10/23
    foreach ($arr_hol_work_date as $wk_date) {

        $wk_type = $wk_date["type"];

        switch ($wk_type) {
            case "1":  // 通常勤務日
                $sums[35] += 1;
                break;
            case "4":  // 法定休日
                $sums[22] += 1;
                break;
            case "5":  // 所定休日
                $sums[21] += 1;
                break;
            case "6":  // 祝日
                $sums[40] += 1;
                break;
            case "7":  // 年末年始休暇
                $sums[41] += 1;
                break;
        }
    }

    // 早退時間を残業時間から減算する設定の場合、法定内残業、法定外残業の順に減算する
    if ($early_leave_time_flg == "1") {

        list($hoteinai_zan, $hoteigai) = $atdbk_common_class->get_overtime_early_leave($hoteinai_zan, $hoteigai, $sums[13]);
        //残業時間 20100713
        $sums[16] = $hoteinai_zan + $hoteigai;
    }
    // 法定内勤務
    $sums[36] = $hoteinai;
    // 法定内残業
    $sums[37] = $hoteinai_zan;
    // 法定外残業
    $sums[38] = $hoteigai;

    // 変則労働期間が月の場合
    if ($arr_irrg_info["irrg_type"] == "2") {

        // 所定労働時間を基準時間とする
        $tmp_irrg_minutes = $arr_irrg_info["irrg_minutes"];
        if ($hol_minus == "t") {
            $tmp_irrg_minutes -= (8 * 60 * $holiday_count);
        }
        $sums[14] = ($tmp_irrg_minutes > 0) ? $tmp_irrg_minutes : 0;

        // 稼働時間−基準時間を残業時間とする
        $sums[16] = $sums[15] - $sums[14];
    }
    else {
        // 基準時間の計算を変更 2008/10/21
        // 要勤務日数 × 所定労働時間
        $sums[14] = $sums[0] * date_utils::hi_to_minute($day1_time);
    }

    // 月集計値の稼働時間・残業時間・深夜勤務を端数処理
    for ($i = 15; $i <= 17; $i++) {
        if ($sums[$i] < 0) {
            $sums[$i] = 0;
        }
        $sums[$i] = round_time($sums[$i], $fraction4, $fraction4_min);
    }
    //深夜残業、休日残業
    for ($i = 43; $i <= 44; $i++) {
        if ($sums[$i] < 0) {
            $sums[$i] = 0;
        }
        $sums[$i] = round_time($sums[$i], $fraction4, $fraction4_min);
    }

    // 残業管理をしない場合、残業時間、法定内勤務、法定内残業、法定外残業を０とする
    if ($no_overtime == "t") {
        $sums[16] = 0;
        $sums[36] = 0;
        $sums[37] = 0;
        $sums[38] = 0;
    }

    // 月集計値を登録用に編集
    for ($i = 12; $i <= 17; $i++) {
        $sums[$i] = ($sums[$i] == 0) ? "0:00" : minute_to_hmm($sums[$i]);
    }

    for ($i = 36; $i <= 39; $i++) {
        $sums[$i] = ($sums[$i] == 0) ? "0:00" : minute_to_hmm($sums[$i]);
    }

    //深夜残業、休日残業
    for ($i = 43; $i <= 44; $i++) {
        $sums[$i] = ($sums[$i] == 0) ? "0:00" : minute_to_hmm($sums[$i]);
    }
    //深夜勤務は未設定とする
    $sums[17] = "";
    //休日勤務は未設定とする
    $sums[39] = "";
    if ($obj_hol_hour->paid_hol_hour_flag == "t") {
        //１日分の所定労働時間(分)
        $specified_time_per_day = $obj_hol_hour->get_specified_time($tmp_emp_id, $day1_time);
        //当月分
        $wk_day = $paid_sums[0] / $specified_time_per_day;
        $sums[7] += $obj_hol_hour->edit_nenkyu_zan($wk_day);
    }

    // 締めデータ（月）を作成
    $sql = "insert into atdbkclose (emp_id, yyyymm) values (";
    $content = array($tmp_emp_id, $c_yyyymm);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rolllback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    // 事由追加 days11からdays22、通常勤務日days_ex6
    // 法定内勤務 time7 法定内残業 time8 法定外残業 time9 休日勤務 time10 追加
    // 祝日days_ex7 年末年始休暇days_ex8
    // 事由追加 days23年末年始 time11深夜残業 time12休日残業
    $sql = "insert into wktotalm (emp_id, yyyymm, days1, days2, days3, days4, days5, days6, days7, days8, days9, days10, count1, count2, time1, time2, time3, time4, time5, time6, days_ex1, days_ex2, days_ex3, days_ex4, days_ex5, days11, days12, days13, days14, days15, days16, days17, days18, days19, days20, days21, days22, days_ex6, time7, time8, time9, time10, days_ex7, days_ex8, days23, time11, time12) values (";
    $content = array_merge(array($tmp_emp_id, $c_yyyymm), $sums);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rolllback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    // トランザクションのコミット
    pg_query($con, "commit");
}


// データベース接続を閉じる
pg_close($con);

// 勤務時間集計画面を再表示
//$url_emp_ids = "";
//foreach ($emp_id as $tmp_id) {
//	$url_emp_ids .= "&emp_id[]=$tmp_id";
//}
//$url_srch_name = urlencode($srch_name);
//echo("<script type=\"text/javascript\">location.href = 'work_admin_total.php?session=$session$url_emp_ids&yyyymm=$yyyymm&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&group_id=$group_id&page=$page&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&srch_id=$srch_id';</script>");

?>
<form name="book" action="work_admin_total.php" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<?
foreach ($emp_id as $tmp_id) {
    echo("<input type=\"hidden\" name=\"emp_id[]\" value=\"$tmp_id\">\n");
}
?>

<input type="hidden" name="c_yyyymm" value="<? echo($c_yyyymm); ?>">
<input type="hidden" name="yyyymm" value="<? echo($yyyymm); ?>">
<input type="hidden" name="srch_id" value="<? echo($srch_id); ?>">
<input type="hidden" name="srch_name" value="<? echo($srch_name); ?>">
<input type="hidden" name="cls" value="<? echo($cls); ?>">
<input type="hidden" name="atrb" value="<? echo($atrb); ?>">
<input type="hidden" name="dept" value="<? echo($dept); ?>">
<input type="hidden" name="group_id" value="<? echo($group_id); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
<input type="hidden" name="shift_group_id" value="<? echo($shift_group_id); ?>">
<input type="hidden" name="csv_layout_id" value="<? echo($csv_layout_id); ?>">
<input type="hidden" name="duty_form_jokin" value="<? echo($duty_form_jokin); ?>">
<input type="hidden" name="duty_form_hijokin" value="<? echo($duty_form_hijokin); ?>">
<input type="hidden" name="sus_flg" value="<? echo($sus_flg); ?>">
<input type="hidden" name="mmode" value="<? echo($mmode); ?>">
</form>

<?
echo("<script language='javascript'>document.book.submit();</script>");

function get_class_history($con, $fname, $emp_id, $start_date, $end_date) {

    $wk_yyyy = substr($start_date, 0, 4);
    $wk_mm = substr($start_date, 4, 2);
    $wk_dd = substr($start_date, 6, 2);

    $wk_ymd = $wk_yyyy."-".$wk_mm."-".$wk_dd." 00:00:00";
    $sql = "select * from class_history ";
    $cond = "where emp_id = '$emp_id' and histdate >= '$wk_ymd' order by histdate ";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    $arr_class_hist = array();
    while ($row = pg_fetch_array($sel)) {
        $arr_class_hist[] = $row;
    }
    return $arr_class_hist;
}

function get_current_class_info($con, $fname, $emp_id) {
    $sql = "select emp_id, emp_class, emp_attribute, emp_dept, emp_room from empmst ";
    $cond = "where emp_id = '$emp_id' ";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    $arr_class_info = array();
    if (pg_num_rows($sel) > 0) {
        $arr_class_info = pg_fetch_array($sel, 0, PGSQL_ASSOC);
    }
    return $arr_class_info;
}
?>
