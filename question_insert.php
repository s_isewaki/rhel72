<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<? require("about_postgres.php"); ?>
<? require("about_session.php"); ?>
<? require("about_authority.php"); ?>
<? require_once("Cmx.php"); ?>
<? require_once("aclg_set.php"); ?>
<?
$fname = $PHP_SELF;

$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$ward = check_authority($session,6,$fname);
if($ward == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);
?>
<?
$question_keyword = trim($question_keyword);
$question = trim($question);

if(!$question_keyword){
echo("<script language='javascript'>alert(\"キーワードを入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($question_keyword) > 100){
echo("<script language='javascript'>alert(\"キーワードを50文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(!$question){
echo("<script language='javascript'>alert(\"内容を入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($question) > 500){
echo("<script language='javascript'>alert(\"内容を250文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

$select_id = "select emp_id from login where emp_id in (select emp_id from session where session_id = '$session')";
$result_select = pg_exec($con, $select_id);
if($result_select == false){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_result($result_select,0,"emp_id");

$date = date(YmdHi);

$select_max = "select max(question_id) from question";
$result_max = pg_exec($con,$select_max);
$count = pg_numrows($result_max);
if($count > 0){
	$question_id = pg_result($result_max,0,"max");
	$question_id = $question_id + 1;
}else{
	$question_id = "1";
}

$insert_question = "insert into question (emp_id, question_id, date, qa_category_id, question_keyword, priority, question, q_del_flg) values ('$emp_id', '$question_id', '$date', '$qa_category_id', '$question_keyword', '$priority', '$question', 'f')";
$result_insert = pg_exec($con,$insert_question);
pg_close($con);
if($result_insert == false){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

//echo("<script language='javascript'>location.href='question_list.php?session=$session';</script>");
echo("<script language='javascript'>location.href='question_list.php?session=$session&qa_category_id=$qa_category_id';</script>");
exit;
?>