<?

ob_start();

require_once("Cmx.php");
require_once("about_comedix.php");
require_once("library_common.php");

$fname = $PHP_SELF;

if ($session != "") {

    // セッションのチェック
    $session = qualify_session($session, $fname);
    if ($session == "0") {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
        exit;
    }

    // 権限のチェック
    $check_auth = check_authority($session, 46, $fname);
    if ($check_auth == "0") {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
        exit;
    }
}

// データベースに接続
$con = connect2db($fname);

// ファイル名を取得
$sql = "select newsfile_name from newsfile";
$cond = "where news_id = $news_id and newsfile_no = $newsfile_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$newsfile_name = pg_fetch_result($sel, 0, "newsfile_name");
$ext = strrchr($newsfile_name, ".");

// データベース接続を閉じる
pg_close($con);

ob_end_clean();

$lib_url = "news/{$news_id}_{$newsfile_no}{$ext}";
$file_path = $lib_url;

// 運用ディレクトリ名を取得
$dir = str_replace($_SERVER["DOCUMENT_ROOT"], "", $_SERVER["SCRIPT_FILENAME"]);
$dir = str_replace("news_file_refer.php", "", $dir);
$lib_url = "http://" . $_SERVER['HTTP_HOST'] . $dir . $lib_url;

// file download
$filename_flg = lib_get_filename_flg();
if ($filename_flg > 0) {
    if (is_file($file_path)) {
        ob_clean();
        if ($filename_flg == 1) { // MSIE
            $newsfile_name = to_sjis($newsfile_name);
        }
        else if ($filename_flg == 2) { // ff
            $newsfile_name = to_utf8($newsfile_name);
        }
        else {
            $newsfile_name = rawurlencode(to_utf8($newsfile_name));
        }

        header("Content-Disposition: attachment; filename=\"$newsfile_name\"");
        header("Content-Type: application/octet-stream; name=$newsfile_name");
        header("Content-Length: " . filesize($file_path));
        readfile($file_path);
        ob_end_flush();
        exit;
    }
    else {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
        exit;
    }
}
else {
    header("Location: $lib_url");
}
