<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_summary_search_bed.ini");
require_once("label_by_profile_type.ini");
require_once("kango_common.ini");
require_once("sum_exist_workflow_check_exec.ini");
require_once("summary_common.ini");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$checkauth = check_authority($session, 57, $fname);
if($checkauth == "0"){
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 棟一覧を取得
$sql = "select bldg_cd, bldg_name from bldgmst";
$cond = "where bldg_del_flg = 'f' order by bldg_cd";
$sel_bldg = select_from_table($con, $sql, $cond, $fname);
$bldg_count = pg_num_rows($sel_bldg);

// 病棟一覧の取得
$sql = "select bldg_cd, ward_cd, ward_name from wdmst";
$cond = "where ward_del_flg = 'f' order by bldg_cd, ward_cd";
$sel_ward = select_from_table($con, $sql, $cond, $fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
$emp_id = @pg_fetch_result($sel, 0, "emp_id");

// ログ
summary_common_write_operate_log("access", $con, $fname, $session, $emp_id, "", "");


if (@$action == "表示") {
	set_user_selected_bldg($con, $fname, $emp_id, $bldg_cd, $ward_cd);
} else {
	$user_selected_bldg = get_user_selected_bldg($con, $fname, $emp_id);
	if (!is_null($user_selected_bldg)) {
		$bldg_cd = $user_selected_bldg["bldg_cd"];
		$ward_cd = $user_selected_bldg["ward_cd"];
		$action = "表示";
	}
}

if ($action == "表示") {

	// 画面更新間隔の値を取得
	$sql = "select refresh_sec from roomlayout";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		require_once("summary_common.ini");
		summary_common_show_error_page_and_die();
	}
	if (pg_num_rows($sel) > 0) {
		$refresh_sec = pg_fetch_result($sel, 0, "refresh_sec");
	} else {
		$refresh_sec = 300;
	}
}

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 患者/利用者
$patient_title = $_label_by_profile["PATIENT"][$profile_type];

// 病床検索/居室検索
$bed_search_title = $_label_by_profile["BED_SEARCH"][$profile_type];

// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];

// 主治医/担当者１
$main_doctor_title = $_label_by_profile["MAIN_DOCTOR"][$profile_type];

// 入院/入所
$in_title = $_label_by_profile["IN"][$profile_type];

// 退院/退所
$out_title = $_label_by_profile["OUT"][$profile_type];

// 在院/在所
$stay_title = $_label_by_profile["STAY_HOSPITAL"][$profile_type];

// 病棟/施設
$ward_title = $_label_by_profile["WARD"][$profile_type];
?>
<title>CoMedix <?
// メドレポート/ＰＯレポート(Problem Oriented)
echo ($_label_by_profile["MED_REPORT"][$profile_type]); ?> | <?=$bed_search_title?></title>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript">
function initPage() {
<? if ($action == "表示") { ?>
	setTimeout(function(){location.reload();}, <? echo($refresh_sec * 1000); ?>);
<? } ?>
	resetWardOptions('<? echo($ward_cd); ?>');
	wardScrolled();
}

// 上部スクロールバー制御
var ward_map_scroll_div = null;
var ward_map_scroll_top = null;
var ward_map_image_gauge = null;
var ward_map_scroll_initialized = false;
function wardScrolled(id){
	if (ward_map_scroll_div==null) ward_map_scroll_div = document.getElementById("ward_map_scroll_div");
	if (!ward_map_scroll_div) return;
	if (ward_map_scroll_top==null) ward_map_scroll_top = document.getElementById("ward_map_scroll_top");
	if (!ward_map_scroll_initialized){
		if (ward_map_scroll_div.scrollWidth > 0){
			if (ward_map_image_gauge==null) ward_map_image_gauge = document.getElementById("ward_map_image_gauge");
			ward_map_image_gauge.style.height = "1px";
			ward_map_image_gauge.style.width = ward_map_scroll_div.scrollWidth + "px";
		}
	}
	ward_map_scroll_initialized = true;
	if(!id) return;
	if (ward_map_scroll_top.scrollLeft != ward_map_scroll_div.scrollLeft){
		if (id == ward_map_scroll_div.id){
			ward_map_scroll_top.scrollLeft = ward_map_scroll_div.scrollLeft;
		} else {
			ward_map_scroll_div.scrollLeft = ward_map_scroll_top.scrollLeft;
		}
	}
}


function resetWardOptions(default_ward_cd) {
	var bldg_cd = document.mainform.bldg_cd.value;
	deleteAllOptions(document.mainform.ward_cd);
	addOption(document.mainform.ward_cd, '', '　　　　　', default_ward_cd);
	addOption(document.mainform.ward_cd, '0', 'すべて', default_ward_cd);

	switch (bldg_cd) {
<?
$pre_bldg_cd = "";
while ($row = pg_fetch_array($sel_ward)) {
	$cur_bldg_cd = $row["bldg_cd"];

	if ($cur_bldg_cd != $pre_bldg_cd) {
		if ($pre_bldg_cd != "") {
			echo ("\t\tbreak;\n");
		}
		echo ("\tcase '$cur_bldg_cd':\n");
		$pre_bldg_cd = $cur_bldg_cd;
	}

	echo("\t\taddOption(document.mainform.ward_cd, '{$row["ward_cd"]}', '{$row["ward_name"]}', default_ward_cd);\n");
}
?>
	}
}

function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	box.style.overflow = 'auto';
}

function popupPatientDetail(pt_info1, pt_info13, pt_info2, pt_info3, pt_info4, pt_info5, pt_info6, pt_info7, pt_info8, pt_info22, pt_info23, pt_info9, pt_info10, pt_info11, pt_info14, pt_info15, pt_info20, pt_info21, pt_info12, pt_info16, pt_info19, pt_info24, pt_info18, pt_info17, e) {
	popupDetailBlue(
		new Array(
			'<?=$patient_title?>氏名', pt_info1,
			'<?=$patient_title?>ID', pt_info13,
			'保険', pt_info2,
			'性別', pt_info3,
			'年齢', pt_info4,
			'<?=$section_title?>', pt_info5,
			'<?=$main_doctor_title?>', pt_info6,
			'移動', pt_info7,
			'観察', pt_info8,
			'面会', pt_info9,
			'外泊', pt_info10,
			'外出', pt_info11,
			'<?=$in_title?>日', pt_info14,
			'<?=$stay_title?>日数', pt_info15,
			'回復期リハビリ算定期限分類', pt_info22,
			'回復期リハビリ算定期限日', pt_info23,
			'リハビリ算定期限分類', pt_info20,
			'リハビリ算定期限日', pt_info21,
			'<?=$out_title?>予定日', pt_info12,
			'医療区分・ADL区分', pt_info16,
			'看護度分類', pt_info19,
			'差額室料免除',pt_info24,
			'個人情報に関する要望', pt_info18,
			'特記事項', pt_info17
		), 350, 100, e
	);
}

function printPage() {
<? $tmp_ward_cd = ($ward_cd == "0") ? "" : $ward_cd; ?>
	window.open('bed_menu_occupation_print.php?session=<? echo($session); ?>&bldg_cd=<? echo($bldg_cd); ?>&ward_cd=<? echo($tmp_ward_cd); ?>', 'occp', 'width=800,height=600,scrollbars=yes');
}

function showDetail() {
	if (document.mainform.ward_cd.value == '') {
		return;
	}

	document.mainform.action.value = '表示';
	document.mainform.submit();
}
</script>
<style type="text/css">
table.bed {border-collapse:collapse;}
table.bed td {border:1px solid #9bc8ec;}
</style>
</head>
<body onload="document.getElementById('page_contents').style.display=''; initPage();">

<? summary_common_show_sinryo_top_header($session, $fname, "病床検索"); ?>

<? summary_common_show_main_tab_menu($con, $fname, $bed_search_title, $is_workflow_exist); ?>

<div id="page_contents" style="display:none">
<form name="mainform" action="summary_search_bed.php" method="get">
<table width="100%" class="list_table" cellspacing="1">
<tr>
<th style="width:10%; text-align:center">事業所（棟）</th>
<td style="width:10%"><select name="bldg_cd" onchange="resetWardOptions();">
<?
while ($row = pg_fetch_array($sel_bldg)) {
	echo("<option value=\"{$row["bldg_cd"]}\"");
	if ($row["bldg_cd"] == $bldg_cd) {
		echo(" selected");
	}
	echo(">{$row["bldg_name"]}\n");
}
?></select>
</td>
<th style="width:10%; text-align:center"><?=$ward_title?></th>
<td><select name="ward_cd" onchange="showDetail();">
</select>
</td>
</tr>
</table>



<? if ($bldg_count == 0) { ?>
	<div>棟が登録されていません。</div>
<? } else if ($action == "表示") {
	if ($ward_cd == "0") {
		show_bldg_report($con, $bldg_cd, $fname);
		$ward_cd = "";
	}

	show_ward_layout($con, $bldg_cd, $ward_cd, $session, $fname);

	if ($ward_cd != "") {
		show_ward_report($con, $bldg_cd, $ward_cd, $fname);
	}
}
?>
<input type="hidden" name="session" value="<? echo $session ?>">
<input type="hidden" name="action" value="">
</form>
</div><!-- end of "page_contents" -->
</body>
<? pg_close($con); ?>
</html>
