<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ワークフロー｜テンプレート編集</title>
<?
require_once("about_comedix.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// ワークフロー権限のチェック
$checkauth = check_authority($session, 3, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$fname = $PHP_SELF;

//$con = connect2db($fname);

?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
function initPage() {
	document.mainform.wkfw_content.value = opener.document.wkfw.wkfw_content.value;
	document.getElementById("wkfw_title").innerHTML = opener.document.wkfw.wkfw_title.value;
}
function setContent() {
	if (opener.document.wkfw.wkfw_content) {
		opener.document.wkfw.wkfw_content.value = document.mainform.wkfw_content.value;
	}
	window.close();
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initPage();">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279a5">
<td width="" height="32" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>テンプレート編集</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>


<form method="post" name="mainform">

<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
ワークフロー名：<span id="wkfw_title"></span></font>
</td>
<td align="right"><input type="button" value="設定" style="width:100px;" onClick="setContent();"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">

<textarea name="wkfw_content" rows="34" cols="120" style="font-size:12pt;">
</textarea>
</font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="button" value="設定" style="width:100px;" onClick="setContent();"></td>
</tr>
</table>

<input type="hidden" name="session" value="<? echo($session); ?>">
</form>


</center>
</body>
<? //pg_close($con); ?>
</html>
