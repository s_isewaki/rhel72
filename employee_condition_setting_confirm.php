<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
//勤務条件登録
?>
<body>
<form name="items" action="employee_condition_setting.php" method="post">
<input type="hidden" name="wage" value="<? echo($wage); ?>">
<input type="hidden" name="tmcd_group_id" value="<? echo($tmcd_group_id); ?>">
<input type="hidden" name="unit_price1" value="<? echo($unit_price1); ?>">
<input type="hidden" name="unit_price2" value="<? echo($unit_price2); ?>">
<input type="hidden" name="unit_price3" value="<? echo($unit_price3); ?>">
<input type="hidden" name="unit_price4" value="<? echo($unit_price4); ?>">
<input type="hidden" name="unit_price5" value="<? echo($unit_price5); ?>">
<input type="hidden" name="unit_price6" value="<? echo($unit_price6); ?>">
<input type="hidden" name="unit_price7" value="<? echo($unit_price7); ?>">
<input type="hidden" name="commuting_distance" value="<? echo($commuting_distance); ?>">
<input type="hidden" name="commuting_type" value="<? echo($commuting_type); ?>">
<input type="hidden" name="irrg_app" value="<? echo($irrg_app); ?>">
<input type="hidden" name="irrg_type" value="<? echo($irrg_type); ?>">
<input type="hidden" name="hol_minus" value="<? echo($hol_minus); ?>">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="key1" value="<? echo($key1); ?>">
<input type="hidden" name="key2" value="<? echo($key2); ?>">
<input type="hidden" name="class_cond" value="<? echo($class_cond); ?>">
<input type="hidden" name="atrb_cond" value="<? echo($atrb_cond); ?>">
<input type="hidden" name="dept_cond" value="<? echo($dept_cond); ?>">
<input type="hidden" name="room_cond" value="<? echo($room_cond); ?>">
<input type="hidden" name="job_cond" value="<? echo($job_cond); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
<input type="hidden" name="view" value="<? echo($view); ?>">
<input type="hidden" name="emp_o" value="<? echo($emp_o); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="atdptn_id" value="<? echo($atdptn_id); ?>">

<!-- ************ oose add start ********* -->
<input type="hidden" name="duty_form" value="<? echo($duty_form); ?>">
<input type="hidden" name="employment_working_other_flg" value="<? echo($employment_working_other_flg); ?>">
<input type="hidden" name="duty_form_type" value="<? echo($duty_form_type); ?>">
<!-- ************ oose add end *********** -->

<input type="hidden" name="no_overtime" value="<? echo($no_overtime); ?>">
<input type="hidden" name="paid_hol_tbl_id" value="<? echo($paid_hol_tbl_id); ?>">
<input type="hidden" name="csv_layout_id" value="<? echo($csv_layout_id); ?>">
<input type="hidden" name="paid_hol_start_yr" value="<? echo($paid_hol_start_yr); ?>">
<input type="hidden" name="paid_hol_start_mon" value="<? echo($paid_hol_start_mon); ?>">
<input type="hidden" name="paid_hol_start_day" value="<? echo($paid_hol_start_day); ?>">
<input type="hidden" name="day1_hour" value="<? echo($day1_hour); ?>">
<input type="hidden" name="day1_min" value="<? echo($day1_min); ?>">
<input type="hidden" name="am_hour" value="<? echo($am_hour); ?>">
<input type="hidden" name="am_min" value="<? echo($am_min); ?>">
<input type="hidden" name="pm_hour" value="<? echo($pm_hour); ?>">
<input type="hidden" name="pm_min" value="<? echo($pm_min); ?>">
<input type="hidden" name="salary_id" value="<? echo($salary_id); ?>">
<input type="hidden" name="org_corp_cd" value="<? echo($org_corp_cd); ?>">
<!-- 週所定労働時間 20131111 YOKOBORI -->
<input type="hidden" name="week_hour" value="<? echo($week_hour); ?>">
<input type="hidden" name="week_min" value="<? echo($week_min); ?>">

<? /* 他部署兼務追加 20120418 */ ?>
<input type="hidden" name="other_post_flg" value="<? echo($other_post_flg); ?>">
<input type="hidden" name="closing" value="<? echo($closing); ?>">
<? /* 個人別勤務時間帯追加 20120203 */
for ($i=0; $i<9; $i++) {
	$varname1 = "start_hour".$i."_2";
	$varname2 = "start_min".$i."_2";
	$varname3 = "end_hour".$i."_2";
	$varname4 = "end_min".$i."_2";
	$varname5 = "start_hour".$i."_4";
	$varname6 = "start_min".$i."_4";
	$varname7 = "end_hour".$i."_4";
	$varname8 = "end_min".$i."_4";

	echo("<input type=\"hidden\" name=\"{$varname1}\" value=\"{$$varname1}\">\n");
	echo("<input type=\"hidden\" name=\"{$varname2}\" value=\"{$$varname2}\">\n");
	echo("<input type=\"hidden\" name=\"{$varname3}\" value=\"{$$varname3}\">\n");
	echo("<input type=\"hidden\" name=\"{$varname4}\" value=\"{$$varname4}\">\n");
	echo("<input type=\"hidden\" name=\"{$varname5}\" value=\"{$$varname5}\">\n");
	echo("<input type=\"hidden\" name=\"{$varname6}\" value=\"{$$varname6}\">\n");
	echo("<input type=\"hidden\" name=\"{$varname7}\" value=\"{$$varname7}\">\n");
	echo("<input type=\"hidden\" name=\"{$varname8}\" value=\"{$$varname8}\">\n");

}
?>
</form>
<?
require_once("about_comedix.php");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 職員登録権限チェック
$reg_auth = check_authority($session, 19, $fname);
if ($reg_auth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 入力チェック
if ($salary_id != "") {
    if (preg_match("/^[a-zA-Z0-9]*$/", $salary_id) == 0) {
        echo("<script type=\"text/javascript\">alert('給与IDは半角英数で入力してください。');</script>\n");
        echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
        exit;
    }
}
if ($org_corp_cd != "") {
    if (preg_match("/^[a-zA-Z0-9]*$/", $org_corp_cd) == 0) {
        echo("<script type=\"text/javascript\">alert('所属元法人コードは半角英数で入力してください。');</script>\n");
        echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
        exit;
    }
}
if ($wage != "1" && $wage != "2" && $wage != "3" && $wage != "4" && $wage != "5") {
    echo("<script type=\"text/javascript\">alert('給与支給区分を選択してください。');</script>\n");
    echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
    exit;
}

for ($i = 1; $i <= 4; $i++) {
    $var_name = "unit_price$i";
    if ($$var_name != "") {
        if (preg_match("/^[1-9]\d*/", $$var_name) == 0) {
            echo("<script type=\"text/javascript\">alert('時間単価は半角数字で入力してください。');</script>\n");
            echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
            exit;
        }
    }
}
if ($unit_price5 != "") {
    if (preg_match("/^[1-9]\d*/", $unit_price5) == 0) {
        echo("<script type=\"text/javascript\">alert('交通費単価は半角数字で入力してください。');</script>\n");
        echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
        exit;
    }
}
if ($unit_price6 != "") {
    if (preg_match("/^[1-9]\d*/", $unit_price6) == 0) {
        echo("<script type=\"text/javascript\">alert('自転車等の交通費1日単価は半角数字で入力してください。');</script>\n");
        echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
        exit;
    }
}
if ($unit_price7 != "") {
    if (preg_match("/^[1-9]\d*/", $unit_price7) == 0) {
        echo("<script type=\"text/javascript\">alert('定期代は半角数字で入力してください。');</script>\n");
        echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
        exit;
    }
}
if ($irrg_app == "2" && $irrg_type == "") {
    echo("<script type=\"text/javascript\">alert('期間を選択してください。');</script>\n");
    echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
    exit;
}

// ********** oose add start ********
if (($duty_form != "1") && ($duty_form != "2") && ($duty_form != "3")) {
    echo("<script type=\"text/javascript\">alert('雇用・勤務形態を選択してください。');</script>\n");
    echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
    exit;
}

if ($employment_working_other_flg == "t" && empty($duty_form_type)) {
	echo("<script type=\"text/javascript\">alert('雇用区分を選択してください。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}
//if ($duty_day_flg != "1" && $duty_day_flg != "2" && $duty_day_flg != "3" && $duty_day_flg != "4" && $duty_day_flg != "5" && $duty_day_flg != "6" && $duty_day_flg != "7") {
//  echo("<script type=\"text/javascript\">alert('勤務日を選択してください。');</script>\n");
//  echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
//  exit;
//}
//if ($night_duty_flg != "1" && $night_duty_flg != "2" && $night_duty_flg != "3") {
//  echo("<script type=\"text/javascript\">alert('夜勤の有無を選択してください。');</script>\n");
//  echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
//  exit;
//}
//if ($night_staff_cnt != "") {
//  if (preg_match("/^[1-9]\d*/", $night_staff_cnt) == 0) {
//      echo("<script type=\"text/javascript\">alert('夜勤従事者への計上は半角数字で入力してください。');</script>\n");
//      echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
//      exit;
//  }
//}
// *********** oose add end *********

// 入力チェック
if (($day1_hour == "--" && $day1_min != "--") || ($day1_hour != "--" && $day1_min == "--") ||
        ($am_hour == "--" && $am_min != "--") || ($am_hour != "--" && $am_min == "--") ||
        ($pm_hour == "--" && $pm_min != "--") || ($pm_hour != "--" && $pm_min == "--") ||
        ($week_hour == "--" && $week_min != "--") || ($week_hour != "--" && $week_min == "--")) {
        echo("<script type=\"text/javascript\">alert('時間と分の一方のみは登録できません。');</script>");
	echo("<script language=\"javascript\">document.items.submit();</script>");
    exit;
}
//個人別勤務時間帯
for ($i=0; $i<9; $i++) {
	$varname1 = "start_hour".$i."_2";
	$varname2 = "start_min".$i."_2";
	$varname3 = "end_hour".$i."_2";
	$varname4 = "end_min".$i."_2";
	$varname5 = "start_hour".$i."_4";
	$varname6 = "start_min".$i."_4";
	$varname7 = "end_hour".$i."_4";
	$varname8 = "end_min".$i."_4";

	if (($$varname1 == "--" && $$varname2 != "--") || ($$varname1 != "--" && $$varname2 == "--") ||
			($$varname3 == "--" && $$varname4 != "--") || ($$varname3 != "--" && $$varname4 == "--") ||
			($$varname5 == "--" && $$varname6 != "--") || ($$varname5 != "--" && $$varname6 == "--") ||
			($$varname7 == "--" && $$varname8 != "--") || ($$varname7 != "--" && $$varname8 == "--") ) {
		echo("<script type=\"text/javascript\">alert('時間と分の一方のみは登録できません。');</script>");
		echo("<script language=\"javascript\">document.items.submit();</script>");
		exit;
	}
	if (($$varname1 == "--"  && $$varname3 != "--") || ($$varname1 != "--"  && $$varname3 == "--") ||
			($$varname5 == "--"  && $$varname7 != "--") || ($$varname5 != "--"  && $$varname7 == "--")) {
		echo("<script type=\"text/javascript\">alert('開始時刻と終了時刻の一方のみは登録できません。');</script>");
		echo("<script language=\"javascript\">document.items.submit();</script>");
		exit;
	}
	if (($$varname1 == "--"  && $$varname5 != "--")) {
		echo("<script type=\"text/javascript\">alert('休憩時間のみは登録できません。');</script>");
		echo("<script language=\"javascript\">document.items.submit();</script>");
		exit;
	}
}

if ($paid_hol_start_yr == "-" && $paid_hol_start_mon == "-" && $paid_hol_start_day == "-") {
    $paid_hol_start_date = "";
} else if ($paid_hol_start_yr != "-" && $paid_hol_start_mon != "-" && $paid_hol_start_day != "-") {
    if (!checkdate($paid_hol_start_mon, $paid_hol_start_day, $paid_hol_start_yr)) {
        echo("<script type=\"text/javascript\">alert('有給付与起算日が不正です。');</script>\n");
        echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
        exit;
    }
    $paid_hol_start_date = "$paid_hol_start_yr$paid_hol_start_mon$paid_hol_start_day";
} else {
    echo("<script type=\"text/javascript\">alert('有給付与起算日が不正です。');</script>\n");
    echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
    exit;
}

// 登録値の編集
if ($irrg_app == "1") {$irrg_type = "";}
if ($hol_minus != "t") {$hol_minus = "f";}
if ($no_overtime != "t") {$no_overtime = "f";}
if ($day1_hour == "--") {
    $day1_hour = "";
}
if ($day1_min == "--") {
    $day1_min = "";
}
$specified_time = "$day1_hour$day1_min";
if ($am_hour == "--") {
    $am_hour = "";
}
if ($am_min == "--") {
    $am_min = "";
}
if ($pm_hour == "--") {
    $pm_hour = "";
}
if ($pm_min == "--") {
    $pm_min = "";
}
$am_time = "$am_hour$am_min";
$pm_time = "$pm_hour$pm_min";

if ($week_hour == "--") {
    $week_hour = "";
}
if ($week_min == "--") {
    $week_min = "";
}
$week_time = "$week_hour$week_min";


// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin transaction");

// 変則労働適用時の所定労働時間が登録されているかチェック
if ($irrg_type != "") {
    $col_name = ($irrg_type == "1") ? "irrg_time_w" : "irrg_time_m";
    $sql = "select $col_name from timecard";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $irrg_time = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, $col_name): "";
    if ($irrg_time == "") {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('変則労働適用時の所定労働時間が登録されていません。\\nマスター管理者にご連絡ください。');</script>\n");
        echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
        exit;
    }
}

// 勤務条件レコードをSELECT
$sql = "SELECT COUNT(*) FROM empcond WHERE emp_id='{$emp_id}'";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

//パターンID未設定時はnullを指定
if (empty($atdptn_id) || $atdptn_id == "--"){
    $atdptn_id = null;
}
if (empty($other_post_flg) || $other_post_flg == ""){
    $other_post_flg = null;
}
if ($closing == "--"){
	$closing = null;
}

// データ
$data = array(
        'emp_id'                => $emp_id,
        'wage'                  => $wage,
        'unit_price1'           => $unit_price1,
        'unit_price2'           => $unit_price2,
        'unit_price3'           => $unit_price3,
        'unit_price4'           => $unit_price4,
        'irrg_type'             => $irrg_type,
        'tmcd_group_id'         => $tmcd_group_id,
        'hol_minus'             => $hol_minus,
        'duty_form'             => $duty_form,
        'atdptn_id'             => $atdptn_id,
        'no_overtime'           => $no_overtime,
        'specified_time'        => $specified_time,
        'paid_hol_tbl_id'       => $paid_hol_tbl_id,
        'csv_layout_id'         => $csv_layout_id,
        'paid_hol_start_date'   => $paid_hol_start_date,
        'am_time'               => $am_time,
        'pm_time'               => $pm_time,
        'unit_price5'           => $unit_price5,
        'unit_price6'           => $unit_price6,
        'unit_price7'           => $unit_price7,
        'commuting_distance'    => p($commuting_distance),
        'commuting_type'        => p($commuting_type),
        'other_post_flg'        => $other_post_flg,
        'week_time'             => $week_time,
        'salary_id'             => $salary_id,
        'closing'               => $closing,
        'org_corp_cd'           => $org_corp_cd
);

if($employment_working_other_flg == "t"){
	$data += array('duty_form_type' => $duty_form_type);
}

// 勤務条件レコードを作成
if (pg_fetch_result($sel,0,0) <= 0) {
    $sql = "INSERT INTO empcond (". implode(",",array_keys($data)) .") VALUES (";
    $ins = insert_into_table($con, $sql, array_values($data), $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

// 勤務条件レコードを更新
else {
    $sql = "UPDATE empcond SET";
    $cond = "WHERE emp_id='{$emp_id}'";
    unset($data['emp_id']);
    $upd = update_set_table($con, $sql, array_keys($data), array_values($data), $cond, $fname);
    if ($upd == 0) {
        pg_query("rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

/* 個人別勤務時間帯履歴追加 20130206 */
//現在の登録されている時刻から変わっている場合、履歴へ登録する
$arr_officehours = array();
$sql = "select * from empcond_officehours";
$cond = "where emp_id = '$emp_id' ";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

while ($row = pg_fetch_assoc($sel)) {
    $weekday = $row["weekday"];
    $arr_officehours[$weekday] = $row;
}
//変更確認フラグ
$chg_flg = false;
//登録済みの個人別時間帯がない場合でも、一度履歴が登録された場合の対応
$already_histdata = false;
if (count($arr_officehours) == 0) {
    $sql = "select count(*) as cnt from empcond_officehours_history";
    $cond = "where emp_id = '$emp_id' ";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $cnt = pg_fetch_result($sel, 0, "cnt");
    if ($cnt > 0) {
        $already_histdata = true;
    }
}
//個人別時間帯がある場合、レコードがあるので確認する。ない場合は元々空白なので履歴も出力しない
if(count($arr_officehours) > 0 || $already_histdata == true) {
    //基本(8)以外の曜日のデータ件数 日0,月1,土6、祝7
    $sub_rec_cnt = 0;
    $arr_officehours_i = array();
    for ($i=0; $i<9; $i++) {
        $varname1 = "start_hour".$i."_2";
        $varname2 = "start_min".$i."_2";
        $varname3 = "end_hour".$i."_2";
        $varname4 = "end_min".$i."_2";
        $varname5 = "start_hour".$i."_4";
        $varname6 = "start_min".$i."_4";
        $varname7 = "end_hour".$i."_4";
        $varname8 = "end_min".$i."_4";

        if ($$varname1 != "--") {
            $arr_officehours_i[$i]["officehours2_start"] = $$varname1.":".$$varname2;
            $arr_officehours_i[$i]["officehours2_end"] = $$varname3.":".$$varname4;
            $arr_officehours_i[$i]["officehours4_start"] = ($$varname5 != "--") ? $$varname5.":".$$varname6 : "";
            $arr_officehours_i[$i]["officehours4_end"] = ($$varname7 != "--") ? $$varname7.":".$$varname8 : "";
        }
        //履歴の曜日別件数
        if ($arr_officehours[$i]["officehours2_start"] != "") {
            if ($i < 8) {
                $sub_rec_cnt++;
            }
        }

    }
    for ($i=0; $i<9; $i++) {
        if ($arr_officehours[$i]["officehours2_start"] != $arr_officehours_i[$i]["officehours2_start"] ||
                $arr_officehours[$i]["officehours2_end"] != $arr_officehours_i[$i]["officehours2_end"] ||
                $arr_officehours[$i]["officehours4_start"] != $arr_officehours_i[$i]["officehours4_start"] ||
                $arr_officehours[$i]["officehours4_end"] != $arr_officehours_i[$i]["officehours4_end"]) {
            $chg_flg = true;
            break;
        }
    }
}
if ($chg_flg) {
    $histdate = date("Y-m-d H:i:s");

    $sql = "INSERT INTO empcond_officehours_history (emp_id, weekday, officehours2_start, officehours2_end, officehours4_start, officehours4_end, sub_rec_cnt, histdate) VALUES (";
    $content = array($emp_id, 0, $arr_officehours[8]["officehours2_start"],  $arr_officehours[8]["officehours2_end"], $arr_officehours[8]["officehours4_start"], $arr_officehours[8]["officehours4_end"], $sub_rec_cnt, $histdate);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if ($sub_rec_cnt > 0) {
//hist_id確認
        $sql = "select max(empcond_officehours_history_id) as hist_id from empcond_officehours_history";
        $cond = "where emp_id = '$emp_id'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $hist_id = pg_fetch_result($sel, 0, "hist_id");

        for ($i=0; $i<8; $i++) {
            if ($arr_officehours[$i]["officehours2_start"] != "") {
                $sql = "INSERT INTO empcond_officehours_history_sub (empcond_officehours_history_sub_id, emp_id, weekday, officehours2_start, officehours2_end, officehours4_start, officehours4_end) VALUES (";
                $content = array($hist_id, $emp_id, $i, $arr_officehours[$i]["officehours2_start"],  $arr_officehours[$i]["officehours2_end"], $arr_officehours[$i]["officehours4_start"], $arr_officehours[$i]["officehours4_end"]);
                $ins = insert_into_table($con, $sql, $content, $fname);
                if ($ins == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
            }
        }
    }
}

/* 個人別勤務時間帯追加 20120203 */
//削除後登録
$sql = "delete from empcond_officehours";
$cond = "where emp_id = '$emp_id' ";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

for ($i=0; $i<9; $i++) {
	$varname1 = "start_hour".$i."_2";
	$varname2 = "start_min".$i."_2";
	$varname3 = "end_hour".$i."_2";
	$varname4 = "end_min".$i."_2";
	$varname5 = "start_hour".$i."_4";
	$varname6 = "start_min".$i."_4";
	$varname7 = "end_hour".$i."_4";
	$varname8 = "end_min".$i."_4";

	if ($$varname1 != "--") {
		$rest_start = ($$varname5 != "--") ? $$varname5.":".$$varname6 : "";
		$rest_end = ($$varname7 != "--") ? $$varname7.":".$$varname8 : "";
		$content = array($emp_id, $i, $$varname1.":".$$varname2, $$varname3.":".$$varname4, $rest_start, $rest_end);
		$sql = "INSERT INTO empcond_officehours (emp_id, weekday, officehours2_start, officehours2_end, officehours4_start, officehours4_end) VALUES (";
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 勤務条件画面を再表示
$url_key1 = urlencode($key1);
$url_key2 = urlencode($key2);
echo("<script type=\"text/javascript\">location.href = 'employee_condition_setting.php?session=$session&emp_id=$emp_id&key1=$url_key1&key2=$url_key2&class_cond=$class_cond&atrb_cond=$atrb_cond&dept_cond=$dept_cond&room_cond=$room_cond&job_cond=$job_cond&page=$page&view=$view&emp_o=$emp_o';</script>");
?>
</body>
