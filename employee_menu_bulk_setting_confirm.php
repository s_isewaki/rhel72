<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="employee_menu_bulk_setting.php" method="post">
<?
foreach ($_POST as $key => $val) {
	if (is_array($val)) {
		foreach ($val as $val2) {
			echo("<input type=\"hidden\" name=\"{$key}[]\" value=\"$val2\">\n");
		}
	} else {
		echo("<input type=\"hidden\" name=\"$key\" value=\"$val\">\n");
	}
}
?>
<input type="hidden" name="back" value="t">
<input type="hidden" name="result" value="">
</form>
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (!is_array($tgt_job)) {
	echo("<script type=\"text/javascript\">alert('職種が選択されていません。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (!is_array($tgt_st)) {
	echo("<script type=\"text/javascript\">alert('役職が選択されていません。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if ($action_mode == "1" && $menu_group_id == "") {
	echo("<script type=\"text/javascript\">alert('メニューグループが選択されていません。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if ($action_mode == "2") {
	$valid_menu_ids = array();
	foreach ($menu_ids as $menu_id) {
		if ($menu_id == "-") {
			continue;
		}

		if (in_array($menu_id, $valid_menu_ids)) {
			echo("<script type=\"text/javascript\">alert('ヘッダメニューの選択肢が重複しています。');</script>");
			echo("<script type=\"text/javascript\">document.items.submit();</script>");
			exit;
		}

		array_push($valid_menu_ids, $menu_id);
	}
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 「メニューグループで設定」の場合
if ($action_mode == "1") {
	$sql = "select * from menugroup";
	$cond = "where group_id = $menu_group_id";
	$menu_sel = select_from_table($con, $sql, $cond, $fname);
	if ($menu_sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
	$menu_groups = pg_fetch_assoc($menu_sel);
	$sidemenu_show_flg = $menu_groups["sidemenu_show_flg"];
	$sidemenu_position = $menu_groups["sidemenu_position"];

// 「個別に設定」の場合
} else {
	$menu_group_id = "0";
}

foreach ($tgt_job as $tmp_job) {
	foreach ($tgt_st as $tmp_st) {

		// 該当する職員一覧を取得
		$sql = "select emp_id from empmst";
		$cond = "where emp_job = $tmp_job and emp_st = $tmp_st and exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f')";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 職員一覧をループ
		while ($row = pg_fetch_array($sel)) {
			$tmp_emp_id = $row["emp_id"];

			// ヘッダメニュー設定をDELETE
			$sql = "delete from headermenu";
			$cond = "where emp_id = '$tmp_emp_id'";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			// 「メニューグループで設定」の場合
			if ($action_mode == "1") {
				for ($i = 1; $i <= 11; $i++) {
					$menu_num = 'menu_id_' . $i;
					if ($menu_groups[$menu_num] == "") {
						break;
					}

					$sql = "insert into headermenu (emp_id, menu_id, menu_order) values (";
					$content = array($tmp_emp_id, $menu_groups[$menu_num], $i);
					$ins = insert_into_table($con, $sql, $content, $fname);
					if ($ins == 0) {
						pg_query($con, "rollback");
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
				}

				// menu_group_idの登録
				$sql = "update option set";
				$set = array("menu_group_id");
				$setvalue = array($menu_group_id);
				$cond = "where emp_id = '$tmp_emp_id'";
				$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
				if ($upd == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}

			// 「個別に設定」の場合
			} else {
				$menu_order = 1;
				foreach ($valid_menu_ids as $menu_id) {
					$sql = "insert into headermenu (emp_id, menu_id, menu_order) values (";
					$content = array($tmp_emp_id, $menu_id, $menu_order);
					$ins = insert_into_table($con, $sql, $content, $fname);
					if ($ins == 0) {
						pg_query($con, "rollback");
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
					$menu_order++;
				}
			}

			// オプション情報を更新
			$sql = "update option set";
			$set = array("sidemenu_show_flg", "sidemenu_position");
			$setvalue = array($sidemenu_show_flg, $sidemenu_position);
			$cond = "where emp_id = '$tmp_emp_id'";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 権限一括設定画面を再表示
echo("<script type=\"text/javascript\">document.items.result.value = 't';</script>");
echo("<script type=\"text/javascript\">document.items.submit();</script>");
?>
</body>
