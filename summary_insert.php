<?
require_once("about_comedix.php");

// テンプレートの場合、
$summary_naiyo = str_replace("&#160;", " ", @$_REQUEST["summary_naiyo_escaped_amp"]);
$summary_naiyo = str_replace("&nbsp;", " ", $summary_naiyo);
$summary_naiyo = str_replace("&amp;", "&", $summary_naiyo);
$summary_naiyo = h($summary_naiyo);
// フリー記載の場合はそのまま
if (!trim(@$_REQUEST["summary_naiyo_escaped_amp"]) || $tmpl_id == ""){
	$summary_naiyo = @$_REQUEST["summary_naiyo"];
}
?>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<? if (@$_REQUEST["direct_registration"]) { ?>
<form name="items" action="sum_application_apply_new.php" method="post">
<? } else { ?>
<form name="items" action="summary_new.php" method="post">
<? } ?>
<?
//========================================
//ファイルの読み込み
//========================================
// メドレポート
require("about_validation.php");
require("get_values.php");
require("label_by_profile_type.ini");
// ワークフロー
require_once("./conf/sql.inf");
require_once("summary_common.ini");


$fname = $PHP_SELF;


//========================================
//セッションチェック
//========================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//========================================
//権限チェック
//========================================
$dept = check_authority($session,57,$fname);
if($dept == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 添付ファイルの確認
if (!is_dir("apply"))          mkdir("apply", 0755);
if (!is_dir("summary/tmp"))    mkdir("summary/tmp", 0755);
if (!is_dir("summary/attach")) mkdir("summary/attach", 0755);

$filename = (array)@$filename;
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$ext = strrchr($filename[$i], ".");

	$tmp_filename = "summary/tmp/{$session}_{$tmp_file_id}{$ext}";
	if (!is_file($tmp_filename)) {
		echo("<script language=\"javascript\">alert('添付ファイルが削除された可能性があります。\\n再度添付してください。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
}


//========================================
//ＤＢのコネクション作成
//========================================
$con = connect2db($fname);

$cre_date = $sel_yr.$sel_mon.$sel_day;

$sql = "select diag_div from divmst where div_id = ".(int)$_REQUEST["sel_diag"];
$sel = select_from_table($con,$sql,"",$fname);
$tmp = pg_fetch_result($sel, 0, 0);
if ($tmp) { $_REQUEST["sel_diag"] = $tmp; $sel_diag = $tmp; }


?>

<? // メドレポート ?>
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="date" value="<?=$cre_date?>"><?//summary_new用?>
<input type="hidden" name="ymd" value="<?=$cre_date?>"><?//sum_application_apply_new用?>
<input type="hidden" name="summary_problem" value="<?=$summary_problem?>">
<input type="hidden" name="diag" value="<?=$sel_diag?>">
<input type="hidden" name="priority" value="<?=$priority?>">
<input type="hidden" name="pt_id" value="<?=$pt_id?>"><?//summary_new用?>
<input type="hidden" name="ptif_id" value="<?=$pt_id?>"><?//sum_application_apply_new用?>
<input type="hidden" name="xml_file" value="<?=$xml_file?>">
<input type="hidden" name="tmpl_id" value="<?=$tmpl_id?>">
<input type="hidden" name="problem_id" value="<?=$problem_id?>">
<input type="hidden" name="summary_naiyo" value="<?=h($summary_naiyo)?>">
<input type="hidden" name="sect_id" value="<?=$sect_id?>">
<input type="hidden" name="outreg" value="<?=$outreg?>">
<input type="hidden" name="seiki_emp_id" value="<?=$seiki_emp_id?>">
<input type="hidden" name="dairi_emp_id" value="<?=$dairi_emp_id?>">
<input type="hidden" name="direct_registration" value="<?=@$direct_registration?>">

<? // ワークフロー ?>

<?
$filename = (array)@$filename;
foreach (@$filename as $tmp_filename) {
	echo("<input type=\"hidden\" name=\"filename[]\" value=\"$tmp_filename\">\n");
}
$file_id = (array)@$file_id;
foreach ($file_id as $tmp_file_id) {
	echo("<input type=\"hidden\" name=\"file_id[]\" value=\"$tmp_file_id\">\n");
}
for ($i = 1; $i <= (int)@@$approve_num; $i++) {
	$varname = "regist_emp_id$i";
	echo("<input type=\"hidden\" name=\"regist_emp_id$i\" value=\"{$$varname}\">\n");
}
for ($i = 1; $i <= @$precond_num; $i++) {
	$varname = "precond_apply_id$i";
	echo("<input type=\"hidden\" name=\"precond_apply_id$i\" value=\"{$$varname}\">\n");
}
for ($i = 1; $i <= @$precond_num; $i++) {
	$varname = "precond_wkfw_id$i";
	echo("<input type=\"hidden\" name=\"precond_wkfw_id$i\" value=\"{$$varname}\">\n");
}
?>
</form>
<?

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];
// 来院/来所
$come_title = $_label_by_profile["COME"][$profile_type];
// 担当医/担当者１
$doctor_title = $_label_by_profile["DOCTOR"][$profile_type];

//========================================
// 入力チェック
//========================================
if($sect_id ==""){
	echo("<script language=\"javascript\">alert(\"{$section_title}を入力してください\");</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if($summary_problem == ""){
	echo("<script language=\"javascript\">alert(\"プロブレムを入力してください\");</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if($summary_naiyo == "" && !@$direct_registration){
	echo("<script language=\"javascript\">alert(\"登録内容を入力してください\");</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

// 未指定の承認者を許可する
if ($wkfw_id) {
	$tmp_arr_apv = array();
	$tmp_apv_order = 0;
	$tmp_pre_apv_order = 0;
	for ($i = 1; $i <= $approve_num; $i++) {
		$tmp_regist_emp_id_var = "regist_emp_id$i";
		$tmp_st_div_var = "st_div$i";
		$tmp_parent_pjt_id_var = "parent_pjt_id$i";
		$tmp_child_pjt_id_var = "child_pjt_id$i";
		$tmp_apv_order_var = "apv_order$i";
		$tmp_apv_sub_order_var = "apv_sub_order$i";
		$tmp_multi_apv_flg_var = "multi_apv_flg$i";
		$tmp_next_notice_div_var = "next_notice_div$i";

		$tmp_org_apv_order = $$tmp_apv_order_var;
		$tmp_org_pst_approve_num_var = "pst_approve_num$tmp_org_apv_order";
		$tmp_radio_emp_id_var = "radio_emp_id$tmp_org_apv_order";

		if ($$tmp_regist_emp_id_var != "" || ($$tmp_org_pst_approve_num_var != "" && $$tmp_radio_emp_id_var != "")) {
			if ($$tmp_apv_order_var != $tmp_pre_apv_order) {
				$tmp_apv_order++;
				$tmp_apv_sub_order = 1;
				$tmp_pre_apv_order = $$tmp_apv_order_var;
			} else {
				$tmp_apv_sub_order += 1;
			}

			if ($tmp_apv_sub_order == 2) {
				$tmp_arr_apv[count($tmp_arr_apv) - 1]["apv_sub_order"] = 1;
			}

			$tmp_arr_apv[] = array(
				"regist_emp_id" => $$tmp_regist_emp_id_var,
				"st_div" => $$tmp_st_div_var,
				"parent_pjt_id" => $$tmp_parent_pjt_id_var,
				"child_pjt_id" => $$tmp_child_pjt_id_var,
				"apv_order" => $tmp_apv_order,
				"apv_sub_order" => (($tmp_apv_sub_order == 1) ? null : $tmp_apv_sub_order),
				"multi_apv_flg" => $$tmp_multi_apv_flg_var,
				"next_notice_div" => $$tmp_next_notice_div_var,
				"org_apv_order" => $tmp_org_apv_order
			);
		}

		if ($$tmp_org_pst_approve_num_var != "" && $$tmp_radio_emp_id_var != "" && $tmp_apv_order != $tmp_org_apv_order) {
			$tmp_pst_approve_num_var = "pst_approve_num$tmp_apv_order";
			$$tmp_pst_approve_num_var = $$tmp_org_pst_approve_num_var;

			$base_varnames = array("pst_emp_id", "pst_st_div", "pst_parent_pjt_id", "pst_child_pjt_id");
			for ($j = 1; $j <= $$tmp_pst_approve_num_var; $j++) {
				foreach ($base_varnames as $base_varname) {
					$org_varname = "$base_varname{$tmp_org_apv_order}_{$j}";
					$new_varname = "$base_varname{$tmp_apv_order}_{$j}";
					$$new_varname = $$org_varname;
					unset($$org_varname);
				}
			}

			unset($$tmp_org_pst_approve_num_var);
		}

		unset($$tmp_regist_emp_id_var);
		unset($$tmp_st_div_var);
		unset($$tmp_parent_pjt_id_var);
		unset($$tmp_child_pjt_id_var);
		unset($$tmp_apv_order_var);
		unset($$tmp_apv_sub_order_var);
		unset($$tmp_multi_apv_flg_var);
		unset($$tmp_next_notice_div_var);
	}

	$approve_num = count($tmp_arr_apv);
	$i = 1;
	foreach ($tmp_arr_apv as $tmp_apv) {
		$tmp_regist_emp_id_var = "regist_emp_id$i";
		$tmp_st_div_var = "st_div$i";
		$tmp_parent_pjt_id_var = "parent_pjt_id$i";
		$tmp_child_pjt_id_var = "child_pjt_id$i";
		$tmp_apv_order_var = "apv_order$i";
		$tmp_apv_sub_order_var = "apv_sub_order$i";
		$tmp_multi_apv_flg_var = "multi_apv_flg$i";
		$tmp_next_notice_div_var = "next_notice_div$i";
		$tmp_org_apv_order_var = "org_apv_order$i";

		$$tmp_regist_emp_id_var = $tmp_apv["regist_emp_id"];
		$$tmp_st_div_var = $tmp_apv["st_div"];
		$$tmp_parent_pjt_id_var = $tmp_apv["parent_pjt_id"];
		$$tmp_child_pjt_id_var = $tmp_apv["child_pjt_id"];
		$$tmp_apv_order_var = $tmp_apv["apv_order"];
		$$tmp_apv_sub_order_var = $tmp_apv["apv_sub_order"];
		$$tmp_multi_apv_flg_var = $tmp_apv["multi_apv_flg"];
		$$tmp_next_notice_div_var = $tmp_apv["next_notice_div"];
		$$tmp_org_apv_order_var = $tmp_apv["org_apv_order"];

		$i++;
	}
	unset($tmp_arr_apv);
}

//========================================
// メドレポート 診療記録ＩＤを採番
//========================================
pg_query($con, "begin");

// 採番テーブルに当該患者のレコードがなければ作成
$sql = "insert into summary_id_num select '$pt_id', (select coalesce(max(summary_id), 0) from summary where ptif_id = '$pt_id') + 10 where not exists (select * from summary_id_num where ptif_id = '$pt_id');";
$ins = delete_from_table($con, $sql, "", $fname);  // 適当な関数がないので代用
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	summary_common_show_error_page_and_die();
}

$sql = "select summary_id from summary_id_num";
$cond = "where ptif_id = '$pt_id' for update";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	summary_common_show_error_page_and_die();
}
$summary_id = intval(pg_fetch_result($sel, 0, "summary_id")) + 1;

$sql = "update summary_id_num set";
$set = array("summary_id");
$setvalue = array($summary_id);
$cond = "where ptif_id = '$pt_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	summary_common_show_error_page_and_die();
}

pg_query($con, "commit");



pg_query($con, "begin");

//========================================
// 同一ユーザがレポート登録処理を並列実行するのは不可
//========================================
if($tmpl_id != ""){
	$sql =
		" select oid, ptif_id, smry_xml, smry_html from sum_xml".
		" where xml_file_name = '".pg_escape_string($xml_file)."'".
		" for update";
	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		summary_common_show_error_page_and_die();
	}

	$tmp_record_count = pg_num_rows($sel);
	$has_error = false;

	// ユーザの一時レコードが1件もなければエラー
	if ($tmp_record_count === 0) {
		$has_error = true;
	} else {
		$first_ptif_id = pg_fetch_result($sel, 0, "ptif_id");
		$first_smry_xml = pg_fetch_result($sel, 0, "smry_xml");
		$first_smry_html = pg_fetch_result($sel, 0, "smry_html");

		// 別患者の一時レコードができていたらエラー
		if ($first_ptif_id !== $pt_id) {
			$has_error = true;
		} else {
			for ($i = 1; $i < $tmp_record_count; $i++) {

				// 内容の異なる一時レコードが複数できていたらエラー
				if (pg_fetch_result($sel, $i, "ptif_id") !== $first_ptif_id ||
					pg_fetch_result($sel, $i, "smry_xml") !== $first_smry_xml ||
					pg_fetch_result($sel, $i, "smry_html") !== $first_smry_html
				) {
					$has_error = true;
					break;
				}
			}

			// すべて同じ内容であれば1件のみにする
			if ($tmp_record_count > 1) {
				$sql = "delete from sum_xml";
				$cond = "where xml_file_name = '".pg_escape_string($xml_file)."'".
						" and oid <> " . pg_fetch_result($sel, 0, "oid");
				$del = delete_from_table($con, $sql, $cond, $fname);
				if ($del == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					summary_common_show_error_page_and_die();
				}
			}
		}
	}

	if ($has_error) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script language=\"javascript\">alert(\"記録データのエラーが発生しました。レポート画面から再入力してください\");</script>\n");
		if (@$_REQUEST["direct_registration"]){
			echo "<script language=\"javascript\">window.close();</script>\n";
		} else {
			echo("<script language=\"javascript\">location.href=\"./summary_new.php?session=".$session."&p_sect_id=".$sect_id."&pt_id=".$pt_id."&diag=".urlencode($sel_diag)."\";</script>\n");
		}
		exit;
	}
}

//========================================
//プロブレムリスト存在チェック
//========================================
if($problem_id != "0")
{
	$tbl = "select count(*) from summary_problem_list ";
	$cond = "where (ptif_id = '$pt_id') and (problem_id = '$problem_id')";
	$sel = select_from_table($con,$tbl,$cond,$fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		summary_common_show_error_page_and_die();
	}
	$num=pg_result($sel,0,"count");
	if($num == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script language=\"javascript\">alert(\"指定されたプロブレムは既に削除されています\");</script>\n");
		echo("<script language=\"javascript\">document.items.submit();</script>\n");
		exit;
	}
}
$login_emp_id = get_emp_id($con, $session, $fname);

//========================================
//担当医情報存在チェック
//========================================
$dr_id = 0;
if (@$outreg == "t") {
	$sql = "select dr_id from drmst";
	$cond = "where dr_emp_id = '".pg_escape_string($seiki_emp_id)."' and dr_del_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	$dr_id = (int)@pg_fetch_result($sel, 0, "dr_id");
}

//========================================
// メドレポート 全ユニーク診療記録ＩＤを採番
//========================================

// 旧：サマリ連番の採番
// $maxsel = select_from_table($con, "select max(summary_seq) as max from summary", "", $fname);
//$summary_seq = (int)(@pg_result($maxsel,0,"max")) + 1;

// 新：サマリ連番の採番方法変更 (採番用テーブルにて管理)
require_once("summary_seqcnt.ini");
$summary_seq = get_summary_seq($con, $fname);
if($summary_seq == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	summary_common_show_error_page_and_die();
}

//========================================
// メドレポート ＤＢ登録データ作成
//========================================
$summary_naiyo = str_replace("\r\n", "\n", $summary_naiyo);
$cre_date = $sel_yr.$sel_mon.$sel_day;
if($problem_id == "") $problem_id = "0";


if($summary_naiyo =="" && @$direct_registration){
	require_once("summary_tmpl_util.ini");
	$summary_naiyo = get_tmpl_naiyo_html_default($con, $fname, $xml_file);
}

//========================================
//ＤＢ登録（メドレポート）
//========================================
$sql =
	" insert into summary (".
	" summary_seq, summary_id, ptif_id, diag_div, problem,emp_id, smry_cmt, cre_date, tmpl_id, priority, problem_id, sect_id".
	" ) values (".
	" ".$summary_seq.
	",".$summary_id.
	",'".pg_escape_string($pt_id)."'".
	",'".pg_escape_string($sel_diag)."'".
	",'".pg_escape_string($summary_problem)."'".
	",'".$seiki_emp_id."'".
	",'".pg_escape_string($summary_naiyo)."'".
	",'".$cre_date."'".
	",".((int)$tmpl_id ? (int)$tmpl_id : "null").
	",".(int)$priority.
	",".(int)$problem_id.
	",".(int)$sect_id.
	" )";
$ret = update_set_table($con, $sql, array(), null, "", $fname);
//writeLog($summary_naiyo) ; // @@@@@

//========================================
//ＤＢ登録（メドレポート）来院履歴登録
//========================================
if (@$outreg == "t") {
	$sql =
		" insert into outhist (".
		" ptif_id, outpt_out_dt, outpt_out_tm, enti_id, outpt_sect_cd, dr_id, outpt_reg_dt, outpt_reg_tm, outpt_emp_id".
		" ) values (".
		" '".pg_escape_string($pt_id)."'".
		",'".pg_escape_string($cre_date)."'".
		",'0000'".
		",1".
		",".$sect_id.
		",".$dr_id.
		",'".date("Ymd")."'".
		",'".date("His")."'".
		",'".pg_escape_string($seiki_emp_id)."'".
		" )";
	$ret = update_set_table($con, $sql, array(), null, "", $fname);
}

//========================================
// 基礎データの血液型を更新
//========================================
if($tmpl_id != ""){
	require_once("summary_tmpl_util.ini");
	update_bloodtype($con,$fname,$xml_file,$pt_id);
}


//========================================
//ＤＢ登録（ワークフロー）
//========================================
if ($wkfw_id){

	$visibility_apv_order = 1; // 稟議回覧なら、最初は１階層のみ
	// 同報なら、階層数を取得
	if ($wkfw_appr == "1"){
		for($i=1; $i<=(int)@$approve_num; $i++) {
			$tmp_apv_order_var = "apv_order" . $i;
			$visibility_apv_order = max($visibility_apv_order, $$tmp_apv_order_var);
		}
	}


	//=================
	// 職員情報取得
	//=================
	$rs = select_from_table($con, "select * from empmst where emp_id = '".$seiki_emp_id."'", "", $fname);
	$owner_emp_info = pg_fetch_all($rs);
	$emp_class     = $owner_emp_info[0]["emp_class"];
	$emp_attribute = $owner_emp_info[0]["emp_attribute"];
	$emp_dept      = $owner_emp_info[0]["emp_dept"];
	$emp_room      = $owner_emp_info[0]["emp_room"];

	//=================
	// 新規申請(apply_id)採番、未送信でも採番
	//=================

	// 新：承認連番IDの採番方法変更 (採番用テーブルにて管理)
	require_once("sum_applycnt.ini");
	$apply_id = get_apply_id($con, $fname);
	if($apply_id == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		summary_common_show_error_page_and_die();
	}

	// 旧：承認連番IDの採番
	//$apply_max_sel = select_from_table($con,"select max(apply_id) as max from sum_apply","",$fname);   // 採番方法変更に伴い不要！
	//$apply_id = (int)pg_result($apply_max_sel,0,"max") + 1;                                            // 採番方法変更に伴い不要！

	$apply_no_max_sel = select_from_table($con, "select max(apply_no) as max_apply_no from sum_apply where apply_date like '".date("Ym")."%'", "",$fname);
	$apply_no = ((int)pg_fetch_result($apply_no_max_sel, 0, "max_apply_no")) + 1;

	$sql =
		" insert into sum_apply (".
		" apply_id, wkfw_id, summary_id, apply_content, emp_id".
		",apply_stat, apply_date, delete_flg, apply_title, re_apply_id".
		",apv_fix_show_flg, apv_bak_show_flg, emp_class, emp_attribute, emp_dept".
		",apv_ng_show_flg, emp_room, draft_flg, wkfw_appr, wkfw_content_type".
		",apply_title_disp_flg, apply_no, notice_sel_flg, wkfw_history_no, wkfwfile_history_no, ptif_id".
		",dairi_emp_id, dairi_kakunin_ymdhms, update_emp_id, visibility_apv_order".
		") values (".
		" '".$apply_id."'".                // apply_id
		",'".$wkfw_id."'".                 // wkfw_id
		",'".$summary_id."'".              // summary_id
		",null".                           // apply_content
		",'".$seiki_emp_id."'".            // emp_id
		",'".($is_regist_and_send ? "1" : "0")."'". // apply_stat 1:送信
		",'".date("YmdHi")."'".            // apply_date
		",'f'".                            // delete_flg
		",null".                           // apply_title
		",null".                           // re_apply_id
		",'t'".                            // apv_fix_show_flg
		",'t'".                            // apv_bak_show_flg
		",".qurt_sql($emp_class).          // emp_class
		",".qurt_sql($emp_attribute).      // emp_attribute
		",".qurt_sql($emp_dept).           // emp_dept
		",'t'".                            // apv_ng_show_flg
		",".qurt_sql($emp_room).           // emp_room
		",'f'".                            // draft_flg
		",".qurt_sql($wkfw_appr).          // wkfw_appr
		",".qurt_sql($wkfw_content_type).  // wkfw_content_type
		",null".                           // apply_title_disp_flg
		",'".$apply_no."'".                // apply_no
		",".qurt_sql(@$rslt_ntc_div2_flg). // notice_sel_flg
		",null".                           // wkfw_history_no
		",null".                           // wkfwfile_history_no
		",".qurt_sql($pt_id).              // ptif_id
		",".qurt_sql($dairi_emp_id).       // dairi_emp_id
		",null".                           // dairi_kakunin_ymdhms
		",".qurt_sql($login_emp_id).       // update_emp_id
		",".$visibility_apv_order.         // visibility_apv_order
		")";
	$ret = update_set_table($con, $sql, array(), null, "", $fname);
	if($ret == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		summary_common_show_error_page_and_die();
	}

	//=================
	// 受信者情報
	//=================
	for($i=1; $i<=(int)@$approve_num; $i++) {
		$tmp_regist_emp_id_var = "regist_emp_id" . $i;
		$tmp_apv_order_var = "apv_order" . $i;
		$tmp_org_apv_order_var = "org_apv_order" . $i;
		$tmp_st_div_var = "st_div" . $i;
		$tmp_apv_sub_order_var = "apv_sub_order" . $i;
		$tmp_multi_apv_flg_var = "multi_apv_flg" . $i;
		$tmp_next_notice_div_var = "next_notice_div" . $i;
		$tmp_next_notice_recv_div_var = "next_notice_recv_div" . $i;
		$tmp_parent_pjt_id_var = "parent_pjt_id" . $i;
		$tmp_child_pjt_id_var = "child_pjt_id" . $i;

		$receiver_emp_info = get_empmst($con, $$tmp_regist_emp_id_var, $fname);
		$sql =
			" insert into sum_applyapv (".
			" wkfw_id, apply_id, apv_order, emp_id, apv_stat_accepted".
			",apv_stat_operated, apv_date_accepted, apv_date_operated, delete_flg, apv_comment".
			",st_div, deci_flg, emp_class, emp_attribute, emp_dept".
			",emp_st, apv_fix_show_flg, emp_room, apv_sub_order, multi_apv_flg".
			",next_notice_div, next_notice_recv_div, parent_pjt_id, child_pjt_id, other_apv_flg".
			",apv_stat_accepted_by_other, apv_stat_operated_by_other, wkfw_apv_order".
			" ) values (".
			" ".$wkfw_id.                                       // wkfw_id
			",".$apply_id.                                      // apply_id
			",".qurt_sql($$tmp_apv_order_var).                  // apv_order
			",".qurt_sql($$tmp_regist_emp_id_var).              // emp_id
			",0".                                               // apv_stat_accepted
			",0".                                               // apv_stat_operated
			",null".                                            // apv_date_accepted
			",null".                                            // apv_date_operated
			",'f'".                                             // delete_flg
			",null".                                            // apv_comment
			",".qurt_sql($$tmp_st_div_var).                     // st_div
			",'t'".                                             // deci_flg
			",".qurt_sql($receiver_emp_info[2]).                // emp_class
			",".qurt_sql($receiver_emp_info[3]).                // emp_attribute
			",".qurt_sql($receiver_emp_info[4]).                // emp_dept
			",".qurt_sql($receiver_emp_info[6]).                // emp_st
			",'t'".                                             // apv_fix_show_flg
			",".qurt_sql($receiver_emp_info[33]).               // emp_room
			",".qurt_sql($$tmp_apv_sub_order_var).              // apv_sub_order
			",".qurt_sql($$tmp_multi_apv_flg_var, "'f'").       // multi_apv_flg
			",".qurt_sql($$tmp_next_notice_div_var).            // next_notice_div
			",".qurt_sql($$tmp_next_notice_recv_div_var).       // next_notice_recv_div
			",".qurt_sql($$tmp_parent_pjt_id_var).              // parent_pjt_id
			",".qurt_sql($$tmp_child_pjt_id_var).               // child_pjt_id
			",'f'".                                             // other_apv_flg
			",0".                                               // apv_stat_accepted_by_other
			",0".                                               // apv_stat_operated_by_other
			",".$$tmp_org_apv_order_var.                        // wkfw_apv_order
			" )";
		$ret = update_set_table($con, $sql, array(), null, "", $fname);
		if($ret == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			summary_common_show_error_page_and_die();
		}
	}

	//=================
	// 受信者候補登録
	//=================
	for($i=1; $i <= (int)@$approve_num; $i++) {
		$tmp_apv_order_var = "apv_order" . $i;
		$apv_order       = @$$tmp_apv_order_var;

		$tmp_pst_approve_num_var = "pst_approve_num" . $apv_order;
		$pst_approve_num = @$$tmp_pst_approve_num_var;

		for($j=1; $j<=$pst_approve_num; $j++) {
			$tmp_pst_emp_id_var = "pst_emp_id" . $apv_order . "_" . $j;
			$tmp_pst_st_div_var = "pst_st_div" . $apv_order . "_" . $j;
			$tmp_pst_parent_pjt_id_var = "pst_parent_pjt_id" . $apv_order . "_" . $j;
			$tmp_pst_child_pjt_id_var = "pst_child_pjt_id" . $apv_order . "_" . $j;

			$sql =
				" insert into sum_applyapvemp (".
				" apply_id, apv_order, person_order, emp_id, delete_flg, st_div, parent_pjt_id, child_pjt_id".
				" ) values (".
				" ". $apply_id. // apply_id
				",". qurt_sql($apv_order).// apv_order
				",". $j.// person_order
				",". qurt_sql(@$$tmp_pst_emp_id_var).// emp_id
				",'f'". // delete_flg
				",". qurt_sql(@$$tmp_pst_st_div_var).// st_div
				",". qurt_sql(@$$tmp_pst_parent_pjt_id_var).// parent_pjt_id
				",". qurt_sql(@$$tmp_pst_child_pjt_id_var).// child_pjt_id
				")";
			$ret = update_set_table($con, $sql, array(), null, "", $fname);
			if($ret == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				summary_common_show_error_page_and_die();
			}
		}
	}

	//=================
	// 添付ファイル登録
	//=================
	$no = 1;
	if (is_array($filename)){
		foreach ($filename as $tmp_filename) {
			$sql =
				" insert into sum_applyfile (".
				" apply_id, applyfile_no, applyfile_name, delete_flg".
				" ) values (".
				" ".$apply_id.
				",".$no.
				",'".pg_escape_string($tmp_filename)."'".
				",'f'".
				" )";
			$ret = update_set_table($con, $sql, array(), null, "", $fname);
			$no++;
		}
	}
}


//========================================
// テンプレートのものは、この時点で、ＸＭＬデータが、
// create_tmpl_xml.phpにより、sum_xmlテーブルに"wk_*****"という名前で仮作成されている。
// ここで「正規登録」する。
// ただし、
// ・ワークフローでないかもしれない
// ・テンプレートでないかもしれない
// このため一括で処理
//========================================
$new_filename = $summary_seq.".xml"; // ワークフローなしの場合
if (@$apply_id!="") $new_filename = $summary_seq."_".$apply_id.".xml"; // ワークフローありの場合

//=================
// 念のための削除処理
// apply_idの無い(ワークフローでない)データは履歴を残さない。（履歴を認識できない。）よって物理削除。
// apply_idのある(ワークフローである)アクティブなXML情報があれば論理削除
//=================
$sql =
	" delete from sum_xml".
	" where xml_file_name = '".pg_escape_string($new_filename)."'".
	" and (apply_id is null or apply_id = 0)";
$upd = update_set_table($con, $sql, array(), null, "", $fname);

if (@$apply_id!=""){
	$sql =
		" update sum_xml set".
		" delete_flg = 1".
		" where xml_file_name = '".pg_escape_string($new_filename)."'";
	$upd = update_set_table($con, $sql, array(), null, "", $fname);
}

//=================
// テンプレートがあればワークフローの有無に関わらず、正規登録する
//=================
if($tmpl_id != ""){
	$sql =
		" update sum_xml set".
		" xml_file_name = '".pg_escape_string($new_filename)."'".
		",is_work_data = '0'".
		",summary_id = ". $summary_id.
		",summary_seq = ". $summary_seq.
		",apply_id = ".(@$apply_id != "" ? (int)$apply_id : "null").
		",update_timestamp = current_timestamp".
		",delete_flg = 0".
		" where xml_file_name = '".pg_escape_string($xml_file)."'";
	$upd = update_set_table($con, $sql, array(), null, "", $fname);
}

//==============================================
// テンプレートのとき、ワークシート用スケジュールが仮登録されているかもしれない。
// もしスケジュール仮登録があれば、スケジュールを正規登録する
//==============================================
if($tmpl_id != ""){
	$sql =
		" update sot_summary_schedule set".
		" temporary_update_flg = 0".
		",summary_id = ". $summary_id.
		",summary_seq = ". $summary_seq.
		",apply_id = ". (@$apply_id != "" ? (int)$apply_id : "null").
		",wkfw_id = ". (@$wkfw_id != "" ? (int)$wkfw_id : "null").
		",xml_file_name = '".$new_filename."'".
		",delete_flg = false".
		" where xml_file_name = '" . $xml_file ."'";
	$upd = update_set_table($con, $sql, array(), null, "", $fname);
}


//========================================
// ＤＢコミット
//========================================
pg_query($con, "commit");

//========================================
// ＤＢ登録（ログ）
// スループット向上を期待してトランザクションには入れない
//========================================
summary_common_write_operate_log("modify", $con, $fname, $session, $login_emp_id, "", $pt_id);

pg_close($con);


//========================================
// 添付ファイルを正規の格納場所へ移動
//========================================
if ($wkfw_id){
	for ($i = 0; $i < count($filename); $i++) {
		$ext = strrchr($filename[$i], ".");
		@copy("summary/tmp/{$session}_".@$file_id[$i].$ext, "summary/attach/".$apply_id."_".($i+1).$ext);
	}
	foreach (glob("summary/tmp/{$session}_*.*") as $tmpfile) unlink($tmpfile);
}
//========================================
//処理成功時のJavaScript
//========================================
if (@$_REQUEST["direct_registration"]){
	echo "<script language=\"javascript\">if (window.opener && window.opener.location) window.opener.location.reload();</script>\n";
	echo "<script language=\"javascript\">window.close();</script>\n";
} else {
	echo("<script language=\"javascript\">parent.hidari.location.href=\"./summary_rireki_table.php?session=$session&pt_id=$pt_id&st_date=$cre_date\";</script>\n");
	echo("<script language=\"javascript\">location.href=\"./summary_new.php?session=".$session."&p_sect_id=".$sect_id."&pt_id=".$pt_id."&diag=".urlencode($sel_diag)."\";</script>\n");
}

function qurt_sql($v, $def = "null"){
	if (is_array($v)) $v = implode(",", $v);
	if (trim($v) == "") return $def;
	return "'" . pg_escape_string($v) . "'";
}
?>
