<?
//ini_set("display_errors", "1");
// タイトル
switch ($wherefrom) {
case "1":  // タイムカード入力画面より
	$doc_title = "出勤表";
	break;
case "2":  // タイムカード修正画面より
	$doc_title = "勤務管理";
	break;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_attendance_pattern.ini");
require_once("show_timecard_common.ini");
require_once("holiday.php");
require_once("timecard_common_class.php");
require_once("atdbk_common_class.php");
require_once("atdbk_duty_shift_common_class.php");
require_once("timecard_bean.php");
require_once("atdbk_workflow_common_class.php");
require_once("timecard_paid_hol_hour_class.php");
require_once("calendar_name_class.php");
require_once("date_utils.php");
require_once("atdbk_close_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
switch ($wherefrom) {
case "1":  // タイムカード入力画面より
	$auth_id = 5;
	break;
case "2":  // タイムカード修正画面より
	$auth_id = 42;
	break;
default:
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
	break;
}
$checkauth = check_authority($session, $auth_id, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 表示モードのデフォルトは「実績表示」
if ($view == "") {
	$view = "1";
}

// データベースに接続
$con = connect2db($fname);

//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);

//時間有休
$obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
$obj_hol_hour->select();
//カレンダー名、所定労働時間履歴用
$calendar_name_class = new calendar_name_class($con, $fname);

// 職員ID未指定の場合はログインユーザの職員IDを取得
if ($emp_id == "") {
	$sql = "select emp_id from session";
	$cond = "where session_id = '$session'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");
}

//会議研修病棟外の列を常に表示とする 20120319
$meeting_display_flag = true;

// 勤務条件テーブルより勤務形態（常勤、非常勤）、残業管理を取得
$sql = "select duty_form, no_overtime from empcond";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$duty_form = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "duty_form") : "";
$no_overtime = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "no_overtime") : "";

//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);

if ($timecard_bean->timecard_title != "") {
	$timecard_title = $timecard_bean->timecard_title;
} else {
	$timecard_title = "タイムカード";
}

$atdbk_close_class = new atdbk_close_class($con, $fname);
$closing = $atdbk_close_class->get_empcond_closing($emp_id);
$closing_month_flg = $atdbk_close_class->closing_month_flg;

// 締め日が登録済みの場合
if ($closing != "") {

	if ($yyyymm != "") {
		$year = substr($yyyymm, 0, 4);
		$month = intval(substr($yyyymm, 4, 2));
		$month_set_flg = true; //月が指定された場合
	} else {
		$year = date("Y");
		$month = date("n");
		$yyyymm = $year . date("m");
		$month_set_flg = false; //月が未指定の場合
	}

	$last_month_y = $year;
	$last_month_m = $month - 1;
	if ($last_month_m == 0) {
		$last_month_y--;
		$last_month_m = 12;
	}
	$last_yyyymm = $last_month_y . sprintf("%02d", $last_month_m);

	$next_month_y = $year;
	$next_month_m = $month + 1;
	if ($next_month_m == 13) {
		$next_month_y++;
		$next_month_m = 1;
	}
	$next_yyyymm = $next_month_y . sprintf("%02d", $next_month_m);

	// 開始日・締め日を算出
	$calced = false;
	switch ($closing) {
	case "1":  // 1日
		$closing_day = 1;
		break;
	case "2":  // 5日
		$closing_day = 5;
		break;
	case "3":  // 10日
		$closing_day = 10;
		break;
	case "4":  // 15日
		$closing_day = 15;
		break;
	case "5":  // 20日
		$closing_day = 20;
		break;
	case "6":  // 末日
		$start_year = $year;
		$start_month = $month;
		$start_day = 1;
		$end_year = $start_year;
		$end_month = $start_month;
		$end_day = days_in_month($end_year, $end_month);
		$calced = true;
		break;
	}
	if (!$calced) {
		$day = date("j");
		$start_day = $closing_day + 1;
		$end_day = $closing_day;

		//月が指定された場合
		if ($month_set_flg) {
			//当月〜翌月
			if ($closing_month_flg != "2") {
				$start_year = $year;
				$start_month = $month;
				
				$end_year = $year;
				$end_month = $month + 1;
				if ($end_month == 13) {
					$end_year++;
					$end_month = 1;
				}
			}
			//前月〜当月
			else {
				$start_year = $year;
				$start_month = $month - 1;
				if ($start_month == 0) {
					$start_year--;
					$start_month = 12;
				}
				
				$end_year = $year;
				$end_month = $month;
			}
			
		} else {
			if ($day <= $closing_day) {
				$start_year = $year;
				$start_month = $month - 1;
				if ($start_month == 0) {
					$start_year--;
					$start_month = 12;
				}
				
				$end_year = $year;
				$end_month = $month;
			} else {
				$start_year = $year;
				$start_month = $month;
				
				$end_year = $year;
				$end_month = $month + 1;
				if ($end_month == 13) {
					$end_year++;
					$end_month = 1;
				}
			}
		}
	}
	$start_date = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
	$end_date = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);

	// 氏名を取得
	$sql = "select emp_lt_nm, emp_ft_nm, emp_personal_id from empmst";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
	$emp_personal_id = pg_fetch_result($sel, 0, "emp_personal_id");

	// 変則労働適用情報を配列で取得
	$arr_irrg_info = get_irrg_info_array($con, $emp_id, $fname);

	// 実績表示ボタン名
	$view_button = ($view == "1") ? "予実績表示" : "実績表示";

	// 対象年月の締め状況を取得
//	$tmp_yyyymm = substr($start_date, 0, 6);
	//月度対応 20100415
	$tmp_yyyymm = $year.sprintf("%02d", $month);
	$sql = "select count(*) from atdbkclose";
	$cond = "where emp_id = '$emp_id' and yyyymm = '$tmp_yyyymm'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$atdbk_closed = (pg_fetch_result($sel, 0, 0) > 0);

	// 締め済みの場合、締めデータから表示対象期間を取得
	if ($atdbk_closed) {
		$sql = "select min(date), max(date) from wktotald";
		$cond = "where emp_id = '$emp_id' and yyyymm = '$tmp_yyyymm'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$start_date = pg_fetch_result($sel, 0, 0);
		$end_date = pg_fetch_result($sel, 0, 1);

		$start_year = intval(substr($start_date, 0, 4));
		$start_month = intval(substr($start_date, 4, 2));
		$start_day = intval(substr($start_date, 6, 2));
		$end_year = intval(substr($end_date, 0, 4));
		$end_month = intval(substr($end_date, 4, 2));
		$end_day = intval(substr($end_date, 6, 2));
	}

	// タイムカード設定情報を取得
	$sql = "select ovtmscr_tm, show_time_flg from timecard";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$ovtmscr_tm = pg_fetch_result($sel, 0, "ovtmscr_tm");
		$show_time_flg = pg_fetch_result($sel, 0, "show_time_flg");
	}
	if ($ovtmscr_tm == "") {$ovtmscr_tm = 0;}
	if ($show_time_flg == "") {$show_time_flg = "t";}
	// タイムカード修正画面からの呼び出し時は出退勤時刻を表示する
	if ($wherefrom == "2") {$show_time_flg = "t";}

	// 給与支給区分・祝日計算方法を取得
    $sql = "select wage, hol_minus, specified_time, am_time, pm_time from empcond";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$wage = pg_fetch_result($sel, 0, "wage");
		$hol_minus = pg_fetch_result($sel, 0, "hol_minus");
        $paid_specified_time = pg_fetch_result($sel, 0, "specified_time");
        $am_time = pg_fetch_result($sel, 0, "am_time");
        $pm_time = pg_fetch_result($sel, 0, "pm_time");
    } else {
		$wage = "";
		$hol_minus = "";
        $paid_specified_time = "";
        $am_time = "";
        $pm_time = "";
    }
}

// 決裁欄役職情報を取得
$st_name_array = get_timecard_st_name_array($con, $fname);

//タイムカード集計項目出力設定からデータ取得 20091104
$arr_total_flg = get_timecard_total_flg($con, $fname); //show_timecard_common.ini
$arr_total_id = get_timecard_total_id($con, $fname);; //出力項目のid

//onload="self.print();self.close();"
?>
<title>CoMedix <? echo($doc_title); ?> | <?=$timecard_title?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
td.tm {width:2.8em;text-align:right;}
td.mark {width:2.8em;text-align:center;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="self.print();self.close();">
<center>
<table width="800" border="0" cellspacing="0" cellpadding="0">
<tr <? /* bgcolor="#5279a5"*/ ?>>
<td width="400" height="22" class="spacing" valign="bottom"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#000000"><b><?=$timecard_title?></b></font></td>
<td width="" align=""><? /*<a href="javascript:window.close()"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">■閉じる</font></a> */ ?>
<?
//決裁欄表示フラグ
if ($timecard_bean->timecard_appr_disp_flg == 't') {
?>
			<table border=1 align="right" cellspacing="0" cellpadding="1">
			<tr align="center">
				<td width="10" heigth="50" rowspan=2><font size=1>決裁</font></td>
<?
//決裁欄役職名
	for ($st_idx=0; $st_idx<count($st_name_array); $st_idx++) {
		echo("<td width=\"60\" height=\"10\"><font size=\"1\">".$st_name_array[$st_idx]["st_name"]."</font></td>\n");
	}
?>
			</tr>
			<tr>
<?
//決裁欄
	for ($st_idx=0; $st_idx<count($st_name_array); $st_idx++) {
		echo("<td width=\"60\" height=\"50\">　</td>\n");
	}
?>
			</tr>
			</table>
<? } ?>
</td>
</tr>
</table>
<table width="800" border="0" cellspacing="0" cellpadding="1">
<tr>
<td height="22" width="260"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象期間&nbsp;&nbsp;<? echo("{$year}年{$month}月度"); ?></font></td>
<td width="340"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">開始日&nbsp;&nbsp;<? echo("{$start_month}月{$start_day}日"); ?>&nbsp;&nbsp;〜&nbsp;&nbsp;締め日&nbsp;&nbsp;<? echo("{$end_month}月{$end_day}日"); ?></font></td>
</tr>
</table>
<table width="800" border="0" cellspacing="0" cellpadding="1">
<tr>
<td height="22" width=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID&nbsp;<? echo($emp_personal_id); ?>&nbsp;&nbsp;職員氏名&nbsp;<? echo($emp_name);

// 勤務条件表示
$str_empcond = get_employee_empcond2($con, $fname, $emp_id);

$str_shiftname = get_shiftname($con, $fname, $atdbk_closed, $emp_id, $start_date, $end_date);
//所属表示
$str_orgname = get_orgname($con, $fname, $emp_id, $timecard_bean);
echo("　<b>（".$str_empcond."　".$str_shiftname."）$str_orgname</b>");
?></font></td>
<td></td>
</tr>
</table>
<table width="800" border="1" cellspacing="0" cellpadding="1">
<tr height="22">
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">曜日</font></td>
<?
	if ($timecard_bean->time_move_flag == "t"){
		if ($timecard_bean->zen_display_flag == "t"){
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">前</font></td>
<?
		}
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤</font></td>
<?
		if ($timecard_bean->yoku_display_flag == "t"){
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">翌</font></td>
<?
		}
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退勤</font></td>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務</font></td>
<? //遅刻早退追加 20100907 ?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">遅刻</font></td>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">早退</font></td>
		<?
	if ($timecard_bean->over_disp_split_flg != "f"){
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">法内残</font></td>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">法外残</font></td>
<?
		} else {
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">残業</font></td>
<?
		}
	}
	if ($timecard_bean->type_display_flag == "t"){
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別</font></td>
<?
	}

	if ($timecard_bean->group_display_flag == "t"){
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">グループ</font></td>
<?
	}
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務実績</font></td>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事由</font></td>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($atdbk_common_class->duty_or_oncall_str); ?></font></td>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">手当</font></td>
<?
	if ($timecard_bean->time_move_flag == "f" || empty($timecard_bean->time_move_flag)){
		if ($timecard_bean->zen_display_flag == "t"){
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">前</font></td>
<?
		}
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤</font></td>
<?
		if ($timecard_bean->yoku_display_flag == "t"){
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">翌</font></td>
<?
		}
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退勤</font></td>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務</font></td>
<? //遅刻早退追加 20100907 ?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">遅刻</font></td>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">早退</font></td>
	<?
	if ($timecard_bean->over_disp_split_flg != "f"){
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">法内残</font></td>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">法外残</font></td>
<?
		} else {
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">残業</font></td>
<?
		}
	}
//深勤を深残へ変更 20110126
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">深残</font></td>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">外出</font></td>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休憩</font></td>
<?
	if ($timecard_bean->ret_display_flag == "t"){
	$ret_str = ($timecard_bean->return_icon_flg != "2") ? "退復" : "呼出";
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$ret_str?></font></td>
<?
	}
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日数</font></td>
<?
	if($meeting_display_flag){
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">会議<br>研修(h)</font></td>
<?
	}
?>
</tr>
<? $sums = show_attendance_book($con, $emp_id, $start_date, $end_date, $view, $yyyymm, $fname, $session, $atdbk_closed, $ovtmscr_tm, $arr_irrg_info, $show_time_flg, $wage, $hol_minus, $atdbk_common_class, $meeting_display_flag, $timecard_bean, $no_overtime, $closing, $closing_month_flg, $obj_hol_hour, $duty_form, $calendar_name_class, $paid_specified_time, $am_time, $pm_time); ?>
</table>
<img src="img/spacer.gif" width="1" height="2" alt=""><br>
<table width="800" border="1" cellspacing="0" cellpadding="1">
<tr height="22">
<?
//要勤務日数〜深夜勤務
for ($i=0; $i<18; $i++) {
	if ($arr_total_flg[$i]["flg"] == "t") {
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo($arr_total_flg[$i]["name"]);
		echo("</font></td>\n");
	}
}
?>
</tr>
<tr height="22">
<?
for ($i = 0; $i < 8; $i++) {
	if ($arr_total_flg[$i]["flg"] == "t") {
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($sums[$i]) . "</font></td>\n");
	}
}
//年残
if ($arr_total_flg[8]["flg"] == "t") {
	echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($sums[35]) . "</font></td>\n");
}
for ($i = 8; $i < 14; $i++) {
	//年残分シフト
	$wk_i = $i + 1;
	if ($arr_total_flg[$wk_i]["flg"] == "t") {
			//遅刻2_10、早退2_11で1回以上あったら、赤で表示する 20101210
			if (($arr_total_flg[$wk_i]["id"] == "2_10" ||
					$arr_total_flg[$wk_i]["id"] == "2_11") && $sums[$i] > 0) {
			$wk_color = " color=\"#ff0000\"";
		} else {
			$wk_color = " color=\"#000000\"";
		}
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" {$wk_color}>" . nbsp($sums[$i]) . "</font></td>\n");
	}
}
// 支給時間
if ($arr_total_flg[15]["flg"] == "t") {
	// 支給時間＝勤務時間＋有給取得日数×所定労働時間
	//		$paid_hol_days = substr($sums[7], 0, strlen($sums[7])-2); //"日"を除く
	// 有給取得日数を支給換算日数に変更 20090908
	//$paid_hol_days = $sums[34]; //支給換算日数
	//$paid_time = $atdbk_common_class->get_paid_time($sums[13], $paid_hol_days, $emp_id);
	//半日有休対応 20110204
	//$paid_time = $atdbk_common_class->get_paid_time($sums[13], $paid_hol_days, $sums[38], $sums[39], $emp_id);
    $paid_time_total = date_utils::hi_to_minute($sums[13]) + $sums[41];
    //時間有休追加 20111207 
	if ($obj_hol_hour->paid_hol_hour_flag == "t") {
        $paid_time_total += $sums[40];
	}
    $paid_time_total = ($paid_time_total == 0) ? "0:00" : minute_to_hmm($paid_time_total);
    echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($paid_time_total) . "</font></td>\n");
}
for ($i = 14; $i < 16; $i++) {
	//年残、支給時間分シフト
	$wk_i = $i + 2;
	if ($arr_total_flg[$wk_i]["flg"] == "t") {
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($sums[$i]) . "</font></td>\n");
	}
}
?>
</tr>
</table>
<?
// 事由ID、表示用名称、表示フラグの配列
$arr_reason_id = $atdbk_common_class->get_add_disp_reason_id();
$arr_reason_name = $atdbk_common_class->get_add_disp_reason_name2();
$arr_disp_flg = array();
for ($i=0; $i<count($arr_reason_id); $i++) {
	$tmp_display_flag = $atdbk_common_class->get_reason_display_flag($arr_reason_id[$i]);
	$arr_disp_flg[$i] = $tmp_display_flag;
}
?>
<img src="img/spacer.gif" width="1" height="2" alt=""><br>
<table width="800" border="1" cellspacing="0" cellpadding="1">
<tr height="22">
<?
//法定内勤務〜休日勤務
for ($i=18; $i<22; $i++) {
	if ($arr_total_flg[$i]["flg"] == "t") {
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo($arr_total_flg[$i]["name"]);
		echo("</font></td>\n");
	}
}

if (in_array("10_0", $arr_total_id)) {	
	for ($i=0; $i<count($arr_reason_id); $i++) {
		if ($arr_disp_flg[$i] == 't') {
?>
<td width="" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$arr_reason_name[$i]?></font></td>
			<?
		}
	}
}
?>
</tr>
<tr height="22">
<?
//法定内勤務、法定内残業、法定外残業、休日勤務
for ($i = 28; $i < 32; $i++) {
	//集計項目情報の位置は18番目から
	$wk_i = $i - 10;
	if ($arr_total_flg[$wk_i]["flg"] == "t") {
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($sums[$i]) . "</font></td>\n");
	}
}
//休暇日数
if (in_array("10_0", $arr_total_id)) {	
	//年末年始追加対応 20090908
	for ($i = 16; $i < 31; $i++) {
		if ($arr_disp_flg[$i-16] == 't') {
			//法定所定追加 20110121
			if ($i==17) { //法定
				$wk_str = nbsp($sums[36]);
			} elseif ($i==18) { //所定 
				$wk_str = nbsp($sums[37]);
			} elseif ($i>18 && $i<27) { 
				$wk_str = nbsp($sums[$i-2]);
			} elseif ($i==27) { //年末年始
				$wk_str = nbsp($sums[33]);
			} elseif ($i>27) { //
				$wk_str = nbsp($sums[$i-3]);
			} else {
				$wk_str = nbsp($sums[$i]);
			}
			echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . $wk_str . "</font></td>\n");
		}
	}
}
?>
</tr>
</table>

<table width="800" border="0" cellspacing="0" cellpadding="1">
<?
// 勤務条件表示
//show_employee_empcond($con, $fname, $emp_id);
// 集計項目表示
show_shift_summary2($con, $fname, $atdbk_closed, $emp_id, $start_date, $end_date, $arr_total_id);

?>
</table>

</center>
</body>
<? pg_close($con); ?>
</html>
<?
// 出勤予定を出力
function show_attendance_book($con, $emp_id, $start_date, $end_date, $view, $yyyymm, $fname, $session, $atdbk_closed, $ovtmscr_tm, $arr_irrg_info, $show_time_flg, $wage, $hol_minus, $atdbk_common_class, $meeting_display_flag, $timecard_bean, $no_overtime, $closing, $closing_month_flg, $obj_hol_hour, $duty_form, $calendar_name_class, $empcond_specified_time, $empcond_am_time, $empcond_pm_time) {

	//休日出勤と判定する日設定取得 20091222
	$arr_timecard_holwk_day = get_timecard_holwk_day($con, $fname, $emp_id);

	//公休と判定する日設定 20100810
	$arr_timecard_holwk_day_std = get_timecard_holwk_day($con, $fname, "");
	
	//ワークフロー関連共通クラス
	$atdbk_workflow_common_class = new atdbk_workflow_common_class($con, $fname);
	//一括修正申請情報
	$arr_tmmdapplyall_status = $atdbk_workflow_common_class->get_tmmdapplyall_status($emp_id, $start_date, $end_date);
	
	// 集計結果格納用配列を初期化
	$sums = array_fill(0, 42, 0);

	// 手当情報取得
	$arr_allowance = get_timecard_allowance($con, $fname);

	// 端数処理情報を取得
	$sql = "select * from timecard";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "fraction$i";
			$var_name_min = "fraction{$i}_min";
			$$var_name = pg_fetch_result($sel, 0, "fraction$i");
			$$var_name_min = pg_fetch_result($sel, 0, "fraction{$i}_min");
		}
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "others$i";
			$$var_name = pg_fetch_result($sel, 0, "others$i");
		}
		$rest1 = intval(pg_fetch_result($sel, 0, "rest1"));
		$rest2 = intval(pg_fetch_result($sel, 0, "rest2"));
		$rest3 = intval(pg_fetch_result($sel, 0, "rest3"));
		//時間給者の休憩時間追加 20100929
		$rest4 = intval(pg_fetch_result($sel, 0, "rest4"));
		$rest5 = intval(pg_fetch_result($sel, 0, "rest5"));
		$rest6 = intval(pg_fetch_result($sel, 0, "rest6"));
		$rest7 = intval(pg_fetch_result($sel, 0, "rest7"));
		
		$night_workday_count = pg_fetch_result($sel, 0, "night_workday_count");
		//$overtime_40_flg = pg_fetch_result($sel, 0, "overtime_40_flg");
		$modify_flg = pg_fetch_result($sel, 0, "modify_flg");
	} else {
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "fraction$i";
			$var_name_min = "fraction{$i}_min";
			$$var_name = "";
			$$var_name_min = "";
		}
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "others$i";
			$$var_name = "";
		}
		$rest1 = 0;
		$rest2 = 0;
		$rest3 = 0;
		//時間給者の休憩時間追加 20100929
		$rest4 = 0;
		$rest5 = 0;
		$rest6 = 0;
		$rest7 = 0;
		
		$night_workday_count = "";
		$modify_flg = "t";
	}
	//週40時間超過分は計算しないを固定とする 20100525
	$overtime_40_flg = "2";

	//集計処理の設定を「集計しない」の固定とする 20100910
	$others1 = "1";	//勤務時間内に外出した場合
	$others2 = "1";	//勤務時間外に外出した場合
	$others3 = "1";	//時間給者の休憩の集計
	$others4 = "1";	//遅刻・早退に休憩が含まれた場合
	$others5 = "2";	//早出残業時間の集計
	
	// 「退勤後復帰」ボタン表示フラグを取得
	$sql = "select ret_btn_flg from config";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rolllback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$ret_btn_flg = pg_fetch_result($sel, 0, "ret_btn_flg");

	// 全日付をループ（変則労働時間・週の場合、週の開始曜日からスタート）
	$tmp_date = $start_date;
	$wd = date("w", to_timestamp($tmp_date));
	if ($arr_irrg_info["irrg_type"] == "1") {
		while (($arr_irrg_info["irrg_wd"] == "1" && $wd != 0) || ($arr_irrg_info["irrg_wd"] == "2" && $wd != 1)) {
			$tmp_date = last_date($tmp_date);
			$wd = date("w", to_timestamp($tmp_date));
		}
	} else {
	//変則労働時間・週以外の場合、日曜から
		while ($wd != 0) {
			$tmp_date = last_date($tmp_date);
			$wd = date("w", to_timestamp($tmp_date));
		}
	}
    //データ検索用開始日
    $wk_start_date = $tmp_date;
    
	//グループ毎のテーブルから勤務時間を取得する
	$timecard_common_class = new timecard_common_class($con, $fname, $emp_id, $tmp_date, $end_date);

    //個人別所定時間を優先するための対応 20120309
    $arr_empcond_officehours_ptn = $timecard_common_class->get_empcond_officehours_pattern();
    $arr_weekday_flg = $timecard_common_class->get_arr_weekday_flag($wk_start_date, $end_date);
    $arr_empcond_officehours = $timecard_common_class->get_arr_empcond_officehours($emp_id, $wk_start_date); //個人別勤務時間帯履歴対応 20130220
    
    //所定労働時間を取得
	//$day1_time = $timecard_common_class->get_calendarname_day1_time();
    //所定労働時間履歴を使用 20121127
    if ($empcond_specified_time != "") {
        $paid_specified_time = $empcond_specified_time;
        $day1_time = $empcond_specified_time;
        $am_time = $empcond_am_time;
        $pm_time = $empcond_pm_time;
        $empcond_spec_time_flg = true;
    }
    else {
        $arr_timehist = $calendar_name_class->get_calendarname_time($start_date);
        $day1_time = $arr_timehist["day1_time"];
        $am_time = $arr_timehist["am_time"];
        $pm_time = $arr_timehist["pm_time"];
        $empcond_spec_time_flg = false;
    }
    
	//出・退勤時刻文字サイズ
	$time_font = "\"j12\"";
	if ($timecard_bean->time_big_font_flag == "t" && $show_time_flg == "t"){
		$time_font = "\"y16\"";
	}
	//翌月初日までデータを取得 20130308
	$wk_end_date_nextone = next_date($end_date);
	// 処理日付の勤務日種別を取得
	$arr_type = array();
	$sql = "select date, type from calendar";
	$cond = "where date >= '$wk_start_date' and date <= '$wk_end_date_nextone'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$date = $row["date"];
		$arr_type[$date] = $row["type"];
	}
// 予定データをまとめて取得
	$arr_atdbk = array();
	$sql =	"SELECT ".
				"atdbk.date, ".
				"atdbk.pattern, ".
				"atdbk.reason, ".
				"atdbk.night_duty, ".
				"atdbk.allow_id, ".
				"atdbk.prov_start_time, ".
				"atdbk.prov_end_time, ".
				"atdbk.tmcd_group_id, ".
				"atdptn.workday_count ".
			"FROM ".
				"atdbk ".
					"LEFT JOIN atdptn ON atdbk.tmcd_group_id = atdptn.group_id AND atdbk.pattern = CAST(atdptn.atdptn_id AS varchar) ";
	$cond = "where emp_id = '$emp_id' and ";
	$cond .= " date >= '$wk_start_date' and date <= '$end_date'";

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$date = $row["date"];
		$arr_atdbk[$date]["tmcd_group_id"] = $row["tmcd_group_id"];
		$arr_atdbk[$date]["pattern"] = $row["pattern"];
		$arr_atdbk[$date]["reason"] = $row["reason"];
		$arr_atdbk[$date]["night_duty"] = $row["night_duty"];
		$arr_atdbk[$date]["allow_id"] = $row["allow_id"];
		$arr_atdbk[$date]["prov_start_time"] = $row["prov_start_time"];
		$arr_atdbk[$date]["prov_end_time"] = $row["prov_end_time"];
		$arr_atdbk[$date]["workday_count"] = $row["workday_count"];
	}
// 実績データをまとめて取得
	$arr_atdbkrslt = array();
	$sql =	"SELECT ".
//				"atdbkrslt.date, ".
				"atdbkrslt.*, ".
				"atdptn.workday_count, ".
				"atdptn.base_time, ".
				"atdptn.over_24hour_flag, ".
				"atdptn.out_time_nosubtract_flag, ".
				"atdptn.after_night_duty_flag, ".
                "atdptn.previous_day_flag as atdptn_previous_day_flag, ".
                "atdptn.no_count_flag, ".
                "tmmdapply.apply_id     AS tmmd_apply_id, ".
				"tmmdapply.apply_status AS tmmd_apply_status, ".
				"ovtmapply.apply_id     AS ovtm_apply_id, ".
				"ovtmapply.apply_status AS ovtm_apply_status, ".
				"rtnapply.apply_id      AS rtn_apply_id, ".
				"rtnapply.apply_status  AS rtn_apply_status ".
			"FROM ".
				"atdbkrslt ".
					"LEFT JOIN atdptn ON atdbkrslt.tmcd_group_id = atdptn.group_id AND atdbkrslt.pattern = CAST(atdptn.atdptn_id AS varchar) ".
					"LEFT JOIN tmmdapply ON atdbkrslt.emp_id = tmmdapply.emp_id AND CAST(atdbkrslt.date AS varchar) = tmmdapply.target_date AND tmmdapply.delete_flg = false AND tmmdapply.re_apply_id IS NULL ".
					"LEFT JOIN ovtmapply ON atdbkrslt.emp_id = ovtmapply.emp_id AND CAST(atdbkrslt.date AS varchar) = ovtmapply.target_date AND ovtmapply.delete_flg = false AND ovtmapply.re_apply_id IS NULL ".
					"LEFT JOIN rtnapply  ON atdbkrslt.emp_id = rtnapply.emp_id  AND CAST(atdbkrslt.date AS varchar) = rtnapply.target_date AND rtnapply.delete_flg = false AND rtnapply.re_apply_id IS NULL ";
	$cond = "where atdbkrslt.emp_id = '$emp_id' and ";
    $cond .= " atdbkrslt.date >= '$wk_start_date' and atdbkrslt.date <= '$wk_end_date_nextone'"; //20130308
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$date = $row["date"];
		$arr_atdbkrslt[$date]["pattern"] = $row["pattern"];
		$arr_atdbkrslt[$date]["reason"] = $row["reason"];
		$arr_atdbkrslt[$date]["night_duty"] = $row["night_duty"];
		$arr_atdbkrslt[$date]["allow_id"] = $row["allow_id"];
		$arr_atdbkrslt[$date]["start_time"] = $row["start_time"];
		$arr_atdbkrslt[$date]["end_time"] = $row["end_time"];
		$arr_atdbkrslt[$date]["out_time"] = $row["out_time"];
		$arr_atdbkrslt[$date]["ret_time"] = $row["ret_time"];
		for ($i = 1; $i <= 10; $i++) {
			$start_var = "o_start_time$i";
			$end_var = "o_end_time$i";
			$arr_atdbkrslt[$date][$start_var] = $row[$start_var];
			$arr_atdbkrslt[$date][$end_var] = $row[$end_var];
		}
		$arr_atdbkrslt[$date]["tmcd_group_id"] = $row["tmcd_group_id"];
		$arr_atdbkrslt[$date]["meeting_time"] = $row["meeting_time"];
		$arr_atdbkrslt[$date]["workday_count"] = $row["workday_count"];
		$arr_atdbkrslt[$date]["previous_day_flag"] = $row["previous_day_flag"];
		$arr_atdbkrslt[$date]["next_day_flag"] = $row["next_day_flag"];
		$arr_atdbkrslt[$date]["tmmd_apply_id"] = $row["tmmd_apply_id"];
		$arr_atdbkrslt[$date]["tmmd_apply_status"] = $row["tmmd_apply_status"];
		$arr_atdbkrslt[$date]["ovtm_apply_id"] = $row["ovtm_apply_id"];
		$arr_atdbkrslt[$date]["ovtm_apply_status"] = $row["ovtm_apply_status"];
		$arr_atdbkrslt[$date]["rtn_apply_id"] = $row["rtn_apply_id"];
		$arr_atdbkrslt[$date]["rtn_apply_status"] = $row["rtn_apply_status"];
		$arr_atdbkrslt[$date]["meeting_start_time"] = $row["meeting_start_time"];
		$arr_atdbkrslt[$date]["meeting_end_time"] = $row["meeting_end_time"];
		$arr_atdbkrslt[$date]["base_time"] = $row["base_time"];
		//残業時刻追加 20100114
		$arr_atdbkrslt[$date]["allow_count"] = $row["allow_count"];
		$arr_atdbkrslt[$date]["over_start_time"] = $row["over_start_time"];
		$arr_atdbkrslt[$date]["over_end_time"] = $row["over_end_time"];
		$arr_atdbkrslt[$date]["over_start_next_day_flag"] = $row["over_start_next_day_flag"];
		$arr_atdbkrslt[$date]["over_end_next_day_flag"] = $row["over_end_next_day_flag"];
		//残業時刻2追加 20110616
		$arr_atdbkrslt[$date]["over_start_time2"] = $row["over_start_time2"];
		$arr_atdbkrslt[$date]["over_end_time2"] = $row["over_end_time2"];
		$arr_atdbkrslt[$date]["over_start_next_day_flag2"] = $row["over_start_next_day_flag2"];
		$arr_atdbkrslt[$date]["over_end_next_day_flag2"] = $row["over_end_next_day_flag2"];
		$arr_atdbkrslt[$date]["over_24hour_flag"] = $row["over_24hour_flag"];
		//法定内残業
		$arr_atdbkrslt[$date]["legal_in_over_time"] = $row["legal_in_over_time"];
		//早出残業 20100601
		$arr_atdbkrslt[$date]["early_over_time"] = $row["early_over_time"];
		//休憩時刻追加 20100921
		$arr_atdbkrslt[$date]["rest_start_time"] = $row["rest_start_time"];
		$arr_atdbkrslt[$date]["rest_end_time"] = $row["rest_end_time"];
		//外出時間を差し引かないフラグ 20110727
		$arr_atdbkrslt[$date]["out_time_nosubtract_flag"] = $row["out_time_nosubtract_flag"];
		//明け追加 20110819
		$arr_atdbkrslt[$date]["after_night_duty_flag"] = $row["after_night_duty_flag"];
        //時間帯画面の前日フラグ 20121120
        $arr_atdbkrslt[$date]["atdptn_previous_day_flag"] = $row["atdptn_previous_day_flag"];
        //勤務時間を計算しないフラグ 20130225
        $arr_atdbkrslt[$date]["no_count_flag"] = $row["no_count_flag"];
    }

	//時間有休追加 20111207 
	if ($obj_hol_hour->paid_hol_hour_flag == "t") {
		//時間有休データをまとめて取得
		$arr_hol_hour_dat = $obj_hol_hour->get_paid_hol_hour_month($emp_id, $wk_start_date, $end_date);
		
	}	

	$counter_for_week = 1;
	$holiday_count = 0;
	//法定内勤務、法定内残業、法定外残業
	$hoteinai = 0;
	$hoteinai_zan = 0;
	$hoteigai = 0;

	//遅刻時間計 20100907
	$delay_time = 0;
	
	//早退時間計
	$early_leave_time = 0;

	//支給換算日数 20090908
	$paid_day_count = 0;

	//休日出勤テーブル 2008/10/23
	$arr_hol_work_date = array();
	while ($tmp_date <= $end_date) {
		$tmp_year = intval(substr($tmp_date, 0, 4));
		$tmp_month = intval(substr($tmp_date, 4, 2));
		$tmp_day = intval(substr($tmp_date, 6, 2));

		if ($counter_for_week >= 8) {
			$counter_for_week = 1;
		}
		if ($counter_for_week == 1) {
			//前日までの計
			$last_day_total = 0;
			$work_time_for_week = 0;
			if ($arr_irrg_info["irrg_type"] == "1") {
				$holiday_count = 0;
			}
			//1日毎の法定外残を週単位に集計
			$wk_week_hoteigai = 0;
		}
		$work_time = 0;
		$wk_return_time = 0;

		$start_time_info = array();
		$end_time_info = array(); //退勤時刻情報 20100910
		
		$time3 = 0; //外出時間（所定時間内） 20100910
		$time4 = 0; //外出時間（所定時間外） 20100910
		$tmp_rest2 = 0; //休憩時間 20100910
		$time3_rest = 0; //休憩時間（所定時間内）20100910
		$time4_rest = 0; //休憩時間（所定時間外）20100910
		$arr_effective_time_wk = array(); //休憩を引く前の実働時間 20100915
		$arr_hol_time = array(); // 休日勤務情報 20101224
		
		$holiday_name = get_holiday_name($tmp_date);
		if ($holiday_name != "") {
			$holiday_count++;
		}

		// 処理日付の勤務日種別を取得
		$type = $arr_type[$tmp_date];
		// カレンダー名を取得
		$type_name = $atdbk_common_class->get_calendarname($type, $tmp_date);
		$prov_tmcd_group_id = $arr_atdbk[$tmp_date]["tmcd_group_id"];
		$prov_pattern = $arr_atdbk[$tmp_date]["pattern"];
		$prov_reason = $arr_atdbk[$tmp_date]["reason"];
		$prov_night_duty = $arr_atdbk[$tmp_date]["night_duty"];
		$prov_allow_id = $arr_atdbk[$tmp_date]["allow_id"];

		$prov_start_time = $arr_atdbk[$tmp_date]["prov_start_time"];
		$prov_end_time = $arr_atdbk[$tmp_date]["prov_end_time"];
		$prov_start_time = ($prov_start_time != "") ? preg_replace("/(\d{2})(\d{2})/", "$1:$2", $prov_start_time) : "";
		$prov_end_time =  ($prov_end_time != "") ? preg_replace("/(\d{2})(\d{2})/", "$1:$2", $prov_end_time) : "";
		$prov_workday_count = $arr_atdbk[$tmp_date]["workday_count"];
		// 要勤務日数をカウント
//		if ($tmp_date >= $start_date) {

			// カレンダーの属性を元にカウントする 2008/10/21
//			$sums[0] += $timecard_common_class->get_you_kinmu_nissu($type);
/*
			// 予定が未入力でも「休暇」でもなければカウント
			if ($prov_pattern != "" && $prov_pattern != "10") {

                // 当直あり
                if($prov_night_duty == "1")
                {
					//曜日に対応したworkday_countを取得する
					$prov_workday_count = $prov_workday_count + $timecard_common_class->get_prov_night_workday_count($tmp_date);

                }
                $sums[0] = $sums[0] + $prov_workday_count;
			}
*/
//		}

		// 処理日付の勤務実績を取得
		$pattern = $arr_atdbkrslt[$tmp_date]["pattern"];
		$reason = $arr_atdbkrslt[$tmp_date]["reason"];
        $night_duty = $arr_atdbkrslt[$tmp_date]["night_duty"];
		$allow_id = $arr_atdbkrslt[$tmp_date]["allow_id"];
		$start_time = $arr_atdbkrslt[$tmp_date]["start_time"];
		$end_time = $arr_atdbkrslt[$tmp_date]["end_time"];
		$out_time = $arr_atdbkrslt[$tmp_date]["out_time"];
		$ret_time = $arr_atdbkrslt[$tmp_date]["ret_time"];

		for ($i = 1; $i <= 10; $i++) {
			$start_var = "o_start_time$i";
			$end_var = "o_end_time$i";
			$$start_var = $arr_atdbkrslt[$tmp_date]["$start_var"];
			$$end_var = $arr_atdbkrslt[$tmp_date]["$end_var"];

			if ($$start_var != "") {
				$$start_var = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $$start_var);
			}
			if ($$end_var != "") {
				$$end_var = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $$end_var);
			}
		}

		$tmcd_group_id = $arr_atdbkrslt[$tmp_date]["tmcd_group_id"];
		$meeting_time = $arr_atdbkrslt[$tmp_date]["meeting_time"];

		$workday_count = $arr_atdbkrslt[$tmp_date]["workday_count"];

		$previous_day_flag = $arr_atdbkrslt[$tmp_date]["previous_day_flag"];
		$next_day_flag = $arr_atdbkrslt[$tmp_date]["next_day_flag"];

		$tmmd_apply_id = $arr_atdbkrslt[$tmp_date]["tmmd_apply_id"];
		$tmmd_apply_status = $arr_atdbkrslt[$tmp_date]["tmmd_apply_status"];
		$ovtm_apply_id = $arr_atdbkrslt[$tmp_date]["ovtm_apply_id"];
		$ovtm_apply_status = $arr_atdbkrslt[$tmp_date]["ovtm_apply_status"];
		$rtn_apply_id = $arr_atdbkrslt[$tmp_date]["rtn_apply_id"];
		$rtn_apply_status = $arr_atdbkrslt[$tmp_date]["rtn_apply_status"];

		$meeting_start_time = $arr_atdbkrslt[$tmp_date]["meeting_start_time"];
		$meeting_end_time = $arr_atdbkrslt[$tmp_date]["meeting_end_time"];
		$base_time = $arr_atdbkrslt[$tmp_date]["base_time"];
		//hhmmをhh:mmに変換している
		if ($start_time != "") {
			$start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $start_time);
		}
		if ($end_time != "") {
			$end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $end_time);
		}
		//表示用に退避 20100802
		$disp_start_time = $start_time;
		$disp_end_time = $end_time;

		if ($out_time != "") {
			$out_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $out_time);
		}
		if ($ret_time != "") {
			$ret_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $ret_time);
		}
		if ($meeting_start_time != "") {
			$meeting_start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $meeting_start_time);
		}
		if ($meeting_end_time != "") {
			$meeting_end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $meeting_end_time);
		}
		if ($previous_day_flag == "") {
			$previous_day_flag = 0;
		}
		if ($next_day_flag == "") {
			$next_day_flag = 0;
		}
		//残業時刻追加 20100114
		$over_start_time = $arr_atdbkrslt[$tmp_date]["over_start_time"];
		$over_end_time = $arr_atdbkrslt[$tmp_date]["over_end_time"];
		$over_start_next_day_flag = $arr_atdbkrslt[$tmp_date]["over_start_next_day_flag"];
		$over_end_next_day_flag = $arr_atdbkrslt[$tmp_date]["over_end_next_day_flag"];
		if ($over_start_time != "") {
			$over_start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_start_time);
		}
		if ($over_end_time != "") {
			$over_end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_end_time);
		}
		//残業時刻2追加 20110616
		$over_start_time2 = $arr_atdbkrslt[$tmp_date]["over_start_time2"];
		$over_end_time2 = $arr_atdbkrslt[$tmp_date]["over_end_time2"];
		$over_start_next_day_flag2 = $arr_atdbkrslt[$tmp_date]["over_start_next_day_flag2"];
		$over_end_next_day_flag2 = $arr_atdbkrslt[$tmp_date]["over_end_next_day_flag2"];
		if ($over_start_time2 != "") {
			$over_start_time2 = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_start_time2);
		}
		if ($over_end_time2 != "") {
			$over_end_time2 = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_end_time2);
		}
		$over_24hour_flag = $arr_atdbkrslt[$tmp_date]["over_24hour_flag"];
		//法定内残業
		$legal_in_over_time = $arr_atdbkrslt[$tmp_date]["legal_in_over_time"];
		if ($legal_in_over_time != "") {
			$legal_in_over_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $legal_in_over_time);
		}
		//早出残業 20100601
		$early_over_time = $arr_atdbkrslt[$tmp_date]["early_over_time"];
		if ($early_over_time != "") {
			$early_over_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $early_over_time);
		}
		//休憩時刻追加 20100921
		$rest_start_time = $arr_atdbkrslt[$tmp_date]["rest_start_time"];
		$rest_end_time = $arr_atdbkrslt[$tmp_date]["rest_end_time"];
		if ($rest_start_time != "") {
			$rest_start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $rest_start_time);
		}
		if ($rest_end_time != "") {
			$rest_end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $rest_end_time);
		}
		//外出時間を差し引かないフラグ 20110727
		$out_time_nosubtract_flag = $arr_atdbkrslt[$tmp_date]["out_time_nosubtract_flag"];
		
		//明け追加 20110819
		$after_night_duty_flag = $arr_atdbkrslt[$tmp_date]["after_night_duty_flag"];
        //時間帯画面の前日フラグ 20121120
        $atdptn_previous_day_flag = $arr_atdbkrslt[$tmp_date]["atdptn_previous_day_flag"];
        //勤務時間を計算しないフラグ 20130225
        $no_count_flag = $arr_atdbkrslt[$tmp_date]["no_count_flag"];
        
		//出勤予定にグループの指定がない場合、ユーザに設定したグループを優先する
		if (empty($tmcd_group_id)){
			// 出勤パターン名を取得
			$tmcd_group_id = $user_tmcd_group_id;
		}

		$selectGroupId   = "tmcd_group_id_".$tmp_date;
		$selectPatternId = "pattern_".$tmp_date;

		// 残業表示用に退勤時刻を退避
		$show_end_time = $end_time;
		$show_start_time = $start_time;
		$show_next_day_flag = $next_day_flag;	//残業時刻日またがり対応 20100114
		
		//当直の有効判定
		$night_duty_flag = $timecard_common_class->is_valid_duty($night_duty, $tmp_date, $start_time, $previous_day_flag, $end_time, $next_day_flag, $tmcd_group_id);

		$work_workday_count = "";

		if ($tmp_date >= $start_date) {

			//公休か事由なし休暇で残業時刻がありの場合 20100802
			//事由に関わらず休暇で残業時刻がありの場合へ変更 20100916
			if (($pattern == "10" || $after_night_duty_flag == "1") && //明けを追加 20110819
				$over_start_time != "" && $over_end_time != "") {
				$legal_hol_over_flg = true;
				//残業時刻を設定
				$start_time = $over_start_time;
				$end_time = $over_end_time;
				
			} else {
				$legal_hol_over_flg = false;
			}
			// 出勤日数をカウント（条件：出勤と退勤の打刻あり、または、「明け」の場合）
			if (($start_time != "" && $end_time != "") || $after_night_duty_flag == "1") {

				//日またがり対応 20101027
				if ($start_time > $end_time && 
						$previous_day_flag == "0") {
					$next_day_flag = "1";
				}

				//休暇時は計算しない 20090806
				if ($pattern != "10") {
					$work_workday_count = $workday_count;
				}
				if($night_duty_flag){
					//曜日に対応したworkday_countを取得する
					$work_workday_count = $work_workday_count + $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);
				}
				$sums[1] = $sums[1] + $work_workday_count;
				//休日出勤のカウント方法変更 20091222
                if (check_timecard_holwk(
                            $arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason, "", $timecard_bean) == true) {
					$sums[2] += 1;
				}
			}

			// 事由ベースの日数計算
			$reason_day_count = $atdbk_common_class->get_reason_day_count($reason);
			//reason_day_countは支給換算日数となる 20090908
            $paid_time = 0;
            if ($reason_day_count > 0) {
				$am_hol_days = 0;
                $pm_hol_days = 0;
                $paid_hol_days = 0;
                //半日有休を区別して集計 20110204
				switch ($reason) {
					case "2": //午前有休
					case "38": //午前年休
					case "67": //午前特別
					case "50": //午前夏休
					case "69": //午前正休
						$sums[38] += $reason_day_count;
                        $am_hol_days = 0.5;
                        break;
					case "3": //午後有休
					case "39": //午後年休
					case "68": //午後特別
					case "51": //午後夏休
					case "70": //午後正休
						$sums[39] += $reason_day_count;
                        $pm_hol_days = 0.5;
                        break;
                    case "44":  // 半有半公
                    case "55":  // 半夏半有
                    case "57":  // 半有半欠
                    case "58":  // 半特半有
                    case "62":  // 半正半有
                        $sums[34] += $reason_day_count;
                        $paid_hol_days = 0.5;
                        break;
                    default: //上記以外	
                        if ($pattern == "10") { //20140106 パターンが休暇の場合
                            $sums[34] += $reason_day_count;
                            $paid_hol_days = 1.0;
                        }
                }
                //個人別勤務時間帯にある場合、曜日別に所定時間を求める 20130220 start
                $wk_weekday = $arr_weekday_flg[$tmp_date];
                $arr_empcond_officehours = $timecard_common_class->get_empcond_officehours($wk_weekday, $tmp_date);
                if ($arr_empcond_officehours["part_specified_time"] != "") {
                    $paid_specified_time = $arr_empcond_officehours["part_specified_time"];
                    $am_time = "";
                    $pm_time = "";
                    $empcond_spec_time_flg = true;
                }
                //else { //20130521 勤務条件の所定時間を2番目に優先とする
                //    $empcond_spec_time_flg = false;
                //}
                //個人別勤務時間帯にある場合、曜日別に所定時間を求める 20130220 end
                //所定時間履歴を使用 20121127
                //勤務条件にない場合
                if ($empcond_spec_time_flg == false) {
                    $arr_timehist = $calendar_name_class->get_calendarname_time($tmp_date);
                    $paid_specified_time = $arr_timehist["day1_time"];
                    $am_time = $arr_timehist["am_time"];
                    $pm_time = $arr_timehist["pm_time"];
                }
                //半日有給
                $min = 0;
                //半日午前所定時間がある場合は計算し加算
                if ($am_time != "") {
                    $hour2 = (int)substr($am_time, 0, 2);
                    $min2 = $hour2 * 60 + (int)substr($am_time, 2, 3);
                    //日数分を加算する(0.5単位のため、1回分の計算として2倍する）
                    $min += $min2 * $am_hol_days * 2;
                }
                else {
                    //ない場合は、半日以外の所定に加算
                    $paid_hol_days += $am_hol_days;
                }
                //半日午後所定時間がある場合は計算し加算
                if ($pm_time != "") {
                    $hour2 = (int)substr($pm_time, 0, 2);
                    $min2 = $hour2 * 60 + (int)substr($pm_time, 2, 3);
                    //日数分を加算する(0.5単位のため、1回分の計算として2倍する）
                    $min += $min2 * $pm_hol_days * 2;
                }
                else {
                    //ない場合は、半日以外の所定に加算
                    $paid_hol_days += $pm_hol_days;
                }
                
                //半日以外の換算日数
                if ($paid_specified_time != "") {
                    //所定労働時間を分単位にする
                    $hour2 = (int)substr($paid_specified_time, 0, 2);
                    $min2 = $hour2 * 60 + (int)substr($paid_specified_time, 2, 3);
                    //日数分を加算する
                    $min += $min2 * $paid_hol_days;
                }
                $paid_time = $min;
                $sums[41] += $min;
            }
			//時間有休追加 20111207
			$paid_time_hour = 0;
			if ($obj_hol_hour->paid_hol_hour_flag == "t") {
				$paid_hol_min =  $arr_hol_hour_dat[$tmp_date]["calc_minute"];
				$paid_time_hour = $paid_hol_min;
				$sums[40] += $paid_hol_min; //時間有休計
			}
			
			switch ($reason) {
//			case "16":  // 休日出勤
//				$sums[2] += $reason_day_count;
//				break;
			case "14":  // 代替出勤
				$sums[3] += 1;
				break;
			case "15":  // 振替出勤
				$sums[4] += 1;
				break;
			case "65":  // 午前振出
			case "66":  // 午後振出
				$sums[4] += 0.5;
				break;
			case "4":   // 代替休暇
				$sums[5] += 1;
				break;
			case "18":  // 半前代替休
			case "19":  // 半後代替休
				$sums[5] += 0.5;
				break;
			case "17":  // 振替休暇
				$sums[6] += 1;
				break;
			case "20":  // 半前振替休
			case "21":  // 半後振替休
				$sums[6] += 0.5;
				break;
			case "1":  // 有休休暇
			case "37": // 年休
                if ($pattern == "10") { //20140106 パターンが休暇の場合
                    $sums[7] += 1;
                }
				break;
			case "2":  // 午前有休
			case "3":  // 午後有休
			case "38": // 午前年休
			case "39": // 午後年休
				$sums[7] += 0.5;
				break;
			case "57":  // 半有半欠
				$sums[7] += 0.5;
				$sums[8] += 0.5;
				break;
			case "6":  // 一般欠勤
			case "7":  // 病傷欠勤
				$sums[8] += 1;
				break;
			case "48": // 午前欠勤
			case "49": // 午後欠勤
				$sums[8] += 0.5;
				break;
			case "8":   // その他休
				$sums[9] += 1;
				break;
			case "52":  // 午前その他休
			case "53":  // 午後その他休
				$sums[9] += 0.5;
				break;
			case "24":  // 公休
			//case "22":  // 法定休暇
			//case "23":  // 所定休暇
			case "45":  // 希望(公休)
			case "46":  // 待機(公休)
			case "47":  // 管理当直前(公休)
				$sums[16] += 1;
				break;
			case "35":  // 午前公休
			case "36":  // 午後公休
				$sums[16] += 0.5;
				break;
			case "5":   // 特別休暇
				$sums[17] += 1;
				break;
			case "67":  // 午前特別
			case "68":  // 午後特別
				$sums[17] += 0.5;
				break;
			case "58":  // 半特半有
				$sums[17] += 0.5;
				$sums[7] += 0.5;
				break;
			case "59":  // 半特半公
				$sums[17] += 0.5;
				$sums[16] += 0.5;
				break;
			case "60":  // 半特半欠
				$sums[17] += 0.5;
				$sums[8] += 0.5;
				break;
			case "25":  // 産休
				$sums[18] += 1;
				break;
			case "26":  // 育児休業
				$sums[19] += 1;
				break;
			case "27":  // 介護休業
				$sums[20] += 1;
				break;
			case "28":  // 傷病休職
				$sums[21] += 1;
				break;
			case "29":  // 学業休職
				$sums[22] += 1;
				break;
			case "30":  // 忌引
				$sums[23] += 1;
				break;
			case "31":  // 夏休
				$sums[24] += 1;
				break;
			case "50":  // 午前夏休
			case "51":  // 午後夏休
				$sums[24] += 0.5;
				break;
				break;
			case "54":  // 半夏半公
				$sums[24] += 0.5;
				$sums[16] += 0.5;
				break;
			case "55":  // 半夏半有
				$sums[24] += 0.5;
				$sums[7] += 0.5;
				break;
			case "56":  // 半夏半欠
				$sums[24] += 0.5;
				$sums[8] += 0.5;
				break;
			case "72":  // 半夏半特
				$sums[24] += 0.5;
				$sums[17] += 0.5;
				break;
			case "61":  // 年末年始
				$sums[33] += 1;
				break;
			case "69":  // 午前正休
			case "70":  // 午後正休
				$sums[33] += 0.5;
				break;
				break;
			case "62":  // 半正半有
				$sums[33] += 0.5;
				$sums[7] += 0.5;
				break;
			case "63":  // 半正半公
				$sums[33] += 0.5;
				$sums[16] += 0.5;
				break;
			case "64":  // 半正半欠
				$sums[33] += 0.5;
				$sums[8] += 0.5;
				break;
			case "32":   // 結婚休
				$sums[25] += 1;
				break;
			case "40":   // リフレッシュ休暇
				$sums[26] += 1;
				break;
			case "42":   // 午前リフレッシュ休暇
			case "43":   // 午後リフレッシュ休暇
				$sums[26] += 0.5;
				break;
			case "41":  // 初盆休暇
				$sums[27] += 1;
				break;
			case "44":  // 半有半公
				$sums[7] += 0.5;
				$sums[16] += 0.5;
				break;
			}
			// 休日となる設定と事由があう場合に公休に計算する 20100810
			// 法定休日
			if ($arr_type[$tmp_date] == "4" && $arr_timecard_holwk_day_std[0]["legal_holiday"] == "t") {
				if ($reason == "22") {
					$sums[16] += 1;
				}
			}
			// 集計行に法定休日追加 20110121
			if ($reason == "22") {
				$sums[36] += 1;
			}
			// 所定休日
			if ($arr_type[$tmp_date] == "5" && $arr_timecard_holwk_day_std[0]["prescribed_holiday"] == "t") {
				if ($reason == "23") {
					$sums[16] += 1;
				}
			}
			// 集計行に所定休日追加 20110121
			if ($reason == "23") {
				$sums[37] += 1;
			}
			// 年末年始
			if ($arr_type[$tmp_date] == "7" && $arr_timecard_holwk_day_std[0]["newyear_holiday"] == "t") {
				if ($reason == "61") {
					$sums[16] += 1;
				}
				if ($reason == "62" || $reason == "63" || $reason == "64") {
					$sums[16] += 0.5;
				}
			}
		}

		// 各時間帯の開始時刻・終了時刻を変数に格納
		$tmp_type = ($type != "2" && $type != "3") ? "1" : $type;
		$start2 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start2" );
		$end2 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end2" );
		$start4 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start4" );
		$end4 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end4" );
		$start5 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start5" );
		$end5 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end5" );

        //個人別所定時間を優先する場合 20120309
        if ($arr_empcond_officehours_ptn[$tmcd_group_id][$pattern] == 1) {
            $wk_weekday = $arr_weekday_flg[$tmp_date];
            $arr_empcond_officehours = $timecard_common_class->get_empcond_officehours($wk_weekday, $tmp_date); //個人別勤務時間帯履歴対応 20130220
            if ($arr_empcond_officehours["officehours2_start"] != "") {
                $start2 = $arr_empcond_officehours["officehours2_start"];
                $end2 = $arr_empcond_officehours["officehours2_end"];
                $start4 = $arr_empcond_officehours["officehours4_start"];
                $end4 = $arr_empcond_officehours["officehours4_end"];
            }
        }
        
        // 所定労働時間帯が確定できない場合、予定時刻を所定労働時間帯として使用
		if (($start2 == "" || $end2 == "") && $prov_start_time != "" && $prov_end_time != "") {
			$start2 = $prov_start_time;
			$end2 = $prov_end_time;
		}
		$over_calc_flg = false;	//残業時刻 20100525
		//残業時刻追加 20100114
		//残業開始が予定終了時刻より前の場合に設定
		if ($over_start_time != "") {
			//残業開始日時
			if ($over_start_next_day_flag == "1") {
				$over_start_date = next_date($tmp_date);
			} else {
				$over_start_date = $tmp_date;
			}
			
			$over_start_date_time = $over_start_date.$over_start_time;
			$over_calc_flg = true;
			//残業終了時刻 20100705
			$over_end_date = ($over_end_next_day_flag == "1") ? next_date($tmp_date) : $tmp_date;
			$over_end_date_time = $over_end_date.$over_end_time;
		}
		// 実働時間をクリア、出勤時刻＜退勤時刻でない場合に前日の残業時間が表示されないようにする。
		$effective_time = 0;
		
		//一括修正のステータスがある場合は設定
		$tmp_status = $arr_tmmdapplyall_status[$tmp_date]["apply_status"];
		if ($tmmd_apply_status == "" && $tmp_status != "") {
			$tmmd_apply_status = $tmp_status;
			$tmmd_apply_id = $arr_tmmdapplyall_status[$tmp_date]["apply_id"];
		}
		//tmmd_apply_statusからlink_type取得
		$modify_link_type = $timecard_common_class->get_link_type_modify($tmmd_apply_status);
		$modify_apply_id  = $tmmd_apply_id;

		// 残業情報を取得
		//ovtm_apply_statusからlink_type取得
		$night_duty_time_array  = array();
		$duty_work_time         = 0;
		if ($night_duty_flag){
			//当直時間の取得
			$night_duty_time_array = $timecard_common_class->get_duty_time_array($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);

			//日数換算の数値ｘ通常勤務日の労働時間の時間
			$duty_work_time = date_utils::hi_to_minute($day1_time) * $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);
		}
		$overtime_link_type = $timecard_common_class->get_link_type_overtime($tmp_date, $start_time, $previous_day_flag, $end_time, $next_day_flag, $night_duty, $start2, $end2, $ovtm_apply_status, $tmcd_group_id, $over_start_time, $over_end_time, $early_over_time, $over_24hour_flag, $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2);
		$overtime_apply_id  = $ovtm_apply_id;

		//rtn_apply_statusからlink_type取得
		$return_link_type = $timecard_common_class->get_link_type_return($ret_btn_flg, $o_start_time1, $rtn_apply_status);
		$return_apply_id  = $rtn_apply_id;

		// 所定労働・休憩開始終了日時の取得
        // 所定開始時刻が前日の場合 20121120
        if ($atdptn_previous_day_flag == "1") {
            $tmp_prev_date = last_date($tmp_date);
			$start2_date_time = $tmp_prev_date.$start2;
			//24時間以上勤務の場合 20100203
			if ($over_24hour_flag == "1") {
				$tmp_prev_date = $tmp_date;
			}
			$end2_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_prev_date, $start2, $end2);
		} else {
			$start2_date_time = $tmp_date.$start2;
			//24時間以上勤務の場合 20100203
			if ($over_24hour_flag == "1") {
				$tmp_end_date = next_date($tmp_date);
			} else {
				$tmp_end_date = $tmp_date;
			}
			$end2_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_end_date, $start2, $end2);
		}
		//休憩時刻追加、入力された場合時間帯の設定の代わりに使用 20100921
		if ($rest_start_time != "" && $rest_end_time != "") {
			$start4 = $rest_start_time;
			$end4 = $rest_end_time;
		}
		$start4_date_time = $tmp_date.$start4;
		$end4_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_date, $start4, $end4);

		//外出復帰日時の取得
		$out_date_time = $tmp_date.$out_time;
		$ret_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_date, $out_time, $ret_time);

		// 時間帯データを配列に格納
		$arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end2_date_time);  // 所定労働
		// 前日出勤フラグ、翌日退勤フラグを考慮した休憩時間配列を取得
		// 24時間超える場合の翌日の休憩は配列にいれない 20101027
		if ($start2 < $start4) {
			$arr_time4 = $timecard_common_class->get_minute_time_array2($start4_date_time, $end4_date_time, $previous_day_flag, "");  // 休憩
		} else {
			$arr_time4 = $timecard_common_class->get_minute_time_array2($start4_date_time, $end4_date_time, $previous_day_flag, $next_day_flag);  // 休憩
		}
		
		//時間帯情報 休憩、深夜、前日当日翌日分の開始終了時刻を保持 20110126
		if ($tmp_date >= $start_date) {
			$work_times_info = $timecard_common_class->get_work_times_info($tmp_date, $start4, $end4, $start5, $end5);
		}
		// 処理当日の所定労働時間を求める
		if ($holiday_name != "" && $hol_minus == "t") {  // 祝日かつ祝日減算設定
			$specified_time = 0;
			//		} else if ($wage == "4" && $others3 == "2") {  // 時間給者かつ休憩を実働加算 集計処理の設定を「集計しない」の固定とする 20100910
			//			$specified_time = count($arr_time2);
		} else {
			$specified_time = count(array_diff($arr_time2, $arr_time4));
		}

// 基準時間の計算を変更 2008/10/21
/*
		// 変則労働期間が月でない場合、所定労働時間を基準時間としてカウント
		if ($tmp_date >= $start_date && $reason != "16") {
			if ($arr_irrg_info["irrg_type"] != "2") {
				$sums[12] += $specified_time;
				//当直の場合稼働時間に日数換算の数値ｘ通常勤務日の労働時間の時間を追加
				if ($night_duty_flag){
					$sums[12] += $duty_work_time;
				}
			}
		}
*/
		// 出退勤とも打刻済みの場合
        //勤務時間を計算しないフラグが"1"以外 20130225
        if ($start_time != "" && $end_time != "" && $no_count_flag != "1") {
            
			// ここまでの処理で所定労働時間帯が未確定の場合、勤務実績で上書き
			$start_date_time = $timecard_common_class->get_timecard_start_date_time($tmp_date, $start_time, $previous_day_flag);
			$end_date_time   = $timecard_common_class->get_timecard_end_date_time($tmp_date, $end_time, $next_day_flag);
			if (empty($start2_date_time) || empty($end2_date_time)) {
				$start2_date_time = $start_date_time;
				$end2_date_time    = $end_date_time;
			}

			$overtime_approve_flg = $timecard_common_class->get_overtime_approve_flg($overtime_link_type, $modify_flg, $modify_link_type);
			// 残業管理をする場合、（しない場合には退勤時刻を変えずに稼動時間の計算をする）
			if ($no_overtime != "t") {
				// 残業申請中の場合は定時退勤時刻を退勤時刻として計算
//				if (!($overtime_link_type == "0" || $overtime_link_type == "3" || $overtime_link_type == "4")) {
				// 残業未承認か確認
				if ($overtime_approve_flg == "1" || ($return_link_type != "0" && $return_link_type != "3")) {
					if (date_utils::to_timestamp_from_ymdhi($start2_date_time) > date_utils::to_timestamp_from_ymdhi($start_date_time)) { //所定より出勤が早い場合には所定にする 20100914 時刻に:があるため遅刻時間の表示されない不具合 20101005
						$start_date_time  = $start2_date_time; //出勤時間、早出残業の場合
					}
					if ($end_date_time > $end2_date_time) {
						$end_date_time = $end2_date_time;
					}
				}
			}

			// 出勤〜退勤までの時間を配列に格納
			$arr_tmp_work_time = $timecard_common_class->get_minute_time_array($start_date_time, $end_date_time);

			//出勤同時刻退勤または、出勤時間＞退勤時間の場合は処理を行わない
			if (count($arr_tmp_work_time) > 0){
/*
				// 時間帯データを配列に格納
				switch ($reason) {
				case "2":  // 午前有休
				case "38": // 午前年休
					$arr_time2 = $timecard_common_class->get_minute_time_array($start4_date_time, $end2_date_time);  // 休憩後の所定労働
					break;
				case "3":  // 午後有休
				case "39": // 午後年休
					$arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end4_date_time);  // 休憩前の所定労働
					break;
				default:
*/
					$arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end2_date_time);  // 所定労働
//					break;
//				}
				//休日で残業がある場合 20101028
				if ($pattern == "10" && $over_start_time != "" && $over_end_time != "") {
					//深夜残業の開始終了時間
					$start5 = "2200";
					$end5 = "0500";
				}
				//前日・翌日の深夜帯も追加する
				$arr_time5 = $timecard_common_class->get_late_night_shift_array($tmp_date, $start5, $end5);  // 深夜帯

				// 確定出勤時刻／遅刻回数を取得
				// 勤務パターンで時刻指定がない場合でも、端数処理する 20090605
				if ($start2 == "" && $end2 == "" && $fraction1 > "1") {
					//端数処理する分
					$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
					//基準日時
					$tmp_office_start_time = substr($start_date_time, 0, 10) . "00";
					$fixed_start_time = date_utils::move_time($tmp_office_start_time, $start_date_time, $moving_minutes, $fraction1);
					$start_time_info = array();
				} else {
					$start_time_info = $timecard_common_class->get_start_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction1, $fraction1_min, $fraction3, $others4, $others5);
					$fixed_start_time = $start_time_info["fixed_time"];
				}

				// 確定退勤時刻／早退回数を取得
				// 勤務パターンで時刻指定がない場合でも、端数処理する 20090605
				if ($start2 == "" && $end2 == "" && $fraction2 > "1") {
					//端数処理する分
					$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
					//基準日時
					$tmp_office_start_time = substr($start_date_time, 0, 10) . "00";
					$fixed_end_time = date_utils::move_time($tmp_office_start_time, $end_date_time, $moving_minutes, $fraction2);
//echo("fixed_end_time=$fixed_end_time");
					$end_time_info = array();
				} else {
					//残業時刻が入っている場合は、端数処理しない 20100806
					$wk_over_end_date_time = str_replace(":", "", $over_end_date_time); //20111128
					if ($over_end_time != "" && $wk_over_end_date_time > $end2_date_time) { //時間が所定終了よりあとか確認20111128
						$fixed_end_time = $wk_over_end_date_time;
					} else {
						$end_time_info = $timecard_common_class->get_end_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction2, $fraction2_min, $fraction3, $others4);
						$fixed_end_time = $end_time_info["fixed_time"];
					}
					//※残業申請未承認の場合と（残業申請不要かつ残業管理をする場合）に退勤時刻を所定にする 20100721
					//残業申請画面を表示しないは除く 20100811
					if ($timecard_bean->over_time_apply_type != "0") {
						if ($overtime_approve_flg == "1" || ($overtime_approve_flg == "0" && $no_overtime != "t")) {
							//退勤時刻が所定よりあとの場合設定 20101224
							if ($fixed_end_time > $end2_date_time) {
								$fixed_end_time = $end2_date_time;
							}
						}
					}
				}
				//時間有休がある場合は、遅刻早退を計算しない 20111207
				if ($obj_hol_hour->paid_hol_hour_flag == "t" && $paid_time_hour > 0) {
					//遅刻
					$tikoku_time = 0;
					$start_time_info["diff_minutes"] = 0;
					$start_time_info["diff_count"] = 0;
					//早退
					$sotai_time = 0;
					$end_time_info["diff_minutes"] = 0;
					$end_time_info["diff_count"] = 0;
				}
				if ($tmp_date >= $start_date) {
					//遅刻
					$sums[10] += $start_time_info["diff_count"];
					$delay_time += $start_time_info["diff_minutes"]; //遅刻時間 20100907
					//早退
					$sums[11] += $end_time_info["diff_count"];
					$early_leave_time += $end_time_info["diff_minutes"];
				}

				// 実働時間・所定労働時間を配列に格納（休憩を除く）・有休時間を算出
				switch ($reason) {
				case "2":  // 午前有休
				case "38": // 午前年休
						// 残業時間が正しく計算されないため、休憩終了時間設定をしない 20100330
						//if (in_array($fixed_start_time, $arr_time2)) {
						$tmp_start_time = $fixed_start_time;
					//} else {
					//	$tmp_start_time = $end4_date_time;
					//}
					$tmp_end_time = $fixed_end_time;
					$arr_effective_time = $timecard_common_class->get_minute_time_array($tmp_start_time, $tmp_end_time);

					$arr_specified_time = $arr_time2;

					//$paid_time = date_utils::get_time_difference($start2_date_time, $start4_date_time);

					//休憩を引く前の実働時間
					$arr_effective_time_wk = $arr_effective_time;
					break;
				case "3":  // 午後有休
				case "39": // 午後年休
						$tmp_start_time = $fixed_start_time;
						// 残業時間が正しく計算されないため、休憩開始時間設定をしない 20100330
						//if (in_array($fixed_end_time, $arr_time2)) {
						$tmp_end_time = $fixed_end_time;
					//} else {
					//	$tmp_end_time = $start4_date_time;
					//}
					$arr_effective_time = $timecard_common_class->get_minute_time_array($tmp_start_time, $tmp_end_time);

					$arr_specified_time = $arr_time2;

					//$paid_time = date_utils::get_time_difference($end4_date_time, $end2_date_time);

					//休憩を引く前の実働時間
					$arr_effective_time_wk = $arr_effective_time;
					break;
				default:
					$tmp_start_time = $fixed_start_time;
					$tmp_end_time = $fixed_end_time;
					$arr_effective_time = $timecard_common_class->get_minute_time_array($tmp_start_time, $tmp_end_time);
					//休憩を引く前の実働時間
					$arr_effective_time_wk = $arr_effective_time;
					if ($wage != "4" || ($start4 != "" && $end4 != "")) { //休憩時刻がある場合を追加 20100925
							$arr_effective_time = array_diff($arr_effective_time, $arr_time4);
					}

					$arr_specified_time = array_diff($arr_time2, $arr_time4);

					//$paid_time = 0;

					break;
				}

				//残業時刻追加対応 20100114
				//予定終了時刻から残業開始時刻の間を除外する
				if ($over_start_time != "") {
					$wk_end_date_time = $end2_date_time;
					
					$arr_jogai = array();
					/* 20100713
					//基準日時
					$tmp_office_start_time = substr($end2_date_time, 0, 10) . "00";
					if ($fraction1 > "1") {
						//端数処理する分
						$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
						$over_start_date_time = date_utils::move_time($tmp_office_start_time, $over_start_date_time, $moving_minutes, $fraction1);
					}
					if ($fraction2 > "1") {
						//端数処理する分
						$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
						$wk_end_date_time = date_utils::move_time($tmp_office_start_time, $wk_end_date_time, $moving_minutes, $fraction2);
					}
					*/
					//残業開始時刻があとの場合
					if (date_utils::to_timestamp_from_ymdhi($over_start_date_time) > date_utils::to_timestamp_from_ymdhi($wk_end_date_time)) {
						//時間配列を取得する（休憩扱い）
						$arr_jogai = $timecard_common_class->get_minute_time_array($wk_end_date_time, $over_start_date_time);  						
						//休憩扱いの時間配列を除外する
						$arr_effective_time = array_diff($arr_effective_time, $arr_jogai);
					}
					
				}
				//当直の場合当直時間を計算から除外する
				if ($night_duty_flag){
					//稼働 - (当直 - 所定労働)
					$arr_effective_time = array_diff($arr_effective_time, array_diff($night_duty_time_array, $arr_time2));
				}

				// 普外時間を算出（分単位）
				// 外出時間を差し引かないフラグ対応 20110727
				if ($out_time_nosubtract_flag != "1") {
					$arr_out_time = $timecard_common_class->get_minute_time_array($out_date_time, $ret_date_time);
					$time3 = count(array_intersect($arr_out_time, $arr_specified_time));
					
					// 残外時間を算出（分単位）
					$time4 = count(array_diff($arr_out_time, $arr_time2)); //$arr_specified_timeから変更、休憩も除くため 20100910
				}
				$time3_rest = 0;
				$time4_rest = 0;

				// 実働時間を算出（分単位）
				$effective_time = count($arr_effective_time);
				if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
					if ($effective_time > 1200) {  // 20時間を超える場合 20100929
						$effective_time -= $rest7;
						$time3_rest = 0;
						$time4_rest = $rest7;
					} else if ($effective_time > 720) {  // 12時間を超える場合
						$effective_time -= $rest6;
						$time3_rest = 0;
						$time4_rest = $rest6;
					} else if ($effective_time > 540) {  // 9時間を超える場合
						$effective_time -= $rest3;
						$time3_rest = 0;
						$time4_rest = $rest3;
					} else if ($effective_time > 480) {  // 8時間を超える場合
						$effective_time -= $rest2;
						$time3_rest = $rest2;
						$time4_rest = 0;
					} else if ($effective_time > 360) {  // 6時間を超える場合
						$effective_time -= $rest1;
						$time3_rest = $rest1;
						$time4_rest = 0;
					} else if ($effective_time > 240 && $start_time < "12:00") {  // 4時間超え午前開始の場合
						$effective_time -= $rest4;
						$time3_rest = $rest4;
						$time4_rest = 0;
					} else if ($effective_time > 240 && $start_time >= "12:00") {  // 4時間超え午後開始の場合
						$effective_time -= $rest5;
						$time3_rest = $rest5;
						$time4_rest = 0;
					}
					//休日残業で残業申請中は休憩を表示しない 20100916
					if ($legal_hol_over_flg /*&& $overtime_approve_flg == "1"*/) {
						$time3_rest = 0;
						$time4_rest = 0;
					}
					//休日で残業がない場合,出勤退勤がある場合 20100917
					if ($pattern == "10" && $over_start_time == "" && $over_end_time == "") {
						$time3_rest = 0;
						$time4_rest = 0;
					}
					//開始した日が休日の場合に休日勤務から休憩を減算 2008/10/23
					//if (($previous_day_flag == true && $arr_hol_time[0] > 0) ||
					//	($previous_day_flag == false && $arr_hol_time[1] > 0)) {
					//	$sums[31] = $sums[31] - $time3_rest - $time4_rest;
					//}
				}

				//古い仕様のため廃止 20100330
				/*
				// 代替出勤または振替出勤の場合、実働時間から所定労働時間を減算
				if ($reason == "14" || $reason == "15") {
					$effective_time -= count($arr_specified_time);
				}
				*/
				
				// 稼働時間を算出（分単位）
				//$work_time = $effective_time + $paid_time - $time3 - $time4;
				// 半年休の時間をプラスしない 20100330
				$work_time = $effective_time - $time3 - $time4;
				
				//休暇時は計算しない 20090806 公休か事由なし休暇で残業時刻が設定済を条件追加 20100802
				if ($pattern == "10" && !$legal_hol_over_flg) {
					$work_time = 0;
				}

				// 退勤後復帰回数・時間を算出
				$return_count = 0;
				$return_time = 0;
				$return_late_time = 0; //20100715
                //申請状態 "0":申請不要 "3":承認済 //勤務時間を計算しないフラグ確認 20130225
				if (($return_link_type == "0" || $return_link_type == "3") && $no_count_flag != "1") { 
					for ($i = 1; $i <= 10; $i++) {
						$start_var = "o_start_time$i";
						if ($$start_var == "") {
							break;
						}
						$return_count++;
						
						$end_var = "o_end_time$i";
						if ($$end_var == "") {
							break;
						}
						
						//端数処理 20090709
						if ($fraction1 > "1") {
							//端数処理する分
							$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
							//開始日時
							$tmp_start_date_time = $timecard_common_class->get_schedule_end_date_time(substr($end_date_time,0,8), substr($end_date_time,8,4), $$start_var);
							//基準日時
							$tmp_base_date_time = substr($tmp_start_date_time,0,10)."00";
							$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_start_date_time, $moving_minutes, $fraction1);
							$tmp_ret_start_time = substr($tmp_date_time, 8, 4);
						} else {
							$tmp_ret_start_time = str_replace(":", "", $$start_var);
						}
						if ($fraction2 > "1") {
							//端数処理する分
							$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
							//終了日時
							$tmp_end_date_time = $timecard_common_class->get_schedule_end_date_time(substr($end_date_time,0,8), substr($end_date_time,8,4), $$end_var);
							//基準日時
							$tmp_base_date_time = substr($tmp_end_date_time,0,10)."00";
							$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_end_date_time, $moving_minutes, $fraction2);
							
							$tmp_ret_end_time = substr($tmp_date_time, 8, 4);
						} else {
							$tmp_ret_end_time = str_replace(":", "", $$end_var);
						}
						$arr_ret = $timecard_common_class->get_return_array($end_date_time, $tmp_ret_start_time, $tmp_ret_end_time);
						
						//					$arr_ret = $timecard_common_class->get_return_array($end_date_time, $$start_var, $$end_var);
						$return_time += count($arr_ret);
						
						// 月の深夜勤務時間に退勤後復帰の深夜分を追加
						//$return_late_time += count(array_intersect($arr_ret, $arr_time5)); //20100715
						//計算方法変更 20110126
						//深夜残業
						$wk_ret_start = $tmp_date.$tmp_ret_start_time;
						$wk_ret_end = $timecard_common_class->get_schedule_end_date_time($tmp_date, $tmp_ret_start_time, $tmp_ret_end_time);
						$wk_late_night = $timecard_common_class->get_late_night($work_times_info, $wk_ret_start,$wk_ret_end, "");
						$return_late_time += $wk_late_night;
						//当日が休日の場合、休日に合計。
						//休日判断変更（勤務パターンに関わらず、カレンダーと休日設定による） 20110228
                        if ((check_timecard_holwk(
                                        $arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason, "", $timecard_bean) == true) ||
								$reason == "16") {
							//当日の残業
							$today_over = $timecard_common_class->get_day_time($work_times_info, $wk_ret_start,$wk_ret_end, "1");
							//事由が普通残業の場合を除く 20110128
							//if ($reason != "71") {
								$sums[31] += $today_over;
							//}
						}
						//翌日の残業
						$next_date = next_date($tmp_date);
                        if (check_timecard_holwk(
                                    $arr_timecard_holwk_day, $arr_type[$next_date], $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], "1", $timecard_bean) == true) { //20130308
                            $nextday_over = $timecard_common_class->get_day_time($work_times_info, $wk_ret_start,$wk_ret_end, "2");
							//事由が普通残業の場合を除く 20110128
							//if ($reason != "71") {
								$sums[31] += $nextday_over;
							//}
						}
					}
				}
				$wk_return_time = $return_time;
				//残業時刻が入力された場合、重複するため、退勤後復帰時間を加算しない。 20100714 不要 20100910
				//if ($over_calc_flg) {
				//	$wk_return_time = 0; //集計用 $return_time 表示用
				//	$return_late_time = 0; //20100715
				//}
				
				// 前日までの計 = 週計
				$last_day_total = $work_time_for_week;
				$work_time_for_week += $work_time;
				$work_time_for_week += $wk_return_time;
				if ($tmp_date < $start_date) {
					$tmp_date = next_date($tmp_date);
					$counter_for_week++;
					continue;
				}
				//$sums[13] += $work_time; // 20100910
				
				//当直の場合稼働時間に日数換算の数値ｘ通常勤務日の労働時間の時間を追加（当直の時間を除外して計算している為)
				//if ($night_duty_flag){
				//	$sums[13] += $duty_work_time;
				//}
/*
				// 残業時間を算出（分単位）
				if ($arr_irrg_info["irrg_type"] == "") {  // 変則労働適用外の場合

					$tmp_specified_time = $specified_time;
					// 遅刻時間を残業時間から減算しない場合（所定時間から引くことで合わせる）
					if ($timecard_bean->delay_overtime_flg == "2") {
						$tmp_specified_time = $specified_time - $start_time_info["diff_minutes"];
					}
					// 日々の表示値は普外・残外の減算前
//					$time1 = ($effective_time > 480) ? $effective_time - 480 : 0;
					if ($effective_time > $tmp_specified_time) {
						$time1 = $effective_time - $tmp_specified_time;
					} else {
						$time1 = 0;
					}
					$sums[14] += $time1;
					if ($others1 == "4") {
						$sums[14] -= $time3;
					}
					if ($others2 == "4") {
						$sums[14] -= $time4;
					}
				} else if ($arr_irrg_info["irrg_type"] == "1") {  // 変則労働期間・週の場合
					if ($counter_for_week == 7) {
						$tmp_irrg_minutes = $arr_irrg_info["irrg_minutes"];
						if ($hol_minus == "t") {
							$tmp_irrg_minutes -= (8 * 60 * $holiday_count);
						}
						$time1 = ($work_time_for_week > $tmp_irrg_minutes) ? $work_time_for_week - $tmp_irrg_minutes : 0;
						$sums[14] += $time1;
					} else {
						$time1 = "";
					}
				}
*/

				// 深夜勤務時間を算出（分単位）
				$time2 = 0;
				//計算方法変更 20110126
				//残業承認時に計算、申請画面を表示しない場合は無条件に計算
				if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
						$timecard_bean->over_time_apply_type == "0") {
					//残業時刻がない場合で、所定終了、退勤を比較後残業計算
					if ($over_start_time == "" || $over_end_time == "") {
                        //if ($end2_date_time < $fixed_end_time) { //退勤時刻を使用しない 20120406
						//	$over_start_date_time = $end2_date_time;
						//	$over_end_date_time = $fixed_end_time;
						//} else {
							$over_start_date_time = ""; //20101109
							$over_end_date_time = "";
						//}
					}
					//深夜残業
					$time2 = $timecard_common_class->get_late_night($work_times_info, $over_start_date_time,$over_end_date_time, "");
                    //休憩対応 20130827
                    if ($time2 > 0 && $start4 != "" && $end4 != "") {
                        $wk_rest_time = $timecard_common_class->get_intersect_times($over_start_date_time, $over_end_date_time, $start4_date_time, $end4_date_time);
                        if ($wk_rest_time > 0) {
                            $late_night_info = $timecard_common_class->get_late_night_shift_info($tmp_date, $start5, $end5);  // 深夜帯情報
                            //深夜時間帯か再確認
                            $wk_s1 = max($over_start_date_time, $start4_date_time);
                            $wk_e1 = min($over_end_date_time, $end4_date_time);
                            $wk_rest_time = $timecard_common_class->get_times_info_calc($late_night_info, $wk_s1, $wk_e1);
                            $time2 -= $wk_rest_time;
                        }
                    }
                    //休日残業
					//当日
					$wk_hol_flg =  check_timecard_holwk(
							$arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason, ""); //休日フラグ 20110228
					if (($pattern != "10" && $wk_hol_flg == true) ||
							$reason == "16" ||
							($pattern == "10" && $over_start_time != "" && $over_end_time != "" && 
                                $overtime_approve_flg != "1" && (($reason != "71"
                                        && $reason != "24") || ($reason == "24" && $timecard_bean->no_hol_overtime_flg == "f"))) //休暇で残業時刻がある場合
						) {
						$today_over = $timecard_common_class->get_day_time($work_times_info, $over_start_date_time,$over_end_date_time, "1");
						//休憩時間
						if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合
							//休憩を減算
                            //$today_over = $today_over - $time3_rest - $time4_rest; //コメント化 20130822
                            ;
						} else {
                            //休憩 20130822
                   			//出勤時刻と残業開始時刻が一致している場合、所定、遅刻、早退を計算せず、残業、休憩のみ計算する
                            if ($start2 != "" && $start2 == $over_start_time && $over_start_next_day_flag != "1") {//残業開始が翌日でないことを追加
                                $overtime_only_flg = true;
                            }
                            else {
                                $overtime_only_flg = false;
                            }
							//休憩時間確認
							if ($start4 != "" && $end4 != "") {
                                if ($legal_hol_over_flg || $overtime_only_flg) {
                                    
                                    $wk_kyukei = $timecard_common_class->get_intersect_times($over_start_date_time, $over_end_date_time, $start4_date_time, $end4_date_time);
                                    if ($wk_kyukei > 0) {
                                        //日勤時間帯か再確認
                                        $wk_s1 = max($over_start_date_time, $start4_date_time);
                                        $wk_e1 = min($over_end_date_time, $end4_date_time);
                                        $wk_rest_time = $timecard_common_class->get_day_time($work_times_info, $wk_s1,$wk_e1, "1");
                                        $today_over -= $wk_rest_time;
                                    }
                                }
                            }
                        }
						//事由が普通残業の場合を除く 20110128
						//if ($reason != "71") {
							$sums[31] += $today_over;
						//}
					}
					//翌日フラグ
					if ($next_day_flag == "1" || $over_end_next_day_flag == "1") {
						$next_date = next_date($tmp_date);
						if (check_timecard_holwk(
									$arr_timecard_holwk_day, $arr_type[$next_date], $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], "1") == true) { //20130308
                            $nextday_over = $timecard_common_class->get_day_time($work_times_info, $over_start_date_time,$over_end_date_time, "2", $timecard_bean);
							//事由が普通残業の場合を除く 20110128
							//if ($reason != "71") {
								$sums[31] += $nextday_over;
							//}
						}
					}
					
					//残業２
					if ($over_start_time2 != "" && $over_end_time2 != "") {
						//開始日時
						$over_start_date_time2 = ($over_start_next_day_flag2 == 1) ? next_date($tmp_date).$over_start_time2 : $tmp_date.$over_start_time2;
						//終了日時
						$over_end_date_time2 = ($over_end_next_day_flag2 == 1) ? next_date($tmp_date).$over_end_time2 : $tmp_date.$over_end_time2;
						//深夜時間帯の時間取得
						$over_time2_late_night = $timecard_common_class->get_late_night($work_times_info, $over_start_date_time2, $over_end_date_time2, "");
						$time2 += $over_time2_late_night;
					}				
				}
				$time2 += $return_late_time; //20100715
				$sums[15] += $time2;

				// 月の稼働時間・残業時間に退勤後復帰分の残業時間を追加
				//$sums[13] += $wk_return_time; //20100714 20100910
				//				$sums[14] += $return_time;

				// 表示値の編集
				$time1 = minute_to_hmm($time1);
				$time2 = minute_to_hmm($time2);
				// 普外−＞外出に変更 20090603
				//$time3 = minute_to_hmm($time3 + $time3_rest);
				$time_outtime = minute_to_hmm($time3);
				if ($time4 > 0) {
					$time_outtime .= "(".minute_to_hmm($time4).")";
				}
				// 残外−＞休憩に変更 20090603
				if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
					$time_rest = minute_to_hmm($time3_rest);
					if ($time4_rest > 0) {
						$time_rest .= "(".minute_to_hmm($time4_rest).")";
					}
				} else {
					// 実働と休憩の配列から休憩時間を算出
					// 所定労働内の休憩時間
					$tmp_rest1 = count(array_intersect($arr_time2, $arr_time4));
					// 実働時間内の休憩時間
					$tmp_rest2 = count(array_intersect($arr_effective_time_wk, $arr_time4));
					// 所定労働外（残業時間内）の休憩時間
					$tmp_rest3 = $tmp_rest2 - $tmp_rest1;
					$time_rest = minute_to_hmm($tmp_rest1);
					if ($tmp_rest3 > 0) {
						$time_rest .= "(".minute_to_hmm($tmp_rest3).")";
					}
					//休憩時間前後の遅刻早退対応 20100925
					if ($tmp_rest1 > $tmp_rest2) {
						$tmp_rest2 = $tmp_rest1;
						$tmp_rest1 = 0;
						$time_rest = "";
					}
				}
				if ($return_count == 0) {$return_count = "";}
				$return_time = minute_to_hmm($return_time);
                // 回数追加 20121109
                if ($return_count != "") {
                    $return_time .= " ".$return_count;
                }
                
				// 出退勤時刻の表示設定
				$time_cell_class = "tm";
				if ($show_time_flg != "t") {
					$time_cell_class = "mark";
					$start_time = ($start_time_info["diff_count"]) ? "遅刻" : "○";
					$show_start_time = ($start_time_info["diff_count"]) ? "遅刻" : "○";
					$show_end_time = ($end_time_info["diff_count"]) ? "早退" : "○";
				}
			} else {
			// 出勤同時刻退勤または、出勤時間＞退勤時間の場合で
			// 月の開始日前は処理しない
				if ($tmp_date < $start_date) {
					$tmp_date = next_date($tmp_date);
					$counter_for_week++;
					continue;
				}
			}
		} else {
			$time1 = "";
			$time2 = "";
			$time3 = "";
			$time4 = "";
			$time_outtime = "";
			$time_rest = "";
			$return_count = "";
			$return_time = "";

			// 出退勤時刻の表示設定
			$time_cell_class = "tm";
			if ($show_time_flg != "t") {
				$time_cell_class = "mark";

				$start_date_time = $timecard_common_class->get_timecard_start_date_time($tmp_date, $start_time, $previous_day_flag);
				$end_date_time   = $timecard_common_class->get_timecard_end_date_time($tmp_date, $end_time, $next_day_flag);

				if (empty($start2_date_time) || empty($end2_date_time)) {
					$start2_date_time  = $start_date_time;
					$end2_date_time    = $end_date_time;
				}

				switch ($reason) {
				case "2":  // 午前有休
				case "38": // 午前年休
					$arr_time2 = $timecard_common_class->get_minute_time_array($start4_date_time, $end2_date_time);  // 休憩後の所定労働
					break;
				case "3":  // 午後有休
				case "39": // 午後年休
					$arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end4_date_time);  // 休憩前の所定労働
					break;
				default:
					$arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end2_date_time);  // 所定労働
					break;
				}

				if ($start_time != "") {
					if ($end2_date_time == "") {
						$start_time = "○";
						$show_start_time = "○";
					} else {
						$arr_tmp_work_time = $timecard_common_class->get_minute_time_array($start_date_time, $end2_date_time);
						$start_time_info = $timecard_common_class->get_start_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction1, $fraction1_min, $fraction3, $others4, $others5);
						$start_time = ($start_time_info["diff_count"]) ? "遅刻" : "○";
						$show_start_time = ($start_time_info["diff_count"]) ? "遅刻" : "○";
					}
				} else if ($end_time != "") {
					if ($start2_date_time == "") {
						$show_end_time = "○";
					} else {
						$arr_tmp_work_time = $timecard_common_class->get_minute_time_array($start2_date_time, $end_date_time);
						$end_time_info = $timecard_common_class->get_end_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction2, $fraction2_min, $fraction3, $others4);
						$show_end_time = ($end_time_info["diff_count"]) ? "早退" : "○";
					}
				}
			}

			// 事由が「有給休暇」「代替休暇」「振替休暇」の場合、
			// 所定労働時間を稼働時間にプラス
			// 「有給休暇」を除く $reason == "1" || 2008/10/14 
			if ($reason == "4" || $reason == "17") {
				if ($tmp_date >= $start_date) {
					$sums[13] += $specified_time;
					$work_time_for_week += $specified_time;
				}
			}
/*
			if ($arr_irrg_info["irrg_type"] == "1" && $counter_for_week == 7) {
				$tmp_irrg_minutes = $arr_irrg_info["irrg_minutes"];
				if ($hol_minus == "t") {
					$tmp_irrg_minutes -= (8 * 60 * $holiday_count);
				}
				$time1 = ($work_time_for_week > $tmp_irrg_minutes) ? $work_time_for_week - $tmp_irrg_minutes : 0;
				$sums[14] += $time1;
				$time1 = minute_to_hmm($time1);
			}
*/
			// 退勤後復帰回数を算出
			$return_count = 0;
			// 退勤後復帰時間を算出 20100413
			$return_time = 0;
            //申請状態 "0":申請不要 "3":承認済 ※未承認時は計算しない //勤務時間を計算しないフラグ確認 20130225
			if (($return_link_type == "0" || $return_link_type == "3") && $no_count_flag != "1") { 
				for ($i = 1; $i <= 10; $i++) {
					$start_var = "o_start_time$i";
					if ($$start_var == "") {
						break;
					}
					$return_count++;

					$end_var = "o_end_time$i";
					if ($$end_var == "") {
						break;
					}
					//端数処理
					if ($fraction1 > "1") {
						//端数処理する分
						$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
						//開始日時
						$tmp_start_date_time = $tmp_date.$$start_var;
						//基準日時
						$tmp_base_date_time = substr($tmp_start_date_time,0,10)."00";
						$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_start_date_time, $moving_minutes, $fraction1);
						$tmp_ret_start_time = str_replace(":", "",substr($tmp_date_time, 8, 5));
					} else {
						$tmp_ret_start_time = str_replace(":", "", $$start_var);
					}
					if ($fraction2 > "1") {
						//端数処理する分
						$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
						//終了日時
						$tmp_end_date_time = $tmp_date.$$end_var;
						//基準日時
						$tmp_base_date_time = substr($tmp_end_date_time,0,10)."00";
						$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_end_date_time, $moving_minutes, $fraction2);
						
						$tmp_ret_end_time = str_replace(":", "",substr($tmp_date_time, 8, 5));
					} else {
						$tmp_ret_end_time = str_replace(":", "", $$end_var);
					}
					$arr_ret = $timecard_common_class->get_return_array($tmp_date.$tmp_ret_start_time, $tmp_ret_start_time, $tmp_ret_end_time);
					
					$return_time += count($arr_ret);
					
					//前日・翌日の深夜帯
					//$arr_time5 = $timecard_common_class->get_late_night_shift_array($tmp_date, $start5, $end5);  // 深夜帯
					// 月の深夜勤務時間に退勤後復帰の深夜分を追加
					//$sums[15] += count(array_intersect($arr_ret, $arr_time5));
				}
			}
			$wk_return_time = $return_time;
			//分を時分に変換
			$return_time = minute_to_hmm($return_time);
            // 回数追加 20121109
            if ($return_count != "") {
                $return_time .= " ".$return_count;
            }
            
			// 前日までの計 = 週計
			$last_day_total = $work_time_for_week;
			
			$work_time_for_week += $work_time;
			$work_time_for_week += $wk_return_time;
			
			if ($tmp_date < $start_date) {
				$tmp_date = next_date($tmp_date);
				$counter_for_week++;
				continue;
			}
		}

		$wk_hoteigai = 0;
		$wk_hoteinai_zan = 0;
        $wk_hoteinai = 0;
        
		// 日数換算
		$total_workday_count = "";
		if (($start_time != "" && $end_time != "") || $after_night_duty_flag == "1") //明けを追加 20110819
		{
			//休暇時は計算しない 20090806
			if ($pattern != "10") {
				if($workday_count != "")
				{
					$total_workday_count = $workday_count;
				}
			}

			if($night_duty_flag)
			{
				//曜日に対応したworkday_countを取得する
				$total_workday_count = $total_workday_count + $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);
			}
		}
		else if ($workday_count == 0){
			//日数換算に0が指定された場合のみ0を入れる
			$total_workday_count = $workday_count;
		}
		//法定外残業の基準時間
		if ($base_time == "") {
			$wk_base_time = 8 * 60;
		} else {
			$base_hour = intval(substr($base_time, 0, 2));
			$base_min = intval(substr($base_time, 2, 2));
			$wk_base_time = $base_hour * 60 + $base_min;
		}
		//公休か事由なし休暇で残業時刻がありの場合 20100802
		if ($legal_hol_over_flg) {
			$wk_base_time = 0;
		}
		//残業時間 20100209 変更 20100910
		$wk_zangyo = 0;
		$kinmu_time = 0;
		//出勤時刻、退勤時刻が両方入っている場合に計算する 20101004
        //勤務時間を計算しないフラグが"1"以外 20130225
        if ($start_time != "" && $end_time != "" && $no_count_flag != "1") {
            //残業承認時に計算、申請画面を表示しない場合は無条件に計算
			if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
					$timecard_bean->over_time_apply_type == "0"
				) {
				
				//残業開始、終了時刻がある場合
				if ($over_start_time != "" && $over_end_time != "") {
					$wk_zangyo = date_utils::get_time_difference($over_end_date_time,$over_start_date_time);
				//残業時刻未設定は計算しない 20110825	
				//} else {
				//	//残業時刻がない場合で、所定終了、退勤を比較後残業計算 20100913
				//	if ($end2_date_time < $fixed_end_time) {
				//		$wk_zangyo = date_utils::get_time_difference($fixed_end_time,$end2_date_time);
				//	}
				}
				//残業２
				if ($over_start_time2 != "" && $over_end_time2 != "") {
					$over_start_date_time2 = $tmp_date.$over_start_time2;
					if ($over_start_next_day_flag2 == 1) {
						$over_start_date_time2 = next_date($tmp_date).$over_start_time2;
					}
					$over_end_date_time2 = $tmp_date.$over_end_time2;
					if ($over_end_next_day_flag2 == 1) {
						$over_end_date_time2 = next_date($tmp_date).$over_end_time2;
					}
					
					$wk_zangyo2 = date_utils::get_time_difference($over_end_date_time2,$over_start_date_time2);
					$wk_zangyo += $wk_zangyo2;
				}				
				//早出残業
				if ($early_over_time != "") {
					$wk_early_over_time = hmm_to_minute($early_over_time);
					$wk_zangyo += $wk_early_over_time;
				}				
			}
			//呼出
			$wk_zangyo += $wk_return_time;
			
            //コメント化 20130822
			//if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
			//	//休日残業時は、休憩減算 20100916
			//	if ($legal_hol_over_flg == true) {
			//		if ($wk_zangyo >= ($time3_rest + $time4_rest)) {
			//			$wk_zangyo -= ($time3_rest + $time4_rest);
			//		}
			//	}
			//}		
			//休日残業で休憩時刻がある場合 20100921
			if ($legal_hol_over_flg == true && $tmp_rest2 > 0) {
				$wk_zangyo -= $tmp_rest2;
			}
			//出勤時刻と残業開始時刻が一致している場合、所定、遅刻、早退を計算せず、残業、休憩のみ計算する 20100917
			if ($start2 != "" && $start2 == $over_start_time && $over_start_next_day_flag != "1") {//残業開始が翌日でないことを追加 20101027
				$overtime_only_flg = true;	
				//遅刻
				$tikoku_time = 0;
				$start_time_info["diff_minutes"] = 0;
				//早退
				$sotai_time = 0;
				$end_time_info["diff_minutes"] = 0;
				//残業承認時に計算、申請画面を表示しない場合は無条件に計算 20100922
				if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
						$timecard_bean->over_time_apply_type == "0"
					) {
					$wk_zangyo -= $tmp_rest2;
				} else {
					$tmp_rest2 = 0;
					$time_rest = "";
				}
			} else {
				$overtime_only_flg = false;	
				//遅刻
				$tikoku_time = $start_time_info["diff_minutes"];
				//早退
				$sotai_time = $end_time_info["diff_minutes"];
			}		
			$tikoku_time2 = 0;
			// 遅刻時間を残業時間から減算する場合
			if ($timecard_bean->delay_overtime_flg == "1") {
				if ($wk_zangyo >= $tikoku_time) { //20100913
					$wk_zangyo -= $tikoku_time;
				} else { // 残業時間より多い遅刻時間対応 20100925 
					$tikoku_time2 = $tikoku_time - $wk_zangyo;
					$wk_zangyo = 0;
				}
			}
			
			//勤務時間＝所定労働時間−休憩時間−外出時間−早退−遅刻＋残業時間 20100907
			//　残業時間＝承認済み残業時間＋承認済み呼出し勤務時間
			//　承認済み残業時間＝早出残業時間＋勤務終了後残業時間
			//所定
			if ($pattern == "10" || //休暇は所定を0とする 20100916
					$overtime_only_flg == true ||	//残業のみ計算する 20100917
					$after_night_duty_flag == "1"  //明けの場合 20110819
				) {
				$shotei_time = 0;
			} elseif ($start2 == "" && $end2 == "") {  // 所定時刻指定がない場合、端数処理不具合対応 20101005
				$shotei_time = count($arr_effective_time_wk); //休憩を引く前の実働時間
			} else {
				$shotei_time = count($arr_time2);
			}
			//時間有休追加 20111207 
			if ($obj_hol_hour->paid_hol_hour_flag == "t") {
				$shotei_time -= $paid_time_hour;
			}
			//休憩 $tmp_rest2 $time3_rest $time4_rest
			//外出 $time3 $time4
			$kinmu_time = $shotei_time - $time3 - $time4 + $wk_zangyo;
			// 遅刻時間を残業時間から減算しない場合
			if ($timecard_bean->delay_overtime_flg == "2") {
				$kinmu_time -= $tikoku_time;
			}			
			// 遅刻時間を残業時間から減算する場合で、残業未承認の場合で遅刻がある場合は引く 20100914
			else {
				if ($overtime_approve_flg == "1") {
					$kinmu_time -= $tikoku_time;
				} else { //遅刻時間不具合対応 20101005
					// 残業時間より多い遅刻時間対応 20100925
					$kinmu_time -= $tikoku_time2;
				}
			}
			// 早退時間を残業時間から減算しない場合
			if ($timecard_bean->early_leave_time_flg == "2") {
				$kinmu_time -= $sotai_time;
			}			
			//時間給者 20100915
			if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
				//休日残業時は、休憩を減算しない 20100916
                //if ($legal_hol_over_flg != true) { //コメント化 20130822
					$kinmu_time -= ($time3_rest + $time4_rest);
				//}
			} else {
				if ($overtime_only_flg != true && $legal_hol_over_flg != true) {	//残業のみ計算する場合と休日残業の場合、残業から引くためここでは引かない 20100921
					//月給制日給制、実働時間内の休憩時間
					$kinmu_time -= $tmp_rest2;
				}
			}
			
			//法定内、法定外残業の計算
			if ($wk_zangyo > 0) {
				//法定内入力有無確認
				if ($legal_in_over_time != "") {
					$wk_hoteinai_zan = hmm_to_minute($legal_in_over_time);
					if ($wk_hoteinai_zan <= $wk_zangyo) {
						$wk_hoteigai = $wk_zangyo - $wk_hoteinai_zan;
					} else {
						$wk_hoteigai = 0;
					}
					
				} else {
					//法定外残業の基準時間を超える分を法定外残業とする
					if ($kinmu_time > $wk_base_time) {
						$wk_hoteigai = $kinmu_time - $wk_base_time;
					}
					//
					if ($wk_zangyo - $wk_hoteigai > 0) {
						$wk_hoteinai_zan = $wk_zangyo - $wk_hoteigai;
					} else {
						//マイナスになる場合
						$wk_hoteinai_zan = 0;
						$wk_hoteigai = $wk_zangyo;
					}
				}
			}
			//合計
			//法定内
			$wk_hoteinai = $kinmu_time - $wk_zangyo;
			$hoteinai += $wk_hoteinai;
			//法定内残業
			$hoteinai_zan += $wk_hoteinai_zan;
			//法定外残
			$hoteigai += $wk_hoteigai;
			//勤務時間
			$sums[13] += $kinmu_time;
			//残業時間
			$sums[14] += $wk_zangyo;
		}
		
//comment化
if ($dummy == "1") {		
		//残業承認時に計算、申請画面を表示しない場合は無条件に計算 20100608
		if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
				$timecard_bean->over_time_apply_type == "0"
				|| $wk_return_time > 0 // 退勤後復帰がある場合 20100714
				|| $legal_hol_over_flg) { // 公休か残業時刻ありを条件追加 20100802
			//休暇時は計算しない 20090806
			if ($pattern != "10" ||
					$legal_hol_over_flg) { // 公休で残業時刻ありを条件追加 20100802
				
				// 遅刻時間を残業時間から減算しない場合
				//			if ($timecard_bean->delay_overtime_flg == "2") {
				//				$work_time += $start_time_info["diff_minutes"];
				//			}
				
				//法定内勤務、法定内残業、法定外残業
				// 前日までの累計 >= 40時間
				//法定外残業で週４０時間超過分 1:残業時間として計算するの場合
				if ($last_day_total >= 40 * 60 && $overtime_40_flg == "1") {
					//  法定外 += 稼動時間
					//下の処理で合計するためここはコメント化
					//			$hoteigai += $work_time + $wk_return_time;
				} else if ($work_time + $wk_return_time > 0){
					// 上記以外、かつ、 稼動時間がある場合
					
					//法定内勤務と法定外残業
					//法定内勤務のうち週40時間を超える分から法定外残業へ移動
					
					//wk法定外
					//残業時刻から計算 20100525
					if ($over_calc_flg) {
						/* 20100713
						// 端数処理する
						if ($fraction2 > "1") {
							//端数処理する分
							$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
							//基準日時
							if ($end2_date_time != "") {
								$tmp_office_start_time = substr($end2_date_time, 0, 10) . "00";
							} else {
								$tmp_office_start_time = substr($start_date_time, 0, 10) . "00";
							}
							$tmp_over_end_date_time = date_utils::move_time($tmp_office_start_time, $over_end_date_time, $moving_minutes, $fraction2);
						} else {
						*/
							$tmp_over_end_date_time = $over_end_date_time;
						//}
						$arr_over_time = $timecard_common_class->get_minute_time_array($over_start_date_time,$tmp_over_end_date_time);
						//20100712
						$wk_hoteigai = count(array_diff($arr_over_time, $arr_time4));
						$wk_hoteigai_org = $wk_hoteigai;
						if ($wk_hoteigai <= $effective_time) {
							$wk_hoteigai = $effective_time - $wk_base_time;
							if ($wk_hoteigai < 0) {
								$wk_hoteigai = 0;
							}
						}
						
						// 所定労働内の休憩時間 20100706
						$tmp_rest1 = count(array_intersect(array_diff($arr_time2, $arr_over_time), $arr_time4));
						// 所定労働外（残業時間内）の休憩時間
						$tmp_rest3 = count(array_intersect($arr_over_time, $arr_time4));
						$time_rest = minute_to_hmm($tmp_rest1);
						if ($tmp_rest3 > 0) {
							$time_rest .= "(".minute_to_hmm($tmp_rest3).")";
						}
						
					}else {
						$wk_hoteigai = $work_time + $wk_return_time - $wk_base_time;
					}
					
					if ($wk_hoteigai > 0) {
						// wk法定内 = (稼動時間 - wk法定外)
						$wk_hoteinai = ($work_time + $wk_return_time - $wk_hoteigai);
						// 40時間を超えた法定外を除外する
						if ($overtime_40_flg == "1") {
							$wk_jogai = $last_day_total + $work_time + $wk_return_time - (40 * 60);
							if ($wk_jogai > 0) {
								$wk_hoteigai -= $wk_jogai;
							}
						}
						//法定外 += wk法定外
						//				$hoteigai += $wk_hoteigai;
					} else {
						// wk法定内 = (稼動時間)
						$wk_hoteinai = ($work_time + $wk_return_time);
						//マイナスを0とする
						$wk_hoteigai = 0;
					}
					//echo("$tmp_date n=$wk_hoteinai g=$wk_hoteigai "); //debug
					//前日までの累計 ＋ wk法定内　＞ 40時間 の場合
					if ($last_day_total + $wk_hoteinai > 40 * 60) {
						if ($overtime_40_flg == "1") {
							$wk_hoteigai = 0;
							//法定内 += (40時間 - 前日までの累計) 20090722 変更
							$hoteinai += (40 * 60) - $last_day_total;
						} else {
							//法定内 += wk法定内 20090722 変更
							$hoteinai += $wk_hoteinai;
						}
						
					} else {
						//上記以外
						//法定内 += wk法定内
						$hoteinai += $wk_hoteinai;
					}
					
					// 法定内残業、所定労働時間から8時間まで間を加算
					// 稼動時間が所定労働時間より大きい場合
					//			$wk_minute = date_utils::hi_to_minute($day1_time);
					$wk_minute = $specified_time;
					// 時間帯の法定外残業の計算を使用する 480 -> $wk_base_time 20091218 20100713
					if (($wk_minute != 0 && $wk_minute < $wk_base_time && ($work_time + $wk_return_time) > $wk_minute) || $over_calc_flg) {
					
						//稼動時間が法定外残業の計算時間以上
						if (($work_time + $wk_return_time) >= $wk_base_time) {
							//基準法定内残業
							$wk_std_hoteinai_zan = $wk_base_time - $wk_minute;
						} else {
							//8時間未満の場合、稼動時間-所定労働時間
							$wk_std_hoteinai_zan = ($work_time + $wk_return_time) - $wk_minute;
							//20100713
							if ($over_calc_flg && $wk_hoteigai_org > ($wk_base_time - $specified_time)) {
								$wk_std_hoteinai_zan = $wk_hoteinai;
							}
						}
						
						//法定外残業で週４０時間超過分 1:残業時間として計算するの場合
						if ($overtime_40_flg == "1") {
							//前日までの累計 + 所定時間 ＜ 40時間 の場合
							if ($last_day_total + $wk_minute < 40 * 60) {
								//wk法定内残業 = 40時間 - (前日までの累計 + 所定時間)
								$wk_hoteinai_zan = 40 * 60 - ($last_day_total + $wk_minute);
								//wk法定内残業 > 基準法定内残業
								if ($wk_hoteinai_zan > $wk_std_hoteinai_zan) {
									// wk法定内残業 = 基準法定内残業
									$wk_hoteinai_zan = $wk_std_hoteinai_zan;
								}
								//法定内勤務 -= wk法定内残業
								$hoteinai -= $wk_hoteinai_zan;
								//法定内残業 += wk法定内残業
								$hoteinai_zan += $wk_hoteinai_zan;
								
								$sums[14] += $wk_hoteinai_zan; // 変更 20090722
							}
						} else {
							// wk法定内残業 = 基準法定内残業
							$wk_hoteinai_zan = $wk_std_hoteinai_zan;
							//法定内勤務 -= wk法定内残業
							$hoteinai -= $wk_hoteinai_zan;
							//法定内残業 += wk法定内残業
							$hoteinai_zan += $wk_hoteinai_zan;
							
							$sums[14] += $wk_hoteinai_zan; // 変更 20090722
						}
					}
					//echo("$tmp_date m=$wk_minute l=$last_day_total z=$wk_hoteinai_zan "); //debug
				}
			}
			
			//1日毎の法定外残を週単位に集計
			$wk_week_hoteigai += $wk_hoteigai;
			
			//法定外残業で週４０時間超過分 1:残業時間として計算するの場合
			if ($overtime_40_flg == "1") {
				if ($counter_for_week == 7) {
					if ($arr_irrg_info["irrg_type"] == "1") {
						$tmp_irrg_minutes = $arr_irrg_info["irrg_minutes"];
					} else {
						$tmp_irrg_minutes = 40 * 60; // 変則労働期間以外の場合40時間
					}
					if ($hol_minus == "t") {
						$tmp_irrg_minutes -= (8 * 60 * $holiday_count);
					}
					$time1 = ($work_time_for_week > $tmp_irrg_minutes) ? $work_time_for_week - $tmp_irrg_minutes : 0;
					//					$sums[14] += $wk_week_hoteigai;
					$wk_hoteigai = $time1 + $wk_week_hoteigai;
					$hoteigai += $wk_hoteigai;
				} else {
					$wk_hoteigai = "";
				}
				//20090722 前日までの累計+稼働時間 >= 40時間、かつ、半日の場合、所定時間を基準とする
				if ($last_day_total + $work_time + $wk_return_time >= 40 * 60 && $workday_count == 0.5) {
					$wk_base_time = $specified_time;
				}
				$wk_hoteigai_yoteigai = $work_time + $wk_return_time - $wk_base_time;
				if ($wk_hoteigai_yoteigai > 0) {
					if ($others1 != "4") { //勤務時間内に外出した場合が、残業時間から減算するでない場合
						$wk_hoteigai_yoteigai += $time3;
					}
					if ($others2 != "4") { //勤務時間外に外出した場合が、残業時間から減算するでない場合
						$wk_hoteigai_yoteigai += $time4;
					}
					$sums[14] += $wk_hoteigai_yoteigai;
				}
			} else {
				//				$sums[14] += $wk_hoteigai + $wk_hoteinai_zan;
				//				if ($arr_irrg_info["irrg_type"] == "1") {
				//					$sums[14] += $wk_hoteigai;
				//				}
				//				$hoteigai += $wk_hoteigai;
				$time1 = 0;
				// 出退勤とも打刻済みの場合
				if ($start_time != "" && $end_time != "") {
					//					$tmp_specified_time = $specified_time;
					//				$tmp_specified_time = 480; //法定内8時間を所定として処理 20090717
					$tmp_specified_time = $wk_base_time; //日数換算対応 20090803
					// 遅刻時間を残業時間から減算しない場合（所定時間から引くことで合わせる）
					if ($timecard_bean->delay_overtime_flg == "2") {
						$tmp_specified_time = $tmp_specified_time - $start_time_info["diff_minutes"];
						if ($tmp_specified_time < 0) {
							$tmp_specified_time = 0;
						}
					}
					//休暇時は計算しない 20090806 公休か事由なし休暇で残業時刻が設定済を条件追加 20100802
					if ($pattern != "10" || $legal_hol_over_flg) {
						//残業時刻から計算 20100525
						if ($over_calc_flg) {
							//法定外 20100705
							$time1 = $wk_hoteigai;
							
						} else {
							if ($effective_time > $tmp_specified_time) {
								$time1 = $effective_time - $tmp_specified_time;
							} else {
								$time1 = $wk_hoteigai; //20100714
							}
						}
						
						if ($others1 == "4") { //勤務時間内に外出した場合
							if ($time1 > $time3) {
								$time1 -= $time3;
							}
						}
						if ($others2 == "4") { //勤務時間外に外出した場合
							if ($time1 > $time4) {
								$time1 -= $time4;
							}
						}
					}
				}
				$wk_hoteigai = $time1; //20100714
				
				$sums[14] += $wk_hoteigai;
				$hoteigai += $wk_hoteigai;
			}
			//法定内残業が入力された場合 20100531
			if ($legal_in_over_time != "") {
				//合計を戻す
				$sums[14] -= $wk_hoteinai_zan;
				$hoteinai_zan -= $wk_hoteinai_zan;
				$sums[14] -= $wk_hoteigai;
				$hoteigai -= $wk_hoteigai;
				
				$wk_zangyo = $wk_hoteinai_zan + $wk_hoteigai;
				
				//法定内残業と法定外残業に分ける
				$wk_hoteinai_zan = hmm_to_minute($legal_in_over_time);
				if ($wk_hoteinai_zan <= $wk_zangyo) {
					$wk_hoteigai = $wk_zangyo - $wk_hoteinai_zan;
				} else {
					$wk_hoteigai = 0;
				}
				//再度、合計する
				$sums[14] += $wk_hoteinai_zan;
				$hoteinai_zan += $wk_hoteinai_zan;
				$sums[14] += $wk_hoteigai;
				$hoteigai += $wk_hoteigai;
				
			}
			//早出残業が入力された場合 20100601
			if ($early_over_time != "") {
				//申請中の場合は計算しない	// 残業未承認か確認
				if (!($overtime_approve_flg == "1" || ($return_link_type != "0" && $return_link_type != "3"))) {
					
					$wk_early_over_time = hmm_to_minute($early_over_time);
					$work_time += $wk_early_over_time;
					$wk_zangyo += $wk_early_over_time;
					$wk_hoteinai_zan += $wk_early_over_time;
					//合計する
					$sums[13] += $wk_early_over_time;
					$sums[14] += $wk_early_over_time;
					$hoteinai_zan += $wk_early_over_time;
				}
				
			}
		}
} //comment化		
		
		// 表示処理
		//表示用退避から設定 20100802
		$start_time = $disp_start_time; 
		$end_time = $disp_end_time; 

		$rowspan = ($view == "1") ? "1" : "2";
		// 背景色設定。法定、祝祭日を赤、所定を青にする
		$bgcolor = get_timecard_bgcolor($type);
		// 2段表示時の背景。#f6f9ffから変更
		if ($bgcolor == "#ffffff") {
			$bgcolor2 = "#f6f9ff";
		} else {
			$bgcolor2 = $bgcolor;
		}
		echo("<tr bgcolor=\"$bgcolor\">\n");
		//日付
		echo("<td rowspan=\"$rowspan\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_month}月{$tmp_day}日</font></td>\n");
		//曜日
		echo("<td rowspan=\"$rowspan\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . get_weekday(to_timestamp($tmp_date)) . "</font></td>\n");

		if ($timecard_bean->time_move_flag == "t" && $view != "2"){

			if ($timecard_bean->zen_display_flag == "t"){

				// 前日になる場合があるフラグ
				$previous_day_value = "";
				if ($previous_day_flag == 1){
					$previous_day_value = "前";
				}
				echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($previous_day_value) . "$hspacer</font></td>\n");
			}

			echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($show_start_time)) . "</font></td>\n"); //20110513

			if ($timecard_bean->yoku_display_flag == "t"){

		        // 翌
		        $next_day = "";
				if ($show_next_day_flag == 1){	//残業時刻日またがり対応 20100114
					$next_day = "翌";
				}
				echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($next_day) . "$hspacer</font></td>\n");
			}

			echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($show_end_time)) . "</font></td>\n");
			
			//勤務
			echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($kinmu_time)) . "$hspacer</font></td>\n"); //$work_time + $wk_return_timeから変更 20100910
			//遅刻 20100907 赤色に変更 20100922
			echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">" . nbsp(minute_to_hmm($start_time_info["diff_minutes"])) . "$hspacer</font></td>\n");
			//早退 20100907 赤色に変更 20100922
			echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">" . nbsp(minute_to_hmm($end_time_info["diff_minutes"])) . "$hspacer</font></td>\n");
			//残業管理をしない場合の対応 2008/10/15
			if ($no_overtime == "t") {
				$time1 = "";	// 残業時間を表示しない
				$wk_hoteinai_zan = "";
				$wk_hoteigai = "";
			}

			if ($timecard_bean->over_disp_split_flg != "f"){
				//法内残
				echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteinai_zan)) . "</font></td>\n");
				//法外残
				echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteigai)) . "</font></td>\n");
			} else {
				//残業=法内残+法外残
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteinai_zan+$wk_hoteigai)) . "$hspacer</font></td>\n");
			}
		}

		if ($timecard_bean->time_move_flag != "t"){
			//種別
			if ($timecard_bean->type_display_flag == "t"){
				echo("<td rowspan=\"$rowspan\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($type_name) . "</font></td>\n");
			}
		}

		// 予実績表示の場合
		if ($view == "2") {

			// 勤務日種別と出勤パターン（予定）から所定労働時間帯を取得
			$prov_start2 = $timecard_common_class->get_officehours_info( $prov_tmcd_group_id, $prov_pattern, $tmp_type, "start2" );
			$prov_end2   = $timecard_common_class->get_officehours_info( $prov_tmcd_group_id, $prov_pattern, $tmp_type, "end2" );

            //個人別所定時間を優先する場合 20120309
            if ($arr_empcond_officehours_ptn[$prov_tmcd_group_id][$prov_pattern] == 1) {
                $wk_weekday = $arr_weekday_flg[$tmp_date];
                $arr_empcond_officehours = $timecard_common_class->get_empcond_officehours($wk_weekday);
                if ($arr_empcond_officehours["officehours2_start"] != "") {
                    $prov_start2 = $arr_empcond_officehours["officehours2_start"];
                    $prov_end2 = $arr_empcond_officehours["officehours2_end"];
                }
            }
            
            // 所定労働時間が確定できたら予定時刻として使用
			if ($prov_start2 != "" && $prov_end2 != "") {
				$prov_start_time = $prov_start2;
				$prov_end_time = $prov_end2;
			}

			// 表示値の編集
			if ($prov_pattern != "") {
				$prov_pattern = $atdbk_common_class->get_pattern_name($prov_tmcd_group_id, $prov_pattern);
			}
			if ($prov_reason != "") {
				$prov_reason = $atdbk_common_class->get_reason_name($prov_reason);
			}

            if($prov_night_duty == "1")
            {
                $prov_night_duty = "有り";
            }
            else if($prov_night_duty == "2")
            {
                $prov_night_duty = "無し";
            }

			$prov_allow_contents = "";
			if ($prov_allow_id != "")
			{
				foreach($arr_allowance as $allowance)
				{
					if($prov_allow_id == $allowance["allow_id"])
					{
						$prov_allow_contents = $allowance["allow_contents"];
						break;
					}
				}
			}

			//グループ名取得
			$prov_group_name = $atdbk_common_class->get_group_name($prov_tmcd_group_id);

			if($timecard_bean->time_move_flag == "t"){
				if ($timecard_bean->zen_display_flag == "t"){
					echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
				}
				echo("<td align=\"right\" bgcolor=\"$bgcolor2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($prov_start_time)) . "</font></td>\n");
				if ($timecard_bean->yoku_display_flag == "t"){
					echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
				}
				echo("<td align=\"right\" bgcolor=\"$bgcolor2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($prov_end_time)) . "</font></td>\n");
				echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
				//遅刻 20100907
				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				//早退 20100907
				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				if ($timecard_bean->over_disp_split_flg != "f"){
					//法定内残、法定外残
					echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
					echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
				} else {
					echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
				}
			}

			if ($timecard_bean->time_move_flag == "t"){
				//種別
				if ($timecard_bean->type_display_flag == "t"){
					echo("<td rowspan=\"$rowspan\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($type_name) . "</font></td>\n");
				}
			}

			if ($timecard_bean->group_display_flag == "t"){
				echo("<td height=\"22\" bgcolor=\"$bgcolor2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_group_name) . "</font></td>\n");
			}
			echo("<td height=\"22\" bgcolor=\"$bgcolor2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_pattern) . "</font></td>\n");
			echo("<td bgcolor=\"$bgcolor2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_reason) . "</font></td>\n");
			echo("<td bgcolor=\"$bgcolor2\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_night_duty) . "</font></td>\n");
			echo("<td bgcolor=\"$bgcolor2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_allow_contents) . "</font></td>\n");
			if($timecard_bean->time_move_flag == "f"){
				if ($timecard_bean->zen_display_flag == "t"){
					echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
				}
				echo("<td align=\"right\" bgcolor=\"$bgcolor2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($prov_start_time)) . "</font></td>\n");
				if ($timecard_bean->yoku_display_flag == "t"){
					echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
				}
				echo("<td align=\"right\" bgcolor=\"$bgcolor2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($prov_end_time)) . "</font></td>\n");
				echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
				//遅刻 20100907
				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				//早退 20100907
				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				if ($timecard_bean->over_disp_split_flg != "f"){
					//法定内残、法定外残
					echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
					echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
				} else {
					echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
				}
			}
			echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
			echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
			echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
			if ($timecard_bean->ret_display_flag == "t"){
				echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
			}

			echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
			if($meeting_display_flag){
				echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
			}
			echo("</tr>\n");

			echo("<tr bgcolor=\"$bgcolor\">\n");
		}

		if ($timecard_bean->time_move_flag == "t" && $view == "2"){

			if ($timecard_bean->zen_display_flag == "t"){

				// 前日になる場合があるフラグ
				$previous_day_value = "";
				if ($previous_day_flag == 1){
					$previous_day_value = "前";
				}
				echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($previous_day_value) . "$hspacer</font></td>\n");
			}

			echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($show_start_time)) . "</font></td>\n"); //20110513

			if ($timecard_bean->yoku_display_flag == "t"){

		        // 翌
		        $next_day = "";
				if ($show_next_day_flag == 1){	//残業時刻日またがり対応 20100114
					$next_day = "翌";
				}
				echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($next_day) . "$hspacer</font></td>\n");
			}

			echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($show_end_time)) . "</font></td>\n");
			//勤務
			echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($kinmu_time)) . "$hspacer</font></td>\n"); //$work_time + $wk_return_timeから変更 20100910
				//遅刻 20100907 赤色に変更 20100922
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">" . nbsp(minute_to_hmm($start_time_info["diff_minutes"])) . "$hspacer</font></td>\n");
				//早退 20100907 赤色に変更 20100922
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">" . nbsp(minute_to_hmm($end_time_info["diff_minutes"])) . "$hspacer</font></td>\n");
			//残業管理をしない場合の対応 2008/10/15
			if ($no_overtime == "t") {
				$time1 = "";	// 残業時間を表示しない
				$wk_hoteinai_zan = "";
				$wk_hoteigai = "";
			}

			if ($timecard_bean->over_disp_split_flg != "f"){
				//法内残
				echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteinai_zan)) . "</font></td>\n");
				//法外残
				echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteigai)) . "</font></td>\n");
			} else {
				//残業=法内残+法外残
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteinai_zan+$wk_hoteigai)) . "$hspacer</font></td>\n");
			}
		}

		if ($timecard_bean->time_move_flag == "t" && $view != "2"){
			//種別
			if ($timecard_bean->type_display_flag == "t"){
				echo("<td rowspan=\"$rowspan\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($type_name) . "</font></td>\n");
			}
		}

		//グループ
		if ($timecard_bean->group_display_flag == "t"){
			echo("<td height=\"22\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			echo(nbsp($atdbk_common_class->get_group_name($tmcd_group_id)));
			echo("</font></td>\n");
		}

		//パターン
		echo("<td height=\"22\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo(nbsp($atdbk_common_class->get_pattern_name($tmcd_group_id, $pattern)));
		echo("</font></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo(nbsp($atdbk_common_class->get_reason_name($reason)));

		echo("</font></td>\n");

        $tmp_night_duty = "";
        if($night_duty == "1")
        {
            $tmp_night_duty = "有り";
        }
        else if($night_duty == "2")
        {
            $tmp_night_duty = "無し";
        }
	    echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo(nbsp($tmp_night_duty));
	    echo("</font></td>\n");

		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		if ($allow_id != "")
		{
			foreach($arr_allowance as $allowance)
			{
				if($allow_id == $allowance["allow_id"])
				{
					echo($allowance["allow_contents"]);
					break;
				}
			}
		}
		else
		{
			echo("&nbsp;");
		}
		echo("</font></td>\n");

		if ($timecard_bean->time_move_flag == "f" || empty($timecard_bean->time_move_flag)){

			if ($timecard_bean->zen_display_flag == "t"){

				// 前日になる場合があるフラグ
				$previous_day_value = "";
				if ($previous_day_flag == 1){
					$previous_day_value = "前";
				}
				echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($previous_day_value) . "$hspacer</font></td>\n");
			}

			echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($show_start_time)) . "</font></td>\n"); //20110513

			if ($timecard_bean->yoku_display_flag == "t"){

		        // 翌
		        $next_day = "";
				if ($show_next_day_flag == 1){	//残業時刻日またがり対応 20100114
					$next_day = "翌";
				}
				echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($next_day) . "$hspacer</font></td>\n");
			}

			echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($show_end_time)) . "</font></td>\n");
//残業管理をしない場合の対応 2008/10/15
			if ($no_overtime == "t") {
				$time1 = "";	// 残業時間を表示しない
				$wk_hoteinai_zan = "";
				$wk_hoteigai = "";
			}

			//勤務
			echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($kinmu_time)) . "$hspacer</font></td>\n"); //$work_time + $wk_return_timeから変更 20100910
			//遅刻 20100907 赤色に変更 20100922
			echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">" . nbsp(minute_to_hmm($start_time_info["diff_minutes"])) . "$hspacer</font></td>\n");
			//早退 20100907 赤色に変更 20100922
			echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">" . nbsp(minute_to_hmm($end_time_info["diff_minutes"])) . "$hspacer</font></td>\n");
			if ($timecard_bean->over_disp_split_flg != "f"){
				//法内残
				echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteinai_zan)) . "</font></td>\n");
				//法外残
				echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteigai)) . "</font></td>\n");
			} else {
				//残業=法内残+法外残
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteinai_zan+$wk_hoteigai)) . "$hspacer</font></td>\n");
			}
		}

		echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($time2) . "</font></td>\n");
		//普外−＞外出に変更 20090603
		echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($time_outtime) . "</font></td>\n");
		//残外−＞休憩に変更 20090603
		echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($time_rest) . "</font></td>\n");

		if ($timecard_bean->ret_display_flag == "t"){
			//退勤後復帰回数
//			echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($return_count) . "</font></td>\n");
			//退勤後復帰時間
			echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($return_time) . "</font></td>\n");
		}

		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . $total_workday_count . "<br></font></td>\n");

		if ($meeting_display_flag){
			//会議研修
			echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			$meeting_time_hh = "";
			$meeting_time_mm = "";
			// 開始終了時刻から時間を計算する 20091008
			if ($meeting_start_time != "" && $meeting_end_time != "") {
				$tmp_meeting_start_date_time = $tmp_date.$meeting_start_time;
				if ($meeting_start_time <= $meeting_end_time) {
					$tmp_meeting_end_date_time = $tmp_date.$meeting_end_time;
				} else {
					// 日またがりの場合
					$tmp_meeting_end_date_time = next_date($tmp_date).$meeting_end_time;
				}
				$tmp_meeting_time = date_utils::get_time_difference($tmp_meeting_end_date_time, $tmp_meeting_start_date_time);
				$tmp_meeting_hhmm = $timecard_common_class->minute_to_h_hh($tmp_meeting_time);
				echo($tmp_meeting_hhmm);
			} else 
			if ($meeting_time != null){
				$meeting_time_hh = (int)substr($meeting_time, 0, 2);
				$meeting_time_mm = (int)substr($meeting_time, 2, 4);
				echo($meeting_time_hh);

				if ($meeting_time_mm == 15)
				{
					echo(".25");
				}
				else if($meeting_time_mm == 30)
				{
					echo(".5");
				}
				else if($meeting_time_mm == 45)
				{
					echo(".75");
				}

			}
			echo("<br></font></td>\n");
		}

		echo("</tr>\n");


		//事由が休日出勤の場合
//		if ($reason == "16") {
//			$sums[31] += $work_time + $wk_return_time;
//		}

		$tmp_date = next_date($tmp_date);
		$counter_for_week++;
	} // end of while

	// 早退時間を残業時間から減算する設定の場合、法定内残業、法定外残業の順に減算する
	if ($timecard_bean->early_leave_time_flg == "1") {

		list($hoteinai_zan, $hoteigai) = $atdbk_common_class->get_overtime_early_leave($hoteinai_zan, $hoteigai, $early_leave_time);
		//残業時間 20100713
		$sums[14] = $hoteinai_zan + $hoteigai;
	}
	// 要勤務日数を取得 20091222
	$wk_year = substr($yyyymm, 0, 4);
	$wk_mon = substr($yyyymm, 4, 2);
//	$sums[0] = get_you_kinmu_su($con, $fname, $wk_year, $wk_mon, $arr_timecard_holwk_day, $closing);
	//当月日数
	if ($closing_month_flg == "2") { //締め日の月度が終了日の月の場合
		$wk_year2 = substr($start_date, 0, 4);
		$wk_mon2 = substr($start_date, 4, 2);
	} else {
		$wk_year2 = $wk_year;
		$wk_mon2 = $wk_mon;
	}
	$days_in_month = days_in_month($wk_year2, $wk_mon2);
	//対象年月の公休数を取得 20100615
	$legal_hol_cnt = get_legal_hol_cnt_month($con, $fname, $wk_year, $wk_mon, $closing, $closing_month_flg, $tmcd_group_id);
	$sums[0] = $days_in_month - $legal_hol_cnt;
	// 休日出勤 2008/10/23
	// 休日出勤のカウント方法変更 20091127
	//	$sums[2] = count($arr_hol_work_date);
	// 法定内 = 勤務時間 - 残業時間 20100713
	//$sums[28] = $hoteinai; //20100713
	$sums[28] = $sums[13] - $sums[14];
	// 法定内残業
	$sums[29] = $hoteinai_zan;
	// 法定外残業
	$sums[30] = $hoteigai;

	// 変則労働期間が月の場合
	if ($arr_irrg_info["irrg_type"] == "2") {

		// 所定労働時間を基準時間とする
		$tmp_irrg_minutes = $arr_irrg_info["irrg_minutes"];
		if ($hol_minus == "t") {
			$tmp_irrg_minutes -= (8 * 60 * $holiday_count);
		}
		$sums[12] = ($tmp_irrg_minutes > 0) ? $tmp_irrg_minutes : 0;

		// 稼働時間−基準時間を残業時間とする
		$sums[14] = $sums[13] - $sums[12];
	}
	else {
	// 基準時間の計算を変更 2008/10/21
	// 要勤務日数 × 所定労働時間 
		$sums[12] = $sums[0] * date_utils::hi_to_minute($day1_time);
	}

	// 月集計値の稼働時間・残業時間・深夜勤務を端数処理
	for ($i = 13; $i <= 15; $i++) {
		if ($sums[$i] < 0) {
			$sums[$i] = 0;
		}
		$sums[$i] = round_time($sums[$i], $fraction4, $fraction4_min);
	}

	// 残業管理をしない場合、残業時間、法定内勤務、法定内残業、法定外残業を０とする
	if ($no_overtime == "t") {
		$sums[14] = 0;
		$sums[28] = 0;
		$sums[29] = 0;
		$sums[30] = 0;
	}

	//時間有休 20111207
	$paid_hol_hour_total = 0;
	if ($obj_hol_hour->paid_hol_hour_flag == "t") {
		//１日分の所定労働時間(分)
		$specified_time_per_day = $obj_hol_hour->get_specified_time($emp_id, $day1_time);
		//当月分
		//$wk_day = $sums[40] / $specified_time_per_day;
		//$sums[7] += $obj_hol_hour->edit_nenkyu_zan($wk_day);
		
		//時間有休合計取得
		$paid_hol_hour_total = $obj_hol_hour->get_paid_hol_hour_total($wk_year, $wk_mon, $emp_id, $timecard_bean->closing_paid_holiday, $timecard_bean->criteria_months, $start_date, $end_date, $specified_time_per_day, 1);
	}
	// 月集計値を表示用に編集
	for ($i = 0; $i <= 9; $i++) {
		$sums[$i] .= "日";
	}
    //当月分の時間有休を有休年休欄に追加出力 20120604
    if ($obj_hol_hour->paid_hol_hour_flag == "t") {
        if ($sums[40] > 0) {
            $wk_hour = $sums[40] / 60;
            $sums[7] .= "(".$wk_hour."時間)";
        }
    }
    for ($i = 10; $i <= 11; $i++) {
		$sums[$i] .= "回";
	}
	for ($i = 12; $i <= 15; $i++) {
		$sums[$i] = ($sums[$i] == 0) ? "0:00" : minute_to_hmm($sums[$i]);
	}
	for ($i = 16; $i <= 27; $i++) {
		$sums[$i] .= "日";
	}
	for ($i = 28; $i <= 31; $i++) {
		$sums[$i] = ($sums[$i] == 0) ? "0:00" : minute_to_hmm($sums[$i]);
	}
	//年末年始追加 20090908
	$sums[33] .= "日";
	//支給換算日数追加 20090908
	//$sums[34] = $paid_day_count;
	//年休残追加 20100625
	$sums[35] = $timecard_common_class->get_nenkyu_zan($wk_year, $wk_mon, $emp_id, $timecard_bean->closing_paid_holiday, $start_date, $end_date);//."日";
	if ($obj_hol_hour->paid_hol_hour_flag == "t") {
        //時間有休がある場合の年休残文字列取得 20120604
        $ret_str = $obj_hol_hour->get_nenkyu_zan_str($wk_year, $wk_mon, $emp_id, $timecard_bean->closing_paid_holiday, $timecard_bean->criteria_months, $start_date, $end_date, $specified_time_per_day, $sums[35], $duty_form, "", $timecard_common_class);
        $sums[35] = $ret_str;
        
    }
    else {
        $sums[35] .= "日";
    }	
	
	//法定追加 20110121
	$sums[36] .= "日";
	//所定追加 20110121
	$sums[37] .= "日";
	/*
	// 締めデータがあるが追加事由分がない場合、追加事由分を設定
	if ($atdbk_closed) {
		//追加事由分がない場合
		if ($sums[31] != "") {
			for ($i = 16; $i <= 27; $i++) {
				$tmp_sums[$i] = $sums[$i];
			}
		}
		//法定内勤務がない場合
		if ($sums[28] != "") {
			for ($i = 28; $i <= 31; $i++) {
				$tmp_sums[$i] = $sums[$i];
			}
		}
	}
*/
	return $sums;
}

// 締めデータを出力
function show_closed_attendance_book($con, $emp_id, $start_date, $end_date, $view, $fname, $show_time_flg, $atdbk_common_class, $meeting_display_flag, $timecard_bean, $no_overtime) {

	// 集計結果格納用配列を初期化
	$sums = array_fill(0, 32, 0);
/*
	// 勤務日・休日の名称を取得
	$sql = "select day2, day3, day4, day5 from calendarname";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$day2 = pg_fetch_result($sel, 0, "day2");
		$day3 = pg_fetch_result($sel, 0, "day3");
		$day4 = pg_fetch_result($sel, 0, "day4");
		$day5 = pg_fetch_result($sel, 0, "day5");
	}
	if ($day2 == "") {
		$day2 = "法定休日";
	}
	if ($day3 == "") {
		$day3 = "所定休日";
	}
	if ($day4 == "") {
		$day4 = "勤務日1";
	}
	if ($day5 == "") {
		$day5 = "勤務日2";
	}
*/
	$yyyymm = substr($start_date, 0, 6);

// ************ oose add start 2008/02/01 *****************
	// 手当情報取得
	$arr_allowance = get_timecard_allowance($con, $fname);
// ************ oose add end 2008/02/01 *****************
// データをまとめて取得
	$arr_wktotald = array();
	$sql = "select * from wktotald";
	$cond = "where emp_id = '$emp_id' and yyyymm = '$yyyymm' and ";
	$cond .= " date >= '$start_date' and date <= '$end_date'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$date = $row["date"];
		$arr_wktotald[$date]["type"] = $row["type"];
		$arr_wktotald[$date]["tmcd_group_id"] = $row["tmcd_group_id"];
		$arr_wktotald[$date]["pattern"] = $row["pattern"];
		$arr_wktotald[$date]["reason"] = $row["reason"];
		$arr_wktotald[$date]["start_time"] = $row["start_time"];
		$arr_wktotald[$date]["end_time"] = $row["end_time"];
		$arr_wktotald[$date]["over_time"] = $row["over_time"];
		$arr_wktotald[$date]["night_time"] = $row["night_time"];
		$arr_wktotald[$date]["goout_time1"] = $row["goout_time1"];
		$arr_wktotald[$date]["goout_time2"] = $row["goout_time2"];
		$arr_wktotald[$date]["return_count"] = $row["return_count"];
		$arr_wktotald[$date]["return_time"] = $row["return_time"];
		$arr_wktotald[$date]["late_flg"] = $row["late_flg"];
		$arr_wktotald[$date]["early_flg"] = $row["early_flg"];
		$arr_wktotald[$date]["allow_id"] = $row["allow_id"];
		$arr_wktotald[$date]["night_duty"] = $row["night_duty"];
		$arr_wktotald[$date]["workday_count"] = $row["workday_count"];
		$arr_wktotald[$date]["meeting_time"] = $row["meeting_time"];
		$arr_wktotald[$date]["start_time"] = $row["start_time"];
		$arr_wktotald[$date]["previous_day_flag"] = $row["previous_day_flag"];
		$arr_wktotald[$date]["next_day_flag"] = $row["next_day_flag"];
	}

	$arr_wktotalp = array();
	if ($view == "2") {
		$sql = "select * from wktotald";
		$cond = "where emp_id = '$emp_id' and yyyymm = '$yyyymm' and ";
		$cond .= " date >= '$start_date' and date <= '$end_date'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		while ($row = pg_fetch_array($sel)) {
			$date = $row["date"];
			$arr_wktotalp[$date]["tmcd_group_id"] = $row["tmcd_group_id"];
			$arr_wktotalp[$date]["pattern"] = $row["pattern"];
			$arr_wktotalp[$date]["reason"] = $row["reason"];
			$arr_wktotalp[$date]["night_duty"] = $row["night_duty"];
			$arr_wktotalp[$date]["allow_id"] = $row["allow_id"];
			$arr_wktotalp[$date]["prov_start_time"] = $row["prov_start_time"];
			$arr_wktotalp[$date]["prov_end_time"] = $row["prov_end_time"];
		}
	}

	// 全日付をループ
	$tmp_date = $start_date;
	while ($tmp_date <= $end_date) {
		$tmp_year = intval(substr($tmp_date, 0, 4));
		$tmp_month = intval(substr($tmp_date, 4, 2));
		$tmp_day = intval(substr($tmp_date, 6, 2));

		// 処理日付の勤務実績を取得
/*
		$sql = "select * from wktotald";
		$cond = "where emp_id = '$emp_id' and yyyymm = '$yyyymm' and date = '$tmp_date'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$type = pg_fetch_result($sel, 0, "type");
		$tmcd_group_id = pg_fetch_result($sel, 0, "tmcd_group_id");
		$pattern = pg_fetch_result($sel, 0, "pattern");
		$reason = pg_fetch_result($sel, 0, "reason");
		$start_time = pg_fetch_result($sel, 0, "start_time");
		$end_time = pg_fetch_result($sel, 0, "end_time");
		$over_time = pg_fetch_result($sel, 0, "over_time");
		$night_time = pg_fetch_result($sel, 0, "night_time");
		$goout_time1 = pg_fetch_result($sel, 0, "goout_time1");
		$goout_time2 = pg_fetch_result($sel, 0, "goout_time2");
		$return_count = pg_fetch_result($sel, 0, "return_count");
		$return_time = pg_fetch_result($sel, 0, "return_time");
		$start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $start_time);
		$end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $end_time);
		$over_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_time);
		$night_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $night_time);
		$goout_time1 = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $goout_time1);
		$goout_time2 = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $goout_time2);
		$late_flg = (pg_fetch_result($sel, 0, "late_flg") == "t");
		$early_flg = (pg_fetch_result($sel, 0, "early_flg") == "t");
// ************ oose add start 2008/02/01 *****************
		$allow_id = pg_fetch_result($sel, 0, "allow_id");	//手当
// ************ oose add end 2008/02/01 *****************
        $night_duty = pg_fetch_result($sel, 0, "night_duty");
        $workday_count = pg_fetch_result($sel, 0, "workday_count");
        $meeting_time = pg_fetch_result($sel, 0, "meeting_time");

        $wk_start_time = pg_fetch_result($sel, 0, "start_time");
        $wk_end_time = pg_fetch_result($sel, 0, "end_time");

        $previous_day_flag = pg_fetch_result($sel, 0, "previous_day_flag");
        $next_day_flag = pg_fetch_result($sel, 0, "next_day_flag");
*/
		$type = $arr_wktotald[$tmp_date]["type"];
		$tmcd_group_id = $arr_wktotald[$tmp_date]["tmcd_group_id"];
		$pattern = $arr_wktotald[$tmp_date]["pattern"];
		$reason = $arr_wktotald[$tmp_date]["reason"];
		$start_time = $arr_wktotald[$tmp_date]["start_time"];
		$end_time = $arr_wktotald[$tmp_date]["end_time"];
		$over_time = $arr_wktotald[$tmp_date]["over_time"];
		$night_time = $arr_wktotald[$tmp_date]["night_time"];
		$goout_time1 = $arr_wktotald[$tmp_date]["goout_time1"];
		$goout_time2 = $arr_wktotald[$tmp_date]["goout_time2"];
		$return_count = $arr_wktotald[$tmp_date]["return_count"];
		$return_time = $arr_wktotald[$tmp_date]["return_time"];
		if ($start_time != "") {
			$start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $start_time);
		}
		if ($end_time != "") {
			$end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $end_time);
		}
		if ($over_time != "") {
			$over_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_time);
		}
		if ($night_time != "") {
			$night_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $night_time);
		}
		if ($goout_time1 != "") {
			$goout_time1 = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $goout_time1);
		}
		if ($goout_time2 != "") {
			$goout_time2 = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $goout_time2);
		}

		$late_flg = ($arr_wktotald[$tmp_date]["late_flg"] == "t");
		$early_flg = ($arr_wktotald[$tmp_date]["early_flg"] == "t");
		$allow_id = $arr_wktotald[$tmp_date]["allow_id"];
        $night_duty = $arr_wktotald[$tmp_date]["night_duty"];
        $workday_count = $arr_wktotald[$tmp_date]["workday_count"];
        $meeting_time = $arr_wktotald[$tmp_date]["meeting_time"];

        $wk_start_time = $arr_wktotald[$tmp_date]["wk_start_time"];
		$previous_day_flag = $arr_wktotald[$tmp_date]["previous_day_flag"];
		$next_day_flag = $arr_wktotald[$tmp_date]["next_day_flag"];

		// カレンダー名を取得
		$type_name = $atdbk_common_class->get_calendarname($type, $tmp_date);
/*
		// 表示値の編集
		switch ($type) {
		case "1":  // 通常勤務日
			$type_name = "通常勤務日";
			break;
		case "2":  // 勤務日1
			$type_name = $day4;
			break;
		case "3":  // 勤務日2
			$type_name = $day5;
			break;
		case "4":  // 法定休日
			$type_name = $day2;
			break;
		case "5":  // 所定休日
			$type_name = $day3;
			break;
		default:
			$type_name = "";
			break;
		}
*/
		$time_cell_class = "tm";
		if ($show_time_flg != "t") {
			$time_cell_class = "mark";
			if ($start_time != "") {
				$start_time = ($late_flg) ? "遅刻" : "○";
			} else {
				$start_time = "";
			}
			if ($end_time != "") {
				$end_time = ($early_flg) ? "早退" : "○";
			} else {
				$end_time = "";
			}
		}

		//出・退勤時刻文字サイズ
		$time_font = "\"j12\"";
		if ($timecard_bean->time_big_font_flag == "t" && $show_time_flg == "t"){
			$time_font = "\"y16\"";
		}

		// 表示処理
		$rowspan = ($view == "1") ? "1" : "2";
		echo("<tr bgcolor=\"$bgcolor\">\n");
		echo("<td rowspan=\"$rowspan\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo("{$tmp_month}月{$tmp_day}日");
		echo("</font></td>\n");
		echo("<td rowspan=\"$rowspan\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . get_weekday(to_timestamp($tmp_date)) . "</font></td>\n");

		if ($timecard_bean->time_move_flag == "t" && $view != "2"){
			if ($timecard_bean->zen_display_flag == "t"){

				// 前日になる場合があるフラグ
				$previous_day_value = "";
				if ($previous_day_flag == 1){
					$previous_day_value = "前";
				}
				echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($previous_day_value) . "$hspacer</font></td>\n");
			}

			echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($start_time)) . "</font></td>\n");

			if ($timecard_bean->yoku_display_flag == "t"){
				// 翌
				$next_day = "";
				if ($show_next_day_flag == 1){	//残業時刻日またがり対応 20100114
					$next_day = "翌";
				}
				echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($next_day) . "$hspacer</font></td>\n");
			}

			echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($end_time)) . "</font></td>\n");
			//勤務
			echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($kinmu_time)) . "$hspacer</font></td>\n"); //$work_time + $wk_return_timeから変更 20100910
			echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($over_time) . "</font></td>\n");
		}

		if ($timecard_bean->time_move_flag != "t"){
			//種別
			if ($timecard_bean->type_display_flag == "t"){
				echo("<td rowspan=\"$rowspan\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($type_name) . "</font></td>\n");
			}
		}

		// 予実績表示の場合
		if ($view == "2") {

			// 処理日付の勤務予定情報を取得
// ************ oose update start 2008/02/05 *****************
//			$sql = "select pattern, reason, prov_start_time, prov_end_time from wktotalp";
/*
			$sql = "select * from wktotalp";
// ************ oose update end 2008/02/05 *****************
			$cond = "where emp_id = '$emp_id' and yyyymm = '$yyyymm' and date = '$tmp_date'";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type='text/javascript' src='js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
			$prov_tmcd_group_id = pg_fetch_result($sel, 0, "tmcd_group_id");
			$prov_pattern = pg_fetch_result($sel, 0, "pattern");
			$prov_reason = pg_fetch_result($sel, 0, "reason");
            $prov_night_duty = pg_fetch_result($sel, 0, "night_duty");
// ************ oose add start 2008/02/05 *****************
			$prov_allow_id = pg_fetch_result($sel, 0, "allow_id");	//手当
// ************ oose add end 2008/02/05 *****************
			$prov_start_time = pg_fetch_result($sel, 0, "prov_start_time");
			$prov_end_time = pg_fetch_result($sel, 0, "prov_end_time");

			$prov_start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $prov_start_time);
			$prov_end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $prov_end_time);
*/
			$prov_tmcd_group_id = $arr_wktotalp[$tmp_date]["tmcd_group_id"];
			$prov_pattern = $arr_wktotalp[$tmp_date]["pattern"];
			$prov_reason = $arr_wktotalp[$tmp_date]["reason"];
            $prov_night_duty = $arr_wktotalp[$tmp_date]["night_duty"];
			$prov_allow_id = $arr_wktotalp[$tmp_date]["allow_id"];

			$prov_start_time = $arr_wktotalp[$tmp_date]["prov_start_time"];
			$prov_end_time = $arr_wktotalp[$tmp_date]["prov_end_time"];

			if ($prov_start_time != "") {
				$prov_start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $prov_start_time);
			}
			if ($prov_end_time != "") {
				$prov_end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $prov_end_time);
			}
			// 表示値の編集
			$prov_tmcd_group_name = $atdbk_common_class->get_group_name($prov_tmcd_group_id);
			$prov_pattern = $atdbk_common_class->get_pattern_name($prov_tmcd_group_id, $prov_pattern);
			$prov_reason = $atdbk_common_class->get_reason_name($prov_reason);

            if($prov_night_duty == "1")
            {
                $prov_night_duty = "有り";
            }
            else if($prov_night_duty == "2")
            {
                $prov_night_duty = "無し";
            }

// ************ oose add start 2008/02/05 *****************
			$prov_allow_contents = "";
			foreach($arr_allowance as $allowance)
			{
				if($prov_allow_id == $allowance["allow_id"])
				{
					$prov_allow_contents = $allowance["allow_contents"];
					break;
				}
			}
// ************ oose add end 2008/02/05 *****************

			if($timecard_bean->time_move_flag == "t"){
				if ($timecard_bean->zen_display_flag == "t"){
					echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
				}
				echo("<td align=\"right\" bgcolor=\"$bgcolor2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($prov_start_time)) . "</font></td>\n");
				if ($timecard_bean->yoku_display_flag == "t"){
					echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
				}
				echo("<td align=\"right\" bgcolor=\"$bgcolor2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($prov_end_time)) . "</font></td>\n");
				echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
			}

			if ($timecard_bean->time_move_flag == "t"){
				//種別
				if ($timecard_bean->type_display_flag == "t"){
					echo("<td rowspan=\"$rowspan\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($type_name) . "</font></td>\n");
				}
			}

			if ($timecard_bean->group_display_flag == "t"){
				echo("<td height=\"22\" bgcolor=\"$bgcolor2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_tmcd_group_name) . "</font></td>\n");
			}
			echo("<td height=\"22\" bgcolor=\"$bgcolor2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_pattern) . "</font></td>\n");
			echo("<td bgcolor=\"$bgcolor2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_reason) . "</font></td>\n");
			echo("<td bgcolor=\"$bgcolor2\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_night_duty) . "</font></td>\n");
// ************ oose add start 2008/02/05 *****************
			echo("<td bgcolor=\"$bgcolor2\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_allow_contents) . "</font></td>\n");
// ************ oose add end 2008/02/05 *****************
			if($timecard_bean->time_move_flag == "f"){
				if ($timecard_bean->zen_display_flag == "t"){
					echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
				}
				echo("<td align=\"right\" bgcolor=\"$bgcolor2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($prov_start_time)) . "</font></td>\n");
				if ($timecard_bean->yoku_display_flag == "t"){
					echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
				}
				echo("<td align=\"right\" bgcolor=\"$bgcolor2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($prov_end_time)) . "</font></td>\n");
				echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
			}
			echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
			echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
			echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
			if ($timecard_bean->ret_display_flag == "t"){
				echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
			}
			echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
			if ($meeting_display_flag){
				echo("<td bgcolor=\"$bgcolor2\">&nbsp;</td>\n");
			}
			echo("</tr>\n");

			echo("<tr bgcolor=\"$bgcolor\">\n");
		}

		if ($timecard_bean->time_move_flag == "t" && $view == "2"){
			if ($timecard_bean->zen_display_flag == "t"){

				// 前日になる場合があるフラグ
				$previous_day_value = "";
				if ($previous_day_flag == 1){
					$previous_day_value = "前";
				}
				echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($previous_day_value) . "$hspacer</font></td>\n");
			}

			echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($start_time)) . "</font></td>\n");

			if ($timecard_bean->yoku_display_flag == "t"){
				// 翌
				$next_day = "";
				if ($show_next_day_flag == 1){	//残業時刻日またがり対応 20100114
					$next_day = "翌";
				}
				echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($next_day) . "$hspacer</font></td>\n");
			}

			echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($end_time)) . "</font></td>\n");
			//勤務
			echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($kinmu_time)) . "$hspacer</font></td>\n"); //$work_time + $wk_return_timeから変更 20100910
			echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($over_time) . "</font></td>\n");
		}

		if ($timecard_bean->time_move_flag == "t" && $view != "2"){
			//種別
			if ($timecard_bean->type_display_flag == "t"){
				echo("<td rowspan=\"$rowspan\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($type_name) . "</font></td>\n");
			}
		}

		//グループ
		if ($timecard_bean->group_display_flag == "t"){
			echo("<td height=\"22\" class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			echo(nbsp($atdbk_common_class->get_group_name($tmcd_group_id)));
			echo("$hspacer</font></td>\n");
		}

		echo("<td height=\"22\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo(nbsp($atdbk_common_class->get_pattern_name($tmcd_group_id, $pattern)));
		echo("</font></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo(nbsp($atdbk_common_class->get_reason_name($reason)));
		echo("</font></td>\n");


        $tmp_night_duty = "";
        if($night_duty == "1")
        {
            $tmp_night_duty = "有り";
        }
        else if($night_duty == "2")
        {
            $tmp_night_duty = "無し";
        }
	    echo("<td class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo(nbsp($tmp_night_duty));
	    echo("</font></td>\n");


// ************ oose add start 2008/02/01 *****************
		//手当表示
		echo("<td class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		$allow_contents = "";
		foreach($arr_allowance as $allowance)
		{
			if($allow_id == $allowance["allow_id"])
			{
				$allow_contents = $allowance["allow_contents"];
				break;
			}
		}
		echo(nbsp($allow_contents));
		echo("</font></td>\n");
// ************ oose add end 2008/02/01 *****************

		if ($timecard_bean->time_move_flag == "f" || empty($timecard_bean->time_move_flag)){

			if ($timecard_bean->zen_display_flag == "t"){

				// 前日になる場合があるフラグ
				$previous_day_value = "";
				if ($previous_day_flag == 1){
					$previous_day_value = "前";
				}
				echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($previous_day_value) . "$hspacer</font></td>\n");
			}

			echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($start_time)) . "</font></td>\n");

			if ($timecard_bean->yoku_display_flag == "t"){
				// 翌
				$next_day = "";
				if ($next_day_flag == 1){
					$next_day = "翌";
				}
				echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($next_day) . "$hspacer</font></td>\n");
			}

			echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($end_time)) . "</font></td>\n");
			//勤務
			echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($kinmu_time)) . "$hspacer</font></td>\n"); //$work_time + $wk_return_timeから変更 20100910
			echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($over_time) . "</font></td>\n");
		}
		echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($night_time) . "</font></td>\n");
		echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($goout_time1) . "</font></td>\n");
		echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($goout_time2) . "</font></td>\n");

		if ($timecard_bean->ret_display_flag == "t"){
			//回数
//			echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($return_count) . "</font></td>\n");
			//時間
			echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($return_time) . "</font></td>\n");
		}
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . $workday_count . "<br></font></td>\n");

		if ($meeting_display_flag){
			$meeting_time_hh = "";
			$meeting_time_mm = "";
			$meeting_time_hhmm = "";
			if ($meeting_time != null){
				$meeting_time_hh = (int)substr($meeting_time, 0, 2);
				$meeting_time_mm = (int)substr($meeting_time, 2, 4);

				$meeting_time_hhmm = $meeting_time_hh;
				if ($meeting_time_mm == 15)
				{
					$meeting_time_hhmm .= ".25";
				}
                else if($meeting_time_mm == 30)
                {
					$meeting_time_hhmm .= ".5";
                }
                else if($meeting_time_mm == 45)
                {
					$meeting_time_hhmm .= ".75";
                }
			}

			echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($meeting_time_hhmm) . "</font></td>\n");
		}

		echo("</tr>\n");

		$tmp_date = next_date($tmp_date);
	}

	// 月集計値の取得
	$sql = "select days1, days2, days3, days4, days5, days6, days7, days8, days9, days10, count1, count2, time3, time4, time5, time6, days11, days12, days13, days14, days15, days16, days17, days18, days19, days20, days21, days22, time7, time8, time9, time10 from wktotalm";
	$cond = "where emp_id = '$emp_id' and yyyymm = '$yyyymm'";

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$sums[0] = pg_fetch_result($sel, 0, "days1");
	$sums[1] = pg_fetch_result($sel, 0, "days2");
	$sums[2] = pg_fetch_result($sel, 0, "days3");
	$sums[3] = pg_fetch_result($sel, 0, "days4");
	$sums[4] = pg_fetch_result($sel, 0, "days5");
	$sums[5] = pg_fetch_result($sel, 0, "days6");
	$sums[6] = pg_fetch_result($sel, 0, "days7");
	$sums[7] = pg_fetch_result($sel, 0, "days8");
	$sums[8] = pg_fetch_result($sel, 0, "days9");
	$sums[9] = pg_fetch_result($sel, 0, "days10");
	$sums[10] = pg_fetch_result($sel, 0, "count1");
	$sums[11] = pg_fetch_result($sel, 0, "count2");
	$sums[12] = pg_fetch_result($sel, 0, "time3");
	$sums[13] = pg_fetch_result($sel, 0, "time4");
	$sums[14] = pg_fetch_result($sel, 0, "time5");
	$sums[15] = pg_fetch_result($sel, 0, "time6");
	$sums[16] = pg_fetch_result($sel, 0, "days11");
	$sums[17] = pg_fetch_result($sel, 0, "days12");
	$sums[18] = pg_fetch_result($sel, 0, "days13");
	$sums[19] = pg_fetch_result($sel, 0, "days14");
	$sums[20] = pg_fetch_result($sel, 0, "days15");
	$sums[21] = pg_fetch_result($sel, 0, "days16");
	$sums[22] = pg_fetch_result($sel, 0, "days17");
	$sums[23] = pg_fetch_result($sel, 0, "days18");
	$sums[24] = pg_fetch_result($sel, 0, "days19");
	$sums[25] = pg_fetch_result($sel, 0, "days20");
	$sums[26] = pg_fetch_result($sel, 0, "days21");
	$sums[27] = pg_fetch_result($sel, 0, "days22");
	$sums[28] = pg_fetch_result($sel, 0, "time7");
	$sums[29] = pg_fetch_result($sel, 0, "time8");
	$sums[30] = pg_fetch_result($sel, 0, "time9");
	$sums[31] = pg_fetch_result($sel, 0, "time10");
	$sums[32] = pg_fetch_result($sel, 0, "days11"); // 締めデータの有無判定に使用

	// 月集計値を表示用に編集
	for ($i = 0; $i <= 9; $i++) {
		$sums[$i] = str_replace(".0", "", $sums[$i]) . "日";
	}
	for ($i = 10; $i <= 11; $i++) {
		$sums[$i] .= "回";
	}
	for ($i = 16; $i <= 27; $i++) {
		$sums[$i] .= "日";
	}

	return $sums;

}
?>
