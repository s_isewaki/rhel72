<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="message_register.php" method="post">
<input type="hidden" name="back" value="t">
<input type="hidden" name="message_date1" value="<? echo($message_date1); ?>">
<input type="hidden" name="message_date2" value="<? echo($message_date2); ?>">
<input type="hidden" name="message_date3" value="<? echo($message_date3); ?>">
<input type="hidden" name="message_date4" value="<? echo($message_date4); ?>">
<input type="hidden" name="message_date5" value="<? echo($message_date5); ?>">
<input type="hidden" name="message_company" value="<? echo($message_company); ?>">
<input type="hidden" name="message_name" value="<? echo($message_name); ?>">
<input type="hidden" name="message_tel1" value="<? echo($message_tel1); ?>">
<input type="hidden" name="message_tel2" value="<? echo($message_tel2); ?>">
<input type="hidden" name="message_tel3" value="<? echo($message_tel3); ?>">
<input type="hidden" name="message_service" value="<? echo($message_service); ?>">
<input type="hidden" name="message" value="<? echo($message); ?>">
<input type="hidden" name="message_towhich_device" value="<? echo($message_towhich_device); ?>">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="hid_message_towhich_name" value="<? echo($hid_message_towhich_name); ?>">
<input type="hidden" name="message_towhich_id" value="<? echo($message_towhich_id); ?>">
<input type="hidden" name="message_towhich_email" value="<? echo($message_towhich_email); ?>">
<input type="hidden" name="message_towhich_email_mobile" value="<? echo($message_towhich_email_mobile); ?>">
</form>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_validation.php");
require_once("Cmx.php");
require_once("aclg_set.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 伝言メモ権限のチェック
$checkauth = check_authority($session, 4, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// 入力チェック
if (!checkdate($message_date2, $message_date3, $message_date1)) {
	echo("<script type=\"text/javascript\">alert('日付が不正です。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if ($message_towhich_id == "") {
	echo("<script type=\"text/javascript\">alert('宛先を選択してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
$message_company = trim($message_company);
if (strlen($message_company) > 50) {
	echo("<script type=\"text/javascript\">alert('相手先所属名が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
$message_name = trim($message_name);
if (strlen($message_name) > 50) {
	echo("<script type=\"text/javascript\">alert('相手先氏名が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
$message = trim($message);
if (!is_valid_eucjpwin($message)) {
	echo("<script type=\"text/javascript\">alert('用件に不正な文字が含まれています。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (strlen($message) > 500) {
	echo("<script type=\"text/javascript\">alert('用件が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

// ログインユーザの職員ID・氏名・メールアドレスを取得
$sql = "select emp_id, emp_lt_nm, emp_ft_nm, emp_email2 from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
$emp_lt_nm = pg_fetch_result($sel, 0, "emp_lt_nm");
$emp_ft_nm = pg_fetch_result($sel, 0, "emp_ft_nm");
$emp_email2 = pg_fetch_result($sel, 0, "emp_email2");
$emp_nm = "$emp_lt_nm $emp_ft_nm";

// メール送信オプションが選択された場合
if ($message_towhich_device == "1" || $message_towhich_device == "2") {

	// ログインユーザのメールアドレスが未設定の場合はエラー
	if ($emp_email2 == "") {
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('あなたのメールアドレスが登録されていないため、メールを送信できません。\\n管理者に連絡してください。');</script>");
		echo("<script type=\"text/javascript\">document.items.submit();</script>");
		exit;
	}

	// 宛先のメールアドレスが未設定の場合はエラー
	if (($message_towhich_device == "1" && $message_towhich_email == "") || ($message_towhich_device == "2" && $message_towhich_email_mobile == "")) {
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('宛先のメールアドレスが登録されていないため、メールを送信できません。');</script>");
		echo("<script type=\"text/javascript\">document.items.submit();</script>");
		exit;
	}

	// 送信内容を編集
	$customer = trim("$message_company $message_name");
	if ($customer != "") {$customer .= " 様";}
	if ($message_tel1 != "" && $message_tel2 != "" && $message_tel3 != "") {
		$tel = "{$message_tel1}-{$message_tel2}-{$message_tel3}";
	} else {
		$tel = "";
	}
	mb_internal_encoding("EUC-JP");
	mb_language("Japanese");
	$content = "$emp_nm さんからの伝言メモです。\n\n";
	$content .= "[相手先] $customer\n";
	$content .= "[用　件] ";
	switch ($message_service) {
	case "1":
		$content .= "お電話がありました";
		break;
	case "2":
		$content .= "訪問されました";
		break;
	case "3":
		$content .= "ファックスがありました";
		break;
	case "4":
		$content .= "折り返しお電話ください";
		break;
	case "5":
		$content .= "その他";
		break;
	}
	$content .= "\n\n";
	if ($message != "") {
		$content .= "また、以下の伝言がありました。\n";
		$content .= "----------------------------------------\n";
		$content .= "$message\n";
	}
	$content .= "----------------------------------------\n\n";
	$content .= "[日　時] $message_date1/$message_date2/$message_date3 $message_date4:$message_date5\n";
	$content .= "[連絡先] $tel";

	// ヘッダの設定
	$additional_headers = "From: $emp_email2";

	// メールを送信
	switch ($message_towhich_device) {
	case "1":
		$to = $message_towhich_email;
		break;
	case "2":
		$to = $message_towhich_email_mobile;
		break;
	}
	$result = mb_send_mail($to, "CoMedix伝言メモ", $content, $additional_headers);
	if (!$result) {
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('メール送信に失敗しました。');</script>");
		echo("<script type=\"text/javascript\">document.items.submit();</script>");
		exit;
	}
}

// 伝言メモIDを採番
$sql = "select max(message_id) from message";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$message_id = pg_fetch_result($sel, 0, 0);
if ($message_id == 0) {
	$message_id = 1;
}else{
	$message_id++;
}

// 伝言メモ情報を作成
$sql = "insert into message (emp_id, message_id, date, message_date, message_towhich_id, message_towhich_device, message_company, message_name, message_tel1, message_tel2, message_tel3, message_service, message, msg_received_flg, msg_receiver_flg, msg_sender_flg) values (";
$content = array($emp_id, $message_id, date("YmdHi"), "$message_date1$message_date2$message_date3$message_date4$message_date5", $message_towhich_id, $message_towhich_device, $message_company, $message_name, $message_tel1, $message_tel2, $message_tel3, $message_service, pg_escape_string($message), "f", "0", "0");
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
$con = pg_close($con);

// 送信完了メッセージを表示、画面をリフレッシュ
echo("<script type=\"text/javascript\">alert('送信しました。');</script>");
echo("<script type=\"text/javascript\">location.href = 'message_left.php?session=$session';</script>");
?>
</body>
