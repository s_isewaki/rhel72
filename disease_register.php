<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<? require("./about_session.php"); ?>
<? require("./about_authority.php"); ?>
<? require("./about_postgres.php"); ?>
<? require("./show_select_values.ini"); ?>
<? require("./conf/sql.inf"); ?>
<? require("label_by_profile_type.ini"); ?>
<?
//ページ名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$disreg = check_authority($session, 36, $fname);
if($empif == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//--------------------------------------------------------------------

//DBへのコネクション作成
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$profile_type = get_profile_type($con, $fname);

$cond = "where ptif_id = '$pt_id' and ptif_del_flg = 'f' order by ptif_id";
$sel = select_from_table($con,$SQL85,$cond,$fname);		//ptifmstから取得

	if($sel == 0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

$lt_kn_nm = pg_result($sel,0,"ptif_lt_kana_nm");
$ft_kn_nm = pg_result($sel,0,"ptif_ft_kana_nm");
$lt_kj_nm = pg_result($sel,0,"ptif_lt_kaj_nm");
$ft_kj_nm = pg_result($sel,0,"ptif_ft_kaj_nm");
$birth    = pg_result($sel,0,"ptif_birth");

$yr = substr($birth,0,4);
$mn = substr($birth,4,2);
$dy = substr($birth,6,2);
$birth = "$yr"."年"."$mn"."月"."$dy"."日";

$sex      = pg_result($sel,0,"ptif_sex");
if($sex == 1){
	$sex = "男性";
}else{
	$sex = "女性";
}

//--------------------------------------------------------------------

// 事業所一覧の取得
$cond = "where enti_del_flg = 'f' order by enti_id";
$sel_enti = select_from_table($con, $SQL91, $cond, $fname);
if ($sel_enti == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$arr_enti = array();
$arr_entisect = array();

// 全事業所をループ
while ($row = pg_fetch_array($sel_enti)) {
	$enti_id = $row["enti_id"];
	$enti_nm = $row["enti_nm"];
	array_push($arr_enti, array("enti_id" => $enti_id, "enti_nm" => $enti_nm));

	// 診療科一覧の取得
	$cond = "where enti_id = '$enti_id' and sect_del_flg = 'f' order by sect_id";
	$sel_sect = select_from_table($con, $SQL109, $cond, $fname);
	if ($sel_sect == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	$arr_sect = array();

	// 全診療科をループ
	while ($row = pg_fetch_array($sel_sect)) {
		$sect_id = $row["sect_id"];
		$sect_nm = $row["sect_nm"];
		array_push($arr_sect, array("sect_id" => $sect_id, "sect_nm" => $sect_nm));
	}

	array_push($arr_entisect, array("enti_id" => $enti_id, "sects" => $arr_sect));
}

/*
print_r($arr_enti);
print_r($arr_entisect);
*/

// 初期表示時は診断名データを取得
if ($back != "t") {
	$sql = "select dishist_disease, dishist_icd_cd, sect_id, dishist_result, dishist_strt_dt, dishist_end_dt, dishist_insurance from dishist";
	$cond = "where ptif_id = '$pt_id' and dishist_emp_id = '$r_emp' and dishist_reg_dt = '$r_date' and dishist_reg_tm = '$r_time'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$disease = pg_fetch_result($sel, 0, "dishist_disease");
	$icd_cd = pg_fetch_result($sel, 0, "dishist_icd_cd");
	$sect = pg_fetch_result($sel, 0, "sect_id");
	$result = pg_fetch_result($sel, 0, "dishist_result");
	$s_ymd = pg_fetch_result($sel, 0, "dishist_strt_dt");
	$e_ymd = pg_fetch_result($sel, 0, "dishist_end_dt");
	$insurance = pg_fetch_result($sel, 0, "dishist_insurance");

	$sy = substr($s_ymd, 0, 4);
	$sm = substr($s_ymd, 4, 2);
	$sd = substr($s_ymd, 6, 2);
	$ey = substr($e_ymd, 0, 4);
	$em = substr($e_ymd, 4, 2);
	$ed = substr($e_ymd, 6, 2);
}

//--------------------------------------------------------------------

// 検索フォームからの引継ぎ値をurlencode
$ekey1 = urlencode($key1);
$ekey2 = urlencode($key2);

if ($sy == "") {
	$sy = date("Y");
}
if ($sm == "") {
	$sm = date("m");
}
if ($sd == "") {
	$sd = date("d");
}
if ($ey == "") {
	$ey = $sy;
}
if ($em == "") {
	$em = $sm;
}
if ($ed == "") {
	$ed = $sd;
}

?>
<title>CoMedix <? echo($_label_by_profile["PATIENT_MANAGE"][$profile_type]); ?> | 診断名更新</title>
<script language="javascript">
function initPage() {

<? if ($enti != "") { ?>
//	document.dis.enti.value = '<? echo $enti; ?>';
<? } ?>

	entiOnChange('<? echo $sect; ?>');
}

function entiOnChange(sect) {

//	var enti_id = document.dis.enti.value;
	var enti_id = '1';

	// 診療科セレクトボックスのオプションを全削除
	deleteAllOptions(document.dis.sect);

	// 診療科セレクトボックスのオプションを作成
<? foreach ($arr_entisect as $entisect) { ?>
	if (enti_id == '<? echo $entisect["enti_id"]; ?>') {
	<? foreach($entisect["sects"] as $sectitem) { ?>
		addOption(document.dis.sect, '<? echo $sectitem["sect_id"]; ?>', '<? echo $sectitem["sect_nm"]; ?>', sect);
	<? } ?>
	}
<? } ?>

	// オプションが1件も作成されなかった場合
	if (document.dis.sect.options.length == 0) {
		addOption(document.dis.sect, '0', '（未登録）', sect);
	}

}

function deleteAllOptions(box) {

	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}

}

function addOption(box, value, text, selected) {

	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';

}
</script>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5"onload="initPage();">
<center>
<form name="dis" action="disease_insert.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>診断名更新</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="20%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["PATIENT_ID"][$profile_type]); ?></font></td>
<td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($pt_id); ?></font></td>
<td width="20%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["PATIENT_NAME"][$profile_type]); ?></font></td>
<td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo("$lt_kj_nm $ft_kj_nm"); ?></font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病名</font></td>
<td colspan="3"><input type="text" name="disease" size="60" value="<? echo $disease; ?>" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ICD10</font></td>
<td colspan="3"><input type="text" name="icd_cd" size="20" value="<? echo $icd_cd; ?>" style="ime-mode:inactive;"></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">診療科</font></td>
<td colspan="3"><select name="sect"></select></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">開始日</font></td>
<td colspan="3"><select name="sy"><? show_select_years(5,$sy); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="sm"><? show_select_months($sm); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="sd"><? show_select_days($sd) ?></select></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">転帰日</font></td>
<td><select name="ey"><? show_select_years(5,$ey); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="em"><? show_select_months($em); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="ed"><? show_select_days($ed) ?></select></td>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">転帰</font></td>
<td><select name="result"><option value="1"<? if ($result == "1") { echo " selected"; } ?>>治癒</option><option value="2"<? if ($result == "2") { echo " selected"; } ?>>中止</option><option value="3"<? if ($result == "3") { echo " selected"; } ?>>継続</option><option value="4"<? if ($result == "4") { echo " selected"; } ?>>死亡</option></select></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">保険</font></td>
<td colspan="3"><input type="text" name="insurance" size="40" value="<? echo $insurance; ?>" style="ime-mode:active;"></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
<input type="hidden" name="r_emp" value="<? echo($r_emp); ?>">
<input type="hidden" name="r_date" value="<? echo($r_date); ?>">
<input type="hidden" name="r_time" value="<? echo($r_time); ?>">
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
