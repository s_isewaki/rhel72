<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
if ($wherefrom == "") {
	$wherefrom = "5";
}

//勤務シフト作成より呼び出し 20130218
if ($wherefrom == "10") {$wherefrom = "7";}

$doc_title = ($wherefrom >= "7") ? "勤務シフト作成" : "勤務管理";
?>
<title>CoMedix <? echo($doc_title); ?> | タイムカード修正A4横</title>
<?
require_once("about_comedix.php");
require_once("show_attendance_pattern.ini");
require_once("show_timecard_common.ini");
require_once("referer_common.ini");
require_once("holiday.php");
require_once("work_admin_menu_common.ini");
require_once("atdbk_common_class.php");
require_once("timecard_common_class.php");
require_once("atdbk_duty_shift_common_class.php");
require_once("date_utils.php");
require_once("atdbk_workflow_common_class.php");
require_once("timecard_bean.php");
require_once("work_admin_timecard_common.php");
require_once("atdbk_info_common.php");
require_once("timecard_paid_hol_hour_class.php");
require_once("calendar_name_class.php");
require_once("atdbk_close_class.php");


$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 勤務管理権限チェック
switch ($wherefrom) {
    case "7":  // 勤務シフト作成の実績入力画面より
    case "8":
        $checkauth = check_authority($session, 69, $fname);
        if ($checkauth == "0") {
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
            exit;
        }
        break;
    default:
        // 勤務管理権限チェック
		$checkauth = check_authority($session, 42, $fname);
		if ($checkauth == "0") {
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showLoginPage(window);</script>");
            exit;
        }
        break;
}

// リンク出力用変数の設定
$url_srch_name = urlencode($srch_name);

// 表示モードのデフォルトは「実績表示」
if ($view == "") {
	$view = "1";
}

// データベースに接続
$con = connect2db($fname);

//前、次の職員対応 20110124
$err_flg = "";
if ($emp_move_flg != "") {
	//勤務シフト作成からの場合
	if ($wherefrom >= "7") {
		$arr_staff_id = split(",", $staff_ids);
		$staff_idx = "";
		$data_cnt = count($arr_staff_id);
		for ($i=0; $i<$data_cnt; $i++) {
			if ($emp_id == $arr_staff_id[$i]) {
				$staff_idx = $i;
				break;
			}
		}
		if (($emp_move_flg == "prev" && $staff_idx == 0) || //先頭で前の職員
			($emp_move_flg == "next" && $staff_idx == $data_cnt - 1)) { //最後で次の職員
			$err_flg = "1";
		} else {
			$move_idx = ($emp_move_flg == "next") ? $staff_idx + 1 : $staff_idx - 1;
			$move_emp_id = $arr_staff_id[$move_idx];
		}
	} else {
		$move_emp_id = get_move_emp_id($con, $fname, $emp_move_flg, $emp_personal_id, $srch_name, $cls, $atrb, $dept, $duty_form_jokin, $duty_form_hijokin, $group_id, $shift_group_id, $csv_layout_id, $srch_id, $sus_flg);
	}
	if ($move_emp_id == "") {
		$err_flg = "1";
	} else {
		$emp_id = $move_emp_id;
	}
}

//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);
//javascript出力
$atdbk_common_class->setJavascript();

//ワークフロー関連共通クラス
$atdbk_workflow_common_class = new atdbk_workflow_common_class($con, $fname);

///時間有休
$obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
$obj_hol_hour->select();
//カレンダー名、所定労働時間履歴用
$calendar_name_class = new calendar_name_class($con, $fname);

// 遷移元の取得
$referer = get_referer($con, $session, "work", $fname);

//会議研修病棟外の列を常に表示とする 20120319
$meeting_display_flag = true;

// ログインユーザの出勤グループを取得
$user_tmcd_group_id = get_timecard_group_id($con, $emp_id, $fname);

// ************ oose add start *****************
// 勤務条件テーブルより勤務形態（常勤、非常勤）、残業管理を取得
$sql = "select duty_form, no_overtime from empcond";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$duty_form = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "duty_form") : "";
$no_overtime = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "no_overtime") : "";
// ************ oose add end *****************

//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);

$atdbk_close_class = new atdbk_close_class($con, $fname);
$closing = $atdbk_close_class->get_empcond_closing($emp_id);
$closing_month_flg = $atdbk_close_class->closing_month_flg;

// 締め日が登録済みの場合
if ($closing != "") {

	if ($yyyymm != "") {
		$year = substr($yyyymm, 0, 4);
		$month = intval(substr($yyyymm, 4, 2));
		$month_set_flg = true; //月が指定された場合
	} else {
		$year = date("Y");
		$month = date("n");
		$yyyymm = $year . date("m");
		$month_set_flg = false; //月が未指定の場合
	}

	// 開始日・締め日を算出
	$calced = false;
	switch ($closing) {
		case "1":  // 1日
			$closing_day = 1;
			break;
		case "2":  // 5日
			$closing_day = 5;
			break;
		case "3":  // 10日
			$closing_day = 10;
			break;
		case "4":  // 15日
			$closing_day = 15;
			break;
		case "5":  // 20日
			$closing_day = 20;
			break;
		case "6":  // 末日
			$start_year = $year;
			$start_month = $month;
			$start_day = 1;
			$end_year = $start_year;
			$end_month = $start_month;
			$end_day = days_in_month($end_year, $end_month);
			$calced = true;
			break;
	}
	if (!$calced) {
		$day = date("j");
		$start_day = $closing_day + 1;
		$end_day = $closing_day;

		//月が指定された場合
		if ($month_set_flg) {
			//当月〜翌月
			if ($closing_month_flg != "2") {
				$start_year = $year;
				$start_month = $month;

				$end_year = $year;
				$end_month = $month + 1;
				if ($end_month == 13) {
					$end_year++;
					$end_month = 1;
				}
			}
			//前月〜当月
			else {
				$start_year = $year;
				$start_month = $month - 1;
				if ($start_month == 0) {
					$start_year--;
					$start_month = 12;
				}

				$end_year = $year;
				$end_month = $month;
			}

		} else {
			if ($day <= $closing_day) {
				$start_year = $year;
				$start_month = $month - 1;
				if ($start_month == 0) {
					$start_year--;
					$start_month = 12;
				}

				$end_year = $year;
				$end_month = $month;
			} else {
				$start_year = $year;
				$start_month = $month;

				$end_year = $year;
				$end_month = $month + 1;
				if ($end_month == 13) {
					$end_year++;
					$end_month = 1;
				}
			}
		}
	}
	$start_date = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
	$end_date = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);

	//月度対応
	//月が未指定の場合
	if (!$month_set_flg) {
		if ($closing_month_flg != "2") {
			$year = $start_year;
			$month = $start_month;
		} else {
			$year = $end_year;
			$month = $end_month;
		}
		$yyyymm = sprintf("%04d%02d", $year, $month);
	}
	//前月、翌月リンク用
	$last_month_y = $year;
	$last_month_m = $month - 1;
	if ($last_month_m == 0) {
		$last_month_y--;
		$last_month_m = 12;
	}
	$last_yyyymm = $last_month_y . sprintf("%02d", $last_month_m);

	$next_month_y = $year;
	$next_month_m = $month + 1;
	if ($next_month_m == 13) {
		$next_month_y++;
		$next_month_m = 1;
	}
	$next_yyyymm = $next_month_y . sprintf("%02d", $next_month_m);

	// 職員氏名を取得
	$sql = "select emp_lt_nm, emp_ft_nm, emp_personal_id from empmst";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
	$emp_personal_id = pg_fetch_result($sel, 0, "emp_personal_id");

	// 変則労働適用情報を配列で取得
	$arr_irrg_info = get_irrg_info_array($con, $emp_id, $fname);

	// 実績表示ボタン名
	//$view_button = ($view == "1") ? "予実績表示" : "実績表示";

	// 対象年月の締め状況を取得
	//$tmp_yyyymm = substr($start_date, 0, 6);
	//月度対応 20100415
	$tmp_yyyymm = $year.sprintf("%02d", $month);
	$sql = "select count(*) from atdbkclose";
	$cond = "where emp_id = '$emp_id' and yyyymm = '$tmp_yyyymm'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$atdbk_closed = (pg_fetch_result($sel, 0, 0) > 0);

	// 締め済みの場合、締めデータから表示対象期間を取得
	if ($atdbk_closed) {
		$sql = "select min(date), max(date) from wktotald";
		$cond = "where emp_id = '$emp_id' and yyyymm = '$tmp_yyyymm'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$start_date = pg_fetch_result($sel, 0, 0);
		$end_date = pg_fetch_result($sel, 0, 1);

		$start_year = intval(substr($start_date, 0, 4));
		$start_month = intval(substr($start_date, 4, 2));
		$start_day = intval(substr($start_date, 6, 2));
		$end_year = intval(substr($end_date, 0, 4));
		$end_month = intval(substr($end_date, 4, 2));
		$end_day = intval(substr($end_date, 6, 2));
	}

	// 残業申請画面を開く時間を取得
	$sql = "select ovtmscr_tm from timecard";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$ovtmscr_tm = pg_fetch_result($sel, 0, "ovtmscr_tm");
	} else {
		$ovtmscr_tm = 0;
	}

	// 給与支給区分・祝日計算方法を取得
	$sql = "select wage, hol_minus from empcond";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$wage = pg_fetch_result($sel, 0, "wage");
		$hol_minus = pg_fetch_result($sel, 0, "hol_minus");
	} else {
		$wage = "";
		$hol_minus = "";
	}
}
//タイムカード集計項目出力設定からデータ取得 20091104
$arr_total_flg = get_timecard_total_flg($con, $fname); //show_timecard_common.ini
$arr_total_id = get_timecard_total_id($con, $fname);; //出力項目のid

//タイムカードデータ取得
$arr_legal_hol_cnt = array();
$arr_legal_hol_cnt_part = array();
$arr_holwk = get_timecard_holwk_day($con, $fname);
$legal_hol_cnt_flg = false;

$arr_atdbk_info = get_atdbk_info($con, $fname, $session, $emp_id, $yyyymm, $atdbk_common_class, $start_date, $end_date, $atdbk_workflow_common_class, $timecard_bean, $meeting_display_flag, $arr_legal_hol_cnt, $arr_legal_hol_cnt_part, $arr_holwk, $legal_hol_cnt_flg, 0, $obj_hol_hour, "", "", "", "1", $calendar_name_class);


?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
<!--
function changeView() {
	document.timecard.action = 'work_admin_timecard.php';
	document.timecard.submit();
}

function editTimecard() {
	document.timecard.action = 'atdbk_timecard_insert.php';
	document.timecard.submit();
}

function openPrintPage() {
	window.open('atdbk_timecard_print.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&yyyymm=<? echo("$yyyymm"); ?>&view=<? echo($view); ?>&wherefrom=2', 'newwin', 'width=840,height=700,scrollbars=yes');
}

function downloadCSV(type) {
	document.csv.action = 'atdbk_timecard_csv' + type + '.php';
	document.csv.submit();
}

//一括修正画面
function openTimecardAll() {
	base_left = 0;
	base = 0;
	wx = window.screen.availWidth;
	wy = window.screen.availHeight;
<?
$wk_wherefrom = ($wherefrom >= "7") ? "7" : "2";
	?>
	url = 'work_admin_timecard_all.php?session=<?=$session?>&emp_id=<? echo($emp_id); ?>&yyyymm=<? echo($yyyymm); ?>&view=<? echo($view); ?>&wherefrom=<? echo($wk_wherefrom); ?>&srch_name=<? echo($url_srch_name); ?>&cls=<? echo($cls); ?>&atrb=<? echo($atrb); ?>&dept=<? echo($dept); ?>&page=<? echo($page); ?>&group_id=<? echo($group_id); ?>&shift_group_id=<? echo($shift_group_id); ?>&csv_layout_id=<? echo($csv_layout_id); ?>&check_flg=<? echo($check_flg); ?>&staff_ids=<? echo($staff_ids); ?>&duty_form_jokin=<? echo($duty_form_jokin); ?>&duty_form_hijokin=<? echo($duty_form_hijokin); ?>&srch_id=<? echo($srch_id); ?>&sus_flg=<? echo($sus_flg); ?>';
	window.open(url, 'TimecardAll', 'left='+base_left+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
}
var arr_atdptn_reason = new Array();
<?
//事由情報を出力
$arr_group_pattern_reason = $atdbk_common_class->get_pattern_reason_array("");
foreach ($arr_group_pattern_reason as $wk_tmcd_group_id => $arr_pattern_reason) {
	foreach ($arr_pattern_reason as $wk_atdptn_id => $reason) {
		if ($reason != "") {
			echo("arr_atdptn_reason['{$wk_tmcd_group_id}_{$wk_atdptn_id}'] = '$reason';\n");
		}
	}
}
?>
function setReason(pos, tmcd_group_id, atdptn_id) {

	if (atdptn_id == '--') {
		document.timecard["reason[]"][pos].value = '--';
		return;
	}
	wk_group_id = document.timecard["list_tmcd_group_id[]"][pos].value;
	wk_id = wk_group_id+'_'+atdptn_id;
	if (arr_atdptn_reason[wk_id] == undefined) {
		document.timecard["reason[]"][pos].value = '--';
		return;
	}
	document.timecard["reason[]"][pos].value = arr_atdptn_reason[wk_id];
}

//前の職員、次の職員
function emp_move(move_flg) {
	document.timecard.emp_move_flg.value = move_flg;
	view = (document.timecard.view.value == '1') ? '2' : '1';
	document.timecard.view.value = view;
<?
if ($wherefrom >= "7") {
	//勤務シフトからの場合
?>
	//職員情報が未設定の場合に呼出元画面から取得
	if (document.timecard.staff_ids.value == '') {
		if(window.opener && !window.opener.closed && window.opener.document.mainform.data_cnt) {
			data_cnt = window.opener.document.mainform.data_cnt.value;
			staff_ids = '';
			for (i=0; i<data_cnt; i++) {
				wk1 = 'staff_id[' + i + ']';
				staff_id = window.opener.document.mainform.elements[wk1].value;
				if (i > 0) {
					staff_ids += ',';
				}
				staff_ids += staff_id;
			}

			document.timecard.staff_ids.value = staff_ids;
			document.timecard.data_cnt.value = data_cnt;
		} else {
			alert('職員情報を取得できません。もう一度開き直してください。');
			return false;
		}
	}
	<?
}
?>
	document.timecard.submit();

}
//チェック
function inputcheck() {
	document.timecard.check_flg.value = '1';
	view = (document.timecard.view.value == '1') ? '2' : '1';
	document.timecard.view.value = view;
	document.timecard.submit();

}

//印刷（pdf）
function print_pdf() {

	window.open("",
		"pdf", "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=1000,height=721");

	document.csv.action = 'work_admin_pdf.php';
	document.csv.target = 'pdf';
	document.csv.submit();

}
//タイムカード修正画面
function openTimecardEdit(tmp_date) {

	window.open('atdbk_timecard_edit.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date='+tmp_date+'&yyyymm=<? echo($yyyymm); ?>&view=<? echo($view); ?>&wherefrom=8&group_id=<? echo($group_id); ?>&staff_ids=<? echo($staff_ids); ?>', 'newwin', 'width=640,height=620,scrollbars=yes');
}
//勤務シフト実績の画面をリフレッシュする
function reflesh_duty(refresh_flg){
	if (refresh_flg){
		opener.location.href = 'duty_shift_results.php?session=<? echo($session); ?>&group_id=<? echo($group_id); ?>&duty_yyyy=<? echo(substr($yyyymm,0,4)); ?>&duty_mm=<? echo(substr($yyyymm,4)); ?>';
	}
}
//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<?
$wk_color_tm1 = "#b8d175";
//枠用
// tl: top left ┏
// tc: top center ┯┯
// tr: top right ┓
// ml: middle left  ┃｜
// mc: middle center ｜｜
// mr: middle right ｜┃
// bl: bottom left ┗
// bc: bottom center ┷┷
// br: bottom right ┛
// 1:出勤、退勤時刻
?>
<style type="text/css">
td.lbl {padding-right:1em;}
td.txt {padding-right:0.5em;}
td.tm {width:2.8em;text-align:right;}
td.tl1 {border-style:solid; border-top-color:<? echo($wk_color_tm1); ?>; border-right-color:<? echo($wk_color_tm1); ?>; border-left-color:<? echo($wk_color_tm1); ?>; border-width:3px 2px 1px 3px;}
td.tc1 {border-style:solid; border-top-color:<? echo($wk_color_tm1); ?>; border-right-color:<? echo($wk_color_tm1); ?>; border-left-color:<? echo($wk_color_tm1); ?>; border-width:3px 2px 1px 1px;}
td.tr1 {border-style:solid; border-top-color:<? echo($wk_color_tm1); ?>; border-right-color:<? echo($wk_color_tm1); ?>; border-left-color:<? echo($wk_color_tm1); ?>; border-width:3px 3px 1px 1px;}
td.ml1 {border-style:solid; border-right-color:<? echo($wk_color_tm1); ?>; border-left-color:<? echo($wk_color_tm1); ?>; border-width:1px 2px 1px 3px;}
td.mc1 {border-style:solid; border-right-color:<? echo($wk_color_tm1); ?>; border-left-color:<? echo($wk_color_tm1); ?>; border-width:1px 2px 1px 1px;}
td.mr1 {border-style:solid; border-right-color:<? echo($wk_color_tm1); ?>; border-left-color:<? echo($wk_color_tm1); ?>; border-width:1px 3px 1px 1px;}
td.bl1 {border-style:solid; border-right-color:<? echo($wk_color_tm1); ?>; border-bottom-color:<? echo($wk_color_tm1); ?>; border-left-color:<? echo($wk_color_tm1); ?>; border-width:1px 2px 3px 3px;}
td.bc1 {border-style:solid; border-right-color:<? echo($wk_color_tm1); ?>; border-bottom-color:<? echo($wk_color_tm1); ?>; border-left-color:<? echo($wk_color_tm1); ?>; border-width:1px 2px 3px 1px;}
td.br1 {border-style:solid; border-right-color:<? echo($wk_color_tm1); ?>; border-bottom-color:<? echo($wk_color_tm1); ?>; border-left-color:<? echo($wk_color_tm1); ?>; border-width:1px 3px 3px 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onUnload="reflesh_duty(<? echo($refresh_flg) ?>)">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? /* 実績入力画面からの場合 20120116 */
 if ($wherefrom >= "7") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279a5">
<td width="100%" height="28" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>タイムカード修正</b></font></td>
<td width="28" bgcolor="#5279a5" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>

<? } else { ?>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<? echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>"><b>勤務管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<!-- atdbk_timecard.phpをatdbk_timecard_shift.phpに変更 20130129 -->
<td width="32" height="32" class="spacing"><a href="atdbk_timecard_shift.php?session=<? echo($session); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="atdbk_timecard_shift.php?session=<? echo($session); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="atdbk_timecard_shift.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>

<?
// メニュータブ
$option = array(
		"emp_id" => $emp_id,
		"srch_name" => $url_srch_name,
		"cls" => $cls,
		"atrb" => $atrb,
		"dept" => $dept,
		"page" => $page,
		"group_id" => $group_id,
		"shift_group_id" => $shift_group_id,
		"csv_layout_id" => $csv_layout_id,
		"duty_form_jokin" => $duty_form_jokin,
        "duty_form_hijokin" => $duty_form_hijokin,
        "srch_id" => $srch_id,
        "sus_flg" => $sus_flg
		);
show_work_admin_menuitem($session, $fname, $option);
?>
<? } ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? if ($closing == "") {  // 締め日未登録の場合 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイムカード締め日が登録されていません。出退勤マスターメンテナンスの締め日画面で登録してください。</font></td>
</tr>
</table>
<? } else if ($wage == "") {  // 給与支給区分未登録の場合 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">給与支給区分が登録されていません。職員登録の勤務条件画面で登録してください。</font></td>
</tr>
</table>
<? } else { ?>
<form name="timecard" method="post">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<!-- メディアテック Start -->
<td height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
	<?
		//
		$wherefrom = ($wherefrom >= "7") ? "7" : $wherefrom;

		//勤務シフトに出勤簿を追加 20130129
		if($wherefrom =="7")
		{
        $url = "work_admin_timecard.php?session=$session&emp_id=$emp_id&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&group_id=$group_id&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&yyyymm=$yyyymm&wherefrom=$wherefrom&staff_ids=$staff_ids&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&sus_flg=$sus_flg";
        $url_shift = "work_admin_timecard_shift.php?session=$session&emp_id=$emp_id&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&group_id=$group_id&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&yyyymm=$yyyymm&wherefrom=$wherefrom&staff_ids=$staff_ids&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&sus_flg=$sus_flg";
		}
	 ?>

	<?
	if ($wherefrom != "7") {
?>
	<a href="javascript:void(0);" onclick="openTimecardAll();">
	<? }
	else {
        $url_all = "work_admin_timecard_all.php?session=$session&emp_id=$emp_id&yyyymm=$yyyymm&view=$view&wherefrom=$wherefrom&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&group_id=$group_id&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&check_flg=$check_flg&staff_ids=$staff_ids&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&sus_flg=$sus_flg";
	?>
		<a href="<? echo($url_shift); ?>">出勤簿</a>&nbsp;
		<a href="<? echo($url); ?>">日別修正</a>&nbsp;
		<b>日別修正（A4横）</b>&nbsp;
		<a href="<? echo($url_all); ?>">
	<? } ?>
	一括修正</a>&nbsp;
<?php

    // opt以下のphpを表示するかどうか(申請用追加機能有無判定)
    $opt_display_flag = file_exists("opt/flag");
    if ( phpversion() >= "5.1"  && $opt_display_flag){
        require_once './kintai/common/master_util.php';
		if(MasterUtil::get_kintai_ptn() != 0){
            $url = "kintai_ovtmlist_adm.php?session=$session&emp_id=$emp_id&yyyymm=$yyyymm&view=$view&wherefrom=$wherefrom&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&group_id=$group_id&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&check_flg=&staff_ids=$staff_ids&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&sus_flg=$sus_flg";
            echo "<a href='".$url."'>出勤簿（超勤区分）</a>";
            $url = "kintai_ovtmlist_detail_adm.php?session=$session&emp_id=$emp_id&yyyymm=$yyyymm&view=$view&wherefrom=$wherefrom&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&group_id=$group_id&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&check_flg=$check_flg&staff_ids=$staff_ids&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&sus_flg=$sus_flg";
            echo "&nbsp;&nbsp;<a href='".$url."'>超勤区分内訳</a>";
		}
	}

?>
<!-- メディアテック End -->
</font></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
    <td height="22" width="560"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象期間&nbsp;&nbsp;<a href="work_admin_timecard_a4.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&yyyymm=<? echo($last_yyyymm); ?>&view=<? echo($view); ?>&srch_name=<? echo($url_srch_name); ?>&cls=<? echo($cls); ?>&atrb=<? echo($atrb); ?>&dept=<? echo($dept); ?>&page=<? echo($page); ?>&group_id=<? echo($group_id); ?>&shift_group_id=<? echo($shift_group_id); ?>&csv_layout_id=<? echo($csv_layout_id); ?>&duty_form_jokin=<? echo($duty_form_jokin); ?>&duty_form_hijokin=<? echo($duty_form_hijokin); ?>&wherefrom=<? echo($wherefrom); ?>&staff_ids=<? echo($staff_ids); ?>&srch_id=<? echo($srch_id); ?>&sus_flg=<? echo($sus_flg); ?>">&lt;前月</a>&nbsp;<?
    $str = show_yyyymm_menu($year, $month);
    echo $str;
?>
&nbsp;<a href="work_admin_timecard_a4.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&yyyymm=<? echo($next_yyyymm); ?>&view=<? echo($view); ?>&srch_name=<? echo($url_srch_name); ?>&cls=<? echo($cls); ?>&atrb=<? echo($atrb); ?>&dept=<? echo($dept); ?>&page=<? echo($page); ?>&group_id=<? echo($group_id); ?>&shift_group_id=<? echo($shift_group_id); ?>&csv_layout_id=<? echo($csv_layout_id); ?>&duty_form_jokin=<? echo($duty_form_jokin); ?>&duty_form_hijokin=<? echo($duty_form_hijokin); ?>&wherefrom=<? echo($wherefrom); ?>&staff_ids=<? echo($staff_ids); ?>&srch_id=<? echo($srch_id); ?>&sus_flg=<? echo($sus_flg); ?>">翌月&gt;</a></font>
&nbsp;
<input type="button" value="<前の職員" onclick="emp_move('prev');">
<input type="button" value="次の職員>" onclick="emp_move('next');">
&nbsp;
<input type="button" value="チェック" onclick="inputcheck();">

</td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">開始日&nbsp;&nbsp;<? echo("{$start_month}月{$start_day}日"); ?>&nbsp;&nbsp;〜&nbsp;&nbsp;締め日&nbsp;&nbsp;<? echo("{$end_month}月{$end_day}日"); ?></font></td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td height="22" width=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID&nbsp;
	<? echo($emp_personal_id); ?>&nbsp;&nbsp;職員氏名&nbsp;<? echo($emp_name);
	// 勤務条件表示
	$str_empcond = get_employee_empcond2($con, $fname, $emp_id);

	$str_shiftname = get_shiftname($con, $fname, $atdbk_closed, $emp_id, $start_date, $end_date);
	//所属表示
	$str_orgname = get_orgname($con, $fname, $emp_id, $timecard_bean);
	echo("　<b>（".$str_empcond."　".$str_shiftname."）$str_orgname</b>");
?></font></td>
<td align="right" width="">
	<? /*
	<input type="button" value="<? echo($view_button); ?>" onclick="changeView();">&nbsp;
	<input type="button" value="登録" onclick="editTimecard();"<? if ($atdbk_closed) {echo(" disabled");} ?>>&nbsp;
*/ ?>
<input type="button" value="印刷（PDF）" onclick="print_pdf();">&nbsp;
	<? /*
	<input type="button" value="打刻CSV出力" onclick="downloadCSV(1);"<? if ($view == "2") {echo(" disabled");} ?>>&nbsp;<input type="button" value="月集計CSV出力" onclick="downloadCSV(2);"<? if ($view == "2") {echo(" disabled");} ?>>
*/ ?>

</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table width="100%" border="1" cellspacing="0" cellpadding="1">
<tr height="22">
<?
//見出し
	$arr_title = get_a4_title();
	for ($i=0; $i<count($arr_title); $i++) {
		if ($i == 2 || $i == 3 || $i == 20 || $i == 21) {
			$width = "64";
		} else {
			$width = "";
		}
		//枠
		switch($i) {
			case 4:
				$wk_class = "tl1";
				break;
			case 5:
			case 6:
				$wk_class = "tc1";
				break;
			case 7:
				$wk_class = "tr1";
				break;
			default:
				$wk_class = "";
		}
	?>
<td width="<? echo($width); ?>" align="center" rowspan="1" class="<? echo($wk_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_title[$i]); ?></font></td>
		<?
	}
?>
</tr>
<?
	//テキスト位置の配列
	$arr_align = get_a4_align();
	$day_count = $arr_atdbk_info["day_count"];

	//当日
	$today = date("Ymd");

	for ($line=0; $line<$day_count; $line++) {
		//色の設定
		$type = $arr_atdbk_info[$line]["type"];

		$tmp_date = $arr_atdbk_info[$line]["date"];
		$tmcd_group_id = $arr_atdbk_info[$line]["tmcd_group_id"];
		if ($tmcd_group_id == "") {
			$tmcd_group_id = $user_tmcd_group_id;
		}
		$pattern = $arr_atdbk_info[$line]["pattern"];
		$reason = $arr_atdbk_info[$line]["reason"];

		$overtime_link_type = $arr_atdbk_info[$line]["overtime_link_type"];
		$overtime_apply_id = $arr_atdbk_info[$line]["overtime_apply_id"];
		$diff_min_end_time = $arr_atdbk_info[$line]["diff_min_end_time"];
		$return_link_type = $arr_atdbk_info[$line]["return_link_type"];
		$return_apply_id = $arr_atdbk_info[$line]["return_apply_id"];
		$modify_link_type = $arr_atdbk_info[$line]["modify_link_type"];
		$workday_count = $arr_atdbk_info[$line]["hidden_workday_count"];
		$start_time = $arr_atdbk_info[$line]["start_time"];
		$end_time = $arr_atdbk_info[$line]["end_time"];
        $over_start_time = $arr_atdbk_info[$line]["over_start_time"];
        $over_end_time = $arr_atdbk_info[$line]["over_end_time"];
        $o_start_time1 = $arr_atdbk_info[$line]["o_start_time1"];
		$delay_time = $arr_atdbk_info[$line]["delay_time"];
		$early_leave_time = $arr_atdbk_info[$line]["early_leave_time"];
		$after_night_duty_flag = $arr_atdbk_info[$line]["after_night_duty_flag"];
		// チェック対応、ハイライト
		//　残業未申請、残業申請中、退復未申請（呼出し未申請）・申請中、修正申請中
		//　打刻漏れ、遅刻、早退がある行をハイライトする。
		if ($check_flg == "1" && (
					($overtime_link_type == "1" &&
						$timecard_bean->over_apply_disp_flg == "t" && //残業申請状況の表示がするの場合 20110805
						(
							($timecard_bean->over_yet_apply_disp_min != "" && //未申請を表示する設定確認
							$diff_min_end_time != "" &&
							$diff_min_end_time >= $timecard_bean->over_yet_apply_disp_min)  ||
							($timecard_bean->over_yet_apply_disp_min == "" && //未申請を表示する設定の「分」が空白の場合 20110523
							$diff_min_end_time != "")
						) &&
						$no_overtime != "t" //残業管理をする場合
					) ||
					($overtime_link_type == "2" && $no_overtime != "t") ||
					$return_link_type == "1" ||
					$return_link_type == "2" ||
					$modify_link_type == "2" ||
					($tmp_date < $today &&
						($pattern != "10" && $workday_count > 0 &&
							$after_night_duty_flag != "1") && //明けの場合を除く 20110819
						($start_time == "" || $end_time == "") &&
						($o_start_time1 == "") //退復なし
						) || //打刻漏れチェックは当日より前の場合、出勤退勤の確認
					($tmp_date == $today &&
						($pattern != "10" && $workday_count > 0 &&
							$after_night_duty_flag != "1") && //明けの場合を除く 20110819
						($start_time == "") &&
						($o_start_time1 == "") //退復なし
						) || //打刻漏れチェックは当日の場合、出勤の確認
					$delay_time != "" ||
					$early_leave_time != "" ||
					($tmp_date <= $today &&
						($pattern == "") && //勤務パターン未設定
						($start_time == "" || $end_time == ""))
					|| (($pattern == "") && //時刻があるのに勤務パターン未設定 20120301
						($start_time != "" || $end_time != ""))
					|| ($pattern == "10" && //勤務パターンがあり、事由が未設定 20120301
						$reason == "")
                    || ($pattern == "10" && //勤務パターンが休暇で、出勤退勤時刻があるのに、残業時刻が入っていない場合 20120507
                        $start_time != "" && $end_time != "" &&
                        $over_start_time == "" && $over_end_time == "")
                    || ($tmp_date != $today && $start_time != "" && $end_time == "") //当日以外で出勤時刻のみ設定 20120720
                    || ($start_time == "" && $end_time != "") //退勤時刻のみ設定 20120720
                    ) ) {
			$bgcolor = "#ffff66";
		}
		// 背景色設定。法定、祝祭日を赤、所定を青にする
		else {
			$bgcolor = get_timecard_bgcolor($type);
		}
		echo("<tr bgcolor=\"$bgcolor\">\n");

        //$enc_cur_url = urlencode("$fname?session=$session&emp_id=$emp_id&date=$tmp_date&yyyymm=$yyyymm&view=$view&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&wherefrom=$wherefrom&staff_ids=$staff_ids&group_id=$group_id&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id");

		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");

		//日付リンク
		// 締めデータがない場合
		if (!$atdbk_closed) {
			$modify_link_type = $arr_atdbk_info[$line]["modify_link_type"];
			if ($modify_link_type == "0") {
//勤務シフト実績入力からの場合
				if ($wherefrom >= "7") {
					echo("<a href=\"javascript:void(0);\" onclick=\"openTimecardEdit({$tmp_date});\">{$arr_atdbk_info[$line][0]}</a>$hspacer");
				}
				else {
                    echo("<a href=\"javascript:void(0);\" onclick=\"window.open('atdbk_timecard_edit.php?session=$session&emp_id=$emp_id&date=$tmp_date&yyyymm=$yyyymm&view=$view&wherefrom=$wherefrom&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&group_id=$group_id&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&sus_flg=$sus_flg', 'newwin', 'width=640,height=620,scrollbars=yes');\">{$arr_atdbk_info[$line][0]}</a>");
				}
			} else {
				$modify_apply_id = $arr_atdbk_info[$line]["modify_apply_id"];
				//申請なし以外にもタイム修正できるように変更 20130218
				//echo("<a href=\"javascript:void(0);\" onclick=\"window.open('work_admin_modify_detail.php?session=$session&apply_id=$modify_apply_id&wherefrom=$wherefrom&pnt_url=$enc_cur_url', 'newwin', 'width=900,height=700,scrollbars=yes');\">{$arr_atdbk_info[$line][0]}</a>");
                echo("<a href=\"javascript:void(0);\" onclick=\"window.open('atdbk_timecard_edit.php?session=$session&emp_id=$emp_id&date=$tmp_date&yyyymm=$yyyymm&view=$view&wherefrom=$wherefrom&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&group_id=$group_id&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&sus_flg=$sus_flg', 'newwin', 'width=640,height=620,scrollbars=yes');\">{$arr_atdbk_info[$line][0]}</a>");
			}
			switch ($modify_link_type) {
				case "0":  // 申請なし
					$apply_stat_str = "";
					break;
				case "2":  // 申請中

					// 勤務時間修正申請承認者情報取得
					$arr_tmmdaprv = $atdbk_workflow_common_class->get_approve_list($modify_apply_id, "TMMD");

					//本人修正不可の場合、残業申請状況を表示する場合
					if ($timecard_bean->modify_flg == "f" && ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1")) {//20140226
						// 承認がひとつでもあるかチェック
						$apv_1_flg = $atdbk_workflow_common_class->get_apv_flg($arr_tmmdaprv);
						// 一部承認
						if($apv_1_flg)
						{
							$apply_stat_nm = "一部承認";
						}
						else
						{
							$apply_stat_nm = "修正申請中";
						}
					} else {
						//本人修正可の場合、申請中の文言は不要（※修正可の設定を何度も変えた場合正しくないまま表示されてしまうのを防止）
						$apply_stat_nm = "";
					}

					$apply_stat_str = "<br><font size=\"2\" color=\"red\" class=\"j10\">$apply_stat_nm</font>";
					break;
				case "3":  // 承認済
					//本人修正不可の場合、残業申請状況を表示する場合
					if ($timecard_bean->modify_flg == "f" && ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1")) {
						$apply_stat_nm = "修正承認済";
					} else {
						//本人修正可の場合
						$apply_stat_nm = "";
					}
					$apply_stat_str = "<br><font size=\"2\" class=\"j10\">$apply_stat_nm</font>";
					break;
				case "4":  // 否認済
					//本人修正不可の場合、残業申請状況を表示する場合
					if ($timecard_bean->modify_flg == "f" && ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1")) {
						$apply_stat_nm = "修正否認済";
					} else {
						//本人修正可の場合
						$apply_stat_nm = "";
					}
					$apply_stat_str = "<br><font size=\"2\" class=\"j10\">$apply_stat_nm</font>";
					break;
				case "5":  // 差戻済
					//本人修正不可の場合、残業申請状況を表示する場合
					if ($timecard_bean->modify_flg == "f" && ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1")) {
						$apply_stat_nm = "修正差戻済";
					} else {
						//本人修正可の場合
						$apply_stat_nm = "";
					}
					$apply_stat_str = "<br><font size=\"2\" color=\"red\" class=\"j10\">$apply_stat_nm</font>";
					break;
			}
			echo($apply_stat_str);
		} else {

			echo($arr_atdbk_info[$line][0]);
		}
		echo("</font>");
		echo("<input type=\"hidden\" name=\"date[]\" value=\"$tmp_date\"></td>\n");
		//曜日
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo($arr_atdbk_info[$line][1]);
		echo("</font></td>\n");
		//勤務実績
		echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		$selectPatternId = "pattern_".$tmp_date;
//表示のみとするためコメント化
/*
		echo("<input type=\"hidden\" name=\"list_tmcd_group_id[]\" value=\"$tmcd_group_id\"> \n");
		$arr_attendance_pattern = $atdbk_common_class->get_pattern_array($tmcd_group_id);
		echo("<select name=\"pattern[]\" id=\"$selectPatternId\" onChange=\"setReason($line, $tmcd_group_id, this.value);\">");
		echo("<option value=\"--\">");
		foreach ($arr_attendance_pattern as $atdptn_id => $atdptn_val) {
			echo("<option value=\"$atdptn_id\"");
			if ($pattern == $atdptn_id) {
				echo(" selected");
			}
			echo(">$atdptn_val");
		}
		echo("</select>");
*/
		$str = $arr_atdbk_info[$line][2];
		if ($str == "") {
			$str = "<br>";
		}
		echo($str);

		echo("</font></td>\n");

		//事由
		echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		//表示のみとするためコメント化
		//		$atdbk_common_class->set_reason_option("reason[]", $reason);
		$str = $arr_atdbk_info[$line][3];
		if ($str == "") {
			$str = "<br>";
		}
		echo($str);

		echo("</font></td>\n");

		//残業申請状態
		$apply_stat_str = "";
		if ($no_overtime != "t") {
			switch ($overtime_link_type) {
				case "0":  // 申請不要
					$apply_stat_str = "";
					break;
				case "1":  // 未申請
					$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t") ? "残業未申請" : "";
					//未申請を表示しない設定追加 20100809
					if ($timecard_bean->over_yet_apply_disp_min != "") {
						if ($diff_min_end_time != "" && $diff_min_end_time < $timecard_bean->over_yet_apply_disp_min) {
							$apply_stat_nm = "";
						}
					}
					$apply_stat_str = "<br><font size=\"2\" class=\"j10\">$apply_stat_nm</font>";
					break;
				case "2":  // 申請中

					// 残業申請承認者情報取得
					$arr_ovtmaprv = $atdbk_workflow_common_class->get_approve_list($overtime_apply_id, "OVTM");
					// 承認がひとつでもあるかチェック
					$apv_1_flg = $atdbk_workflow_common_class->get_apv_flg($arr_ovtmaprv);
					//残業申請状況表示フラグ
					if ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") {
						// 一部承認
						if ($apv_1_flg) {
							$apply_stat_nm = "一部承認";
						} else {
							$apply_stat_nm = "残業申請中";
						}
					} else {
						$apply_stat_nm = "";
					}

					$apply_stat_str = "<br><font size=\"2\" color=\"red\" class=\"j10\">$apply_stat_nm</font>";
					break;
				case "3":  // 承認済
					$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") ? "残業承認済" : "";
					$apply_stat_str = "<br><font size=\"2\" class=\"j10\">$apply_stat_nm</font>";
					break;
				case "4":  // 否認済
					$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") ? "残業否認済" : "";
					$apply_stat_str = "<br><font size=\"2\" class=\"j10\">$apply_stat_nm</font>";
					break;
				case "5":  // 差戻済
					$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") ? "残業差戻済" : "";
					$apply_stat_str = "<br><font size=\"2\" color=\"red\" class=\"j10\">$apply_stat_nm</font>";
					break;
				case "6":  // 残業申請不要
					$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") ? "残業申請不要" : "";
					$apply_stat_str = "<br><font size=\"2\" class=\"j10\">$apply_stat_nm</font>";
					break;
			}
		}
		//呼出、退復申請状態
		$rtn_stat_str = "";
		if ($timecard_bean->ret_display_flag == "t"){
			$ret1_str = ($timecard_bean->return_icon_flg != "2") ? "復帰" : "呼出";

			switch ($return_link_type) {
				case "0":  // 申請不要
					$rtn_stat_str = "";
					break;
				case "1":  // 未申請
					$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t") ? "{$ret1_str}未申請" : "";
					$rtn_stat_str = "<br><font size=\"2\" class=\"j10\">$apply_stat_nm</font>";
					break;
				case "2":  // 申請中

					// 退勤後復帰申請承認者情報取得
					$arr_rtnaprv = $atdbk_workflow_common_class->get_approve_list($return_apply_id, "RTN");
					// 承認がひとつでもあるかチェック
					$apv_1_flg = $atdbk_workflow_common_class->get_apv_flg($arr_rtnaprv);
					//残業申請状況表示フラグ
					if ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") {
						// 一部承認
						if($apv_1_flg)
						{
							$apply_stat_nm = "一部承認";
						}
						else
						{
							$apply_stat_nm = "{$ret1_str}申請中";
						}
					} else {
						$apply_stat_nm = "";
					}

					$rtn_stat_str = "<br><font size=\"2\" color=\"red\" class=\"j10\">$apply_stat_nm</font>";
					break;
				case "3":  // 承認済
					$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") ? "{$ret1_str}承認済" : "";
					$rtn_stat_str = "<br><font size=\"2\" class=\"j10\">$apply_stat_nm</font>";
					break;
				case "4":  // 否認済
					$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") ? "{$ret1_str}否認済" : "";
					$rtn_stat_str = "<br><font size=\"2\" class=\"j10\">$apply_stat_nm</font>";
					break;
				case "5":  // 差戻済
					$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") ? "{$ret1_str}差戻済" : "";
					$rtn_stat_str = "<br><font size=\"2\" color=\"red\" class=\"j10\">$apply_stat_nm</font>";
					break;
			}
		}
		//前〜会議研修
		for ($i=4; $i<count($arr_title); $i++) {
			//
			switch ($arr_align[$i]) {
				case "C":
					$align = "center";
					break;
				case "L":
					$align = "left";
					break;
				case "R":
					$align = "right";
					break;
			}

			//遅刻早退の色
			$color = ($i == 8 || $i == 9) ? "color=\"red\"" : "";
			//出勤退勤の文字サイズ
			$font_size = "j12";
			if ($i == 5 || $i == 7) {
				if ($timecard_bean->time_big_font_flag == "t"){
					$font_size = "y16";
				}
			}
			//枠
			switch($i) {
				case 4:
					$wk_class = "ml1";
					break;
				case 5:
				case 6:
					$wk_class = "mc1";
					break;
				case 7:
					$wk_class = "mr1";
					break;
				default:
					$wk_class = "";
			}
			echo("<td align=\"$align\" class=\"$wk_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_size\" $color>");
			$str = $arr_atdbk_info[$line][$i];
			//残業理由
			//if ($i == 20) {
			//	$overtime_link_type = $arr_atdbk_info[$line]["overtime_link_type"];
			//申請不要　※申請不要でも表示する 20110830
			//	if ($overtime_link_type == "6") {
			//		$str = "";
			//	}
			//}
			if ($str == "") {
				$str = "&nbsp;";
			}
			//呼出の設定が表示しないの場合
			if ($i == 18 && $timecard_bean->ret_display_flag == "f") {
				$str = "&nbsp;";
			}
			echo($str);
			//残業申請状態
			if ($i == 7) {
				echo($apply_stat_str);
			}
			//呼出、退復申請状態
			if ($i == 18) {
				//呼出の設定が表示しないの場合
				if ($timecard_bean->ret_display_flag == "t") {
					echo($rtn_stat_str);
				} else {
					echo("&nbsp;");
				}
			}
			echo("</font>");
			echo("</td>\n");
		}
		echo("</tr>\n");
	}

	//見出し
	echo("<tr height=\"22\">");

	$arr_title = get_a4_title();
	for ($i=0; $i<count($arr_title); $i++) {
		if ($i == 2 || $i == 3 || $i == 20 || $i == 21) {
			$width = "64";
		} else {
			$width = "";
		}
		//枠
		switch($i) {
			case 4:
				$wk_class = "ml1";
				break;
			case 5:
			case 6:
				$wk_class = "mc1";
				break;
			case 7:
				$wk_class = "mr1";
				break;
			default:
				$wk_class = "";
		}
	?>
<td width="<? echo($width); ?>" align="center" rowspan="1" class="<? echo($wk_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_title[$i]); ?></font></td>
		<?
	}
	echo("</tr>");

	//合計
	$sums = $arr_atdbk_info["sums"];
	$arr_str = array("合計", "", "", "", "", "", "", "",
			$arr_atdbk_info["delay_time_total"], //遅刻
			$arr_atdbk_info["early_leave_time_total"], //早退
			$arr_atdbk_info["out_time_total"], //外出
			$arr_atdbk_info["rest_time_total"], //休憩
			$sums[13], //勤務
			$arr_atdbk_info["paid_time_total"], //有給
			$sums[29], //法定内残業
			$sums[30], //法定外残業
			$sums[15], //深夜残業
			$sums[31], //休日残業
			$arr_atdbk_info["return_time_total"], //呼出（退復）
			$sums[1], //日数
			"", "",
			$arr_atdbk_info["meeting_time_total"] //会議・件数

			);
	echo("<tr>\n");
	for ($i=0; $i<count($arr_title); $i++) {
		$str = $arr_str[$i];
		//テキスト位置
			switch ($arr_align[$i]) {
			case "C":
				$align = "center";
				break;
			case "L":
				$align = "left";
				break;
			case "R":
				$align = "right";
				break;
		}
		//遅刻早退の色
		if (($i == 8 || $i == 9) && $str != "0:00") {
			$color = " color=\"red\"";
		} else {
			$color = "";
		}
		if ($str == "") {
			$str = "<br>";
		}
		//呼出の設定が表示しないの場合
		if ($i == 18 && $timecard_bean->ret_display_flag == "f") {
			$str = "<br>";
		}
		//枠
		switch($i) {
			case 4:
				$wk_class = "bl1";
				break;
			case 5:
			case 6:
				$wk_class = "bc1";
				break;
			case 7:
				$wk_class = "br1";
				break;
			default:
				$wk_class = "";
		}

		echo("<td align=\"$align\" class=\"$wk_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" $color>");
		echo($str);
		echo("</td>");

	}
	echo("</tr>\n");
	/*
	//日、回の設定
	for ($i = 0; $i <= 9; $i++) {
		$sums[$i] .= "日";
	}
	for ($i = 10; $i <= 11; $i++) {
		$sums[$i] .= "回";
	}
	for ($i = 16; $i <= 27; $i++) {
		$sums[$i] .= "日";
	}
	$sums[33] .= "日";
	$sums[35] .= "日";
	$sums[36] .= "日";
	$sums[37] .= "日";
	*/
?>
</table>
<img src="img/spacer.gif" width="1" height="2" alt=""><br>
</td>
</tr>
<tr>
<td align="right">
<table width="95%" border="0" cellspacing="0" cellpadding="1">
<tr height="22">
<td colspan="14">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<b>集計項目</b>
</font>
</td>
</tr>
<?
	$arr_total_info = array();
	//要勤務日数〜早退
	$idx = 0;
	for ($i=0; $i<13; $i++) {
		if ($arr_total_flg[$i]["flg"] == "t") {
			$arr_total_info[$idx]["name"] = $arr_total_flg[$i]["name"];
			if ($i < 8) {
				$wk_str = $sums[$i];
			} elseif ($i== 8) {
				$wk_str = $sums[35]; //年残
			} else {
				$wk_idx = $i - 1;
				$wk_str = $sums[$wk_idx];
			}

			$arr_total_info[$idx]["sum"] = $wk_str;
			$idx++;
		}
	}
	if (in_array("10_0", $arr_total_id)) {
		// 事由ID、表示用名称、表示フラグの配列
		$arr_reason_id = $atdbk_common_class->get_add_disp_reason_id();
		$arr_reason_name = $atdbk_common_class->get_add_disp_reason_name2();
		$arr_disp_flg = array();
		for ($i=0; $i<count($arr_reason_id); $i++) {
			$tmp_display_flag = $atdbk_common_class->get_reason_display_flag($arr_reason_id[$i]);
			$arr_disp_flg[$i] = $tmp_display_flag;
		}
		for ($i=0; $i<count($arr_reason_id); $i++) {
			if ($arr_disp_flg[$i] == 't') {
				$arr_total_info[$idx]["name"] = $arr_reason_name[$i];
				// $sumsのindexを調整、0公休,1法定〜 $atdbk_common_class->get_add_disp_reason_name()参照
				//法定所定追加 20110121
				if ($i == 1) { //法定
					$wk_idx = 36;
				} elseif ($i == 2) { //所定
					$wk_idx = 37;
				} elseif ($i > 2 && $i < 11) {
					$wk_idx = $i + 14; //[17]:特別
				} elseif ($i == 11) { //年末年始
					$wk_idx = 33;
				} elseif ($i > 11) {
					$wk_idx = $i + 13; //[25]:結婚休
				} else {
					$wk_idx = $i + 16; //[16]:公休
				}

				$arr_total_info[$idx]["sum"] = $sums[$wk_idx];
				$idx++;
			}
		}
	}
	for ($i=0; $i<4; $i++) {
		echo("<tr height=\"22\">");
		for ($j=0; $j<7; $j++) {
			echo("<td width=\"105\">");
			echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			//集計項目名
			$idx = $i + $j*4;
			$item_name = $arr_total_info[$idx]["name"];
			$item_name = str_replace("<br>", "", $item_name);
			$item_name = mb_substr($item_name, 0, 7);

			echo($item_name);
			echo("</font>");
			echo("</td>");
			echo("<td width=\"40\">");
			//日数
			$str = $arr_total_info[$idx]["sum"];
			if ($str == "" && $item_name != "") {
				$str = "0";
			}
			if (($item_name == "遅刻" || $item_name == "早退") && $str != "0") {
			$color = " color=\"red\"";
			} else {
				$color = "";
			}
			echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" $color>");
			echo($str);
			echo("</font>");
			echo("</td>\n");
		}
		echo("</tr>\n");
	}
    //支給時間 20140926
    if (in_array("2_90", $arr_total_id)) {
        $paid_t = date_utils::hi_to_minute($sums[13]) + date_utils::hi_to_minute($arr_atdbk_info["paid_time_total"]);
        $paid_t_str = ($paid_t == 0) ? "0:00" : minute_to_hmm($paid_t);
        echo("<tr height=\"22\">");
        echo("<td width=\"105\">");
        echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
        echo("支給時間");
        echo("</font>");
        echo("</td>");
        echo("<td width=\"40\">");
        echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
        echo($paid_t_str);
        echo("</td>");
        echo("</font>");
        for ($i=0; $i<6; $i++) {
            echo("<td width=\"105\">");
            echo("</td>");
            echo("<td width=\"40\">");
            echo("</td>");
        }
        
        echo("</tr>\n");
    }
	// 集計項目表示
	show_shift_summary_from_info($con, $fname, $atdbk_closed, $emp_id, $start_date, $end_date, $arr_total_id, $arr_atdbk_info, $atdbk_common_class);
?>
</table>
</td>
</tr>
<tr>
<td>
<img src="img/spacer.gif" width="1" height="2" alt=""><br>
</td>
</tr>

<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td align="right">
	<? /*
	<input type="button" value="<? echo($view_button); ?>" onclick="changeView();">&nbsp;
	<input type="button" value="登録" onclick="editTimecard();"<? if ($atdbk_closed) {echo(" disabled");} ?>>&nbsp;
*/ ?>
<input type="button" value="印刷（PDF）" onclick="print_pdf();">&nbsp;
	<? /*
	<input type="button" value="打刻CSV出力" onclick="downloadCSV(1);"<? if ($view == "2") {echo(" disabled");} ?>>&nbsp;<input type="button" value="月集計CSV出力" onclick="downloadCSV(2);"<? if ($view == "2") {echo(" disabled");} ?>>
*/ ?>
</td>
</tr>
</table>
</td>
</tr>

</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="yyyymm" value="<? echo($yyyymm); ?>">
<input type="hidden" name="view" value="<? if ($view == "1") {echo("2");} else {echo("1");} ?>">
<?
//勤務シフト実績入力からの場合8
	$wk_wherefrom = ($wherefrom >= "7") ? "8" : $wherefrom;
?>
<input type="hidden" name="wherefrom" value="<? echo($wk_wherefrom); ?>">
<input type="hidden" name="srch_id" value="<? echo($srch_id); ?>">
<input type="hidden" name="srch_name" value="<? echo($srch_name); ?>">
<input type="hidden" name="cls" value="<? echo($cls); ?>">
<input type="hidden" name="atrb" value="<? echo($atrb); ?>">
<input type="hidden" name="dept" value="<? echo($dept); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
<input type="hidden" name="group_id" value="<? echo($group_id); ?>">
<input type="hidden" name="shift_group_id" value="<? echo($shift_group_id); ?>">
<input type="hidden" name="csv_layout_id" value="<? echo($csv_layout_id); ?>">
<input type="hidden" name="duty_form_jokin" value="<? echo($duty_form_jokin); ?>">
<input type="hidden" name="duty_form_hijokin" value="<? echo($duty_form_hijokin); ?>">
<input type="hidden" name="emp_move_flg" value="">
<input type="hidden" name="emp_personal_id" value="<? echo($emp_personal_id); ?>">
<input type="hidden" name="check_flg" value="">
<input type="hidden" name="staff_ids" value="<? echo($staff_ids); ?>">
<input type="hidden" name="data_cnt" value="<? echo($data_cnt); ?>">
<input type="hidden" name="sus_flg" value="<?php echo $sus_flg; ?>">
</form>
<form name="csv" method="get" target="download">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="target_emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="yyyymm" value="<? echo($yyyymm); ?>">
<input type="hidden" name="select_start_yr" value="<? echo(substr($start_date, 0, 4)); ?>">
<input type="hidden" name="select_start_mon" value="<? echo(substr($start_date, 4, 2)); ?>">
<input type="hidden" name="select_start_day" value="<? echo(substr($start_date, 6, 2)); ?>">
<input type="hidden" name="select_end_yr" value="<? echo(substr($end_date, 0, 4)); ?>">
<input type="hidden" name="select_end_mon" value="<? echo(substr($end_date, 4, 2)); ?>">
<input type="hidden" name="select_end_day" value="<? echo(substr($end_date, 6, 2)); ?>">
<input type="hidden" name="default_start_date" value="<? echo($start_date); ?>">
<input type="hidden" name="default_end_date" value="<? echo($end_date); ?>">
<input type="hidden" name="wherefrom" value="3">

</form>
<iframe name="download" width="0" height="0" frameborder="0"></iframe>
<? } ?>
</td>
</tr>
</table>
<?
if ($err_flg == "1") {
	if ($emp_move_flg == "next") {
		$err_msg = "最後の職員です。";
	} else {
		$err_msg = "先頭の職員です。";
	}
	echo("<script type=\"text/javascript\">alert('$err_msg');</script>");
}
?>
</body>
<? pg_close($con); ?>
</html>
