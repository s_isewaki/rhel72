<?/*

画面パラメータは以下を除くshow_rireki_naiyo()の引数仕様に従います。
	$con
	$fname
この画面独自のパラメータ
	$return_url
		戻り先ＵＲＬ。省略時は$_SERVER["HTTP_REFERER"]に戻る。

*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css"/>
<? require("./conf/sql.inf"); ?>
<? require("./about_session.php"); ?>
<? require("./about_authority.php"); ?>
<? require("./show_rireki_naiyo.ini"); ?>
<?
require("label_by_profile_type.ini");
//ページ名
$fname = $PHP_SELF;
//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$summary = check_authority($session, 57, $fname);
if ($summary == "0") {
	showLoginPage();
	exit;
}

//DBコネクション
$con = connect2db($fname);
$cond = " where session_id='$session'";
$sel = select_from_table($con,$SQL1,$cond,$fname);
if($sel==0){
	pg_close($con);
	require_once("summary_common.ini");
	summary_common_show_error_page_and_die();
}

//戻るボタンの遷移先決定
$return_url = urldecode(@$return_url);
if(@$return_url == "") $return_url = @$_SERVER["HTTP_REFERER"];

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
?>

<title>CoMedix <?
// メドレポート/ＰＯレポート(Problem Oriented)
echo ($_label_by_profile["MED_REPORT"][$profile_type]); ?>｜履歴内容</title>
<script type="text/javascript">
	// この子画面の現在の高さを求める
	function getCurrentHeight(){
		if (!window.parent) return 0;
		var h = (document.documentElement.clientHeight || window.innerHeight || document.body.clientHeight);
		h = Math.max(h, document.body.scrollHeight);
		h = Math.max(h, 300);
		return (h*1+50);
	}
	// この子画面の最初の高さを親画面(summary_rireki.php)に通知しておく。onloadで呼ばれる。
	// 一旦サイズを小さくしたのち、正しい値を再通知するようにしないといけない。
	function setDefaultHeight(){
		if (!window.parent) return;
		window.parent.setDefaultHeight("hidari", 300);
		resetIframeHeight();
		var h = getCurrentHeight();
		if (h) window.parent.setDefaultHeight("hidari",h);
	}
	// この子画面がリサイズされたら、高さを再測定して親画面(summary_rireki.php)に通知する。
	// 「サマリ内容ポップアップ」が開くときも呼ばれる。onloadでも呼ばれる。
	// 親画面はこの子画面のIFRAMEサイズを変更する。
	// 一旦大きくしたIFRAMEのサイズは通常小さくできない。
	// よって、getCurrentHeightでサイズ測定する前に、画面を一旦元に戻さないといけない。
	function resizeIframeHeight(){
		resetIframeHeight();
		var h = getCurrentHeight();
		if (h) window.parent.resizeIframeHeight("hidari", h);
	}
	// この子画面の高さを、最初の高さに戻す。「サマリ内容ポップアップ」が閉じるとき呼ばれる。
	// 親画面(summary_rireki.php)は、この子画面IFRAMEの高さを最初の値に戻す。
	function resetIframeHeight(){
		if (!window.parent) return;
		parent.resetIframeHeight("hidari");
	}
</script>
</head>
<body style="background-color:#f4f4f4" onload="setDefaultHeight();resizeIframeHeight();">
<? if ($return_url != "") { ?>
<div class="search_button_div">
	<input type="button" onclick="window.location.href='<?=$return_url?>';" value="戻る"/>
</div>
<? } ?>
<input type="hidden" name="return_url_keep" id="return_url_keep" value="<?=urlencode($return_url)?>">


<?
show_rireki_naiyo(
		$con,
		$session,
		$fname,
		@$mode,
		$pt_id,
		$emp_id,
		@$summary_id,
		@$diag_div,
		@$sect_id,
		@$problem,
		@$kisaisha_id,
		@$shokushu_id,
		@$priority,
		@$date,
		@$from_date,
		@$to_date,
    @$emp_id2,
    @$summary_seq
	);
?>

</body>
</html>
<? @pg_close($con); ?>
