<?
require_once("about_comedix.php");

class fplus_common_class
{

	var $file_name;	// 呼び出し元ファイル名
	var $_db_con;	// DBコネクション

	/**
	 * コンストラクタ
	 * @param object $con DBコネクション
	 * @param string $fname 画面名
	 */
	function fplus_common_class($con, $fname)
	{
		// 呼び出し元ファイル名を保持
		$this->file_name = $fname;
		// DBコネクションを保持
		$this->_db_con = $con;
		// 全項目リストを初期化
	}


//-------------------------------------------------------------------------------------------------------------------------
// フォルダツリー関連
//-------------------------------------------------------------------------------------------------------------------------

	/**
	 * ワークフローカテゴリ/フォルダ情報取得
	 * @return   array  ワークフローカテゴリ/フォルダ情報配列
	 */
	function get_workflow_folder_list(&$wkfw_counts)
	{
		$category_list = $this->get_workflow_category();

		foreach($category_list as $i => $category)
		{
			$wkfw_type = $category["wkfw_type"];
			$category["type"] = "category";
			$category["folders"] = $this->get_workflow_tree("", $wkfw_type);

			$category_list[$i] = $category;
		}

		// フォルダごとのワークフロー数を算出
		$wkfw_counts = $this->calc_workflow_for_count($category_list, "workflow", array(), "");

		return $category_list;
	}


	/**
	 * ワークフローカテゴリ情報取得
	 * @return   array  ワークフローカテゴリ情報配列
	 */
	function get_workflow_category()
	{
		$sql = "select wkfw_type, wkfw_nm from fpluscatemst";
		$cond = "where not wkfwcate_del_flg order by wkfw_type asc";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_type" => $row["wkfw_type"], "wkfw_nm" => $row["wkfw_nm"]);
		}
		return $arr;
	}

	/**
	 * ワークフローフォルダ情報取得
	 * @param    string  $parent_id   親フォルダID
	 * @param    string  $category_id カテゴリID
	 * @return   array  ワークフローフォルダ情報配列
	 */
	function get_workflow_tree($parent_id, $category_id)
	{

		$sql  = "select a.wkfw_folder_id as id, a.wkfw_type as cate, a.wkfw_folder_name as name ";
		$sql .= "from fplusfolder a ";
		$sql .= "left join (select * from fplustree where not wkfw_tree_del_flg) b on a.wkfw_folder_id = b.wkfw_child_id ";
		$sql .= "where not a.wkfw_folder_del_flg ";

		if($parent_id == "")
		{
			$sql .= "and b.wkfw_parent_id is null ";
		}
		else
		{
			$sql .= "and b.wkfw_parent_id = $parent_id ";
		}

		if ($category_id != "") {
			$sql .= " and a.wkfw_type = $category_id ";
		}
		$sql .= "order by a.wkfw_folder_id ";

		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);

		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while ($row = pg_fetch_array($sel))
		{
			$id      = $row["id"];
			$cate    = $row["cate"];
			$name    = $row["name"];

			$arr[] = array(
				"type"      => "folder",
				"cate"      => $cate,
				"parent_id" => $parent_id,
				"id"        => $id,
				"name"      => $name,
				"folders"   => $this->get_workflow_tree($id, $cate)
			);
		}

		return $arr;
	}


	/**
	 * ワークフロー情報取得
	 * @param    string $wkfw_type ワークフロータイプ
	 * @return   array  ワークフロー情報配列
	 */
	function get_wkfw_mst($wkfw_type)
	{
		$sql   = "select * from fpluswkfwmst";
		$cond  = "where wkfw_type = $wkfw_type and not wkfw_del_flg ";
		$cond .= "order by wkfw_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);

		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while ($row = pg_fetch_array($sel))
		{
			$arr[] = array("type" => "file", "wkfw_id" => $row["wkfw_id"], "wkfw_type" => $row["wkfw_type"], "wkfw_title" => $row["wkfw_title"]);
		}
		return $arr;
	}



	// ワークフローフォルダ一覧取得(修正あり)
	function get_cate_workflow_list($category_id, $folder_id, $parent_id)
	{

		if($category_id != "" && $folder_id == "")
		{
			$sql  = "select 1 as num, varchar(6) 'folder' as type, a.wkfw_folder_id as id, a.wkfw_type as cate, a.wkfw_folder_name as name, b.wkfw_parent_id as parent_id ";
			$sql .= "from fplusfolder a ";
			$sql .= "left join (select * from fplustree where not wkfw_tree_del_flg) b on a.wkfw_folder_id = b.wkfw_child_id ";
			$sql .= "where b.wkfw_parent_id is null and a.wkfw_type = $category_id and not a.wkfw_folder_del_flg ";
		}
		else if($category_id != "" && $folder_id != "")
		{
			$sql  = "select 1 as num, varchar(6) 'folder' as type, a.wkfw_folder_id as id, a.wkfw_type as cate, a.wkfw_folder_name as name, b.wkfw_parent_id as parent_id ";
			$sql .= "from fplusfolder a ";
			$sql .= "inner join (SELECT * FROM fplustree WHERE wkfw_parent_id = $folder_id and not wkfw_tree_del_flg) b on a.wkfw_folder_id = b.wkfw_child_id ";
			$sql .= "where not a.wkfw_folder_del_flg ";
		}
		$sql .= "union all ";

		$sql .= "select 2 as num, varchar(6) 'file' as type, a.wkfw_id as id, a.wkfw_type as cate, a.wkfw_title as name, null as parent_id ";
		$sql .= "from fpluswkfwmst a ";
		$sql .= "where a.wkfw_type = $category_id and not a.wkfw_del_flg ";
		if($folder_id == "")
		{
			$sql .= "and a.wkfw_folder_id is null ";
		}
		else if($folder_id != "")
		{
			$sql .= "and a.wkfw_folder_id = $folder_id ";
		}

		$sql .= "order by num, id asc ";

		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);

		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while ($row = pg_fetch_array($sel))
		{
			$type               = $row["type"];
			$tmp_folder_id      = $row["id"];
			$tmp_category_type  = $row["cate"];
			$wkfw_folder_name   = $row["name"];
			$tmp_parent_id      = $row["parent_id"];

			if($type == "folder")
			{
				$arr[] = array(
					"type" => $type,
					"cate" => $tmp_category_type,
					"parent_id" => $tmp_parent_id,
					"id" => $tmp_folder_id,
					"name" => $wkfw_folder_name,
				);
			}
			else if($type == "file")
			{
				$arr[] = array(
					"type" => $type,
					"cate" => $tmp_category_type,
					"parent_id" => null,
					"id" => $tmp_folder_id,
					"name" => $wkfw_folder_name,
				);
			}
		}
		return $arr;
	}

	// フォルダパス取得
	function get_folder_path($folder_id)
	{

		// 当該フォルダ情報を取得
		$sql = "select wkfw_folder_id, wkfw_folder_name from fplusfolder";
		$cond = "where wkfw_folder_id = $folder_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$folder_name = pg_fetch_result($sel, 0, "wkfw_folder_name");
		$arr = array("id" => $folder_id, "name" => $folder_name);

		// 親フォルダ情報を取得
		$sql = "select wkfw_parent_id from fplustree";
		$cond = "where wkfw_child_id = $folder_id and not wkfw_tree_del_flg";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 親フォルダが存在しない場合は自分の情報のみ返す
		if (pg_num_rows($sel) == 0) {
			return array($arr);
		}

		// 親フォルダが存在した場合は先祖の情報も含めて返す
		$parent_id = pg_fetch_result($sel, 0, "wkfw_parent_id");
		$ancestor = $this->get_folder_path($parent_id);
		$ancestor[] = $arr;
		return $ancestor;
	}

	// カテゴリ取得
	function get_wkfwcate_mst($wkfw_type)
	{
		$sql = "select wkfw_type, wkfw_nm, ref_dept_st_flg, ref_dept_flg, ref_st_flg from fpluscatemst";
		$cond = "where not wkfwcate_del_flg and wkfw_type = $wkfw_type";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$wkfw_type       = pg_fetch_result($sel, 0, "wkfw_type");
		$wkfw_nm         = pg_fetch_result($sel, 0, "wkfw_nm");
		$ref_dept_st_flg = pg_fetch_result($sel, 0, "ref_dept_st_flg");
		$ref_dept_flg    = pg_fetch_result($sel, 0, "ref_dept_flg");
		$ref_st_flg      = pg_fetch_result($sel, 0, "ref_st_flg");

		$arr  = array("wkfw_type" => $wkfw_type,
		                "wkfw_nm" => $wkfw_nm,
                        "ref_dept_st_flg" => $ref_dept_st_flg,
                        "ref_dept_flg" => $ref_dept_flg,
                        "ref_st_flg" => $ref_st_flg);
		return $arr;
	}

	// フォルダ取得
	function get_wkfwfolder_mst($wkfw_folder_id)
	{
		$sql = "select wkfw_folder_id, wkfw_folder_name, wkfw_folder_del_flg, ref_dept_st_flg, ref_dept_flg, ref_st_flg from fplusfolder";
		$cond = "where not wkfw_folder_del_flg and wkfw_folder_id = $wkfw_folder_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$foder_id        = pg_fetch_result($sel, 0, "wkfw_folder_id");
		$folder_name     = pg_fetch_result($sel, 0, "wkfw_folder_name");
		$ref_dept_st_flg = pg_fetch_result($sel, 0, "ref_dept_st_flg");
		$ref_dept_flg    = pg_fetch_result($sel, 0, "ref_dept_flg");
		$ref_st_flg      = pg_fetch_result($sel, 0, "ref_st_flg");

		$arr = array("wkfw_folder_id" => $foder_id,
                      "wkfw_folder_name" => $folder_name,
                      "ref_dept_st_flg" => $ref_dept_st_flg,
                      "ref_dept_flg" => $ref_dept_flg,
                      "ref_st_flg" => $ref_st_flg);

		return $arr;
	}


	// ワークフロー取得(カウント用)
	function get_workflow_for_count($srceen_mode, $arr_emp_info, $emp_id)
	{
		$sql = "select wkfw_type, wkfw_folder_id from fpluswkfwmst";
		$cond = "where not wkfw_del_flg";

		if($srceen_mode == "apply")
		{
			$today = date("Ymd");
			$cond .= " and ((wkfw_start_date <= '$today' and wkfw_end_date >= '$today') or ";
			$cond .= "(wkfw_start_date is null and wkfw_end_date is null) or ";
			$cond .= "(wkfw_start_date <= '$today' and wkfw_end_date is null) or ";
			$cond .= "(wkfw_start_date is null and wkfw_end_date >= '$today')) ";


			if($arr_emp_info != "")
			{
				$cond .= "and (";

				for($i=0; $i<count($arr_emp_info); $i++)
				{
					$emp_class     = $arr_emp_info[$i]["emp_class"];
					$emp_attribute = $arr_emp_info[$i]["emp_attribute"];
					$emp_dept      = $arr_emp_info[$i]["emp_dept"];
					$emp_st        = $arr_emp_info[$i]["emp_st"];

					if($i > 0)
					{
						$cond .= "or ";
					}

					$cond .= "(";

					$cond .= "(fpluswkfwmst.ref_dept_flg = '1' or (fpluswkfwmst.ref_dept_flg = '2' and ";
					$cond .= "exists (select * from fpluswkfw_refdept where fpluswkfw_refdept.wkfw_id = fpluswkfwmst.wkfw_id ";
					$cond .= "and fpluswkfw_refdept.class_id = $emp_class and fpluswkfw_refdept.atrb_id = $emp_attribute and fpluswkfw_refdept.dept_id = $emp_dept))) ";

					$cond .= "and ";

					$cond .= "(fpluswkfwmst.ref_st_flg = '1' or (fpluswkfwmst.ref_st_flg = '2' and exists (select * from fpluswkfw_refst where fpluswkfw_refst.wkfw_id = fpluswkfwmst.wkfw_id ";
					$cond .= "and fpluswkfw_refst.st_id = $emp_st)))";

					$cond .= ") ";
				}

				$cond .= "or exists (select * from fpluswkfw_refemp where fpluswkfw_refemp.wkfw_id = fpluswkfwmst.wkfw_id and fpluswkfw_refemp.emp_id = '$emp_id')";
				$cond .= ") ";
			}
		}

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_type" => $row["wkfw_type"], "wkfw_folder_id" => $row["wkfw_folder_id"]);
		}

		return $arr;
	}

	// ワークフロー数を算出
	function calc_workflow_for_count($folder_list, $screen_mode, $arr_emp_info, $emp_id)
	{

		$wkfw_list = $this->get_workflow_for_count($screen_mode, $arr_emp_info, $emp_id);
		$wkfw_count_per_folder = array();
		if (!is_null($wkfw_list))
		{
			foreach ($wkfw_list as $wkfw) {
				$cate_id = $wkfw["wkfw_type"];
				$folder_id = ($wkfw["wkfw_folder_id"] != "") ? $wkfw["wkfw_folder_id"] : "-";
				$doc_count_per_folder[$cate_id][$folder_id]++;
			}
		}
		if (!is_null($wkfw_list))
		{
			$wkfw_counts = array();
			foreach ($folder_list as $folder) {
				$this->calc_wkfw_counts_per_folder($folder, $doc_count_per_folder, $wkfw_counts);
			}
		}
		return $wkfw_counts;
	}

	// 指定フォルダ配下のワークフロー数を算出
	function calc_wkfw_counts_per_folder($folder, $doc_count_per_folder, &$wkfw_counts) {

		if ($folder["type"] == "category")
		{
			$cate_id = $folder["wkfw_type"];
			$folder_id = "-";
		}
		else if($folder["type"] == "folder")
		{
			$cate_id = $folder["cate"];
			$folder_id = $folder["id"];
		}

		$wkfw_counts[$cate_id][$folder_id] = intval($doc_count_per_folder[$cate_id][$folder_id]);

		foreach ($folder["folders"] as $folder)
		{
			$wkfw_counts[$cate_id][$folder_id] += $this->calc_wkfw_counts_per_folder($folder, $doc_count_per_folder, $wkfw_counts);
		}

		return $wkfw_counts[$cate_id][$folder_id];
	}


	// ワークフローカテゴリ/フォルダ情報取得
	function get_workflow_folder_list_for_apply($emp_id, &$wkfw_counts)
	{

		$today = date("Ymd");

		// 職員の部署・役職取得
		if($emp_id != "")
		{
			$arr_emp_info = $this->get_emp_info($emp_id);
		}
		else
		{
			$arr_emp_info = "";
		}

		// カテゴリ取得
		$category_list = $this->get_workflow_category_for_apply($arr_emp_info, $emp_id);

		// フォルダ/ワークフロー取得
		foreach($category_list as $i => $category)
		{
			$wkfw_type = $category["wkfw_type"];
			$category["type"] = "category";
			$category["folders"] = $this->get_workflow_tree_for_apply("", $wkfw_type, $today, $arr_emp_info, $emp_id);

			$category_list[$i] = $category;
		}

		// フォルダごとのワークフロー数を算出
		$wkfw_counts = $this->calc_workflow_for_count($category_list, "apply", $arr_emp_info, $emp_id);

		return $category_list;
	}

	// ワークフローカテゴリ/フォルダ情報取得(申請参照一覧用)
	function get_workflow_folder_list_for_apply_refer(&$apply_counts)
	{

		// カテゴリ取得
		$category_list = $this->get_workflow_category_for_apply_refer();

		// フォルダ/ワークフロー取得
		foreach($category_list as $i => $category)
		{
			$wkfw_type = $category["wkfw_type"];
			$category["type"] = "category";
			$category["folders"] = $this->get_workflow_tree_for_apply_refer("", $wkfw_type);

			$category_list[$i] = $category;
		}

		// フォルダごとの申請数を算出
		$apply_counts = $this->calc_apply_for_count($category_list);

		return $category_list;
	}

    // ワークフローカテゴリ情報取得(申請参照一覧用)
	function get_workflow_category_for_apply_refer()
	{
		// カテゴリ情報取得
		$sql   = "select wkfw_type, wkfw_nm from fpluscatemst";
		$cond  = "where not wkfwcate_del_flg ";
		$cond .= "order by wkfw_type asc ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_type" => $row["wkfw_type"], "wkfw_nm" => $row["wkfw_nm"]);
		}
		return $arr;
	}

	// フォルダ/ワークフロー情報取得(申請参照一覧用)
	function get_workflow_tree_for_apply_refer($parent_id, $category_id)
	{
		// フォルダ
		$sql  = "select varchar(6) 'folder' as type, fplusfolder.wkfw_folder_id as id, fplusfolder.wkfw_type as cate, fplusfolder.wkfw_folder_name as name from fplusfolder ";
		$sql .= "left join (select * from fplustree where not wkfw_tree_del_flg) fplustree on fplusfolder.wkfw_folder_id = fplustree.wkfw_child_id ";
		$sql .= "where not fplusfolder.wkfw_folder_del_flg ";

		if($parent_id == "")
		{
			$sql .= "and fplustree.wkfw_parent_id is null ";
		}
		else
		{
			$sql .= "and fplustree.wkfw_parent_id = $parent_id ";
		}
		if ($category_id != "") {
			$sql .= " and fplusfolder.wkfw_type = $category_id ";
		}

		$sql .= "union all ";

		// ワークフロー
		$sql .= "select varchar(6) 'file' as type, fpluswkfwmst.wkfw_id as id, fpluswkfwmst.wkfw_type as cate, fpluswkfwmst.wkfw_title as name from fpluswkfwmst ";
		$sql .= "where wkfw_type = $category_id and not wkfw_del_flg ";

		if($parent_id == "")
		{
			$sql .= "and wkfw_folder_id is null ";
		}
		else if($parent_id != "")
		{
			$sql .= "and wkfw_folder_id = $parent_id ";
		}

		$sql .= "order by type desc, id asc ";
		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);

		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$tree = array();
		while ($row = pg_fetch_array($sel))
		{
			$type               = $row["type"];
			$folder_id          = $row["id"];
			$category_type      = $row["cate"];
			$wkfw_folder_name   = $row["name"];

			if($type == "folder")
			{
				$tree[] = array(
					"type" => $type,
					"cate" => $category_type,
					"parent_id" => $parent_id,
					"id" => $folder_id,
					"name" => $wkfw_folder_name,
					"folders" => $this->get_workflow_tree_for_apply($folder_id, $category_type)
				);
			}
			else if($type == "file")
			{
				$tree[] = array(
					"type" => $type,
					"cate" => $category_type,
					"parent_id" => $parent_id,
					"id" => $folder_id,
					"name" => $wkfw_folder_name,
					"folders" => array()
				);
			}
		}
		return $tree;
	}

	// 申請数を算出
	function calc_apply_for_count($folder_list)
	{

		$wkfw_list = $this->get_apply_for_count();
		$wkfw_count_per_folder = array();
		if (!is_null($wkfw_list))
		{
			foreach ($wkfw_list as $wkfw) {
				$cate_id = $wkfw["wkfw_type"];
				$folder_id = ($wkfw["wkfw_folder_id"] != "") ? $wkfw["wkfw_folder_id"] : "-";
				$doc_count_per_folder[$cate_id][$folder_id]++;
			}
		}

		if (!is_null($wkfw_list))
		{
			$wkfw_counts = array();
			foreach ($folder_list as $folder) {
				$this->calc_apply_counts_per_folder($folder, $doc_count_per_folder, $wkfw_counts);
			}
		}
		return $wkfw_counts;
	}

	// 申請数取得(カウント用)
	function get_apply_for_count()
	{
		$sql  = "select a.wkfw_type, a.wkfw_folder_id, a.wkfw_id, b.apply_id ";
		$sql .= "from fpluswkfwmst a ";
		$sql .= "inner join fplusapply b on a.wkfw_id = b.wkfw_id ";
		$cond = "where not wkfw_del_flg and not b.draft_flg and not b.delete_flg ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_type" => $row["wkfw_type"], "wkfw_folder_id" => $row["wkfw_folder_id"], "wkfwr_id" => $row["wkfw_id"]);
		}

		return $arr;
	}

	// 指定フォルダ配下の申請数を算出
	function calc_apply_counts_per_folder($folder, $doc_count_per_folder, &$wkfw_counts) {

		if ($folder["type"] == "category")
		{
			$cate_id = $folder["wkfw_type"];
			$folder_id = "-";
			$wkfw_id = "-";
		}
		else if($folder["type"] == "folder")
		{
			$cate_id = $folder["cate"];
			$folder_id = $folder["id"];
			$wkfw_id = "-";
		}

		$wkfw_counts[$cate_id][$folder_id] = intval($doc_count_per_folder[$cate_id][$folder_id]);

		foreach ($folder["folders"] as $folder)
		{
			$wkfw_counts[$cate_id][$folder_id] += $this->calc_apply_counts_per_folder($folder, $doc_count_per_folder, $wkfw_counts);
		}

		return $wkfw_counts[$cate_id][$folder_id];
	}

	function get_apply_count($wkfw_id)
	{
		$sql  = "select count(*) as cnt from fplusapply ";
		$cond = "where not draft_flg and not delete_flg and wkfw_id = $wkfw_id";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0, "cnt");
	}


	// フォルダ/ワークフロー情報取得
	function get_workflow_tree_for_apply($parent_id, $category_id, $today, $arr_emp_info, $emp_id)
	{

		// フォルダ
		$sql  = "select varchar(6) 'folder' as type, fplusfolder.wkfw_folder_id as id, fplusfolder.wkfw_type as cate, fplusfolder.wkfw_folder_name as name from fplusfolder ";
		$sql .= "left join (select * from fplustree where not wkfw_tree_del_flg) fplustree on fplusfolder.wkfw_folder_id = fplustree.wkfw_child_id ";
		$sql .= "where not fplusfolder.wkfw_folder_del_flg ";

		if($parent_id == "")
		{
			$sql .= "and fplustree.wkfw_parent_id is null ";
		}
		else
		{
			$sql .= "and fplustree.wkfw_parent_id = $parent_id ";
		}
		if ($category_id != "") {
			$sql .= " and fplusfolder.wkfw_type = $category_id ";
		}

		if($arr_emp_info != "")
		{
			$sql .= "and (";

			for($i=0; $i<count($arr_emp_info); $i++)
			{
				$emp_class     = $arr_emp_info[$i]["emp_class"];
				$emp_attribute = $arr_emp_info[$i]["emp_attribute"];
				$emp_dept      = $arr_emp_info[$i]["emp_dept"];
				$emp_st        = $arr_emp_info[$i]["emp_st"];

				if($i > 0)
				{
					$sql .= "or ";
				}

				$sql .= "(";

				$sql .= "(fplusfolder.ref_dept_flg = '1' or (fplusfolder.ref_dept_flg = '2' and ";
				$sql .= "exists (select * from fplusfolder_refdept where fplusfolder_refdept.wkfw_folder_id = fplusfolder.wkfw_folder_id ";
				$sql .= "and fplusfolder_refdept.class_id = $emp_class and fplusfolder_refdept.atrb_id = $emp_attribute and fplusfolder_refdept.dept_id = $emp_dept))) ";

				$sql .= "and ";

				$sql .= "(fplusfolder.ref_st_flg = '1' or (fplusfolder.ref_st_flg = '2' and exists (select * from fplusfolder_refst where fplusfolder_refst.wkfw_folder_id = fplusfolder.wkfw_folder_id ";
				$sql .= "and fplusfolder_refst.st_id = $emp_st)))";

				$sql .= ") ";
			}
			$sql .= "or exists (select * from fplusfolder_refemp where fplusfolder_refemp.wkfw_folder_id = fplusfolder.wkfw_folder_id and fplusfolder_refemp.emp_id = '$emp_id')";
			$sql .= ") ";
		}

		$sql .= "union all ";


		// ワークフロー
		$sql .= "select varchar(6) 'file' as type, fpluswkfwmst.wkfw_id as id, fpluswkfwmst.wkfw_type as cate, fpluswkfwmst.wkfw_title as name from fpluswkfwmst ";
		$sql .= "where wkfw_type = $category_id and not wkfw_del_flg ";

		$sql .= "and ((wkfw_start_date <= '$today' and wkfw_end_date >= '$today') or ";
		$sql .= "(wkfw_start_date is null and wkfw_end_date is null) or ";
		$sql .= "(wkfw_start_date <= '$today' and wkfw_end_date is null) or ";
		$sql .= "(wkfw_start_date is null and wkfw_end_date >= '$today')) ";

		if($parent_id == "")
		{
			$sql .= "and wkfw_folder_id is null ";
		}
		else if($parent_id != "")
		{
			$sql .= "and wkfw_folder_id = $parent_id ";
		}

		if($arr_emp_info != "")
		{
			$sql .= "and (";

			for($i=0; $i<count($arr_emp_info); $i++)
			{
				$emp_class     = $arr_emp_info[$i]["emp_class"];
				$emp_attribute = $arr_emp_info[$i]["emp_attribute"];
				$emp_dept      = $arr_emp_info[$i]["emp_dept"];
				$emp_st        = $arr_emp_info[$i]["emp_st"];

				if($i > 0)
				{
					$sql .= "or ";
				}

				$sql .= "(";

				$sql .= "(fpluswkfwmst.ref_dept_flg = '1' or (fpluswkfwmst.ref_dept_flg = '2' and ";
				$sql .= "exists (select * from fpluswkfw_refdept where fpluswkfw_refdept.wkfw_id = fpluswkfwmst.wkfw_id ";
				$sql .= "and fpluswkfw_refdept.class_id = $emp_class and fpluswkfw_refdept.atrb_id = $emp_attribute and fpluswkfw_refdept.dept_id = $emp_dept))) ";

				$sql .= "and ";

				$sql .= "(fpluswkfwmst.ref_st_flg = '1' or (fpluswkfwmst.ref_st_flg = '2' and exists (select * from fpluswkfw_refst where fpluswkfw_refst.wkfw_id = fpluswkfwmst.wkfw_id ";
				$sql .= "and fpluswkfw_refst.st_id = $emp_st)))";

				$sql .= ") ";
			}

			$sql .= "or exists (select * from fpluswkfw_refemp where fpluswkfw_refemp.wkfw_id = fpluswkfwmst.wkfw_id and fpluswkfw_refemp.emp_id = '$emp_id')";
			$sql .= ") ";
		}

		$sql .= "order by type desc, id asc ";
		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);

		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$tree = array();
		while ($row = pg_fetch_array($sel))
		{
			$type               = $row["type"];
			$folder_id          = $row["id"];
			$category_type      = $row["cate"];
			$wkfw_folder_name   = $row["name"];

			if($type == "folder")
			{
				$tree[] = array(
					"type" => $type,
					"cate" => $category_type,
					"parent_id" => $parent_id,
					"id" => $folder_id,
					"name" => $wkfw_folder_name,
					"folders" => $this->get_workflow_tree_for_apply($folder_id, $category_type, $today, $arr_emp_info, $emp_id)
				);
			}
			else if($type == "file")
			{
				$tree[] = array(
					"type" => $type,
					"cate" => $category_type,
					"parent_id" => $parent_id,
					"id" => $folder_id,
					"name" => $wkfw_folder_name,
					"folders" => array()
				);
			}
		}
		return $tree;
	}

	// カテゴリ名取得
	function get_category_from_folder_id($folder_id)
	{
		$sql  = "select wkfw_type, wkfw_nm from fpluscatemst ";
		$cond = "where wkfw_type in (select wkfw_type from fplusfolder where wkfw_folder_id = $folder_id) and not wkfwcate_del_flg ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$wkfw_type        = pg_fetch_result($sel, 0, "wkfw_type");
		$wkfw_nm = pg_fetch_result($sel, 0, "wkfw_nm");

		$arr[] = array("wkfw_type" => $wkfw_type, "wkfw_nm" => $wkfw_nm);

		return $arr;
	}

    // ワークフローカテゴリ情報取得(申請画面用)
	function get_workflow_category_for_apply($arr_emp_info, $emp_id)
	{
		// カテゴリ情報取得
		$sql   = "select wkfw_type, wkfw_nm from fpluscatemst";
		$cond  = "where not wkfwcate_del_flg ";

		if($arr_emp_info !=  "")
		{
			$cond .= "and (";

			for($i=0; $i<count($arr_emp_info); $i++)
			{
				$emp_class     = $arr_emp_info[$i]["emp_class"];
				$emp_attribute = $arr_emp_info[$i]["emp_attribute"];
				$emp_dept      = $arr_emp_info[$i]["emp_dept"];
				$emp_st        = $arr_emp_info[$i]["emp_st"];

				if($i > 0)
				{
					$cond .= "or ";
				}

				$cond .= "(";

				$cond .= "(ref_dept_flg = '1' or (ref_dept_flg = '2' and ";
				$cond .= "exists (select * from fpluscate_refdept where fpluscate_refdept.wkfw_type = fpluscatemst.wkfw_type ";
				$cond .= "and fpluscate_refdept.class_id = $emp_class and fpluscate_refdept.atrb_id = $emp_attribute and fpluscate_refdept.dept_id = $emp_dept))) ";

				$cond .= "and ";

				$cond .= "(ref_st_flg = '1' or (ref_st_flg = '2' and exists (select * from fpluscate_refst where fpluscate_refst.wkfw_type = fpluscatemst.wkfw_type ";
				$cond .= "and fpluscate_refst.st_id = $emp_st)))";

				$cond .= ") ";
			}

			$cond .= "or exists (select * from fpluscate_refemp where fpluscate_refemp.wkfw_type = fpluscatemst.wkfw_type and fpluscate_refemp.emp_id = '$emp_id')";
			$cond .= ") ";
		}
		$cond .= "order by wkfw_type asc ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_type" => $row["wkfw_type"], "wkfw_nm" => $row["wkfw_nm"]);
		}
		return $arr;
	}


	// 職員の部署役職取得
	function get_emp_info($emp_id)
	{

		//==============================
		//報告書承認者設定を取得
		//==============================
		$item_list = "";
		$sql = "select * from fplus_etc_setting";
		$cond = "";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$item_list = pg_fetch_all($sel);




		// 職員情報取得(複数部署役職対応)
		$sql  = "select emp_class, emp_attribute, emp_dept, emp_room, emp_st from empmst where emp_id = '$emp_id' ";

		//兼務承認者のチェック
		if($item_list[0]["only_approver"] != "t")
		{

			//兼務先を所属として取得するのが下記ＳＱＬですが、兼務先を所属先として認定して報告先（自分の所属が兼務先でもあり、その所属の承認者を取得する→兼務先の承認者が報告書に出てくる）
			//として設定するのは、設定画面で選択するようにしました

			$sql .= "union ";
			$sql .= "select emp_class, emp_attribute, emp_dept, emp_room, emp_st from concurrent where emp_id = '$emp_id'";
		}

		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("emp_class" => $row["emp_class"], "emp_attribute" => $row["emp_attribute"], "emp_dept" => $row["emp_dept"], "emp_room" => $row["emp_room"], "emp_st" => $row["emp_st"]);
		}

		return $arr;
	}

//-------------------------------------------------------------------------------------------------------------------------
// アクセス権限関連
//-------------------------------------------------------------------------------------------------------------------------

	// 部門一覧取得
	function get_classmst()
	{
		$sql = "select class_id, class_nm from classmst";
		$cond = "where class_del_flg = 'f' order by order_no";
		$sel_class = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_class == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_class;
	}

	// 課一覧取得
	function get_atrbmst()
	{
		$sql = "select classmst.class_id, atrbmst.atrb_id, classmst.class_nm, atrbmst.atrb_nm from atrbmst inner join classmst on atrbmst.class_id = classmst.class_id";
		$cond = "where classmst.class_del_flg = 'f' and atrbmst.atrb_del_flg = 'f' order by classmst.order_no, atrbmst.order_no";
		$sel_atrb = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_atrb == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_atrb;
	}

	// 科一覧取得
	function get_deptmst()
	{
		$sql = "select classmst.class_id, atrbmst.atrb_id, deptmst.dept_id, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm from deptmst inner join atrbmst on deptmst.atrb_id = atrbmst.atrb_id inner join classmst on atrbmst.class_id = classmst.class_id";
		$cond = "where classmst.class_del_flg = 'f' and atrbmst.atrb_del_flg = 'f' and deptmst.dept_del_flg = 'f' order by classmst.order_no, atrbmst.order_no, deptmst.order_no";
		$sel_dept = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_dept == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_dept;
	}

	// 役職一覧取得
	function get_stmst()
	{
		$sql = "select st_id, st_nm from stmst";
		$cond = "where st_del_flg = 'f' order by order_no";
		$sel_st = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_st == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_st;
	}

	// カテゴリ用アクセス権（科）登録
	function delete_regist_wkfwcate_refdept($wkfw_type, $arr_ref_dept, $mode)
	{
		if($mode == "UPD")
		{
			$sql = "delete from fpluscate_refdept";
			$cond = "where wkfw_type = $wkfw_type";
			$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if($del == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}

		foreach ($arr_ref_dept as $ref_dept)
		{
			list($class_id, $atrb_id, $dept_id) = split("-", $ref_dept);
			$sql = "insert into fpluscate_refdept (wkfw_type, class_id, atrb_id, dept_id) values (";
			$content = array($wkfw_type, $class_id, $atrb_id, $dept_id);
			$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
			if ($ins == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}

	// カテゴリ用アクセス権（役職）登録
	function delete_regist_wkfwcate_refst($wkfw_type, $arr_ref_st, $mode)
	{
		if($mode == "UPD")
		{
			$sql = "delete from fpluscate_refst";
			$cond = "where wkfw_type = $wkfw_type";
			$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if($del == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
		foreach ($arr_ref_st as $ref_st)
		{
			$sql = "insert into fpluscate_refst (wkfw_type, st_id) values (";
			$content = array($wkfw_type, $ref_st);
			$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
			if ($ins == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}

	// カテゴリ用アクセス権（職員）登録
	function delete_regist_wkfwcate_refemp($wkfw_type, $arr_ref_emp, $mode)
	{
		if($mode == "UPD")
		{
			$sql = "delete from fpluscate_refemp";
			$cond = "where wkfw_type = $wkfw_type";
			$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($del == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
		foreach ($arr_ref_emp as $ref_emp) {
			$sql = "insert into fpluscate_refemp (wkfw_type, emp_id) values (";
			$content = array($wkfw_type, $ref_emp);
			$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
			if ($ins == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}


	// フォルダ用アクセス権（科）登録
	function delete_regist_wkfwfolder_refdept($wkfw_folder_id, $arr_ref_dept, $mode)
	{
		if($mode == "UPD")
		{
			$sql = "delete from fplusfolder_refdept";
			$cond = "where wkfw_folder_id = $wkfw_folder_id";
			$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if($del == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
		foreach ($arr_ref_dept as $ref_dept)
		{
			list($class_id, $atrb_id, $dept_id) = split("-", $ref_dept);
			$sql = "insert into fplusfolder_refdept (wkfw_folder_id, class_id, atrb_id, dept_id) values (";
			$content = array($wkfw_folder_id, $class_id, $atrb_id, $dept_id);
			$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
			if ($ins == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}

	// フォルダ用アクセス権（役職）登録
	function delete_regist_wkfwfolder_refst($wkfw_folder_id, $arr_ref_st, $mode)
	{
		if($mode == "UPD")
		{
			$sql = "delete from fplusfolder_refst";
			$cond = "where wkfw_folder_id = $wkfw_folder_id";
			$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if($del == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
		foreach ($arr_ref_st as $ref_st)
		{
			$sql = "insert into fplusfolder_refst (wkfw_folder_id, st_id) values (";
			$content = array($wkfw_folder_id, $ref_st);
			$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
			if ($ins == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}

	// フォルダ用アクセス権（職員）登録
	function delete_regist_wkfwfolder_refemp($wkfw_folder_id, $arr_ref_emp, $mode)
	{
		if($mode == "UPD")
		{
			$sql = "delete from fplusfolder_refemp";
			$cond = "where wkfw_folder_id = $wkfw_folder_id";
			$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($del == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
		foreach ($arr_ref_emp as $ref_emp) {
			$sql = "insert into fplusfolder_refemp (wkfw_folder_id, emp_id) values (";
			$content = array($wkfw_folder_id, $ref_emp);
			$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
			if ($ins == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}

	// ワークフロー用アクセス権（科）登録
	function regist_wkfw_refdept($wkfw_id, $arr_ref_dept, $mode)
	{
		foreach ($arr_ref_dept as $ref_dept)
		{
			list($class_id, $atrb_id, $dept_id) = split("-", $ref_dept);

			if($mode == "ALIAS")
			{
				$sql = "insert into fpluswkfw_refdept ";
			}
			else
			{
				$sql = "insert into fpluswkfw_refdept_real ";
			}
			$sql .= "(wkfw_id, class_id, atrb_id, dept_id) values (";
			$content = array($wkfw_id, $class_id, $atrb_id, $dept_id);
			$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
			if ($ins == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}

	// ワークフロー用アクセス権（科）削除
	function delete_wkfw_refdept($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from fpluswkfw_refdept";
		}
		else
		{
			$sql = "delete from fpluswkfw_refdept_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー用アクセス権（役職）登録
	function regist_wkfw_refst($wkfw_id, $arr_ref_st, $mode)
	{
		foreach ($arr_ref_st as $ref_st)
		{
			if($mode == "ALIAS")
			{
				$sql = "insert into fpluswkfw_refst ";
			}
			else
			{
				$sql = "insert into fpluswkfw_refst_real ";
			}

			$sql .= "(wkfw_id, st_id) values (";
			$content = array($wkfw_id, $ref_st);
			$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
			if ($ins == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}

	// ワークフロー用アクセス権（役職）削除
	function delete_wkfw_refst($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from fpluswkfw_refst";
		}
		else
		{
			$sql = "delete from fpluswkfw_refst_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー用アクセス権（職員）登録
	function regist_wkfw_refemp($wkfw_id, $arr_ref_emp, $mode)
	{
		foreach ($arr_ref_emp as $ref_emp)
		{
			if($mode == "ALIAS")
			{
				$sql = "insert into fpluswkfw_refemp ";
			}
			else
			{
				$sql = "insert into fpluswkfw_refemp_real ";
			}

			$sql .= "(wkfw_id, emp_id) values (";
			$content = array($wkfw_id, $ref_emp);
			$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
			if ($ins == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}

	// ワークフロー用アクセス権（職員）削除
	function delete_wkfw_refemp($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from fpluswkfw_refemp";
		}
		else
		{
			$sql = "delete from fpluswkfw_refemp_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// カテゴリ用アクセス権（科）取得
	function get_wkfwcate_refdept($wkfw_type)
	{
		$sql = "select wkfw_type, class_id, atrb_id, dept_id from fpluscate_refdept";
		$cond = "where wkfw_type = $wkfw_type";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_type" => $row["wkfw_type"], "class_id" => $row["class_id"], "atrb_id" => $row["atrb_id"], "dept_id" => $row["dept_id"]);
		}
		return $arr;
	}

	// カテゴリ用アクセス権（役職）取得
	function get_wkfwcate_refst($wkfw_type)
	{
		$sql = "select wkfw_type, st_id from fpluscate_refst";
		$cond = "where wkfw_type = $wkfw_type";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_type" => $row["wkfw_type"], "st_id" => $row["st_id"]);
		}
		return $arr;
	}

    // カテゴリ用アクセス権（職員）取得
	function get_wkfwcate_refemp($wkfw_type)
	{
		$sql = "select wkfw_type, emp_id from fpluscate_refemp";
		$cond = "where wkfw_type = $wkfw_type";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_type" => $row["wkfw_type"], "emp_id" => $row["emp_id"]);
		}
		return $arr;
	}

	// フォルダ用アクセス権（科）取得
	function get_wkfwfolder_refdept($wkfw_folder_id)
	{
		$sql = "select wkfw_folder_id, class_id, atrb_id, dept_id from fplusfolder_refdept";
		$cond = "where wkfw_folder_id = $wkfw_folder_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_folder_id" => $row["wkfw_folder_id"], "class_id" => $row["class_id"], "atrb_id" => $row["atrb_id"], "dept_id" => $row["dept_id"]);
		}
		return $arr;
	}

	// フォルダ用アクセス権（役職）取得
	function get_wkfwfolder_refst($wkfw_folder_id)
	{
		$sql = "select wkfw_folder_id, st_id from fplusfolder_refst";
		$cond = "where wkfw_folder_id = $wkfw_folder_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_folder_id" => $row["wkfw_folder_id"], "st_id" => $row["st_id"]);
		}
		return $arr;
	}

    // フォルダ用アクセス権（職員）取得
	function get_wkfwfolder_refemp($wkfw_folder_id)
	{
		$sql = "select wkfw_folder_id, emp_id from fplusfolder_refemp";
		$cond = "where wkfw_folder_id = $wkfw_folder_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_folder_id" => $row["wkfw_folder_id"], "emp_id" => $row["emp_id"]);
		}
		return $arr;
	}

	// ワークフロー用アクセス権（科）取得
	function get_wkfw_refdept($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "select wkfw_id, class_id, atrb_id, dept_id from fpluswkfw_refdept";
		}
		else
		{
			$sql = "select wkfw_id, class_id, atrb_id, dept_id from fpluswkfw_refdept_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_id" => $row["wkfw_id"], "class_id" => $row["class_id"], "atrb_id" => $row["atrb_id"], "dept_id" => $row["dept_id"]);
		}
		return $arr;
	}

	// ワークフロー用アクセス権（役職）取得
	function get_wkfw_refst($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "select wkfw_id, st_id from fpluswkfw_refst";
		}
		else
		{
			$sql = "select wkfw_id, st_id from fpluswkfw_refst_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_id" => $row["wkfw_id"], "st_id" => $row["st_id"]);
		}
		return $arr;
	}

	// ワークフロー用アクセス権（職員）取得
	function get_wkfw_refemp($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "select wkfw_id, emp_id from fpluswkfw_refemp";
		}
		else
		{
			$sql = "select wkfw_id, emp_id from fpluswkfw_refemp_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_id" => $row["wkfw_id"], "emp_id" => $row["emp_id"]);
		}
		return $arr;
	}


	// 結果通知管理情報取得
	function get_wkfwnoticemng($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql  = "select * from fplusnoticemng";
		}
		else
		{
			$sql  = "select * from fplusnoticemng_real";
		}
		$cond = "where wkfw_id = $wkfw_id";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$notice_target_class_div = pg_fetch_result($sel, 0, "target_class_div");
        $rslt_ntc_div0_flg = pg_fetch_result($sel, 0, "rslt_ntc_div0_flg");
        $rslt_ntc_div1_flg = pg_fetch_result($sel, 0, "rslt_ntc_div1_flg");
        $rslt_ntc_div2_flg = pg_fetch_result($sel, 0, "rslt_ntc_div2_flg");
        $rslt_ntc_div3_flg = pg_fetch_result($sel, 0, "rslt_ntc_div3_flg");
        $rslt_ntc_div4_flg = pg_fetch_result($sel, 0, "rslt_ntc_div4_flg");

		return array(
                       "notice_target_class_div" => $notice_target_class_div,
                       "rslt_ntc_div0_flg" => $rslt_ntc_div0_flg,
                       "rslt_ntc_div1_flg" => $rslt_ntc_div1_flg,
                       "rslt_ntc_div2_flg" => $rslt_ntc_div2_flg,
                       "rslt_ntc_div3_flg" => $rslt_ntc_div3_flg,
                       "rslt_ntc_div4_flg" => $rslt_ntc_div4_flg
		              );
	}

	// 部署役職指定(結果通知)情報取得
	function get_wkfwnoticestdtl($wkfw_id, $st_div, $mode)
	{
		$sql   = "select a.*, b.st_nm ";
		if($mode == "ALIAS")
		{
			$sql  .= "from fplusnoticestdtl a ";
		}
		else
		{
			$sql  .= "from fplusnoticestdtl_real a ";
		}
		$sql  .= "inner join stmst b on a.st_id = b.st_id ";
		$cond  = "where a.wkfw_id = $wkfw_id and st_div = $st_div";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();

		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("st_id" => $row["st_id"], "st_nm" => $row["st_nm"]);

		}
		return $arr;
	}

	// 職員指定(結果通知)情報取得
	function get_wkfwnoticedtl($wkfw_id, $mode)
	{
		$sql  = "select a.*, b.emp_lt_nm, b.emp_ft_nm ";
		if($mode == "ALIAS")
		{
			$sql .= "from fplusnoticedtl a ";
		}
		else
		{
			$sql .= "from fplusnoticedtl_real a ";
		}
		$sql .= "inner join empmst b on a.emp_id = b.emp_id ";
		$cond = "where a.wkfw_id = $wkfw_id ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();

		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("emp_id" => $row["emp_id"], "emp_lt_nm" => $row["emp_lt_nm"], "emp_ft_nm" => $row["emp_ft_nm"]);

		}
		return $arr;
	}

	// 委員会・ＷＧ指定(結果通知)情報取得
	function get_wkfwnoticepjtdtl($wkfw_id, $mode)
	{
        $sql  = "select pjt_id, pjt_name from project ";
		if($mode == "ALIAS")
		{
	        $sql .= "where pjt_id in (select parent_pjt_id from fplusnoticepjtdtl where wkfw_id = $wkfw_id) ";
		}
		else
		{
	        $sql .= "where pjt_id in (select parent_pjt_id from fplusnoticepjtdtl_real where wkfw_id = $wkfw_id) ";
		}
		$sql .= "union all ";

		$sql .= "select pjt_id, pjt_name from project ";

		if($mode == "ALIAS")
		{
			$sql .= "where pjt_id in (select child_pjt_id from fplusnoticepjtdtl where wkfw_id = $wkfw_id)";
		}
		else
		{
			$sql .= "where pjt_id in (select child_pjt_id from fplusnoticepjtdtl_real where wkfw_id = $wkfw_id)";
		}

		$cond = "";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		if(pg_numrows($sel) == 1)
		{
			$pjt_parent_id = pg_fetch_result($sel, 0, "pjt_id");
			$pjt_parent_nm = pg_fetch_result($sel, 0, "pjt_name");

			$arr[] = array("pjt_parent_id" => $pjt_parent_id, "pjt_parent_nm" => $pjt_parent_nm);

		}
		else if(pg_numrows($sel) == 2)
		{
			$pjt_parent_id = pg_fetch_result($sel, 0, "pjt_id");
			$pjt_parent_nm = pg_fetch_result($sel, 0, "pjt_name");
			$pjt_child_id = pg_fetch_result($sel, 1, "pjt_id");
			$pjt_child_nm = pg_fetch_result($sel, 1, "pjt_name");

			$arr[] = array("pjt_parent_id" => $pjt_parent_id, "pjt_parent_nm" => $pjt_parent_nm, "pjt_child_id" => $pjt_child_id, "pjt_child_nm" => $pjt_child_nm);
		}
		return $arr;
	}

	// 部署役職指定(結果通知)(部署指定)情報取得
	function get_wkfwnoticesectdtl($wkfw_id, $mode)
	{
		$sql  = "select a.*, b.class_nm, c.atrb_nm, d.dept_nm, e.room_nm ";
		if($mode == "ALIAS")
		{
			$sql .= "from fplusnoticesectdtl a ";
		}
		else
		{
			$sql .= "from fplusnoticesectdtl_real a ";
		}
		$sql .= "left join classmst b on a.class_id = b.class_id and not b.class_del_flg ";
		$sql .= "left join atrbmst c on a.atrb_id = c.atrb_id and not c.atrb_del_flg ";
		$sql .= "left join deptmst d on a.dept_id = d.dept_id and not d.dept_del_flg ";
		$sql .= "left join classroom e on a.room_id = e.room_id and not e.room_del_flg ";
		$cond = "where a.wkfw_id = $wkfw_id ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$class_id = pg_fetch_result($sel, 0, "class_id");
		$atrb_id = pg_fetch_result($sel, 0, "atrb_id");
		$dept_id = pg_fetch_result($sel, 0, "dept_id");
		$room_id = pg_fetch_result($sel, 0, "room_id");

		$class_nm = pg_fetch_result($sel, 0, "class_nm");
		$atrb_nm = pg_fetch_result($sel, 0, "atrb_nm");
		$dept_nm = pg_fetch_result($sel, 0, "dept_nm");
		$room_nm = pg_fetch_result($sel, 0, "room_nm");

		return array("class_id" => $class_id,
         			   "atrb_id" => $atrb_id,
				       "dept_id" => $dept_id,
				       "room_id" => $room_id,
         			   "class_nm" => $class_nm,
         			   "atrb_nm" => $atrb_nm,
				       "dept_nm" => $dept_nm,
				       "room_nm" => $room_nm);

	}


//-------------------------------------------------------------------------------------------------------------------------
// ワークフロー登録・更新・削除関連
//-------------------------------------------------------------------------------------------------------------------------

	// 承認者管理登録
	function regist_wkfwapvmng($wkfw_id, $apv_order, $approve_mng, $mode)
	{

		$deci_flg         = $approve_mng["deci_flg"];
		$target_class_div = $approve_mng["target_class_div"];
		$multi_apv_flg    = $approve_mng["multi_apv_flg"];
		$next_notice_div  = $approve_mng["next_notice_div"];
		$apv_div0_flg     = $approve_mng["apv_div0_flg"];
		$apv_div1_flg     = $approve_mng["apv_div1_flg"];
		$apv_div2_flg     = $approve_mng["apv_div2_flg"];
		$apv_div3_flg     = $approve_mng["apv_div3_flg"];
		$apv_div4_flg     = $approve_mng["apv_div4_flg"];
		$apv_num          = $approve_mng["apv_num"];

		if($mode == "ALIAS")
		{
			$sql = "insert into fplusapvmng ";
		}
		else
		{
			$sql = "insert into fplusapvmng_real ";
		}

		$sql .= "(wkfw_id, apv_order, deci_flg, target_class_div, multi_apv_flg, next_notice_div, apv_div0_flg, apv_div1_flg, apv_div2_flg, apv_div3_flg, apv_div4_flg, apv_num) values(";
		$content = array($wkfw_id, $apv_order, $deci_flg, $target_class_div, $multi_apv_flg, $next_notice_div, $apv_div0_flg, $apv_div1_flg, $apv_div2_flg, $apv_div3_flg, $apv_div4_flg, $apv_num);

		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職指定登録
	function regist_wkfwapvpstdtl($wkfw_id, $apv_order, $st_id, $st_div, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "insert into fplusapvpstdtl ";
		}
		else
		{
			$sql = "insert into fplusapvpstdtl_real ";
		}
		$sql .= "(wkfw_id, apv_order, st_id, st_div) values(";
		$content = array($wkfw_id, $apv_order, $st_id, $st_div);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 職員指定登録
	function regist_wkfwapvdtl($wkfw_id, $apv_order, $emp_id, $apv_sub_order, $mode)
	{

		if($mode == "ALIAS")
		{
			$sql = "insert into fplusapvdtl ";
		}
		else
		{
			$sql = "insert into fplusapvdtl_real ";
		}

		$sql .= "(wkfw_id, apv_order, emp_id, apv_sub_order) values(";
		$content = array($wkfw_id, $apv_order, $emp_id, $apv_sub_order);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 委員会・ＷＧ登録
	function regist_wkfwpjtdtl($wkfw_id, $apv_order, $pjt_parent_id, $pjt_child_id, $mode)
	{
		$pjt_child_id = ($pjt_child_id == "") ? null : $pjt_child_id;

		if($mode == "ALIAS")
		{
			$sql = "insert into fpluswkfwpjtdtl ";
		}
		else
		{
			$sql = "insert into fpluswkfwpjtdtl_real ";
		}

		$sql .= "(wkfw_id, apv_order, parent_pjt_id, child_pjt_id) values(";
		$content = array($wkfw_id, $apv_order, $pjt_parent_id, $pjt_child_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職(部署指定)
	function regist_wkfwapvsectdtl($wkfw_id, $apv_order, $class_id, $atrb_id, $dept_id, $room_id, $mode)
	{

		$atrb_id = ($atrb_id == "") ? null : $atrb_id;
		$dept_id = ($dept_id == "") ? null : $dept_id;
		$room_id = ($room_id == "") ? null : $room_id;

		if($mode == "ALIAS")
		{
			$sql = "insert into fplusapvsectdtl ";
		}
		else
		{
			$sql = "insert into fplusapvsectdtl_real ";
		}

		$sql .= "(wkfw_id, apv_order, class_id, atrb_id, dept_id, room_id) values(";
		$content = array($wkfw_id, $apv_order, $class_id, $atrb_id, $dept_id, $room_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 添付ファイル登録
	function regist_wkfwfile($wkfw_id, $wkfwfile_no, $wkfwfile_name, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "insert into fpluswkfwfile ";
		}
		else
		{
			$sql = "insert into fpluswkfwfile_real ";
		}

		$sql .= "(wkfw_id, wkfwfile_no, wkfwfile_name) values (";
		$content = array($wkfw_id, $wkfwfile_no, $wkfwfile_name);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 結果通知管理登録
	function regist_wkfwnoticemng($arr, $mode)
	{
		$wkfw_id           = $arr["wkfw_id"];
		$target_class_div  = $arr["target_class_div"];
		$rslt_ntc_div0_flg = $arr["rslt_ntc_div0_flg"];
		$rslt_ntc_div1_flg = $arr["rslt_ntc_div1_flg"];
		$rslt_ntc_div2_flg = $arr["rslt_ntc_div2_flg"];
		$rslt_ntc_div3_flg = $arr["rslt_ntc_div3_flg"];
		$rslt_ntc_div4_flg = $arr["rslt_ntc_div4_flg"];

		if($mode == "ALIAS")
		{
			$sql = "insert into fplusnoticemng ";
		}
		else
		{
			$sql = "insert into fplusnoticemng_real ";
		}
		$sql .= "(wkfw_id, target_class_div, rslt_ntc_div0_flg, rslt_ntc_div1_flg, rslt_ntc_div2_flg, rslt_ntc_div3_flg, rslt_ntc_div4_flg) values(";
		$content = array($wkfw_id, $target_class_div, $rslt_ntc_div0_flg, $rslt_ntc_div1_flg, $rslt_ntc_div2_flg, $rslt_ntc_div3_flg, $rslt_ntc_div4_flg);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職指定(結果通知)登録
	function regist_wkfwnoticestdtl($wkfw_id, $st_id, $st_div, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "insert into fplusnoticestdtl ";
		}
		else
		{
			$sql = "insert into fplusnoticestdtl_real ";
		}
		$sql .= "(wkfw_id, st_id, st_div) values(";
		$content = array($wkfw_id, $st_id, $st_div);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 職員指定(結果通知)登録
	function regist_wkfwnoticedtl($wkfw_id, $emp_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "insert into fplusnoticedtl ";
		}
		else
		{
			$sql = "insert into fplusnoticedtl_real ";
		}
		$sql .= "(wkfw_id, emp_id) values(";
		$content = array($wkfw_id, $emp_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 委員会・ＷＧ指定(結果通知)登録
	function regist_wkfwnoticepjtdtl($wkfw_id, $pjt_parent_id, $pjt_child_id, $mode)
	{
		$pjt_child_id = ($pjt_child_id == "") ? null : $pjt_child_id;

		if($mode == "ALIAS")
		{
			$sql = "insert into fplusnoticepjtdtl ";
		}
		else
		{
			$sql = "insert into fplusnoticepjtdtl_real ";
		}
		$sql .= "(wkfw_id, parent_pjt_id, child_pjt_id) values(";
		$content = array($wkfw_id, $pjt_parent_id, $pjt_child_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職指定(結果通知)(部署指定)登録
	function regist_wkfwnoticesectdtl($wkfw_id, $class_id, $atrb_id, $dept_id, $room_id, $mode)
	{
		$atrb_id = ($atrb_id == "") ? null : $atrb_id;
		$dept_id = ($dept_id == "") ? null : $dept_id;
		$room_id = ($room_id == "") ? null : $room_id;

		if($mode == "ALIAS")
		{
			$sql = "insert into fplusnoticesectdtl ";
		}
		else
		{
			$sql = "insert into fplusnoticesectdtl_real ";
		}
		$sql .= "(wkfw_id, class_id, atrb_id, dept_id, room_id) values(";
		$content = array($wkfw_id, $class_id, $atrb_id, $dept_id, $room_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 前提とする申請書(ワークフロー用)登録
	function regist_wkfwfprecond($wkfw_id, $precond_order, $precond_wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "insert into fplusfprecond ";
		}
		else
		{
			$sql = "insert into fplusfprecond_real ";
		}

		$sql .= "(wkfw_id, precond_order, precond_wkfw_id) values(";
		$content = array($wkfw_id, $precond_order, $precond_wkfw_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}



	// 承認者管理削除
	function delete_wkfwapvmng($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from fplusapvmng";
		}
		else
		{
			$sql = "delete from fplusapvmng_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職削除
	function delete_wkfwapvpstdtl($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from fplusapvpstdtl";
		}
		else
		{
			$sql = "delete from fplusapvpstdtl_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 職員指定削除
	function delete_wkfwapvdtl($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from fplusapvdtl";
		}
		else
		{
			$sql = "delete from fplusapvdtl_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 委員会・ＷＧ削除
	function delete_wkfwpjtdtl($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from fpluswkfwpjtdtl";
		}
		else
		{
			$sql = "delete from fpluswkfwpjtdtl_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職(部署指定)
	function delete_wkfwapvsectdtl($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from fplusapvsectdtl";
		}
		else
		{
			$sql = "delete from fplusapvsectdtl_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 結果通知管理削除
	function delete_wkfwnoticemng($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from fplusnoticemng";
		}
		else
		{
			$sql = "delete from fplusnoticemng_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職指定(結果通知)削除
	function delete_wkfwnoticestdtl($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from fplusnoticestdtl";
		}
		else
		{
			$sql = "delete from fplusnoticestdtl_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 職員指定(結果通知)削除
	function delete_wkfwnoticedtl($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from fplusnoticedtl";
		}
		else
		{
			$sql = "delete from fplusnoticedtl_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 委員会・ＷＧ指定(結果通知)削除
	function delete_wkfwnoticepjtdtl($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from fplusnoticepjtdtl";
		}
		else
		{
			$sql = "delete from fplusnoticepjtdtl_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職指定(結果通知)(部署指定)削除
	function delete_wkfwnoticesectdtl($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from fplusnoticesectdtl";
		}
		else
		{
			$sql = "delete from fplusnoticesectdtl_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 添付ファイル削除
	function delete_wkfwfile($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from fpluswkfwfile";
		}
		else
		{
			$sql = "delete from fpluswkfwfile_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 前提とする申請書(ワークフロー用)
	function delete_wkfwfprecond($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from fplusfprecond";
		}
		else
		{
			$sql = "delete from fplusfprecond_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}



	// ワークフロー登録
	function regist_wkfwmst($arr, $mode)
	{
		$wkfw_id = $arr["wkfw_id"];
		$wkfw_type = $arr["wkfw_type"];
		$wkfw_title = $arr["wkfw_title"];
		$wkfw_content = $arr["wkfw_content"];
		$start_date = $arr["start_date"];
		$end_date = $arr["end_date"];
		$wkfw_appr = $arr["wkfw_appr"];
		$wkfw_content_type = $arr["wkfw_content_type"];
		$wkfw_folder_id = $arr["wkfw_folder_id"];
		$ref_dept_st_flg = $arr["ref_dept_st_flg"];
		$ref_dept_flg = $arr["ref_dept_flg"];
		$ref_st_flg = $arr["ref_st_flg"];
		$short_wkfw_name = $arr["short_wkfw_name"];
		$apply_title_disp_flg = $arr["apply_title_disp_flg"];
		$send_mail_flg = $arr["send_mail_flg"];
		$lib_reg_flg = $arr["lib_reg_flg"];
		$lib_keyword = $arr["lib_keyword"];
		$lib_no = $arr["lib_no"];
		$lib_summary = $arr["lib_summary"];
		$lib_archive = $arr["lib_archive"];
		$lib_cate_id = $arr["lib_cate_id"];
		$lib_folder_id = $arr["lib_folder_id"];
		$lib_show_login_flg = $arr["lib_show_login_flg"];
		$lib_show_login_begin = $arr["lib_show_login_begin"];
		$lib_show_login_end = $arr["lib_show_login_end"];
		$lib_private_flg = $arr["lib_private_flg"];
		$lib_ref_dept_st_flg = $arr["lib_ref_dept_st_flg"];
		$lib_ref_dept_flg = $arr["lib_ref_dept_flg"];
		$lib_ref_st_flg = $arr["lib_ref_st_flg"];
		$lib_upd_dept_st_flg = $arr["lib_upd_dept_st_flg"];
		$lib_upd_dept_flg = $arr["lib_upd_dept_flg"];
		$lib_upd_st_flg = $arr["lib_upd_st_flg"];
		$approve_label = $arr["approve_label"];

		if($mode == "ALIAS")
		{
			$sql = "insert into fpluswkfwmst ";
		}
		else
		{
			$sql = "insert into fpluswkfwmst_real ";
		}

		$sql .= "(wkfw_id, wkfw_type, wkfw_title, wkfw_content, wkfw_start_date, wkfw_end_date, wkfw_appr, wkfw_content_type, wkfw_folder_id, ref_dept_st_flg, ref_dept_flg, ref_st_flg, short_wkfw_name, apply_title_disp_flg, send_mail_flg, lib_reg_flg, lib_keyword, lib_no, lib_summary, lib_archive, lib_cate_id, lib_folder_id, lib_show_login_flg, lib_show_login_begin, lib_show_login_end, lib_private_flg, lib_ref_dept_st_flg, lib_ref_dept_flg, lib_ref_st_flg, lib_upd_dept_st_flg, lib_upd_dept_flg, lib_upd_st_flg, approve_label) values (";
		$content = array($wkfw_id, $wkfw_type, pg_escape_string($wkfw_title), pg_escape_string($wkfw_content), $start_date, $end_date, $wkfw_appr, $wkfw_content_type, $wkfw_folder_id, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg, pg_escape_string($short_wkfw_name), $apply_title_disp_flg, $send_mail_flg, $lib_reg_flg, $lib_keyword, $lib_no, $lib_summary, $lib_archive, $lib_cate_id, $lib_folder_id, $lib_show_login_flg, $lib_show_login_begin, $lib_show_login_end, $lib_private_flg, $lib_ref_dept_st_flg, $lib_ref_dept_flg, $lib_ref_st_flg, $lib_upd_dept_st_flg, $lib_upd_dept_flg, $lib_upd_st_flg, $approve_label);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー更新
	function update_wkfwmst($arr, $mode)
	{
		$wkfw_id = $arr["wkfw_id"];
		$wkfw_type = $arr["wkfw_type"];
		$wkfw_title = $arr["wkfw_title"];
		$wkfw_content = $arr["wkfw_content"];
		$start_date = $arr["start_date"];
		$end_date = $arr["end_date"];
		$wkfw_appr = $arr["wkfw_appr"];
		$wkfw_content_type = $arr["wkfw_content_type"];
		$wkfw_folder_id = $arr["wkfw_folder_id"];
		$ref_dept_st_flg = $arr["ref_dept_st_flg"];
		$ref_dept_flg = $arr["ref_dept_flg"];
		$ref_st_flg = $arr["ref_st_flg"];
		$short_wkfw_name = $arr["short_wkfw_name"];
		$apply_title_disp_flg = $arr["apply_title_disp_flg"];
		$send_mail_flg = $arr["send_mail_flg"];
		$lib_reg_flg = $arr["lib_reg_flg"];
		$lib_keyword = $arr["lib_keyword"];
		$lib_no = $arr["lib_no"];
		$lib_summary = $arr["lib_summary"];
		$lib_archive = $arr["lib_archive"];
		$lib_cate_id = $arr["lib_cate_id"];
		$lib_folder_id = $arr["lib_folder_id"];
		$lib_show_login_flg = $arr["lib_show_login_flg"];
		$lib_show_login_begin = $arr["lib_show_login_begin"];
		$lib_show_login_end = $arr["lib_show_login_end"];
		$lib_private_flg = $arr["lib_private_flg"];
		$lib_ref_dept_st_flg = $arr["lib_ref_dept_st_flg"];
		$lib_ref_dept_flg = $arr["lib_ref_dept_flg"];
		$lib_ref_st_flg = $arr["lib_ref_st_flg"];
		$lib_upd_dept_st_flg = $arr["lib_upd_dept_st_flg"];
		$lib_upd_dept_flg = $arr["lib_upd_dept_flg"];
		$lib_upd_st_flg = $arr["lib_upd_st_flg"];
		$approve_label = $arr["approve_label"];

		if($mode == "ALIAS")
		{
			$sql = "update fpluswkfwmst set";
		}
		else
		{
			$sql = "update fpluswkfwmst_real set";
		}
		$set = array("wkfw_type", "wkfw_title", "wkfw_content", "wkfw_start_date", "wkfw_end_date", "wkfw_appr", "wkfw_content_type", "wkfw_folder_id", "ref_dept_st_flg", "ref_dept_flg", "ref_st_flg", "short_wkfw_name", "apply_title_disp_flg", "send_mail_flg", "lib_reg_flg", "lib_keyword", "lib_no", "lib_summary", "lib_archive", "lib_cate_id", "lib_folder_id", "lib_show_login_flg", "lib_show_login_begin", "lib_show_login_end", "lib_private_flg", "lib_ref_dept_st_flg", "lib_ref_dept_flg", "lib_ref_st_flg", "lib_upd_dept_st_flg", "lib_upd_dept_flg", "lib_upd_st_flg", "approve_label");
		$setvalue = array($wkfw_type, pg_escape_string($wkfw_title), pg_escape_string($wkfw_content), $start_date, $end_date, $wkfw_appr, $wkfw_content_type, $wkfw_folder_id, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg, pg_escape_string($short_wkfw_name), $apply_title_disp_flg, $send_mail_flg, $lib_reg_flg, $lib_keyword, $lib_no, $lib_summary, $lib_archive, $lib_cate_id, $lib_folder_id, $lib_show_login_flg, $lib_show_login_begin, $lib_show_login_end, $lib_private_flg, $lib_ref_dept_st_flg, $lib_ref_dept_flg, $lib_ref_st_flg, $lib_upd_dept_st_flg, $lib_upd_dept_flg, $lib_upd_st_flg, $approve_label);
		$cond = "where wkfw_id = $wkfw_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 略称取得
	function get_short_wkfw_name_used_cnt($short_wkfw_name, $wkfw_id)
	{
		$sql  .= "select count(*) as cnt from fpluswkfwmst_real";
		$cond .= "where short_wkfw_name = '$short_wkfw_name'";

		if($wkfw_id != "")
		{
			$cond .= "and wkfw_id <> $wkfw_id";
		}

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cnt");
	}
//-------------------------------------------------------------------------------------------------------------------------
// 申請関連
//-------------------------------------------------------------------------------------------------------------------------

	// 承認者管理取得
	function get_wkfwapvmng($wkfw_id)
	{
		$sql  = "select wkfw_id, apv_order, deci_flg, target_class_div, multi_apv_flg, next_notice_div, apv_div0_flg, apv_div1_flg, apv_div2_flg, apv_div3_flg, apv_div4_flg, apv_num ";
		$sql .= "from fplusapvmng ";
		$cond = "where wkfw_id = $wkfw_id order by apv_order";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_id" => $row["wkfw_id"],
                            "apv_order" => $row["apv_order"],
                            "deci_flg" => $row["deci_flg"],
                            "target_class_div" => $row["target_class_div"],
                            "multi_apv_flg" => $row["multi_apv_flg"],
                            "next_notice_div" => $row["next_notice_div"],
							"apv_div0_flg" => $row["apv_div0_flg"],
							"apv_div1_flg" => $row["apv_div1_flg"],
							"apv_div2_flg" => $row["apv_div2_flg"],
							"apv_div3_flg" => $row["apv_div3_flg"],
							"apv_div4_flg" => $row["apv_div4_flg"],
							"apv_num" => $row["apv_num"]
                           );
		}
		return $arr;
	}

	// 部署役職指定情報取得
	function get_wkfwapvpstdtl($wkfw_id, $apv_order, $target_class_div, $emp_id)
	{
		$arr = array();

		// 申請者の部署役職取得
		$arr_emp_info = $this->get_emp_info($emp_id);

		if($target_class_div == "4")
		{
			$fourth_post_exist_flg = false;
			foreach($arr_emp_info as $emp_info)
			{
				$emp_room  = $emp_info["emp_room"];
				if($emp_room != "")
				{
					$fourth_post_exist_flg = true;
					break;
				}
			}

			if(!$fourth_post_exist_flg)
			{
				return $arr;
			}
		}

		$sql  = "select ";
		$sql .= "emp.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, empmst.emp_class, empmst.emp_attribute, empmst.emp_dept, empmst.emp_room, empmst.emp_st, stmst.st_nm ";
		$sql .= "from ";
    	$sql .= "(";
    	$sql .= "select emp_id from empmst where ";

		// 「部署指定しない」以外
		if($target_class_div != 0)
		{
			$sql .= "( ";
			$idx = 0;
			foreach($arr_emp_info as $emp_info)
			{
				$emp_class     = $emp_info["emp_class"];
				$emp_attribute = $emp_info["emp_attribute"];
				$emp_dept      = $emp_info["emp_dept"];
				$emp_room      = $emp_info["emp_room"];

				if($target_class_div == "4")
				{
					if($emp_room == "")
					{
						continue;
					}
				}

				if($idx > 0)
				{
					$sql .= "or ";
				}

				if($target_class_div == "1")
				{
					$sql .= "emp_class = $emp_class ";
				}
				else if($target_class_div == "2")
				{
					$sql .= "emp_attribute = $emp_attribute ";
				}
				else if($target_class_div == "3")
				{
					$sql .= "emp_dept = $emp_dept ";
				}
				else if($target_class_div == "4")
				{
					$sql .= "emp_room = $emp_room ";
				}
				$idx++;
			}
			$sql .= ") ";
			$sql .= "and ";
		}
		$sql .= "exists (select st_id from fplusapvpstdtl where empmst.emp_st = fplusapvpstdtl.st_id and wkfw_id = $wkfw_id and apv_order = $apv_order and st_div = 0) ";
		$sql .= "union ";
		$sql .= "select emp_id from concurrent where ";

		// 「部署指定しない」以外
		if($target_class_div != 0)
		{
			$sql .= "( ";
			$idx = 0;
			foreach($arr_emp_info as $emp_info)
			{
				$emp_class     = $emp_info["emp_class"];
				$emp_attribute = $emp_info["emp_attribute"];
				$emp_dept      = $emp_info["emp_dept"];
				$emp_room      = $emp_info["emp_room"];

				if($target_class_div == "4")
				{
					if($emp_room == "")
					{
						continue;
					}
				}

				if($idx > 0)
				{
					$sql .= "or ";
				}

				if($target_class_div == "1")
				{
					$sql .= "emp_class = $emp_class ";
				}
				else if($target_class_div == "2")
				{
					$sql .= "emp_attribute = $emp_attribute ";
				}
				else if($target_class_div == "3")
				{
					$sql .= "emp_dept = $emp_dept ";
				}
				else if($target_class_div == "4")
				{
					$sql .= "emp_room = $emp_room ";
				}
				$idx++;
			}
			$sql .= ") ";
			$sql .= "and ";
		}
		$sql .= "exists (select st_id from fplusapvpstdtl where concurrent.emp_st = fplusapvpstdtl.st_id and wkfw_id = $wkfw_id and apv_order = $apv_order and st_div = 0) ";
		$sql .= ") emp ";
		$sql .= "inner join empmst on emp.emp_id = empmst.emp_id ";
		$sql .= "inner join authmst on emp.emp_id = authmst.emp_id and not authmst.emp_del_flg ";
		$sql .= "inner join stmst on empmst.emp_st = stmst.st_id and not stmst.st_del_flg ";
		$cond = "order by emp.emp_id asc ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$idx = 1;
		while($row = pg_fetch_array($sel))
		{
			$emp_full_nm = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
			$arr[] = array("emp_id" => $row["emp_id"],
			                "emp_full_nm" => $emp_full_nm,
			                "st_nm" => $row["st_nm"],
   			                "apv_sub_order" => $idx
			               );
			$idx++;
		}
		return $arr;
	}

	// 職員指定情報取得
	function get_wkfwapvdtl($wkfw_id, $apv_order)
	{
		$sql   = "select a.emp_id, a.apv_sub_order, b.emp_lt_nm, b.emp_ft_nm, d.st_nm from fplusapvdtl a ";
		$sql  .= "inner join empmst b on a.emp_id = b.emp_id ";
		$sql  .= "inner join authmst c on a.emp_id = c.emp_id ";
		$sql  .= "and c.emp_del_flg = 'f' ";
		$sql  .= "inner join stmst d on b.emp_st = d.st_id ";
		$cond  = "where a.wkfw_id = $wkfw_id and a.apv_order = $apv_order ";
		$cond .= "order by apv_sub_order asc";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$emp_full_nm = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
			$arr[] = array("emp_id" => $row["emp_id"],
			                "emp_full_nm" => $emp_full_nm,
			                "st_nm" => $row["st_nm"],
			                "apv_sub_order" => $row["apv_sub_order"]
			               );
		}
		return $arr;
	}

	// 委員会・ＷＧ指定情報取得
	function get_wkfwpjtdtl($wkfw_id, $apv_order)
	{
		$sql  = "select parent_pjt_id, child_pjt_id from fpluswkfwpjtdtl ";
		$cond = "where wkfw_id = $wkfw_id and apv_order = $apv_order";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$parent_pjt_id = pg_fetch_result($sel, 0, "parent_pjt_id");
		$child_pjt_id  = pg_fetch_result($sel, 0, "child_pjt_id");

		return array("parent_pjt_id" => $parent_pjt_id, "child_pjt_id" => $child_pjt_id);

	}



	// 委員会・ＷＧメンバー取得
	function get_project_member($parent_pjt_id, $child_pjt_id)
	{
		$sql  = "select emp.project_no, emp.pjt_response as emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, stmst.st_nm ";
		$sql .= "from ";
		$sql .= "( ";

		$sql .= "select varchar(1) '1' as project_no, project.pjt_id, project.pjt_response ";
		$sql .= "from project ";
		$sql .= "where project.pjt_public_flag and not project.pjt_delete_flag and project.pjt_parent_id is null and project.pjt_id = $parent_pjt_id ";

	 	if($child_pjt_id != "")
		{
			$sql .= "union all ";
			$sql .= "select varchar(1) '3' as project_no, project.pjt_id, project.pjt_response ";
			$sql .= "from project ";
			$sql .= "where project.pjt_public_flag and not project.pjt_delete_flag and project.pjt_parent_id = $parent_pjt_id and project.pjt_id = $child_pjt_id ";
		}

		$sql .= ") emp ";


		$sql .= "inner join empmst on emp.pjt_response = empmst.emp_id ";
		$sql .= "inner join authmst on emp.pjt_response = authmst.emp_id and not emp_del_flg ";
		$sql .= "inner join stmst on empmst.emp_st = stmst.st_id and not stmst.st_del_flg ";

		$sql .= "union all ";

		$sql .= "select emp.project_no, emp.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, stmst.st_nm ";
		$sql .= "from ";
		$sql .= "( ";

		$sql .= "select varchar(1) '2' as project_no, project.pjt_id, promember.emp_id, promember.pjt_member_id ";
		$sql .= "from project ";
		$sql .= "inner join promember on project.pjt_id = promember.pjt_id ";
		$sql .= "where project.pjt_public_flag and not project.pjt_delete_flag and project.pjt_parent_id is null and project.pjt_id = $parent_pjt_id ";

	 	if($child_pjt_id != "")
		{
			$sql .= "union all ";
			$sql .= "select varchar(1) '4' as project_no, project.pjt_id, promember.emp_id, promember.pjt_member_id ";
			$sql .= "from project ";
			$sql .= "inner join promember on project.pjt_id = promember.pjt_id ";
			$sql .= "where project.pjt_public_flag and not project.pjt_delete_flag and project.pjt_parent_id = $parent_pjt_id and project.pjt_id = $child_pjt_id ";
		}

		$sql .= ") emp ";

		$sql .= "inner join empmst on emp.emp_id = empmst.emp_id ";
		$sql .= "inner join authmst on emp.emp_id = authmst.emp_id and not emp_del_flg ";
		$sql .= "inner join stmst on empmst.emp_st = stmst.st_id and not stmst.st_del_flg ";
		$cond .= "order by project_no ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		$idx = 1;
		while($row = pg_fetch_array($sel))
		{
			$emp_id      = $row["emp_id"];
			$emp_full_nm = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
			$st_nm       = $row["st_nm"];

			$dpl_flg = false;

			// 重複があればセットしない。
			foreach($arr as $arr_apv)
			{
				if($emp_id == $arr_apv["emp_id"])
				{
					$dpl_flg = true;
				}
			}

			if(!$dpl_flg)
			{
				$arr[] = array("emp_id" => $row["emp_id"],
				                "emp_full_nm" => $emp_full_nm,
				                "st_nm" => $row["st_nm"],
				                "apv_sub_order" => $idx
				               );
				$idx++;
			}
		}
		return $arr;
	}


	// 申請者以外の結果通知者取得
	function get_wkfw_notice_for_apply($wkfw_id,
	                                    $notice_target_class_div,
                                        $rslt_ntc_div0_flg,
                                        $rslt_ntc_div1_flg,
                                        $rslt_ntc_div2_flg,
                                        $rslt_ntc_div3_flg,
                                        $rslt_ntc_div4_flg,
	                                    $emp_id)
	{
		$arr_tmp = array();

		// 部署役職指定(申請書所属)
		if($rslt_ntc_div0_flg == "t")
		{
			$arr_wkfwnoticestdtl = $this->get_wkfwnoticestdtl($wkfw_id, "0", "ALIAS");
			$notice_st_id = "";
			foreach($arr_wkfwnoticestdtl as $wkfwnoticestdtl)
			{
				if($notice_st_id != "")
				{
					$notice_st_id .= ",";
				}
				$notice_st_id .= $wkfwnoticestdtl["st_id"];
			}
			$arr_apvpstdtl = $this->get_apvpstdtl_for_wkfwpreview($notice_target_class_div, $notice_st_id, $emp_id);

			for($i=0; $i<count($arr_apvpstdtl); $i++)
			{
				$arr_apvpstdtl[$i]["rslt_ntc_div"] = "0";
			}

			// マージ処理
			$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $arr_apvpstdtl);
		}

		// 部署役職指定(部署指定)
		if($rslt_ntc_div4_flg == "t")
		{
			$arrr_wkfwnoticesectdtl = $this->get_wkfwnoticesectdtl($wkfw_id, "ALIAS");
			$arr_wkfwnoticestdtl = $this->get_wkfwnoticestdtl($wkfw_id, "4", "ALIAS");
			$notice_st_id = "";
			foreach($arr_wkfwnoticestdtl as $wkfwnoticestdtl)
			{
				if($notice_st_id != "")
				{
					$notice_st_id .= ",";
				}
				$notice_st_id .= $wkfwnoticestdtl["st_id"];
			}
			$arr_post_sect = $this->get_emp_info_for_post_sect($arrr_wkfwnoticesectdtl["class_id"],
			                                        $arrr_wkfwnoticesectdtl["atrb_id"],
			                                        $arrr_wkfwnoticesectdtl["dept_id"],
			                                        $arrr_wkfwnoticesectdtl["room_id"],
			                                        $notice_st_id);

			for($i=0; $i<count($arr_post_sect); $i++)
			{
				$arr_post_sect[$i]["rslt_ntc_div"] = "4";
			}

			// マージ処理
			$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $arr_post_sect);
		}

		// 職員指定
		if($rslt_ntc_div1_flg == "t")
		{
			$arr_emp_info = array();
			$arr_wkfwnoticedtl = $this->get_wkfwnoticedtl($wkfw_id, "ALIAS");
			foreach($arr_wkfwnoticedtl as $wkfwnoticedtl)
			{
				$arr_empmst_detail = $this->get_empmst_detail($wkfwnoticedtl["emp_id"]);
				$emp_lt_nm = $arr_empmst_detail[0]["emp_lt_nm"];
				$emp_ft_nm = $arr_empmst_detail[0]["emp_ft_nm"];
                $emp_full_nm = $emp_lt_nm." ".$emp_ft_nm;
				$arr_emp_info[] = array(
								 "emp_id" => $arr_empmst_detail[0]["emp_id"],
								 "emp_full_nm" => $emp_full_nm,
								 "st_nm" => $arr_empmst_detail[0]["st_nm"]
								 );
			}

			for($i=0; $i<count($arr_emp_info); $i++)
			{
				$arr_emp_info[$i]["rslt_ntc_div"] = "1";
			}

			// マージ処理
			$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $arr_emp_info);
		}

		// 委員会・ＷＧ指定
		if($rslt_ntc_div3_flg == "t")
		{
			$arr_wkfwnoticepjtdtl = $this->get_wkfwnoticepjtdtl($wkfw_id, "ALIAS");
			$arr_project_member = $this->get_project_member($arr_wkfwnoticepjtdtl[0]["pjt_parent_id"], $arr_wkfwnoticepjtdtl[0]["pjt_child_id"]);

			for($i=0; $i<count($arr_project_member); $i++)
			{
				$arr_project_member[$i]["rslt_ntc_div"] = "3";
			}

			// マージ処理
			$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $arr_project_member);
		}

		// 申請者を除去する
		$arr = array();
		foreach($arr_tmp as $tmp)
		{
			$tmp_emp_id = $tmp["emp_id"];
			if($tmp_emp_id == $emp_id)
			{
				continue;
			}
			$arr[] = $tmp;
		}

		return $arr;
	}



	// 承認者情報取得
	function get_wkfwapv_info($wkfw_id, $emp_id)
	{
		$arr_wkfwapvmng = $this->get_wkfwapvmng($wkfw_id);

		$arr = array();
		foreach($arr_wkfwapvmng as $apvmng)
		{
			$apvmng["apv_setting_flg"] = "f";

			$arr_apv = array();
			$multi_apv_flg = $apvmng["multi_apv_flg"];

			$apv_div0_flg  = $apvmng["apv_div0_flg"];
			$apv_div1_flg  = $apvmng["apv_div1_flg"];
			$apv_div2_flg  = $apvmng["apv_div2_flg"];
			$apv_div3_flg  = $apvmng["apv_div3_flg"];
			$apv_div4_flg  = $apvmng["apv_div4_flg"];

			if($apv_div0_flg == "f" && $apv_div1_flg == "f" && $apv_div2_flg == "t" && $apv_div3_flg == "f" && $apv_div4_flg == "f")
			{
				$apvmng["apv_setting_flg"] = "t";
			}

			// 部署役職(申請者所属)指定
			if($apv_div0_flg == "t")
			{
				$arr_wkfwapvpstdtl = $this->get_wkfwapvpstdtl($wkfw_id, $apvmng["apv_order"], $apvmng["target_class_div"], $emp_id);

				for($i=0; $i<count($arr_wkfwapvpstdtl); $i++)
				{
					$arr_wkfwapvpstdtl[$i]["apv_div"] = "0";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_wkfwapvpstdtl);

			}
			// 部署役職(部署指定)指定
			if($apv_div4_flg == "t")
			{

  				$arr_apvpstdtl = $this->get_apvpstdtl($wkfw_id, $apvmng["apv_order"], "4");
				$st_sect_ids = "";
				foreach($arr_apvpstdtl as $apvpstdtl)
				{
					if($st_sect_ids != "")
					{
						$st_sect_ids .= ",";
					}
					$st_sect_ids .= $apvpstdtl["st_id"];
				}
				$arr_sect_dtl = $this->get_apvsectdtl($wkfw_id, $apvmng["apv_order"]);
				$arr_post_sect = $this->get_emp_info_for_post_sect($arr_sect_dtl["class_id"], $arr_sect_dtl["atrb_id"], $arr_sect_dtl["dept_id"], $arr_sect_dtl["room_id"], $st_sect_ids);

				for($i=0; $i<count($arr_post_sect); $i++)
				{
					$arr_post_sect[$i]["apv_div"] = "4";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_post_sect);
			}
			// 職員指定
			if($apv_div1_flg == "t")
			{
				$arr_wkfwapvdtl = $this->get_wkfwapvdtl($wkfw_id, $apvmng["apv_order"]);
				for($i=0; $i<count($arr_wkfwapvdtl); $i++)
				{
					$arr_wkfwapvdtl[$i]["apv_div"] = "1";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_wkfwapvdtl);
			}
			// 委員会・ＷＧ
			if($apv_div3_flg == "t")
			{
				$arr_pjt = $this->get_wkfwpjtdtl($wkfw_id, $apvmng["apv_order"]);
				$arr_project_member = $this->get_project_member($arr_pjt["parent_pjt_id"], $arr_pjt["child_pjt_id"]);

				$pjt_nm = $this->get_pjt_nm($arr_pjt["parent_pjt_id"]);
				if($arr_pjt["child_pjt_id"] != "")
				{
					$pjt_nm .= " > ";
					$pjt_nm .= $this->get_pjt_nm($arr_pjt["child_pjt_id"]);
				}

				for($i=0; $i<count($arr_project_member); $i++)
				{
					$arr_project_member[$i]["apv_div"] = "3";
					$arr_project_member[$i]["pjt_nm"] = $pjt_nm;
					$arr_project_member[$i]["parent_pjt_id"] = $arr_pjt["parent_pjt_id"];
					$arr_project_member[$i]["child_pjt_id"]  = $arr_pjt["child_pjt_id"];
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_project_member);
			}
			// その他
			if($apv_div2_flg == "t")
			{
				$arr_setting_apv = "";
				for($i=0; $i<$apvmng["apv_num"]; $i++)
				{
					$arr_setting_apv[] = array();
				}

				for($i=0; $i<count($arr_setting_apv); $i++)
				{
					$arr_setting_apv[$i]["apv_div"] = "2";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_setting_apv);

			}

			// 複数承認者「許可する」
			if($multi_apv_flg == "t")
			{
				if(count($arr_apv) > 0)
				{
					if(count($arr_apv) > 1)
					{
						$apv_sub_order = 1;
						foreach($arr_apv as $apv)
						{
							$arr_emp = array();
							$apvmng["apv_sub_order"] = $apv_sub_order;
							$arr_emp[] = array(
                                               "emp_id" => $apv["emp_id"],
                                               "emp_full_nm" => $apv["emp_full_nm"],
                                               "st_nm" => $apv["st_nm"],
                                               "apv_div" => $apv["apv_div"],
                                               "pjt_nm" => $apv["pjt_nm"],
                                               "parent_pjt_id" => $apv["parent_pjt_id"],
                                               "child_pjt_id" => $apv["child_pjt_id"]
                                               );
							$apvmng["emp_infos"] = $arr_emp;

							if($apv["apv_div"] == "2")
							{
								$apvmng["apv_setting_flg"] = "t";
							}

							$arr[] = $apvmng;
							$apv_sub_order++;
						}
					}
					else
					{
						$arr_emp = array();
						$apvmng["apv_sub_order"] = "";
						$arr_emp[] = array(
                                           "emp_id" => $arr_apv[0]["emp_id"],
                                           "emp_full_nm" => $arr_apv[0]["emp_full_nm"],
                                           "st_nm" => $arr_apv[0]["st_nm"],
                                           "apv_div" => $arr_apv[0]["apv_div"],
                                           "pjt_nm" => $arr_apv[0]["pjt_nm"],
                                           "parent_pjt_id" => $arr_apv[0]["parent_pjt_id"],
                                           "child_pjt_id" => $arr_apv[0]["child_pjt_id"]
                                           );
						$apvmng["emp_infos"] = $arr_emp;

						if($apv["apv_div"] == "2")
						{
							$apvmng["apv_setting_flg"] = "t";
						}

						$arr[] = $apvmng;
					}
				}
				else
				{
					$apvmng["emp_infos"] = array();
					$arr[] = $apvmng;
				}
			}
			// 複数承認者「許可しない」
			else
			{
				$apvmng["apv_sub_order"] = "";
				$apvmng["emp_infos"] = $arr_apv;
				$arr[] = $apvmng;
			}
		}

		return $arr;
	}

	// ワークフロー情報取得
	function get_wkfwmst($wkfw_id)
	{
		$sql  = "select * from fpluswkfwmst";
		$cond ="where wkfw_id = $wkfw_id";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_all($sel);
	}

	// フォーマットファイル情報取得
	function get_wkfwfile($wkfw_id)
	{
		$sql  = "select wkfwfile_no, wkfwfile_name from fpluswkfwfile";
		$cond = "where wkfw_id = $wkfw_id order by wkfwfile_no";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfwfile_no" => $row["wkfwfile_no"], "wkfwfile_name" => $row["wkfwfile_name"]);
		}
		return $arr;
	}

	// 前提とする申請書情報取得
	function get_wkfwfprecond($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql  = "select a.precond_wkfw_id, b.wkfw_title from fplusfprecond a ";
		}
		else
		{
			$sql  = "select a.precond_wkfw_id, b.wkfw_title from fplusfprecond_real a ";
		}

		$sql .= "inner join fpluswkfwmst b on a.precond_wkfw_id = b.wkfw_id ";
		$cond = "where a.wkfw_id = $wkfw_id order by a.precond_order asc ";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_all($sel);
	}

	// 承認確定申請情報取得
	function get_approved_apply_list($session, $wkfw_id, $page, $max_page)
	{
		$arr_empmst = $this->get_empmst($session);
		$emp_id = $arr_empmst[0]["emp_id"];

		$page = ($page - 1) * 15;

		$sql   = "select ";
		$sql  .= "a.*, ";
		$sql  .= "b.wkfw_title, b.short_wkfw_name ";
		$sql  .= "from fplusapply a ";
		$sql  .= "inner join fpluswkfwmst b on a.wkfw_id = b.wkfw_id ";
		$cond .= "where not a.delete_flg and a.apply_stat = '1' and a.emp_id = '$emp_id' and b.wkfw_id = $wkfw_id ";
		$cond .= "order by a.apply_date desc, a.apply_no desc ";
		$cond .= "offset $page limit $max_page ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_all($sel);
	}

	// 承認確定申請件数情報取得
	function get_approved_apply_list_count($session, $wkfw_id)
	{
		$arr_empmst = $this->get_empmst($session);
		$emp_id = $arr_empmst[0]["emp_id"];

		$sql   = "select ";
		$sql  .= "count(a.apply_id) as cnt ";
		$sql  .= "from fplusapply a ";
		$sql  .= "inner join fpluswkfwmst b on a.wkfw_id = b.wkfw_id ";
		$cond .= "where not a.delete_flg and a.apply_stat = '1' and a.emp_id = '$emp_id' and b.wkfw_id = $wkfw_id ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cnt");
	}




	// エピネット報告書（Ａ針刺し・切創）登録
	function regist_Episys107A($set_key,$set_data)
	{

		$sql=	"insert into fplus_epi_a (";
		$sql .=	 $set_key[0];
//		$sql .=	"	apply_id  ";


		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			$sql .=	 ",".$set_key[$i];
		}
		$sql .=	") values (";

		//登録するデータを設定
		$content = $set_data;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

	}



	// エピネット報告書（B.皮膚・粘膜汚染報告書）登録
	function regist_Episys107B($set_key,$set_data)
	{

		$sql=	"insert into fplus_epi_b (";
		$sql .=	 $set_key[0];
		//		$sql .=	"	apply_id  ";


		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			$sql .=	 ",".$set_key[$i];
		}
		$sql .=	") values (";

		//登録するデータを設定
		$content = $set_data;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

	}

        // エピネット報告書（ＡO針刺し・切創:手術）登録
	function regist_Episys107AO($set_key,$set_data)
	{

		$sql=	"insert into fplus_epi_ao (";
		$sql .=	 $set_key[0];
//		$sql .=	"	apply_id  ";


		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			$sql .=	 ",".$set_key[$i];
		}
		$sql .=	") values (";

		//登録するデータを設定
		$content = $set_data;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

	}

        // エピネット報告書（BO皮膚・粘膜汚染報告書:手術）登録
	function regist_Episys107BO($set_key,$set_data)
	{

		$sql=	"insert into fplus_epi_bo (";
		$sql .=	 $set_key[0];
//		$sql .=	"	apply_id  ";


		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			$sql .=	 ",".$set_key[$i];
		}
		$sql .=	") values (";

		//登録するデータを設定
		$content = $set_data;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

	}

        
        
        
        
        
        
	// 感染症報告書（MRSA）登録
	function regist_MRSA($set_key,$set_data)
	{

		$sql=	"insert into fplus_infection_mrsa (";
		$sql .=	 $set_key[0];
		//		$sql .=	"	apply_id  ";


		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			$sql .=	 ",".$set_key[$i];
		}
		$sql .=	") values (";

		//登録するデータを設定
		$content = $set_data;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

	}

	//医療事故関係報告用登録
	// 緊急報告受付
	function regist_immediate_repo($set_key,$set_data)
	{
		$sql=	"insert into fplus_immediate_report (";
		$sql .=	 $set_key[0];

		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			$sql .=	 ",".$set_key[$i];
		}
		$sql .=	") values (";

		//登録するデータを設定
		$content = $set_data;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
	// 第一報
	function regist_first_repo($set_key,$set_data)
	{

		$sql=	"insert into fplus_first_report (";
		$sql .=	 $set_key[0];
//		$sql .=	"	apply_id  ";


		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			$sql .=	 ",".$set_key[$i];
		}
		$sql .=	") values (";

		//登録するデータを設定
		$content = $set_data;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

	}
	// 様式２
	function regist_second_repo($set_key,$set_data)
	{

		$sql=	"insert into fplus_second_report (";
		$sql .=	 $set_key[0];
//		$sql .=	"	apply_id  ";


		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			$sql .=	 ",".$set_key[$i];
		}
		$sql .=	") values (";

		//登録するデータを設定
		$content = $set_data;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

	}
	// コンサルテーション依頼書登録
	function regist_EpisysConsreq($set_key,$set_data)
	{

		$sql=	"insert into fplus_consultation_request (";
		$sql .=	 $set_key[0];
//		$sql .=	"	apply_id  ";


		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			$sql .=	 ",".$set_key[$i];
		}
		$sql .=	") values (";

		//登録するデータを設定
		$content = $set_data;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

	}
	// コンサルテーション報告書登録
	function regist_EpisysConsrep($set_key,$set_data)
	{

		$sql=	"insert into fplus_consultation_report (";
		$sql .=	 $set_key[0];
//		$sql .=	"	apply_id  ";


		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			$sql .=	 ",".$set_key[$i];
		}
		$sql .=	") values (";

		//登録するデータを設定
		$content = $set_data;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

	}
	// リンクテーブル登録
	function regist_link_table($set_key,$set_data)
	{
		$sql=	"insert into fplus_medical_accident_report_management (";
		$sql .=	 $set_key[0];

		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			$sql .=	 ",".$set_key[$i];
		}
		$sql .=	") values (";

		//登録するデータを設定
		$content = $set_data;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 医療機器不具合報告登録
	function regist_medical_apparatus_fault_report($set_key,$set_data)
	{

		$sql=	"insert into fplus_medical_apparatus_fault_report (";
		$sql .=	 $set_key[0];
//		$sql .=	"	apply_id  ";


		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			$sql .=	 ",".$set_key[$i];
		}
		$sql .=	") values (";

		//登録するデータを設定
		$content = $set_data;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

	}

	// レベル別発生件数データ登録（ヒヤリハット件数表）
	function regist_number_of_occurrences_level($set_key,$set_data)
	{

		$sql=	"insert into fplus_number_of_occurrences_level (";
		$sql .=	 $set_key[0];
//		$sql .=	"	apply_id  ";


		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			$sql .=	 ",".$set_key[$i];
		}
		$sql .=	") values (";

		//登録するデータを設定
		$content = $set_data;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

	}

	// 職種別発生件数データ登録（ヒヤリハット件数表）
	function regist_number_of_occurrences($set_key,$set_data)
	{

		$sql=	"insert into fplus_number_of_occurrences (";
		$sql .=	 $set_key[0];
//		$sql .=	"	apply_id  ";


		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			$sql .=	 ",".$set_key[$i];
		}
		$sql .=	") values (";

		//登録するデータを設定
		$content = $set_data;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

	}

	// 転倒・転落発生件数データ登録（ヒヤリハット件数表）
	function regist_number_of_occurrences_fall_accident($set_key,$set_data)
	{

		$sql=	"insert into fplus_number_of_occurrences_fall_accident (";
		$sql .=	 $set_key[0];
//		$sql .=	"	apply_id  ";


		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			$sql .=	 ",".$set_key[$i];
		}
		$sql .=	") values (";

		//登録するデータを設定
		$content = $set_data;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

	}

	function regist_medical_accident_info($set_key,$set_data)
	{
		$sql=	"insert into fplus_medical_accident_info (";
		$sql .=	 $set_key[0];

		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			$sql .=	 ",".$set_key[$i];
		}
		$sql .=	") values (";

		//登録するデータを設定
		$content = $set_data;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 申請登録
	function regist_apply($arr)
	{
		$apply_id          = $arr["apply_id"];
		$wkfw_id           = $arr["wkfw_id"];
		$apply_content     = $arr["apply_content"];
		$emp_id            = $arr["emp_id"];
		$apply_stat        = $arr["apply_stat"];
		$apply_date        = $arr["apply_date"];
		$delete_flg        = $arr["delete_flg"];
		$apply_title       = $arr["apply_title"];
		$re_apply_id       = $arr["re_apply_id"];
		$apv_fix_show_flg  = $arr["apv_fix_show_flg"];
		$apv_bak_show_flg  = $arr["apv_bak_show_flg"];
		$emp_class         = $arr["emp_class"];
		$emp_attribute     = $arr["emp_attribute"];
		$emp_dept          = $arr["emp_dept"];
		$apv_ng_show_flg   = $arr["apv_ng_show_flg"];
		$emp_room          = $arr["emp_room"];
		$draft_flg         = $arr["draft_flg"];
		$wkfw_appr         = $arr["wkfw_appr"];
		$wkfw_content_type = $arr["wkfw_content_type"];
		$apply_title_disp_flg = $arr["apply_title_disp_flg"];
		$apply_no          = $arr["apply_no"];
        $notice_sel_flg    = $arr["notice_sel_flg"];
        $wkfw_history_no   = $arr["wkfw_history_no"];
		$wkfwfile_history_no = $arr["wkfwfile_history_no"];

		$sql  = "insert into fplusapply ";
		$sql .= "(apply_id, ";
		$sql .= "wkfw_id, ";
		$sql .= "apply_content, ";
		$sql .= "emp_id, ";
		$sql .= "apply_stat, ";
		$sql .= "apply_date, ";
		$sql .= "delete_flg, ";
		$sql .= "apply_title, ";
		$sql .= "re_apply_id, ";
		$sql .= "apv_fix_show_flg, ";
		$sql .= "apv_bak_show_flg, ";
		$sql .= "emp_class, ";
		$sql .= "emp_attribute, ";
		$sql .= "emp_dept, ";
		$sql .= "apv_ng_show_flg, ";
		$sql .= "emp_room, ";
		$sql .= "draft_flg, ";
		$sql .= "wkfw_appr, ";
		$sql .= "wkfw_content_type, ";
		$sql .= "apply_title_disp_flg, ";
		$sql .= "apply_no, ";
		$sql .= "notice_sel_flg, ";
		$sql .= "wkfw_history_no, ";
		$sql .= "wkfwfile_history_no) ";
		$sql .= "values (";

		$content = array(
							$apply_id,
							$wkfw_id,
							pg_escape_string($apply_content),
							$emp_id,
							$apply_stat,
							$apply_date,
							$delete_flg,
							pg_escape_string($apply_title),
							$re_apply_id,
							$apv_fix_show_flg,
							$apv_bak_show_flg,
							$emp_class,
							$emp_attribute,
							$emp_dept,
							$apv_ng_show_flg,
							$emp_room,
							$draft_flg,
							$wkfw_appr,
							$wkfw_content_type,
							$apply_title_disp_flg,
							$apply_no,
							$notice_sel_flg,
							$wkfw_history_no,
							$wkfwfile_history_no
						);


		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 承認登録
	function regist_applyapv($arr)
	{
		$wkfw_id          = $arr["wkfw_id"];
		$apply_id         = $arr["apply_id"];
		$apv_order        = $arr["apv_order"];
		$emp_id           = $arr["emp_id"];
		$apv_stat         = $arr["apv_stat"];
		$apv_date         = $arr["apv_date"];
		$delete_flg       = $arr["delete_flg"];
		$apv_comment      = $arr["apv_comment"];
		$st_div           = $arr["st_div"];
		$deci_flg         = $arr["deci_flg"];
		$emp_class        = $arr["emp_class"];
		$emp_attribute    = $arr["emp_attribute"];
		$emp_dept         = $arr["emp_dept"];
		$emp_st           = $arr["emp_st"];
		$apv_fix_show_flg = $arr["apv_fix_show_flg"];
		$emp_room         = $arr["emp_room"];
		$apv_sub_order    = $arr["apv_sub_order"];
		$multi_apv_flg    = $arr["multi_apv_flg"];
		$next_notice_div  = $arr["next_notice_div"];
		$parent_pjt_id    = $arr["parent_pjt_id"];
		$child_pjt_id     = $arr["child_pjt_id"];
		$other_apv_flg    = $arr["other_apv_flg"];

		$sql  = "insert into fplusapplyapv ";
		$sql .= "(wkfw_id, ";
		$sql .= "apply_id, ";
		$sql .= "apv_order, ";
		$sql .= "emp_id, ";
		$sql .= "apv_stat, ";
		$sql .= "apv_date, ";
		$sql .= "delete_flg, ";
		$sql .= "apv_comment, ";
		$sql .= "st_div, ";
		$sql .= "deci_flg, ";
		$sql .= "emp_class, ";
		$sql .= "emp_attribute, ";
		$sql .= "emp_dept, ";
		$sql .= "emp_st, ";
		$sql .= "apv_fix_show_flg, ";
		$sql .= "emp_room, ";
		$sql .= "apv_sub_order, ";
		$sql .= "multi_apv_flg, ";
		$sql .= "next_notice_div, ";
		$sql .= "parent_pjt_id, ";
		$sql .= "child_pjt_id, ";
		$sql .= "other_apv_flg) ";

		$sql .= "values (";

		$content = array(
							$wkfw_id,
							$apply_id,
							$apv_order,
							$emp_id,
							$apv_stat,
							$apv_date,
							$delete_flg,
							$apv_comment,
							$st_div,
							$deci_flg,
							$emp_class,
							$emp_attribute,
							$emp_dept,
							$emp_st,
							$apv_fix_show_flg,
							$emp_room,
							$apv_sub_order,
							$multi_apv_flg,
							$next_notice_div,
							$parent_pjt_id,
							$child_pjt_id,
							$other_apv_flg
						);

		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 承認者候補登録
	function regist_applyapvemp($apply_id, $apv_order, $person_order, $emp_id, $st_div, $parent_pjt_id, $child_pjt_id)
	{
		$sql = "insert into fplusapplyapvemp (apply_id, apv_order, person_order, emp_id, delete_flg, st_div, parent_pjt_id, child_pjt_id) values (";
		$content = array($apply_id, $apv_order, $person_order, $emp_id, "f", $st_div, $parent_pjt_id, $child_pjt_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 添付ファイル登録
	function regist_applyfile($apply_id, $applyfile_no, $applyfile_name)
	{
		$sql = "insert into fplusapplyfile (apply_id, applyfile_no, applyfile_name, delete_flg) values (";
		$content = array($apply_id, $applyfile_no, $applyfile_name, "f");
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 前提とする申請書(申請用)登録
	function regist_applyprecond($apply_id, $precond_wkfw_id, $precond_order, $precond_apply_id)
	{

		$precond_apply_id = ($precond_apply_id == "") ? null : $precond_apply_id;

		$sql = "insert into fplusapplyprecond (apply_id, precond_wkfw_id, precond_order, precond_apply_id, delete_flg) values (";
		$content = array($apply_id, $precond_wkfw_id, $precond_order, $precond_apply_id, "f");
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 申請結果通知登録
	function regist_applynotice($apply_id, $recv_emp_id, $rslt_ntc_div)
	{
		$sql = "insert into fplusapplynotice (apply_id, recv_emp_id, confirmed_flg, send_emp_id, send_date, delete_flg, rslt_ntc_div) values (";
		$content = array($apply_id, $recv_emp_id, "f", null, null, "f", $rslt_ntc_div);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 年度の申請件数取得
	function get_apply_cnt_per_year($year)
	{
		$this_ymd = $year."0401";
		$next_year = $year + 1;
		$next_ymd = $next_year."0331";

		$sql  = "select count(*) as cnt from fplusapply ";
		$cond = "where substring(apply_date from 1 for 8) between '$this_ymd' and '$next_ymd' and not draft_flg";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0, "cnt");
	}

    function delete_applyapv($apply_id)
    {
		$sql = "delete from fplusapplyapv";
		$cond = "where apply_id = $apply_id and (emp_id is null or emp_id = '')";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
    }
//-------------------------------------------------------------------------------------------------------------------------
// 下書き・更新関連
//-------------------------------------------------------------------------------------------------------------------------

	// 申請・ワークフロー情報取得
	function get_apply_wkfwmst($apply_id)
	{
		$sql  = "select fplusapply.*, empmst.emp_lt_nm, empmst.emp_ft_nm, ";
		$sql .= "fpluswkfwmst.wkfw_title, fpluswkfwmst.wkfw_folder_id, fpluscatemst.wkfw_nm, fpluswkfwmst.wkfw_content, short_wkfw_name, fpluswkfwmst.approve_label, ";
		$sql .= "classmst.class_nm as apply_class_nm, atrbmst.atrb_nm as apply_atrb_nm, deptmst.dept_nm as apply_dept_nm, classroom.room_nm as apply_room_nm ";
		$sql .= "from fplusapply ";
		$sql .= "inner join fpluswkfwmst on fplusapply.wkfw_id = fpluswkfwmst.wkfw_id ";
		$sql .= "inner join fpluscatemst on fpluswkfwmst.wkfw_type = fpluscatemst.wkfw_type ";
		$sql .= "inner join empmst on fplusapply.emp_id = empmst.emp_id ";
		$sql .= "left join classmst on fplusapply.emp_class = classmst.class_id ";
		$sql .= "left join atrbmst on fplusapply.emp_attribute = atrbmst.atrb_id ";
		$sql .= "left join deptmst on fplusapply.emp_dept = deptmst.dept_id ";
		$sql .= "left join classroom on fplusapply.emp_room = classroom.room_id ";
		$cond = "where fplusapply.apply_id = $apply_id ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_all($sel);
	}


	// 添付ファイル情報取得
	function get_applyfile($apply_id)
	{
		$sql  = "select applyfile_no, applyfile_name from fplusapplyfile";
		$cond = "where apply_id = $apply_id";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("applyfile_no" => $row["applyfile_no"], "applyfile_name" => $row["applyfile_name"]);

		}
		return $arr;
	}

	// 承認情報取得
	function get_applyapv($apply_id)
	{
		$sql  = "select a.*, b.emp_lt_nm, b.emp_ft_nm, c.st_nm,  ";
		$sql .= "d.pjt_name as parent_pjt_name, f.pjt_name as child_pjt_name ";
		$sql .= "from fplusapplyapv a ";
		$sql .= "left join empmst b on a.emp_id = b.emp_id ";
		$sql .= "left join stmst c on a.emp_st = c.st_id ";
		$sql .= "left join project d on a.parent_pjt_id = d.pjt_id ";
		$sql .= "left join project f on a.child_pjt_id = f.pjt_id ";
		$cond = "where a.apply_id = $apply_id order by a.apv_order, a.apv_sub_order asc";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_all($sel);
	}

	// 承認者候補情報取得
	function get_applyapvemp($apply_id, $apv_order)
	{
		$sql  = "select a.*, b.emp_lt_nm, b.emp_ft_nm, d.st_nm, c.emp_del_flg, ";
		$sql .= "f.pjt_name as parent_pjt_name, g.pjt_name as child_pjt_name ";
		$sql .= "from fplusapplyapvemp a ";
		$sql .= "inner join empmst b on a.emp_id = b.emp_id ";
		$sql .= "inner join authmst c on b.emp_id = c.emp_id ";
		$sql .= "left join stmst d on b.emp_st = d.st_id ";
		$sql .= "left join project f on a.parent_pjt_id = f.pjt_id ";
		$sql .= "left join project g on a.child_pjt_id = g.pjt_id ";
		$cond = "where a.apply_id = $apply_id and a.apv_order = $apv_order order by a.person_order asc";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_all($sel);
	}

	// 申請結果通知情報取得
	function get_applynotice($apply_id)
	{
		$sql   = "select a.*, b.emp_lt_nm, b.emp_ft_nm ";
		$sql  .= "from fplusapplynotice a ";
		$sql  .= "left join empmst b on a.recv_emp_id = b.emp_id ";
		$cond  = "where a.apply_id = $apply_id order by a.oid ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array(
                            "apply_id" => $row["apply_id"],
                            "recv_emp_id" => $row["recv_emp_id"],
                            "confirmed_flg" => $row["confirmed_flg"],
                            "send_emp_id" => $row["send_emp_id"],
                            "send_date" => $row["send_date"],
                            "delete_flg" => $row["delete_flg"],
                            "rslt_ntc_div" => $row["rslt_ntc_div"],
                            "emp_lt_nm" => $row["emp_lt_nm"],
                            "emp_ft_nm" => $row["emp_ft_nm"]
                           );
		}
		return $arr;
	}

	// 下書き申請取得
	function get_draft_apply($emp_id)
	{
		$sql   = "select apply_id, apply_title, wkfw_id from fplusapply";
		$cond  = "where not delete_flg and draft_flg = 't' and emp_id = '$emp_id' ";
		$cond .= "order by apply_id";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("apply_id" => $row["apply_id"], "apply_title" => $row["apply_title"], "wkfw_id" => $row["wkfw_id"]);
		}

		return $arr;
	}

	// 前提とする申請書(申請用)取得
	function get_applyprecond($apply_id)
	{
		$sql   = "select ";
		$sql  .= "a.precond_wkfw_id, ";
		$sql  .= "a.precond_order, ";
		$sql  .= "a.precond_apply_id, ";
		$sql  .= "b.wkfw_title, ";
		$sql  .= "b.short_wkfw_name, ";
		$sql  .= "c.apply_title, ";
		$sql  .= "c.apply_date, ";
		$sql  .= "c.apply_no ";
		$sql  .= "from fplusapplyprecond a ";
		$sql  .= "inner join fpluswkfwmst b on a.precond_wkfw_id = b.wkfw_id ";
		$sql  .= "left join fplusapply c on a.precond_apply_id = c.apply_id ";
		$cond .= "where a.apply_id = $apply_id ";
		$cond .= "order by a.precond_order asc ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("precond_wkfw_id" => $row["precond_wkfw_id"],
                            "precond_order" => $row["precond_wkfw_id"],
                            "precond_apply_id" => $row["precond_apply_id"],
                            "wkfw_title" => $row["wkfw_title"],
                            "short_wkfw_name" => $row["short_wkfw_name"],
                            "apply_title" => $row["apply_title"],
                            "apply_date" => $row["apply_date"],
                            "apply_no" => $row["apply_no"]
			               );
		}

		return $arr;
	}

	// 申請更新(下書き用)
	function update_apply_draft($apply_id, $apply_content, $apply_date, $apply_title, $draft_flg, $apply_no)
	{
		$sql = "update fplusapply set";

		if($apply_no != "")
		{
			$set = array("apply_content", "apply_date", "apply_title", "draft_flg", "apply_no");
			$setvalue = array(pg_escape_string($apply_content), $apply_date, pg_escape_string($apply_title), $draft_flg, $apply_no);
		}
		else
		{
			$set = array("apply_content", "apply_date", "apply_title", "draft_flg");
			$setvalue = array(pg_escape_string($apply_content), $apply_date, pg_escape_string($apply_title), $draft_flg);
		}
		$cond = "where apply_id = $apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}



	// エピネットＡ針刺し更新
	function update_Episys107A($apply_id, $set_key,$set_data)
	{

		$sql=	"update  fplus_epi_a set";
		$set =	array("apply_id");


		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			array_push($set,$set_key[$i]);
		}

		$cond = "where apply_id = $apply_id";
		$setvalue = $set_data;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// エピネットB.皮膚・粘膜汚染報告書 更新
	function update_Episys107B($apply_id, $set_key,$set_data)
	{

		$sql=	"update  fplus_epi_b set";
		$set =	array("apply_id");


		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			array_push($set,$set_key[$i]);
		}

		$cond = "where apply_id = $apply_id";
		$setvalue = $set_data;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// MRSA等耐性菌検出報告書 更新
	function update_MRSA($apply_id, $set_key,$set_data)
	{

		$sql=	"update fplus_infection_mrsa set";
		$set =	array("apply_id");


		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			array_push($set,$set_key[$i]);
		}

		$cond = "where apply_id = $apply_id";
		$setvalue = $set_data;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 緊急報告受付 更新
	function update_immediate_repo($apply_id, $set_key,$set_data)
	{

		$sql=	"update fplus_immediate_report set";
		$set =	array("apply_id");


		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			array_push($set,$set_key[$i]);
		}

		$cond = "where apply_id = $apply_id";
		$setvalue = $set_data;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 第一報 更新
	function update_first_repo($apply_id, $set_key,$set_data)
	{

		$sql=	"update fplus_first_report set";
		$set =	array("apply_id");


		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			array_push($set,$set_key[$i]);
		}

		$cond = "where apply_id = $apply_id";
		$setvalue = $set_data;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 様式２ 更新
	function update_second_repo($apply_id, $set_key,$set_data)
	{

		$sql=	"update fplus_second_report set";
		$set =	array("apply_id");


		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			array_push($set,$set_key[$i]);
		}

		$cond = "where apply_id = $apply_id";
		$setvalue = $set_data;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// コンサルテーション依頼書 更新
	function update_cons_req($apply_id, $set_key,$set_data)
	{

		$sql=	"update fplus_consultation_request set";
		$set =	array("apply_id");


		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			array_push($set,$set_key[$i]);
		}

		$cond = "where apply_id = $apply_id";
		$setvalue = $set_data;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// コンサルテーション報告書 更新
	function update_cons_report($apply_id, $set_key,$set_data)
	{

		$sql=	"update fplus_consultation_report set";
		$set =	array("apply_id");


		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			array_push($set,$set_key[$i]);
		}

		$cond = "where apply_id = $apply_id";
		$setvalue = $set_data;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 医療機器不具合報告 更新
	function update_medical_apparatus_fault_report($apply_id, $set_key,$set_data)
	{

		$sql=	"update fplus_medical_apparatus_fault_report set";
		$set =	array("apply_id");


		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			array_push($set,$set_key[$i]);
		}

		$cond = "where apply_id = $apply_id";
		$setvalue = $set_data;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// レベル別発生件数データ 更新
	function update_number_of_occurrences_level($apply_id, $set_key,$set_data)
	{

		$sql=	"update fplus_number_of_occurrences_level set";
		$set =	array("apply_id");


		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			array_push($set,$set_key[$i]);
		}

		$cond = "where apply_id = $apply_id";
		$setvalue = $set_data;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 職種別発生件数データ 更新
	function update_number_of_occurrences($apply_id, $set_key,$set_data)
	{

		$sql=	"update fplus_number_of_occurrences set";
		$set =	array("apply_id");


		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			array_push($set,$set_key[$i]);
		}

		$cond = "where apply_id = $apply_id";
		$setvalue = $set_data;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 転倒・転落発生件数データ 更新
	function update_number_of_occurrences_fall_accident($apply_id, $set_key,$set_data)
	{

		$sql=	"update fplus_number_of_occurrences_fall_accident set";
		$set =	array("apply_id");


		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			array_push($set,$set_key[$i]);
		}

		$cond = "where apply_id = $apply_id";
		$setvalue = $set_data;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 医療事故報告管理表 更新
	function update_medical_accident_report_management($medical_accident_id, $set_key,$set_data)
	{

		$sql=	"update fplus_medical_accident_report_management set";
		$set =	array("medical_accident_id");


		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			array_push($set,$set_key[$i]);
		}

		$cond = "where medical_accident_id = $medical_accident_id";
		$setvalue = $set_data;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 医療事故情報 更新
	function update_medical_accident_info($medical_accident_info_id, $set_key,$set_data)
	{

		$sql=	"update fplus_medical_accident_info set";
		$set =	array("medical_accident_info_id");


		//登録するカラム名を設定
		for($i = 1 ; $i < count($set_key)  ; $i++)
		{
			array_push($set,$set_key[$i]);
		}

		$cond = "where medical_accident_info_id = $medical_accident_info_id";
		$setvalue = $set_data;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 申請更新
	function update_apply($apply_id, $apply_content, $apply_title)
	{
		$sql = "update fplusapply set";
		$set = array("apply_content", "apply_title");
		$setvalue = array(pg_escape_string($apply_content), pg_escape_string($apply_title));
		$cond = "where apply_id = $apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 承認更新
	function update_applyapv($arr)
	{
		$apply_id       = $arr["apply_id"];
		$apv_order      = $arr["apv_order"];
		$apv_sub_order  = $arr["apv_sub_order"];
		$emp_id         = $arr["emp_id"];
		$st_div         = $arr["st_div"];
		$emp_class      = $arr["emp_class"];
		$emp_attribute  = $arr["emp_attribute"];
		$emp_dept       = $arr["emp_dept"];
		$emp_st         = $arr["emp_st"];
		$emp_room       = $arr["emp_room"];
		$parent_pjt_id  = $arr["parent_pjt_id"];
		$child_pjt_id   = $arr["child_pjt_id"];

		$sql = "update fplusapplyapv set";
		$set = array("emp_id", "st_div", "emp_class", "emp_attribute", "emp_dept", "emp_st", "emp_room", "parent_pjt_id", "child_pjt_id");
		$setvalue = array($emp_id, $st_div, $emp_class, $emp_attribute, $emp_dept, $emp_st, $emp_room, $parent_pjt_id, $child_pjt_id);
		$cond = "where apply_id = $apply_id and apv_order = $apv_order ";

		if($apv_sub_order != "")
		{
			$cond .= "and apv_sub_order = $apv_sub_order";
		}

		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 添付ファイル削除
	function delete_applyfile($apply_id)
	{
		$sql = "delete from fplusapplyfile";
		$cond = "where apply_id = $apply_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 前提とする申請書(申請用)更新
	function update_applyprecond($apply_id, $precond_wkfw_id, $precond_order, $precond_apply_id)
	{
		$precond_apply_id = ($precond_apply_id == "") ? null : $precond_apply_id;

		$sql = "update fplusapplyprecond set";
		$set = array("precond_apply_id");
		$setvalue = array($precond_apply_id);
		$cond = "where apply_id = $apply_id and precond_wkfw_id = $precond_wkfw_id and precond_order = $precond_order ";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 申請結果通知削除
	function delete_applynotice($apply_id)
	{
		$sql = "delete from fplusapplynotice";
		$cond = "where apply_id = $apply_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

//-------------------------------------------------------------------------------------------------------------------------
// 申請承認関連(論理削除)
//-------------------------------------------------------------------------------------------------------------------------
	// 申請論理削除
	function update_delflg_apply($apply_id, $delete_flg)
	{
		$sql = "update fplusapply set";
		$set = array("delete_flg");
		$setvalue = array($delete_flg);
		$cond = "where apply_id = $apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 承認論理削除
	function update_delflg_applyapv($apply_id, $delete_flg)
	{
		$sql = "update fplusapplyapv set";
		$set = array("delete_flg");
		$setvalue = array($delete_flg);
		$cond = "where apply_id = $apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 承認候補論理削除
	function update_delflg_applyapvemp($apply_id, $delete_flg)
	{
		$sql = "update fplusapplyapvemp set";
		$set = array("delete_flg");
		$setvalue = array($delete_flg);
		$cond = "where apply_id = $apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 添付ファイル論理削除
	function update_delflg_applyfile($apply_id, $delete_flg)
	{
		$sql = "update fplusapplyfile set";
		$set = array("delete_flg");
		$setvalue = array($delete_flg);
		$cond = "where apply_id = $apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 非同期・同期受信論理削除
	function update_delflg_applyasyncrecv($apply_id, $delete_flg)
	{
		$sql = "update fplusapplyasyncrecv set";
		$set = array("delete_flg");
		$setvalue = array($delete_flg);
		$cond = "where apply_id = $apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 申請結果通知論理削除
	function update_delflg_applynotice($apply_id, $delete_flg)
	{
		$sql = "update fplusapplynotice set";
		$set = array("delete_flg");
		$setvalue = array($delete_flg);
		$cond = "where apply_id = $apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 前提とする申請書(申請用)論理削除
	function update_delflg_applyprecond($apply_id, $delete_flg)
	{
		$sql = "update fplusapplyprecond set";
		$set = array("delete_flg");
		$setvalue = array($delete_flg);
		$cond = "where apply_id = $apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 申請論理削除更新(全部)
	function update_delflg_all_apply($apply_id, $delete_flg)
	{
		// 申請情報を論理削除
		$this->update_delflg_apply($apply_id, $delete_flg);

		// 承認情報を論理削除
		$this->update_delflg_applyapv($apply_id, $delete_flg);

		// 承認者候補情報を論理削除
		$this->update_delflg_applyapvemp($apply_id, $delete_flg);

		// 添付ファイル情報論理削除
		$this->update_delflg_applyfile($apply_id, $delete_flg);

		// 非同期・同期受信論理削除
		$this->update_delflg_applyasyncrecv($apply_id, $delete_flg);

		// 申請結果通知論理削除
		$this->update_delflg_applynotice($apply_id, $delete_flg);

		// 前提とする申請書(申請用)論理削除
		$this->update_delflg_applyprecond($apply_id, $delete_flg);
	}

//-------------------------------------------------------------------------------------------------------------------------
// 再申請関連
//-------------------------------------------------------------------------------------------------------------------------

	// 申請登録
	function regist_re_apply($new_apply_id, $apply_id, $apply_content, $apply_title, $sceen_div)
	{
		$date = date("YmdHi");

		$year = substr($date, 0, 4);
		$md   = substr($date, 4, 4);
		if($md >= "0101" and $md <= "0331")
		{
			$year = $year - 1;
		}
		$max_cnt = $this->get_apply_cnt_per_year($year);
		$apply_no = $max_cnt + 1;


		$sql  = "insert into fplusapply ";
		$sql .= "(select ";
		$sql .= "$new_apply_id, ";
		$sql .= "a.wkfw_id, ";

		if($sceen_div == "DETAIL")
		{
			$sql .= "'$apply_content', ";
		}
		else
		{
			$sql .= "a.apply_content, ";
		}

		$sql .= "a.emp_id, ";
		$sql .= "'0', ";
		$sql .= "'$date', ";
		$sql .= "'f', ";

		if($sceen_div == "DETAIL")
		{
			$sql .= "'$apply_title', ";
		}
		else
		{
			$sql .= "a.apply_title, ";
		}

		$sql .= "null, ";
		$sql .= "'t', ";
		$sql .= "'t', ";
		$sql .= "b.emp_class, ";
		$sql .= "b.emp_attribute, ";
		$sql .= "b.emp_dept, ";
		$sql .= "'t', ";
		$sql .= "b.emp_room, ";
		$sql .= "'f', ";
		$sql .= "a.wkfw_appr, ";
		$sql .= "a.wkfw_content_type, ";
		$sql .= "apply_title_disp_flg, ";
        $sql .= "$apply_no, ";
		$sql .= "a.notice_sel_flg, ";
		$sql .= "a.wkfw_history_no ";
		$sql .= "from fplusapply a ";
		$sql .= "left join empmst b on a.emp_id = b.emp_id ";
		$sql .= "where a.apply_id = $apply_id";

		$ins = insert_into_table($this->_db_con, $sql, "", $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 承認登録
	function regist_re_applyapv($new_apply_id, $apply_id)
	{
		$sql  = "insert into fplusapplyapv ";
		$sql .= "(select ";
		$sql .= "a.wkfw_id, ";
		$sql .= "$new_apply_id, ";
		$sql .= "a.apv_order, ";
		$sql .= "a.emp_id, ";
		$sql .= "'0', ";
		$sql .= "null, ";
		$sql .= "'f', ";
		$sql .= "'', ";
		$sql .= "a.st_div, ";
		$sql .= "a.deci_flg, ";
		$sql .= "b.emp_class, ";
		$sql .= "b.emp_attribute, ";
		$sql .= "b.emp_dept, ";
		$sql .= "b.emp_st, ";
		$sql .= "'t', ";
		$sql .= "b.emp_room, ";
		$sql .= "a.apv_sub_order, ";
		$sql .= "a.multi_apv_flg, ";
		$sql .= "a.next_notice_div, ";
		$sql .= "a.parent_pjt_id, ";
		$sql .= "a.child_pjt_id, ";
		$sql .= "'f'  ";
		$sql .= "from fplusapplyapv a ";
		$sql .= "left join empmst b on a.emp_id = b.emp_id ";
		$sql .= "where a.apply_id = $apply_id";

		$ins = insert_into_table($this->_db_con, $sql, "", $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

	}

	// 承認者候補登録
	function regist_re_applyapvemp($new_apply_id, $apply_id)
	{
		$sql  = "insert into fplusapplyapvemp ";
		$sql .= "(select ";
		$sql .= "$new_apply_id, ";
		$sql .= "apv_order, ";
		$sql .= "person_order, ";
		$sql .= "emp_id, ";
		$sql .= "'f', ";
		$sql .= "st_div, ";
		$sql .= "parent_pjt_id, ";
		$sql .= "child_pjt_id ";
		$sql .= "from fplusapplyapvemp ";
		$sql .= "where apply_id = $apply_id";

		$ins = insert_into_table($this->_db_con, $sql, "", $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 添付ファイル登録
	function regist_re_applyfile($new_apply_id, $apply_id)
	{

		$sql  = "insert into fplusapplyfile ";
		$sql .= "(select ";
		$sql .= "$new_apply_id, ";
		$sql .= "applyfile_no, ";
		$sql .= "applyfile_name, ";
		$sql .= "'f' ";
		$sql .= "from fplusapplyfile ";
		$sql .= "where apply_id = $apply_id";

		$ins = insert_into_table($this->_db_con, $sql, "", $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 非同期・同期受信登録
	function regist_re_applyasyncrecv($new_apply_id, $apply_id)
	{
		$sql  = "insert into fplusapplyasyncrecv ";
		$sql .= "(select ";
		$sql .= "$new_apply_id, ";
		$sql .= "send_apv_order, ";
		$sql .= "send_apv_sub_order, ";
		$sql .= "recv_apv_order, ";
		$sql .= "recv_apv_sub_order, ";
		$sql .= "null, ";
		$sql .= "'f', ";
		$sql .= "'f' ";
		$sql .= "from fplusapplyasyncrecv ";
		$sql .= "where apply_id = $apply_id";


		$ins = insert_into_table($this->_db_con, $sql, "", $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 申請結果通知登録
	function regist_re_applynotice($new_apply_id, $apply_id)
	{
		$sql  = "insert into fplusapplynotice ";
		$sql .= "(select ";
		$sql .= "$new_apply_id, ";
		$sql .= "recv_emp_id, ";
		$sql .= "'f', ";
		$sql .= "null, ";
		$sql .= "null, ";
		$sql .= "'f' ";
		$sql .= "from fplusapplynotice ";
		$sql .= "where apply_id = $apply_id";

		$ins = insert_into_table($this->_db_con, $sql, "", $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 前提とする申請書(申請用)登録
	function regist_re_applyprecond($new_apply_id, $apply_id)
	{
		$sql  = "insert into fplusapplyprecond ";
		$sql .= "(select ";
		$sql .= "$new_apply_id, ";
		$sql .= "precond_wkfw_id, ";
		$sql .= "precond_order, ";
		$sql .= "precond_apply_id, ";
		$sql .= "'f' ";
		$sql .= "from fplusapplyprecond ";
		$sql .= "where apply_id = $apply_id";

		$ins = insert_into_table($this->_db_con, $sql, "", $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 再申請ＩＤ更新
	function update_re_apply_id($apply_id, $next_apply_id)
	{
		$sql = "update fplusapply set";
		$set = array("re_apply_id");
		$setvalue = array($next_apply_id);
		$cond = "where apply_id = $apply_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

//-------------------------------------------------------------------------------------------------------------------------
// 非同期・同期通知関連
//-------------------------------------------------------------------------------------------------------------------------
	// 非同期・同期受信登録
	function regist_applyasyncrecv($apply_id, $send_apv_order, $send_apv_sub_order, $recv_apv_order, $recv_apv_sub_order)
	{
		$sql = "insert into fplusapplyasyncrecv (apply_id, send_apv_order, send_apv_sub_order, recv_apv_order, recv_apv_sub_order, send_apved_order, apv_show_flg, delete_flg) values (";
		$content = array($apply_id, $send_apv_order, $send_apv_sub_order, $recv_apv_order, $recv_apv_sub_order, null, "f", "f");
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

//-------------------------------------------------------------------------------------------------------------------------
// 承認
//-------------------------------------------------------------------------------------------------------------------------
	// 同一階層の承認者数取得
	function get_same_hierarchy_apvcnt($apply_id, $apv_order)
	{
		$sql  = "select count(*) as cnt from fplusapplyapv";
		$cond ="where apply_id = $apply_id and apv_order = $apv_order";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0, "cnt");
	}

	// 最終承認階層番号取得
	function get_last_apv_order($apply_id)
	{
		$sql  = "select max(apv_order) as max from fplusapplyapv";
		$cond ="where apply_id = $apply_id";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "max");
	}


	// 承認情報更新
	function update_apvstat($apv_stat, $apv_date, $apv_comment, $apply_id, $apv_order, $apv_sub_order, $screen_div)
	{
		$sql = "update fplusapplyapv set";

		if($screen_div == "DETAIL")
		{
			$set = array("apv_stat", "apv_date", "apv_comment");
			$setvalue = array($apv_stat, $apv_date, pg_escape_string($apv_comment));
		}
		else if($screen_div == "LIST")
		{
			$set = array("apv_stat", "apv_date");
			$setvalue = array($apv_stat, $apv_date);
		}

		if($apv_sub_order != "")
		{
			$cond = "where apply_id = $apply_id and apv_order = $apv_order and apv_sub_order = $apv_sub_order";
		}
		else
		{
			$cond = "where apply_id = $apply_id and apv_order = $apv_order and apv_sub_order is null";
		}

		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 指定した承認ステータス数取得
	function get_apvstatcnt($apply_id, $apv_stat)
	{
		$sql = "select count(*) as cnt from fplusapplyapv ";
		$cond = "where apply_id = $apply_id and apv_stat = '$apv_stat'";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cnt");
	}

	// 全承認者数取得
	function get_allapvcnt($apply_id)
	{
		$sql = "select count(*) as cnt from fplusapplyapv ";
		$cond = "where apply_id = $apply_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cnt");
	}

	// 申請ステータス更新
	function update_applystat($apply_id, $apply_stat, $session)
	{
		$sql = "update fplusapply set";
		$set = array("apply_stat");
		$setvalue = array($apply_stat);
		$cond = "where apply_id = $apply_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		// 申請結果通知更新
		$arr_empmst = $this->get_empmst($session);
		$emp_id = $arr_empmst[0]["emp_id"];
		$this->update_send_applynotice($apply_id, $emp_id);
	}

	// 否認フラグ更新
	function update_ng_show_flg($apply_id, $apv_ng_show_flg)
	{
		$sql = "update fplusapply set";
		$set = array("apv_ng_show_flg");
		$setvalue = array($apv_ng_show_flg);
		$cond = "where apply_id = $apply_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// 差戻しフラグ更新
	function update_bak_show_flg($apply_id, $apv_bak_show_flg)
	{
		$sql = "update fplusapply set";
		$set = array("apv_bak_show_flg");
		$setvalue = array($apv_bak_show_flg);
		$cond = "where apply_id = $apply_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// 非同期・同期受信更新
	function update_apv_show_flg($apply_id, $send_apv_order, $send_apv_sub_order, $send_apved_order)
	{
		$sql = "update fplusapplyasyncrecv set";
		$set = array("apv_show_flg", "send_apved_order");
		$setvalue = array("t", $send_apved_order);

		if($send_apv_sub_order != "")
		{
			$cond = "where apply_id = $apply_id and send_apv_order = $send_apv_order and send_apv_sub_order = $send_apv_sub_order";
		}
		else
		{
			$cond = "where apply_id = $apply_id and send_apv_order = $send_apv_order and send_apv_sub_order is null";
		}
		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// 承認更新番号取得
	function get_max_send_apved_order($apply_id, $send_apv_order)
	{
		$sql  = "select max(send_apved_order) as max from fplusapplyasyncrecv ";
		$cond = "where apply_id = $apply_id and send_apv_order = $send_apv_order";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "max");
	}

	// 同一階層で指定した承認ステータス数取得
	function get_same_hierarchy_apvstatcnt($apply_id, $apv_order, $apv_stat)
	{
		$sql = "select count(*) as cnt from fplusapplyapv ";
		$cond = "where apply_id = $apply_id and apv_order = $apv_order and apv_stat = '$apv_stat'";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cnt");
	}


	// 階層ごとの承認者情報取得
	function get_applyapv_per_hierarchy($apply_id, $apv_order)
	{
		$sql = "select * from fplusapplyapv ";
		$cond = "where apply_id = $apply_id and apv_order = $apv_order";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_all($sel);
	}

	function get_apply_stat($apply_id)
	{
		$sql = "select apply_stat from fplusapply ";
		$cond = "where apply_id = $apply_id ";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "apply_stat");
	}


	// 承認処理
	function  approve_application($apply_id, $wkfw_appr, $apv_order, $apv_sub_order, $approve, $apv_comment, $next_notice_div, $session, $screen_div)
	{
		// 承認ステータス更新
		$apv_date = date("YmdHi");
		$this->update_apvstat($approve, $apv_date, $apv_comment, $apply_id, $apv_order, $apv_sub_order, $screen_div);

		// 同報タイプ
		if($wkfw_appr == "1")
		{
			// 全承認者数取得
			$allapvcnt = $this->get_allapvcnt($apply_id);
			switch($approve)
			{
				case "1":   // 承認
					$apvstatcnt = $this->get_apvstatcnt($apply_id, "1");
					if($allapvcnt == $apvstatcnt)
					{
						// 申請ステータス更新
						$this->update_applystat($apply_id, "1", $session);
					}
					break;
				case "2":   // 否認
					// 申請ステータス更新
					$this->update_applystat($apply_id, "2", $session);
					break;

				case "3":   // 差戻し
					// 申請ステータス更新
					$this->update_applystat($apply_id, "3", $session);
					break;
				default:
					break;
			}
		}
		// 稟議タイプ
		else if($wkfw_appr == "2")
		{
			$same_hierarchy_apvcnt = $this->get_same_hierarchy_apvcnt($apply_id, $apv_order);

			// 該当承認者の同一階層に他の承認者がいる場合(複数承認者)
			if($same_hierarchy_apvcnt > 1)
			{
				// 最終承認階層取得
				$last_apv_order = $this->get_last_apv_order($apply_id);

				// 承認者の階層より後につづく階層がある場合
				if($apv_order < $last_apv_order)
				{
					// 承認者の階層が非同期指定の場合
					if($next_notice_div == "1")
					{
						switch($approve)
						{
							case "1":   // 承認
								// 非同期・同期受信テーブル更新
								$apved_order = $this->get_max_send_apved_order($apply_id, $apv_order);
								$this->update_apv_show_flg($apply_id, $apv_order, $apv_sub_order, $apved_order+1);
								break;
							case "2":   // 否認

								$same_hierarchy_apvcnt = $this->get_same_hierarchy_apvcnt($apply_id, $apv_order);
								$same_hierarchy_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "2");

								// 全員が否認の場合
								if($same_hierarchy_apvcnt == $same_hierarchy_apvstatcnt)
								{
									// 申請ステータス更新
									$this->update_applystat($apply_id, "2", $session);
								}
								// 同一階層で先に「承認」した人がいた場合
								$same_hierarchy_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1");
								if($same_hierarchy_apvstatcnt > 0)
								{
									// 非同期・同期受信テーブル更新
									$apved_order = $this->get_max_send_apved_order($apply_id, $apv_order);
									$this->update_apv_show_flg($apply_id, $apv_order, $apv_sub_order, $apved_order+1);
								}
								break;
							case "3":   // 差戻し

								// 同一階層で先に「承認」した人がいた場合
								$same_hierarchy_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1");
								if($same_hierarchy_apvstatcnt > 0)
								{
									// 非同期・同期受信テーブル更新
									$apved_order = $this->get_max_send_apved_order($apply_id, $apv_order);
									$this->update_apv_show_flg($apply_id, $apv_order, $apv_sub_order, $apved_order+1);
								}

								break;
							default:
								break;
						}

						// 未承認者がいない ＡＮＤ 承認が一人もいない ＡＮＤ 差戻しが一人でもいる
						$non_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "0");
						$ok_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1");
						$bak_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "3");
						if($non_apvstatcnt == 0 && $ok_apvstatcnt == 0 && $bak_apvstatcnt > 0)
						{
							// 申請ステータス更新
							$this->update_applystat($apply_id, "3", $session);
						}
					}
					// 承認者の階層が同期指定の場合
					else if($next_notice_div == "2")
					{
/*
						switch($approve)
						{
							case "1":   // 承認
								$same_hierarchy_apvcnt = $this->get_same_hierarchy_apvcnt($apply_id, $apv_order);
								$same_hierarchy_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1");
								if($same_hierarchy_apvcnt == $same_hierarchy_apvstatcnt)
								{
									// 非同期・同期受信テーブル更新
									$this->update_apv_show_flg($apply_id, $apv_order, "", 1);
								}
								break;
							case "2":   // 否認

								// 申請ステータス更新
								$this->update_applystat($apply_id, "2", $session);

								break;
							case "3":   // 差戻し

								// 申請ステータス更新
								$this->update_applystat($apply_id, "3", $session);
								break;
							default:
								break;
						}
*/
						$same_hierarchy_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "0");
						// 同一階層で未承認者がいない場合
						if($same_hierarchy_apvstatcnt == 0)
						{
							$same_hierarchy_apvcnt = $this->get_same_hierarchy_apvcnt($apply_id, $apv_order);
							$ok_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1");
							$no_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "2");
							$bak_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "3");

							// 全員が承認の場合
							if($same_hierarchy_apvcnt == $ok_approvecnt)
							{
								// 非同期・同期受信テーブル更新
								$this->update_apv_show_flg($apply_id, $apv_order, "", 1);
							}

							// 否認があって差戻しがない場合、否認にする。
							if($no_approvecnt > 0 && $bak_approvecnt == 0)
							{
								// 申請ステータス更新
								$this->update_applystat($apply_id, "2", $session);
							}

							// 差戻しが１つでもある場合
							if($bak_approvecnt > 0)
							{
								// 申請ステータス更新
								$this->update_applystat($apply_id, "3", $session);

							}
						}
					}
					// 権限並列指定の場合
					else if($next_notice_div == "3")
					{
						$non_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "0");

						// 未承認者がいた場合
						if($non_apvstatcnt > 0)
						{
							switch($approve)
							{
								case "1":   // 承認

									// 非同期・同期受信テーブル更新
									$this->update_apv_show_flg($apply_id, $apv_order, "", 1);

									// 他の承認者の承認ステータスが「未承認」の場合、「承認」に更新
									$this->update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, "1");

									break;
								case "2":   // 否認

									// 他の承認者の承認ステータスが「未承認」の場合、「否認」に更新
									$this->update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, "2");
									// 申請ステータス更新
									$this->update_applystat($apply_id, "2", $session);
									break;
								case "3":   // 差戻し

									// 他の承認者の承認ステータスが「未承認」の場合、「差戻し」に更新
									$this->update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, "3");
									// 申請ステータス更新
									$this->update_applystat($apply_id, "3", $session);

									break;
								default:
									break;
							}
						}
					}
				}
				// 承認者の階層より後につづく階層がない場合（最終階層）
				else if($apv_order == $last_apv_order)
				{
					// 承認者の階層が非同期指定の場合
					if($next_notice_div == "1")
					{
						$same_hierarchy_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "0");
						// 同一階層で未承認者がいない場合
						if($same_hierarchy_apvstatcnt == 0)
						{
							// 承認が１つでもある場合
							$approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1");
							if($approvecnt > 0)
							{
								// 申請ステータス更新
								$this->update_applystat($apply_id, "1", $session);
							}

							// 全員が否認の場合
							$same_hierarchy_apvcnt = $this->get_same_hierarchy_apvcnt($apply_id, $apv_order);
							$no_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "2");
							if($same_hierarchy_apvcnt == $no_approvecnt)
							{
								// 申請ステータス更新
								$this->update_applystat($apply_id, "2", $session);
							}

							// 承認がなく差戻しがある場合
							if($approvecnt == 0)
							{
								$bak_cnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "3");
								if($bak_cnt > 0)
								{
									// 申請ステータス更新
									$this->update_applystat($apply_id, "3", $session);
								}
							}
						}
					}
					// 承認者の階層が同期指定の場合
					else if($next_notice_div == "2")
					{
						$same_hierarchy_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "0");
						// 同一階層で未承認者がいない場合
						if($same_hierarchy_apvstatcnt == 0)
						{
							$same_hierarchy_apvcnt = $this->get_same_hierarchy_apvcnt($apply_id, $apv_order);
							$ok_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1");
							$no_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "2");
							$bak_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "3");

							// 全員が承認の場合
							if($same_hierarchy_apvcnt == $ok_approvecnt)
							{
								// 申請ステータス更新
								$this->update_applystat($apply_id, "1", $session);
							}

							// 否認があって差戻しがない場合、否認にする。
							if($no_approvecnt > 0 && $bak_approvecnt == 0)
							{
								// 申請ステータス更新
								$this->update_applystat($apply_id, "2", $session);
							}

							// 差戻しが１つでもある場合
							if($bak_approvecnt > 0)
							{
								// 申請ステータス更新
								$this->update_applystat($apply_id, "3", $session);

							}
						}
					}
					// 権限並列指定の場合
					else if($next_notice_div == "3")
					{
						$non_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "0");

						// 未承認者がいた場合
						if($non_apvstatcnt > 0)
						{
							switch($approve)
							{
								case "1":   // 承認

									// 他の承認者の承認ステータスが「未承認」の場合、「承認」に更新
									$this->update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, "1");
									// 申請ステータス更新
									$this->update_applystat($apply_id, "1", $session);
									break;
								case "2":   // 否認

									// 他の承認者の承認ステータスが「未承認」の場合、「否認」に更新
									$this->update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, "2");
									// 申請ステータス更新
									$this->update_applystat($apply_id, "2", $session);
									break;
								case "3":   // 差戻し

									// 他の承認者の承認ステータスが「未承認」の場合、「差戻し」に更新
									$this->update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, "3");
									// 申請ステータス更新
									$this->update_applystat($apply_id, "3", $session);

									break;
								default:
									break;
							}
						}
					}
				}
			}
			// 該当承認者の同一階層に他の承認者がいない場合(承認者一人)
			else if($same_hierarchy_apvcnt == 1)
			{
				switch($approve)
				{
					case "1":   // 承認
						// 最終承認階層取得
						$last_apv_order = $this->get_last_apv_order($apply_id);
						if($apv_order == $last_apv_order)
						{
							// 申請ステータス更新
							$this->update_applystat($apply_id, "1", $session);
						}
						break;
					case "2":   // 否認
						// 申請ステータス更新
						$this->update_applystat($apply_id, "2", $session);
						break;
					case "3":   // 差戻し
						// 申請ステータス更新
						$this->update_applystat($apply_id, "3", $session);
						break;
					default:
						break;

				}
			}
		}
	}

	// 非同期・同期受信情報取得
	function get_applyasyncrecv($apply_id, $recv_apv_order, $recv_apv_sub_order, $send_apved_order)
	{
		$sql   = "select * from fplusapplyasyncrecv ";
		$cond .= "where apply_id = $apply_id and ";
		$cond .= "recv_apv_order = $recv_apv_order and ";
        if($recv_apv_sub_order != "")
		{
			$cond .= "recv_apv_sub_order = $recv_apv_sub_order and ";
		}
		else
		{
			$cond .= "recv_apv_sub_order is null and ";
		}
		$cond .= "send_apved_order <= $send_apved_order ";
		$cond .= "order by send_apved_order asc ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("apply_id" => $row["apply_id"],
                            "send_apv_order" =>  $row["send_apv_order"],
                            "send_apv_sub_order" =>  $row["send_apv_sub_order"],
                            "recv_apv_order" =>  $row["recv_apv_order"],
                            "recv_apv_sub_order" =>  $row["recv_apv_sub_order"],
                            "send_apved_order" =>  $row["send_apved_order"]
			               );
		}
		return $arr;
	}

	// 申請結果通知・送信者更新
	function update_send_applynotice($apply_id, $send_emp_id)
	{
		$date = date("YmdHi");

		$sql = "update fplusapplynotice set";
		$set = array("send_emp_id", "send_date");
		$setvalue = array($send_emp_id, $date);
		$cond = "where apply_id = $apply_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// 承認テーブル更新処理（権限並列用）
	function update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, $apv_stat)
	{
		$sql = "update fplusapplyapv set";
		$set = array("apv_stat", "other_apv_flg", "apv_date");
		$setvalue = array($apv_stat, "t", $apv_date);
		$cond = "where apply_id = $apply_id and apv_order = $apv_order and apv_stat = '0'";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

//-------------------------------------------------------------------------------------------------------------------------
// ワークフロープレビュー
//-------------------------------------------------------------------------------------------------------------------------

	// 承認者管理情報取得(プレビュー)
	function get_approve_mng_for_wkfwpreview($data)
	{
		// 承認者階層数取得
		$approve_num = $data["approve_num"];

		$arr = array();

		for($i=0; $i<$approve_num; $i++)
		{
			$j = $i + 1;

			$tmp_apv_div0_flg = "apv_div0_flg".$j;
			$apv_div0_flg = $data[$tmp_apv_div0_flg];

			$tmp_apv_div1_flg = "apv_div1_flg".$j;
			$apv_div1_flg = $data[$tmp_apv_div1_flg];

			$tmp_apv_div2_flg = "apv_div2_flg".$j;
			$apv_div2_flg = $data[$tmp_apv_div2_flg];

			$tmp_apv_div3_flg = "apv_div3_flg".$j;
			$apv_div3_flg = $data[$tmp_apv_div3_flg];

			$tmp_apv_div4_flg = "apv_div4_flg".$j;
			$apv_div4_flg = $data[$tmp_apv_div4_flg];

			$tmp_target_class_div = "target_class_div".$j;
			$target_class_div = $data[$tmp_target_class_div];

			$tmp_st_id = "st_id".$j;
			$st_id = $data[$tmp_st_id];

			$tmp_multi_apv_flg = "multi_apv_flg".$j;
			$multi_apv_flg = $data[$tmp_multi_apv_flg];

			$tmp_apv_num = "apv_num".$j;
			$apv_num = $data[$tmp_apv_num];

			$tmp_emp_id = "emp_id".$j;
			$emp_id = $data[$tmp_emp_id];

			$tmp_pjt_parent_id = "pjt_parent_id".$j;
			$pjt_parent_id = $data[$tmp_pjt_parent_id];

			$tmp_pjt_child_id = "pjt_child_id".$j;
			$pjt_child_id = $data[$tmp_pjt_child_id];

			$tmp_class_sect_id = "class_sect_id".$j;
			$class_sect_id = $data[$tmp_class_sect_id];

			$tmp_atrb_sect_id = "atrb_sect_id".$j;
			$atrb_sect_id = $data[$tmp_atrb_sect_id];

			$tmp_dept_sect_id = "dept_sect_id".$j;
			$dept_sect_id = $data[$tmp_dept_sect_id];

			$tmp_room_sect_id = "room_sect_id".$j;
			$room_sect_id = $data[$tmp_room_sect_id];

			$tmp_st_sect_id = "st_sect_id".$j;
			$st_sect_id = $data[$tmp_st_sect_id];


			$apv_setting_flg = "f";
			if($apv_div0_flg == "f" && $apv_div1_flg == "f" && $apv_div2_flg == "t" && $apv_div3_flg == "f" && $apv_div4_flg == "f")
			{
				$apv_setting_flg = "t";
			}

			$arr[] = array("apv_order" => $j,
                            "apv_div0_flg" => $apv_div0_flg,
                            "apv_div1_flg" => $apv_div1_flg,
                            "apv_div2_flg" => $apv_div2_flg,
                            "apv_div3_flg" => $apv_div3_flg,
                            "apv_div4_flg" => $apv_div4_flg,
                            "target_class_div" => $target_class_div,
                            "st_id" => $st_id,
                            "multi_apv_flg" => $multi_apv_flg,
                            "emp_id" => $emp_id,
                            "pjt_parent_id" => $pjt_parent_id,
                            "pjt_child_id" => $pjt_child_id,
                            "class_sect_id" => $class_sect_id,
                            "atrb_sect_id" => $atrb_sect_id,
                            "dept_sect_id" => $dept_sect_id,
                            "room_sect_id" => $room_sect_id,
                            "st_sect_id" => $st_sect_id,
                            "apv_num" => $apv_num,
                            "apv_setting_flg" => $apv_setting_flg);
		}
		return $arr;
	}

	// 承認者詳細情報取得(プレビュー)
	function get_approve_dtl_for_wkfwpreview($arr_wkfwapvmng, $emp_id)
	{
		$arr = array();
		foreach($arr_wkfwapvmng as $apvmng)
		{
			$arr_apv = array();
			$multi_apv_flg = $apvmng["multi_apv_flg"];

			$apv_div0_flg = $apvmng["apv_div0_flg"];
			$apv_div1_flg = $apvmng["apv_div1_flg"];
			$apv_div2_flg = $apvmng["apv_div2_flg"];
			$apv_div3_flg = $apvmng["apv_div3_flg"];
			$apv_div4_flg = $apvmng["apv_div4_flg"];

			// 部署役職(申請者所属)指定
			if($apv_div0_flg == "t")
			{
				$arr_apvpstdtl = $this->get_apvpstdtl_for_wkfwpreview($apvmng["target_class_div"], $apvmng["st_id"], $emp_id);
				for($i=0; $i<count($arr_apvpstdtl); $i++)
				{
					$arr_apvpstdtl[$i]["apv_div"] = "0";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_apvpstdtl);

			}

			// 部署役職(部署指定)指定
			if($apv_div4_flg == "t")
			{
				$arr_emp_info_sect = $this->get_emp_info_for_post_sect($apvmng["class_sect_id"], $apvmng["atrb_sect_id"], $apvmng["dept_sect_id"], $apvmng["room_sect_id"], $apvmng["st_sect_id"]);

				for($i=0; $i<count($arr_emp_info_sect); $i++)
				{
					$arr_emp_info_sect[$i]["apv_div"] = "4";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_emp_info_sect);
			}

			// 職員指定
			if($apv_div1_flg == "t")
			{
				$arr_emp_id = split(",", $apvmng["emp_id"]);
				$arr_emp_info = array();
				foreach($arr_emp_id as $emp_id)
				{
					$arr_empmst_detail = $this->get_empmst_detail($emp_id);
					$emp_lt_nm = $arr_empmst_detail[0]["emp_lt_nm"];
					$emp_ft_nm = $arr_empmst_detail[0]["emp_ft_nm"];
                    $emp_full_nm = $emp_lt_nm." ".$emp_ft_nm;
					$arr_emp_info[] = array(
									 "emp_id" => $arr_empmst_detail[0]["emp_id"],
									 "emp_full_nm" => $emp_full_nm,
									 "st_nm" => $arr_empmst_detail[0]["st_nm"]
 									   );
				}
				$apvmng["emp_id"] = "";

				for($i=0; $i<count($arr_emp_info); $i++)
				{
					$arr_emp_info[$i]["apv_div"] = "1";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_emp_info);
			}

			// 委員会・ＷＧ
			if($apv_div3_flg == "t")
			{
				$arr_project_member = $this->get_project_member($apvmng["pjt_parent_id"], $apvmng["pjt_child_id"]);

				$pjt_nm = $this->get_pjt_nm($apvmng["pjt_parent_id"]);
				if($apvmng["pjt_child_id"] != "")
				{
					$pjt_nm .= " > ";
					$pjt_nm .= $this->get_pjt_nm($apvmng["pjt_child_id"]);
				}

				for($i=0; $i<count($arr_project_member); $i++)
				{
					$arr_project_member[$i]["apv_div"] = "3";
					$arr_project_member[$i]["pjt_nm"] = $pjt_nm;
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_project_member);
			}

			// その他
			if($apv_div2_flg == "t")
			{
				$apv_num = $apvmng["apv_num"];
				$arr_setting_apv = "";
				for($i=0; $i<$apv_num; $i++)
				{
					$arr_setting_apv[] = array();
				}

				for($i=0; $i<count($arr_setting_apv); $i++)
				{
					$arr_setting_apv[$i]["apv_div"] = "2";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_setting_apv);

			}

			// 複数承認者「許可する」
			if($multi_apv_flg == "t")
			{
				if(count($arr_apv) > 0)
				{
					if(count($arr_apv) > 1)
					{
						$apv_sub_order = 1;
						foreach($arr_apv as $apv)
						{
							$arr_emp = array();
							$apvmng["apv_sub_order"] = $apv_sub_order;
							$arr_emp[] = array(
                                                "emp_id" => $apv["emp_id"],
                                                "emp_full_nm" => $apv["emp_full_nm"],
                                                "st_nm" => $apv["st_nm"],
                                                "apv_div" => $apv["apv_div"],
                                                "pjt_nm" => $apv["pjt_nm"]
                                               );
							$apvmng["emp_infos"] = $arr_emp;

							if($apv["apv_div"] == "2")
							{
								$apvmng["apv_setting_flg"] = "t";
							}

							$arr[] = $apvmng;
							$apv_sub_order++;
						}
					}
					else
					{
						$arr_emp = array();
						$apvmng["apv_sub_order"] = "";
						$arr_emp[] = array(
                                            "emp_id" => $arr_apv[0]["emp_id"],
                                            "emp_full_nm" => $arr_apv[0]["emp_full_nm"],
                                            "st_nm" => $arr_apv[0]["st_nm"],
                                            "apv_div" => $arr_apv[0]["apv_div"],
                                            "pjt_nm" => $arr_apv[0]["pjt_nm"]
                                           );
						$apvmng["emp_infos"] = $arr_emp;

						if($arr_apv[0]["apv_div"] == "2")
						{
							$apvmng["apv_setting_flg"] = "t";
						}

						$arr[] = $apvmng;
					}
				}
				else
				{
					$apvmng["emp_infos"] = array();
					$arr[] = $apvmng;
				}
			}
			// 複数承認者「許可しない」
			else
			{
				$apvmng["apv_sub_order"] = "";
				$apvmng["emp_infos"] = $arr_apv;
				$arr[] = $apvmng;
			}
		}

		return $arr;

	}


	// 部署役職指定情報取得(プレビュー用)
	function get_apvpstdtl_for_wkfwpreview($target_class_div, $st_id, $emp_id)
	{
		$arr = array();

		// 申請者の部署役職取得
		$arr_emp_info = $this->get_emp_info($emp_id);

		if($target_class_div == "4")
		{
			$fourth_post_exist_flg = false;
			foreach($arr_emp_info as $emp_info)
			{
				$emp_room  = $emp_info["emp_room"];
				if($emp_room != "")
				{
					$fourth_post_exist_flg = true;
					break;
				}
			}

			if(!$fourth_post_exist_flg)
			{
				return $arr;
			}
		}


		$sql  = "select ";
		$sql .= "emp.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, empmst.emp_class, empmst.emp_attribute, empmst.emp_dept, empmst.emp_room, empmst.emp_st, stmst.st_nm ";
		$sql .= "from ";
    	$sql .= "(";
    	$sql .= "select emp_id from empmst where ";
		// 「部署指定しない」以外
		if($target_class_div != 0)
		{
			$sql .= "( ";
			$idx = 0;
			foreach($arr_emp_info as $emp_info)
			{
				$emp_class     = $emp_info["emp_class"];
				$emp_attribute = $emp_info["emp_attribute"];
				$emp_dept      = $emp_info["emp_dept"];
				$emp_room      = $emp_info["emp_room"];

				if($target_class_div == "4")
				{
					if($emp_room == "")
					{
						continue;
					}
				}

				if($idx > 0)
				{
					$sql .= "or ";
				}

				if($target_class_div == "1")
				{
					$sql .= "emp_class = $emp_class ";
				}
				else if($target_class_div == "2")
				{
					$sql .= "emp_attribute = $emp_attribute ";
				}
				else if($target_class_div == "3")
				{
					$sql .= "emp_dept = $emp_dept ";
				}
				else if($target_class_div == "4")
				{
					$sql .= "emp_room = $emp_room ";
				}
				$idx++;
			}
			$sql .= ") ";
			$sql .= "and ";
		}

		$sql .= "empmst.emp_st in ($st_id) ";
		$sql .= "union ";
		$sql .= "select emp_id from concurrent where ";

		// 「部署指定しない」以外
		if($target_class_div != 0)
		{
			$sql .= "( ";
			$idx = 0;
			foreach($arr_emp_info as $emp_info)
			{
				$emp_class     = $emp_info["emp_class"];
				$emp_attribute = $emp_info["emp_attribute"];
				$emp_dept      = $emp_info["emp_dept"];
				$emp_room      = $emp_info["emp_room"];

				if($target_class_div == "4")
				{
					if($emp_room == "")
					{
						continue;
					}
				}

				if($idx > 0)
				{
					$sql .= "or ";
				}

				if($target_class_div == "1")
				{
					$sql .= "emp_class = $emp_class ";
				}
				else if($target_class_div == "2")
				{
					$sql .= "emp_attribute = $emp_attribute ";
				}
				else if($target_class_div == "3")
				{
					$sql .= "emp_dept = $emp_dept ";
				}
				else if($target_class_div == "4")
				{
					$sql .= "emp_room = $emp_room ";
				}
				$idx++;
			}
			$sql .= ") ";
			$sql .= "and ";
		}
		$sql .= "concurrent.emp_st in ($st_id) ";
		$sql .= ") emp ";
		$sql .= "inner join empmst on emp.emp_id = empmst.emp_id ";
		$sql .= "inner join authmst on emp.emp_id = authmst.emp_id and not authmst.emp_del_flg ";
		$sql .= "inner join stmst on empmst.emp_st = stmst.st_id and not stmst.st_del_flg ";
		$cond = "order by emp.emp_id asc ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$idx = 1;
		while($row = pg_fetch_array($sel))
		{
			$emp_full_nm = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
			$arr[] = array("emp_id" => $row["emp_id"],
			                "emp_full_nm" => $emp_full_nm,
			                "st_nm" => $row["st_nm"]
			               );
			$idx++;
		}
		return $arr;
	}

	// 申請者以外の結果通知者取得(プレビュー用)
	function get_wkfw_notice_for_wkfwpreview($data, $emp_id)
	{
		$arr_tmp = array();
		$notice = $data["notice"];

		if($notice != "")
		{
			$rslt_ntc_div0_flg = $data["rslt_ntc_div0_flg"];
			$rslt_ntc_div1_flg = $data["rslt_ntc_div1_flg"];
			$rslt_ntc_div2_flg = $data["rslt_ntc_div2_flg"];
			$rslt_ntc_div3_flg = $data["rslt_ntc_div3_flg"];
			$rslt_ntc_div4_flg = $data["rslt_ntc_div4_flg"];

			// 部署役職指定(申請者所属)
			if($rslt_ntc_div0_flg == "t")
			{
				$notice_target_class_div = $data["notice_target_class_div"];
				$notice_st_id = $data["notice_st_id"];
				$arr_apvpstdtl = $this->get_apvpstdtl_for_wkfwpreview($notice_target_class_div, $notice_st_id, $emp_id);

				// マージ処理
				$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $arr_apvpstdtl);
			}

			// 部署役職指定
			if($rslt_ntc_div4_flg == "t")
			{
				$arr_post_sect = $this->get_emp_info_for_post_sect($data["notice_class_sect_id"], $data["notice_atrb_sect_id"], $data["notice_dept_sect_id"], $data["notice_room_sect_id"], $data["notice_st_sect_id"]);

				// マージ処理
				$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $arr_post_sect);
			}

			// 職員指定
			if($rslt_ntc_div1_flg == "t")
			{
				$notice_emp_id = $data["notice_emp_id"];
				$arr_notice_emp_id = split(",", $notice_emp_id);
				foreach($arr_notice_emp_id as $notice_emp_id)
				{
					$arr_empmst_detail = $this->get_empmst_detail($notice_emp_id);
					$emp_lt_nm = $arr_empmst_detail[0]["emp_lt_nm"];
					$emp_ft_nm = $arr_empmst_detail[0]["emp_ft_nm"];
                    $emp_full_nm = $emp_lt_nm." ".$emp_ft_nm;
					$arr_emp_info[] = array(
									 "emp_id" => $arr_empmst_detail[0]["emp_id"],
									 "emp_full_nm" => $emp_full_nm,
									 "st_nm" => $arr_empmst_detail[0]["st_nm"]
									 );
				}

				// マージ処理
				$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $arr_emp_info);
			}

			// 委員会・ＷＧ指定
			if($rslt_ntc_div3_flg == "t")
			{
				$notice_pjt_parent_id = $data["notice_pjt_parent_id"];
				$notice_pjt_child_id = $data["notice_pjt_child_id"];
				$arr_project_member = $this->get_project_member($notice_pjt_parent_id, $notice_pjt_child_id);

				// マージ処理
				$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $arr_project_member);
			}
		}

		// 申請者を除去する
		$arr = array();
		foreach($arr_tmp as $tmp)
		{
			$tmp_emp_id = $tmp["emp_id"];
			if($tmp_emp_id == $emp_id)
			{
				continue;
			}
			$arr[] = $tmp;
		}
		return $arr;
	}

	// 部署役職(部署指定)情報取得
	function get_apvsectdtl($wkfw_id, $apv_order)
	{
		$sql   = "select a.class_id, a.atrb_id, a.dept_id, a.room_id ";
		$sql  .= "from fplusapvsectdtl a ";
		$cond  = "where wkfw_id = $wkfw_id and apv_order = $apv_order";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$class_id = pg_fetch_result($sel, 0, "class_id");
		$atrb_id = pg_fetch_result($sel, 0, "atrb_id");
		$dept_id = pg_fetch_result($sel, 0, "dept_id");
		$room_id = pg_fetch_result($sel, 0, "room_id");

		return array("class_id" => $class_id, "atrb_id" => $atrb_id, "dept_id" => $dept_id, "room_id" => $room_id);
	}

	// 役職情報取得
	function get_apvpstdtl($wkfw_id, $apv_order, $st_div)
	{
		$sql   = "select st_id from fplusapvpstdtl ";
		$cond  = "where wkfw_id = $wkfw_id and apv_order = $apv_order and st_div = $st_div order by st_id";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("st_id" => $row["st_id"]);
		}

		return $arr;
	}

	// 職員情報取得(部署役職指定用)
	function get_emp_info_for_post_sect($class_id, $attribute_id, $dept_id, $room_id, $st_sect_id)
	{

		$sql  = "select a.emp_id, a.emp_lt_nm, a.emp_ft_nm, c.st_nm ";
		$sql .= "from ( ";
		$sql .= "select varchar(1) '1' as type, emp_id, emp_lt_nm, emp_ft_nm, emp_st from empmst ";

		if($class_id != "")
		{
			$sql .= "where emp_class = $class_id ";
		}

		if($attribute_id != "")
		{
			$sql .= "and emp_attribute = $attribute_id ";
		}

		if($dept_id != "")
		{
			$sql .= "and emp_dept = $dept_id ";
		}

		if($room_id != "")
		{
			$sql .= "and emp_room = $room_id ";
		}

		if($st_sect_id)
		{
			$sql .= "and emp_st in ($st_sect_id) ";
		}

		$sql .= "union ";
		$sql .= "select varchar(1) '2' as type, sub_a.emp_id, sub_b.emp_lt_nm, sub_b.emp_ft_nm, sub_a.emp_st from concurrent sub_a ";
		$sql .= "inner join empmst sub_b on sub_a.emp_id = sub_b.emp_id ";

		if($class_id != "")
		{
			$sql .= "where sub_a.emp_class = $class_id ";
		}

		if($attribute_id != "")
		{
			$sql .= "and sub_a.emp_attribute = $attribute_id ";
		}

		if($dept_id != "")
		{
			$sql .= "and sub_a.emp_dept = $dept_id ";
		}

		if($room_id != "")
		{
			$sql .= "and sub_a.emp_room = $room_id ";
		}

		if($st_sect_id)
		{
			$sql .= "and sub_a.emp_st in ($st_sect_id) ";
		}

		$sql .= ") a ";
		$sql .= "inner join authmst b on a.emp_id = b.emp_id and not b.emp_del_flg ";
		$sql .= "left join stmst c on a.emp_st = c.st_id and not c.st_del_flg ";
		$sql .= "order by a.emp_id asc, a.type asc";

		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		$tmp_emp_id = "";
		$idx = 1;
		while($row = pg_fetch_array($sel))
		{
			if($tmp_emp_id == $row["emp_id"])
			{
				continue;
			}
			$emp_full_nm = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
			$arr[] = array("emp_id" => $row["emp_id"],
			                "emp_full_nm" => $emp_full_nm,
			                "st_nm" => $row["st_nm"],
			                "apv_sub_order" => $idx);

			$tmp_emp_id = $row["emp_id"];
			$idx++;
		}
		return $arr;
	}
//-------------------------------------------------------------------------------------------------------------------------
// 申請結果通知関連
//-------------------------------------------------------------------------------------------------------------------------

	// 申請結果通知一覧取得
	function get_applynotice_list($arr)
	{

		$session      = $arr["session"];
		$apply_emp_nm = $arr["apply_emp_nm"];
		$apply_stat   = $arr["apply_stat"];
		$class        = $arr["class"];
		$attribute    = $arr["attribute"];
		$dept         = $arr["dept"];
		$room         = $arr["room"];
		$page         = $arr["page"];
		$max_page     = $arr["max_page"];

		$arr_empmst = $this->get_empmst($session);
		$emp_id = $arr_empmst[0]["emp_id"];

		$page = ($page - 1) * 15;

		$sql   = "select ";
		$sql  .= "b.*, ";
		$sql  .= "d.emp_lt_nm as apply_lt_nm, ";
		$sql  .= "d.emp_ft_nm as apply_ft_nm, ";
		$sql  .= "a.confirmed_flg, ";
		$sql  .= "a.send_date, ";
		$sql  .= "c.emp_lt_nm as send_lt_nm, ";
		$sql  .= "c.emp_ft_nm as send_ft_nm, ";
		$sql  .= "e.wkfw_nm, ";
		$sql  .= "e.wkfw_title, ";
		$sql  .= "e.short_wkfw_name, ";
		$sql  .= "e.wkfw_folder_id ";
		$sql  .= "from fplusapplynotice a ";
		$sql  .= "inner join fplusapply b on a.apply_id = b.apply_id ";
		$sql  .= "inner join empmst c on a.send_emp_id = c.emp_id ";
		$sql  .= "inner join empmst d on b.emp_id = d.emp_id ";
		$sql  .= "inner join ";
		$sql  .= "(select a.wkfw_id, a.wkfw_type, a.wkfw_title, b.wkfw_nm, a.short_wkfw_name, a.wkfw_folder_id ";
		$sql  .= "from fpluswkfwmst a ";
		$sql  .= "inner join fpluscatemst b on ";
		$sql  .= "a.wkfw_type = b.wkfw_type) e on ";
		$sql  .= "b.wkfw_id = e.wkfw_id ";

		$cond .= "where not a.delete_flg and a.send_emp_id is not null and not b.draft_flg ";
		$cond .= "and a.recv_emp_id = '$emp_id' ";

		// 申請者名
		if($apply_emp_nm != "")
		{
			$apply_emp_nm = pg_escape_string($apply_emp_nm);
			$cond .= "and ";
			$cond .= "(d.emp_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or d.emp_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or d.emp_kn_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or d.emp_kn_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or (d.emp_lt_nm || d.emp_ft_nm) like '%$apply_emp_nm%' ";
			$cond .= "or (d.emp_kn_lt_nm || d.emp_kn_ft_nm) like '%$apply_emp_nm%') ";
		}

		// 申請状況
		if($apply_stat != "" && $apply_stat != "-")
		{
			$cond .= "and b.apply_stat = '$apply_stat' ";
		}

		// 部署
		if($class != "")
		{
			$cond .= "and b.emp_class = $class ";
		}

		if($attribute != "")
		{
			$cond .= "and b.emp_attribute = $attribute ";
		}

		if($dept != "")
		{
			$cond .= "and b.emp_dept = $dept ";
		}

		if($room != "")
		{
			$cond .= "and b.emp_room = $room ";
		}

		$cond .= "order by a.send_date desc, b.apply_date desc ";
		$cond .= "offset $page limit $max_page ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_all($sel);
	}


	// 申請結果通知一覧件数取得
	function get_applynotice_list_count($arr)
	{

		$session      = $arr["session"];
		$apply_emp_nm = $arr["apply_emp_nm"];
		$apply_stat   = $arr["apply_stat"];
		$class        = $arr["class"];
		$attribute    = $arr["attribute"];
		$dept         = $arr["dept"];
		$room         = $arr["room"];

		$arr_empmst = $this->get_empmst($session);
		$emp_id = $arr_empmst[0]["emp_id"];


		$sql   = "select ";
		$sql  .= "count(a.apply_id) as cnt ";
		$sql  .= "from fplusapplynotice a ";
		$sql  .= "inner join fplusapply b on a.apply_id = b.apply_id ";
		$sql  .= "inner join empmst c on a.send_emp_id = c.emp_id ";
		$sql  .= "inner join empmst d on b.emp_id = d.emp_id ";
		$sql  .= "inner join fpluswkfwmst e on b.wkfw_id = e.wkfw_id ";

		$cond .= "where not a.delete_flg and a.send_emp_id is not null and not b.draft_flg ";
		$cond .= "and a.recv_emp_id = '$emp_id' ";

		// 申請者名
		if($apply_emp_nm != "")
		{
			$apply_emp_nm = pg_escape_string($apply_emp_nm);
			$cond .= "and ";
			$cond .= "(d.emp_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or d.emp_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or d.emp_kn_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or d.emp_kn_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or (d.emp_lt_nm || d.emp_ft_nm) like '%$apply_emp_nm%' ";
			$cond .= "or (d.emp_kn_lt_nm || d.emp_kn_ft_nm) like '%$apply_emp_nm%') ";
		}

		// 申請状況
		if($apply_stat != "" && $apply_stat != "-")
		{
			$cond .= "and b.apply_stat = '$apply_stat' ";
		}

		// 部署
		if($class != "")
		{
			$cond .= "and b.emp_class = $class ";
		}

		if($attribute != "")
		{
			$cond .= "and b.emp_attribute = $attribute ";
		}

		if($dept != "")
		{
			$cond .= "and b.emp_dept = $dept ";
		}

		if($room != "")
		{
			$cond .= "and b.emp_room = $room ";
		}

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cnt");
	}

	// 申請結果通知一覧未確認件数取得
	function get_non_confirmed_notice_count($session)
	{
		$arr_empmst = $this->get_empmst($session);
		$emp_id = $arr_empmst[0]["emp_id"];

		$sql   = "select ";
		$sql  .= "count(a.apply_id) as cnt ";
		$sql  .= "from fplusapplynotice a ";
		$cond .= "where not a.delete_flg and not a.confirmed_flg ";
		$cond .= "and a.send_emp_id is not null and a.recv_emp_id = '$emp_id' ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cnt");
	}

	// 確認済みフラグ更新
	function update_confirmed_flg($apply_id, $session)
	{
		$arr_empmst = $this->get_empmst($session);
		$emp_id = $arr_empmst[0]["emp_id"];

		$sql = "update fplusapplynotice set";
		$set = array("confirmed_flg");
		$setvalue = array("t");
		$cond = "where not confirmed_flg and apply_id = $apply_id and recv_emp_id = '$emp_id' ";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

	}

//-------------------------------------------------------------------------------------------------------------------------
// 申請参照一覧関連
//-------------------------------------------------------------------------------------------------------------------------
	// 申請書件数取得(下書きは除く)
	function get_all_apply_count($delete_flg)
	{
		$sql  = "select count(*) as cnt from fplusapply ";
		$cond = "where not draft_flg and delete_flg = '$delete_flg' ";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cnt");
	}
        
        
        	// 申請書取得(下書きは除く)
	function get_apply_list_1($delete_flg, $page, $max_page, $selected_cate, $selected_folder, $selected_wkfw_id, $arr_condition,$part_aprv)
	{
                $page = ($page - 1) * 20;

		$category           = $arr_condition["category"];
		$workflow           = $arr_condition["workflow"];
		$apply_title        = $arr_condition["apply_title"];
		$apply_emp_nm       = $arr_condition["apply_emp_nm"];
		$apply_yyyy_from    = $arr_condition["date_y1"];
		$apply_mm_from      = $arr_condition["date_m1"];
		$apply_dd_from      = $arr_condition["date_d1"];
		$apply_yyyy_to      = $arr_condition["date_y2"];
		$apply_mm_to        = $arr_condition["date_m2"];
		$apply_dd_to        = $arr_condition["date_d2"];
                
                $occure_yyyy_from    = $arr_condition["date_y3"];
		$occure_mm_from      = $arr_condition["date_m3"];
		$occure_dd_from      = $arr_condition["date_d3"];
		$occure_yyyy_to      = $arr_condition["date_y4"];
		$occure_mm_to        = $arr_condition["date_m4"];
		$occure_dd_to        = $arr_condition["date_d4"];

                
		$apply_stat         = $arr_condition["apply_stat"];
		$class              = $arr_condition["class"];
		$attribute          = $arr_condition["attribute"];
		$dept               = $arr_condition["dept"];
		$room               = $arr_condition["room"];
		$apply_content      = $arr_condition["apply_content"];

                $sql   = "select * from ( ";
		$sql  .= "select ";
		$sql  .= "a.*, ";
		$sql  .= "b.wkfw_title, ";
		$sql  .= "b.short_wkfw_name, ";
                $sql  .= "b.wkfw_type, ";
		$sql  .= "c.emp_lt_nm, ";

                $sql .= "COALESCE(epi_a.data_2_1_arise_date,'') || COALESCE(epi_b.data_2_1_arise_date,'') as arise_date,  ";
               
                
		if(($part_aprv == "1") or ($part_aprv == "2"))
		{
			$sql  .= "d.apply_stat_detail, ";
		}

		$sql  .= "c.emp_ft_nm ";
		$sql  .= "from fplusapply a ";
                
                
                $sql .= "left join fplus_epi_a epi_a on epi_a.apply_id  = a.apply_id ";
                $sql .= "left join fplus_epi_b epi_b on epi_b.apply_id  = a.apply_id ";
                
                
		$sql  .= "left join fpluswkfwmst b on a.wkfw_id = b.wkfw_id ";
		$sql  .= "inner join empmst c on a.emp_id = c.emp_id ";

		if($part_aprv == "1")
		{
			//一部受付
			$sql  .= "inner join (select apply_id, '1' as apply_stat_detail from (select apply_id, max(apv_stat) as apv_stat from fplusapplyapv group by apply_id) Apv where apv_stat = 1) d on a.apply_id = d.apply_id ";
		}
		else if($part_aprv == "2")
		{
			//「受付待ち（一部受付を除く）」
			$sql  .= "inner join (select apply_id, '2' as apply_stat_detail from (select apply_id, max(apv_stat) as apv_stat from fplusapplyapv group by apply_id) Apv where apv_stat = 0) d on a.apply_id = d.apply_id ";
		}

		$cond .= ") as fplus where not fplus.draft_flg and fplus.delete_flg = '$delete_flg' ";

                //発生日
                $srch_oc_from_ymd='';
                $srch_oc_to_ymd='';
                if(ctype_digit($occure_yyyy_from)){
                    $srch_oc_from_ymd = "$occure_yyyy_from";
                }
                if(ctype_digit($occure_mm_from)){
                  $srch_oc_from_ymd .= "/$occure_mm_from";
                }
                if(ctype_digit($occure_dd_from)){
                    $srch_oc_from_ymd .= "/$occure_dd_from";
                }
                if(ctype_digit($occure_yyyy_to)){
                    $srch_oc_to_ymd = "$occure_yyyy_to";
                }
                if(ctype_digit($occure_mm_to)){
                    $srch_oc_to_ymd .= "/$occure_mm_to";
                }
                if(ctype_digit($occure_dd_to)){
                    $srch_oc_to_ymd .= "/$occure_dd_to";
                }
                
               if (preg_match("/^(\d+)/", $srch_oc_from_ymd, $matches) == 1) {
                    $cond .= "and fplus.arise_date >= '$srch_oc_from_ymd' ";
		}
                
		if (preg_match("/^(\d+)/", $srch_oc_to_ymd, $matches) == 1) {
                    $cond .= " and fplus.arise_date <= '$srch_oc_to_ymd' ";
		}
                  
                $cond .= " ";
                
		if($selected_cate != "")
		{
			$cond .= "and fplus.wkfw_type = $selected_cate ";
		}

		if($selected_folder != "")
		{
			$cond .= "and fplus.wkfw_folder_id = $selected_folder ";
		}

		if($selected_wkfw_id != "")
		{
			$cond .= "and fplus.wkfw_id = $selected_wkfw_id ";
		}


		// カテゴリ
		if($category != "" && $category != "-")
		{
			$cond .= "and fplus.wkfw_type = $category ";
		}

		// 申請書名
		if($workflow != "" &&  $workflow != "-")
		{
			$cond .= "and fplus.wkfw_id = $workflow ";
		}

		// 表題
		if($apply_title != "")
		{
			$apply_title = pg_escape_string($apply_title);
			$cond .= "and fplus.apply_title like '%$apply_title%' ";
		}

		// 申請者名
		if($apply_emp_nm != "")
		{
			$apply_emp_nm = pg_escape_string($apply_emp_nm);
			$cond .= "and ";
			$cond .= "(c.emp_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or fplus.emp_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or fplus.emp_kn_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or fplus.emp_kn_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or (fplus.emp_lt_nm || fplus.emp_ft_nm) like '%$apply_emp_nm%' ";
			$cond .= "or (fplus.emp_kn_lt_nm || fplus.emp_kn_ft_nm) like '%$apply_emp_nm%') ";
		}

		// 申請状況
		if($apply_stat != "" && $apply_stat != "-")
		{
			$cond .= "and fplus.apply_stat = '$apply_stat' ";
		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(a.apply_date from 1 for $date_len) >= '$date_str' ";
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(a.apply_date from 1 for $date_len) <= '$date_str' ";
		}
                
		// 部署
		if($class != "")
		{
			$cond .= "and fplus.emp_class = $class ";
		}

		if($attribute != "")
		{
			$cond .= "and fplus.emp_attribute = $attribute ";
		}

		if($dept != "")
		{
			$cond .= "and fplus.emp_dept = $dept ";
		}

		if($room != "")
		{
			$cond .= "and fplus.emp_room = $room ";
		}

		// 文字列検索
		if ($apply_content != "") {
			$search_keys = split(" ", $apply_content);
			for ($i = 0, $j = count($search_keys); $i < $j; $i++) {
				$tmp_cond[] = "fplus.apply_content like '%{$search_keys[$i]}%'";
			}
			$cond .= "and (" . join(" or ", $tmp_cond) . ")";
		}

		$cond .= "order by fplus.apply_date desc, fplus.apply_no desc ";

		if($max_page != "FALSE")
		{
			//件数を取得したい場合はFALSEになってその場合にはページ指定と限度は要らない
			$cond .= "offset $page limit $max_page ";
		}

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_all($sel);
                
        
        }

		// 申請書取得(下書きは除く)
	function get_apply_list($delete_flg, $page, $max_page, $selected_cate, $selected_folder, $selected_wkfw_id, $arr_condition,$part_aprv)
	{

		$page = ($page - 1) * 20;

		$category           = $arr_condition["category"];
		$workflow           = $arr_condition["workflow"];
		$apply_title        = $arr_condition["apply_title"];
		$apply_emp_nm       = $arr_condition["apply_emp_nm"];
		$apply_yyyy_from    = $arr_condition["date_y1"];
		$apply_mm_from      = $arr_condition["date_m1"];
		$apply_dd_from      = $arr_condition["date_d1"];
		$apply_yyyy_to      = $arr_condition["date_y2"];
		$apply_mm_to        = $arr_condition["date_m2"];
		$apply_dd_to        = $arr_condition["date_d2"];
		$apply_stat         = $arr_condition["apply_stat"];
		$class              = $arr_condition["class"];
		$attribute          = $arr_condition["attribute"];
		$dept               = $arr_condition["dept"];
		$room               = $arr_condition["room"];
		$apply_content      = $arr_condition["apply_content"];

		$sql   = "select ";
		$sql  .= "a.*, ";
		$sql  .= "b.wkfw_title, ";
		$sql  .= "b.short_wkfw_name, ";
		$sql  .= "c.emp_lt_nm, ";

		if(($part_aprv == "1") or ($part_aprv == "2"))
		{
			$sql  .= "d.apply_stat_detail, ";
		}

		$sql  .= "c.emp_ft_nm ";
		$sql  .= "from fplusapply a ";
		$sql  .= "left join fpluswkfwmst b on a.wkfw_id = b.wkfw_id ";
		$sql  .= "inner join empmst c on a.emp_id = c.emp_id ";

		if($part_aprv == "1")
		{
			//一部受付
			$sql  .= "inner join (select apply_id, '1' as apply_stat_detail from (select apply_id, max(apv_stat) as apv_stat from fplusapplyapv group by apply_id) Apv where apv_stat = 1) d on a.apply_id = d.apply_id ";
		}
		else if($part_aprv == "2")
		{
			//「受付待ち（一部受付を除く）」
			$sql  .= "inner join (select apply_id, '2' as apply_stat_detail from (select apply_id, max(apv_stat) as apv_stat from fplusapplyapv group by apply_id) Apv where apv_stat = 0) d on a.apply_id = d.apply_id ";
		}

		$cond .= "where not a.draft_flg and a.delete_flg = '$delete_flg' ";

		if($selected_cate != "")
		{
			$cond .= "and b.wkfw_type = $selected_cate ";
		}

		if($selected_folder != "")
		{
			$cond .= "and b.wkfw_folder_id = $selected_folder ";
		}

		if($selected_wkfw_id != "")
		{
			$cond .= "and b.wkfw_id = $selected_wkfw_id ";
		}


		// カテゴリ
		if($category != "" && $category != "-")
		{
			$cond .= "and b.wkfw_type = $category ";
		}

		// 申請書名
		if($workflow != "" &&  $workflow != "-")
		{
			$cond .= "and b.wkfw_id = $workflow ";
		}

		// 表題
		if($apply_title != "")
		{
			$apply_title = pg_escape_string($apply_title);
			$cond .= "and a.apply_title like '%$apply_title%' ";
		}

		// 申請者名
		if($apply_emp_nm != "")
		{
			$apply_emp_nm = pg_escape_string($apply_emp_nm);
			$cond .= "and ";
			$cond .= "(c.emp_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or c.emp_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or c.emp_kn_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or c.emp_kn_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or (c.emp_lt_nm || c.emp_ft_nm) like '%$apply_emp_nm%' ";
			$cond .= "or (c.emp_kn_lt_nm || c.emp_kn_ft_nm) like '%$apply_emp_nm%') ";
		}

		// 申請状況
		if($apply_stat != "" && $apply_stat != "-")
		{
			$cond .= "and a.apply_stat = '$apply_stat' ";
		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(a.apply_date from 1 for $date_len) >= '$date_str' ";
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(a.apply_date from 1 for $date_len) <= '$date_str' ";
		}


		// 部署
		if($class != "")
		{
			$cond .= "and a.emp_class = $class ";
		}

		if($attribute != "")
		{
			$cond .= "and a.emp_attribute = $attribute ";
		}

		if($dept != "")
		{
			$cond .= "and a.emp_dept = $dept ";
		}

		if($room != "")
		{
			$cond .= "and a.emp_room = $room ";
		}

		// 文字列検索
		if ($apply_content != "") {
			$search_keys = split(" ", $apply_content);
			for ($i = 0, $j = count($search_keys); $i < $j; $i++) {
				$tmp_cond[] = "a.apply_content like '%{$search_keys[$i]}%'";
			}
			$cond .= "and (" . join(" or ", $tmp_cond) . ")";
		}

		$cond .= "order by a.apply_date desc, a.apply_no desc ";

		if($max_page != "FALSE")
		{
			//件数を取得したい場合はFALSEになってその場合にはページ指定と限度は要らない
			$cond .= "offset $page limit $max_page ";
		}

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_all($sel);
	

	}
        
        	// 申請書件数取得(下書きは除く)
	function get_apply_list_count1($delete_flg, $selected_cate, $selected_folder, $selected_wkfw_id, $arr_condition,$part_aprv)
	{
            	$category           = $arr_condition["category"];
		$workflow           = $arr_condition["workflow"];
		$apply_title        = $arr_condition["apply_title"];
		$apply_emp_nm       = $arr_condition["apply_emp_nm"];
		$apply_yyyy_from    = $arr_condition["date_y1"];
		$apply_mm_from      = $arr_condition["date_m1"];
		$apply_dd_from      = $arr_condition["date_d1"];
		$apply_yyyy_to      = $arr_condition["date_y2"];
		$apply_mm_to        = $arr_condition["date_m2"];
		$apply_dd_to        = $arr_condition["date_d2"];
                
                $occure_yyyy_from    = $arr_condition["date_y3"];
		$occure_mm_from      = $arr_condition["date_m3"];
		$occure_dd_from      = $arr_condition["date_d3"];
		$occure_yyyy_to      = $arr_condition["date_y4"];
		$occure_mm_to        = $arr_condition["date_m4"];
		$occure_dd_to        = $arr_condition["date_d4"];

                
		$apply_stat         = $arr_condition["apply_stat"];
		$class              = $arr_condition["class"];
		$attribute          = $arr_condition["attribute"];
		$dept               = $arr_condition["dept"];
		$room               = $arr_condition["room"];
		$apply_content      = $arr_condition["apply_content"];

                $sql   = "select count(*) as cnt from ( ";
		$sql  .= "select ";
		$sql  .= "a.*, ";
		$sql  .= "b.wkfw_title, ";
		$sql  .= "b.short_wkfw_name, ";
                $sql  .= "b.wkfw_type, ";
                $sql  .= "c.emp_lt_nm, ";

                $sql .= "COALESCE(epi_a.data_2_1_arise_date,'') || COALESCE(epi_b.data_2_1_arise_date,'') as arise_date,  ";
               
                
		if(($part_aprv == "1") or ($part_aprv == "2"))
		{
			$sql  .= "d.apply_stat_detail, ";
		}

		$sql  .= "c.emp_ft_nm ";
		$sql  .= "from fplusapply a ";
                
                
                $sql .= "left join fplus_epi_a epi_a on epi_a.apply_id  = a.apply_id ";
                $sql .= "left join fplus_epi_b epi_b on epi_b.apply_id  = a.apply_id ";
                
                
		$sql  .= "left join fpluswkfwmst b on a.wkfw_id = b.wkfw_id ";
		$sql  .= "inner join empmst c on a.emp_id = c.emp_id ";

		if($part_aprv == "1")
		{
			//一部受付
			$sql  .= "inner join (select apply_id, '1' as apply_stat_detail from (select apply_id, max(apv_stat) as apv_stat from fplusapplyapv group by apply_id) Apv where apv_stat = 1) d on a.apply_id = d.apply_id ";
		}
		else if($part_aprv == "2")
		{
			//「受付待ち（一部受付を除く）」
			$sql  .= "inner join (select apply_id, '2' as apply_stat_detail from (select apply_id, max(apv_stat) as apv_stat from fplusapplyapv group by apply_id) Apv where apv_stat = 0) d on a.apply_id = d.apply_id ";
		}

		$cond .= ") as fplus where not fplus.draft_flg and fplus.delete_flg = '$delete_flg' ";

                //発生日
                $srch_oc_from_ymd='';
                $srch_oc_to_ymd='';
                if(ctype_digit($occure_yyyy_from)){
                    $srch_oc_from_ymd = "$occure_yyyy_from";
                }
                if(ctype_digit($occure_mm_from)){
                  $srch_oc_from_ymd .= "/$occure_mm_from";
                }
                if(ctype_digit($occure_dd_from)){
                    $srch_oc_from_ymd .= "/$occure_dd_from";
                }
                if(ctype_digit($occure_yyyy_to)){
                    $srch_oc_to_ymd = "$occure_yyyy_to";
                }
                if(ctype_digit($occure_mm_to)){
                    $srch_oc_to_ymd .= "/$occure_mm_to";
                }
                if(ctype_digit($occure_dd_to)){
                    $srch_oc_to_ymd .= "/$occure_dd_to";
                }
                
               if (preg_match("/^(\d+)/", $srch_oc_from_ymd, $matches) == 1) {
                    $cond .= "and fplus.arise_date >= '$srch_oc_from_ymd' ";
		}
                
		if (preg_match("/^(\d+)/", $srch_oc_to_ymd, $matches) == 1) {
                    $cond .= " and fplus.arise_date <= '$srch_oc_to_ymd' ";
		}
                  
                $cond .= " ";
                
		if($selected_cate != "")
		{
			$cond .= "and fplus.wkfw_type = $selected_cate ";
		}

		if($selected_folder != "")
		{
			$cond .= "and fplus.wkfw_folder_id = $selected_folder ";
		}

		if($selected_wkfw_id != "")
		{
			$cond .= "and fplus.wkfw_id = $selected_wkfw_id ";
		}


		// カテゴリ
		if($category != "" && $category != "-")
		{
			$cond .= "and fplus.wkfw_type = $category ";
		}

		// 申請書名
		if($workflow != "" &&  $workflow != "-")
		{
			$cond .= "and fplus.wkfw_id = $workflow ";
		}

		// 表題
		if($apply_title != "")
		{
			$apply_title = pg_escape_string($apply_title);
			$cond .= "and fplus.apply_title like '%$apply_title%' ";
		}

		// 申請者名
		if($apply_emp_nm != "")
		{
			$apply_emp_nm = pg_escape_string($apply_emp_nm);
			$cond .= "and ";
			$cond .= "(c.emp_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or fplus.emp_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or fplus.emp_kn_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or fplus.emp_kn_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or (fplus.emp_lt_nm || fplus.emp_ft_nm) like '%$apply_emp_nm%' ";
			$cond .= "or (fplus.emp_kn_lt_nm || fplus.emp_kn_ft_nm) like '%$apply_emp_nm%') ";
		}

		// 申請状況
		if($apply_stat != "" && $apply_stat != "-")
		{
			$cond .= "and fplus.apply_stat = '$apply_stat' ";
		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(a.apply_date from 1 for $date_len) >= '$date_str' ";
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(a.apply_date from 1 for $date_len) <= '$date_str' ";
		}
                
		// 部署
		if($class != "")
		{
			$cond .= "and fplus.emp_class = $class ";
		}

		if($attribute != "")
		{
			$cond .= "and fplus.emp_attribute = $attribute ";
		}

		if($dept != "")
		{
			$cond .= "and fplus.emp_dept = $dept ";
		}

		if($room != "")
		{
			$cond .= "and fplus.emp_room = $room ";
		}

		// 文字列検索
		if ($apply_content != "") {
			$search_keys = split(" ", $apply_content);
			for ($i = 0, $j = count($search_keys); $i < $j; $i++) {
				$tmp_cond[] = "fplus.apply_content like '%{$search_keys[$i]}%'";
			}
			$cond .= "and (" . join(" or ", $tmp_cond) . ")";
		}
                
		
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
                
                
                return pg_fetch_result($sel, 0,"cnt");
                
        }

	// 申請書件数取得(下書きは除く)
	function get_apply_list_count($delete_flg, $selected_cate, $selected_folder, $selected_wkfw_id, $arr_condition,$part_aprv)
	{

		$category           = $arr_condition["category"];
		$workflow           = $arr_condition["workflow"];
		$apply_title        = $arr_condition["apply_title"];
		$apply_emp_nm       = $arr_condition["apply_emp_nm"];
		$apply_yyyy_from    = $arr_condition["date_y1"];
		$apply_mm_from      = $arr_condition["date_m1"];
		$apply_dd_from      = $arr_condition["date_d1"];
		$apply_yyyy_to      = $arr_condition["date_y2"];
		$apply_mm_to        = $arr_condition["date_m2"];
		$apply_dd_to        = $arr_condition["date_d2"];
		$apply_stat         = $arr_condition["apply_stat"];
		$class              = $arr_condition["class"];
		$attribute          = $arr_condition["attribute"];
		$dept               = $arr_condition["dept"];
		$room               = $arr_condition["room"];
		$apply_content      = $arr_condition["apply_content"];

		$sql   = "select ";
		$sql  .= "count(a.apply_id) as cnt ";
		$sql  .= "from fplusapply a ";
		$sql  .= "left join fpluswkfwmst b on a.wkfw_id = b.wkfw_id ";
		$sql  .= "inner join empmst c on a.emp_id = c.emp_id ";
		if($part_aprv == "1")
		{
			//一部受付
			$sql  .= "inner join (select apply_id, '1' as apply_stat_detail from (select apply_id, max(apv_stat) as apv_stat from fplusapplyapv group by apply_id) Apv where apv_stat = 1) d on a.apply_id = d.apply_id ";
		}
		else if($part_aprv == "2")
		{
			//「受付待ち（一部受付を除く）」
			$sql  .= "inner join (select apply_id, '2' as apply_stat_detail from (select apply_id, max(apv_stat) as apv_stat from fplusapplyapv group by apply_id) Apv where apv_stat = 0) d on a.apply_id = d.apply_id ";
		}


		$cond .= "where not a.draft_flg and a.delete_flg = '$delete_flg' ";

		if($selected_cate != "")
		{
			$cond .= "and b.wkfw_type = $selected_cate ";
		}

		if($selected_folder != "")
		{
			$cond .= "and b.wkfw_folder_id = $selected_folder ";
		}

		if($selected_wkfw_id != "")
		{
			$cond .= "and b.wkfw_id = $selected_wkfw_id ";
		}

		// カテゴリ
		if($category != "" && $category != "-")
		{
			$cond .= "and b.wkfw_type = $category ";
		}

		// 申請書名
		if($workflow != "" &&  $workflow != "-")
		{
			$cond .= "and b.wkfw_id = $workflow ";
		}

		// 表題
		if($apply_title != "")
		{
			$apply_title = pg_escape_string($apply_title);
			$cond .= "and a.apply_title like '%$apply_title%' ";
		}

		// 申請者名
		if($apply_emp_nm != "")
		{
			$apply_emp_nm = pg_escape_string($apply_emp_nm);
			$cond .= "and ";
			$cond .= "(c.emp_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or c.emp_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or c.emp_kn_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or c.emp_kn_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or (c.emp_lt_nm || c.emp_ft_nm) like '%$apply_emp_nm%' ";
			$cond .= "or (c.emp_kn_lt_nm || c.emp_kn_ft_nm) like '%$apply_emp_nm%') ";
		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(a.apply_date from 1 for $date_len) >= '$date_str' ";
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(a.apply_date from 1 for $date_len) <= '$date_str' ";
		}

		// 申請状況
		if($apply_stat != "" && $apply_stat != "-")
		{
			$cond .= "and a.apply_stat = '$apply_stat' ";
		}

		// 部署
		if($class != "")
		{
			$cond .= "and a.emp_class = $class ";
		}

		if($attribute != "")
		{
			$cond .= "and a.emp_attribute = $attribute ";
		}

		if($dept != "")
		{
			$cond .= "and a.emp_dept = $dept ";
		}

		if($room != "")
		{
			$cond .= "and a.emp_room = $room ";
		}

		// 文字列検索
		if ($apply_content != "") {
			$search_keys = split(" ", $apply_content);
			for ($i = 0, $j = count($search_keys); $i < $j; $i++) {
				$tmp_cond[] = "a.apply_content like '%{$search_keys[$i]}%'";
			}
			$cond .= "and (" . join(" or ", $tmp_cond) . ")";
		}

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0,"cnt");
	}


//-------------------------------------------------------------------------------------------------------------------------
// テンプレート履歴およびワークファイル履歴関連
//-------------------------------------------------------------------------------------------------------------------------

	// テンプレート履歴登録
	function regist_wkfw_template_history($wkfw_id, $wkfw_history_no, $wkfw_content)
	{
		$sql = "insert into fplus_template_history (wkfw_id, wkfw_history_no, wkfw_content) values (";
		$content = array($wkfw_id, $wkfw_history_no, pg_escape_string($wkfw_content));
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// テンプレート履歴取得
	function get_wkfw_template_history($wkfw_id, $wkfw_history_no)
	{
		$sql  = "select * from fplus_template_history ";
		$cond = "where wkfw_id = $wkfw_id and wkfw_history_no = $wkfw_history_no";
		$sel  = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}


		$wkfw_history_no = pg_fetch_result($sel, 0, "wkfw_history_no");
		$wkfw_content    = pg_fetch_result($sel, 0, "wkfw_content");

		$arr = array("wkfw_history_no" => $wkfw_history_no, "wkfw_content" => $wkfw_content);
		return $arr;
	}

	// テンプレート履歴No(ＭＡＸ値)取得
	function get_max_wkfw_history_no($wkfw_id)
	{
		$sql  = "select max(wkfw_history_no) as max from fplus_template_history ";
		$cond = "where wkfw_id = $wkfw_id ";
		$sel  = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "max");
	}

	// ワークファイル履歴登録
	function regist_wkfwfile_history($wkfw_id, $wkfwfile_no, $wkfwfile_history_no, $wkfwfile_name)
	{
		$sql = "insert into fpluswkfwfile_history (wkfw_id, wkfwfile_no, wkfwfile_history_no, wkfwfile_name) values (";
		$content = array($wkfw_id, $wkfwfile_no, $wkfwfile_history_no, pg_escape_string($wkfwfile_name));
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークファイル履歴取得
	function get_wkfwfile_history($wkfw_id, $wkfwfile_history_no)
	{
		$sql  = "select * from fpluswkfwfile_history ";
		$cond = "where wkfw_id = $wkfw_id and wkfwfile_history_no = $wkfwfile_history_no order by wkfwfile_no ";
		$sel  = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfwfile_history_no" => $row["wkfwfile_history_no"], "wkfwfile_no" => $row["wkfwfile_no"], "wkfwfile_name" => $row["wkfwfile_name"]);
		}
		return $arr;
	}

	// ワークファイル履歴No(ＭＡＸ値)取得
	function get_max_wkfwfile_history_no($wkfw_id)
	{
		$sql  = "select max(wkfwfile_history_no) as max from fpluswkfwfile_history ";
		$cond = "where wkfw_id = $wkfw_id ";
		$sel  = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "max");
	}

	// ワークファイルNo(ＭＡＸ値)取得
	function get_max_wkfwfile_no($wkfw_id)
	{
		$sql  = "select max(wkfwfile_no) as max from fpluswkfwfile_history ";
		$cond = "where wkfw_id = $wkfw_id ";
		$sel  = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "max");
	}

//-------------------------------------------------------------------------------------------------------------------------
// ワークフロー・本体情報関連
//-------------------------------------------------------------------------------------------------------------------------

	// 削除済みワークフロー取得
	function get_deleted_workflow_real()
	{
		$sql  = "select wkfw_id, wkfw_title from fpluswkfwmst_real";
		$cond = "where wkfw_del_flg order by wkfw_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);

		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_id" => $row["wkfw_id"], "wkfw_title" => $row["wkfw_title"]);
		}
		return $arr;
	}

	// 削除済みワークフロー数取得
	function get_deleted_workflow_real_cnt()
	{
		$sql  = "select count(*) as cnt from fpluswkfwmst_real";
		$cond = "where wkfw_del_flg";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);

		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$num = pg_fetch_result($sel, 0, "cnt");
		return $num;
	}

	// すべてフォルダ用ワークフロー取得
	function get_wkfwmst_real()
	{
		$sql  = "select wkfw_id, wkfw_title from fpluswkfwmst_real";
		$cond = "where not wkfw_del_flg order by wkfw_id";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);

		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_id" => $row["wkfw_id"], "wkfw_title" => $row["wkfw_title"]);
		}
		return $arr;
	}

	// すべてフォルダ用ワークフロー数取得
	function get_wkfwmst_real_cnt()
	{
		$sql  = "select count(*) as cnt from fpluswkfwmst_real";
		$cond = "where not wkfw_del_flg";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);

		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$num = pg_fetch_result($sel, 0, "cnt");
		return $num;
	}

	// ワークフローＩＤのＭＡＸ値取得
	function get_max_wkfw_id($mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "select max(wkfw_id) as max from fpluswkfwmst";
		}
		else
		{
			$sql = "select max(wkfw_id) as max from fpluswkfwmst_real";
		}
		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);

		if ($sel == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0, "max");
	}

	// エイリアスワークフローＩＤ取得
	function get_alias_wkfw_id($real_wkfw_id)
	{
		$sql  = "select alias_wkfw_id from fpluswkfwaliasmng";
		$cond = "where real_wkfw_id = $real_wkfw_id order by alias_wkfw_no asc";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("alias_wkfw_id" => $row["alias_wkfw_id"]);
		}
		return $arr;
	}

	// エイリアス管理登録
	function regist_wkfwaliasmng($real_wkfw_id, $alias_wkfw_id, $alias_wkfw_no)
	{
		$sql = "insert into fpluswkfwaliasmng (real_wkfw_id, alias_wkfw_id, alias_wkfw_no, delete_flg) values (";
		$content = array($real_wkfw_id, $alias_wkfw_id, $alias_wkfw_no, "f");
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// エイリアスワークフローＮｏ(ＭＡＸ)取得
	function get_max_alias_wkfw_no($real_wkfw_id)
	{
		$sql  = "select max(alias_wkfw_no) from fpluswkfwaliasmng";
		$cond = "where real_wkfw_id = $real_wkfw_id";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "max");
	}

//-------------------------------------------------------------------------------------------------------------------------
// エイリアスワークフローコピー処理
//-------------------------------------------------------------------------------------------------------------------------

	// ワークフロー情報登録
	function regist_copy_wkfwmst($real_wkfw_id, $alias_wkfw_id, $wkfw_type, $wkfw_folder_id)
	{
		$wkfw_folder_id = ($wkfw_folder_id == "") ? "null" : $wkfw_folder_id;

		$sql  = "insert into fpluswkfwmst( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "$wkfw_type, ";
		$sql .= "wkfw_title, ";
		$sql .= "wkfw_content, ";
		$sql .= "'f', ";
		$sql .= "wkfw_start_date, ";
		$sql .= "wkfw_end_date, ";
		$sql .= "wkfw_appr, ";
		$sql .= "wkfw_content_type, ";
		$sql .= "$wkfw_folder_id, ";
		$sql .= "ref_dept_st_flg, ";
		$sql .= "ref_dept_flg, ";
		$sql .= "ref_st_flg, ";
		$sql .= "short_wkfw_name, ";
		$sql .= "apply_title_disp_flg, ";
		$sql .= "send_mail_flg, ";
		$sql .= "approve_label, ";
		$sql .= "lib_reg_flg, ";
		$sql .= "lib_keyword, ";
		$sql .= "lib_no, ";
		$sql .= "lib_summary, ";
		$sql .= "lib_archive, ";
		$sql .= "lib_cate_id, ";
		$sql .= "lib_folder_id, ";
		$sql .= "lib_show_login_flg, ";
		$sql .= "lib_show_login_begin, ";
		$sql .= "lib_show_login_end, ";
		$sql .= "lib_private_flg, ";
		$sql .= "lib_ref_dept_st_flg, ";
		$sql .= "lib_ref_dept_flg, ";
		$sql .= "lib_ref_st_flg, ";
		$sql .= "lib_upd_dept_st_flg, ";
		$sql .= "lib_upd_dept_flg, ";
		$sql .= "lib_upd_st_flg ";
		$sql .= "from fpluswkfwmst_real ";
		$sql .= "where wkfw_id = $real_wkfw_id ";
		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 承認者管理登録
	function regist_copy_wkfwapvmng($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into fplusapvmng( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "apv_order, ";
		$sql .= "deci_flg, ";
		$sql .= "target_class_div, ";
		$sql .= "multi_apv_flg, ";
		$sql .= "next_notice_div, ";
		$sql .= "apv_div0_flg, ";
		$sql .= "apv_div1_flg, ";
		$sql .= "apv_div2_flg, ";
		$sql .= "apv_div3_flg, ";
		$sql .= "apv_div4_flg, ";
		$sql .= "apv_num ";
		$sql .= "from fplusapvmng_real ";
		$sql .= "where wkfw_id = $real_wkfw_id order by apv_order";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 職員指定登録
	function regist_copy_wkfwapvdtl($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into fplusapvdtl( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "apv_order, ";
		$sql .= "emp_id, ";
		$sql .= "apv_sub_order ";
		$sql .= "from fplusapvdtl_real ";
		$sql .= "where wkfw_id = $real_wkfw_id order by apv_order, apv_sub_order";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職(申請者所属)指定登録
	function regist_copy_wkfwapvpstdtl($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into fplusapvpstdtl( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "apv_order, ";
		$sql .= "st_id, ";
		$sql .= "st_div ";
		$sql .= "from fplusapvpstdtl_real ";
		$sql .= "where wkfw_id = $real_wkfw_id order by apv_order ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 委員会・ＷＧ指定登録
	function regist_copy_wkfwpjtdtl($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into fpluswkfwpjtdtl( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "apv_order, ";
		$sql .= "parent_pjt_id, ";
		$sql .= "child_pjt_id ";
		$sql .= "from fpluswkfwpjtdtl_real ";
		$sql .= "where wkfw_id = $real_wkfw_id order by apv_order ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職(部署指定)指定登録
	function regist_copy_wkfwapvsectdtl($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into fplusapvsectdtl( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "apv_order, ";
		$sql .= "class_id, ";
		$sql .= "atrb_id, ";
		$sql .= "dept_id, ";
		$sql .= "room_id ";
		$sql .= "from fplusapvsectdtl_real ";
		$sql .= "where wkfw_id = $real_wkfw_id order by apv_order ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 結果通知管理登録
	function regist_copy_wkfwnoticemng($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into fplusnoticemng( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "target_class_div, ";
		$sql .= "rslt_ntc_div0_flg, ";
		$sql .= "rslt_ntc_div1_flg, ";
		$sql .= "rslt_ntc_div2_flg, ";
		$sql .= "rslt_ntc_div3_flg, ";
		$sql .= "rslt_ntc_div4_flg ";
		$sql .= "from fplusnoticemng_real ";
		$sql .= "where wkfw_id = $real_wkfw_id ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 職員指定(結果通知)登録
	function regist_copy_wkfwnoticedtl($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into fplusnoticedtl( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "emp_id ";
		$sql .= "from fplusnoticedtl_real ";
		$sql .= "where wkfw_id = $real_wkfw_id ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職指定(結果通知)登録
	function regist_copy_wkfwnoticestdtl($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into fplusnoticestdtl( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "st_id, ";
		$sql .= "st_div ";
		$sql .= "from fplusnoticestdtl_real ";
		$sql .= "where wkfw_id = $real_wkfw_id ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 委員会・ＷＧ指定(結果通知)登録
	function regist_copy_wkfwnoticepjtdtl($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into fplusnoticepjtdtl( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "parent_pjt_id, ";
		$sql .= "child_pjt_id ";
		$sql .= "from fplusnoticepjtdtl_real ";
		$sql .= "where wkfw_id = $real_wkfw_id ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職指定(結果通知)(部署指定)登録
	function regist_copy_wkfwnoticesectdtl($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into fplusnoticesectdtl( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "class_id, ";
		$sql .= "atrb_id, ";
		$sql .= "dept_id, ";
		$sql .= "room_id ";
		$sql .= "from fplusnoticesectdtl_real ";
		$sql .= "where wkfw_id = $real_wkfw_id ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 前提とする申請書(ワークフロー用)登録
	function regist_copy_wkfwfprecond($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into fplusfprecond( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "precond_order, ";
		$sql .= "precond_wkfw_id ";
		$sql .= "from fplusfprecond_real ";
		$sql .= "where wkfw_id = $real_wkfw_id order by precond_order";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー用アクセス権（科）登録
	function regist_copy_wkfw_refdept($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into fpluswkfw_refdept( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "class_id, ";
		$sql .= "atrb_id, ";
		$sql .= "dept_id ";
		$sql .= "from fpluswkfw_refdept_real ";
		$sql .= "where wkfw_id = $real_wkfw_id ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー用アクセス権（役職）登録
	function regist_copy_wkfw_refst($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into fpluswkfw_refst( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "st_id ";
		$sql .= "from fpluswkfw_refst_real ";
		$sql .= "where wkfw_id = $real_wkfw_id ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー用アクセス権（職員）登録
	function regist_copy_wkfw_refemp($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into fpluswkfw_refemp( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "emp_id ";
		$sql .= "from fpluswkfw_refemp_real ";
		$sql .= "where wkfw_id = $real_wkfw_id ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// フォーマットファイル登録
	function regist_copy_wkfwfile($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into fpluswkfwfile( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "wkfwfile_no, ";
		$sql .= "wkfwfile_name ";
		$sql .= "from fpluswkfwfile_real ";
		$sql .= "where wkfw_id = $real_wkfw_id ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

//-------------------------------------------------------------------------------------------------------------------------
// ワークフロー・ドラッグアンドドロップ処理
//-------------------------------------------------------------------------------------------------------------------------

	// 指定フォルダの子フォルダ取得
	function get_child_folder($wkfw_parent_id, &$res)
	{
		$arr_wkfw_child_folder = $this->get_wkfw_child_folder($wkfw_parent_id);
		foreach($arr_wkfw_child_folder as $wkfw_child_folder)
		{
			$res[] = $wkfw_child_folder;

			$this->get_child_folder($wkfw_child_folder, $res);
		}
	}

	// 子フォルダ取得
	function get_wkfw_child_folder($wkfw_parent_id)
	{
		$sql  = "select wkfw_child_id from fplustree ";
		$cond = "where wkfw_parent_id = $wkfw_parent_id and not wkfw_tree_del_flg ";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = $row["wkfw_child_id"];
		}
		return $arr;
	}

	// 指定フォルダの親フォルダ取得
	function get_parent_folder($wkfw_child_id)
	{
		// 親フォルダ情報を取得
		$sql = "select wkfw_parent_id from fplustree";
		$cond = "where wkfw_child_id = $wkfw_child_id and not wkfw_tree_del_flg ";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "wkfw_parent_id");
	}

	// カテゴリ論理削除
	function update_wkfwcatemst_delflg($wkfw_type, $wkfwcate_del_flg)
	{
		$sql = "update fpluscatemst set";
		$set = array("wkfwcate_del_flg");
		$setvalue = array($wkfwcate_del_flg);
		$cond = "where wkfw_type = $wkfw_type";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// フォルダ取得(wkfw_folder_idの最大値取得)
	function get_max_wkfw_folder_id()
	{
		$sql = "select max(wkfw_folder_id) as max from fplusfolder";
		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0, "max");
	}

	// カテゴリ取得(wkfw_typeの最大値取得)
	function get_max_wkfw_type()
	{
		$sql = "select max(wkfw_type) as max from fpluscatemst";
		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0, "max");
	}

	// ワークフローフォルダ登録
	function regist_wkfwfolder($arr)
	{
		$wkfw_folder_id      = $arr["wkfw_folder_id"];
		$wkfw_type           = $arr["wkfw_type"];
		$wkfw_folder_name    = $arr["wkfw_folder_name"];
		$ref_dept_st_flg     = $arr["ref_dept_st_flg"];
		$ref_dept_flg        = $arr["ref_dept_flg"];
		$ref_st_flg          = $arr["ref_st_flg"];

		$sql = "insert into fplusfolder (wkfw_folder_id, wkfw_type, wkfw_folder_name, wkfw_folder_del_flg, ref_dept_st_flg, ref_dept_flg, ref_st_flg) values (";
		$content = array($wkfw_folder_id, $wkfw_type, pg_escape_string($wkfw_folder_name), "f", $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg);
		$result = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($result == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// フォルダ用アクセス権（科）登録
	function regist_wkfwfolder_refdept($wkfw_folder_id, $class_id, $atrb_id, $dept_id)
	{
		$sql = "insert into fplusfolder_refdept (wkfw_folder_id, class_id, atrb_id, dept_id) values (";
		$content = array($wkfw_folder_id, $class_id, $atrb_id, $dept_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// フォルダ用アクセス権（役職）登録
	function regist_wkfwfolder_refst($wkfw_folder_id, $st_id)
	{
		$sql = "insert into fplusfolder_refst (wkfw_folder_id, st_id) values (";
		$content = array($wkfw_folder_id, $st_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// フォルダ用アクセス権（職員）登録
	function regist_wkfwfolder_refemp($wkfw_folder_id, $emp_id)
	{
		$sql = "insert into fplusfolder_refemp (wkfw_folder_id, emp_id) values (";
		$content = array($wkfw_folder_id, $emp_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー申請書・ワークフロータイプ更新
	function update_wkfwmst_wkfw_type($src_wkfw_type, $dest_wkfw_type)
	{
		$sql = "update fpluswkfwmst set";
		$set = array("wkfw_type");
		$setvalue = array($dest_wkfw_type);
		$cond = "where wkfw_type = $src_wkfw_type";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー申請書・ファルダＩＤ更新
	function update_wkfwmst_wkfw_folder_id($new_wkfw_folder_id, $wkfw_id)
	{
		$sql = "update fpluswkfwmst set";
		$set = array("wkfw_folder_id");
		$setvalue = array($new_wkfw_folder_id);
		$cond = "where wkfw_id = $wkfw_id and wkfw_folder_id is null";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフローＩ取得
	function get_wkfw_id_for_categoryDD($dest_wkfw_type)
	{
		$sql  = "select wkfw_id from fpluswkfwmst ";
		$cond = "where wkfw_type = $dest_wkfw_type and wkfw_folder_id is null and not wkfw_del_flg ";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = $row["wkfw_id"];
		}
		return $arr;

	}

	// ワークフローフォルダ・ワークフロータイプ更新
	function update_wkfwfolder_wkfw_type($src_wkfw_type, $dest_wkfw_type)
	{
		$sql = "update fplusfolder set";
		$set = array("wkfw_type");
		$setvalue = array($dest_wkfw_type);
		$cond = "where wkfw_type = $src_wkfw_type";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// フォルダツリー登録
	function regist_wkfwtree($wkfw_parent_id, $wkfw_child_id)
	{
		$sql = "insert into fplustree (wkfw_parent_id, wkfw_child_id, wkfw_tree_del_flg) values (";
		$content = array($wkfw_parent_id, $wkfw_child_id, "f");
		$result = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($result == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// フォルダツリー論理削除
	function update_wkfwtree_del_flg($wkfw_child_id, $wkfw_tree_del_flg)
	{
		$sql = "update fplustree set";
		$set = array("wkfw_tree_del_flg");
		$setvalue = array($wkfw_tree_del_flg);
		$cond = "where wkfw_child_id = $wkfw_child_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// フォルダツリー論理削除
	function update_wkfwtree_del_flg_for_parent_id($wkfw_parent_id, $wkfw_tree_del_flg)
	{
		$sql = "update fplustree set";
		$set = array("wkfw_tree_del_flg");
		$setvalue = array($wkfw_tree_del_flg);
		$cond = "where wkfw_parent_id = $wkfw_parent_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// フォルダツリー・親フォルダＩＤ更新
	function update_wkfwtree_wkfw_parent_id($wkfw_parent_id, $wkfw_child_id)
	{
		$sql = "update fplustree set";
		$set = array("wkfw_parent_id");
		$setvalue = array($wkfw_parent_id);
		$cond = "where wkfw_child_id = $wkfw_child_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// フォルダツリー・件数取得
	function get_wkfwtree_count($wkfw_child_id)
	{
		$sql  = "select count(wkfw_parent_id) as cnt from fplustree";
		$cond = "where not wkfw_tree_del_flg and wkfw_child_id = $wkfw_child_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0, "cnt");
	}

	// ワークフローフォルダＩＤ取得
	function get_wkfwfolder_from_wkfw_type($wkfw_type)
	{
		$sql  = "select wkfw_folder_id from fplusfolder";
		$cond = "where not wkfw_folder_del_flg and wkfw_type = $wkfw_type order by wkfw_folder_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = $row["wkfw_folder_id"];
		}
		return $arr;
	}


	// ワークフローフォルダ・ワークフロータイプ更新
	function update_wkfwfolder_wkfw_type_from_folder_id($wkfw_folder_id, $dest_wkfw_type)
	{
		$sql = "update fplusfolder set";
		$set = array("wkfw_type");
		$setvalue = array($dest_wkfw_type);
		$cond = "where wkfw_folder_id = $wkfw_folder_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー申請書・ワークフロータイプ更新
	function update_wkfwmst_wkfw_type_from_folder_id($src_wkfw_type, $wkfw_folder_id, $dest_wkfw_type)
	{
		$sql = "update fpluswkfwmst set";
		$set = array("wkfw_type");
		$setvalue = array($dest_wkfw_type);
		$cond = "where wkfw_type = $src_wkfw_type and wkfw_folder_id = $wkfw_folder_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// カテゴリ登録
	function regist_wkfwcatemst($wkfw_type, $category_name, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg)
	{
		$sql = "insert into fpluscatemst (wkfw_type, wkfw_nm, wkfwcate_del_flg, ref_dept_st_flg, ref_dept_flg, ref_st_flg) values (";
		$content = array($wkfw_type, pg_escape_string($category_name), "f", $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg);
		$result = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($result == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// カテゴリ用アクセス権（科）登録
	function regist_wkfwcate_refdept($wkfw_type, $class_id, $atrb_id, $dept_id)
	{
		$sql = "insert into fpluscate_refdept (wkfw_type, class_id, atrb_id, dept_id) values (";
		$content = array($wkfw_type, $class_id, $atrb_id, $dept_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// カテゴリ用アクセス権（役職）登録
	function regist_wkfwcate_refst($wkfw_type, $st_id)
	{
		$sql = "insert into fpluscate_refst (wkfw_type, st_id) values (";
		$content = array($wkfw_type, $st_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

    // カテゴリ用アクセス権（職員）登録
	function regist_wkfwcate_refemp($wkfw_type, $emp_id)
	{
		$sql = "insert into fpluscate_refemp (wkfw_type, emp_id) values (";
		$content = array($wkfw_type, $emp_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// カテゴリフォルダ論理削除
	function update_wkfwfolder_del_flg($wkfw_folder_id, $del_flg)
	{
		$sql = "update fplusfolder set";
		$set = array("wkfw_folder_del_flg");
		$setvalue = array($del_flg);
		$cond = "where wkfw_folder_id = $wkfw_folder_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー申請書・ワークフロータイプ、フォルダーＩＤ更新
	function update_wkfwmst_wkfw_type_folder_id($src_wkfw_type, $wkfw_folder_id, $dest_wkfw_type)
	{
		$sql = "update fpluswkfwmst set";
		$set = array("wkfw_type", "wkfw_folder_id");
		$setvalue = array($dest_wkfw_type, null);
		$cond = "where wkfw_type = $src_wkfw_type and wkfw_folder_id = $wkfw_folder_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}


	// カテゴリ移動
	function move_category($src_wkfw_type, $dest_wkfw_type, $dest_wkfw_folder_id)
	{
		// カテゴリ情報取得
	    $arr_wkfwcate_mst = $this->get_wkfwcate_mst($src_wkfw_type);

		// カテゴリ論理削除
		$this->update_wkfwcatemst_delflg($src_wkfw_type, "t");

		// フォルダ新規作成
		$max_wkfw_folder_id = $this->get_max_wkfw_folder_id();
		$new_wkfw_folder_id = intval($max_wkfw_folder_id) + 1;

		$arr = array
		(
			"wkfw_folder_id"     => $new_wkfw_folder_id,
			"wkfw_type"          => $dest_wkfw_type,
			"wkfw_folder_name"   => $arr_wkfwcate_mst["wkfw_nm"],
			"ref_dept_st_flg"    => $arr_wkfwcate_mst["ref_dept_st_flg"],
			"ref_dept_flg"       => $arr_wkfwcate_mst["ref_dept_flg"],
			"ref_st_flg"         => $arr_wkfwcate_mst["ref_st_flg"]
		);
		$this->regist_wkfwfolder($arr);

		// フォルダのアクセス権登録
		// カテゴリ用アクセス権（科）取得
		$arr_wkfwcate_refdept = $this->get_wkfwcate_refdept($src_wkfw_type);
		foreach($arr_wkfwcate_refdept as $wkfwcate_refdept)
		{
			$this->regist_wkfwfolder_refdept($new_wkfw_folder_id, $wkfwcate_refdept["class_id"], $wkfwcate_refdept["atrb_id"], $wkfwcate_refdept["dept_id"]);
		}

		// カテゴリ用アクセス権（役職）取得
		$arr_wkfwcate_refst = $this->get_wkfwcate_refst($src_wkfw_type);
		foreach($arr_wkfwcate_refst as $wkfwcate_refst)
		{
			$this->regist_wkfwfolder_refst($new_wkfw_folder_id, $wkfwcate_refst["st_id"]);
		}

	    // カテゴリ用アクセス権（職員）取得
		$arr_wkfwcate_refemp = $this->get_wkfwcate_refemp($src_wkfw_type);
		foreach($arr_wkfwcate_refemp as $wkfwcate_refemp)
		{
			$this->regist_wkfwfolder_refemp($new_wkfw_folder_id, $wkfwcate_refemp["emp_id"]);
		}

		// 遷移元カテゴリの直下にあるフォルダを取得。
		$arr_wkfwfolder = $this->get_wkfwfolder_from_wkfw_type($src_wkfw_type);
		foreach($arr_wkfwfolder as $wkfw_folder_id)
		{
			$wkfwtree_count = $this->get_wkfwtree_count($wkfw_folder_id);

			if($wkfwtree_count == 0)
			{
				// ワークフローツリー登録
				$this->regist_wkfwtree($new_wkfw_folder_id, $wkfw_folder_id);
			}
		}

		// 遷移先がフォルダの場合
		if($dest_wkfw_folder_id != "")
		{
			$this->regist_wkfwtree($dest_wkfw_folder_id, $new_wkfw_folder_id);
		}

		// ワークフローフォルダ・ワークフロータイプ更新
		$this->update_wkfwfolder_wkfw_type($src_wkfw_type, $dest_wkfw_type);

		// ワークフローＩＤ取得
		$arr_wkfw_id = $this->get_wkfw_id_for_categoryDD($src_wkfw_type);

		// ワークフロー申請書・ワークフロータイプ更新
		$this->update_wkfwmst_wkfw_type($src_wkfw_type, $dest_wkfw_type);

		// ワークフロー申請書・ファルダＩＤ	更新
		foreach($arr_wkfw_id as $wkfw_id)
		{
			$this->update_wkfwmst_wkfw_folder_id($new_wkfw_folder_id, $wkfw_id);
		}
	}




	// フォルダ移動
	function move_folder($src_wkfw_type, $src_wkfw_folder_id, $dest_wkfw_type, $dest_wkfw_folder_id, $root_flg)
	{
		// 遷移先がルートの場合
		if($root_flg)
		{
			// 遷移元フォルダ情報取得
			$arr_wkfwfolder_mst = $this->get_wkfwfolder_mst($src_wkfw_folder_id);

			// 遷移先カテゴリ新規作成
			$max_wkfw_type = $this->get_max_wkfw_type();
			$new_wkfw_type = intval($max_wkfw_type) + 1;

			$this->regist_wkfwcatemst($new_wkfw_type,
			                          $arr_wkfwfolder_mst["wkfw_folder_name"],
			                          $arr_wkfwfolder_mst["ref_dept_st_flg"],
			                          $arr_wkfwfolder_mst["ref_dept_flg"],
			                          $arr_wkfwfolder_mst["ref_st_flg"]);

			// 遷移先カテゴリアクセス権作成
			// 遷移元のフォルダ用アクセス権（科）取得、カテゴリ用アクセス権（科）登録
			$arr_wkfwfolder_refdept = $this->get_wkfwfolder_refdept($src_wkfw_folder_id);
			foreach($arr_wkfwfolder_refdept as $wkfwfolder_refdept)
			{
				$this->regist_wkfwcate_refdept($new_wkfw_type, $wkfwfolder_refdept["class_id"], $wkfwfolder_refdept["atrb_id"], $wkfwfolder_refdept["dept_id"]);
			}

			// フォルダ用アクセス権（役職）取得、カテゴリ用アクセス権（役職）登録
			$arr_wkfwfolder_refst = $this->get_wkfwfolder_refst($src_wkfw_folder_id);
			foreach($arr_wkfwfolder_refst as $wkfwfolder_refst)
			{
				$this->regist_wkfwcate_refst($new_wkfw_type, $wkfwfolder_refst["st_id"]);
			}
			// フォルダ用アクセス権（職員）取得、カテゴリ用アクセス権（職員）登録
			$arr_wkfwfolder_refemp = $this->get_wkfwfolder_refemp($src_wkfw_folder_id);
			foreach($arr_wkfwfolder_refemp as $wkfwfolder_refemp)
			{
				$this->regist_wkfwcate_refemp($new_wkfw_type, $wkfwfolder_refemp["emp_id"]);
			}

			// 遷移元フォルダ論理削除
			$this->update_wkfwfolder_del_flg($src_wkfw_folder_id, "t");

			// 遷移元フォルダの子フォルダを取得
			$arr_child_folder = array();
			$this->get_child_folder($src_wkfw_folder_id, $arr_child_folder);

			// 遷移元フォルダが親または子フォルダをもっている場合、ワークフローツリー論理削除
			$this->update_wkfwtree_del_flg($src_wkfw_folder_id, "t");
			$this->update_wkfwtree_del_flg_for_parent_id($src_wkfw_folder_id, "t");

			// ワークフローフォルダ・ワークフロータイプ更新(子フォルダ)
			foreach($arr_child_folder as $child_folder_id)
			{
				$this->update_wkfwfolder_wkfw_type_from_folder_id($child_folder_id, $new_wkfw_type);
			}

			// ワークフロー申請書・ワークフロータイプ更新
			$this->update_wkfwmst_wkfw_type_folder_id($src_wkfw_type, $src_wkfw_folder_id, $new_wkfw_type);
			foreach($arr_child_folder as $child_folder_id)
			{
				$this->update_wkfwmst_wkfw_type_from_folder_id($src_wkfw_type, $child_folder_id, $new_wkfw_type);
			}

		}
		else
		{
			// 遷移先がカテゴリの場合
			if($dest_wkfw_folder_id == "")
			{
				// 遷移元フォルダが親フォルダをもっている場合、ワークフローツリー論理削除
				$this->update_wkfwtree_del_flg($src_wkfw_folder_id, "t");
			}
			// 遷移先がフォルダの場合
			else
			{
				$wkfwtree_count = $this->get_wkfwtree_count($src_wkfw_folder_id);
				if($wkfwtree_count > 0)
				{
					// ワークフローツリー更新
					$this->update_wkfwtree_wkfw_parent_id($dest_wkfw_folder_id, $src_wkfw_folder_id);
				}
				else
				{
					// ワークフローツリー登録
					$this->regist_wkfwtree($dest_wkfw_folder_id, $src_wkfw_folder_id);
				}
			}

			// 遷移元フォルダの子フォルダを取得
			$arr_child_folder = array();
			$this->get_child_folder($src_wkfw_folder_id, $arr_child_folder);

			// ワークフローフォルダ・ワークフロータイプ更新
			$this->update_wkfwfolder_wkfw_type_from_folder_id($src_wkfw_folder_id, $dest_wkfw_type);
			foreach($arr_child_folder as $child_folder_id)
			{
				$this->update_wkfwfolder_wkfw_type_from_folder_id($child_folder_id, $dest_wkfw_type);
			}

			// ワークフロー申請書・ワークフロータイプ更新
			$this->update_wkfwmst_wkfw_type_from_folder_id($src_wkfw_type, $src_wkfw_folder_id, $dest_wkfw_type);
			foreach($arr_child_folder as $child_folder_id)
			{
				$this->update_wkfwmst_wkfw_type_from_folder_id($src_wkfw_type, $child_folder_id, $dest_wkfw_type);
			}
		}
	}


//-------------------------------------------------------------------------------------------------------------------------
// 申請一覧用
//-------------------------------------------------------------------------------------------------------------------------

	// 通常申請のＳＱＬ取得
	function get_normal_apply_sql($arr_cond)
	{
		$emp_id          = $arr_cond["emp_id"];
		$wkfw_type       = $arr_cond["wkfw_type"];
		$wkfw_id         = $arr_cond["wkfw_id"];
		$apply_title     = $arr_cond["apply_title"];
		$emp_nm          = $arr_cond["emp_nm"];
		$apply_yyyy_from = $arr_cond["apply_yyyy_from"];
		$apply_mm_from   = $arr_cond["apply_mm_from"];
		$apply_dd_from   = $arr_cond["apply_dd_from"];
		$apply_yyyy_to   = $arr_cond["apply_yyyy_to"];
		$apply_mm_to     = $arr_cond["apply_mm_to"];
		$apply_dd_to     = $arr_cond["apply_dd_to"];
		$apply_stat      = $arr_cond["apply_stat"];

        $sql  = "select ";
        $sql .= "varchar(1) '1' as apply_type, ";
        $sql .= "A.apply_id, ";
        $sql .= "A.apply_title, ";
        $sql .= "A.apply_date, ";
        $sql .= "A.apply_stat, ";
        $sql .= "A.apply_no, ";
        $sql .= "A.apv_fix_show_flg, ";
        $sql .= "A.apv_bak_show_flg, ";
        $sql .= "A.apv_ng_show_flg, ";
        $sql .= "B.wkfw_title, ";
        $sql .= "B.wkfw_nm, ";
        $sql .= "B.short_wkfw_name, ";
        $sql .= "B.wkfw_folder_id, ";
        $sql .= "null as pjt_schd_id, ";
        $sql .= "epi_a.data_2_1_arise_date as a_arise_date, ";
        $sql .= "epi_b.data_2_1_arise_date as b_arise_date ";
        $sql .= "from fplusapply A ";

        $sql .= "inner join ";
        $sql .= "(select WM.wkfw_id, WM.wkfw_type, WM.wkfw_title, CATE.wkfw_nm, WM.short_wkfw_name, WM.wkfw_folder_id ";
        $sql .= "from fpluswkfwmst WM ";
        $sql .= "inner join fpluscatemst CATE on ";
        $sql .= "WM.wkfw_type = CATE.wkfw_type) B on A.wkfw_id = B.wkfw_id ";
        $sql .= "left join fplus_epi_a epi_a on epi_a.apply_id  = A.apply_id ";
        $sql .= "left join fplus_epi_b epi_b on epi_b.apply_id  = A.apply_id ";
        $sql .= "where A.emp_id = '$emp_id' ";
        $sql .= "and A.re_apply_id is null ";
        $sql .= "and not A.delete_flg ";
        $sql .= "and not A.draft_flg ";

		// カテゴリ
		if($wkfw_type != "" && $wkfw_type != "-") {
			$sql .= "and B.wkfw_type = $wkfw_type ";
		}

		// 申請書名
		if($wkfw_id != "" &&  $wkfw_id != "-") {
			$sql .= "and B.wkfw_id = $wkfw_id ";
		}

		// 表題
		if($apply_title != "") {
			$sql .= "and A.apply_title like '%$apply_title%' ";
		}

		// 承認者
		if($emp_nm != "") {

            $sql .= "and exists ";
            $sql .= "(select APV.apply_id from fplusapplyapv APV ";
            $sql .= "where exists (select * from empmst EMP where APV.emp_id = EMP.emp_id and ";
            $sql .= "(EMP.emp_lt_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_ft_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_kn_lt_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_kn_ft_nm like '%$emp_nm%' ";
            $sql .= "or (EMP.emp_lt_nm || EMP.emp_ft_nm) like '%$emp_nm%' ";
            $sql .= "or (EMP.emp_kn_lt_nm || EMP.emp_kn_ft_nm) like '%$emp_nm%' ";
            $sql .= "or (EMP.emp_kn_lt_nm || EMP.emp_kn_ft_nm) like '%$emp_nm%')) ";
            $sql .= "and A.apply_id = APV.apply_id ";
            $sql .= "group by APV.apply_id) ";

		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(A.apply_date from 1 for $date_len) >= '$date_str' ";
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(A.apply_date from 1 for $date_len) <= '$date_str' ";
		}

		// 申請状況
		if($apply_stat != "" && $apply_stat != "-") {
			$sql .= "and A.apply_stat = '$apply_stat' ";
		}

		return $sql;
	}

    // 議事録公開申請（委員会・WG）のＳＱＬ取得
	function get_proceeding_apply_sql($arr_cond)
    {

		$emp_id          = $arr_cond["emp_id"];
		$apply_title     = $arr_cond["apply_title"];
		$emp_nm          = $arr_cond["emp_nm"];
		$apply_yyyy_from = $arr_cond["apply_yyyy_from"];
		$apply_mm_from   = $arr_cond["apply_mm_from"];
		$apply_dd_from   = $arr_cond["apply_dd_from"];
		$apply_yyyy_to   = $arr_cond["apply_yyyy_to"];
		$apply_mm_to     = $arr_cond["apply_mm_to"];
		$apply_dd_to     = $arr_cond["apply_dd_to"];
		$apply_stat      = $arr_cond["apply_stat"];

		$sql  = "select ";
		$sql .= "varchar(1) '2' as apply_type, ";
		$sql .= "null as apply_id, ";
        $sql .= "A.prcd_subject as apply_title, ";
        $sql .= "A.prcd_create_time as apply_date, ";
        $sql .= "A.prcd_status as apply_stat, ";
        $sql .= "null as apply_no, ";
        $sql .= "A.apv_fix_show_flg, ";
        $sql .= "null as apv_bak_show_flg, ";
        $sql .= "null as apv_ng_show_flg, ";
        $sql .= "'議事録公開申請（委員会・WG）' as wkfw_title, ";
        $sql .= "'CoMedix' as wkfw_nm, ";
        $sql .= "null as short_wkfw_name,";
        $sql .= "null as wkfw_folder_id, ";
        $sql .= "A.pjt_schd_id ";

        $sql .= "from proceeding A ";
        $sql .= "where A.prcd_create_emp_id = '$emp_id' ";

		// 表題
		if($apply_title != "") {
			$sql .= "and A.prcd_subject like '%$apply_title%' ";
		}

		// 承認者
		if($emp_nm != "") {

            $sql .= "and exists ";
            $sql .= "(select APV.pjt_schd_id from prcdaprv APV ";
            $sql .= "where exists (select * from empmst EMP where APV.prcdaprv_emp_id = EMP.emp_id and ";
            $sql .= "(EMP.emp_lt_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_ft_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_kn_lt_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_kn_ft_nm like '%$emp_nm%' ";
            $sql .= "or (EMP.emp_lt_nm || EMP.emp_ft_nm) like '%$emp_nm%' ";
            $sql .= "or (EMP.emp_kn_lt_nm || EMP.emp_kn_ft_nm) like '%$emp_nm%')) ";
            $sql .= "and A.pjt_schd_id = APV.pjt_schd_id ";
            $sql .= "group by APV.pjt_schd_id) ";
		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			if($date_len > 0) {
				$sql .= "and substring(A.prcd_create_time from 1 for $date_len) >= '$date_str' ";
			}
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			if($date_len > 0) {
				$sql .= "and substring(A.prcd_create_time from 1 for $date_len) <= '$date_str' ";
			}
		}

		// 申請状況
		if($apply_stat != "" && $apply_stat != "-") {
			$tmp_apply_stat = $apply_stat + 1;
			$sql .= "and A.prcd_status = '$tmp_apply_stat' ";
		}

		return $sql;
    }


	// 残業申請のＳＱＬ取得
	function get_ovtm_apply_sql($arr_cond)
	{
		$emp_id          = $arr_cond["emp_id"];
		$emp_nm          = $arr_cond["emp_nm"];
		$apply_yyyy_from = $arr_cond["apply_yyyy_from"];
		$apply_mm_from   = $arr_cond["apply_mm_from"];
		$apply_dd_from   = $arr_cond["apply_dd_from"];
		$apply_yyyy_to   = $arr_cond["apply_yyyy_to"];
		$apply_mm_to     = $arr_cond["apply_mm_to"];
		$apply_dd_to     = $arr_cond["apply_dd_to"];
		$apply_stat      = $arr_cond["apply_stat"];

		$sql  = "select ";
		$sql .= "varchar(1) '3' as apply_type, ";
		$sql .= "A.apply_id, ";
		$sql .= "null as apply_title, ";
		$sql .= "A.apply_time as apply_date, ";
		$sql .= "A.apply_status as apply_stat, ";
		$sql .= "null as apply_no, ";
		$sql .= "A.apv_fix_show_flg, ";
		$sql .= "A.apv_bak_show_flg, ";
		$sql .= "A.apv_ng_show_flg, ";
		$sql .= "'残業申請' as wkfw_title, ";
		$sql .= "'CoMedix' as wkfw_nm, ";
		$sql .= "null as short_wkfw_name, ";
		$sql .= "null as wkfw_folder_id, ";
		$sql .= "null as pjt_schd_id ";
		$sql .= "from ovtmapply A ";

		$sql .= "where A.emp_id = '$emp_id' ";
        $sql .= "and A.re_apply_id is null ";
        $sql .= "and not A.delete_flg ";


		// 承認者
		if($emp_nm != "") {

            $sql .= "and exists ";
            $sql .= "(select APV.apply_id from ovtmaprv APV ";
            $sql .= "where exists (select * from empmst EMP where APV.aprv_emp_id = EMP.emp_id and ";
            $sql .= "(EMP.emp_lt_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_ft_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_kn_lt_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_kn_ft_nm like '%$emp_nm%' ";
            $sql .= "or (EMP.emp_lt_nm || EMP.emp_ft_nm) like '%$emp_nm%' ";
            $sql .= "or (EMP.emp_kn_lt_nm || EMP.emp_kn_ft_nm) like '%$emp_nm%')) ";
            $sql .= "and A.apply_id = APV.apply_id ";
            $sql .= "group by APV.apply_id) ";
		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(A.apply_time from 1 for $date_len) >= '$date_str' ";
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(A.apply_time from 1 for $date_len) <= '$date_str' ";
		}

		// 申請状況
		if($apply_stat != "" && $apply_stat != "-") {
			$sql .= "and A.apply_status = '$apply_stat' ";
		}

		return $sql;
	}

	// 勤務時間修正申請のＳＱＬ取得
	function get_tmmd_apply_sql($arr_cond)
	{
		$emp_id          = $arr_cond["emp_id"];
		$emp_nm          = $arr_cond["emp_nm"];
		$apply_yyyy_from = $arr_cond["apply_yyyy_from"];
		$apply_mm_from   = $arr_cond["apply_mm_from"];
		$apply_dd_from   = $arr_cond["apply_dd_from"];
		$apply_yyyy_to   = $arr_cond["apply_yyyy_to"];
		$apply_mm_to     = $arr_cond["apply_mm_to"];
		$apply_dd_to     = $arr_cond["apply_dd_to"];
		$apply_stat      = $arr_cond["apply_stat"];

		$sql  = "select ";
		$sql .= "varchar(1) '4' as apply_type, ";
		$sql .= "A.apply_id, ";
		$sql .= "null as apply_title, ";
		$sql .= "A.apply_time as apply_date, ";
		$sql .= "A.apply_status as apply_stat, ";
		$sql .= "null as apply_no, ";
		$sql .= "A.apv_fix_show_flg, ";
		$sql .= "A.apv_bak_show_flg, ";
		$sql .= "A.apv_ng_show_flg, ";
		$sql .= "'勤務時間修正申請' as wkfw_title, ";
		$sql .= "'CoMedix' as wkfw_nm, ";
		$sql .= "null as short_wkfw_name, ";
		$sql .= "null as wkfw_folder_id, ";
		$sql .= "null as pjt_schd_id ";
		$sql .= "from tmmdapply A ";

		$sql .= "where A.emp_id = '$emp_id' ";
        $sql .= "and A.re_apply_id is null ";
        $sql .= "and not A.delete_flg ";

		// 承認者
		if($emp_nm != "") {

            $sql .= "and exists ";
            $sql .= "(select APV.apply_id from tmmdaprv APV ";
            $sql .= "where exists (select * from empmst EMP where APV.aprv_emp_id = EMP.emp_id and ";
            $sql .= "(EMP.emp_lt_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_ft_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_kn_lt_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_kn_ft_nm like '%$emp_nm%' ";
            $sql .= "or (EMP.emp_lt_nm || EMP.emp_ft_nm) like '%$emp_nm%' ";
            $sql .= "or (EMP.emp_kn_lt_nm || EMP.emp_kn_ft_nm) like '%$emp_nm%')) ";
            $sql .= "and A.apply_id = APV.apply_id ";
            $sql .= "group by APV.apply_id) ";
		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(A.apply_time from 1 for $date_len) >= '$date_str' ";
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(A.apply_time from 1 for $date_len) <= '$date_str' ";
		}

		// 申請状況
		if($apply_stat != "" && $apply_stat != "-") {
			$sql .= "and A.apply_status = '$apply_stat' ";
		}

		return $sql;
	}

    // 退勤後復帰申請のＳＱＬ取得
	function get_rtn_apply_sql($arr_cond)
	{
		$emp_id          = $arr_cond["emp_id"];
		$emp_nm          = $arr_cond["emp_nm"];
		$apply_yyyy_from = $arr_cond["apply_yyyy_from"];
		$apply_mm_from   = $arr_cond["apply_mm_from"];
		$apply_dd_from   = $arr_cond["apply_dd_from"];
		$apply_yyyy_to   = $arr_cond["apply_yyyy_to"];
		$apply_mm_to     = $arr_cond["apply_mm_to"];
		$apply_dd_to     = $arr_cond["apply_dd_to"];
		$apply_stat      = $arr_cond["apply_stat"];

		$sql  = "select ";
		$sql .= "varchar(1) '5' as apply_type, ";
		$sql .= "A.apply_id, ";
		$sql .= "null as apply_title, ";
		$sql .= "A.apply_time as apply_date, ";
		$sql .= "A.apply_status as apply_stat, ";
		$sql .= "null as apply_no, ";
		$sql .= "A.apv_fix_show_flg, ";
		$sql .= "A.apv_bak_show_flg, ";
		$sql .= "A.apv_ng_show_flg, ";
		$sql .= "'退勤後復帰申請' as wkfw_title, ";
		$sql .= "'CoMedix' as wkfw_nm, ";
		$sql .= "null as short_wkfw_name, ";
		$sql .= "null as wkfw_folder_id, ";
		$sql .= "null as pjt_schd_id ";
		$sql .= "from rtnapply A ";

		$sql .= "where A.emp_id = '$emp_id' ";
        $sql .= "and A.re_apply_id is null ";
        $sql .= "and not A.delete_flg ";

		// 承認者
		if($emp_nm != "") {

            $sql .= "and exists ";
            $sql .= "(select APV.apply_id from rtnaprv APV ";
            $sql .= "where exists (select * from empmst EMP where APV.aprv_emp_id = EMP.emp_id and ";
            $sql .= "(EMP.emp_lt_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_ft_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_kn_lt_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_kn_ft_nm like '%$emp_nm%' ";
            $sql .= "or (EMP.emp_lt_nm || EMP.emp_ft_nm) like '%$emp_nm%' ";
            $sql .= "or (EMP.emp_kn_lt_nm || EMP.emp_kn_ft_nm) like '%$emp_nm%')) ";
            $sql .= "and A.apply_id = APV.apply_id ";
            $sql .= "group by APV.apply_id) ";
		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(A.apply_time from 1 for $date_len) >= '$date_str' ";
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(A.apply_time from 1 for $date_len) <= '$date_str' ";
		}

		// 申請状況
		if($apply_stat != "" && $apply_stat != "-") {
			$sql .= "and A.apply_status = '$apply_stat' ";
		}

		return $sql;


	}

    // 通常申請の承認情報取得
	function get_applyapv_for_applylist($apply_id)
    {
		$sql   = "select A.apply_id, A.apv_stat, A.apv_fix_show_flg, B.emp_lt_nm, B.emp_ft_nm from fplusapplyapv A ";
        $sql  .= "left join empmst B on A.emp_id = B.emp_id ";
        $cond  = "where A.apply_id = $apply_id ";
        $cond .= "order by A.apv_order, A.apv_sub_order asc ";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("apply_id" => $row["apply_id"], "apv_stat" => $row["apv_stat"], "apv_fix_show_flg" => $row["apv_fix_show_flg"], "emp_lt_nm" => $row["emp_lt_nm"], "emp_ft_nm" => $row["emp_ft_nm"]);
		}
		return $arr;
    }

    // 議事録公開申請（委員会・WG）の承認情報取得
	function get_prcdaprv_for_applylist($pjt_schd_id)
	{
		$sql   = "select ";
		$sql  .= "A.pjt_schd_id, ";
		$sql  .= "A.prcdaprv_no, ";
		$sql  .= "A.prcdaprv_decide_flg, ";
		$sql  .= "A.prcdaprv_date, ";
		$sql  .= "A.apv_fix_show_flg, ";
		$sql  .= "B.emp_lt_nm, ";
		$sql  .= "B.emp_ft_nm ";

		$sql  .= "from prcdaprv A ";
        $sql  .= "left join empmst B on A.prcdaprv_emp_id = B.emp_id ";
        $cond  = "where A.pjt_schd_id = '$pjt_schd_id' ";
        $cond .= "order by A.prcdaprv_no asc ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("pjt_schd_id" => $row["pjt_schd_id"],
                            "prcdaprv_no" => $row["prcdaprv_no"],
                            "prcdaprv_decide_flg" => $row["prcdaprv_decide_flg"],
                            "prcdaprv_date" => $row["prcdaprv_date"],
                            "apv_fix_show_flg" => $row["apv_fix_show_flg"],
                            "emp_lt_nm" => $row["emp_lt_nm"],
                            "emp_ft_nm" => $row["emp_ft_nm"]);
		}
		return $arr;
	}

	// 残業申請の承認情報取得
	function get_ovtmaprv_for_applylist($apply_id)
	{
        $sql  .= "select ";
        $sql  .= "A.apply_id, ";
        $sql  .= "A.aprv_status, ";
        $sql  .= "A.apv_fix_show_flg, ";
        $sql  .= "B.emp_lt_nm, ";
        $sql  .= "B.emp_ft_nm ";

        $sql  .= "from ovtmaprv A ";
        $sql  .= "left join empmst B on A.aprv_emp_id = B.emp_id ";
        $cond  = "where A.apply_id = $apply_id ";
        $cond .= "order by A.aprv_no, A.aprv_sub_no asc ";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array(
                            "apply_id" => $row["apply_id"],
                            "apv_stat" => $row["aprv_status"],
                            "apv_fix_show_flg" => $row["apv_fix_show_flg"],
                            "emp_lt_nm" => $row["emp_lt_nm"],
                            "emp_ft_nm" => $row["emp_ft_nm"]
                           );
		}
		return $arr;
    }


	// 勤務時間修正申請の承認情報取得
	function get_tmmdaprv_for_applylist($apply_id)
	{
        $sql  .= "select ";
        $sql  .= "A.apply_id, ";
        $sql  .= "A.aprv_status, ";
        $sql  .= "A.apv_fix_show_flg, ";
        $sql  .= "B.emp_lt_nm, ";
        $sql  .= "B.emp_ft_nm ";

        $sql  .= "from tmmdaprv A ";
        $sql  .= "left join empmst B on A.aprv_emp_id = B.emp_id ";
        $cond  = "where A.apply_id = $apply_id ";
        $cond .= "order by A.aprv_no, A.aprv_sub_no asc ";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array(
                            "apply_id" => $row["apply_id"],
                            "apv_stat" => $row["aprv_status"],
                            "apv_fix_show_flg" => $row["apv_fix_show_flg"],
                            "emp_lt_nm" => $row["emp_lt_nm"],
                            "emp_ft_nm" => $row["emp_ft_nm"]
                           );
		}
		return $arr;

	}

	// 退勤後復帰申請の承認情報取得
	function get_rtnaprv_for_applylist($apply_id)
	{

        $sql  .= "select ";
        $sql  .= "A.apply_id, ";
        $sql  .= "A.aprv_status, ";
        $sql  .= "A.apv_fix_show_flg, ";
        $sql  .= "B.emp_lt_nm, ";
        $sql  .= "B.emp_ft_nm ";

        $sql  .= "from rtnaprv A ";
        $sql  .= "left join empmst B on A.aprv_emp_id = B.emp_id ";
        $cond  = "where A.apply_id = $apply_id ";
        $cond .= "order by A.aprv_no, A.aprv_sub_no asc ";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array(
                            "apply_id" => $row["apply_id"],
                            "apv_stat" => $row["aprv_status"],
                            "apv_fix_show_flg" => $row["apv_fix_show_flg"],
                            "emp_lt_nm" => $row["emp_lt_nm"],
                            "emp_ft_nm" => $row["emp_ft_nm"]
                           );
		}
		return $arr;
	}


	// 申請一覧取得ＳＱＬ
    function get_applylist_sql($arr_cond)
    {
		$wkfw_type       = $arr_cond["wkfw_type"];
		$wkfw_id         = $arr_cond["wkfw_id"];
		$apply_title     = $arr_cond["apply_title"];
		$apply_stat      = $arr_cond["apply_stat"];

		$sql = "";

		//-------------------------------------------
		// ■通常の申請
		// ・「カテゴリ」がCoMedix(0)以外
		//-------------------------------------------
		if($wkfw_type != "0")
		{
			$sql .= $this->get_normal_apply_sql($arr_cond);
		}

/*
		//-------------------------------------------
		// ■議事録公開申請（委員会・WG）
		// ・「カテゴリ」がすべて(-)
		// ・「カテゴリ」がCoMedix(0) and 「申請書名」がすべて(-)
		// ・「カテゴリ」がCoMedix(0) and 「申請書名」が議事録公開申請（委員会・WG）(1)
		// ・「申請状況」が否認(2) and 差戻し(3)以外
		//-------------------------------------------
		if($wkfw_type == ""
		    or ($wkfw_type == "-" and $apply_stat != "2" and $apply_stat != "3")
	        or ($wkfw_type == "0" and $wkfw_id == "-" and $apply_stat != "2" and $apply_stat != "3")
	        or ($wkfw_type == "0" and $wkfw_id == "1" and $apply_stat != "2" and $apply_stat != "3"))
	    {

			if($sql != "")
			{
				$sql .= "union all ";
			}
			$sql .= $this->get_proceeding_apply_sql($arr_cond);
		}

		//-------------------------------------------
		// ■残業申請
		// ・「カテゴリ」がすべて(-)
		// ・「カテゴリ」がCoMedix(0) and 「申請書名」がすべて(-)
		// ・「カテゴリ」がCoMedix(0) and 「申請書名」が残業申請(2)
		// ・表題が未入力の場合
		//-------------------------------------------
		if($wkfw_type == ""
			or ($wkfw_type == "-" and $apply_title == "")
	        or ($wkfw_type == "0" and $wkfw_id == "-" and $apply_title == "")
	        or ($wkfw_type == "0" and $wkfw_id == "2" and $apply_title == "") )
	    {

			if($sql != "")
			{
				$sql .= "union all ";
			}
			$sql .= $this->get_ovtm_apply_sql($arr_cond);
		}

		//-------------------------------------------
		// ■勤務時間修正申請
		// ・「カテゴリ」がすべて(-)
		// ・「カテゴリ」がCoMedix(0) and 「申請書名」がすべて(-)
		// ・「カテゴリ」がCoMedix(0) and 「申請書名」が勤務時間修正申請(3)
		// ・表題が未入力の場合
		//-------------------------------------------
		if($wkfw_type == ""
			or ($wkfw_type == "-" and $apply_title == "")
	        or ($wkfw_type == "0" and $wkfw_id == "-" and $apply_title == "")
	        or ($wkfw_type == "0" and $wkfw_id == "3" and $apply_title == "") )
	    {
			if($sql != "")
			{
				$sql .= "union all ";
			}
			$sql .= $this->get_tmmd_apply_sql($arr_cond);
		}
		//-------------------------------------------
		// ■退勤後復帰申請
		// ・「カテゴリ」がすべて(-)
		// ・「カテゴリ」がCoMedix(0) and 「申請書名」がすべて(-)
		// ・「カテゴリ」がCoMedix(0) and 「申請書名」が退勤後復帰申請(4)
		// ・表題が未入力の場合
		//-------------------------------------------
		if($wkfw_type == ""
		    or ($wkfw_type == "-" and $apply_title == "")
	        or ($wkfw_type == "0" and $wkfw_id == "-" and $apply_title == "")
	        or ($wkfw_type == "0" and $wkfw_id == "4" and $apply_title == "") )
	    {
			if($sql != "")
			{
				$sql .= "union all ";
			}
			$sql .= $this->get_rtn_apply_sql($arr_cond);
		}
*/

		return $sql;
    }

//-------------------------------------------------------------------------------------------------------------------------
// 承認一覧
//-------------------------------------------------------------------------------------------------------------------------
	// 通常申請のＳＱＬ取得
	function get_normal_approve_sql($arr_cond)
    {
		$emp_id          = $arr_cond["emp_id"];
		$wkfw_type       = $arr_cond["wkfw_type"];
		$wkfw_id         = $arr_cond["wkfw_id"];
		$apply_title     = $arr_cond["apply_title"];
		$emp_nm          = $arr_cond["emp_nm"];
		$apply_yyyy_from = $arr_cond["apply_yyyy_from"];
		$apply_mm_from   = $arr_cond["apply_mm_from"];
		$apply_dd_from   = $arr_cond["apply_dd_from"];
		$apply_yyyy_to   = $arr_cond["apply_yyyy_to"];
		$apply_mm_to     = $arr_cond["apply_mm_to"];
		$apply_dd_to     = $arr_cond["apply_dd_to"];
		$apv_stat        = $arr_cond["apv_stat"];
		$all_apv_stat       = $arr_cond["all_apv_stat"];

		$sql  = "select ";
		$sql .= "varchar(1) '1' as apply_type, ";
		$sql .= "a.apply_id, ";
		$sql .= "a.apply_stat, ";
		$sql .= "a.apply_date, ";
		$sql .= "a.apply_title, ";
		$sql .= "d.wkfw_title, ";
		$sql .= "d.wkfw_nm, ";
		$sql .= "e.emp_lt_nm, ";
		$sql .= "e.emp_ft_nm, ";


//20110817 受付一覧に第三階層・第四階層を表示する
		$sql .= "deptm.dept_nm, ";
		$sql .= "roomm.room_nm, ";

		$sql .= "b.apv_stat, ";
		$sql .= "null as pjt_schd_id, ";
		$sql .= "b.apv_order, ";
		$sql .= "b.apv_sub_order, ";
		$sql .= "c.send_apved_order, ";
		$sql .= "a.apply_no, ";
		$sql .= "d.short_wkfw_name, ";
		$sql .= "d.wkfw_folder_id, ";
                $sql .= "epi_a.data_2_1_arise_date as a_arise_date, ";
                $sql .= "epi_b.data_2_1_arise_date as b_arise_date ";
		$sql .= "from fplusapply a ";

		if($all_apv_stat == "1")
		{
			//報告状況が「一部受付」
			$sql  .= "inner join (select apply_id, '1' as apply_stat_detail from (select apply_id, max(apv_stat) as apv_stat from fplusapplyapv group by apply_id) Apv where apv_stat = 1) g on a.apply_id = g.apply_id and a.apply_stat = '0' ";
		}
		else if($all_apv_stat == "0")
		{
			//報告状況が「受付待ち（一部受付を除く）」
			$sql  .= "inner join (select apply_id, '0' as apply_stat_detail from (select apply_id, max(apv_stat) as apv_stat from fplusapplyapv group by apply_id) Apv where apv_stat = 0) g on a.apply_id = g.apply_id and a.apply_stat = '0' ";
		}

		$sql .= "inner join fplusapplyapv b on a.apply_id = b.apply_id ";

		$sql .= "left join fplusapplyasyncrecv c on b.apply_id = c.apply_id ";
		$sql .= "and b.apv_order = c.recv_apv_order ";
		$sql .= "and ((b.apv_sub_order = c.recv_apv_sub_order) or (b.apv_sub_order is null and c.recv_apv_sub_order is null)) ";
		$sql .= "inner join ";
		$sql .= "(select a.wkfw_id, a.wkfw_type, a.wkfw_title, b.wkfw_nm, a.short_wkfw_name, a.wkfw_folder_id from fpluswkfwmst a ";
		$sql .= "inner join fpluscatemst b on a.wkfw_type = b.wkfw_type) d on a.wkfw_id = d.wkfw_id ";
		$sql .= "inner join empmst e on a.emp_id = e.emp_id ";

		$sql .= "left join deptmst deptm on deptm.dept_id = a.emp_dept ";
		$sql .= "left join classroom roomm on roomm.room_id = a.emp_room ";

                $sql .= "left join fplus_epi_a epi_a on epi_a.apply_id  = a.apply_id ";
                $sql .= "left join fplus_epi_b epi_b on epi_b.apply_id  = a.apply_id ";
                
                
		$sql .= "where a.re_apply_id is null ";
		$sql .= "and not a.delete_flg and not a.draft_flg ";
		$sql .= "and b.emp_id = '$emp_id' ";
		$sql .= "and (c.apv_show_flg or c.apv_show_flg is null) ";

		// 承認状況
		if($apv_stat == "0")
		{
			$sql .= "and b.apv_stat = '0' ";

			$sql .= "and (";
			$sql .= "(a.wkfw_appr = '1' and ";
			$sql .= "exists (select apply_id from fplusapplyapv apv1 where a.apply_id = apv1.apply_id and apv1.emp_id = '$emp_id' and apv1.apv_stat = '0')) ";

			$sql .= "or ";
			$sql .= "(a.wkfw_appr = '2' and a.apply_stat = '0' and ";
			$sql .= "exists (select apv2.* from ";
			$sql .= "(select apply_id, apv_order from fplusapplyapv where emp_id = '$emp_id' and apv_stat = '0' ";
			$sql .= "and apv_order = (select min(apv_order) from fplusapplyapv minapplyapv where minapplyapv.apply_id = a.apply_id and apv_stat = '0')) apv2 ";
			$sql .= "where a.apply_id = apv2.apply_id and b.apv_order = apv2.apv_order)) ";

			$sql .= "or ";
			$sql .= "(a.wkfw_appr = '2' and c.apv_show_flg and ";
			$sql .= "exists (select apply_id from fplusapplyapv apv3 where a.apply_id = apv3.apply_id and apv3.emp_id = '$emp_id' and apv3.apv_stat = '0'))";

			$sql .= ") ";

		}
		else if($apv_stat == "" || $apv_stat == "-")
		{
			$sql .= "and (";
			$sql .= "(a.wkfw_appr = '1' and ";
			$sql .= "exists (select apply_id from fplusapplyapv apv1 where a.apply_id = apv1.apply_id and apv1.emp_id = '$emp_id')) ";

			$sql .= "or ";
			$sql .= "(a.wkfw_appr = '2' and a.apply_stat = '0' and ";
			$sql .= "exists (select apv2.* from ";
			$sql .= "(select apply_id, apv_order from fplusapplyapv where emp_id = '$emp_id' and apv_stat = '0' ";
			$sql .= "and apv_order = (select min(apv_order) from fplusapplyapv minapplyapv where minapplyapv.apply_id = a.apply_id and apv_stat = '0')) apv2 ";
			$sql .= "where a.apply_id = apv2.apply_id and b.apv_order = apv2.apv_order)) ";

			$sql .= "or ";
			$sql .= "(a.wkfw_appr = '2' and c.apv_show_flg and ";
			$sql .= "exists (select apply_id from fplusapplyapv apv3 where a.apply_id = apv3.apply_id and apv3.emp_id = '$emp_id' and apv3.apv_stat = '0'))";

			$sql .= "or ";
			$sql .= "(a.wkfw_appr = '2' and ";
			$sql .= "exists (select apv4.apply_id, apv4.apv_order from fplusapplyapv apv4 where a.apply_id = apv4.apply_id and b.apv_order = apv4.apv_order and apv4.emp_id = '$emp_id' and apv4.apv_stat <> '0'))";

			$sql .= ") ";
		}
		else
		{
			if($apv_stat != "" && $apv_stat != "-")
			{
				$sql .= "and b.apv_stat = '$apv_stat' ";
			}
		}

		// カテゴリ
		if($wkfw_type != "" && $wkfw_type != "-")
		{
			$sql .= "and d.wkfw_type = $wkfw_type ";
		}
		// 申請書名
		if($wkfw_id != "" &&  $wkfw_id != "-")
		{
			$sql .= "and d.wkfw_id = $wkfw_id ";
		}
		// 表題
		if($apply_title != "")
		{
			$sql .= "and a.apply_title like '%$apply_title%' ";
		}

		// 申請者
		if($emp_nm != "") {

			$sql .= "and ";
			$sql .= "(e.emp_lt_nm like '%$emp_nm%' ";
			$sql .= "or e.emp_ft_nm like '%$emp_nm%' ";
			$sql .= "or e.emp_kn_lt_nm like '%$emp_nm%' ";
			$sql .= "or e.emp_kn_ft_nm like '%$emp_nm%' ";
			$sql .= "or (e.emp_lt_nm || e.emp_ft_nm) like '%$emp_nm%' ";
			$sql .= "or (e.emp_kn_lt_nm || e.emp_kn_ft_nm) like '%$emp_nm%') ";

		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(a.apply_date from 1 for $date_len) >= '$date_str' ";
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(a.apply_date from 1 for $date_len) <= '$date_str' ";
		}

		return $sql;
    }

	// 議事録公開申請（委員会・WG）のＳＱＬ取得
    function get_proceeding_approve_sql($arr_cond)
    {

		$emp_id          = $arr_cond["emp_id"];
		$apply_title     = $arr_cond["apply_title"];
		$emp_nm          = $arr_cond["emp_nm"];
		$apply_yyyy_from = $arr_cond["apply_yyyy_from"];
		$apply_mm_from   = $arr_cond["apply_mm_from"];
		$apply_dd_from   = $arr_cond["apply_dd_from"];
		$apply_yyyy_to   = $arr_cond["apply_yyyy_to"];
		$apply_mm_to     = $arr_cond["apply_mm_to"];
		$apply_dd_to     = $arr_cond["apply_dd_to"];
		$apv_stat        = $arr_cond["apv_stat"];

    	$sql = "select ";
		$sql .= "varchar(1) '2' as apply_type, ";
		$sql .= "null as apply_id, ";
		$sql .= "a.prcd_status as apply_stat, ";
		$sql .= "a.prcd_create_time as apply_date, ";
		$sql .= "a.prcd_subject as apply_title, ";
		$sql .= "'議事録公開申請（委員会・WG）' as wkfw_title, ";
		$sql .= "'CoMedix' as wkfw_nm, ";
		$sql .= "c.emp_lt_nm, ";
		$sql .= "c.emp_ft_nm, ";
		$sql .= "b.prcdaprv_date as apv_stat, ";
		$sql .= "a.pjt_schd_id, ";
		$sql .= "null as apv_order, ";
		$sql .= "null as apv_sub_order, ";
		$sql .= "null as send_apved_order, ";
		$sql .= "null as apply_no, ";
		$sql .= "null as short_wkfw_name, ";
		$sql .= "null as wkfw_folder_id ";
		$sql .= "from proceeding a ";
		$sql .= "inner join prcdaprv b on a.pjt_schd_id = b.pjt_schd_id ";
		$sql .= "inner join empmst c on a.prcd_create_emp_id = c.emp_id ";
	    $sql .= "where b.prcdaprv_emp_id = '$emp_id' ";

		// 表題
		if($apply_title != "")
		{
			$sql .= "and a.prcd_subject like '%$apply_title%' ";
		}

		// 申請者
		if($emp_nm != "")
		{
			$sql .= "and ";
			$sql .= "(c.emp_lt_nm like '%$emp_nm%' ";
			$sql .= "or c.emp_ft_nm like '%$emp_nm%' ";
			$sql .= "or c.emp_kn_lt_nm like '%$emp_nm%' ";
			$sql .= "or c.emp_kn_ft_nm like '%$emp_nm%' ";
			$sql .= "or (c.emp_lt_nm || c.emp_ft_nm) like '%$emp_nm%' ";
			$sql .= "or (c.emp_kn_lt_nm || c.emp_kn_ft_nm) like '%$emp_nm%') ";

		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			if($date_len > 0) {
				$sql .= "and substring(a.prcd_create_time from 1 for $date_len) >= '$date_str' ";
			}
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			if($date_len > 0) {
				$sql .= "and substring(a.prcd_create_time from 1 for $date_len) <= '$date_str' ";
			}
		}

		// 承認状況
		if($apv_stat != "" && $apv_stat != "-")
		{
			if($apv_stat == "0") {
				$sql .= "and b.prcdaprv_date is null ";
			} else if($apv_stat == "1") {
			    $sql .= "and b.prcdaprv_date is not null ";
			}
		}
		return $sql;
    }

    // 残業申請のＳＱＬ取得
    function get_ovtm_approve_sql($arr_cond)
    {
		$emp_id          = $arr_cond["emp_id"];
		$apply_title     = $arr_cond["apply_title"];
		$emp_nm          = $arr_cond["emp_nm"];
		$apply_yyyy_from = $arr_cond["apply_yyyy_from"];
		$apply_mm_from   = $arr_cond["apply_mm_from"];
		$apply_dd_from   = $arr_cond["apply_dd_from"];
		$apply_yyyy_to   = $arr_cond["apply_yyyy_to"];
		$apply_mm_to     = $arr_cond["apply_mm_to"];
		$apply_dd_to     = $arr_cond["apply_dd_to"];
		$apv_stat        = $arr_cond["apv_stat"];

		$sql = "select ";
		$sql .= "varchar(1) '3' as apply_type, ";
		$sql .= "a.apply_id, ";
		$sql .= "a.apply_status as apply_stat, ";
		$sql .= "a.apply_time as apply_date, ";
		$sql .= "null as apply_title, ";
		$sql .= "'残業申請' as wkfw_title, ";
		$sql .= "'CoMedix' as wkfw_nm, ";
		$sql .= "emp_lt_nm, ";
		$sql .= "emp_ft_nm, ";
		$sql .= "b.aprv_status as apv_stat, ";
		$sql .= "null as pjt_schd_id, ";
		$sql .= "b.aprv_no as apv_order, ";
		$sql .= "b.aprv_sub_no as apv_sub_order, ";
		$sql .= "d.send_aprved_no as send_apved_order, ";
		$sql .= "null as apply_no, ";
		$sql .= "null as short_wkfw_name, ";
		$sql .= "null as wkfw_folder_id ";
		$sql .= "from ovtmapply a ";
		$sql .= "inner join ovtmaprv b on a.apply_id = b.apply_id ";
		$sql .= "left join ovtm_async_recv d on b.apply_id = d.apply_id ";
		$sql .= "and b.aprv_no = d.recv_aprv_no ";
		$sql .= "and ((b.aprv_sub_no = d.recv_aprv_sub_no) or (b.aprv_sub_no is null and d.recv_aprv_sub_no is null)) ";
		$sql .= "inner join empmst c on a.emp_id = c.emp_id ";

		$sql .= "where a.re_apply_id is null ";
		$sql .= "and not a.delete_flg ";
		$sql .= "and b.aprv_emp_id = '$emp_id' ";
		$sql .= "and (d.apv_show_flg or d.apv_show_flg is null) ";

		// 申請者
		if($emp_nm != "") {

			$sql .= "and ";
			$sql .= "(c.emp_lt_nm like '%$emp_nm%' ";
			$sql .= "or c.emp_ft_nm like '%$emp_nm%' ";
			$sql .= "or c.emp_kn_lt_nm like '%$emp_nm%' ";
			$sql .= "or c.emp_kn_ft_nm like '%$emp_nm%' ";
			$sql .= "or (c.emp_lt_nm || c.emp_ft_nm) like '%$emp_nm%' ";
			$sql .= "or (c.emp_kn_lt_nm || c.emp_kn_ft_nm) like '%$emp_nm%') ";

		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			if($date_len > 0) {
				$sql .= "and substring(a.apply_time from 1 for $date_len) >= '$date_str' ";
			}
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			if($date_len > 0) {
				$sql .= "and substring(a.apply_time from 1 for $date_len) <= '$date_str' ";
			}
		}

		// 承認状況
		if($apv_stat == "0")
		{
			$sql .= "and b.aprv_status = '0' ";

			$sql .= "and (";

			$sql .= "(((a.apply_status = '0' or a.apply_status = '1') and ";
			$sql .= "exists (select apv2.* from ";
			$sql .= "(select apply_id, aprv_no from ovtmaprv where aprv_emp_id = '$emp_id' and aprv_status = '0' ";
			$sql .= "and aprv_no = (select min(aprv_no) from ovtmaprv minovtmaprv where minovtmaprv.apply_id = a.apply_id and aprv_status = '0')) apv2 ";
			$sql .= "where a.apply_id = apv2.apply_id and b.aprv_no = apv2.aprv_no))) ";

			$sql .= "or ";
			$sql .= "(d.apv_show_flg and ";
			$sql .= "exists (select apply_id from ovtmaprv apv3 where a.apply_id = apv3.apply_id and apv3.aprv_emp_id = '$emp_id' and apv3.aprv_status = '0'))";

			$sql .= ") ";

		}
		else if($apv_stat == "" || $apv_stat == "-")
		{
			$sql .= "and (";

			$sql .= "((a.apply_status = '0' or a.apply_status = '1') and ";
			$sql .= "exists (select apv2.* from ";
			$sql .= "(select apply_id, aprv_no from ovtmaprv where aprv_emp_id = '$emp_id' and aprv_status = '0' ";
			$sql .= "and aprv_no = (select min(aprv_no) from ovtmaprv minovtmaprv where minovtmaprv.apply_id = a.apply_id and aprv_status = '0')) apv2 ";
			$sql .= "where a.apply_id = apv2.apply_id and b.aprv_no = apv2.aprv_no)) ";


			$sql .= "or ";
			$sql .= "(d.apv_show_flg and ";
			$sql .= "exists (select apply_id from ovtmaprv apv3 where a.apply_id = apv3.apply_id and apv3.aprv_emp_id = '$emp_id' and apv3.aprv_status = '0'))";

			$sql .= "or ";
			$sql .= "(";
			$sql .= "exists (select apv4.apply_id, apv4.aprv_no from ovtmaprv apv4 where a.apply_id = apv4.apply_id and b.aprv_no = apv4.aprv_no and apv4.aprv_emp_id = '$emp_id' and apv4.aprv_status <> '0'))";

			$sql .= ") ";
		}
		else
		{
			if($apv_stat != "" && $apv_stat != "-")
			{
				$sql .= "and b.aprv_status = '$apv_stat' ";
			}
		}

		return $sql;
    }

	// 勤務時間修正申請のＳＱＬ取得
	function get_tmmd_approve_sql($arr_cond)
    {
		$emp_id          = $arr_cond["emp_id"];
		$apply_title     = $arr_cond["apply_title"];
		$emp_nm          = $arr_cond["emp_nm"];
		$apply_yyyy_from = $arr_cond["apply_yyyy_from"];
		$apply_mm_from   = $arr_cond["apply_mm_from"];
		$apply_dd_from   = $arr_cond["apply_dd_from"];
		$apply_yyyy_to   = $arr_cond["apply_yyyy_to"];
		$apply_mm_to     = $arr_cond["apply_mm_to"];
		$apply_dd_to     = $arr_cond["apply_dd_to"];
		$apv_stat        = $arr_cond["apv_stat"];

		$sql = "select ";
		$sql .= "varchar(1) '4' as apply_type, ";
		$sql .= "a.apply_id, ";
		$sql .= "a.apply_status as apply_stat, ";
		$sql .= "a.apply_time as apply_date, ";
		$sql .= "null as apply_title, ";
		$sql .= "'勤務時間修正申請' as wkfw_title, ";
		$sql .= "'CoMedix' as wkfw_nm, ";
		$sql .= "emp_lt_nm, ";
		$sql .= "emp_ft_nm, ";
		$sql .= "b.aprv_status as apv_stat, ";
		$sql .= "null as pjt_schd_id, ";
		$sql .= "b.aprv_no as apv_order, ";
		$sql .= "b.aprv_sub_no as apv_sub_order, ";
		$sql .= "d.send_aprved_no as send_apved_order, ";
		$sql .= "null as apply_no, ";
		$sql .= "null as short_wkfw_name, ";
		$sql .= "null as wkfw_folder_id ";
		$sql .= "from tmmdapply a ";
		$sql .= "inner join tmmdaprv b on a.apply_id = b.apply_id ";
		$sql .= "left join tmmd_async_recv d on b.apply_id = d.apply_id ";
		$sql .= "and b.aprv_no = d.recv_aprv_no ";
		$sql .= "and ((b.aprv_sub_no = d.recv_aprv_sub_no) or (b.aprv_sub_no is null and d.recv_aprv_sub_no is null)) ";
		$sql .= "inner join empmst c on a.emp_id = c.emp_id ";

		$sql .= "where a.re_apply_id is null ";
		$sql .= "and not a.delete_flg ";
		$sql .= "and b.aprv_emp_id = '$emp_id' ";
		$sql .= "and (d.apv_show_flg or d.apv_show_flg is null) ";

		// 申請者
		if($emp_nm != "") {

			$sql .= "and ";
			$sql .= "(c.emp_lt_nm like '%$emp_nm%' ";
			$sql .= "or c.emp_ft_nm like '%$emp_nm%' ";
			$sql .= "or c.emp_kn_lt_nm like '%$emp_nm%' ";
			$sql .= "or c.emp_kn_ft_nm like '%$emp_nm%' ";
			$sql .= "or (c.emp_lt_nm || c.emp_ft_nm) like '%$emp_nm%' ";
			$sql .= "or (c.emp_kn_lt_nm || c.emp_kn_ft_nm) like '%$emp_nm%') ";

		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			if($date_len > 0) {
				$sql .= "and substring(a.apply_time from 1 for $date_len) >= '$date_str' ";
			}
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			if($date_len > 0) {
				$sql .= "and substring(a.apply_time from 1 for $date_len) <= '$date_str' ";
			}
		}

		// 承認状況
		if($apv_stat == "0")
		{
			$sql .= "and b.aprv_status = '0' ";

			$sql .= "and (";

			$sql .= "(((a.apply_status = '0' or a.apply_status = '1') and ";
			$sql .= "exists (select apv2.* from ";
			$sql .= "(select apply_id, aprv_no from tmmdaprv where aprv_emp_id = '$emp_id' and aprv_status = '0' ";
			$sql .= "and aprv_no = (select min(aprv_no) from tmmdaprv mintmmdaprv where mintmmdaprv.apply_id = a.apply_id and aprv_status = '0')) apv2 ";
			$sql .= "where a.apply_id = apv2.apply_id and b.aprv_no = apv2.aprv_no))) ";

			$sql .= "or ";
			$sql .= "(d.apv_show_flg and ";
			$sql .= "exists (select apply_id from tmmdaprv apv3 where a.apply_id = apv3.apply_id and apv3.aprv_emp_id = '$emp_id' and apv3.aprv_status = '0'))";

			$sql .= ") ";

		}
		else if($apv_stat == "" || $apv_stat == "-")
		{
			$sql .= "and (";

			$sql .= "((a.apply_status = '0' or a.apply_status = '1') and ";
			$sql .= "exists (select apv2.* from ";
			$sql .= "(select apply_id, aprv_no from tmmdaprv where aprv_emp_id = '$emp_id' and aprv_status = '0' ";
			$sql .= "and aprv_no = (select min(aprv_no) from tmmdaprv mintmmdaprv where mintmmdaprv.apply_id = a.apply_id and aprv_status = '0')) apv2 ";
			$sql .= "where a.apply_id = apv2.apply_id and b.aprv_no = apv2.aprv_no)) ";


			$sql .= "or ";
			$sql .= "(d.apv_show_flg and ";
			$sql .= "exists (select apply_id from tmmdaprv apv3 where a.apply_id = apv3.apply_id and apv3.aprv_emp_id = '$emp_id' and apv3.aprv_status = '0'))";

			$sql .= "or ";
			$sql .= "(";
			$sql .= "exists (select apv4.apply_id, apv4.aprv_no from tmmdaprv apv4 where a.apply_id = apv4.apply_id and b.aprv_no = apv4.aprv_no and apv4.aprv_emp_id = '$emp_id' and apv4.aprv_status <> '0'))";

			$sql .= ") ";
		}
		else
		{
			if($apv_stat != "" && $apv_stat != "-")
			{
				$sql .= "and b.aprv_status = '$apv_stat' ";
			}
		}

		return $sql;
    }

	// 退勤後復帰申請のＳＱＬ取得
	function get_rtn_approve_sql($arr_cond)
    {
		$emp_id          = $arr_cond["emp_id"];
		$apply_title     = $arr_cond["apply_title"];
		$emp_nm          = $arr_cond["emp_nm"];
		$apply_yyyy_from = $arr_cond["apply_yyyy_from"];
		$apply_mm_from   = $arr_cond["apply_mm_from"];
		$apply_dd_from   = $arr_cond["apply_dd_from"];
		$apply_yyyy_to   = $arr_cond["apply_yyyy_to"];
		$apply_mm_to     = $arr_cond["apply_mm_to"];
		$apply_dd_to     = $arr_cond["apply_dd_to"];
		$apv_stat        = $arr_cond["apv_stat"];

		$sql = "select ";
		$sql .= "varchar(1) '5' as apply_type, ";
		$sql .= "a.apply_id, ";
		$sql .= "a.apply_status as apply_stat, ";
		$sql .= "a.apply_time as apply_date, ";
		$sql .= "null as apply_title, ";
		$sql .= "'退勤後復帰申請' as wkfw_title, ";
		$sql .= "'CoMedix' as wkfw_nm, ";
		$sql .= "emp_lt_nm, ";
		$sql .= "emp_ft_nm, ";
		$sql .= "b.aprv_status as apv_stat, ";
		$sql .= "null as pjt_schd_id, ";
		$sql .= "b.aprv_no as apv_order, ";
		$sql .= "b.aprv_sub_no as apv_sub_order, ";
		$sql .= "d.send_aprved_no as send_apved_order, ";
		$sql .= "null as apply_no, ";
		$sql .= "null as short_wkfw_name, ";
		$sql .= "null as wkfw_folder_id ";
		$sql .= "from rtnapply a ";
		$sql .= "inner join rtnaprv b on a.apply_id = b.apply_id ";
		$sql .= "left join rtn_async_recv d on b.apply_id = d.apply_id ";
		$sql .= "and b.aprv_no = d.recv_aprv_no ";
		$sql .= "and ((b.aprv_sub_no = d.recv_aprv_sub_no) or (b.aprv_sub_no is null and d.recv_aprv_sub_no is null)) ";
		$sql .= "inner join empmst c on a.emp_id = c.emp_id ";

		$sql .= "where a.re_apply_id is null ";
		$sql .= "and not a.delete_flg ";
		$sql .= "and b.aprv_emp_id = '$emp_id' ";
		$sql .= "and (d.apv_show_flg or d.apv_show_flg is null) ";

		// 申請者
		if($emp_nm != "") {

			$sql .= "and ";
			$sql .= "(c.emp_lt_nm like '%$emp_nm%' ";
			$sql .= "or c.emp_ft_nm like '%$emp_nm%' ";
			$sql .= "or c.emp_kn_lt_nm like '%$emp_nm%' ";
			$sql .= "or c.emp_kn_ft_nm like '%$emp_nm%' ";
			$sql .= "or (c.emp_lt_nm || c.emp_ft_nm) like '%$emp_nm%' ";
			$sql .= "or (c.emp_kn_lt_nm || c.emp_kn_ft_nm) like '%$emp_nm%') ";

		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			if($date_len > 0) {
				$sql .= "and substring(a.apply_time from 1 for $date_len) >= '$date_str' ";
			}
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			if($date_len > 0) {
				$sql .= "and substring(a.apply_time from 1 for $date_len) <= '$date_str' ";
			}
		}

		// 承認状況
		if($apv_stat == "0")
		{
			$sql .= "and b.aprv_status = '0' ";

			$sql .= "and (";

			$sql .= "(((a.apply_status = '0' or a.apply_status = '1') and ";
			$sql .= "exists (select apv2.* from ";
			$sql .= "(select apply_id, aprv_no from rtnaprv where aprv_emp_id = '$emp_id' and aprv_status = '0' ";
			$sql .= "and aprv_no = (select min(aprv_no) from rtnaprv minrtnaprv where minrtnaprv.apply_id = a.apply_id and aprv_status = '0')) apv2 ";
			$sql .= "where a.apply_id = apv2.apply_id and b.aprv_no = apv2.aprv_no))) ";

			$sql .= "or ";
			$sql .= "(d.apv_show_flg and ";
			$sql .= "exists (select apply_id from rtnaprv apv3 where a.apply_id = apv3.apply_id and apv3.aprv_emp_id = '$emp_id' and apv3.aprv_status = '0'))";

			$sql .= ") ";

		}
		else if($apv_stat == "" || $apv_stat == "-")
		{
			$sql .= "and (";

			$sql .= "((a.apply_status = '0' or a.apply_status = '1') and ";
			$sql .= "exists (select apv2.* from ";
			$sql .= "(select apply_id, aprv_no from rtnaprv where aprv_emp_id = '$emp_id' and aprv_status = '0' ";
			$sql .= "and aprv_no = (select min(aprv_no) from rtnaprv minrtnaprv where minrtnaprv.apply_id = a.apply_id and aprv_status = '0')) apv2 ";
			$sql .= "where a.apply_id = apv2.apply_id and b.aprv_no = apv2.aprv_no)) ";


			$sql .= "or ";
			$sql .= "(d.apv_show_flg and ";
			$sql .= "exists (select apply_id from rtnaprv apv3 where a.apply_id = apv3.apply_id and apv3.aprv_emp_id = '$emp_id' and apv3.aprv_status = '0'))";

			$sql .= "or ";
			$sql .= "(";
			$sql .= "exists (select apv4.apply_id, apv4.aprv_no from rtnaprv apv4 where a.apply_id = apv4.apply_id and b.aprv_no = apv4.aprv_no and apv4.aprv_emp_id = '$emp_id' and apv4.aprv_status <> '0'))";

			$sql .= ") ";
		}
		else
		{
			if($apv_stat != "" && $apv_stat != "-")
			{
				$sql .= "and b.aprv_status = '$apv_stat' ";
			}
		}

		return $sql;
    }

	// 承認一覧取得ＳＱＬ
    function get_approvelist_sql($arr_cond)
    {
		$wkfw_type       = $arr_cond["wkfw_type"];
		$wkfw_id         = $arr_cond["wkfw_id"];
		$apply_title     = $arr_cond["apply_title"];
		$apv_stat        = $arr_cond["apv_stat"];

		$sql = "";

		//-------------------------------------------
		// ■通常の申請
		// ・「カテゴリ」がCoMedix(0)以外
		//-------------------------------------------
		if($wkfw_type != "0")
		{
			$sql .= $this->get_normal_approve_sql($arr_cond);
		}

/*
		//--------------------------------------------------------------------------------------------------------------------------
		// ■議事録公開申請（委員会・WG）
		// ・「カテゴリ」がすべて(-) and 「承認状況」が否認(2) and 差戻し(3)以外
		// ・「カテゴリ」がCoMedix(0) and 「申請書名」がすべて(-) and 「承認状況」が否認(2) and 差戻し(3)以外
		// ・「カテゴリ」がCoMedix(0) and 「申請書名」が議事録公開申請（委員会・WG）(1) and 「承認状況」が否認(2) and 差戻し(3)以外
		//--------------------------------------------------------------------------------------------------------------------------
		if($wkfw_type == ""
		    or ($wkfw_type == "-" and $apv_stat != "2" and $apv_stat != "3")
	        or ($wkfw_type == "0" and $wkfw_id == "-" and $apv_stat != "2" and $apv_stat != "3")
	        or ($wkfw_type == "0" and $wkfw_id == "1" and $apv_stat != "2" and $apv_stat != "3")) {

			if($sql != "")
			{
				$sql .= "union all ";
			}
	    	$sql .= $this->get_proceeding_approve_sql($arr_cond);
		}

		//--------------------------------------------------------------------------------------------------------------------------
		// ■残業申請
		// ・「カテゴリ」がすべて(-) and 表題が未入力の場合
		// ・「カテゴリ」がCoMedix(0) and 「申請書名」がすべて(-) and 表題が未入力の場合
		// ・「カテゴリ」がCoMedix(0) and 「申請書名」が残業申請(2) and 表題が未入力の場合
		//--------------------------------------------------------------------------------------------------------------------------

		if($wkfw_type == ""
		    or ($wkfw_type == "-" and $apply_title == "")
	        or ($wkfw_type == "0" and $wkfw_id == "-" and $apply_title == "")
	        or ($wkfw_type == "0" and $wkfw_id == "2" and $apply_title == ""))
	    {
			if($sql != "")
			{
				$sql .= "union all ";
			}
			$sql .= $this->get_ovtm_approve_sql($arr_cond);
		}

		//--------------------------------------------------------------------------------------------------------------------------
		// ■勤務時間修正申請
		// ・「カテゴリ」がすべて(-) and 表題が未入力の場合
		// ・「カテゴリ」がCoMedix(0) and 「申請書名」がすべて(-) and 表題が未入力の場合
		// ・「カテゴリ」がCoMedix(0) and 「申請書名」が勤務時間修正申請(3) and 表題が未入力の場合
		//--------------------------------------------------------------------------------------------------------------------------
		if($wkfw_type == ""
			or ($wkfw_type == "-" and $apply_title == "")
	        or ($wkfw_type == "0" and $wkfw_id == "-" and $apply_title == "")
	        or ($wkfw_type == "0" and $wkfw_id == "3" and $apply_title == "")) {

			if($sql != "")
			{
				$sql .= "union all ";
			}

			$sql .= $this->get_tmmd_approve_sql($arr_cond);
		}

		//--------------------------------------------------------------------------------------------------------------------------
		// ■退勤後復帰申請
		// ・「カテゴリ」がすべて(-) and 表題が未入力の場合
		// ・「カテゴリ」がCoMedix(0) and 「申請書名」がすべて(-) and 表題が未入力の場合
		// ・「カテゴリ」がCoMedix(0) and 「申請書名」が退勤後復帰申請(4) and 表題が未入力の場合
		//--------------------------------------------------------------------------------------------------------------------------
		if($wkfw_type == ""
			or ($wkfw_type == "-" and $apply_title == "")
	        or ($wkfw_type == "0" and $wkfw_id == "-" and $apply_title == "")
	        or ($wkfw_type == "0" and $wkfw_id == "4" and $apply_title == ""))
	    {
			if($sql != "")
			{
				$sql .= "union all ";
			}
			$sql .= $this->get_rtn_approve_sql($arr_cond);
		}
*/

		return $sql;
    }

//-------------------------------------------------------------------------------------------------------------------------
// 通知件数取得
//-------------------------------------------------------------------------------------------------------------------------
    // 承認済み情報取得
    function get_apply_fix($emp_id)
    {
//        $sql = "select varchar(1) '1' as apply_type, apply.apply_id, null as pjt_schd_id from apply where apply.emp_id = '$emp_id' and apply.apply_stat = '1' and apply.apv_fix_show_flg = 't' union select varchar(1) '2' as apply_type, null, a.pjt_schd_id from proceeding a inner join (select pjt_schd_id from prcdaprv group by pjt_schd_id) b on a.pjt_schd_id = b.pjt_schd_id where a.prcd_create_emp_id = '$emp_id' and a.prcd_status = '2' and a.apv_fix_show_flg = 't' union select varchar(1) '3', apply_id, null as pjt_schd_id from ovtmapply where apply_status = '1' and emp_id = '$emp_id' and not delete_flg and apv_fix_show_flg = 't' union select varchar(1) '4', apply_id, null as pjt_schd_id from tmmdapply where apply_status = '1' and emp_id = '$emp_id' and apv_fix_show_flg = 't' union select varchar(1) '5', apply_id, null as pjt_schd_id from rtnapply where apply_status = '1' and emp_id = '$emp_id' and apv_fix_show_flg = 't' ";
//        $sql  = "select varchar(1) '1' as apply_type, apply_id, null as pjt_schd_id from fplusapply ";
        $sql  = "select apply_id from fplusapply ";
        $sql .= "where apply_stat = '1' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_fix_show_flg ";
/*
        $sql .= "union ";
        $sql .= "select varchar(1) '2' as apply_type, null, a.pjt_schd_id from proceeding a ";
        $sql .= "inner join (select pjt_schd_id from prcdaprv group by pjt_schd_id) b on a.pjt_schd_id = b.pjt_schd_id ";
        $sql .= "where a.prcd_create_emp_id = '$emp_id' and a.prcd_status = '2' and a.apv_fix_show_flg ";
        $sql .= "union ";
        $sql .= "select varchar(1) '3' as apply_type, apply_id, null as pjt_schd_id from ovtmapply ";
        $sql .= "where apply_status = '1' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_fix_show_flg ";
        $sql .= "union ";
        $sql .= "select varchar(1) '4' as apply_type, apply_id, null as pjt_schd_id from tmmdapply ";
        $sql .= "where apply_status = '1' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_fix_show_flg ";
        $sql .= "union ";
        $sql .= "select varchar(1) '5' as apply_type, apply_id, null as pjt_schd_id from rtnapply ";
        $sql .= "where apply_status = '1' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_fix_show_flg ";
*/

	    $cond = "";
	    $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
	    if ($sel == 0) {
		    pg_close($this->_db_con);
		    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		    exit;
        }

        $num = pg_numrows($sel);
        return $num;
    }


    // 差戻し情報取得
    function get_apply_bak($emp_id)
    {
//        $sql = "select varchar(1) '1' as apply_type, apply.apply_id from apply where apply.emp_id = '$emp_id' and apply.apply_stat = '3' and re_apply_id is null and apply.apv_bak_show_flg = 't' union select varchar(1) '3', apply_id from ovtmapply where apply_status = '3' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_bak_show_flg = 't' union select varchar(1) '4', apply_id from tmmdapply where apply_status = '3' and emp_id = '$emp_id' and apv_bak_show_flg = 't' union select varchar(1) '5', apply_id from rtnapply where apply_status = '3' and emp_id = '$emp_id' and apv_bak_show_flg = 't' ";

//        $sql  = "select varchar(1) '1' as apply_type, apply_id from fplusapply ";
        $sql  = "select apply_id from fplusapply ";
        $sql .= "where apply_stat = '3' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_bak_show_flg ";
/*
        $sql .= "union ";
        $sql .= "select varchar(1) '3' as apply_type, apply_id from ovtmapply ";
        $sql .= "where apply_status = '3' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_bak_show_flg ";
        $sql .= "union ";
        $sql .= "select varchar(1) '4' as apply_type, apply_id from tmmdapply ";
        $sql .= "where apply_status = '3' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_bak_show_flg ";
        $sql .= "union ";
        $sql .= "select varchar(1) '5' as apply_type, apply_id from rtnapply ";
        $sql .= "where apply_status = '3' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_bak_show_flg ";
*/

        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_numrows($sel);
        return $num;
    }


    // 承認待ち情報情報
    function get_approve_wait($emp_id)
    {
/*
        $sql = "select varchar(1) '1' as apply_type, a.apply_id, a.wkfw_id, a.emp_id, a.apply_date, a.apply_title, null as pjt_schd_id, b.apv_order, b.apv_sub_order from apply a inner join applyapv b on a.apply_id = b.apply_id left join applyasyncrecv c on b.apply_id = c.apply_id and b.apv_order = c.recv_apv_order and ((b.apv_sub_order = c.recv_apv_sub_order) or (b.apv_sub_order is null and c.recv_apv_sub_order is null)) where not a.delete_flg and not a.draft_flg and (c.apv_show_flg or c.apv_show_flg is null) and b.emp_id = '$emp_id' and a.re_apply_id is null and b.apv_stat = '0' and ((a.wkfw_appr = '1' and exists (select apply_id from applyapv apv1 where a.apply_id = apv1.apply_id and apv1.emp_id = '$emp_id' and apv1.apv_stat = '0')) or (a.wkfw_appr = '2' and a.apply_stat = '0' and exists (select apv2.* from (select apply_id, apv_order from applyapv where emp_id = '$emp_id' and apv_stat = '0' and apv_order = (select min(apv_order) from applyapv minapplyapv where minapplyapv.apply_id = a.apply_id and apv_stat = '0')) apv2 where a.apply_id = apv2.apply_id and b.apv_order = apv2.apv_order)) or (a.wkfw_appr = '2' and c.apv_show_flg and exists (select apply_id from applyapv apv3 where a.apply_id = apv3.apply_id and apv3.emp_id = '$emp_id' and apv3.apv_stat = '0'))) union select varchar(1) '2', null, null, prcd_create_emp_id, prcd_create_time, prcd_subject, pjt_schd_id, null, null from proceeding where prcd_status = '1' and pjt_schd_id in (select pjt_schd_id from prcdaprv where prcdaprv_emp_id = '$emp_id' and prcdaprv_date is null) ";
	    $sql = $sql."union ";
	    $sql = $sql."select varchar(1) '3', a.apply_id, null, a.emp_id, a.apply_time, null, null as pjt_schd_id, b.aprv_no, b.aprv_sub_no from ovtmapply a inner join ovtmaprv b on a.apply_id = b.apply_id left join ovtm_async_recv c on b.apply_id = c.apply_id and b.aprv_no = c.recv_aprv_no and ((b.aprv_sub_no = c.recv_aprv_sub_no) or (b.aprv_sub_no is null and c.recv_aprv_sub_no is null)) where not a.delete_flg and (c.apv_show_flg or c.apv_show_flg is null) and b.aprv_emp_id = '$emp_id' and a.re_apply_id is null and b.aprv_status = '0' and ((((a.apply_status = '0' or a.apply_status = '1') and exists (select apv2.* from (select apply_id, aprv_no from ovtmaprv where aprv_emp_id = '$emp_id' and aprv_status = '0' and aprv_no = (select min(aprv_no) from ovtmaprv minapplyapv where minapplyapv.apply_id = a.apply_id and aprv_status = '0')) apv2 where a.apply_id = apv2.apply_id and b.aprv_no = apv2.aprv_no))) or (c.apv_show_flg and exists (select apply_id from ovtmaprv apv3 where a.apply_id = apv3.apply_id and apv3.aprv_emp_id = '$emp_id' and apv3.aprv_status = '0'))) ";
	    $sql = $sql."union select varchar(1) '4', apply_id, null, emp_id, apply_time, null, null, null, null from tmmdapply where apply_status = '0' and apply_id in (select apply_id from tmmdaprv where aprv_emp_id = '$emp_id' and aprv_status = '0') union select varchar(1) '5', apply_id, null, emp_id, apply_time, null, null, null, null from rtnapply where apply_status = '0' and apply_id in (select apply_id from rtnaprv where aprv_emp_id = '$emp_id' and aprv_status = '0')";
*/
//        $sql  = "select varchar(1) '1' as apply_type, a.apply_id, a.wkfw_id, a.emp_id, a.apply_date, a.apply_title, null as pjt_schd_id, b.apv_order, b.apv_sub_order ";
        $sql  = "select a.apply_id ";
        $sql .= "from fplusapply a ";
        $sql .= "inner join fplusapplyapv b on a.apply_id = b.apply_id ";
        $sql .= "left join fplusapplyasyncrecv c on b.apply_id = c.apply_id and b.apv_order = c.recv_apv_order and ";
        $sql .= "((b.apv_sub_order = c.recv_apv_sub_order) or (b.apv_sub_order is null and c.recv_apv_sub_order is null)) ";
        $sql .= "where ";
        $sql .= "not a.delete_flg and ";
        $sql .= "not a.draft_flg and ";
        $sql .= "(c.apv_show_flg or c.apv_show_flg is null) and ";
        $sql .= "b.emp_id = '$emp_id' and ";
        $sql .= "a.re_apply_id is null and ";
        $sql .= "b.apv_stat = '0' and ";
        $sql .= "(";
        $sql .= "(a.wkfw_appr = '1' and ";
        $sql .= "exists (select apply_id from fplusapplyapv apv1 where a.apply_id = apv1.apply_id and apv1.emp_id = '$emp_id' and apv1.apv_stat = '0') ";
        $sql .= ") ";
        $sql .= "or ";
        $sql .= "(a.wkfw_appr = '2' and a.apply_stat = '0' and ";
        $sql .= "exists (select apv2.* from (select apply_id, apv_order from fplusapplyapv where emp_id = '$emp_id' and apv_stat = '0' and ";
        $sql .= "apv_order = (select min(apv_order) from fplusapplyapv minapplyapv where minapplyapv.apply_id = a.apply_id and apv_stat = '0')) apv2 ";
        $sql .= "where a.apply_id = apv2.apply_id and b.apv_order = apv2.apv_order) ";
        $sql .= ") ";
        $sql .= "or ";
        $sql .= "(a.wkfw_appr = '2' and c.apv_show_flg and ";
        $sql .= "exists (select apply_id from fplusapplyapv apv3 where a.apply_id = apv3.apply_id and apv3.emp_id = '$emp_id' and apv3.apv_stat = '0') ";
        $sql .= ") ";
        $sql .= ") ";

/*
        $sql .= "union ";
        $sql .= "select varchar(1) '2' as apply_type, null, null, prcd_create_emp_id, prcd_create_time, prcd_subject, pjt_schd_id, null, null ";
        $sql .= "from proceeding ";
        $sql .= "where prcd_status = '1' and pjt_schd_id in (select pjt_schd_id from prcdaprv where prcdaprv_emp_id = '$emp_id' and prcdaprv_date is null) ";

        $sql .= "union ";
        $sql .= "select varchar(1) '3' as apply_type, a.apply_id, null, a.emp_id, a.apply_time, null, null as pjt_schd_id, b.aprv_no, b.aprv_sub_no ";
        $sql .= "from ovtmapply a ";
        $sql .= "inner join ovtmaprv b on a.apply_id = b.apply_id ";
        $sql .= "left join ovtm_async_recv c on b.apply_id = c.apply_id and b.aprv_no = c.recv_aprv_no and ";
        $sql .= "((b.aprv_sub_no = c.recv_aprv_sub_no) or (b.aprv_sub_no is null and c.recv_aprv_sub_no is null)) ";
        $sql .= "where ";
        $sql .= "not a.delete_flg and ";
        $sql .= "(c.apv_show_flg or c.apv_show_flg is null) and ";
        $sql .= "b.aprv_emp_id = '$emp_id' and ";
        $sql .= "a.re_apply_id is null and ";
        $sql .= "b.aprv_status = '0' and ";
        $sql .= "( ";
        $sql .= "( ";
        $sql .= "( ";
        $sql .= "(a.apply_status = '0' or a.apply_status = '1') and ";
        $sql .= "exists (select apv2.* from (select apply_id, aprv_no from ovtmaprv where aprv_emp_id = '$emp_id' and aprv_status = '0' and ";
        $sql .= "aprv_no = (select min(aprv_no) from ovtmaprv minapplyapv where minapplyapv.apply_id = a.apply_id and aprv_status = '0')) apv2 ";
        $sql .= "where a.apply_id = apv2.apply_id and b.aprv_no = apv2.aprv_no) ";
        $sql .= ") ";
        $sql .= ") ";
        $sql .= "or ";
        $sql .= "(c.apv_show_flg and ";
        $sql .= "exists (select apply_id from ovtmaprv apv3 where a.apply_id = apv3.apply_id and apv3.aprv_emp_id = '$emp_id' and apv3.aprv_status = '0') ";
        $sql .= ") ";
        $sql .= ") ";

        $sql .= "union ";
        $sql .= "select varchar(1) '4' as apply_type, a.apply_id, null, a.emp_id, a.apply_time, null, null as pjt_schd_id, b.aprv_no, b.aprv_sub_no ";
        $sql .= "from tmmdapply a ";
        $sql .= "inner join tmmdaprv b on a.apply_id = b.apply_id ";
        $sql .= "left join tmmd_async_recv c on b.apply_id = c.apply_id and b.aprv_no = c.recv_aprv_no and ";
        $sql .= "((b.aprv_sub_no = c.recv_aprv_sub_no) or (b.aprv_sub_no is null and c.recv_aprv_sub_no is null)) ";
        $sql .= "where ";
        $sql .= "not a.delete_flg and ";
        $sql .= "(c.apv_show_flg or c.apv_show_flg is null) and ";
        $sql .= "b.aprv_emp_id = '$emp_id' and ";
        $sql .= "a.re_apply_id is null and ";
        $sql .= "b.aprv_status = '0' and ";
        $sql .= "( ";
        $sql .= "( ";
        $sql .= "( ";
        $sql .= "(a.apply_status = '0' or a.apply_status = '1') and ";
        $sql .= "exists (select apv2.* from (select apply_id, aprv_no from tmmdaprv where aprv_emp_id = '$emp_id' and aprv_status = '0' and ";
        $sql .= "aprv_no = (select min(aprv_no) from tmmdaprv minapplyapv where minapplyapv.apply_id = a.apply_id and aprv_status = '0')) apv2 ";
        $sql .= "where a.apply_id = apv2.apply_id and b.aprv_no = apv2.aprv_no) ";
        $sql .= ") ";
        $sql .= ") ";
        $sql .= "or ";
        $sql .= "(c.apv_show_flg and ";
        $sql .= "exists (select apply_id from tmmdaprv apv3 where a.apply_id = apv3.apply_id and apv3.aprv_emp_id = '$emp_id' and apv3.aprv_status = '0') ";
        $sql .= ") ";
        $sql .= ") ";

        $sql .= "union ";
        $sql .= "select varchar(1) '5' as apply_type, a.apply_id, null, a.emp_id, a.apply_time, null, null as pjt_schd_id, b.aprv_no, b.aprv_sub_no ";
        $sql .= "from rtnapply a ";
        $sql .= "inner join rtnaprv b on a.apply_id = b.apply_id ";
        $sql .= "left join rtn_async_recv c on b.apply_id = c.apply_id and b.aprv_no = c.recv_aprv_no and ";
        $sql .= "((b.aprv_sub_no = c.recv_aprv_sub_no) or (b.aprv_sub_no is null and c.recv_aprv_sub_no is null)) ";
        $sql .= "where ";
        $sql .= "not a.delete_flg and ";
        $sql .= "(c.apv_show_flg or c.apv_show_flg is null) and ";
        $sql .= "b.aprv_emp_id = '$emp_id' and ";
        $sql .= "a.re_apply_id is null and ";
        $sql .= "b.aprv_status = '0' and ";
        $sql .= "( ";
        $sql .= "( ";
        $sql .= "( ";
        $sql .= "(a.apply_status = '0' or a.apply_status = '1') and ";
        $sql .= "exists (select apv2.* from (select apply_id, aprv_no from rtnaprv where aprv_emp_id = '$emp_id' and aprv_status = '0' and ";
        $sql .= "aprv_no = (select min(aprv_no) from rtnaprv minapplyapv where minapplyapv.apply_id = a.apply_id and aprv_status = '0')) apv2 ";
        $sql .= "where a.apply_id = apv2.apply_id and b.aprv_no = apv2.aprv_no) ";
        $sql .= ") ";
        $sql .= ") ";
        $sql .= "or ";
        $sql .= "(c.apv_show_flg and ";
        $sql .= "exists (select apply_id from rtnaprv apv3 where a.apply_id = apv3.apply_id and apv3.aprv_emp_id = '$emp_id' and apv3.aprv_status = '0') ";
        $sql .= ") ";
        $sql .= ") ";
*/

        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        $num = pg_numrows($sel);
        return $num;
    }


    // 否認件数取得
    function get_apply_ng($emp_id)
    {
//	    $sql = "select varchar(1) '1' as apply_type, apply.apply_id from apply where apply.emp_id = '$emp_id' and apply.apply_stat = '2' and re_apply_id is null and apply.apv_ng_show_flg = 't' union select varchar(1) '3', apply_id from ovtmapply where apply_status = '2' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_ng_show_flg = 't' union select varchar(1) '4', apply_id from tmmdapply where apply_status = '2' and emp_id = '$emp_id' and apv_ng_show_flg = 't' union select varchar(1) '5', apply_id from rtnapply where apply_status = '2' and emp_id = '$emp_id' and apv_ng_show_flg = 't' ";

//        $sql  = "select varchar(1) '1' as apply_type, apply_id from fplusapply ";
        $sql  = "select apply_id from fplusapply ";
        $sql .= "where apply_stat = '2' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_ng_show_flg ";
/*
        $sql .= "union ";
        $sql .= "select varchar(1) '3' as apply_type, apply_id from ovtmapply ";
        $sql .= "where apply_status = '2' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_ng_show_flg ";
        $sql .= "union ";
        $sql .= "select varchar(1) '4' as apply_type, apply_id from tmmdapply ";
        $sql .= "where apply_status = '2' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_ng_show_flg ";
        $sql .= "union ";
        $sql .= "select varchar(1) '5' as apply_type, apply_id from rtnapply ";
        $sql .= "where apply_status = '2' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_ng_show_flg ";
*/

        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_numrows($sel);
        return $num;
    }


    // 一部承認件数取得
    function get_apply_fix_1($emp_id)
    {

        // 申請ステータスが0:申請中のデータで、
        // 承認ステータスが1:承認の件数を承認テーブルから申請毎にカウントする。
        // 1人以上いる申請を計算し一部承認の件数を取得する。
        // 削除は除く。未読フラグapv_fix_show_flgはtであること。
        // 議事録分も同様に追加。
	    //$sql = "select sum(case when b.apv_1 > 0 then 1 else 0 end) as apv_1_cnt from (select count(a.apply_id) as apv_1 from applyapv a where a.apply_id in (select apply_id from apply where emp_id = '$emp_id' and delete_flg = 'f' and apply_stat = '0') and a.apv_stat = '1' and a.delete_flg = 'f' and a.apv_fix_show_flg = 't' group by a.apply_id) b";

        $sql  = "select sum(case when b.apv_1 > 0 then 1 else 0 end) as apv_1_cnt ";
        $sql .= "from ";
        $sql .= "(select count(a.apply_id) as apv_1 from fplusapplyapv a ";
        $sql .= "where a.apply_id in (select apply_id from fplusapply where emp_id = '$emp_id' and not delete_flg and apply_stat = '0') and ";
        $sql .= "a.apv_stat = '1' and ";
        $sql .= "not a.delete_flg and ";
        $sql .= "a.apv_fix_show_flg ";
        $sql .= "group by a.apply_id ";
        $sql .= ") b ";

        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_fetch_result($sel, 0, "apv_1_cnt");
        if ($num == "") {
            $num = 0;
        }

/*
        // $sql = "select sum(case when b.apv_1 > 0 then 1 else 0 end) as apv_1_cnt from (select count(a.pjt_schd_id) as apv_1 from prcdaprv a where a.pjt_schd_id in (select pjt_schd_id from proceeding where prcd_create_emp_id = '$emp_id' and prcd_status = '1') and a.prcdaprv_date is not null and a.apv_fix_show_flg = 't' group by a.pjt_schd_id) b";
        $sql  = "select sum(case when b.apv_1 > 0 then 1 else 0 end) as apv_1_cnt ";
        $sql .= "from ";
        $sql .= "(select count(a.pjt_schd_id) as apv_1 from prcdaprv a ";
        $sql .= "where a.pjt_schd_id in (select pjt_schd_id from proceeding where prcd_create_emp_id = '$emp_id' and prcd_status = '1') and ";
        $sql .= "a.prcdaprv_date is not null and ";
        $sql .= "a.apv_fix_show_flg ";
        $sql .= "group by a.pjt_schd_id ";
        $sql .= ") b ";
        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num2 = pg_fetch_result($sel, 0, "apv_1_cnt");
        if ($num2 == "") {
            $num2 = 0;
        }

        //残業申請一部承認件数取得
        //$sql = "select sum(case when b.apv_1 > 0 then 1 else 0 end) as apv_1_cnt from (select count(a.apply_id) as apv_1 from ovtmaprv a where a.apply_id in (select apply_id from ovtmapply where emp_id = '$emp_id' and delete_flg = 'f' and apply_status = '0') and a.aprv_status = '1' and a.delete_flg = 'f' and a.apv_fix_show_flg = 't' group by a.apply_id) b";
        $sql  = "select sum(case when b.apv_1 > 0 then 1 else 0 end) as apv_1_cnt ";
        $sql .= "from ";
        $sql .= "(select count(a.apply_id) as apv_1 from ovtmaprv a ";
        $sql .= "where a.apply_id in (select apply_id from ovtmapply where emp_id = '$emp_id' and not delete_flg and apply_status = '0') and ";
        $sql .= "a.aprv_status = '1' and ";
        $sql .= "not a.delete_flg and ";
        $sql .= "a.apv_fix_show_flg ";
        $sql .= "group by a.apply_id ";
        $sql .= ") b ";

        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num3 = pg_fetch_result($sel, 0, "apv_1_cnt");
        if ($num3 == "") {
            $num3 = 0;
        }

        //勤務時間修正申請一部承認件数取得
        $sql  = "select sum(case when b.apv_1 > 0 then 1 else 0 end) as apv_1_cnt ";
        $sql .= "from ";
        $sql .= "(select count(a.apply_id) as apv_1 from tmmdaprv a ";
        $sql .= "where a.apply_id in (select apply_id from tmmdapply where emp_id = '$emp_id' and not delete_flg and apply_status = '0') and ";
        $sql .= "a.aprv_status = '1' and ";
        $sql .= "not a.delete_flg and ";
        $sql .= "a.apv_fix_show_flg ";
        $sql .= "group by a.apply_id ";
        $sql .= ") b ";

        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num4 = pg_fetch_result($sel, 0, "apv_1_cnt");
        if ($num4 == "") {
            $num4 = 0;
        }

        //退勤後復帰申請一部承認件数取得
        $sql  = "select sum(case when b.apv_1 > 0 then 1 else 0 end) as apv_1_cnt ";
        $sql .= "from ";
        $sql .= "(select count(a.apply_id) as apv_1 from rtnaprv a ";
        $sql .= "where a.apply_id in (select apply_id from rtnapply where emp_id = '$emp_id' and not delete_flg and apply_status = '0') and ";
        $sql .= "a.aprv_status = '1' and ";
        $sql .= "not a.delete_flg and ";
        $sql .= "a.apv_fix_show_flg ";
        $sql .= "group by a.apply_id ";
        $sql .= ") b ";

        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num5 = pg_fetch_result($sel, 0, "apv_1_cnt");
        if ($num5 == "") {
            $num5 = 0;
        }

        return $num+$num2+$num3+$num4+$num5;
*/

		return $num;
    }




//-------------------------------------------------------------------------------------------------------------------------
// 共通
//-------------------------------------------------------------------------------------------------------------------------

	// 職員情報取得
	function get_empmst($session)
	{
		$sql  = "select * from empmst";
		$cond = "where emp_id in (select emp_id from session where session_id='$session')";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_all($sel);
	}

	// 職員詳細情報取得
	function get_empmst_detail($emp_id)
	{
		$sql  = "select empmst.*, stmst.st_nm, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, classroom.room_nm ";
		$sql .= "from empmst ";
		$sql .= "inner join authmst on empmst.emp_id = authmst.emp_id and not authmst.emp_del_flg ";
		$sql .= "left join stmst on empmst.emp_st = stmst.st_id ";
		$sql .= "left join classmst on empmst.emp_class = classmst.class_id ";
		$sql .= "left join atrbmst on empmst.emp_attribute = atrbmst.atrb_id ";
		$sql .= "left join deptmst on empmst.emp_dept = deptmst.dept_id ";
		$sql .= "left join classroom on empmst.emp_room = classroom.room_id ";
		$cond = "where empmst.emp_id = '$emp_id'";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_all($sel);

	}

	// 室名称取得
	function get_room_nm($room_id)
	{
		$sql  = "select * from classroom";
		$cond = "where room_id = $room_id";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$room_nm = pg_result($sel,0,"room_nm");
		return $room_nm;
	}

	// 部署情報取得
	function get_classname()
	{
		$sql  = "select * from classname";
		$cond = "";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_all($sel);
	}

	// 部門マスタ取得
	function get_class_mst() {
		$sql = "select class_id, class_nm from classmst";
		$cond = "where class_del_flg = 'f' order by order_no";
		$sel_class = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_class == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_class;
	}

	// 課マスタ取得
	function get_atrb_mst() {
		$sql = "select class_id, atrb_id, atrb_nm from atrbmst";
		$cond = "where atrb_del_flg = 'f' order by class_id, order_no";
		$sel_atrb = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_atrb == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_atrb;
	}

	// 科マスタ取得
	function get_dept_mst() {
		$sql = "select atrb_id, dept_id, dept_nm from deptmst";
		$cond = "where dept_del_flg = 'f' order by atrb_id, order_no";
		$sel_dept = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_dept == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_dept;
	}

	// 室マスタ取得
	function get_room_mst() {
		$sql = "select dept_id, room_id, room_nm from classroom";
		$cond = "where room_del_flg = 'f' order by dept_id, order_no";
		$sel_room = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_room == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_room;
	}

//-------------------------------------------------------------------------------------------------------------------------
// その他
//-------------------------------------------------------------------------------------------------------------------------

	// 委員会ＷＧ名取得
	function get_pjt_nm($pjt_id)
	{
		$sql  = "select pjt_name from project";
		$cond = "where pjt_id = $pjt_id ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "pjt_name");
	}

	// 未承認以外をカウントする
	function get_applyapv_cnt($apply_id)
	{
		$sql  = "select count(*) as cnt from fplusapplyapv";
		$cond = "where apply_id = $apply_id and apv_stat <> '0'";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0, "cnt");
	}

	// マージ処理
	function merge_arr_emp_info($arr_all_emp_info, $arr_target_emp_info)
	{
		$arr_tmp_apv = array();
		foreach($arr_target_emp_info as $target_emp_info)
		{
			$dpl_flg = false;
			foreach($arr_all_emp_info as $all_emp_info)
			{
				if($target_emp_info["emp_id"] == $all_emp_info["emp_id"])
				{
					$dpl_flg = true;
				}
			}
			if(!$dpl_flg)
			{
				$arr_tmp_apv[] = $target_emp_info;
			}
		}

		for($i=0; $i<count($arr_tmp_apv); $i++)
		{
			array_push($arr_all_emp_info, $arr_tmp_apv[$i]);
		}

		return $arr_all_emp_info;
	}

	// 本体用ワークフローディレクトリ作成
	function create_wkfwreal_directory() {
		if (!is_dir("fplus")) {
			mkdir("fplus", 0755);
		}
		if (!is_dir("fplus/workflow")) {
			mkdir("fplus/workflow", 0755);
		}
		if (!is_dir("fplus/workflow/real")) {
			mkdir("fplus/workflow/real", 0755);

			// フォーマットファイルコピー
			foreach (glob("fplus/workflow/*.*") as $file) {
				$tmp_file = substr($file, 9);
				copy($file, "fplus/workflow/real/$tmp_file");
			}
		}
	}

	// ワークフロー用ディレクトリ作成
	function create_wkfwtmp_directory() {
		if (!is_dir("fplus")) {
			mkdir("fplus", 0755);
		}
		if (!is_dir("fplus/workflow")) {
			mkdir("fplus/workflow", 0755);
		}
		if (!is_dir("fplus/workflow/tmp")) {
			mkdir("fplus/workflow/tmp", 0755);
		}
	}

	// 申請用ディレクトリ作成
	function create_applytmp_directory() {
		if (!is_dir("fplus")) {
			mkdir("fplus", 0755);
		}
		if (!is_dir("fplus/apply")) {
			mkdir("fplus/apply", 0755);
		}
		if (!is_dir("fplus/apply/tmp")) {
			mkdir("fplus/apply/tmp", 0755);
		}
	}
}

function fplus_get_wkfw_send_mail_flg($con, $wkfw_id, $fname) {
	$sql = "select send_mail_flg from fpluswkfwmst";
	$cond="where wkfw_id = $wkfw_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	return pg_fetch_result($sel, 0, "send_mail_flg");
}

function fplus_must_send_mail($wkfw_send_mail_flg, $emp_email, $wkfw_appr, $apv_order) {
	return ($wkfw_send_mail_flg == "t" && $emp_email != "" && ($wkfw_appr == "1" || ($wkfw_appr == "2" && $apv_order == 1)));
}

function fplus_format_emp_nm($emp_detail) {
	return $emp_detail["emp_lt_nm"] . " " . $emp_detail["emp_ft_nm"];
}

function fplus_format_emp_mail($emp_detail) {
	if ($emp_detail["emp_email2"] != "") {
		return $emp_detail["emp_email2"];
	}
	require("webmail/config/config.php");
	return "noreply@" . $domain;
}

function fplus_format_emp_pos($emp_detail) {
	$labels = array($emp_detail["class_nm"], $emp_detail["dept_nm"], $emp_detail["atrb_nm"]);
	if ($emp_detail["room_nm"] != "") {
		$labels[] = $emp_detail["room_nm"];
	}
	return join(" > ", $labels);
}

function fplus_format_mail_content($wkfw_content_type, $content) {
	return ($wkfw_content_type == "2") ? "" : $content;
}

function fplus_get_wkfw_id_by_apply_id($con, $apply_id, $fname) {
	$sql = "select wkfw_id from fplusapply";
	$cond="where apply_id = $apply_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	return pg_fetch_result($sel, 0, "wkfw_id");
}

function fplus_get_apply_by_apply_id($con, $apply_id, $fname) {
	$sql = "select * from fplusapply";
	$cond="where apply_id = $apply_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	return pg_fetch_array($sel);
}

function fplus_must_send_mail_to_next_approvers($con, $obj, $apply_id, $wkfw_appr, $apv_order, $approve, $next_notice_div, $fname) {

	// 稟議でない場合はメールを送らない
	if ($wkfw_appr != "2") {
		return false;
	}

	// 「承認」でない場合はメールを送らない
	if ($approve != "1") {
		return false;
	}

	// ワークフローが通知メールなし設定の場合はメールを送らない
	$wkfw_id = fplus_get_wkfw_id_by_apply_id($con, $apply_id, $fname);
	$wkfw_send_mail_flg = fplus_get_wkfw_send_mail_flg($con, $wkfw_id, $fname);
	if ($wkfw_send_mail_flg != "t") {
		return false;
	}

	// 次の階層がなければメール送信不要
	if ($apv_order == $obj->get_last_apv_order($apply_id)) {
		return false;
	}

	switch ($next_notice_div) {

	case "1":  // 非同期の場合

		// 同階層において初めての承認ならメール
		return ($obj->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1") == 1);

	case "2":  // 同期の場合

		// 同階層において全員が「承認」ならメール
		$same_hierarchy_apvcnt = $obj->get_same_hierarchy_apvcnt($apply_id, $apv_order);
		$ok_approvecnt = $obj->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1");
		return ($same_hierarchy_apvcnt == $ok_approvecnt);

	case "3":  // 権限並列の場合

		// 無条件でメール（同階層において初めての承認のはずなので）
		return true;
	}
}

function fplus_register_library($con, $approve, $obj, $apply_id, $fname) {
	$files = array();

	// 「承認」でない場合は文書登録不要
	if ($approve != "1") {
		return $files;
	}

	// 承認確定でない場合は文書登録不要
	if ($obj->get_apply_stat($apply_id) != "1") {
		return $files;
	}

	// 添付ファイルがない場合は文書登録不要
	$apply_files = $obj->get_applyfile($apply_id);
	if (count($apply_files) == 0) {
		return $files;
	}

	// ワークフロー情報を取得
	$wkfw_id = fplus_get_wkfw_id_by_apply_id($con, $apply_id, $fname);
	$wkfwmsts = $obj->get_wkfwmst($wkfw_id);
	$wkfwmst = $wkfwmsts[0];

	// 文書登録と連動しない場合は文書登録不要
	if ($wkfwmst["lib_reg_flg"] != "t") {
		return $files;
	}

	// 申請者の職員IDを取得
	$apply = fplus_get_apply_by_apply_id($con, $apply_id, $fname);
	$apply_emp_id = $apply["emp_id"];

	foreach ($apply_files as $tmp_apply_file) {
		$tmp_file_no = $tmp_apply_file["applyfile_no"];
		$tmp_file_name = $tmp_apply_file["applyfile_name"];

		// 拡張子の設定
		$tmp_lib_extension = (strpos($tmp_file_name, ".")) ? preg_replace("/^.*\.([^.]*)$/", "$1", $tmp_file_name) : "txt";

		// 文書タイプの決定
		switch ($tmp_lib_extension) {
		case "doc":
		case "docx":
			$tmp_lib_type = "1";
			break;
		case "xls":
		case "xlsx":
			$tmp_lib_type = "2";
			break;
		case "ppt":
		case "pptx":
			$tmp_lib_type = "3";
			break;
		case "pdf":
			$tmp_lib_type = "4";
			break;
		case "txt":
			$tmp_lib_type = "5";
			break;
		case "jpg":
		case "jpeg":
			$tmp_lib_type = "6";
			break;
		case "gif":
			$tmp_lib_type = "7";
			break;
		default:
			$tmp_lib_type = "99";
			break;
		}

		// 文書名の設定
		$tmp_lib_nm = preg_replace("/^(.*)\.($tmp_lib_extension)$/", "$1", $tmp_file_name);

/*
		// 文書IDの採番
		$sql = "select max(lib_id) from libinfo";
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$tmp_lib_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

*/
		
		//シーケンスで自動採番に変更
		$sql = "select nextval('library_lib_id_seq') as lib_id";
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$tmp_lib_id = intval(pg_fetch_result($sel, 0, 0));
		
		

		// 文書情報を登録
		$sql = "insert into libinfo (lib_archive, lib_id, lib_cate_id, lib_extension, lib_keyword, lib_summary, emp_id, lib_delete_flag, show_login_flg, show_login_begin, show_login_end, folder_id, lib_type, private_flg, ref_dept_st_flg, ref_dept_flg, ref_st_flg, upd_dept_st_flg, upd_dept_flg, upd_st_flg, lib_nm, lib_up_date, lib_no) values (";
		$content = array($wkfwmst["lib_archive"], $tmp_lib_id, $wkfwmst["lib_cate_id"], $tmp_lib_extension, $wkfwmst["lib_keyword"], $wkfwmst["lib_summary"], $apply_emp_id, "f", $wkfwmst["lib_show_login_flg"], $wkfwmst["lib_show_login_begin"], $wkfwmst["lib_show_login_end"], $wkfwmst["lib_folder_id"], $tmp_lib_type, $wkfwmst["lib_private_flg"], $wkfwmst["lib_ref_dept_st_flg"], $wkfwmst["lib_ref_dept_flg"], $wkfwmst["lib_ref_st_flg"], $wkfwmst["lib_upd_dept_st_flg"], $wkfwmst["lib_upd_dept_flg"], $wkfwmst["lib_upd_st_flg"], $tmp_lib_nm, date("YmdHis"), $wkfwmst["lib_no"]);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 文書参照ログを削除（念のため）
		$sql = "delete from libreflog";
		$cond = "where lib_id = $tmp_lib_id";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 版情報を登録
		$sql = "insert into libedition values (";
		$content = array($tmp_lib_id, 1, $tmp_lib_id);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 文書参照可能部署情報を登録
		$sql = "insert into librefdept select cast($tmp_lib_id as int), class_id, atrb_id, dept_id from fpluswkfwlibrefdept where wkfw_mode = 2 and wkfw_id = $wkfw_id";
		if (!pg_query($con, $sql)) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 文書参照可能役職情報を登録
		$sql = "insert into librefst select cast($tmp_lib_id as int), st_id from fpluswkfwlibrefst where wkfw_mode = 2 and wkfw_id = $wkfw_id";
		if (!pg_query($con, $sql)) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 文書参照可能職員情報を登録
		$sql = "insert into librefemp select cast($tmp_lib_id as int), emp_id from fpluswkfwlibrefemp where wkfw_mode = 2 and wkfw_id = $wkfw_id";
		if (!pg_query($con, $sql)) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 文書更新可能部署情報を登録
		$sql = "insert into libupddept select cast($tmp_lib_id as int), class_id, atrb_id, dept_id from fpluswkfwlibupddept where wkfw_mode = 2 and wkfw_id = $wkfw_id";
		if (!pg_query($con, $sql)) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 文書更新可能役職情報を登録
		$sql = "insert into libupdst select cast($tmp_lib_id as int), st_id from fpluswkfwlibupdst where wkfw_mode = 2 and wkfw_id = $wkfw_id";
		if (!pg_query($con, $sql)) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 文書更新可能職員情報を登録
		$sql = "insert into libupdemp select cast($tmp_lib_id as int), emp_id from fpluswkfwlibupdemp where wkfw_mode = 2 and wkfw_id = $wkfw_id";
		if (!pg_query($con, $sql)) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// ファイル情報を戻り配列に追加
		$files[] = array(
			"apply_id" => $apply_id,
			"apply_file_no" => $tmp_file_no,
			"extension" => $tmp_lib_extension,
			"lib_archive" => $wkfwmst["lib_archive"],
			"lib_cate_id" => $wkfwmst["lib_cate_id"],
			"lib_id" => $tmp_lib_id
		);
	}

	return $files;
}

function fplus_copy_apply_file_to_library($file) {

	// ディレクトリ名を決定
	switch ($file["lib_archive"]) {
	case "1":
		$dir_name = "private";
		break;
	case "2":
		$dir_name = "all";
		break;
	case "3":
		$dir_name = "section";
		break;
	case "4":
		$dir_name = "project";
		break;
	}

	// 添付ファイル保存用ディレクトリがなければ作成
	if (!is_dir("docArchive")) {
		mkdir("docArchive", 0755);
	}
	if (!is_dir("docArchive/{$dir_name}")) {
		mkdir("docArchive/{$dir_name}", 0755);
	}
	if (!is_dir("docArchive/{$dir_name}/cate{$file["lib_cate_id"]}")) {
		mkdir("docArchive/{$dir_name}/cate{$file["lib_cate_id"]}", 0755);
	}

	// 文書管理フォルダへコピー
	$apply_file = "fplus/apply/{$file["apply_id"]}_{$file["apply_file_no"]}";
	if (!file_exists($apply_file)) {
		$apply_file .= ".{$file["extension"]}";
	}

	copy($apply_file, "docArchive/{$dir_name}/cate{$file["lib_cate_id"]}/document{$file["lib_id"]}.{$file["extension"]}");
}
?>
