<?
require_once("Cmx.php");
require_once("Cmx/Model/SystemConfig.php");
require_once("aclg_set.php");
require_once("about_comedix.php");
require_once("show_news.ini");
require_once("referer_common.ini");
require_once("news_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$check_auth = check_authority($session, 24, $fname);
if ($check_auth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// ページ番号の設定
if ($page == "") {
    $page = 1;
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の設定
if ($news != "") {
    set_referer($con, $session, "news", $news, $fname);
    $referer = $news;
}
else {
    $referer = get_referer($con, $session, "news", $fname);
}
// 一覧表示の件数を取得
$rows_per_page = get_rows_per_page($con, $fname);

// お知らせ区分リストを取得
$notice2_list = get_notice2_list($con, $fname);

// お知らせ返信機能設定を取得
$conf = new Cmx_SystemConfig();
$setting_henshin = $conf->get('setting_henshin');

// お知らせ集計機能設定を取得
$setting_aggregate = $conf->get('setting_aggregate');

//オプションから引用した設定が"t"ではなかった場合に、"f"を設定する。
if ($setting_henshin != "t") {
    $setting_henshin = "f";
}
if ($setting_aggregate != "t") {
    $setting_aggregate = "f";
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix お知らせ管理 | ゴミ箱</title>
<? include_js("js/fontsize.js"); ?>
<? include_js("js/jquery/jquery-1.9.1.min.js"); ?>
<script type="text/javascript">
function delNews() {
	if (confirm('削除してよろしいですか？')) {
		document.delform.mode.value = 'del';
		document.delform.submit();
	}
}
function undelNews() {
	document.delform.mode.value = 'undel';
	document.delform.submit();
}
$(function() {
    $('#check').click(function() {
        if($(this).get(0).checked) {
            $('input.delete').prop('checked', true);
        }
        else {
            $('input.delete').prop('checked', false);
        }
    });
});
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="news_menu.php?session=<? echo($session); ?>"><img src="img/icon/b21.gif" width="32" height="32" border="0" alt="お知らせ・回覧板管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="news_menu.php?session=<? echo($session); ?>"><b>お知らせ・回覧板管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="newsuser_menu.php?session=<? echo($session); ?>"><img src="img/icon/b35.gif" width="32" height="32" border="0" alt="お知らせ・回覧板"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="newsuser_menu.php?session=<? echo($session); ?>"><b>お知らせ・回覧板</b></a> &gt; <a href="news_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="newsuser_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>
<form name="delform" action="news_trash_delete.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_search.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice2_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ区分<br>一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice2_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ区分<br>登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_cate_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_cate_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#5279a5"><a href="news_trash.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>ゴミ箱</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="news_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td width="5">&nbsp;</td>
<?php //オプション機能で集計表に使用するにチェックを入れることで、タブに集計表を表示させる ?>
		<?php if($setting_aggregate == 't'){?>
		<td width="70" align="center" bgcolor="#bdd1e7">
			<a href="news_aggregate.php?session=<? echo($session); ?>">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">集計表</font>
			</a>
		</td>
		<?php }?>
    <td align="right">
        <input type="button" value="元に戻す" onclick="undelNews();">
        <input type="button" value="完全削除" onclick="delNews();">
    </td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10">

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td width="25" align="center"><input type="checkbox" id="check"></td>
<td width="70" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録日</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象カテゴリ</font></td>
<td width="65" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分</font></td>
<? if (!empty($notice2_list)) {?>
<td width="65" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ<br>区分</font></td>
<? } ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発信者</font></td>
<td width="175" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">掲載期間<br />（掲載期間：ログイン画面）</font></td>
<td width="70" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照者一覧</font></td>
<?php if($setting_henshin == 't'){?>
	<td width="55" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">返信一覧</font></td>
<? } ?>
</tr>
<? show_news($con, $page, $session, $fname, true, $rows_per_page, $notice2_list, $setting_henshin); ?>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
<input type="hidden" name="mode" value="">
</form>
<? show_news_page_list($con, $page, $session, $fname, true, $rows_per_page); ?>
</body>
<? pg_close($con); ?>
</html>
