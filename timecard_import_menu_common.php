<?php
/**
 * インポートメニュータブ表示
 *
 * @param mixed $session セッションID
 * @param mixed $fname プログラムファイル名
 * @return mixed なし
 *
 */
function show_timecard_import_menuitem($session,$fname)
{
    define("IMPORT"                     ,"勤務シフト");
    define("IMPORT_KAI"                 ,"勤務シフト（快決シフト）");
    define("IMPORT_RECORDER1"           ,"タイムレコーダ（マックス）");
    define("IMPORT_RECORDER2"           ,"タイムレコーダ（アマノ）");
    define("IMPORT_RECORDER3"           ,"タイムレコーダ（smartbase）");
    define("IMPORT_RECORDER4"           ,"タイムレコーダ（セコム）");
    define("IMPORT_PAID_HOL"            ,"有給休暇残日数移行");
    define("IMPORT_PAID_HOL_HOUR"       ,"時間有休データ移行");
    define("IMPORT_AMANO"               ,"勤務実績（アマノ）");
    
    //==============================
    //画面ファイル名の変換
    //==============================
    $index = strrpos($fname,"/");
    if($index != false)
    {
        $file = substr($fname,$index+1);
    }
    else
    {
        $file = $fname;
    }
    //==============================
    //選択タブの決定
    //==============================
    switch($file)
    {
        case "timecard_import.php":
            $selected_tab = IMPORT;
            break;
        case "timecard_import_kai.php":
            $selected_tab = IMPORT_KAI;
            break;
        case "timecard_import_recorder.php":
            $selected_tab = IMPORT_RECORDER1;
            break;
        case "timecard_import_recorder2.php":
            $selected_tab = IMPORT_RECORDER2;
            break;
        case "timecard_import_recorder3.php":
            $selected_tab = IMPORT_RECORDER3;
            break;
        case "timecard_import_recorder4.php":
            $selected_tab = IMPORT_RECORDER4;
            break;
        case "timecard_import_paid_hol.php":
            $selected_tab = IMPORT_PAID_HOL;
            break;
        case "timecard_import_paid_hol_hour.php":
            $selected_tab = IMPORT_PAID_HOL_HOUR;
            break;
        case "timecard_import_amano.php":
            $selected_tab = IMPORT_AMANO;
            break;
        default:
            $selected_tab = IMPORT;
            break;
    }
    //==============================
    //HTML出力
    //==============================
    echo("&nbsp;&nbsp;");
    echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
    // 勤務シフト
    $url = "timecard_import.php?session=$session";
    show_timecard_import_tab(IMPORT, $url, $selected_tab == IMPORT);
    // 勤務シフト（快決シフト）
    $url = "timecard_import_kai.php?session=$session";
    show_timecard_import_tab(IMPORT_KAI, $url, $selected_tab == IMPORT_KAI);
    // タイムレコーダ（マックス）
    $url = "timecard_import_recorder.php?session=$session";
    show_timecard_import_tab(IMPORT_RECORDER1, $url, $selected_tab == IMPORT_RECORDER1);
    // タイムレコーダ（アマノ）
    $url = "timecard_import_recorder2.php?session=$session";
    show_timecard_import_tab(IMPORT_RECORDER2, $url, $selected_tab == IMPORT_RECORDER2);
    // タイムレコーダ（smartbase）
    $url = "timecard_import_recorder3.php?session=$session";
    show_timecard_import_tab(IMPORT_RECORDER3, $url, $selected_tab == IMPORT_RECORDER3);
    // タイムレコーダ（セコム）
    $url = "timecard_import_recorder4.php?session=$session";
    show_timecard_import_tab(IMPORT_RECORDER4, $url, $selected_tab == IMPORT_RECORDER4);
    echo("<br>\n");
    echo("&nbsp;&nbsp;");
    // 有給休暇残日数移行
    $url = "timecard_import_paid_hol.php?session=$session";
    show_timecard_import_tab(IMPORT_PAID_HOL, $url, $selected_tab == IMPORT_PAID_HOL);
    // 時間有休データ移行
    $url = "timecard_import_paid_hol_hour.php?session=$session";
    show_timecard_import_tab(IMPORT_PAID_HOL_HOUR, $url, $selected_tab == IMPORT_PAID_HOL_HOUR);
    // 勤務実績（アマノ）
    $url = "timecard_import_amano.php?session=$session";
    show_timecard_import_tab(IMPORT_AMANO, $url, $selected_tab == IMPORT_AMANO);
    
    echo("</font>");
}


/**
 * タブ表示
 *
 * @param mixed $text メニュー名
 * @param mixed $url URL
 * @param mixed $is_selected 選択状態 true:選択
 * @return mixed なし
 *
 */
function show_timecard_import_tab($text,$url,$is_selected)
{
	if($is_selected)
	{
		?>
        <b><?=$text?></b>&nbsp;&nbsp;
		<?
	}
	else
	{
		?>
        <a href="<?=$url?>"><?=$text?></a>&nbsp;&nbsp;
        <?
    }
}
?>
