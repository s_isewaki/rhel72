<?
/*
看護必要度・月間評価表

mode=print で 印刷
mode=excel で Excel出力

※注意
患者IDを見て評価表を作成しているので、同じ月に退院→入院した患者も、同じ評価表になっています。（評価表を入院毎に別々にしていない）
*/
ob_start();
require_once("about_comedix.php");
require_once("kango_common.ini");
require_once("holiday.php");
ob_end_clean();

//==============================
//前処理
//==============================

// ページ名
$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($_REQUEST["session"], $fname);
if ($session == "0")
{
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$checkauth = check_authority($session, $AUTH_NO_KANGO_USER, $fname);
if($checkauth == "0")
{
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// パラメータ
$pt_id = isset($_REQUEST["pt_id"]) ? $_REQUEST["pt_id"] : null;
$cal_y = isset($_REQUEST["cal_y"]) ? $_REQUEST["cal_y"] : null;
$cal_m = isset($_REQUEST["cal_m"]) ? $_REQUEST["cal_m"] : null;
$mode = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : null;

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
//ユーザーID
//==============================
$emp_id = get_emp_id($con,$session,$fname);

//==============================
//現在の患者の入棟情報
//==============================
$pt_all_inpt_info =  get_all_inpt_info($con,$fname,"","",$pt_id);

//==============================
//対象:患者情報の取得
//==============================
$sql = "select * from ptifmst where ptif_id = '{$pt_id}'";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0)
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if(pg_num_rows($sel) == 0)
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

//氏名
$pt_name = pg_fetch_result($sel,0,'ptif_lt_kaj_nm').' '.pg_fetch_result($sel,0,'ptif_ft_kaj_nm');

//性別
$pt_sex = get_sex_name(pg_fetch_result($sel,0,'ptif_sex'));


//==============================
//対象:場所情報の取得(入院中判定/入院患者氏名)
//==============================

//初期アクセス時は「場所」条件を取得しておく。
$bldg_cd = $pt_all_inpt_info[0]['bldg_cd'];
$ward_cd = $pt_all_inpt_info[0]['ward_cd'];
$ptrm_room_no = $pt_all_inpt_info[0]['ptrm_room_no'];
$inpt_bed_no = $pt_all_inpt_info[0]['inpt_bed_no'];

//初期アクセスの場合は必ず患者は存在する。
$is_target_inpt = true;

//==============================
//病室名の取得
//==============================

//病室名
$sql  = " select ward_name,ptrm_name from";
$sql .= " (";
$sql .= get_bldg_ward_room_sql();
$sql .= " ) bldg_ward_room_sql";
$sql .= " where bldg_cd = {$bldg_cd}";
$sql .= " and ward_cd = {$ward_cd}";
$sql .= " and ptrm_room_no = {$ptrm_room_no}";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0)
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if(pg_num_rows($sel) == 0)
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$ward_name = pg_fetch_result($sel,0,'ward_name');
$ptrm_name = pg_fetch_result($sel,0,'ptrm_name');


//==============================
//カレンダー情報
//==============================

//初期アクセス時　※他画面から指定されて呼び出されるケースあり。
if($cal_y == "")
{
	$cal_y = date("Y");
}
if($cal_m == "")
{
	$cal_m = date("m");
}

//先月翌月
$tmp_date = mktime(0, 0, 0, $cal_m, -1, $cal_y);
$cal_prev_y = date("Y", $tmp_date);
$cal_prev_m = date("m", $tmp_date);
$tmp_date = mktime(0, 0, 0, $cal_m, 32, $cal_y);
$cal_next_y = date("Y", $tmp_date);
$cal_next_m = date("m", $tmp_date);

//2008/04/01以前は評価票の定義がないため、アクセス禁止
if($cal_prev_y <= "2008" && $cal_prev_m < "04")
{
	$cal_prev_y = $cal_y;
	$cal_prev_m = $cal_m;
}


//==============================
//評価票情報を取得
//==============================
$form_list = get_eval_form_list($con,$fname);
$item_list = get_eval_item_list($con,$fname);
$point_list = get_eval_point_list($con,$fname,$pt_id,$cal_y,$cal_m);
$point_count = array();

//==============================
//その他
//==============================

//画面名
$PAGE_TITLE = "看護観察記録";

//========================================================================================================================================================================================================
// Excel出力
//========================================================================================================================================================================================================
if ($mode === 'excel') {
	header("Content-Type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=kango_eval_report_{$pt_id}_{$cal_y}{$cal_m}.xls");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
	header("Pragma: public");
}

//========================================================================================================================================================================================================
// HTML出力
//========================================================================================================================================================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$KANGO_TITLE?> | <?=$PAGE_TITLE?></title>

<?if ($mode !== 'excel') { ?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript">

//カレンダー先月
function go_cal_prev()
{
	location.href = 'kango_eval_report.php?session=<?=$session?>&pt_id=<?=$pt_id?>&cal_y=<?=$cal_prev_y?>&cal_m=<?=$cal_prev_m?>';
}
//カレンダー翌月
function go_cal_next()
{
	location.href = 'kango_eval_report.php?session=<?=$session?>&pt_id=<?=$pt_id?>&cal_y=<?=$cal_next_y?>&cal_m=<?=$cal_next_m?>';
}
//印刷
function print_out()
{
	window.open('kango_eval_report.php?session=<?=$session?>&pt_id=<?=$pt_id?>&cal_y=<?=$cal_y?>&cal_m=<?=$cal_m?>&mode=print', 'print', 'width=700,height=600,scrollbars=yes');
}
//Excel出力
function excel_download()
{
	location.href = 'kango_eval_report.php?session=<?=$session?>&pt_id=<?=$pt_id?>&cal_y=<?=$cal_y?>&cal_m=<?=$cal_m?>&mode=excel';
}


</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<?}?>

<style type="text/css">
.list {border-collapse:collapse; margin-bottom:5px;}
.list td {border:#5279a5 solid 1px;}
.list_in {border-collapse:collapse; border:#5279a5 solid 1px;}
.list_in td {border:#FFFFFF solid 0px;}
table.report {border-collapse:collapse; margin:15px 0 0 0; border:#000 solid 1px;}
table.report thead tr {font-weight:bold; background-color:silver;}
table.report tfoot tr {font-weight:bold; background-color:#dcdcdc;}
table.report td {font-size:80%; width:25px; text-align:right; border:#000 solid 1px;}
table.report td.first {width:250px; text-align:left;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
<? if ($mode === 'print') { echo "onload=\"setTimeout('self.print();self.close();', 100);\"";} // 印刷ダイアログ ?>
>
<!-- ヘッダー START -->
<?
// 印刷、Excelではヘッダは表示させない
if ($mode === null) { 
	$show_kango_header_option = null;
	$show_kango_header_option['input_date'] = "";
	$show_kango_header_option['input_time'] = "";
	$show_kango_header_option['bldg_cd'] = $bldg_cd;
	$show_kango_header_option['ward_cd'] = $ward_cd;
	$show_kango_header_option['ptrm_room_no'] = $ptrm_room_no;
	$show_kango_header_option['inpt_bed_no'] = $inpt_bed_no;
	$show_kango_header_option['pt_id'] = $pt_id;
	$show_kango_header_option['cal_y'] = $cal_y;
	$show_kango_header_option['cal_m'] = $cal_m;
	show_kango_header($session,$fname,false,$show_kango_header_option);
}
?>
<!-- ヘッダー END -->

<!-- 全体 START -->
<div style="margin:2px;">

<!-- 機能タイトル START -->
<div style="margin:5px 0;">
<b><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">看護必要度・月間評価表</font></b>
</div>
<!-- 機能タイトル END -->

<!-- 患者情報表示領域 START -->
<!-- 患者情報 START -->
<?if ($mode !== 'excel') { ?>
<table width='100%' border='0' cellspacing='0' cellpadding='1' class="list">
<tr>
<td bgcolor='#bdd1e7' width="45" align="right" nowrap><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>患者ID</font></td>
<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$pt_id?></font></td>
<td bgcolor='#bdd1e7' width="60" align="right" nowrap><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>患者氏名</font></td>
<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=h($pt_name)?></font></td>
<td bgcolor='#bdd1e7' width="35" align="right" nowrap><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>性別</font></td>
<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$pt_sex?></font></td>
<td bgcolor='#bdd1e7' width="35" align="right" nowrap><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>病棟</font></td>
<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=h($ward_name)?></font></td>
<td bgcolor='#bdd1e7' width="35" align="right" nowrap><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>部屋</font></td>
<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=h($ptrm_name)?></font></td>
<td bgcolor='#bdd1e7' width="65" align="right" nowrap><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>ベッド番号</font></td>
<td width="20"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$inpt_bed_no?></font></td>
</tr>
</table>
<?} else {?>
<div style="margin:5px 0;">
　　<b>患者ID</b>: <?=$pt_id?>
　　<b>患者氏名</b>: <?=h($pt_name)?>
　　<b>性別</b>: <?=$pt_sex?>
　　<b>病棟</b>: <?=h($ward_name)?>
　　<b>部屋</b>: <?=h($ptrm_name)?>
　　<b>ベッド番号</b>: <?=$inpt_bed_no?>
</div>
<br>
<?}?>
<!-- 患者情報 END -->
<!-- 患者情報表示領域 END -->

<!-- 看護記録履歴表示領域 START -->
<?if ($mode === null) { ?>

<!-- 看護記録履歴_月切り替え START -->
<table width='100%' border='0' cellspacing='0' cellpadding='1' class="list_in">
<tr bgcolor='#bdd1e7'>
<td width="120" align="center">
	<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
	<a href="javascript:go_cal_prev()">＜＜</a>
	<?=$cal_y."/".$cal_m?>
	<a href="javascript:go_cal_next()">＞＞</a>
	</font>
	</td>
<td width="30">&nbsp;</td>
<td align="right" nowrap><?if ($point_list) {?>
<button type="button" onclick="print_out();">印刷</button> <button type="button" onclick="excel_download();">Excel出力</button>
<?}?></td>
</tr>
</table>
<!-- 看護記録履歴_月切り替え END -->

<?} else {?>
<div style="background-color:#bdd1e7;border:#5279a5 solid 1px;padding:3px;">
<?=$cal_y."年".$cal_m."月"?>
</div>
<?}?>
<!-- 看護記録履歴表示領域 END -->

<!-- 看護観察記録・月間評価表 START -->
<?
if (!$point_list) {
	echo '<p style="margin:10px;">当月の評価データが存在しません。</p>';
}
else {
foreach($form_list as $form => $type) {
	if (!isset($point_list[$form])) {
		continue;
	}
?>
<table class="report" border="1">
<thead>
<tr>
	<td class="first"><?=$type?>: <?=get_eval_form_type_name($type)?></td>
	<? for( $day=1; checkdate($cal_m,$day,$cal_y); $day++ ) {?>
	<td><?=$day?></td>
	<?}?>
</tr>
</thead>

<tbody>
<? foreach($item_list[$form] as $item => $item_name) {?>
<tr>
	<td class="first"><?=preg_replace("/\s+/","<br>",$item_name)?></td>
	<? for( $day=1; checkdate($cal_m,$day,$cal_y); $day++ ) {?>
	<td>
		<?
		if (isset($point_list[$form][$item][$day])) {
			echo $point_list[$form][$item][$day];
			@$point_count[$form][$day] += $point_list[$form][$item][$day];
		}
		else {
			echo "";
		}
		?>
	</td>
	<?}?>
</tr>
<?}?>
</tbody>

<tfoot>
<tr>
	<td class="first">合計点数</td>
	<? for( $day=1; checkdate($cal_m,$day,$cal_y); $day++ ) {?>
	<td><?echo isset($point_count[$form][$day]) ? $point_count[$form][$day] : ""?></td>
	<?}?>
</tr>
</tfoot>

</table><br>
<?
}
}
?>
<!-- 看護観察記録・月間評価表 END -->

</div>
<!-- 全体 END -->

</body>
</html>
<?
pg_close($con);

//========================================================================================================================================================================================================
// Private関数
//========================================================================================================================================================================================================
/**
 * 評価表一覧リストを取得します。
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @return array 評価表一覧リスト
 */
function get_eval_form_list($con,$fname)
{
	$sql = <<<_SQL_END_
SELECT * FROM nlcs_eval_form
WHERE delete_flg='f'
ORDER BY eval_form_type,eval_form_id
_SQL_END_;
	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0)
	{
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	$list = array();
	while ($row = pg_fetch_assoc($sel)) {
		$list[$row['eval_form_id']] = $row['eval_form_type'];
	}
	return $list;
}

/**
 * 評価表アイテムリストを取得します。
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @return array 評価表アイテムリスト
 */
function get_eval_item_list($con,$fname)
{
	$sql = <<<_SQL_END_
SELECT * FROM nlcs_eval_item
WHERE delete_flg='f'
ORDER BY disp_index
_SQL_END_;
	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0)
	{
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	$list = array();
	while ($row = pg_fetch_assoc($sel)) {
		$list[$row['eval_form_id']][$row['eval_item_id']] = $row['eval_item_name'];
	}
	return $list;
}

/**
 * 患者の評価点数を取得します。
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param string $pt_id 患者ID
 * @param string $cal_y 取得対象年
 * @param string $cal_m 取得対象月
 * @return array 評価点数リスト
 */
function get_eval_point_list($con,$fname,$pt_id,$cal_y,$cal_m)
{
	$sql = <<<_SQL_END_
SELECT
eval_form_id,eval_item_id,eval_point1,input_date
FROM nlcs_eval_record
JOIN nlcs_eval_record_data USING (eval_data_id)
JOIN nlcs_eval_cate USING (eval_form_id,eval_item_id,eval_cate_no)
WHERE delete_flg='f'
AND input_date BETWEEN '{$cal_y}{$cal_m}01' AND '{$cal_y}{$cal_m}31'
AND input_time=2
AND pt_id='{$pt_id}'
_SQL_END_;
	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0)
	{
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	$list = array();
	while ($row = pg_fetch_assoc($sel)) {
		$list[$row['eval_form_id']][$row['eval_item_id']][sprintf("%d",substr($row['input_date'],-2))] = $row['eval_point1'];
	}
	return $list;
}
