<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションをチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限をチェック
$checkauth = check_authority($session, 21, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

for ($i = 1; $i <= 46; $i++) {
	$varname = "required$i";
	if ($$varname != "t") {
		$$varname = "f";
	}

	if ($$varname == "t") {
		$varname = "display$i";
		$$varname = "t";
	}

	$varname = "display$i";
	if ($$varname != "t") {
		$$varname = "f";
	}
}

// 前医が「必須」の場合、診療科（紹介元）も「必須」
if ($required31 == "t") {
	$required30 = "t";
	$display30 = "t";
}

// 診療科（紹介元）が「必須」の場合、紹介元医療機関も「必須」
if ($required30 == "t") {
	$required29 = "t";
	$display29 = "t";
}

// 前医が「表示」の場合、診療科（紹介元）も「表示」
if ($display31 == "t") {
	$display30 = "t";
}

// 診療科（紹介元）が「表示」の場合、紹介元医療機関も「表示」
if ($display30 == "t") {
	$display29 = "t";
}

$set = array();
$setvalue = array();
for ($i = 1; $i <= 46; $i++) {
	$varname = "required$i";
	$set[] = $varname;
	$setvalue[] = $$varname;

	$varname = "display$i";
	$set[] = $varname;
	$setvalue[] = $$varname;
}

// データベースに接続
$con = connect2db($fname);

// 表示設定を更新
$sql = "update bedcheck set";
$cond = "";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// 元画面に戻る
echo "<script type=\"text/javascript\">location.href = 'bed_check_setting_in.php?session=$session';</script>";
