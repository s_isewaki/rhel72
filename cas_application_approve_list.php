<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_select_values.ini");
require_once("cas_approve_list.ini");
require_once("cas_yui_calendar_util.ini");
require_once("cas_application_common.ini");
require_once("cas_common.ini");
require_once("cas_application_workflow_common_class.php");
require_once("get_cas_title_name.ini");
require_once("cas_application_workflow_select_box.ini");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, $CAS_MANAGE_AUTH, $fname);

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
// 初期化処理
//====================================
$arr_wkfwcatemst = array();
$sel_wkfwcate = search_wkfwcatemst($con, $fname);
while($row_cate = pg_fetch_array($sel_wkfwcate)) {
	$tmp_wkfw_type = $row_cate["wkfw_type"];
	$tmp_wkfw_nm = $row_cate["wkfw_nm"];

	$arr_wkfwmst_tmp = array();
	$sel_wkfwmst = search_wkfwmst($con, $fname, $tmp_wkfw_type);
	while($row_wkfwmst = pg_fetch_array($sel_wkfwmst)) {
		$tmp_wkfw_id = $row_wkfwmst["wkfw_id"];
		$wkfw_title = $row_wkfwmst["wkfw_title"];

		$arr_wkfwmst_tmp[$tmp_wkfw_id] = $wkfw_title;
	}

	$arr_wkfwcatemst[$tmp_wkfw_type] = array("name" => $tmp_wkfw_nm, "wkfw" => $arr_wkfwmst_tmp);
}
// 「CoMedix」というカテゴリ追加
/*
$arr_comedix_tmp["1"] = "議事録公開申請（委員会・WG）";
$arr_comedix_tmp["2"] = "残業申請";
$arr_comedix_tmp["3"] = "勤務時間修正申請";
$arr_comedix_tmp["4"] = "退勤後復帰申請";
$arr_wkfwcatemst["0"] = array("name" => "CoMedix", "wkfw" => $arr_comedix_tmp);
*/

// 申請日セット(本日日付から過去２ヶ月分)
// 申請タブをクリックした場合のみ。

if($apply_date_defalut == "on")
{
	$today = date("Y/m/d", strtotime("today"));

	$arr_today = split("/", $today);
	$date_y2 = $arr_today[0];
	$date_m2 = $arr_today[1];
	$date_d2 = $arr_today[2];

	// ２ヶ月前の日付取得
	$two_months_ago = date("Y/m/d",strtotime("-2 months" ,strtotime($today)));

	$arr_two_months_ago = split("/", $two_months_ago);
	$date_y1 = $arr_two_months_ago[0];
	$date_m1 = $arr_two_months_ago[1];
	$date_d1 = $arr_two_months_ago[2];
}

$obj = new cas_application_workflow_common_class($con, $fname);

$cas_title = get_cas_title_name();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cas_title?> | 受付一覧</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<?

// 外部ファイルを読み込む
write_yui_calendar_use_file_read_0_12_2();

// カレンダー作成、関数出力
write_yui_calendar_script2(4);
?>
<script type="text/javascript">
<!--

function reload_page() {
	document.approve.action="cas_application_approve_list.php?session=<?=$session?>&mode=search";
	document.approve.submit();
}

function initPage() {

	cateOnChange();

}

function cateOnChange() {

	var obj_cate = document.approve.category;
	var obj_wkfw = document.approve.workflow;

	var cate_id = getSelectedValue(obj_cate);
	// 申請書セレクトボックスのオプションを全削除
	deleteAllOptions(obj_wkfw);

	// 申請書セレクトボックスのオプションを作成
	addOption(obj_wkfw, '-', 'すべて');

<? foreach ($arr_wkfwcatemst as $tmp_cate_id => $arr) { ?>
	if (cate_id == '<? echo $tmp_cate_id; ?>') {
	<? foreach($arr["wkfw"] as $tmp_wkfw_id => $tmp_wkfw_nm)
	   {
           $tmp_wkfw_nm = htmlspecialchars($tmp_wkfw_nm, ENT_QUOTES);
           $tmp_wkfw_nm = str_replace("&#039;", "\'", $tmp_wkfw_nm);
           $tmp_wkfw_nm = str_replace("&quot;", "\"", $tmp_wkfw_nm);
	?>
		addOption(obj_wkfw, '<?=$tmp_wkfw_id?>', '<?=$tmp_wkfw_nm?>', '<?=$workflow?>');
	<? } ?>
	}
<? } ?>
}


function getSelectedValue(sel) {
	return sel.options[sel.selectedIndex].value;
}


function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {

	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}


function approve_search() {

	document.approve.action="cas_application_approve_list.php?session=<?=$session?>&mode=search";
	document.approve.submit();

}

function approve_regist(idx) {

	var msg = "";

	if(idx == '1') {
		msg = '「承認」';
	} else if(idx == '2') {
		msg = '「否認」';
	} else if(idx == '3') {
		msg = '「差戻し」';
	}

	if (document.approve.elements['approve_chk[]'] == undefined) {
		alert(msg + 'するデータを選択してくだい。');
		return;
	}

	if (document.approve.elements['approve_chk[]'].length == undefined) {
		if (!document.approve.elements['approve_chk[]'].checked) {
			alert(msg + 'するデータを選択してくだい。');
			return;
		}
	} else {
		var checked = false;
		for (var i = 0, j = document.approve.elements['approve_chk[]'].length; i < j; i++) {
			if (document.approve.elements['approve_chk[]'][i].checked) {
				checked = true;
				break;
			}
		}
		if (!checked) {
			alert(msg + 'するデータを選択してくだい。');
			return;
		}
	}

	if (confirm('選択された申請書の' + msg + 'をします。よろしいですか？')) {
		document.approve.action="cas_application_approve_list_regist.php?session=<?=$session?>&approve=" + idx;
		document.approve.submit();
	}
}

function highlightCells(class_name) {
	changeCellsColor(class_name, '#ffff66');
}

function dehighlightCells(class_name) {
	changeCellsColor(class_name, '');
}

function changeCellsColor(class_name, color) {
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++) {
		if (cells[i].className != class_name) {
			continue;
		}
		cells[i].style.backgroundColor = color;
	}
}

function show_sub_window(url) {
//	var h = window.screen.availHeight;
//	var w = window.screen.availWidth;
	var h = '700';
	var w = '1024';
	var option = "directories=no,location=no,menubar=no,resizable=no,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
	window.open(url, 'approvewin',option);
}

function clearApplyDate() {
	document.approve.date_y1.selectedIndex = 0;
	document.approve.date_m1.selectedIndex = 0;
	document.approve.date_d1.selectedIndex = 0;
	document.approve.date_y2.selectedIndex = 0;
	document.approve.date_m2.selectedIndex = 0;
	document.approve.date_d2.selectedIndex = 0;
}

function setEventHandler() {
	YAHOO.ui.calendar.cal3.selectEvent.subscribe(clearApplyDate);
	YAHOO.ui.calendar.cal4.selectEvent.subscribe(clearApplyDate);
	YAHOO.util.Event.addListener(['date_m3', 'date_d3', 'date_y3', 'date_m4', 'date_d4', 'date_y4'], 'change', clearApplyDate);
}
//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#A0D25A solid 1px;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#A0D25A solid 0px;}

table.block2 {border-collapse:collapse;}
table.block2 td {border:#A0D25A solid 1px;padding:1px;}
table.block2 td {border-width:1;}


</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();initcal();setEventHandler();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="cas_application_menu.php?session=<? echo($session); ?>"><img src="img/icon/s42.gif" width="32" height="32" border="0" alt="<? echo($cas_title); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="cas_application_menu.php?session=<? echo($session); ?>"><b><? echo($cas_title); ?></b></a> &gt; <a href="cas_application_approve_list.php?session=<? echo($session);?>"><b>受付一覧</b></a></font></td>
<? if ($workflow_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="cas_workflow_menu.php?session=<? echo($session); ?>&workflow=1"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>

<?
show_applycation_menuitem($session,$fname,"");
?>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#A0D25A"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<form name="approve" action="#" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">

<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">
<tr>
<td bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォルダ</font></td>
<td><select name="category" onchange="cateOnChange();"><? show_cate_options($arr_wkfwcatemst, $category); ?></select></td>
<td bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請書名</font>
<td><select name="workflow"></select></td>
<td bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">研修テーマ</font>
<td><input type="text" size="25" name="training_theme" value="<?=htmlspecialchars($training_theme)?>"></td>
</tr>
<tr>
<td bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">組織</font></td>
<td colspan="3"><? show_post_select_box_strict($con, $fname, $search_emp_class, $search_emp_attribute, $search_emp_dept, $search_emp_room, $obj, "", true); ?></td>
<td bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者</font></td>
<td><input type="text" size="25" maxlength="30" name="apply_emp_nm" value="<?=htmlspecialchars($apply_emp_nm)?>"></td>
</tr>
<tr>
<td bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請日</font></td>
<td colspan="3">

<table border="0" cellspacing="0" cellpadding="0" class="block_in">
<tr><td>
<select id="date_y1" name="date_y1"><? show_select_years(10, $date_y1, true); ?></select>/<select id="date_m1" name="date_m1"><? show_select_months($date_m1, true); ?></select>/<select id="date_d1" name="date_d1"><? show_select_days($date_d1, true); ?></select>
</td><td>
	<img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/><br>

<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
</font>
</td><td>
&nbsp;〜<select id="date_y2" name="date_y2"><? show_select_years(10, $date_y2, true); ?></select>/<select id="date_m2" name="date_m2"><? show_select_months($date_m2, true); ?></select>/<select id="date_d2" name="date_d2"><? show_select_days($date_d2, true); ?></select>

</td><td>
	<img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal2()"/><br>
<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<div id="cal2Container" style="position:absolute;display:none;z-index:10000;"></div>
</font>
</td></tr>
</table>

</td>
<td bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">承認状況</font></td>
<td>
<select name="apv_stat"><? show_apv_stat_options($apv_stat); ?></select>
</td>
</tr>

<tr>
<td bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">受講期間</font></td>
<td colspan="3">
<table border="0" cellspacing="0" cellpadding="0" class="block_in">
<tr>
<td><select id="date_y3" name="date_y3"><? show_select_years(10, $date_y3, true); ?></select>/<select id="date_m3" name="date_m3"><? show_select_months($date_m3, true); ?></select>/<select id="date_d3" name="date_d3"><? show_select_days($date_d3, true); ?></select></td>
<td><img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal3();"/><br><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><div id="cal3Container" style="position:absolute;display:none;z-index:10000;"></div></font></td>
<td>&nbsp;〜<select id="date_y4" name="date_y4"><? show_select_years(10, $date_y4, true); ?></select>/<select id="date_m4" name="date_m4"><? show_select_months($date_m4, true); ?></select>/<select id="date_d4" name="date_d4"><? show_select_days($date_d4, true); ?></select></td>
<td><img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal4()"/><br><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><div id="cal4Container" style="position:absolute;display:none;z-index:10000;"></div></font></td>
</tr>
</table>

</td>
<td colspan="2">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="block_in">
<tr align="right">
<td><input type="button" value="検索" onclick="approve_search();"></td>
</tr>
</table>
</td>
</tr>

</table>
</td>
</tr>
</table>


<img src="img/spacer.gif" alt="" width="1" height="1">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td align="right">
<input type="button" value="承認" onclick="approve_regist('1');" id="approve_ok">
<input type="button" value="差戻し" onclick="approve_regist('3');" id="approve_back">
<input type="button" value="否認" onclick="approve_regist('2');" id="approve_ng">
</td>
</tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="block2">
<tr bgcolor="#E5F6CD">
<td width="20" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;</font></td>
<td width="100" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請番号<br>(管理ＣＤ-連番)</font></td>
<td width="*" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォルダ</font></td>
<td width="*" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請書名</font></td>
<td width="100" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者</font></td>
<td width="100" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請日</font></td>
<td width="60" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">承認<br>状況</font></td>
</tr>
<?
search_approve_list($con, $session, $fname, $category, $workflow, pg_escape_string($apply_title), pg_escape_string($training_theme), $search_emp_class, $search_emp_attribute, $search_emp_dept, $search_emp_room, pg_escape_string($apply_emp_nm), $date_y1, $date_m1, $date_d1, $date_y2, $date_m2, $date_d2, $date_y3, $date_m3, $date_d3, $date_y4, $date_m4, $date_d4, $apv_stat, $page);
?>


</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
</td>
</tr>
</table>


</td>
</tr>
</table>
</form>


</td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>

</body>
<? pg_close($con); ?>
</html>


<?
// カテゴリ名称を取得
function search_wkfwcatemst($con, $fname) {

	$sql = "select * from cas_wkfwcatemst";
	$cond = "where wkfwcate_del_flg = 'f' order by wkfw_type";
	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}

// ワークフロー情報取得
function search_wkfwmst($con, $fname, $wkfw_type) {

	$sql = "select * from cas_wkfwmst";
	$cond="where wkfw_type='$wkfw_type' and wkfw_del_flg = 'f' order by wkfw_id asc";
	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}

// カテゴリオプションを出力
function show_cate_options($arr_cate, $cate) {
	echo("<option value=\"-\">すべて");
	foreach ($arr_cate as $tmp_cate_id => $arr) {
		$tmp_cate_name = $arr["name"];
		echo("<option value=\"$tmp_cate_id\"");
		if($cate != "" && $cate != '-') {
			if ($cate == $tmp_cate_id) {
				echo(" selected");
			}
		}
		echo(">$tmp_cate_name\n");
	}
}

// 承認状況オプションを出力
function show_apv_stat_options($stat) {

	$arr_apv_stat_nm = array('未承認','承認','否認','差戻し');
	$arr_apv_stat_id = array('0','1','2','3');

	echo("<option value=\"-\">すべて");
	for($i=0;$i<count($arr_apv_stat_nm);$i++) {

		echo("<option value=\"$arr_apv_stat_id[$i]\"");
		if($stat == $arr_apv_stat_id[$i]) {
			echo(" selected");
		}
		echo(">$arr_apv_stat_nm[$i]\n");
	}
}


?>