<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_application_category_list.ini");
require_once("application_template.ini");
require_once("show_select_values.ini");
require_once("yui_calendar_util.ini");
require_once("get_values.ini");
require_once("application_workflow_common_class.php");
require_once("get_values_for_template.ini");
require_once("library_common.php");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,7,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new application_workflow_common_class($con, $fname);
?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 決裁・申請 | 申請印刷</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
// 全テキストエリアの行数変更
function resizeAllTextArea() {
    var objs = document.getElementsByTagName('textarea');
    for (var i = 0, j = objs.length; i < j; i++) {
        var t = objs[i];
        if (t.scrollHeight > t.clientHeight) {
            t.style.height = (t.scrollHeight + 5) + 'px';
        }
    }
}
</script>
<?
if($wkfw_id != "") {

	// ワークフロー情報取得
	$arr_wkfwmst = $obj->get_wkfwmst($wkfw_id);
	$wkfw_content = $arr_wkfwmst[0]["wkfw_content"];

	$num = 0;
	$pos = 0;
	while (1) {
		$pos = strpos($wkfw_content, 'show_cal', $pos);
		if ($pos === false) {
			break;
		} else {
			$num++;
		}
		$pos++;
	}

	if ($num > 0) {
		// 外部ファイルを読み込む
		write_yui_calendar_use_file_read_0_12_2();
	}
	// カレンダー作成、関数出力
	write_yui_calendar_script2($num);
}
?>

<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}


table.block2 {border-collapse:collapse;}
table.block2 td {border:#5279a5 solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#5279a5 solid 0px;}
table.block3 td {border-width:0;}

img.close {
	background-image:url("images/folder_close_bro.gif");
	cursor:hand;
}
img.open {
	background-image:url("images/folder_open_bro.gif");
	cursor:hand;
}
p {margin:0;}
textarea {overflow:auto;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initcal();if (window.OnloadSub) { OnloadSub(); };resizeAllTextArea();copyApproveOrders();">

<form name="apply" action="#" method="post">
<?
	if($wkfw_id != "") {

		show_application_template($con, $session, $fname, $wkfw_id, $apply_title, $content, $file_id, $filename, $back, $mode);
?>
    <table id="approve_orders" width="100%" border="0" cellspacing="0" cellpadding="2" class="block2">
    </table>
<?
	}
?>
</form>
</body>
<? pg_close($con); ?>
<script type="text/javascript">
function copyApproveOrders() {
	opener.parent.updateDOM();

	var self_table = document.getElementById('approve_orders');
	var opener_rows = opener.parent.document.getElementById('approve_orders').rows;
	for (var i = 0, j = opener_rows.length; i < j; i++) {
		var row = self_table.insertRow(-1);
		row.style.backgroundColor = opener_rows[i].style.backgroundColor;
		row.style.height = opener_rows[i].style.height;
		row.style.textAlign = 'center';

		var opener_cells = opener_rows[i].cells;
		for (var m = 0, n = opener_cells.length; m < n; m++) {
			var cell = row.insertCell(-1);
			cell.style.width = '20%';
			cell.innerHTML = opener_cells[m].innerHTML;
		}
	}
}

document.apply.apply_title.value=opener.parent.document.apply.apply_title.value;
setTimeout("self.print();self.close();", 100);
</script>
</html>
