<?php
require_once('Cmx.php');
require_once('Cmx/View/Smarty.php');
require_once("l4pWClass.php");
$lfp = new l4pWClass("FANTOL");

require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");
require_once("get_values.php");
require_once("hiyari_report_class.php");
require_once("hiyari_mail.ini");
require_once("hiyari_mail_input.ini");
require_once("hiyari_common.ini");
require_once("hiyari_item_time_setting_models.php");
require_once("hiyari_item_timelag_setting_models.php");
require_once("aclg_set.php");
require_once('Cmx/Model/SystemConfig.php');

//====================================================================================================
//前処理
//====================================================================================================

// CmxSystemConfigインスタンス生成
$sysConf = new Cmx_SystemConfig();
//デザイン（1=旧デザイン、2=新デザイン）
$design_mode = $sysConf->get('fantol.design.mode');

//テンプレート読み込み
require_once("smarty_setting.ini");
$smarty = new Cmx_View();

//ページ名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
    showLoginPage();
    exit;
}

// 権限チェック
$hiyari = check_authority($session, 47, $fname);
if ($hiyari == "0") {
    showLoginPage();
    exit;
}

//DBコネクション
$con = connect2db($fname);
if($con == "0"){
    showErrorPage();
    exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

//20130630
//==================================================
//ファイル添付
//==================================================
//==================================================
//オプション
//==================================================
$sysConf = new Cmx_SystemConfig();
// 報告書へのファイル添付フラグ
$fantol_file_attach = $sysConf->get('fantol.report.file.attach');
$fantol_file_attach = $fantol_file_attach === null ? '0' : $fantol_file_attach;

// 報告書を編集、評価時、送信者本人をメール送信の宛先に含めない 20141008
$exclude_mail_sender_flg = $sysConf->get('fantol.exclude.mail.sender.flg') == 't';

//受信トレイにある他の部署の報告書を評価可能にするフラグを取得
//$is_progress_eval = get_progress_eval($con, $fname);
$is_progress_eval = $sysConf->get('fantol.evaluation.common.use.flg');
//==================================================
//SM未設定の場合は終了
//==================================================
if(! is_sm_emp_exist($session,$fname,$con))
{
    //画面終了。
    ?>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
    <html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
    <script language='javascript'>
    alert("ＳＭが設定されていません。");
    window.close();
    </script>
    </head>
    <body>
    </body>
    </html>
    <?
    pg_close($con);
    exit;
}

//==================================================
// セッション開始
//==================================================
session_name("hyr_sid");
session_start();

//画面モード
$gamen_mode = $_SESSION['easyinput']['gamen_mode'];

//ログインユーザの権限
$arr_auth = get_my_inci_auth_list($session,$fname,$con);

//====================================================================================================
//ポストバック時の処理
//====================================================================================================
//※入力画面から直接更新する際もポストバック指定されるケースあり。

if ( $is_postback != "" ) {
    //==================================================
    //POST送信／GET送信を吸収
    //==================================================
    //※ただし、メール送信についてはPOST限定。
    $postback_mode = "";

    if ( $shitagaki != '' ) {
        $postback_mode = "shitagaki";
    }

    if ( $send_create != '' ) {
        $postback_mode = "send_create";
    }

    if ( $update != '' ) {
        $postback_mode = "update";
    }

    if ( $send_update != '' ) {
        $postback_mode = "send_update";
    }

    if ( $back != '' ) {
        $postback_mode = "back";
    }


    //==================================================
    // 保存処理　{下書き／送信(新規)／更新／送信(更新)}
    //==================================================
    if ( $postback_mode == "shitagaki" or $postback_mode == "send_create" or $postback_mode == "update" or $postback_mode == "send_update" )
    {
        //==============================
        //処理フラグ判定
        //==============================

        //新規画面で下書き
        if ( $postback_mode == "shitagaki" && $_SESSION['easyinput']['gamen_mode'] == 'new') {
            $is_shitagaki = true;
            $is_update    = false;
            $is_mail_send = false;
        }

        //下書き画面で下書き
        elseif ( $postback_mode == "shitagaki" && $_SESSION['easyinput']['gamen_mode'] == 'shitagaki' ) {
            $is_shitagaki = true;
            $is_update    = true;
            $is_mail_send = false;
        }

        //新規作成(下書きからの作成も含む)
        elseif ( $postback_mode == "send_create" ) {
            $is_shitagaki = false;
            $is_update    = false;
            $is_mail_send = true;
        }

        //更新(送信なし)
        elseif ( $postback_mode == "update" ) {
            $is_shitagaki = false;
            $is_update    = true;
            $is_mail_send = false;

            //ユーザーID取得
            $emp_id = get_emp_id($con, $session, $fname);
            $lfp->debugRst("FANTOL","REPORT-UPDATE-NO-MAIL:emp_id:".$emp_id." report_id:".$_SESSION['easyinput']['report_id']);

        }

        //更新(送信あり)
        elseif ( $postback_mode == "send_update" ) {
            $is_shitagaki = false;
            $is_update    = true;
            $is_mail_send = true;
        }

        //ありえない
        else{}

        //===========================================
        // 報告書更新ロック
        //===========================================
        action_lock(LOCK_HIYARI_REPORT_UPDATE);

        //==============================
        //準備処理
        //==============================

        //トランザクション開始
        pg_query($con,"begin transaction");

        //ユーザーID取得
        $emp_id = get_emp_id($con, $session, $fname);

        //レポートクラス
        $rep_obj = new hiyari_report_class($con, $fname);


        //==============================
        //更新前データ情報の取得
        //==============================
        //更新(送信あり)のみ必要
        if ( $is_update && $is_mail_send ) {
            $update_before_report_data = $rep_obj->get_report( $_SESSION['easyinput']['report_id'] );
        }

        else {
            $update_before_report_data = "";
        }

        //==============================
        //登録処理
        //==============================
        if ( !$is_update ) {

            // レポートを新規登録
            $is_anonymous = ( !$is_shitagaki ) && ( $anonymous != "" );

            // 下書きの時は「匿名にする」
            if($is_shitagaki) $is_anonymous = get_anonymous_mail_use_flg($con,$fname);//false;
            $report_id = $rep_obj->set_report($session,$emp_id, $_SESSION['easyinput'], $is_anonymous, $is_shitagaki);

            // 新規作成(下書きからの作成も含む)の場合、レポート進捗を登録
            if ( $postback_mode == "send_create" ) {
                $to_default_list = get_new_report_default_tocc_list($con,$fname,'TO',$registrant_class,$registrant_attribute,$registrant_dept,$registrant_room,$doctor_emp_id);
                $rep_obj->regist_inci_report_progress_at_send($report_id,$to_default_list);
            }

            // 下書きを登録した場合の下書き削除処理
            if ( $_SESSION['easyinput']['gamen_mode'] == 'shitagaki') {
                $shitagaki_report_id = $_SESSION['easyinput']['report_id'];

                //20130630
                //==================================================
                //ファイル添付(下書きから送信した場合は添付ファイルをフォルダ移動させる)report_idが変更になる為
                //==================================================
                $rep_obj->upd_tmpFile_for_shitagaki($emp_id, $report_id, $_SESSION['easyinput']['report_id']);

                $rep_obj->del_report($shitagaki_report_id);
            }

        }


        //==============================
        //更新処理
        //==============================
        else {
            //==============================
            //更新対象のレポートID
            //==============================
            $report_id = $_SESSION['easyinput']['report_id'];

            // レポートデータを取得
            $report_data = $rep_obj->get_report($report_id);


            // 下書きのとき匿名の判別
            if ( $is_shitagaki ) {
                $is_anonymous = get_anonymous_mail_use_flg($con,$fname);

                if ( $is_anonymous === true ) {
                    $is_anonymous_bool = 't';
                }

                else if ( $is_anonymous === false ) {
                    $is_anonymous_bool = 'f';
                }

                $rep_obj->set_report_anonymous($report_id,$is_anonymous_bool);
            }
            //==============================
            //今回の入力グループ情報を取得
            //==============================

            //分析・再発防止の場合(独自様式)
            if ( $_SESSION['easyinput']['gamen_mode'] == 'analysis') {
                $use_grp_code = $rep_obj->get_analysis_grps($session);
                if ( is_sm_emp($session, $fname) ) {
                    $use_grp_code[] = 690;
                }
            }

            //カテゴリ指定の場合
            else if ( $_SESSION['easyinput']['gamen_mode'] == 'cate_update' ) {
                $use_grp_code = $rep_obj->get_original_eis_grps($_SESSION['easyinput']['cate']);
                //SM専用項目なし
            }

            //様式がある場合
            else if ($_SESSION['easyinput']['eis_no'] != "" ) {

                $grp_flags = $rep_obj->get_report_flags_by_eis_no($_SESSION['easyinput']['eis_no']);
                $use_grp_code = $grp_flags['use_grp_code'];

                // 表題モード取得
                $subject_flags = $rep_obj->get_report_flags_by_eis_no($_SESSION['easyinput']['eis_no']);
                $sql    = "select subject_mode from inci_subject_mode ";
                $cond   = "where eis_id = " .$subject_flags['eis_id'];
                $result = select_from_table($con, $sql, $cond, $fname);
                $subject = pg_fetch_result($result, 0, "subject_mode");

                if ( $subject == "1" ) {
                    if( FALSE == array_search('125', $use_grp_code) ) {
                        //ヒヤリハット分類が表示されない且つ表題で分類を登録している場合は125を追加しておく
                        $use_grp_code[] = "125";
                    }
                }

                //SM専用項目なし
            }

            //様式が無い場合(全項目様式)
            else {
                $use_grp_code = $rep_obj->get_full_grps();
                if ( is_sm_emp($session,$fname) ) {
                    $use_grp_code[] = 690;
                }
            }

            //==============================
            //入力項目を更新
            //==============================
            $report_title = $_SESSION['easyinput']['report_title'];
            $regist_date_y = $_SESSION['easyinput']['regist_date_y'];
            $regist_date_m = $_SESSION['easyinput']['regist_date_m'];
            $regist_date_d = $_SESSION['easyinput']['regist_date_d'];


            // 報告（第一報）時刻（タイムスタンプ）取得
            $first_send_time_stamp = mktime(
                substr($report_data['first_send_time'], 8, 2),
                substr($report_data['first_send_time'], 10, 2),
                substr($report_data['first_send_time'], 12, 2),
                substr($report_data['first_send_time'], 4, 2),
                substr($report_data['first_send_time'], 6, 2),
                substr($report_data['first_send_time'], 0, 4)
            );

            // タイムラグ区分取得
            $_SESSION['easyinput']['_105_70'] = getTimelagClass($con, $fname , $first_send_time_stamp, $_SESSION['easyinput'] );

            // タイムラグ取得
            $_SESSION['easyinput']['_105_75'] = getTimelag($con, $fname , $first_send_time_stamp, $_SESSION['easyinput'] );

            // 様式がある場合
            if ( $_SESSION['easyinput']['job_id'] != "" && $_SESSION['easyinput']['eis_no'] != "") {
                $job_id = $_SESSION['easyinput']['job_id'];
                $eis_id = $rep_obj->get_eis_id_from_eis_no($_SESSION['easyinput']['eis_no']);
                if ($eis_id == "") {
                    pg_query($con,"rollback");
                    pg_close($con);
                    showErrorPage();
                    exit;
                }
            }

            $rep_obj->set_report_title($report_id,$report_title);
            $rep_obj->set_registration_date($report_id,$regist_date_y, $regist_date_m, $regist_date_d);
            // 下書き更新の場合のみ様式情報を更新。つまり、新規作成時の様式が保存されることとなる。
            if ( $_SESSION['easyinput']['gamen_mode'] == 'shitagaki') {
                $rep_obj->set_report_data_field($report_id, "job_id", "$job_id");
                $rep_obj->set_report_data_field($report_id, "eis_id", "$eis_id");
            }

            $rep_obj->set_report_content($session,$report_id, $eis_id, $_SESSION['easyinput'], $use_grp_code);

            //所属情報
            $registrant_class = $_SESSION['easyinput']['registrant_class'];
            $registrant_attribute = $_SESSION['easyinput']['registrant_attribute'];
            $registrant_dept = $_SESSION['easyinput']['registrant_dept'];
            $registrant_room = $_SESSION['easyinput']['registrant_room'];
            if($registrant_class == ""){$registrant_class = null;}
            if($registrant_attribute == ""){$registrant_attribute = null;}
            if($registrant_dept == ""){$registrant_dept = null;}
            if($registrant_room == ""){$registrant_room = null;}
            $rep_obj->set_report_data_field($report_id, "registrant_class", $registrant_class);
            $rep_obj->set_report_data_field($report_id, "registrant_attribute", $registrant_attribute);
            $rep_obj->set_report_data_field($report_id, "registrant_dept", $registrant_dept);
            $rep_obj->set_report_data_field($report_id, "registrant_room", $registrant_room);

            //主治医
            $doctor_emp_id = $_SESSION['easyinput']['doctor_emp_id'];
            if($doctor_emp_id == ""){$doctor_emp_id = null;}
            $rep_obj->set_report_data_field($report_id, "doctor_emp_id", $doctor_emp_id);

            //20130630
            //==================================================
            //ファイル添付(実領域へ移動)
            //==================================================
            $rep_obj->move_hiyari_file($report_id, $emp_id, $_SESSION['easyinput']['gamen_mode']);

            //inci_reportに添付ファイルを登録
            $rep_obj->set_inci_file_report($report_id);
        }

        //==============================
        //メール送信処理
        //==============================
        $original_mail_id = $_SESSION['easyinput']['mail_id'];
        $mail_id = "";
        if($is_mail_send)
        {
            //インシデントメール送信(レポートデータTEXTをメッセージに付加して送信)
            $job_id = $_SESSION['easyinput']['job_id'];
            $eis_id = $rep_obj->get_eis_id_from_eis_no($_SESSION['easyinput']['eis_no']);
            $mail_post_data = $_POST;
            $message2 = get_report_html_for_mail_message($con,$fname,$report_id,$eis_id,$update_before_report_data,'',$eis_title_id);//TEXT形式(改行付き)で得られる。
            $message2 = '<table width="60%" border="0" cellspacing="0" cellpadding="0"><tr><td>'.$message2.'</td></tr></table>';//メール表示画面用にカット
            $report_html = $mail_message."<br><br>".$message2;

            $mail_messagexxx = $mail_post_data["mail_message"];

            $mail_id = send_mail_from_submit_data($session,$con,$fname,$mail_post_data,$report_id,$job_id,$eis_id,$update_before_report_data,$eis_title_id,'',$report_html);
        }

        //==============================
        //報告書履歴登録
        //==============================
        if(!$is_shitagaki)
        {
            $rep_obj->set_report_rireki($report_id,$emp_id,$mail_id);
        }

        //==============================
        //評価
        //==============================
        if ($gamen_mode == 'analysis') {
            //評価ステータス更新
            if($is_progress_eval=='t'){
                $arr_report_auth = $arr_auth;
            }else{
                $arr_report_auth = $rep_obj->get_my_inci_auth_list_for_report_progress($report_id, $emp_id);
            }

            foreach ($arr_report_auth as $key => $auth) {
                //受け済みにする
                $rep_obj->set_report_progress_start_date($report_id, $auth['auth'], $emp_id,$original_mail_id);
                if ($analysis_draft == '') {
                    //確認済みにする
                    $rep_obj->set_report_progress_end_date($report_id, $auth['auth'], $emp_id,$original_mail_id);
                    //評価済みにする
                    $rep_obj->set_report_progress_evaluation_date($report_id, $auth['auth'], $emp_id,$original_mail_id);
                }
            }

            if ($design_mode == 2){
                // 新デザインの場合はコメント保存
                $comment_list = array();
                $target_auth_arr = explode(",", $_SESSION['easyinput']['target_auth_list']);
                foreach ($target_auth_arr as $auth) {
                    $key = "comment_" . $auth;
                    $comment_list[$auth] = $_SESSION['easyinput'][$key];
                }
                $rep_obj->set_report_progress_comment($report_id, $emp_id, $comment_list);
            }
        }

        //==============================
        //後処理
        //==============================

        //トランザクション終了
        pg_query($con, "commit");

        //===========================================
        // 報告書更新ロック解除
        //===========================================
        action_unlock(LOCK_HIYARI_REPORT_UPDATE);


        //画面終了
        if($is_mail_send)
        {
            $msg = "報告書を送信しました";
        }
        elseif($is_shitagaki)
        {
            $msg = '報告書を下書き保存しました。\n報告は完了していません。\n再開後速やかに報告を完了してください。';
        }
        elseif($is_update)
        {
            $msg = "報告書を更新しました";
        }
        else
        {
            $msg = "報告書を登録しました";
        }
        ?>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
        <html>
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
        <script language='javascript'>
        alert("<?=$msg?>");
        if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}
        window.close();
        </script>
        </head>
        <body>
        </body>
        </html>
        <?

        // セッションの破棄
        session_unset();
        session_destroy();

        //DB切断
        pg_close($con);

        // 実行終了
        exit;
    }

    //==================================================
    // 戻る
    //==================================================
    else if ( $postback_mode == "back" )
    {
        //確認画面から戻ったことを通知するマークをセッションに設定
        $_SESSION['easyinput']['return_from_confirm'] = true;

        //入力画面へ移動
        $callerpage = $_SESSION['easyinput']['callerpage'];
        redirectPage("hiyari_easyinput.php?session=$session&callerpage=$callerpage" . SID);

        //DB切断
        pg_close($con);

        // 実行終了
        exit;
    }
}

$rep_obj   = new hiyari_report_class($con, $fname);

// 様式の取得
if($id = $_SESSION['easyinput']['eis_title_id']) {
    $sql  = "select * from inci_item_title ";
    $cond = "where eis_id = " . pg_escape_string($id);
    $result = select_from_table( $con, $sql, $cond, $fname );
    if ($result == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        showErrorPage();
        exit;
    }

    $tmp_eis_item_title = pg_fetch_all($result);

    // 文言変更データを修正する
    foreach($tmp_eis_item_title as $val) {
        $eis_item_title[$val['eis_column']] = $val['eis_title'];
    }

    // ユーザー定義カテゴリ項目
    $grp_flags = $rep_obj->get_report_flags($id);
    $user_cate_name = $grp_flags['user_cate_name'];

} else {
    $sql    = "select cate_name from inci_easyinput_category_mst";
    $cond   = "where cate_code = 10";
    $result = select_from_table($con, $sql, $cond, $fname);
    $user_cate_name = @pg_fetch_result($result, 0, "cate_name");
}

//====================================================================================================
//通常表示時の処理
//====================================================================================================

//==================================================
//テンプレートに値を設定
//==================================================

//セッション情報
$smarty->assign("session", $session);
$smarty->assign("hyr_sid", session_id());

//画面モード
$smarty->assign("gamen_mode", $gamen_mode);

//画面タイトル
if($gamen_mode == "new" || $gamen_mode == "shitagaki")
{
    $PAGE_TITLE = "報告書登録";
}
elseif($gamen_mode == "update")
{
    $PAGE_TITLE = "出来事報告編集";
}
elseif($gamen_mode == "analysis")
{
    if ($design_mode == 1){
        $PAGE_TITLE = "分析・再発防止";
    }
    else{
        $PAGE_TITLE = "評価";
    }
}

$smarty->assign("INCIDENT_TITLE", $INCIDENT_TITLE);
$smarty->assign("PAGE_TITLE", $PAGE_TITLE);

// 報告書スタイル
$smarty->assign("style_code", $_SESSION['easyinput']['style_code']);

//DBアクセス情報
$smarty->assign("con", $con);
$smarty->assign("fname", $fname);

//所属情報
$registrant_class = $_SESSION['easyinput']['registrant_class'];
$registrant_attribute = $_SESSION['easyinput']['registrant_attribute'];
$registrant_dept = $_SESSION['easyinput']['registrant_dept'];
$registrant_room = $_SESSION['easyinput']['registrant_room'];
$smarty->assign("registrant_class", $registrant_class);
$smarty->assign("registrant_attribute", $registrant_attribute);
$smarty->assign("registrant_dept", $registrant_dept);
$smarty->assign("registrant_room", $registrant_room);

if ($_SESSION['easyinput']['style_code'] == 2){
    if ($registrant_class != "") {
        $class_name = get_class_nm($con,$registrant_class,$fname);
    }
    else{
        $class_name = "不明";
    }
    $smarty->assign("class_name", $class_name);

    if($registrant_attribute != "") {
        $attribute_name = get_atrb_nm($con,$registrant_attribute,$fname);
    }
    $smarty->assign("attribute_name", $attribute_name);

    if($registrant_dept != "") {
        $dept_name = get_dept_nm($con,$registrant_dept,$fname);
    }
    $smarty->assign("dept_name", $dept_name);

    $arr_class_name = get_class_name_array($con, $fname);
    if($registrant_room != "" && $arr_class_name["class_cnt"] == 4) {
        $room_name = get_room_nm($con,$registrant_room,$fname);
    }
    $smarty->assign("room_name", $room_name);
}

// 様式
$smarty->assign("eis_item_title", $eis_item_title);

//レポートタイトル
$report_title = $_SESSION['easyinput']['report_title'];
$report_title = h($report_title);
$smarty->assign("report_title", $report_title);

//報告日
$regist_date_y = $_SESSION['easyinput']['regist_date_y'];
$regist_date_m = $_SESSION['easyinput']['regist_date_m'];
$regist_date_d = $_SESSION['easyinput']['regist_date_d'];
$smarty->assign("regist_date_y", $regist_date_y);
$smarty->assign("regist_date_m", $regist_date_m);
$smarty->assign("regist_date_d", $regist_date_d);

if ($gamen_mode == 'analysis'){
    if (isset($_SESSION['easyinput']['comment'])){
        $smarty->assign("progress_comment_data", $_SESSION['easyinput']['comment']);
    }
}

//主治医
$doctor_emp_id = $_SESSION['easyinput']['doctor_emp_id'];
$smarty->assign("doctor_emp_id", $doctor_emp_id);

//ユーザー定義カテゴリ名
$smarty->assign("user_cate_name", $user_cate_name);

//レポート項目
$grps      = $rep_obj->get_all_easy_items();
$inputs1   = $rep_obj->conv_form_item_codes($_SESSION['easyinput']);
$inputs    = $rep_obj->get_easy_item_names_value( $inputs1 );
$grp_flags = array_keys($inputs);
$user_grp_flags = array();
foreach ($grp_flags as $grp_code){
    if ($grp_code >= 10000){
        $user_grp_flags[] = $grp_code;
    }
}

$vals      = $rep_obj->get_easy_item_names( $inputs1 );
// 8番目の処理
$text_vals = $rep_obj->get_easy_item_names_8( $inputs1 );
$smarty->assign("text_vals", $text_vals);

$patient_use = explode(",", $_SESSION['easyinput']['patient_use']);
$eis_400_use = explode(",", $_SESSION['easyinput']['eis_400_use']);
$eis_410_use = explode(",", $_SESSION['easyinput']['eis_410_use']);
$eis_420_use = explode(",", $_SESSION['easyinput']['eis_420_use']);
$eis_430_use = explode(",", $_SESSION['easyinput']['eis_430_use']);
$eis_440_use = explode(",", $_SESSION['easyinput']['eis_440_use']);
$eis_450_use = explode(",", $_SESSION['easyinput']['eis_450_use']);
$eis_460_use = explode(",", $_SESSION['easyinput']['eis_460_use']);
$eis_470_use = explode(",", $_SESSION['easyinput']['eis_470_use']);
$eis_480_use = explode(",", $_SESSION['easyinput']['eis_480_use']);
$eis_490_use = explode(",", $_SESSION['easyinput']['eis_490_use']);

/* インシデントの修正 START */
if($_SESSION['easyinput']['_700_10'])$eis_700_10 = $rep_obj->get_super_item_by_id($_SESSION['easyinput']['_700_10']);
if($_SESSION['easyinput']['_710_10'])$eis_710_10 = $rep_obj->get_item_by_id($_SESSION['easyinput']['_710_10'], 'scene');
if($_SESSION['easyinput']['_720_10'])$eis_720_10 = $rep_obj->get_sub_item_by_id($_SESSION['easyinput']['_720_10'], 'scene');
if($_SESSION['easyinput']['_740_10'])$eis_740_10 = $rep_obj->get_item_by_id($_SESSION['easyinput']['_740_10'], 'kind');
if($_SESSION['easyinput']['_750_10'])$eis_750_10 = $rep_obj->get_sub_item_by_id($_SESSION['easyinput']['_750_10'], 'kind');
if($_SESSION['easyinput']['_770_10'])$eis_770_10 = $rep_obj->get_item_by_id($_SESSION['easyinput']['_770_10'], 'content');
if($_SESSION['easyinput']['_780_10'])$eis_780_10 = $rep_obj->get_sub_item_by_id($_SESSION['easyinput']['_780_10'], 'content');
$smarty->assign("eis_700_10", $eis_700_10);
$smarty->assign("eis_710_10", $eis_710_10);
$smarty->assign("eis_720_10", $eis_720_10);
$smarty->assign("eis_740_10", $eis_740_10);
$smarty->assign("eis_750_10", $eis_750_10);
$smarty->assign("eis_770_10", $eis_770_10);
$smarty->assign("eis_780_10", $eis_780_10);
/* インシデントの修正 END */
/* 追加項目の修正 START */
if($_SESSION['easyinput']['_800_5'])$item_show_flags[] = '5';
if($_SESSION['easyinput']['_800_10'])$item_show_flags[] = '10';
if($_SESSION['easyinput']['_800_15'])$item_show_flags[] = '15';
if($_SESSION['easyinput']['_800_20'])$item_show_flags[] = '20';
if($_SESSION['easyinput']['_800_25'])$item_show_flags[] = '25';
if($_SESSION['easyinput']['_800_30'])$item_show_flags[] = '30';
if($_SESSION['easyinput']['_800_35'])$item_show_flags[] = '35';
if($_SESSION['easyinput']['_800_40'])$item_show_flags[] = '40';
if($_SESSION['easyinput']['_800_45'])$item_show_flags[] = '45';
if($_SESSION['easyinput']['_800_50'])$item_show_flags[] = '50';
if($_SESSION['easyinput']['_800_55'])$item_show_flags[] = '55';
if($_SESSION['easyinput']['_800_60'])$item_show_flags[] = '60';
if($_SESSION['easyinput']['_800_65'])$item_show_flags[] = '65';
if($_SESSION['easyinput']['_800_70'])$item_show_flags[] = '70';
$smarty->assign("item_show_flags", $item_show_flags);
/* 追加項目の修正 END */
/* インシデントの修正2010 START */
if ($_SESSION['easyinput']['style_code']==2){
  if($_SESSION['easyinput']['_900_10'][0])$eis_900_10 = $rep_obj->get_super_item_by_id_2010($_SESSION['easyinput']['_900_10'][0]);
  if($_SESSION['easyinput']['_910_10'][0])$eis_910_10 = $rep_obj->get_item_by_id_2010($_SESSION['easyinput']['_910_10'][0], 'kind');
  if($_SESSION['easyinput']['_920_10'][0])$eis_920_10 = $rep_obj->get_sub_item_by_id_2010($_SESSION['easyinput']['_920_10'][0], 'kind');
  if($_SESSION['easyinput']['_940_10'][0])$eis_940_10 = $rep_obj->get_item_by_id_2010($_SESSION['easyinput']['_940_10'][0], 'scene');
  if($_SESSION['easyinput']['_950_10'][0])$eis_950_10 = $rep_obj->get_sub_item_by_id_2010($_SESSION['easyinput']['_950_10'][0], 'scene');
  if($_SESSION['easyinput']['_970_10'][0])$eis_970_10 = $rep_obj->get_item_by_id_2010($_SESSION['easyinput']['_970_10'][0], 'content');
  if($_SESSION['easyinput']['_980_10'][0])$eis_980_10 = $rep_obj->get_sub_item_by_id_2010($_SESSION['easyinput']['_980_10'][0], 'content');
}
else{
  if($_SESSION['easyinput']['_900_10'])$eis_900_10 = $rep_obj->get_super_item_by_id_2010($_SESSION['easyinput']['_900_10']);
  if($_SESSION['easyinput']['_910_10'])$eis_910_10 = $rep_obj->get_item_by_id_2010($_SESSION['easyinput']['_910_10'], 'kind');
  if($_SESSION['easyinput']['_920_10'])$eis_920_10 = $rep_obj->get_sub_item_by_id_2010($_SESSION['easyinput']['_920_10'], 'kind');
  if($_SESSION['easyinput']['_940_10'])$eis_940_10 = $rep_obj->get_item_by_id_2010($_SESSION['easyinput']['_940_10'], 'scene');
  if($_SESSION['easyinput']['_950_10'])$eis_950_10 = $rep_obj->get_sub_item_by_id_2010($_SESSION['easyinput']['_950_10'], 'scene');
  if($_SESSION['easyinput']['_970_10'])$eis_970_10 = $rep_obj->get_item_by_id_2010($_SESSION['easyinput']['_970_10'], 'content');
  if($_SESSION['easyinput']['_980_10'])$eis_980_10 = $rep_obj->get_sub_item_by_id_2010($_SESSION['easyinput']['_980_10'], 'content');
}

$smarty->assign("eis_900_10", $eis_900_10);
$smarty->assign("eis_910_10", $eis_910_10);
$smarty->assign("eis_920_10", $eis_920_10);
$smarty->assign("eis_940_10", $eis_940_10);
$smarty->assign("eis_950_10", $eis_950_10);
$smarty->assign("eis_970_10", $eis_970_10);
$smarty->assign("eis_980_10", $eis_980_10);
/* インシデントの修正 END */
/* 影響と対応 START */
if ($_SESSION['easyinput']['style_code']==2){
  if($_SESSION['easyinput']['_1400_10'][0])$eis_1400_10 = $rep_obj->get_ic_super_item_by_id($_SESSION['easyinput']['_1400_10'][0]);
}
else{
  if($_SESSION['easyinput']['_1400_10'])$eis_1400_10 = $rep_obj->get_ic_super_item_by_id($_SESSION['easyinput']['_1400_10']);
}
if($_SESSION['easyinput']['_1400_30']) {
    foreach ($_SESSION['easyinput']['_1400_30'] as $value) {
        $eis_1400_30[] = $rep_obj->get_ic_item_by_id($value, 'influence');
    }
}
if($_SESSION['easyinput']['_1400_60']) {
    foreach ($_SESSION['easyinput']['_1400_60'] as $value) {
        $eis_1400_60[] = $rep_obj->get_ic_item_by_id($value, 'mental');
    }
}
if ($_SESSION['easyinput']['style_code']==2){
  if($_SESSION['easyinput']['_1410_10'][0])$eis_1410_10 = $rep_obj->get_ic_super_item_by_id($_SESSION['easyinput']['_1410_10'][0]);
}
else{
  if($_SESSION['easyinput']['_1410_10'])$eis_1410_10 = $rep_obj->get_ic_super_item_by_id($_SESSION['easyinput']['_1410_10']);
}
if($_SESSION['easyinput']['_1410_20']) {
    foreach ($_SESSION['easyinput']['_1410_20'] as $value) {
        $eis_1410_20[] = $rep_obj->get_ic_item_by_id($value, 'correspondence');
    }
}
if($_SESSION['easyinput']['_1410_30']) {
    //unset($grps[1410]['easy_item_list'][30]['easy_item_name']);
    foreach ($_SESSION['easyinput']['_1410_30'] as $value) {
        $param = $rep_obj->get_ic_item_name_by_sub_item_id($value);
        $temp = $rep_obj->get_ic_sub_item_by_id($value, 'correspondence');
        $temp['sub_item_name'] = $temp['sub_item_name'];
        $eis_1410_30[] = $temp;
    }
}
$smarty->assign("eis_1400_10", $eis_1400_10);
$smarty->assign("eis_1400_30", $eis_1400_30);
$smarty->assign("eis_1400_60", $eis_1400_60);
$smarty->assign("eis_1410_10", $eis_1410_10);
$smarty->assign("eis_1410_20", $eis_1410_20);
$smarty->assign("eis_1410_30", $eis_1410_30);
/* 影響と対応 END */
/* 追加項目の修正 START */
if($_SESSION['easyinput']['_1000_5'])$item_show_flags_2010[] = '5';
if($_SESSION['easyinput']['_1000_10'])$item_show_flags_2010[] = '10';
if($_SESSION['easyinput']['_1000_15'])$item_show_flags_2010[] = '15';
if($_SESSION['easyinput']['_1000_20'])$item_show_flags_2010[] = '20';
if($_SESSION['easyinput']['_1000_25'])$item_show_flags_2010[] = '25';
if($_SESSION['easyinput']['_1000_30'])$item_show_flags_2010[] = '30';
if($_SESSION['easyinput']['_1000_35'])$item_show_flags_2010[] = '35';
if($_SESSION['easyinput']['_1000_40'])$item_show_flags_2010[] = '40';
if($_SESSION['easyinput']['_1000_45'])$item_show_flags_2010[] = '45';
if($_SESSION['easyinput']['_1000_50'])$item_show_flags_2010[] = '50';
if($_SESSION['easyinput']['_1000_55'])$item_show_flags_2010[] = '55';
$smarty->assign("item_show_flags_2010", $item_show_flags_2010);

/* 追加項目の修正 END */
$smarty->assign("grps", $grps);
$smarty->assign("grp_flags", $grp_flags);
$smarty->assign("user_grp_flags", $user_grp_flags);

$smarty->assign("vals", $vals);
$smarty->assign("patient_use", $patient_use);
$smarty->assign("eis_400_use", $eis_400_use);
$smarty->assign("eis_410_use", $eis_410_use);
$smarty->assign("eis_420_use", $eis_420_use);
$smarty->assign("eis_430_use", $eis_430_use);
$smarty->assign("eis_440_use", $eis_440_use);
$smarty->assign("eis_450_use", $eis_450_use);
$smarty->assign("eis_460_use", $eis_460_use);
$smarty->assign("eis_470_use", $eis_470_use);
$smarty->assign("eis_480_use", $eis_480_use);
$smarty->assign("eis_490_use", $eis_490_use);

// *** 発見・発生日時のモード設定 ***

// 設定情報を取得
$time_setting_flg = getTimeSetting($con, $PHP_SELF);

// 発見・発生日時のモード設定を送信
$smarty->assign("time_setting_flg", $time_setting_flg);


/*************************************************
 * 更新権限の取得 20100525
 *************************************************/
$update = get_auth_analysis_use($con, $fname);

if(empty($arr_auth)) $arr_auth[0]['auth'] = 'GU';

$update_flg = false;
foreach($arr_auth as $auth) {
    if($update[$auth['auth']]['update_flg'] == 't') {
        $update_flg = true;
        break;
    }
}


//メール送信情報
$default_subject = $_SESSION['easyinput']['report_title'];
$default_subject = h($default_subject);
$default_to_list = get_new_report_default_tocc_list($con,$fname,'TO',$registrant_class,$registrant_attribute,$registrant_dept,$registrant_room,$doctor_emp_id);
$default_cc_list = get_new_report_default_tocc_list($con,$fname,'CC',$registrant_class,$registrant_attribute,$registrant_dept,$registrant_room,$doctor_emp_id);
//$is_mail_sendable = ($_SESSION['easyinput']['eis_no'] != "" );//様式が無ければ送信不可能。
$is_mail_sendable = ($_SESSION['easyinput']['eis_no'] != "" && !($update_flg && $gamen_mode == 'analysis'));//様式が無ければ送信不可能。
//$is_mail_not_sendable = (is_sm_emp($session,$fname)  && $gamen_mode != 'new' && $gamen_mode != 'shitagaki') || !$is_mail_sendable;//SM(更新時)は常に非送信可能。メール送信不可能なケース以外は非送信可能。
$is_mail_not_sendable = ($update_flg  && $gamen_mode != 'new' && $gamen_mode != 'shitagaki');//SM(更新時)は常に非送信可能。メール送信不可能なケース以外は非送信可能。
/*
error_log("
    is_mail_sendable={$is_mail_sendable},
    eis_no={$_SESSION['easyinput']['eis_no']},
    update_flg={$update_flg},
    gamen_mode={$gamen_mode},\n
", 3, "log/fantol.log");
*/
$smarty->assign("is_mail_sendable", $is_mail_sendable);
$smarty->assign("is_mail_not_sendable", $is_mail_not_sendable);
$smarty->assign("eis_title_id", $_SESSION['easyinput']['eis_title_id']);

// 分析再発画面利用権限・取得処理
function get_auth_analysis_use($con, $fname)
{
    $array = array();

    $sql  = "select a.auth, a.use_flg, a.update_flg, case when a.auth = 'GU' then '一般ユーザ' else b.auth_name end ";
    $sql .= "from inci_auth_analysis_use a left join inci_auth_mst b on a.auth = b.auth ";
    $cond = "order by b.auth_rank is not null, b.auth_rank asc";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0)
    {
        pg_close($con);
        echo("<script type='text/javascript' src='js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    while($row = pg_fetch_array($sel))
    {
        $auth = $row["auth"];

        if($auth != "GU" && !is_usable_auth($auth, $fname, $con))
        {
            continue;
        }

        $array[$auth] = array("use_flg" => $row["use_flg"], "auth_name" => $row["auth_name"], "update_flg" => $row["update_flg"]);
    }

    return $array;
}

//事案番号/報告者ID
if($gamen_mode == "update" || $gamen_mode == "analysis")
{
    $report_id = $_SESSION['easyinput']['report_id'];
    $inci_report_record = $rep_obj->get_inci_report_record($report_id);
    $report_no = $inci_report_record["report_no"];
    $registrant_id = $inci_report_record["registrant_id"];
}
else
{
    $report_no = "";
    $registrant_id = get_emp_id($con,$session,$fname);
}
$smarty->assign("report_no", $report_no);
$smarty->assign("registrant_id", $registrant_id);

//メールヘッダー
if ($exclude_mail_sender_flg){
    // 報告書を編集、評価時、送信者本人をメール送信の宛先に含めない 20141008
    if ($gamen_mode == "update" || $gamen_mode == "analysis") {
        $sender_emp_id = get_emp_id($con,$session,$fname);
        if (!empty($sender_emp_id)){
            foreach ($default_to_list as $to_key=>$to_item) {
                if ($sender_emp_id == $to_item["emp_id"]) {
                    unset($default_to_list[$to_key]);
                }
            }
            $default_to_list = array_values($default_to_list);

            foreach ($default_cc_list as $cc_key=>$cc_item) {
                if ($sender_emp_id = $cc_item["emp_id"]) {
                    unset($default_cc_list[$cc_key]);
                }
            }
            $default_cc_list = array_values($default_cc_list);
        }
    }
}
assign_mail_header_data($smarty,$session,$con,$fname,$registrant_class,$registrant_attribute,$registrant_dept,$registrant_room,$doctor_emp_id,$registrant_id,$default_subject,$default_to_list,$default_cc_list);

/*************************************************
 * 発生場所・詳細
 *************************************************/
foreach($grps[115]['easy_item_list'] as $item_id => $item_value) {
    $list = array();
    foreach($item_value['easy_list'] as $key => $list_value) {
        $list[ $list_value['easy_code'] ] = $list_value['easy_name'];
    }
    $grps_115[$item_id] = $list;
}
$smarty->assign("grps_115", $grps_115);

//20130630
//==================================================
//ファイル添付
//==================================================
$smarty->assign("file_attach", $fantol_file_attach);

//登録前添付ファイル
$arr_file = array();
foreach ($_COOKIE['hiyariFileEnc'] as $enc => $name) {
    array_push($arr_file, $name);
}
$smarty->assign('arr_file', $arr_file);

if($gamen_mode != "new")
{
    //new以外はreport_idは振られいているはず

    //登録済み添付ファイル
    $file_list = array();
    $file_list = $rep_obj->get_inci_file($_SESSION['easyinput']['report_id']);
    $smarty->assign('file_list', $file_list);

}
//がん研カスタマイズ
$report_title_wording = $sysConf->get('fantol.report.title.wording');
if($report_title_wording){
    $smarty->assign(report_title_wording,$report_title_wording);
}else{
    $smarty->assign(report_title_wording,"表題");
}


//==================================================
//表示
//==================================================
if ($design_mode == 1){
    $smarty->display("hiyari_easyinput_confirm.tpl");
}
else{
    $smarty->display("report_confirm.tpl");
}

//==================================================
//DB切断
//==================================================
pg_close($con);

//==================================================
// 実行終了
//==================================================
exit;
