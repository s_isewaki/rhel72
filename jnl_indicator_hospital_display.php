<?php
require_once('jnl_indicator_display.php');
require_once('jnl_indicator_hospital_excel.php');
class IndicatorHospitalDisplay extends IndicatorDisplay
{
    var $indicatorHospitalExcel;

    /**
     * コンストラクタ
     * @param  array $arr
     * @return void
     */
    function IndicatorHospitalDisplay($arr) {
        parent::IndicatorDisplay($arr);
    }

    function setIndicatorHospitalExcel()
    {
        $this->indicatorHospitalExcel = new IndicatorHospitalExcel(array('fname' => $this->fname, 'con' => $this->con));
    }

    function getIndicatorHospitalExcel()
    {
        if (!is_object($this->indicatorHospitalExcel)) { $this->setIndicatorHospitalExcel(); }
        return $this->indicatorHospitalExcel;
    }

    /**
     * 各画面に応じた表示データを出力する
     * @param  $no
     * @param  $dataArr
     * @param  $plantArr
     * @param  $arr
     * @param  $dateArr
     * @return string
     */
    function getDataDisplayByNo($no, $dataArr, $plantArr, $arr, $optionArr)
    {
        $displayData = false;
        switch ($no) {
            case '': #
//                $displayData = $this->getPatientDailyFacilities($dataArr, $plantArr, $arr, $optionArr);
                break;

            default:
                break;
        }
        return $displayData;
    }

    function outputExcelData($no, $dataArr, $plantArr, $arr, $optionArr=null) {
        $excelData = null;

        /* @var $indicatorHospitalExcel IndicatorHospitalExcel */
        $indicatorHospitalExcel = $this->getIndicatorHospitalExcel();

        switch ($no) {
            default:
                $excelData = parent::outputExcelData($no, $dataArr, $plantArr, $arr, $optionArr);
                break;

        }

        return $excelData;
    }

    /**
     * (non-PHPdoc)
     * @see IndicatorDisplay#checkCellDataStatus($type, $arr)
     */
    function checkCellDataStatus($type, $arr)
    {
        $rtn           = 3;
        $startMonth    = $this->getFiscalYearStartMonth();

        switch ($type) {
            default:
                parent::checkCellDataStatus($type, $arr);
                break;
        }

        return $rtn;
    }
}