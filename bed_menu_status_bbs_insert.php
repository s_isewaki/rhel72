<?
require_once("about_comedix.php");

if ($post_id == "") {
	$post_id = "0";
	$ret_url = "bed_menu_status_bbs_post_register.php";
} else {
	$ret_url = "bed_menu_status_bbs_register.php";
}
?>
<body>
<form name="items" action="<? echo($ret_url); ?>" method="post">
<input type="hidden" name="bbs_title" value="<? echo($bbs_title) ?>">
<input type="hidden" name="bbs" value="<? echo(h($bbs)) ?>">
<?
foreach ($filename as $tmp_filename) {
	echo("<input type=\"hidden\" name=\"filename[]\" value=\"$tmp_filename\">\n");
}
foreach ($file_id as $tmp_file_id) {
	echo("<input type=\"hidden\" name=\"file_id[]\" value=\"$tmp_file_id\">\n");
}
?>
<input type="hidden" name="session" value="<? echo($session) ?>">
<input type="hidden" name="theme_id" value="<? echo($theme_id) ?>">
<input type="hidden" name="post_id" value="<? echo($post_id); ?>">
<input type="hidden" name="sort" value="<? echo($sort) ?>">
<input type="hidden" name="show_content" value="<? echo($show_content) ?>">
<input type="hidden" name="term" value="<? echo($term) ?>">
<input type="hidden" name="back" value="t">
</form>
<?
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($bbs_title == "") {
	echo("<script type=\"text/javascript\">alert('タイトルを入力してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (strlen($bbs_title) > 100) {
	echo("<script type=\"text/javascript\">alert('タイトルが長すぎます。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if ($bbs == "") {
	echo("<script type=\"text/javascript\">alert('内容を入力してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

// 添付ファイルの確認
if (!is_dir("bbs")) {
	mkdir("bbs", 0755);
}
if (!is_dir("bbs/bed")) {
	mkdir("bbs/bed", 0755);
}
if (!is_dir("bbs/bed/tmp")) {
	mkdir("bbs/bed/tmp", 0755);
}
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$ext = strrchr($filename[$i], ".");

	$tmp_filename = "bbs/bed/tmp/{$session}_{$tmp_file_id}{$ext}";
	if (!is_file($tmp_filename)) {
		echo("<script language=\"javascript\">alert('添付ファイルが削除された可能性があります。\\n再度添付してください。');</script>");
		echo("<script language=\"javascript\">document.items.submit();</script>");
		exit;
	}
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 投稿IDを採番
$sql = "select max(bbs_id) from bedbbs where bbsthread_id = '$theme_id'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$bbs_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

// 投稿情報を登録
$reg_date_time = date("YmdHi");
$sql = "insert into bedbbs (emp_id, bbs_id, date, bbsthread_id, bbs_towhich_id, bbs_title, bbs) values (";
$content = array($emp_id, $bbs_id, $reg_date_time, $theme_id, $post_id, $bbs_title, $bbs);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 添付ファイル情報を作成
$no = 1;
foreach ($filename as $tmp_filename) {
	$sql = "insert into bedbbsfile (bbsthread_id, bbs_id, bbsfile_no, bbsfile_name) values (";
	$content = array($theme_id, $bbs_id, $no, $tmp_filename);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$no++;
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 添付ファイルの移動
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$tmp_filename = $filename[$i];
	$tmp_fileno = $i + 1;
	$ext = strrchr($tmp_filename, ".");

	$tmp_filename = "bbs/bed/tmp/{$session}_{$tmp_file_id}{$ext}";
	copy($tmp_filename, "bbs/bed/{$theme_id}_{$bbs_id}_{$tmp_fileno}{$ext}");
}
foreach (glob("bbs/bed/tmp/{$session}_*.*") as $tmpfile) {
	unlink($tmpfile);
}

// 投稿一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'bed_menu_status_bbs.php?session=$session&theme_id=$theme_id&sort=$sort&show_content=$show_content&term=$term';</script>");
?>
</body>
