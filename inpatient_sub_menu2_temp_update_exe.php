<?
require_once("about_session.php");
require_once("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($sel, "begin");

// 仮抑え情報をDELETE〜INSERT
$sql = "delete from bedtemp";
$cond = "where bldg_cd = $bldg_cd and ward_cd = $ward_cd and ptrm_room_no = $room_no and inpt_bed_no = '$bed_no'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($sel, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if ($reserved == "") {$reserved = "f";}
$sql = "insert into bedtemp (bldg_cd, ward_cd, ptrm_room_no, inpt_bed_no, reserved, reason) values (";
$content = array($bldg_cd, $ward_cd, $room_no, $bed_no, $reserved, $reason);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($sel, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// トランザクションをコミット
pg_query($sel, "commit");

// データベース接続を閉じる
pg_close($con);

// 親画面をリフレッシュ
echo("<script type=\"text/javascript\">opener.location.reload(); self.close();</script>");
?>
