<!-- ************************************************************************ -->
<!-- 勤務シフト作成　当直用共通ＣＬＡＳＳ -->
<!-- ************************************************************************ -->

<?
//ini_set("display_errors","1");
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");

require_once("duty_shift_common_class.php");

class duty_shift_duty_common_class
{
	var $file_name;	// 呼び出し元ファイル名
	var $_db_con;	// DBコネクション
	var $obj;		// 勤務シフト作成共通クラス

	/*************************************************************************/
	// コンストラクタ
	// @param object $con DBコネクション
	// @param string $fname 画面名
	/*************************************************************************/
	function duty_shift_duty_common_class($con, $fname)
	{
		// 呼び出し元ファイル名を保持
		$this->file_name = $fname;
		// DBコネクションを保持
		$this->_db_con = $con;
		// クラス new
		$this->obj = new duty_shift_common_class($con, $fname);
	}

//-------------------------------------------------------------------------------------------------------------------------
// 関数一覧（新規作成：固定値）
//-------------------------------------------------------------------------------------------------------------------------
	/*************************************************************************/
	// 当直情報取得
	// @param	なし
	//
	// @return	$data	取得情報
	/*************************************************************************/
	function get_duty_pattern_array() {
		$str = mb_substr($this->obj->duty_or_oncall_str, 0, 1);
		$data = array(
					0 => array("id" => "1", "name" => "有り", "font_name" => $str, font_color => "#000000", back_color => "" ),
					1 => array("id" => "2", "name" => "無し", "font_name" => "_", font_color => "", back_color => "#C0C0C0" )
				);

		return $data;
	}
//-------------------------------------------------------------------------------------------------------------------------
// 関数一覧（表示：ボタン）
//-------------------------------------------------------------------------------------------------------------------------
	/*************************************************************************/
	// ボタン（勤務シフト作成｜当直画面用）
	/*************************************************************************/
	function showButton($create_flg, $plan_results_flg, $finish_flg) {
		$disabled_flg = "disabled";
		if ($create_flg == "1") { $disabled_flg = ""; }
//		$wk_style = "WIDTH: 80px; HEIGHT: 22px;";

		echo("<tr height=\"22\"> \n");
		echo("<td align=\"left\" width=\"\">\n");
		//-------------------------------------------------------------------------------
		//応援追加
		//-------------------------------------------------------------------------------
//		echo("<input $disabled_flg type=\"button\" value=\"職員追加\" onclick=\"staffAdd();\" style=\"$wk_style\">\n");
		//-------------------------------------------------------------------------------
		//下書き保存
		//登録
		//-------------------------------------------------------------------------------
		echo("<input $disabled_flg type=\"button\" value=\"下書き保存\" onclick=\"editData(1);\" style=\"$wk_style\">\n");
		echo("<input $disabled_flg type=\"button\" value=\"登録\" onclick=\"editData(2);\" style=\"$wk_style\">&nbsp; \n");
		//-------------------------------------------------------------------------------
		//予定／実績表示
		//-------------------------------------------------------------------------------
		if ($plan_results_flg == "2") {
			echo("<input type=\"button\" value=\"予定表示\" onclick=\"showPlanResults(0);\" style=\"$wk_style\">\n");
		} else {
			echo("<input type=\"button\" value=\"予実績表示\" onclick=\"showPlanResults(2);\" style=\"$wk_style\">&nbsp; \n");
		}
		echo("</td>\n");
		//-------------------------------------------------------------------------------
		//登録済み
		//-------------------------------------------------------------------------------
		if ($finish_flg != "") {
			echo("<td align=\"left\" width=\"\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">&nbsp;$finish_flg</font></td>");
		}

		echo("</tr>\n");
	}

//-------------------------------------------------------------------------------------------------------------------------
// 関数一覧（表示：画面）
//-------------------------------------------------------------------------------------------------------------------------
	/*************************************************************************/
	//
	// シフトグループ（病棟）名、年、月
	//
	/*************************************************************************/
	function showHead($session, $fname,
					$group_show_flg,			//シフトグループ表示フラグ（１：表示）
					$group_id,					//勤務シフトグループＩＤ
					$group_array,				//勤務シフトグループ情報
					$duty_yyyy,					//勤務年
					$duty_mm,					//勤務月
					$url,						//遷移先URL
					$url_option					//URLオプション
	) {

//		echo("<tr height=\"22\"> \n");
//		echo("</tr>\n");
		//-------------------------------------------------------------------------------
		// 年
		//-------------------------------------------------------------------------------
		echo("<tr height=\"22\"> \n");
		echo("<td width=\"100%\" align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">&nbsp;&nbsp; \n");
		// 年
		$wk_next = $duty_yyyy + 1;
		$wk_back = $duty_yyyy - 1;
		echo("<a href=\"$url?session=$session&group_id=$group_id&duty_yyyy=$wk_back&duty_mm=$duty_mm$url_option\">←</a>&nbsp; \n");
		echo("{$duty_yyyy}年&nbsp; \n");
		echo("<a href=\"$url?session=$session&group_id=$group_id&duty_yyyy=$wk_next&duty_mm=$duty_mm$url_option\">→</a>&nbsp; \n");
		echo("</font>\n");
		//-------------------------------------------------------------------------------
		// シフトグループ（病棟）名
		//-------------------------------------------------------------------------------
//		echo("</td> <td width=\"25%\" align=\"left\" nowrap> \n");
		echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">&nbsp;&nbsp;シフトグループ(病棟)名");
		if ($group_show_flg == "1") {
			for ($k=0; $k<count($group_array); $k++) {
				$wk_id = $group_array[$k]["group_id"];
				$wk_name = $group_array[$k]["group_name"];
				if ($group_id == $wk_id) {
					break;
				}
			}
			echo("&nbsp;&nbsp;$wk_name");
		} else {
			echo("<select name=\"group_id\" onchange=\"this.form.action = '$fname'; document.mainform.duty_only_disp_flg.value = ''; this.form.submit();\">");
			for ($k=0; $k<count($group_array); $k++) {
				$wk_id = $group_array[$k]["group_id"];
				$wk_name = $group_array[$k]["group_name"];
				echo("<option  value=\"$wk_id\"");
				if ($group_id == $wk_id) {
					echo(" selected");
				}
				echo(">$wk_name \n");
			}
			echo("</select>\n");
		}
		echo("</font>&nbsp;\n");
		// 当直のみ表示ボタン
//		echo("</td><td width=\"60%\" align=\"left\"> \n");
		// 当直予定表参照では無効化する
		if (strpos($fname,"atdbk_duty_shift_duty.php") !== FALSE) {
			$wk_disabled = " disabled";
		} else {
			$wk_disabled = "";
		}
		echo("<input type=\"button\" value=\"{$this->obj->duty_or_oncall_str}のみ表示\" onclick=\"showDutyOnly();\" $wk_disabled> \n");

		echo("</td>\n");
		echo("</tr> \n");
		//-------------------------------------------------------------------------------
		// 月
		//-------------------------------------------------------------------------------
		echo("<tr height=\"22\"> \n");
		echo("<td width=\"100%\" align=\"left\" colspan=\"3\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">&nbsp;&nbsp; \n");
		$prev_mm = $duty_mm - 1;
		$prev_yyyy = $duty_yyyy;
		if ($prev_mm <= 0) {
			$prev_mm = 12;
			$prev_yyyy = $duty_yyyy - 1;
		}
		$next_mm = $duty_mm + 1;
		$next_yyyy = $duty_yyyy;
		if ($next_mm >= 13) {
			$next_mm = 1;
			$next_yyyy = $duty_yyyy + 1;
		}
		echo("<a href=\"$url?session=$session&group_id=$group_id&duty_yyyy=$prev_yyyy&duty_mm=$prev_mm$url_option\">←</a>&nbsp; \n");
		for ($i=1;$i<=12;$i++) {
			if ((int)$duty_mm == (int)$i) {
				$wk = $i . "月&nbsp; \n";
				echo($wk);
			} else {
				echo("<a href=\"$url?session=$session&group_id=$group_id&duty_yyyy=$duty_yyyy&duty_mm=$i$url_option\"> $i 月</a>&nbsp; \n");
			}
		}
		echo("<a href=\"$url?session=$session&group_id=$group_id&duty_yyyy=$next_yyyy&duty_mm=$next_mm$url_option\">→</a>&nbsp; \n");
		echo("</font></td>\n");
		echo("</tr>\n");
	}
	/*************************************************************************/
	//
	// 見出し（週段）
	//
	/*************************************************************************/
	function showTitleSub1(
						$duty_yyyy,			//勤務年
						$duty_mm,			//勤務月
						$day_cnt,			//日数
						$edit_start_day,	//入力表示の開始日（位置）
						$edit_end_day,		//入力表示の終了日（位置）
						$week_array,		//週情報
						$refer_flg,			//参照フラグ：（１：参照）
						$duty_only_disp_flg	//当直のみ表示フラグ（１：ボタン押下 ２：再表示）
	){
		//-------------------------------------------------------------------------------
		//週段
		//-------------------------------------------------------------------------------
	//	echo("<tr height=\"22\"> \n");
		echo("<tr>\n");
		echo("<td width=\"20\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"></font></td>\n");
		// シフトグループ名
		if ($duty_only_disp_flg != "") {
			echo("<td width=\"100\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"></font></td> \n");
		}
		echo("<td width=\"100\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"></font></td>\n");
		echo("<td width=\"70\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"></font></td>\n");

		//勤務状況（日）
		$wk_cnt = 1;
		$wk_idx = 1;
		for ($k=1; $k<=$day_cnt; $k++) {
			//表示チェック
			$wk_show_flg = "";
			if ((($edit_start_day <= $k) && ($edit_end_day >= $k)) ||
				($edit_start_day == $edit_end_day)) {
				$wk_show_flg = "1";
			}
			//表示
			if ($wk_show_flg == "1") {
				$wk_td_id = "week" . $k;
				$wk_id_img = "img" . $wk_cnt;
				//週の最後日を算出
				for ($m=$k; $m<=$day_cnt; $m++) {
					if (($week_array[$m]["name"] == "日") || ($m == $day_cnt)){
						break;
					}
				}
				//表示／入力で幅変更
				$wk_width = "20";
				if (($edit_start_day <= $k) && ($edit_end_day >= $k)) {
					$wk_width = "80";
				}
				//１週間入力指定の場合、他の日は非表示
				if (($week_array[$k]["name"] == "月") || ($k == 1)){
					echo("<td id=\"$wk_td_id\" width=\"$wk_width\" align=\"left\" style=\"cursor:pointer;\">");
					if ($wk_width == 80) {
						echo("<img src=\"img/spacer.gif\" width=\"11\" height=\"11\" id=\"$wk_id_img\" class=\"close\" onclick=\"showEdit($k, $m);\">");
					} else {
						echo("<img src=\"img/spacer.gif\" width=\"11\" height=\"11\" id=\"$wk_id_img\" class=\"open\" onclick=\"showEdit($k, $m);\">");
					}
					$wk_idx = 0;
					$wk_cnt++;
				} else {
					echo("<td id=\"$wk_td_id\" width=\"$wk_width\" align=\"left\">");
				}
				echo("</td>\n");
			}
			$wk_idx++;
		}

		//合計
		echo("<td width=\"30\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"></font></td>\n");
		echo("</tr>\n");
	}
	/*************************************************************************/
	//
	// 見出し（日段）
	//
	/*************************************************************************/
	function showTitleSub2(
						$duty_yyyy,			//勤務年
						$duty_mm,			//勤務月
						$day_cnt,			//日数
						$edit_start_day,	//入力表示の開始日（位置）
						$edit_end_day,		//入力表示の終了日（位置）
						$week_array,		//週情報
						$calendar_array,	//カレンダー情報
						$refer_flg,			//参照フラグ：（１：参照）
						$duty_only_disp_flg	//当直のみ表示フラグ（１：ボタン押下 ２：再表示）
	){
		//-------------------------------------------------------------------------------
		//日段
		//-------------------------------------------------------------------------------
	//	echo("<tr height=\"22\"> \n");
		echo("<tr> \n");
		echo("<td width=\"20\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">番号</font></td> \n");
		// シフトグループ名
		if ($duty_only_disp_flg != "") {
			echo("<td width=\"100\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">シフトグループ</font></td> \n");
		}
		echo("<td width=\"100\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">氏名</font></td> \n");
		echo("<td width=\"70\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">職種</font></td> \n");

		//勤務状況（日）
		for ($k=1; $k<=$day_cnt; $k++) {
			//表示チェック
			$wk_show_flg = "";
			if ((($edit_start_day <= $k) && ($edit_end_day >= $k)) ||
				($edit_start_day == $edit_end_day)) {
				$wk_show_flg = "1";
			}
			//表示／入力で幅変更
			$wk_width = "20";
			if (($edit_start_day <= $k) && ($edit_end_day >= $k)) {
				$wk_width = "80";
			}
			//表示
			if ($wk_show_flg == "1") {
				$wk_td_id = "day" . $k;
				$wk_date = $calendar_array[$k-1]["date"];
				$wk_dd = (int)substr($wk_date,6,2);
				echo("<td id=\"$wk_td_id\" width=\"$wk_width\" align=\"center\" colspan=\"1\" onClick=\"showEdit($k, $k);\" style=\"cursor:pointer;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_dd</font></td>\n");
			}
		}

		//チェック結果
		if ($check_flg == "1"){
			echo("<td width=\"20\" align=\"center\" rowspan=\"2\">警告</td> \n");
		}

		//合計
		echo("<td width=\"30\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">合計</font></td> \n");
		//-------------------------------------------------------------------------------
		//曜日段
		//-------------------------------------------------------------------------------
	//	echo("<tr height=\"22\">\n");
		echo("<tr>\n");
		//勤務状況（曜日）
		for ($k=1; $k<=$day_cnt; $k++) {
			//表示チェック
			$wk_show_flg = "";
			if ((($edit_start_day <= $k) && ($edit_end_day >= $k)) ||
				($edit_start_day == $edit_end_day)) {
				$wk_show_flg = "1";
			}
			//表示
			if ($wk_show_flg == "1") {
				//TDタグID
				$wk_td_id = "day_of_week" . $k;
				//表示／入力で幅変更
				$wk_width = "20";
				if (($edit_start_day <= $k) && ($edit_end_day >= $k)) {
					$wk_width = "80";
				}
				//土、日、祝日の文字色変更
				$wk_color = "";
				if ($week_array[$k]["name"] == "土"){
					$wk_color = "#0000ff";
				} else if ($week_array[$k]["name"] == "日") {
					$wk_color = "#ff0000";
				} else {
					if (($calendar_array[$k-1]["type"] == "4") ||
						($calendar_array[$k-1]["type"] == "5") ||
						($calendar_array[$k-1]["type"] == "6")) {
						$wk_color = "#ff0000";
					}
				}
				//表示
				$wk_memo = trim($calendar_array[$k-1]["memo"]);
				$wk_name = $week_array[$k]["name"];
				echo("<td id=\"$wk_td_id\" width=\"$wk_width\" align=\"center\" colspan=\"1\" onClick=\"showEdit($k, $k);\" onmouseover=\"showMemo($k,'$wk_memo');\" onmouseout=\"hideMsg();\" style=\"cursor:pointer;\">");
				echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=$wk_color>$wk_name</font></td> \n");
			}
		}
		echo("</tr> \n");
	}
	/*************************************************************************/
	//
	// 表データ
	//
	/*************************************************************************/
	function showList(
					$duty_array,			//勤務シフト情報
					$plan_results_flg,		//予実表示フラグ（２：実績も表示）
					$day_cnt,				//日数
					$edit_start_day,		//入力表示の開始日（位置）
					$edit_end_day,			//入力表示の終了日（位置）
					$week_array,			//週情報
					$start_cnt,				//表示スタッフ開始位置
					$end_cnt,				//表示スタッフ終了位置
					$highlight_flg,			//強調表示フラグ（１：強調表示）
					$refer_flg,				//参照フラグ：（１：参照）
					$duty_only_disp_flg,	//当直のみ表示フラグ（１：ボタン押下 ２：再表示）
					$group_array			//シフトグループ情報
	){
		//-------------------------------------------------------------------------------
		// 当直パターン取得
		//-------------------------------------------------------------------------------
		$duty_pattern =	$this->get_duty_pattern_array();
		//-------------------------------------------------------------------------------
		// 表
		//-------------------------------------------------------------------------------
		$cnt = 1;
		for ($i=$start_cnt; $i<$end_cnt; $i++) {
			//-------------------------------------------------------------------------------
			// 予定データ
			//-------------------------------------------------------------------------------
	//		echo("<tr height=\"22\">\n");
			echo("<tr>\n");
			$span = "1";
			if ($plan_results_flg == "2") {
				$span = "2";
			}
			//-------------------------------------------------------------------------------
			// 表示順
			//-------------------------------------------------------------------------------
			//TDタグID
			$wk_td_id = "no" . $cnt;
			$wk = $duty_array[$i]["no"];
			echo("<td id=\"$wk_td_id\" width=\"20\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td>\n");
			// シフトグループ名
			if ($duty_only_disp_flg != "") {
				$wk_td_id = "group" . $cnt;
				echo("<td id=\"$wk_td_id\" width=\"100\" align=\"center\" rowspan=\"$span\" bgcolor=\"$wk_color\">");
				for ($j=0;$j<count($group_array);$j++) {
					if ($duty_array[$i]["group_id"] == $group_array[$j]["group_id"]) {
						$wk = $group_array[$j]["group_name"];
						break;
					}
				}
				echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td>\n");
			}
			//-------------------------------------------------------------------------------
			// 氏名
			//-------------------------------------------------------------------------------
			//TDタグID
			$wk_td_id = "name" . $cnt;
			// 氏名の背景色
			$wk_color = "";
			// 重複スタッフの場合
			if ($duty_array[$i]["other_staff_flg"] == "1") {
				//他病棟スタッフ
				$wk_color = "#dce8bb";
			}
			// 氏名
			echo("<td id=\"$wk_td_id\" width=\"100\" align=\"center\" rowspan=\"$span\" bgcolor=\"$wk_color\">");
			$wk = $duty_array[$i]["staff_name"];
			echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td>\n");
			//-------------------------------------------------------------------------------
			// 職種
			//-------------------------------------------------------------------------------
			$wk_td_id = "job" . $cnt;	//TDタグID
			$wk = $duty_array[$i]["job_name"];
			echo("<td id=\"$wk_td_id\" width=\"70\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td>\n");
			//-------------------------------------------------------------------------------
			// 勤務状況
			//-------------------------------------------------------------------------------
			for ($k=1; $k<=$day_cnt; $k++) {
				//表示チェック
				$wk_show_flg = "";
				if ((($edit_start_day <= $k) && ($edit_end_day >= $k)) ||
					($edit_start_day == $edit_end_day)) {
					$wk_show_flg = "1";
				}
				//対象データの場合
				if ($wk_show_flg == "1") {
					$wk_flg = "";
					if (($edit_start_day <= $k) && ($edit_end_day >= $k)) {
						$wk_flg = "1";
					}
					//TDタグID
					$wk_td_id = "data" . $cnt . "_" . $k;
					//表示／入力で幅変更
					$wk_width = "20";
					if (($edit_start_day <= $k) && ($edit_end_day >= $k)) {
						$wk_width = "80";
					}
					//入力又は表示
					if ($wk_flg == "1") {
						//当直（選択入力）
						echo("<td id=\"$wk_td_id\" width=\"$wk_width\" align=\"center\" colspan=\"1\">");
						echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
						$wk = "night_duty_$i" . "_" . $k;
						echo("<select name=\"$wk\">");
						echo("<option value=\"\">__ \n");
						for ($m=0; $m<count($duty_pattern); $m++) {
							$wk_id = $duty_pattern[$m]["id"];
							$wk_name = $duty_pattern[$m]["name"];
							echo("<option value=\"$wk_id\"");
							if ($duty_array[$i]["night_duty_$k"] == $wk_id) {
								echo(" selected");
							}
							echo(">$wk_name \n");
						}
						echo("</select> \n");
					} else {
						//表示
						$wk_name = "&nbsp;"; // "_" -> " "
						$wk_font_color = "";
						$wk_back_color = "";
						for ($m=0; $m<count($duty_pattern); $m++) {
						if ($duty_array[$i]["night_duty_$k"] == $duty_pattern[$m]["id"]) {
							//表示文字名
							$wk_name = $duty_pattern[$m]["font_name"];
							//文字、背景色
							$wk_font_color = $duty_pattern[$m]["font_color"];
							$wk_back_color = $duty_pattern[$m]["back_color"];
						}
						}
						//当直（表示）
						echo("<td id=\"$wk_td_id\" width=\"$wk_width\" align=\"center\" colspan=\"1\" bgcolor=\"$wk_back_color\" onClick=\"showEdit($k, $k);\" onmouseover=\"highlightCells(this.className,$cnt,$k);this.style.cursor = '';\" onmouseout=\"dehighlightCells(this.className,$cnt,$k);this.style.cursor = '';\">");
						echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"$wk_font_color\" >$wk_name");
					}
					echo("</font></td> \n");
				}
			}
			//-------------------------------------------------------------------------------
			//個人の合計
			//-------------------------------------------------------------------------------
			$wk_td_id = "sum_gyo" . $cnt;	//TDタグID
			$wk_sum = 0;
			for ($k=1; $k<=$day_cnt; $k++) {
			if ($duty_array[$i]["night_duty_$k"] == "1") {
				$wk_sum += 1;
			}
			}
			echo("<td id=\"$wk_td_id\" width=\"30\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_sum</font></td>\n");
			echo("</tr> \n");
			//-------------------------------------------------------------------------------
			// 実績データ
			//-------------------------------------------------------------------------------
			if ($plan_results_flg == "2") {

				$cnt++;

				echo("<tr height=\"\"> \n");
				//勤務状況（実績）
				for ($k=1; $k<=$day_cnt; $k++) {
					//表示チェック
					$wk_show_flg = "";
					if ((($edit_start_day <= $k) && ($edit_end_day >= $k)) ||
						($edit_start_day == $edit_end_day)) {
						$wk_show_flg = "1";
					}
					//表示
					if ($wk_show_flg == "1") {
						$wk_flg = "";
						if (($edit_start_day <= $k) && ($edit_end_day >= $k)) {
							$wk_flg = "1";
						}
						//TDタグID
						$wk_td_id = "data" . $cnt . "_" . $k;
						//表示／入力で幅変更
						$wk_width = "20";
						if (($edit_start_day <= $k) && ($edit_end_day >= $k)) {
							$wk_width = "80";
						}

						//表示
						$wk_name = "&nbsp;"; // "_" -> " "
						$wk_font_color = "";
						$wk_back_color = "";
						for ($m=0; $m<count($duty_pattern); $m++) {
						if ($duty_array[$i]["rslt_night_duty_$k"] == $duty_pattern[$m]["id"]) {
							//表示文字名
							$wk_name = $duty_pattern[$m]["font_name"];
							//文字、背景色
							$wk_font_color = $duty_pattern[$m]["font_color"];
							$wk_back_color = $duty_pattern[$m]["back_color"];
						}
						}
						//当直（表示）
						echo("<td id=\"$wk_td_id\" width=\"$wk_width\" align=\"center\" colspan=\"1\" bgcolor=\"$wk_back_color\" onClick=\"showEdit($k, $k);\">");
						echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"$wk_font_color\" >$wk_name");
					}
				}
				echo("</tr>\n");
			}

			$cnt++;
		}
	}
	/*************************************************************************/
	//
	// 列計
	//
	/*************************************************************************/
	function showTitleRetu(
						$duty_array,		//当直情報
						$day_cnt,			//日数
						$edit_start_day,	//入力表示の開始日（位置）
						$edit_end_day,		//入力表示の終了日（位置）
						$week_array,		//週情報
						$data_cnt,			//スタッフ数
						$refer_flg,			//参照フラグ：（１：参照）
						$duty_only_disp_flg	//当直のみ表示フラグ（１：ボタン押下 ２：再表示）
	){
//		echo("<tr height=\"22\"> \n");
		echo("<tr>\n");
		echo("<td width=\"20\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">　</font></td>\n");	// 番号（ブランク）
		// シフトグループ（ブランク）
		if ($duty_only_disp_flg != "") {
			echo("<td width=\"100\" align=\"right\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"></font></td>\n");
		}
		echo("<td width=\"100\" align=\"right\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">合計</font></td>\n");
		//-------------------------------------------------------------------------------
		// 総合計
		//-------------------------------------------------------------------------------
		$wk_cnt = 0;
		for ($i=0; $i<$data_cnt; $i++) {
		for ($k=1; $k<=$day_cnt; $k++) {
		if ($duty_array[$i]["night_duty_$k"] == "1") {
			$wk_cnt += 1;
		}
		}
		}
		echo("<td width=\"70\" align=\"right\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_cnt</font></td>\n");
		//-------------------------------------------------------------------------------
		// 日ごとの合計
		//-------------------------------------------------------------------------------
		for ($k=1; $k<=$day_cnt; $k++) {
			//表示チェック
			$wk_show_flg = "";
			if ((($edit_start_day <= $k) && ($edit_end_day >= $k)) ||
				($edit_start_day == $edit_end_day)) {
				$wk_show_flg = "1";
			}
			//表示
			if ($wk_show_flg == "1") {
				//TDタグID
				$wk_td_id = "summary" . $k;
				//表示／入力で幅変更
				$wk_width = "20";
				if (($edit_start_day <= $k) && ($edit_end_day >= $k)) {
					$wk_width = "80";
				}
				//表示
				$wk_cnt = 0;
				for ($i=0; $i<$data_cnt; $i++) {
				if ($duty_array[$i]["night_duty_$k"] == "1") {
					$wk_cnt += 1;
				}
				}
				echo("<td id=\"$wk_td_id\" width=\"$wk_width\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_cnt</font></td>\n");
			}
		}
		echo("</tr>\n");
	}

//-------------------------------------------------------------------------------------------------------------------------
// 関数一覧（ＨＩＤＤＥＮ）
//-------------------------------------------------------------------------------------------------------------------------
	/*************************************************************************/
	//
	// 勤務シフ作成ト／希望／実績共通１
	//
	/*************************************************************************/
	function showHidden_1(
						$data_cnt,				//スタッフ数
						$day_cnt,				//日数
						$plan_results_flg,		//予実表示フラグ（２，３：実績も表示）
						$edit_start_day,		//入力表示の開始日（位置）
						$edit_end_day			//入力表示の終了日（位置）
	){
		echo("<input type=\"hidden\" name=\"data_cnt\" value=\"$data_cnt\">\n");
		echo("<input type=\"hidden\" name=\"day_cnt\" value=\"$day_cnt\">\n");
		echo("<input type=\"hidden\" name=\"plan_results_flg\" value=\"$plan_results_flg\">\n");

		echo("<input type=\"hidden\" name=\"edit_start_day\" value=\"$edit_start_day\">\n");
		echo("<input type=\"hidden\" name=\"edit_end_day\" value=\"$edit_end_day\">\n");
	}
	/*************************************************************************/
	//
	// 勤務シフ作成ト／希望／実績共通２
	//
	/*************************************************************************/
	function showHidden_2(
						$duty_array,		//勤務シフト情報
						$calendar_array,	//カレンダー情報
						$session,			//セッション
						$day_cnt,			//日数
						$group_id,			//勤務シフトグループＩＤ
						$duty_yyyy,			//勤務年
						$duty_mm,			//勤務月
						$edit_start_day,	//入力表示の開始日（位置）
						$edit_end_day,		//入力表示の終了日（位置）
						$draft_flg,			//下書きフラグ（１：下書き）
						$refer_flg			//参照フラグ：（１：参照）
	) {
		echo("<input type=\"hidden\" name=\"session\" value=\"$session\">\n");

		echo("<input type=\"hidden\" name=\"duty_yyyy\" value=\"$duty_yyyy\">\n");
		echo("<input type=\"hidden\" name=\"duty_mm\" value=\"$duty_mm\">\n");

		echo("<input type=\"hidden\" name=\"cause_group_id\" value=\"$group_id\">\n");
		echo("<input type=\"hidden\" name=\"cause_duty_yyyy\" value=\"$duty_yyyy\">\n");
		echo("<input type=\"hidden\" name=\"cause_duty_mm\" value=\"$duty_mm\">\n");

		echo("<input type=\"hidden\" name=\"draft_flg\" value=\"$draft_flg\">\n");

		echo("<input type=\"hidden\" name=\"refer_flg\" value=\"$refer_flg\">\n");

		//-------------------------------------------------------------------------------
		/// 勤務当直データ
		//-------------------------------------------------------------------------------
		for ($i=0; $i<count($duty_array); $i++) {
			//-------------------------------------------------------------------------------
			//スタッフ
			//-------------------------------------------------------------------------------
			$wk = $duty_array[$i]["staff_id"];
			echo("<input type=\"hidden\" name=\"staff_id[$i]\" value=$wk >\n");
			//-------------------------------------------------------------------------------
			//予定当直
			//-------------------------------------------------------------------------------
			$wk_rslt = "";
			for ($k=1; $k<=$day_cnt; $k++) {
				//-------------------------------------------------------------------------------
				//予定当直
				//-------------------------------------------------------------------------------
				if (($edit_start_day <= $k) && ($edit_end_day >= $k)) {
					//入力データで既に設定されているため、ここでは未設定
				} else {
					$wk2 = $duty_array[$i]["night_duty_$k"];
					$wk3 = "night_duty_$i" . "_" . $k;
					echo("<input type=\"hidden\" name=\"$wk3\" value=$wk2 >\n");
				}
				//-------------------------------------------------------------------------------
				//実績設定
				//-------------------------------------------------------------------------------
				if ($k > 1) {
					$wk_rslt .= ",";
				}
				$wk_rslt .= $duty_array[$i]["rslt_night_duty_$k"];
			}
			//-------------------------------------------------------------------------------
			//実績当直
			//-------------------------------------------------------------------------------
			echo("<input type=\"hidden\" name=\"rslt_night_duty[$i]\" value=$wk_rslt >\n");
		}
		//-------------------------------------------------------------------------------
		/// 年月日（カレンダーより）
		//-------------------------------------------------------------------------------
		$k = count($calendar_array) - 1;
		$duty_start_date = $calendar_array[0]["date"];
		$duty_end_date = $calendar_array[$k]["date"];
		echo("<input type=\"hidden\" name=\"duty_start_date\" value=$duty_start_date >\n");
		echo("<input type=\"hidden\" name=\"duty_end_date\" value=$duty_end_date >\n");
	}

//-------------------------------------------------------------------------------------------------------------------------
// 関数一覧（新規作成：ＤＢ読み込み）
//-------------------------------------------------------------------------------------------------------------------------
	/*************************************************************************/
	// 当直管理者情報取得
	// @param	$data_st	ＤＢ(stmst)より役職情報取得
	// @param	$data_job	ＤＢ(jobmst)より職種情報取得
	// @param	$data_emp	ＤＢ(empmst)より職員情報取得
	//
	// @return	$data	取得情報
	/*************************************************************************/
	function get_duty_shift_duty_person_array($data_st, $data_job, $data_emp) {
		//-------------------------------------------------------------------
		//勤務シフトグループ情報ＤＢより情報取得
		//-------------------------------------------------------------------
		$sql = "select * from duty_shift_duty_person";
		$cond = "where 1=1 ";
		$cond .= "order by no";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num = pg_numrows($sel);
		//-------------------------------------------------------------------
		//情報設定
		//-------------------------------------------------------------------
		$data = array();
		for($i=0;$i<$num;$i++){
			$wk_id = pg_result($sel,$i,"emp_id");	//当直管理者ID
			$wk_data = $this->obj->get_duty_shift_staff_one($wk_id, $data_st, $data_job, $data_emp);
			$data[$i]["no"] = $i + 1;
			$data[$i]["id"] = $wk_id;								//スタッフＩＤ（職員ＩＤ）
			$data[$i]["name"] = $wk_data["name"];					//スタッフ名称
			$data[$i]["job_id"] = $wk_data["job_id"];				//職種ＩＤ
			$data[$i]["job_name"] = $wk_data["job_name"];			//職種名
			$data[$i]["status"] = $wk_data["status"];				//役職
			$data[$i]["duty_years"] = $wk_data["duty_years"];		//勤務年数
		}
		return $data;
	}
///////////////////////////////////////////////////////////////
	/*************************************************************************/
	// 勤務当直報取得（勤務当直情報の編集設定）
	// @param	$sel				検索ＳＱＬ情報
	// @param	$num				検索件数
	// @param	$day_cnt			日数
	// @param	$data_st			ＤＢ(stmst)より役職情報取得
	// @param	$data_job			ＤＢ(jobmst)より職種情報取得
	// @param	$data_emp			ＤＢ(empmst)より職員情報取得
	// @param	$calendar_array		カレンダー情報
	// @param	$duty_only_disp_flg	当直のみ表示フラグ（１：ボタン押下 ２：再表示）
	//
	// @return	$wk_data2		取得情報
	/*************************************************************************/
	function get_duty_shift_duty_set($sel, $num, $day_cnt,
									$data_st, $data_job, $data_emp, $calendar_array, $duty_only_disp_flg) {
		//-------------------------------------------------------------------
		//勤務シフト情報の編集設定
		//-------------------------------------------------------------------
		$wk_data = array();
		for($i=0;$i<$num;$i++){
			//-------------------------------------------------------------------
			//ＤＢより取得値
			//-------------------------------------------------------------------
			$wk_data[$i]["staff_id"] = pg_result($sel,$i,"emp_id");
			$key_duty_date = trim(pg_result($sel,$i,"duty_date"));
			$key_night_duty = trim(pg_result($sel,$i,"night_duty"));
			//-------------------------------------------------------------------
			//職員DBより職員名設定
			//-------------------------------------------------------------------
			for ($k=0;$k<count($data_emp);$k++) {
			if ($wk_data[$i]["staff_id"]  == $data_emp[$k]["id"]) {
				$wk_data[$i]["staff_name"] = $data_emp[$k]["name"];
				break;
			}
			}
			//-------------------------------------------------------------------
			//勤務当直設定
			//-------------------------------------------------------------------
			$m=0;
			for ($p=0;$p<count($calendar_array);$p++) {
			if ($calendar_array[$p]["date"] == $key_duty_date) {
				$m = $p + 1;
				break;
			}
			}
			$wk_data[$i]["night_duty_$m"] = $key_night_duty;
		}
		//-------------------------------------------------------------------
		//取得データを職員毎に再設定
		//-------------------------------------------------------------------
		$k=0;
		$wk_staff_id = "";
		$wk_data2 = array();
		for ($i=0;$i<count($wk_data);$i++) {
			if ($i == 0) {
				$wk_staff_id = $wk_data[0]["staff_id"];
			} else if ($wk_staff_id != $wk_data[$i]["staff_id"]) {
				$wk_staff_id = $wk_data[$i]["staff_id"];
				$k++;
			}
			//職員データ設定
			$wk_data2[$k]["staff_id"] = $wk_data[$i]["staff_id"];
			$wk_data2[$k]["staff_name"] = $wk_data[$i]["staff_name"];
			//勤務当直
			for ($m=1;$m<=$day_cnt;$m++) {
				if ($wk_data[$i]["night_duty_$m"] != "") { $wk_data2[$k]["night_duty_$m"] = $wk_data[$i]["night_duty_$m"]; }
			}
			//職種DBより職種設定
			$data_staff_one = $this->obj->get_duty_shift_staff_one($wk_data2[$k]["staff_id"], $data_st, $data_job, $data_emp);
			$wk_data2[$k]["job_id"] = $data_staff_one["job_id"];
			$wk_data2[$k]["job_name"] = $data_staff_one["job_name"];
		}

		return $wk_data2;
	}
	/*************************************************************************/
	// 勤務当直情報取得
	// @param	$duty_yyyy			年
	// @param	$duty_mm			月
	// @param	$day_cnt			日数
	// @param	$set_data			再設定する情報
	// @param	$data_week			週情報
	// @param	$calendar_array		カレンダー情報
	// @param	$duty_only_disp_flg	当直のみ表示フラグ（１：ボタン押下 ２：再表示）
	// @param	$data_st			役職情報
	// @param	$jobmst				職種情報
	// @param	$empmst				職員情報
	// @param	$sche               パラメータ追加 20131115 取得元テーブル(atdbk(出勤予定):未指定時 / atdbk_sche(当初予定))
	//                              出勤予定テーブル名に付加する拡張子("_sche")
	//
	// @return	$data		取得情報
	/*************************************************************************/
	function get_duty_shift_duty_array(
									$group_id,
									$duty_yyyy, $duty_mm, $day_cnt,
									$set_data, $data_week, $calendar_array,
									$duty_only_disp_flg,
									$data_st, $data_job, $data_emp, $sche) {
		//-------------------------------------------------------------------
		//開始／終了年月日設定
		//-------------------------------------------------------------------
		$wk_cnt = count($calendar_array) - 1;
		$start_date = $calendar_array[0]["date"];
		$end_date = $calendar_array[$wk_cnt]["date"];
		//-------------------------------------------------------------------
		//ＤＢ(stmst)より役職情報取得
		//ＤＢ(jobmst)より職種情報取得
		//ＤＢ(empmst)より職員情報を取得
		//-------------------------------------------------------------------
		//$data_st = $this->obj->get_stmst_array();
		//$data_job = $this->obj->get_jobmst_array();
		//$data_emp = $this->obj->get_empmst_array("");
		//-------------------------------------------------------------------
		//ＤＢ(atdptn)より出勤パターン情報取得
		//ＤＢ(duty_shift_pattern)より勤務シフト記号情報を取得
		//-------------------------------------------------------------------
		//$data_atdptn_all = $this->obj->get_atdptn_array(""); //※未使用のためコメント化 20140312
		//$data_pattern_all = $this->obj->get_duty_shift_pattern_history_array("", $duty_yyyy, $duty_mm, $data_atdptn_all);
		//-------------------------------------------------------------------
		//ＤＢ(duty_shift_staff)より勤務シフトスタッフ情報を取得
		//ＤＢ(duty_shift_plan_staff)より勤務シフト情報（スタッフ情報）を取得
		//-------------------------------------------------------------------
		$data_staff = $this->obj->get_duty_shift_staff_array($group_id, $data_st, $data_job, $data_emp);
		$data_duty_staff = $this->obj->get_duty_shift_plan_staff_array($group_id, $duty_yyyy, $duty_mm, $data_st, $data_job, $data_emp);

		//*******************************************************************
		//スタッフの決定
		//*******************************************************************
		$data = array();
		$plan_rslt_flg = "";	//１：予定、２：実績、３：ＤＢ取得なし
		// 当直のみ表示ボタン押下時を除くを条件追加
		if (count($set_data) > 0 && $duty_only_disp_flg != "1") {
			//-------------------------------------------------------------------
			//勤務シフト情報の各名称設定
			//-------------------------------------------------------------------
			for($i=0;$i<count($set_data);$i++){
				//-------------------------------------------------------------------
				//予定／実績判定
				//-------------------------------------------------------------------
				$plan_rslt_flg = $set_data[$i]["plan_rslt_flg"];
				//-------------------------------------------------------------------
				//設定
				//-------------------------------------------------------------------
				$data[$i] = $set_data[$i];
				$data[$i]["no"] = $i + 1;
				//-------------------------------------------------------------------
				//職員DBより職員名設定
				//-------------------------------------------------------------------
				for ($k=0;$k<count($data_emp);$k++) {
				if ($data[$i]["staff_id"]  == $data_emp[$k]["id"]) {
					$data[$i]["staff_name"] = $data_emp[$k]["name"];
					break;
				}
				}
				//-------------------------------------------------------------------
				//職種DBより職種設定
				//-------------------------------------------------------------------
				$data_staff_one = $this->obj->get_duty_shift_staff_one($data[$i]["staff_id"], $data_st, $data_job, $data_emp);
				$data[$i]["job_id"] = $data_staff_one["job_id"];
				$data[$i]["job_name"] = $data_staff_one["job_name"];

				// group_id設定
				for ($j=0;$j<count($data_staff);$j++) {
					if ($data[$i]["staff_id"] == $data_staff[$j]["id"]) {		//スタッフＩＤ
						$data[$i]["group_id"] = $data_staff[$j]["group_id"];	//シフトグループID
						break;
					}
				}
			}

		} else {
			//-------------------------------------------------------------------
			//スタッフ情報設定
			//-------------------------------------------------------------------
			// 当直のみ表示以外を条件追加（重複シフトグループを表示しないため）
			if (count($data_duty_staff) > 0 && $duty_only_disp_flg == "") {
				//登録済み年月単位のスタッフ
				for ($i=0;$i<count($data_duty_staff);$i++) {
					$data[$i]["staff_id"] 	= $data_duty_staff[$i]["id"];			//スタッフＩＤ
					$data[$i]["staff_name"] = $data_duty_staff[$i]["name"];			//指名
					$data[$i]["job_id"] 	= $data_duty_staff[$i]["job_id"];		//職種ＩＤ
					$data[$i]["job_name"] 	= $data_duty_staff[$i]["job_name"];		//職種名
				}
			} else {
				//基本スタッフ
				for ($i=0;$i<count($data_staff);$i++) {
					$data[$i]["staff_id"] 	= $data_staff[$i]["id"];		//スタッフＩＤ
					$data[$i]["staff_name"] = $data_staff[$i]["name"];		//指名
					$data[$i]["job_id"] 	= $data_staff[$i]["job_id"];	//職種ＩＤ
					$data[$i]["job_name"] 	= $data_staff[$i]["job_name"];	//職種名
					$data[$i]["group_id"] 	= $data_staff[$i]["group_id"];	//シフトグループID
				}
			}
		}
		//-------------------------------------------------------------------
		//出勤表／（予定／実績）検索用
		//-------------------------------------------------------------------
		$cond_add = "";
		for ($i=0;$i<count($data);$i++) {
			$wk_id = $data[$i]["staff_id"];
			if ($cond_add == "") {
				$cond_add .= "and (a.emp_id = '$wk_id' ";
			} else {
				$cond_add .= "or a.emp_id = '$wk_id' ";
			}
		}
		if ($cond_add != "") {
			$cond_add .= ") ";
		}

		//*******************************************************************
		//勤務予定情報(atdbk)を追加
		//勤務予定情報(atdbk)を追加　又は　当初予定情報(atdbk_sche)を追加
		//*******************************************************************
		// 当直のみ表示ボタン押下時も条件追加
		if (count($set_data) <= 0 || $duty_only_disp_flg == "1") {
			//-------------------------------------------------------------------
			// 勤務予定情報を取得
			//-------------------------------------------------------------------
            $wk_data2 = $this->obj->get_atdbk_atdbkrslt_array("atdbk".$sche, $cond_add, $calendar_array, $data, $sche);
			//-------------------------------------------------------------------
			//情報を設定
			//-------------------------------------------------------------------
			for ($i=0;$i<count($data);$i++) {
			for ($k=0;$k<count($wk_data2);$k++) {
			if ($wk_data2[$k]["staff_id"] == $data[$i]["staff_id"]) {
				for ($m=1;$m<=$day_cnt;$m++) {
					$data[$i]["night_duty_$m"] = $wk_data2[$k]["night_duty_$m"];	//当直（１：有り、２：無し）
					// 当直スタッフフラグ
					if ($data[$i]["night_duty_$m"] == "1") {
						$data[$i]["night_duty_flg"] = "1";
					}
				}
			}
			}
			}
		}

		//*******************************************************************
		//勤務実績情報(atdbkrslt)を追加
		//*******************************************************************
		//-------------------------------------------------------------------
		// 勤務実績情報を取得
		//-------------------------------------------------------------------
        $data_rslt = $this->obj->get_atdbk_atdbkrslt_array("atdbkrslt", $cond_add, $calendar_array, $data);
		//-------------------------------------------------------------------
		//実績情報を追加
		//-------------------------------------------------------------------
		for ($i=0;$i<count($data);$i++) {
		for ($k=0;$k<count($data_rslt);$k++) {
		if ($data[$i]["staff_id"] == $data_rslt[$k]["staff_id"]) {
			for ($m=1;$m<=$day_cnt;$m++) {
				$data[$i]["rslt_night_duty_$m"] = $data_rslt[$k]["night_duty_$m"];	//当直（１：有り、２：無し）
				// 当直スタッフフラグ
				if ($data[$i]["rslt_night_duty_$m"] == "1") {
					$data[$i]["night_duty_flg"] = "1";
				}
			}
		}
		}
		}

		//*******************************************************************
		//勤務当直情報（下書き）情報取得
		//*******************************************************************
		// 当直のみ表示ボタン押下時も条件追加
		if (count($set_data) <= 0 || $duty_only_disp_flg == "1") {
			//-------------------------------------------------------------------
			//検索条件（職員）
			//-------------------------------------------------------------------
			$cond_add = "";
			for ($i=0;$i<count($data);$i++) {
				$wk_id = $data[$i]["staff_id"];
				if ($cond_add == "") {
					$cond_add .= "and (emp_id = '$wk_id' ";
				} else {
					$cond_add .= "or emp_id = '$wk_id' ";
				}
			}
			if ($cond_add != "") {
				$cond_add .= ") ";
			}
			//-------------------------------------------------------------------
			//データ取得
			//-------------------------------------------------------------------
			$sql = "select * from duty_shift_duty_draft";
			$cond = "where 1 = 1 ";
			$cond .= "and duty_date >= '$start_date' and duty_date <= '$end_date' ";
			$cond .= $cond_add;
			$cond .= "order by emp_id, duty_date";
			$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($sel == 0) {
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$num = pg_numrows($sel);
			//-------------------------------------------------------------------
			//情報の編集設定
			//-------------------------------------------------------------------
			$wk_data2 = $this->get_duty_shift_duty_set($sel, $num, $day_cnt,
													$data_st, $data_job, $data_emp,
													$calendar_array, $duty_only_disp_flg);
			//-------------------------------------------------------------------
			//情報を設定
			//-------------------------------------------------------------------
			for ($i=0;$i<count($data);$i++) {
			for ($k=0;$k<count($wk_data2);$k++) {
			if ($data[$i]["staff_id"] == $wk_data2[$k]["staff_id"]) {
				//日ごとの設定
				for ($m=1;$m<=$day_cnt;$m++) {
					$data[$i]["night_duty_$m"] = $wk_data2[$k]["night_duty_$m"];	//当直（１：有り、２：無し）
					// 当直スタッフフラグ
					if ($data[$i]["night_duty_$m"] == "1") {
						$data[$i]["night_duty_flg"] = "1";
					}
				}
			}
			}
			}
		}

		// 当直のみ表示の時、当直があるスタッフを配列に設定し直す。
		if ($duty_only_disp_flg != "") {
			$tmp_data = $data;
			$data = array();
			$j = 0;
			for ($i=0; $i<count($tmp_data); $i++) {
				if ($tmp_data[$i]["night_duty_flg"] == "1") {
					$data[$j] = $tmp_data[$i];
					$data[$j]["no"] = $j + 1;
					$j++;
				}
			}
		}

		//*******************************************************************
		//勤務当直情報の編集
		//*******************************************************************
		//-------------------------------------------------------------------
		//情報設定（表示順の設定）
		//-------------------------------------------------------------------
		for ($i=0;$i<count($data);$i++) {
			//-------------------------------------------------------------------
			//表示順
			//-------------------------------------------------------------------
			$data[$i]["no"] = $i + 1;

			//-------------------------------------------------------------------
			//他病棟と重複スタッフか
			//-------------------------------------------------------------------
			// 当直のみ表示の時は、group_idを設定し直す
			if ($duty_only_disp_flg != "") {
				$group_id = $data[$i]["group_id"];
			}
			$wk_id = $data[$i]["staff_id"];
			$sql = "select count(*) as cnt from duty_shift_plan_staff";
			$cond = "where group_id != '$group_id' ";
			$cond .= "and emp_id = '$wk_id' ";
			$cond .= "and duty_yyyy = $duty_yyyy ";
			$cond .= "and duty_mm = $duty_mm ";
			$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($sel == 0) {
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$num_cnt = pg_fetch_result($sel, 0, "cnt");
			//重複スタッフの場合
			if ($num_cnt > 0) {
				$data[$i]["other_staff_flg"] = "1";		//他病棟スタッフ（１：スタッフ）
			}
		}
		return $data;
	}
///////////////////////////////////////////////////////////////

}

?>
