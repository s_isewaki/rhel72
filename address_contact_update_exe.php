<?
require("about_session.php");
require("about_authority.php");
require("label_by_profile_type.ini");
require_once("Cmx.php");
require_once("aclg_set.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
if ($admin_flg == "t") {
	$checkauth = check_authority($session, 64, $fname);
} else {
	$checkauth = check_authority($session, 9, $fname);
}
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 入力チェック
if (strlen($post1) > 3) {
	echo("<script type=\"text/javascript\">alert('郵便番号1が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($post2) > 4) {
	echo("<script type=\"text/javascript\">alert('郵便番号2が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($address1) > 100) {
	echo("<script type=\"text/javascript\">alert('住所1が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($address2) > 100) {
	echo("<script type=\"text/javascript\">alert('住所2が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($tel1) > 6) {
	echo("<script type=\"text/javascript\">alert('電話番号1が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($tel2) > 6) {
	echo("<script type=\"text/javascript\">alert('電話番号2が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($tel3) > 6) {
	echo("<script type=\"text/javascript\">alert('電話番号3が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($email2) > 120) {
	echo("<script type=\"text/javascript\">alert('E-Mailが長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($mobile1) > 6) {
	echo("<script type=\"text/javascript\">alert('携帯端末1が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($mobile2) > 6) {
	echo("<script type=\"text/javascript\">alert('携帯端末2が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($mobile3) > 6) {
	echo("<script type=\"text/javascript\">alert('携帯端末3が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($m_mail) > 120) {
	echo("<script type=\"text/javascript\">alert('携帯Mailが長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($extension) > 10) {
	echo("<script type=\"text/javascript\">alert('内線番号が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($phs) > 6) {
	// 院内PHS/所内PHS
	$phs_name = $_label_by_profile["PHS"][$profile_type];
	echo("<script type=\"text/javascript\">alert('{$phs_name}が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

// 職員の連絡先情報を更新
$sql = "update empmst set";
$set = array("emp_zip1", "emp_zip2", "emp_prv", "emp_addr1", "emp_addr2", "emp_ext", "emp_tel1", "emp_tel2", "emp_tel3", "emp_mobile1", "emp_mobile2", "emp_mobile3", "emp_m_email", "emp_email2", "emp_phs");
$setvalue = array($post1, $post2, $province, $address1, $address2, $extension, $tel1, $tel2, $tel3, $mobile1, $mobile2, $mobile3, $m_mail, $email2, $phs);
$cond = "where emp_id = '$emp_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);


// 連絡先画面を再表示
$url_name = urlencode($search_name);
$url_submit = urlencode($search_submit);

// 本人連絡先からの場合
if ($contact_title_flg == "t") {
	// メニュータブを詳細から本人連絡先にするためIDをクリア
	$emp_id = "";
	echo("<script type=\"text/javascript\">location.href = 'address_contact_update.php?session=$session&emp_id=$emp_id&search_shared_flg=$search_shared_flg&search_name=$url_name&search_submit=$url_submit&page=$page&admin_flg=$admin_flg';</script>");
} else {
// 一覧からの場合
	if ($admin_flg == "t") {
		$next_php = "address_admin_menu.php";
	} else {
		$next_php = "address_menu.php";
	}
	echo("<script type=\"text/javascript\">location.href = '$next_php?session=$session&search_shared_flg=$search_shared_flg&search_name=$url_name&search_submit=$url_submit&page=$page';</script>");
}

?>
