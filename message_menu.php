<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 伝言メモ | 受信履歴</title>
<?
require_once("Cmx.php");
require_once("aclg_set.php");
?>
<? require_once("about_authority.php"); ?>
<? require_once("about_session.php"); ?>
<? require_once("show_message.ini"); ?>
<?
$fname=$PHP_SELF;

$session=qualify_session($session,$fname);
if($session=="0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

$ward=check_authority($session,4,$fname);
if($ward=="0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);
// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

?>
<script language="JavaScript" type="text/JavaScript">
<!--
function checkValue(){
var res=confirm("削除してよろしいですか？");
if(res==true){
document.message.action = "./message_delete.php";
document.message.submit();
}
}
//-->
</script>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<form name="message" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="message_menu.php?session=<? echo($session); ?>"><img src="img/icon/b05.gif" width="32" height="32" border="0" alt="伝言メモ"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="message_menu.php?session=<? echo($session); ?>"><b>伝言メモ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="120" height="22" align="center" bgcolor="#5279a5"><a href="./message_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>受信履歴</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="./message_left.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" >送信履歴</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="message_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">伝言メモ作成</font></a></td>
<td width="">&nbsp;</td>
<td width="100" align="center"><input type="button" value="削除" onclick="checkValue()"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="30" height="22"></td>
<td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">受信日時</font></td>
<td width=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">相手先</font></td>
<td width="320"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">用件</font></td>
<td width="50" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">伝言</font></td>
</tr>
</table>
<? show_message($authorities,$session,$page); ?>
<div align="right"><? showNext($authorities,$session,$page); ?></div>
</td>
</tr>
</table>
<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</body>
</html>