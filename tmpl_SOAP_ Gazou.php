<? // COMEDIX_TEMPLATE_VERSION_DEFINITION_2.0
ob_start();
require_once("read_xml.ini");
require_once("summary_tmpl_util.ini");
require_once("sot_util.php");
require_once("summary_write_tmpl_util.ini");
require_once("html_to_text_for_tinymce.php"); //tinymce文字化け対応(html2text4tinymce()関数など)
require_once('fpdf153/mbfpdf.php');
ob_end_clean();

//--------------------------------------------------------------------
// ダイレクトにPDF印刷指示が来たら、PDF印刷関数をコールする           
// ※これより上行に、改行などを含めてはいけません。                   
//--------------------------------------------------------------------
if ($_REQUEST['direct_template_pdf_print_requested']) exec_template_pdf_print();

//--------------------------------------------------------------------
// テンプレートビヘイビアを返す。                                     
//--------------------------------------------------------------------
function get_template_behavior(){ return array("VERSION"=>2, "COPYABLE"=>1); }


//--------------------------------------------------------------------
// テンプレートHTMLを作成する。初期値も設定する。                     
//--------------------------------------------------------------------
function write_tmpl_page($xml_file_name,$con,$fname,$tmpl_param){
	global $c_sot_util;
	global $session;
	$xml = new template_xml_class();
	$data = $xml->get_xml_object_array_from_db($con, $fname, $xml_file_name);
	$data = @$data->nodes["template"]->nodes["SOAP"];

	// 20120201 PDF用に追加 -from-
    //----------------------------------------------------------------
    // 患者情報                                                       
    //----------------------------------------------------------------
    $pt_info    = get_pt_info($con, $fname, $tmpl_param['pt_id']);
    // 患者性別
    $pt_info_sex = '';
    if ($pt_info['ptif_sex'] == '1') {
        $pt_info_sex = '男';
    } elseif ($pt_info['ptif_sex'] == '2') {
        $pt_info_sex = '女';
    }
    // 患者生年月日
    $pt_info_birth = (int)substr($pt_info['ptif_birth'], 0, 4) . '年' .
                     (int)substr($pt_info['ptif_birth'], 4, 2) . '月' .
                     (int)substr($pt_info['ptif_birth'], 6, 2) . '日生';

    //----------------------------------------------------------------
    // 入院情報                                                       
    //----------------------------------------------------------------
    $nyuin_info = $c_sot_util->get_nyuin_info($con, $fname, $tmpl_param['pt_id'], date('Ymd'));
    // 病棟情報
    $place_name = '';
    if ($nyuin_info['bldg_name'] != '') {
        $place_name .= $nyuin_info['bldg_name'];
    }
    if ($nyuin_info['ward_name'] != '') {
        $place_name .= ' ・' . $nyuin_info['ward_name'];
    }

    //----------------------------------------------------------------
    // 記載者情報                                                       
    //----------------------------------------------------------------
    $sql = "select emp_lt_nm || '  ' || emp_ft_nm as n from empmst where emp_id = '" . pg_escape_string(@$tmpl_param['login_emp_id']) . "'";
    $sel = $c_sot_util->select_from_table($sql);
    $input_name = pg_fetch_result($sel, 0, 0);

    //----------------------------------------------------------------
    // ヘッダ情報                                                     
    //----------------------------------------------------------------
    // ヘッダ年月日情報（作成日）
    $create_date_ymd = @$tmpl_param['cre_date'];    // yyyymmdd
    if (!$create_date_ymd) {
        $create_date_ymd = date('Ymd');
    }
    // 「年月日」差込
    $create_date_ymd_k = (int)substr($create_date_ymd, 0, 4) . '年' .
                         (int)substr($create_date_ymd, 4, 2) . '月' .
                         (int)substr($create_date_ymd, 6, 2) . '日';

    // 入力日時（値がない場合は現在の日時）
    $create_date_ymd = (int)@$data->nodes['create_date_ymd']->value;
    $create_date_y   = (int)@$data->nodes['create_date_y']->value;
    $create_date_m   = (int)@$data->nodes['create_date_m']->value;
    $create_date_d   = (int)@$data->nodes['create_date_d']->value;

    if ($create_date_y == '' ||
        $create_date_m == '' ||
        $create_date_d == '')
    {
        $create_date_y = (int)date('Y');
        $create_date_m = (int)date('m');
        $create_date_d = (int)date('d');
    }

    $create_date_ymd   = $create_date_y .
                         $create_date_m .
                         $create_date_d;
    $create_date_ymd_k = $create_date_y . '年' .
                         $create_date_m . '月' .
                         $create_date_d . '日';
	//----------------------------------------------------------------
	// 20120201 PDF用に追加 -to-

	//----------------------------------------------------------------
	// SOAP                                                              
    //----------------------------------------------------------------

	$def_y = @$data->nodes["exec_y"]->value;
	$def_m = @$data->nodes["exec_m"]->value;
	$def_d = @$data->nodes["exec_d"]->value;
	$def_time_h = @$data->nodes["exec_time_h"]->value;
	$def_time_i = @$data->nodes["exec_time_i"]->value;
	if (!$def_y) {
		$def_y = (int)date("Y");
		$def_m = (int)date("m");
		$def_d = (int)date("d");
		$def_time_h = (int)date("H");
		$def_time_i = (int)date("i");
	}

	$ta_s_to = @$data->nodes["Textarea_S"   ]->value;
	$ta_s    = @$data->nodes["Textarea_S_tm"]->value;
	if (!$ta_s) $ta_s = $ta_s_to;
	$ta_o_to = @$data->nodes["Textarea_O"   ]->value;
	$ta_o    = @$data->nodes["Textarea_O_tm"]->value;
	if (!$ta_o) $ta_o = $ta_o_to;
	$ta_a_to = @$data->nodes["Textarea_A"   ]->value;
	$ta_a    = @$data->nodes["Textarea_A_tm"]->value;
	if (!$ta_a) $ta_a = $ta_a_to;
	$ta_p_to = @$data->nodes["Textarea_P"   ]->value;
	$ta_p    = @$data->nodes["Textarea_P_tm"]->value;
	if (!$ta_p) $ta_p = $ta_p_to;
	$ta_n_to = @$data->nodes["Textarea_N"   ]->value;
	$ta_n    = @$data->nodes["Textarea_N_tm"]->value;
	if (!$ta_n) $ta_n = $ta_n_to;



    // カレンダー用設定
	require_once("yui_calendar_util.ini");
	write_yui_calendar_use_file_read_0_12_2();
	write_yui_calendar_script2(1);

    $seiki_emp_id = $_REQUEST['seiki_emp_id'] ;
    if (($seiki_emp_id == null) || (strlen($seiki_emp_id) < 1)) {
        $seiki_emp_id = $_REQUEST['emp_id'];
    }
    $sql2 = "select empmst.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, jobmst.job_nm from empmst, jobmst where empmst.emp_id = '" . $seiki_emp_id . "' and empmst.emp_job = jobmst.job_id";
    $sel2 = @select_from_table($con, $sql2, '', $fname);
    $job_info2 = @pg_fetch_array($sel2);
    $emp_nm = @$job_info2['emp_lt_nm'] . ' ' . @$job_info2['emp_ft_nm'];
    //----------------------------------------------------------------
    // Canvas画像ファイル名セット                                     
    //----------------------------------------------------------------
	$canvas_jpg_name = 'cpd_' . md5(rand()) . '.jpg';
?>

<style type="text/css">
	.tmpltable { font-size:18px; }
	.tmpltable td { vertical-align:top }
	.tmplta { width:424px; overflow:hidden; }
	.toppad { padding-top:12px }
	.w50px { width:50px }
	.w40px { width:40px }
	.pad2px { padding-left:2px; padding-right:2px; }
	#canvas {
    	border:1px solid #aaaaaa;
    	width:150px;
    	height:150px;
	}
</style>
<script src="js/jquery.js"></script>
<script type="text/javascript">
	function load_action(){

		if(!tinyMCE.isOpera && !tinyMCE.isSafari)
		{
			document.getElementById("Textarea_S").value=document.getElementById("Textarea_S").value.split("\n").join("<br />");
			document.getElementById("Textarea_O").value=document.getElementById("Textarea_O").value.split("\n").join("<br />");
			document.getElementById("Textarea_A").value=document.getElementById("Textarea_A").value.split("\n").join("<br />");
			document.getElementById("Textarea_P").value=document.getElementById("Textarea_P").value.split("\n").join("<br />");
			document.getElementById("Textarea_N").value=document.getElementById("Textarea_N").value.split("\n").join("<br />");
		}
		else
		{
			document.getElementById("Textarea_S").value=document.getElementById("Textarea_S_to").value;
			document.getElementById("Textarea_O").value=document.getElementById("Textarea_O_to").value;
			document.getElementById("Textarea_A").value=document.getElementById("Textarea_A_to").value;
			document.getElementById("Textarea_P").value=document.getElementById("Textarea_P_to").value;
			document.getElementById("Textarea_N").value=document.getElementById("Textarea_N_to").value;
		}
		var dmy = document.getElementById("textarea_dummy");
		if (!dmy) return;
		adjustTextAreaHeight("Textarea_S", 10, 1);
		adjustTextAreaHeight("Textarea_O", 10, 1);
		adjustTextAreaHeight("Textarea_A", 10, 1);
		adjustTextAreaHeight("Textarea_P", 10, 1);
		adjustTextAreaHeight("Textarea_N", 10, 1);
		dmy.style.display = "none";

		// 20120131追加 -from-
		// 保存画像があれば表示
		<? if (@$data->nodes['canvas_painter']->value != '') { ?>
			document.getElementById('canvas_data').value = "<?=@$data->nodes['canvas_painter']->value?>";
			canvas = document.getElementById('canvas');
			if (canvas.getContext) {
				c = canvas.getContext('2d');
        		var img = new Image();
				img.onload = function() {
					c.drawImage(img, 0, 0);
				}
			}
        	img.src = "<?=@$data->nodes['canvas_painter']->value?>";
		<? }?>
		// 保存画像のPDF用画像データ
		<? if (@$data->nodes['canvas_painter_pdf']->value != '') { ?>
			document.getElementById('canvas_pdf').value = "<?=@$data->nodes['canvas_painter_pdf']->value?>";


		<? } else { ?>
			document.getElementById('canvas_pdf').value  = "";
		<? }?>

		// 画像データがあれば表示
		if(document.getElementById('canvas_data').value != "") {
			$('#canvas').show();
		} else {
			$('#canvas').hide();
		}
		// 20120131追加 -to-

		// 20120201追加 -from-
		var isMSIE = /*@cc_on!@*/false;
        if (isMSIE) { 
            alert('このブラウザではご利用できません。FirefoxまたはSafariをご利用ください。');
        }
        var userAgent = window.navigator.userAgent.toLowerCase();
        if (userAgent.indexOf("chrome") > -1) {
            alert('このブラウザではご利用できません。FirefoxまたはSafariをご利用ください。');
        }

        var yyyy;
        var mm;
        var dd;
        var creDate;
        var empName = "<?=$emp_nm?>";
	<? // XMLファイルが指定されていないときは、親画面から取ってきた値を医師情報ヘッダにセットする ?>
		<? if ($data == '') { ?>
        	var opfm = opener.document.touroku;

			yyyy = opfm.sel_yr.options[opfm.sel_yr.selectedIndex].text;
			mm   = opfm.sel_mon.options[opfm.sel_mon.selectedIndex].text;
			dd   = opfm.sel_day.options[opfm.sel_day.selectedIndex].text;
			creDate = yyyy + '年' + mm + '月' + dd + '日';
		<? } else { ?>
        	yyyy = "<?=date('Y')?>";
        	mm   = "<?=date('m')?>";
        	dd   = "<?=date('d')?>";
        	creDate = yyyy + '年' + mm + '月' + dd + '日';
			<? if (strlen(trim($_REQUEST['tmpl_copy_xml_file'])) < 1) {  // ←コピー以外のとき(｢tmpl_copy_xml_file｣が指定されているときはコピーとみなす) ?>
        		creDate = "<?=@$data->nodes['create_date_ymd_k']->value?>";
        		empName = "<?=@$data->nodes['trks_emp_name']->value?>";
			<? } else { ?>
			<? } ?>
		<? } ?>

		// 20120201追加 -to-


	<? // PDF印刷用のターゲットを作成 20120201 PDF用に追加?>
        formPdf = document.createElement('FORM');
        var b   = document.getElementsByTagName('body');
        b[0].appendChild(formPdf);
        formPdf.name   = 'frm_pdf_print';
        formPdf.target = '_blank';
        formPdf.method = 'post';
        formPdf.action = 'tmpl_SOAP_Teikeibun.php';
        formPdf.innerHTML = '' +
            '<input type="hidden" name="ptif_id" value="<?=$tmpl_param['pt_id']?>" />' +
            '<input type="hidden" name="inpt_in_dt" value="<?=$nyuin_info['inpt_in_dt']?>" />' +
            '<input type="hidden" name="input_name" value="<?=$input_name?>" />' +
            '<input type="hidden" name="ptif_lt_kaj_nm" value="<?=$pt_info['ptif_lt_kaj_nm']?>" />' +
            '<input type="hidden" name="ptif_ft_kaj_nm" value="<?=$pt_info['ptif_ft_kaj_nm']?>" />' +
            '<input type="hidden" name="ptif_sex" value="<?=$pt_info_sex?>" />' +
            '<input type="hidden" name="create_date_ymd_k" value="' + creDate + '" />' +
            '<input type="hidden" name="create_date_ymd_y" value="<?=$create_date_y?>" />' +
            '<input type="hidden" name="create_date_ymd_m" value="<?=$create_date_m?>" />' +
            '<input type="hidden" name="create_date_ymd_d" value="<?=$create_date_d?>" />' +
            '<input type="hidden" name="ptif_birth" value="<?=$pt_info_birth?>" />' +
            '<input type="hidden" name="place_name" value="<?=$place_name?>" />' +
            '<input type="hidden" name="exec_y" value="" />' +
            '<input type="hidden" name="exec_m" value="" />' +
            '<input type="hidden" name="exec_d" value="" />' +
            '<input type="hidden" name="exec_time_h" value="" />' +
            '<input type="hidden" name="exec_time_i" value="" />' +
            '<input type="hidden" name="Textarea_S" value="" />' +
            '<input type="hidden" name="Textarea_O" value="" />' +
            '<input type="hidden" name="Textarea_A" value="" />' +
            '<input type="hidden" name="Textarea_P" value="" />' +
            '<input type="hidden" name="Textarea_N" value="" />' +
            '<input type="hidden" name="canvas_jpg_name" value="<?=$canvas_jpg_name?>" />' +
            '<input type="hidden" name="canvas_jpg_flg" value="" />' +
            '<input type="hidden" name="direct_template_pdf_print_requested" value="1" />';

	}
</script>
<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	if(!tinyMCE.isOpera && !tinyMCE.isSafari) {
		m_use_tyny_mce = true;
		tinyMCE.init({
			mode : "exact",
			theme : "advanced",
			elements: "Textarea_S,Textarea_O,Textarea_A,Textarea_P,Textarea_N",
			plugins : "preview,table,emotions,fullscreen,layer,paste",
			//language : "ja_euc-jp",
			language : "ja",
			width : "424px",
			height : "212",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			content_css : "tinymce/tinymce_content.css",
			theme_advanced_buttons1 : "strikethrough,pasteword",
			theme_advanced_buttons2 : "",
			theme_advanced_buttons3 : "",
			theme_advanced_statusbar_location : "none",
			force_br_newlines : true,
			forced_root_block : '',
			force_p_newlines : false
		});
	}

	function popupDraw(target_id){	// 描画画面ポップアップ
		window.open(
			"summary_draw.php?session=<?=$session?>&multi_flg=&caller=dummy&target_id=" + target_id,
			"draw_window",
			"directories=no,location=no,menubar=no,resizable=yes,scrollbars=no,status=no,titlebar=no,toolbar=no,left=10,top=10,width=650,height=650");
	}

	// 描画画面コールバック
	function call_back_draw(value, pdf_img){
//alert(value.length);
		if(value.length > 10180) {	// データあり
		// 白紙の状態は10131だが、書いた後に消すと誤差がでるので影響のない範囲で多めにする。
		// 一見、見えていなくてもデータとしての消し残りが多い場合は画像があると判断する場合もある。

			document.getElementById('canvas_data').value = value;
			document.getElementById('canvas_pdf').value  = pdf_img;
			canvas = document.getElementById('canvas');
			if (canvas.getContext) {
				c = canvas.getContext('2d');
        		var img = new Image();
				img.onload = function() {
					c.drawImage(img, 0, 0);
				}
			}
			img.src = document.getElementById('canvas_data').value;
			$('#canvas').show();
		}else{
			document.getElementById('canvas_data').value = "";
			document.getElementById('canvas_pdf').value  = "";
			$('#canvas').hide();
		}
		
	}

	function popupTeikeibunSelect(target_id){
		window.open(
			"summary_fixedform_select.php?session=<?=$session?>&multi_flg=&caller=dummy&target_id=" + target_id,
			"fixedform_window",
			"directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=10,top=10,width=600,height=700");
	}

	function call_back_fixedform_select_2(target_id, value){
		//var value_ = value.split("<br>").join("\r\n").replace(/(^\s+)|(\s+$)/g, "");
		if(!tinyMCE.isOpera && !tinyMCE.isSafari) {
			tinyMCE.get(target_id).setContent(tinyMCE.activeEditor.getContent() + value);
		} else {
			document.getElementById(target_id).value += value;
		}
	}

    function popupKensakekkaSelect(target_id){
		window.open(
			"summary_kensakekka_timeseries.php?session=<?=$session?>&pt_id=<?=$tmpl_param['pt_id']?>&target_id=" + target_id,
			"kennsakekka_window",
			"directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=10,top=10,width=300,height=700");
	}

    function call_back_kensakekka_select(target_id, value){
		if(!tinyMCE.isOpera && !tinyMCE.isSafari) {
			tinyMCE.get(target_id).setContent(tinyMCE.activeEditor.getContent() + value);
		} else {
			document.getElementById(target_id).value += value;
		}
	}

	// TEXTAREAの最新値取得
	function getValue(key){

		var val;

		if(!tinyMCE.isOpera && !tinyMCE.isSafari) {
			val = tinyMCE.get(key).getContent(); 
		} else {
			val = document.getElementById(key).value;
		}

		return val;

}

</script>


 <? //=============================================================== ?>
  <? // 印刷（その１）                                                 ?>
  <? //=============================================================== ?>
  <div style="border:1px solid #aaa; background-color:#ddd; padding:4px; margin-top:10px; text-align:right">
    <input type="button" value="PDF印刷" onclick="printPDF()" />
  </div>
  <div style="margin-top:10px;"></div>

  <? //=============================================================== ?>
  <? // 日付                                                           ?>
  <? //=============================================================== ?>
<div style="margin-top:6px; padding-bottom:4px; border-bottom:1px solid #999">
<table><tr>
<td class="w50px"><?=$font?>日付</font></td>
<td><?=$font?>
<select name="exec_y" id="date_y1"><?= $c_sot_util->generate_dropdown_options_y($def_y); ?></select><span class="pad2px">年</span>
<select name="exec_m" id="date_m1"><?= $c_sot_util->generate_dropdown_options_m($def_m); ?></select><span class="pad2px">月</span>
<select name="exec_d" id="date_d1"><?= $c_sot_util->generate_dropdown_options_d($def_d); ?></select><span class="pad2px">日</span>
<select name="exec_time_h" id="exec_time_h"><?= $c_sot_util->generate_dropdown_options_hour($def_time_h); ?></select><span class="pad2px">時</span>
<select name="exec_time_i" id="exec_time_i"><?= $c_sot_util->generate_dropdown_options_minute($def_time_i); ?></select><span class="pad2px">分</span>
</font></td>
<td>
<img src="img/calendar_link.gif" style="cursor:pointer;" onclick="show_cal1();"/>
<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
<div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
</font>
</td>
</tr></table>
</div>

<table class="tmpltable" style="margin-top:10px" border=0>
<tr><td colspan="2">S（Subjective）</td></tr>
<tr>
<td>
<textarea class="tmplta" id="Textarea_S" name="Textarea_S" rows="10" cols="50" onkeyup="adjustTextAreaHeight(this.id, 10);" onchange="adjustTextAreaHeight(this.id, 10);"><?=$ta_s?></textarea>
<textarea id="Textarea_S_to" name="Textarea_S_to" style="display:none;"><?=$ta_s_to?></textarea></td>
<td><input type="button" value="定型文" onclick="popupTeikeibunSelect('Textarea_S');"></td>
</tr>
<tr><td colspan="2" class="toppad">O（Objective）</td></tr>
<tr>
<td><textarea class="tmplta" id="Textarea_O" name="Textarea_O" rows="10" cols="50" onkeyup="adjustTextAreaHeight(this.id, 10);" onchange="adjustTextAreaHeight(this.id, 10);"><?=$ta_o?></textarea>
<textarea id="Textarea_O_to" name="Textarea_O_to" style="display:none;"><?=$ta_o_to?></textarea>
</td>
<td>
<input type="button" value="定型文" onclick="popupTeikeibunSelect('Textarea_O');"><br />
<input type="button" value="検査結果" onclick="popupKensakekkaSelect('Textarea_O');"><br />
<input type="button" value="描画画面" onclick="popupDraw('canvas');"><br />
</td>
</tr>
<tr><td colspan="2">
<canvas id="canvas" class="canvas" width="500" height="500"></canvas>
<input type="hidden" id="canvas_data" name="canvas_data" value="">
<input type="hidden" id="canvas_pdf" name="canvas_pdf" value="">
<input type="hidden" name="canvas_jpg_name" value="<?=@$canvas_jpg_name?>" />
</td></tr>
<tr><td colspan="2" class="toppad">A（Assessment）</td></tr>
<tr>
<td><textarea class="tmplta" id="Textarea_A" name="Textarea_A" rows="10" cols="50" onkeyup="adjustTextAreaHeight(this.id, 10);" onchange="adjustTextAreaHeight(this.id, 10);"><?=$ta_a?></textarea>
<textarea id="Textarea_A_to" name="Textarea_A_to" style="display:none;"><?=$ta_a_to?></textarea></td>
<td><input type="button" value="定型文" onclick="popupTeikeibunSelect('Textarea_A');"></td>
</tr>
<tr><td colspan="2" class="toppad">P（Plan）</td></tr>
<tr>
<td><textarea class="tmplta" id="Textarea_P" name="Textarea_P" rows="10" cols="50" onkeyup="adjustTextAreaHeight(this.id, 10);" onchange="adjustTextAreaHeight(this.id, 10);"><?=$ta_p?></textarea>
<textarea id="Textarea_P_to" name="Textarea_P_to" style="display:none;"><?=$ta_p_to?></textarea></td>
<td><input type="button" value="定型文" onclick="popupTeikeibunSelect('Textarea_P');"></td>
</tr>
<tr><td colspan="2" class="toppad">N（その他）</td></tr>
<tr>
<td><textarea class="tmplta" id="Textarea_N" name="Textarea_N" rows="10" cols="50" onkeyup="adjustTextAreaHeight(this.id, 10);" onchange="adjustTextAreaHeight(this.id, 10);"><?=$ta_n?></textarea>
<textarea id="Textarea_N_to" name="Textarea_N_to" style="display:none;"><?=$ta_n_to?></textarea></td>
<td><input type="button" value="定型文" onclick="popupTeikeibunSelect('Textarea_N');"></td>
</tr>
</table>
<br/>
<input type="hidden" name="User_Agent" value ="<?=$_SERVER["HTTP_USER_AGENT"]?>"/>

<script type="text/javascript">initcal();</script>

<?
//===============================================================
// PDF出力（兼画像ファイル生成param送信）                        
//===============================================================
?>
<script type="text/javascript">
    function printPDF()
    {
        var cpjn = document.tmplform.canvas_jpg_name.value;
        var data = document.getElementById('canvas_pdf').value;

		if (data != "") {	// 画像データがあるときJPGを作成する。

			formPdf.canvas_jpg_flg.value     = 1;	// 画像作成フラグオン

        	$.ajax({
				type: 'POST',
				url:  'save_canvas_jpg.php',
				data: 'canvas_jpg_name=' + cpjn + '&img=' + data,
				success: function(){
					pdfPrintStart();
				}
        	});
		}
		else {
			formPdf.canvas_jpg_flg.value     = 0;	// 画像作成フラグオフ
			pdfPrintStart();
		}

	}

	function pdfPrintStart(){

        formPdf.exec_y.value         = document.tmplform.exec_y.value;
        formPdf.exec_m.value         = document.tmplform.exec_m.value;
        formPdf.exec_d.value         = document.tmplform.exec_d.value;
        formPdf.exec_time_h.value    = document.tmplform.exec_time_h.value;
        formPdf.exec_time_i.value    = document.tmplform.exec_time_i.value;
		formPdf.Textarea_S.value     = getValue("Textarea_S");
		formPdf.Textarea_O.value     = getValue("Textarea_O");
		formPdf.Textarea_A.value     = getValue("Textarea_A");
		formPdf.Textarea_P.value     = getValue("Textarea_P");
		formPdf.Textarea_N.value     = getValue("Textarea_N");

        formPdf.submit();

    }
</script>
<? } ?>

<? //-------------------------------------------------------------------- ?>
<? // PDF印刷スクリプトを出力する                                         ?>
<? //-------------------------------------------------------------------- ?>
<?
function exec_template_pdf_print(){

	// 日時
	$date = "";
	if (@$_REQUEST["exec_y"]) $date .= @$_REQUEST["exec_y"]."年";
	if (@$_REQUEST["exec_m"]) $date .= @$_REQUEST["exec_m"]."月";
	if (@$_REQUEST["exec_d"]) $date .= @$_REQUEST["exec_d"]."日";
	if (@strlen($_REQUEST["exec_time_h"]) > 0) $date .= @$_REQUEST["exec_time_h"]."時";
	if (@strlen($_REQUEST["exec_time_i"]) > 0) $date .= @$_REQUEST["exec_time_i"]."分";
	//=================================================
	// PDF生成ライブラリの拡張クラス定義
	// ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
	//=================================================
	class CustomMBFPDF extends MBFPDF
	{
		// A4：210 x 297

		var $MOST_LEFT  =  15 ;
		var $MOST_TOP   =  10 ;
		var $HDR_CELL_H = 7.0 ;
		var $CNT_LINE_H = 5.0 ; 
    
		var $HD1_CELL_W = array(20, 25, 22, 33, 15, 8, 22, 35) ;
		var $HD2_CELL_W = array(25, 42, 25, 33, 20, 35) ;
		var $CNT_CELL_W = 180 ;

		var $CNT_CELL_W_O = 140 ;	// 画像表示時用Objective幅

		var $currY ;
    
		// ページヘッダーを出力する
		function writePageHeader()
		{
			$this->SetY($this->MOST_TOP) ;
			$this->SetFont(GOTHIC, '', 20) ;
			$this->Cell(0, 10.0, "ＳＯＡＰ(定型文)", '0', 1, 'C') ;
			$this->SetFont(GOTHIC, '', 11.0) ; // 9.5
			$this->Cell($this->HD1_CELL_W[0], $this->HDR_CELL_H, "患者ID",                    '1',   0, 'C') ;
			$this->Cell($this->HD1_CELL_W[1], $this->HDR_CELL_H, $_REQUEST['ptif_id'    ], 'TRB', 0, 'C') ;
			$this->Cell($this->HD1_CELL_W[2], $this->HDR_CELL_H, "患者氏名",                  'TRB', 0, 'C') ;
			$this->Cell($this->HD1_CELL_W[3], $this->HDR_CELL_H, $_REQUEST['ptif_lt_kaj_nm'] . ' ' . $_REQUEST['ptif_ft_kaj_nm'], 'TRB', 0, 'C') ;
			$this->Cell($this->HD1_CELL_W[4], $this->HDR_CELL_H, "性別",                      'TRB', 0, 'C') ;
			$this->Cell($this->HD1_CELL_W[5], $this->HDR_CELL_H, $_REQUEST['ptif_sex'], 'TRB', 0, 'C') ;
			$this->Cell($this->HD1_CELL_W[6], $this->HDR_CELL_H, "生年月日",                  'TRB', 0, 'C') ;
			$this->Cell($this->HD1_CELL_W[7], $this->HDR_CELL_H, $_REQUEST['ptif_birth'], 'TRB', 1, 'C') ;
			$this->SetY($this->GetY() + 2) ;
			$this->Cell($this->HD2_CELL_W[0], $this->HDR_CELL_H, "年月日",                        '1',   0, 'C') ;
			$this->Cell($this->HD2_CELL_W[1], $this->HDR_CELL_H, $_REQUEST['create_date_ymd_k'], 'TRB', 0, 'C') ;
			$this->Cell($this->HD2_CELL_W[2], $this->HDR_CELL_H, "記入者",                        'TRB', 0, 'C') ;
			$this->Cell($this->HD2_CELL_W[3], $this->HDR_CELL_H, $_REQUEST["input_name"], 'TRB', 0, 'C') ;
			$this->Cell($this->HD2_CELL_W[4], $this->HDR_CELL_H, "病棟",                          'TRB', 0, 'C') ;
			$this->Cell($this->HD2_CELL_W[5], $this->HDR_CELL_H, $_REQUEST['place_name'    ], 'TRB', 1, 'C') ;
      
			$this->currY = $this->getY() ;
		}

		function writeEachPart($title, $key)
		{

    		$data_cels = 9;
			$key_name = 'Textarea_'.$key;

  			// TinyMCE対応のためのブラウザ判定
  			$ua = $_REQUEST["User_Agent"] ;
  			$useTinyMCE = ((strpos($ua, "Opera") === FALSE) && (strpos($ua, "Safari") === FALSE)) ? true : false ;
			$textarea = html2text4tinymce2($_REQUEST[$key_name], $useTinyMCE);
			// ここまで

    		$skn_cnt_array = explode("\n", $textarea);
    		$skn_cnt       = count($skn_cnt_array);
    		if ($skn_cnt < $data_cels) {
        		$rp = $data_cels - $skn_cnt;
        		for ($j = 0; $j < $rp; ++$j) {
           			$textarea .= "\n";
        		}
			}

			$this->Cell(0, $this->CNT_LINE_H, $title, '0', 1, 'L') ;

			if($key == "O" &&  $_REQUEST['canvas_jpg_flg'] == 1){
				$h_right = $this->getY();
				$this->MultiCell($this->CNT_CELL_W_O, $this->CNT_LINE_H, $textarea, 'BLTR', 1, 'C') ;
				$h_left = $this->getY();

				$this->Cell(5, $this->CNT_LINE_H, '', '', 0, 'C') ;
    			$this->setXY(156, $h_right);
   				 $this->Image('./canvas_data_temp/' . $_REQUEST['canvas_jpg_name'], 156, $h_right, 39, 39, 'jpg');
    			$this->Cell(39, 39, "", 'TLRB', 1, 'C');
				
				$this->SetY($h_left);	// テキスト欄側のY軸をセット
			}
			else {
				$this->MultiCell($this->CNT_CELL_W, $this->CNT_LINE_H, $textarea, 'BLTR', 1, 'C') ;
			}
		}

        function Footer()
        {
            $this->setY(-15);
            $this->SetFont(GOTHIC, '', 10);
            $this->Cell(90, 5.0, '患者ID : ' . $_REQUEST['ptif_id'], 0, 0, 'L');
            $this->Cell(90, 5.0, $this->PageNo() . 'ページ　',             0, 1, 'R');
        }
	}

	//=================================================
	// ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
	// PDF生成ライブラリの拡張クラス定義
	//=================================================
 
  
	$pdf = new CustomMBFPDF() ;
	$pdf->AddMBFont(GOTHIC, 'EUC-JP') ;
	$pdf->SetMargins($pdf->MOST_LEFT, $pdf->MOST_TOP) ;
	$pdf->SetAutoPageBreak(true, 15) ;
	$pdf->Open() ;
	$pdf->AddPage() ;
  
	$pdf->writePageHeader() ;

	$pdf->SetY($pdf->currY + 4) ;
	$pdf->Cell(0, $pdf->CNT_LINE_H, "日時  " . $date, 'B', 1, 'L') ;

	$titles = array('Ｓ（Subjective）', 'Ｏ（Objective）', 'Ａ（Assessment）', 'Ｐ（Plan）', 'Ｎ（その他）');
	$keys   = array('S', 'O', 'A', 'P', 'N');

	$cnt = count($titles);
	for($i = 0; $i < $cnt; $i++){
		$pdf->SetY($pdf->getY() + 2) ;
		$pdf->writeEachPart($titles[$i], $keys[$i]);
	}


/* 必要なら入れるが、これによって無駄に改ページすることもあるのでコメントアウト

	$pdf->setXY(150, $pdf->getY() + 1);

    // ▼「以下余白」----------------------------------------------------------------
    $pdf->Cell(180, 5.0, '', 0, 1);
    $pdf->Cell(180, 5.0, '以下余白　　', 0, 1, 'R');
    // ▲「以下余白」----------------------------------------------------------------
*/

	$pdf->Output();

    sleep(1);

    deleteCanvasJpg($_REQUEST['canvas_jpg_name']);

    exit();
}


//--------------------------------------------------------------------
// PDF用画像ファイルを削除する                                        
//--------------------------------------------------------------------
function deleteCanvasJpg($param)
{
    if (file_exists('canvas_data_temp/' . $param)) {
        unlink('canvas_data_temp/' . $param);
    }

    $res = opendir("canvas_data_temp");
    while ($files[] = readdir($res));
    closedir($res);
    // 現在時刻より2日以上前のファイルを削除する
    $dtime = time() - 2 * 24 * 60 * 60;

    for ($i = 0; $files[$i]; ++$i) {
        if ($files[$i] !== '.' && $files[$i] !== '..') {
            $mtime = filemtime('canvas_data_temp/' . $files[$i]);
            if ($mtime < $dtime) {
                unlink('canvas_data_temp/' . $files[$i]);
            }
        }
    }
}
?>

<? //-------------------------------------------------------------------- ?>
<? // リクエストからXMLを作成する。create_tmpl_xml.phpから呼ばれる。      ?>
<? //-------------------------------------------------------------------- ?>
<?
function create_utf8_xmldom_from_request($con, $fname){

  global $c_sot_util;
  $xml = new template_xml_class();
  $template = $xml->create_root_template_element();
  $body = $xml->add_node($template, 0, "SOAP");

  $xml->add_node($body, 1, "exec_y", "exec_y",  @$_REQUEST["exec_y"]);
  $xml->add_node($body, 1, "exec_m", "exec_m",  @$_REQUEST["exec_m"]);
  $xml->add_node($body, 1, "exec_d", "exec_d",  @$_REQUEST["exec_d"]);
  $xml->add_node($body, 1, "exec_time_h", "exec_time_h",  @$_REQUEST["exec_time_h"]);
  $xml->add_node($body, 1, "exec_time_i", "exec_time_i",  @$_REQUEST["exec_time_i"]);

	$out = "";
	if (@$_REQUEST["exec_y"]) $out .= @$_REQUEST["exec_y"]."年";
	if (@$_REQUEST["exec_m"]) $out .= @$_REQUEST["exec_m"]."月";
	if (@$_REQUEST["exec_d"]) $out .= @$_REQUEST["exec_d"]."日";
  if (@strlen($_REQUEST["exec_time_h"]) > 0) $out .= @$_REQUEST["exec_time_h"]."時";
  if (@strlen($_REQUEST["exec_time_i"]) > 0) $out .= @$_REQUEST["exec_time_i"]."分";
  $xml->add_node($body, 0, "exec_ymdhm", "日時",  $out);

  // TinyMCE対応のためのブラウザ判定
  $ua = $_REQUEST["User_Agent"] ;
  $useTinyMCE = ((strpos($ua, "Opera") === FALSE) && (strpos($ua, "Safari") === FALSE)) ? true : false ;

  $ta_s = @$_REQUEST["Textarea_S"] ;
  $ta_o = @$_REQUEST["Textarea_O"] ;
  $ta_a = @$_REQUEST["Textarea_A"] ;
  $ta_p = @$_REQUEST["Textarea_P"] ;
  $ta_n = @$_REQUEST["Textarea_N"] ;
  $xml->add_node($body, 0, "Textarea_S", "Subjective",  html2text4tinymce2($ta_s, $useTinyMCE));
  $xml->add_node($body, 0, "Textarea_O", "Objective",   html2text4tinymce2($ta_o, $useTinyMCE));
  $xml->add_node($body, 0, "Textarea_A", "Assessment",  html2text4tinymce2($ta_a, $useTinyMCE));
  $xml->add_node($body, 0, "Textarea_P", "Plan",        html2text4tinymce2($ta_p, $useTinyMCE));
  $xml->add_node($body, 0, "Textarea_N", "その他",      html2text4tinymce2($ta_n, $useTinyMCE));
/*
  $xml->add_node($body, 0, "Textarea_S_tm", "ta_s", htmlspecialchars4tinymce($ta_s, $useTinyMCE));
  $xml->add_node($body, 0, "Textarea_O_tm", "ta_o", htmlspecialchars4tinymce($ta_o, $useTinyMCE));
  $xml->add_node($body, 0, "Textarea_A_tm", "ta_a", htmlspecialchars4tinymce($ta_a, $useTinyMCE));
  $xml->add_node($body, 0, "Textarea_P_tm", "ta_p", htmlspecialchars4tinymce($ta_p, $useTinyMCE));
  $xml->add_node($body, 0, "Textarea_N_tm", "ta_n", htmlspecialchars4tinymce($ta_n, $useTinyMCE));
*/
  $xml->add_node($body, 1, "Textarea_S_tm", "ta_s", htmlspecialchars4tinymce2($ta_s, $useTinyMCE));
  $xml->add_node($body, 1, "Textarea_O_tm", "ta_o", htmlspecialchars4tinymce2($ta_o, $useTinyMCE));
  $xml->add_node($body, 1, "Textarea_A_tm", "ta_a", htmlspecialchars4tinymce2($ta_a, $useTinyMCE));
  $xml->add_node($body, 1, "Textarea_P_tm", "ta_p", htmlspecialchars4tinymce2($ta_p, $useTinyMCE));
  $xml->add_node($body, 1, "Textarea_N_tm", "ta_n", htmlspecialchars4tinymce2($ta_n, $useTinyMCE));

    // ペイントデータ（データ）
    $xml->add_node($body, 1, 'canvas_painter', 'ペイントデータ（データ）', @$_REQUEST['canvas_data']);
//    $xml->add_node($body, 0, 'canvas_painter', 'ペイントデータ（容量）', strlen($_REQUEST['canvas_data']));
    // PDF用データ（データ）
    $xml->add_node($body, 1, 'canvas_painter_pdf', 'PDF用データ（データ）', @$_REQUEST['canvas_pdf']);
    // 画像データ有無
    $is_canvas_data = 'なし';

    if (10180 < strlen(@$_REQUEST['canvas_data'])) {
        $is_canvas_data = 'あり';
    }
    $xml->add_node($body, 0, 'canvas_jpg_name', '画像データ入力', $is_canvas_data);
  return $xml->dump_mem_utf8();
}
?>

<? //-------------------------------------------------------------------- ?>
<? // フローシート一覧表示関数                                            ?>
<? //-------------------------------------------------------------------- ?>
<? function write_flowsheet_page($xml_files, $disp_count) { write_flowsheet_page_default($xml_files, $disp_count); } ?>

