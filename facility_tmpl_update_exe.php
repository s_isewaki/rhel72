<?
require_once("about_comedix.php");
require_once("Cmx.php");
require_once("aclg_set.php");
?>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="facility_tmpl_update.php" method="post">
<input type="hidden" name="tmpl_title" value="<? echo($tmpl_title); ?>">
<input type="hidden" name="content" value="<?
$content = eregi_replace("_(textarea)", "/\\1", $content);
$content = eregi_replace("_(script)", "/\\1", $content);
echo(h($content)); ?>">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="fcltmpl_id" value="<? echo($fcltmpl_id); ?>">
<input type="hidden" name="back" value="t">
</form>
<?
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, 41, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// 入力チェック
if ($tmpl_title == "") {
	echo("<script type=\"text/javascript\">alert('テンプレート名称を入力してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

if (strlen($content) == 0) {
	echo("<script type=\"text/javascript\">alert('テンプレートを指定してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

if (strpos($content, "<? // XML") === false) {
	echo("<script type=\"text/javascript\">alert('XML作成用コードがありません。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

if (strpos($content, "dump_mem") === false) {
	echo("<script type=\"text/javascript\">alert('テンプレートが最後まで設定されていません。確認してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

// 内容チェック
$content_upper = strtoupper($content);
$sqlchkflg = true;

$chkarray = array("INSERT", "UPDATE", "DELETE", "CONNECT2DB", "SELECT_FROM_TABLE", "PG_CONNECT", "PG_EXEC", "<FORM");
foreach($chkarray as $chkstr){
	if(!(strpos($content_upper, $chkstr) === false)){
		$sqlchkflg = false;
		break;
	}
}

if($sqlchkflg == false){
	echo("<script language=\"javascript\">alert('指定できない文字列があります($chkstr)。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;

}

// 'エスケープ
$tmpl_title = pg_escape_string($tmpl_title);
$content = pg_escape_string($content);

// トランザクションを開始
pg_query($con, "begin");


$now = date("YmdHis");

// テンプレート情報を登録
$sql = "update fcltmpl set ";
$set = array("fcltmpl_name", "fcltmpl_content", "fcltmpl_up_date");
$setvalue = array($tmpl_title, $content, $now);
$cond = "where fcltmpl_id = $fcltmpl_id";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 一覧画面に遷移
echo("<script type=\"text/javascript\">location.href='facility_tmpl_list.php?session=$session';</script>");
?>
</body>
