<?
ob_start();
$fname = $_SERVER["PHP_SELF"];
require_once("about_comedix.php");
require("get_values.php");
require("show_sinryo_top_header.ini");
require("label_by_profile_type.ini");
require_once("sot_util.php");
require("summary_common.ini");
ob_end_clean();

$session = qualify_session($session,$fname);
$fplusadm_auth = check_authority($session, 59, $fname);
$con = @connect2db($fname);
if(!$session || !$fplusadm_auth || !$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}


$mode = @$_REQUEST["mode"];
$db_key = explode("_", @$_REQUEST["key"]);
$is_disabled = @$_REQUEST["is_disabled"];
$caption = @$_REQUEST["caption"];
$yakkou_bunrui_cd = @$_REQUEST["yakkou_bunrui_cd"];
$keylen = count($db_key);


//==========================================================================
// 更新
//==========================================================================
if (@$_REQUEST["mode"] == "update"){
	$sql  = " update sum_med_yakkou_bunrui".$keylen."_mst set";
	$sql .= " caption = '" . pg_escape_string($caption)."'";
	$sql .= ",is_disabled = '" . pg_escape_string($is_disabled)."'";
	if ($keylen == 3) $sql .= ",yakkou_bunrui_cd = '". pg_escape_string($yakkou_bunrui_cd)."'";
	$sql .= " where code1 = '".pg_escape_string($db_key[0])."'";
	if ($keylen > 1) $sql .= " and code2 = '".pg_escape_string($db_key[1])."'";
	if ($keylen > 2) $sql .= " and code3 = '".pg_escape_string($db_key[2])."'";
	$upd = update_set_table($con, $sql, array(), null, "", $fname);
	header("Location: sum_ordsc_yakkou_list.php?session=".$session);
	die;
}

//==========================================================================
// 削除
//==========================================================================
if (@$_REQUEST["mode"] == "delete"){
	if ($keylen <= 3){
		$sql = " delete from sum_med_yakkou_bunrui3_mst where code1 = '".pg_escape_string($db_key[0])."'";
		if ($keylen <= 2) $sql .= " and code2 = '".pg_escape_string($db_key[1])."'";
		if ($keylen <= 3) $sql .= " and code3 = '".pg_escape_string($db_key[2])."'";
		$upd = update_set_table($con, $sql, array(), null, "", $fname);
	}
	if ($keylen <= 2){
		$sql = " delete from sum_med_yakkou_bunrui2_mst where code1 = '".pg_escape_string($db_key[0])."'";
		if ($keylen <= 2) $sql .= " and code2 = '".pg_escape_string($db_key[1])."'";
		$upd = update_set_table($con, $sql, array(), null, "", $fname);
	}
	if ($keylen <= 1){
		$sql =
			" delete from sum_med_yakkou_bunrui1_mst".
			" where code1 = '".pg_escape_string($db_key[0])."'";
		$upd = update_set_table($con, $sql, array(), null, "", $fname);
	}
	header("Location: sum_ordsc_yakkou_list.php?session=".$session);
	die;
}

//==========================================================================
// 表示順変更
//==========================================================================
if (@$_REQUEST["mode"] == "up" || @$_REQUEST["mode"] == "down"){
	$sql =
		" select sort_order from sum_med_yakkou_bunrui".$keylen."_mst".
		" where code1 = '".pg_escape_string($db_key[0])."'";
	if ($keylen > 1) $sql .= " and code2 = '".pg_escape_string($db_key[1])."'";
	if ($keylen > 2) $sql .= " and code3 = '".pg_escape_string($db_key[2])."'";
	$sel = select_from_table($con, $sql, "", $fname);
	$cur_sort_order = pg_fetch_result($sel,0,0);

	$sql = " select * from sum_med_yakkou_bunrui".$keylen."_mst";
	if ($_REQUEST["mode"] == "up") $sql .= " where sort_order < ".$cur_sort_order;
	if ($_REQUEST["mode"] == "down") $sql .= " where sort_order > ".$cur_sort_order;
	if ($keylen > 1) $sql .= " and code1 = '".pg_escape_string($db_key[0])."'";
	if ($keylen > 2) $sql .= " and code2 = '".pg_escape_string($db_key[1])."'";
	if ($_REQUEST["mode"] == "up") $sql .= " order by sort_order desc limit 1";
	if ($_REQUEST["mode"] == "down") $sql .= " order by sort_order limit 1";
	$sel = select_from_table($con, $sql, "", $fname);
	$swap_row = pg_fetch_array($sel);

	if ($swap_row["sort_order"]!=""){

		$sql  = " update sum_med_yakkou_bunrui".$keylen."_mst set";
		$sql .= " sort_order = " . $swap_row["sort_order"];
		$sql .= " where code1 = '".pg_escape_string($db_key[0])."'";
		if ($keylen > 1) $sql .= " and code2 = '".pg_escape_string($db_key[1])."'";
		if ($keylen > 2) $sql .= " and code3 = '".pg_escape_string($db_key[2])."'";
		$upd = update_set_table($con, $sql, array(), null, "", $fname);

		$sql  = " update sum_med_yakkou_bunrui".$keylen."_mst set";
		$sql .= " sort_order = " . $cur_sort_order;
		$sql .= " where code1 = '".pg_escape_string($swap_row["code1"])."'";
		if ($keylen > 1) $sql .= " and code2 = '".pg_escape_string($swap_row["code2"])."'";
		if ($keylen > 2) $sql .= " and code3 = '".pg_escape_string($swap_row["code3"])."'";
		$upd = update_set_table($con, $sql, array(), null, "", $fname);

		header("Location: sum_ordsc_yakkou_list.php?session=".$session);
		die;
	}
}



//==========================================================================
// 以下、通常処理
//==========================================================================
$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';


// 診療科権限を取得
$entity_admin_auth = check_authority($session, 28, $fname);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];
// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = $_label_by_profile["MED_REPORT"][$profile_type];
// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <? echo($med_report_title); ?>管理 | 処方・注射オーダ</title>

<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.td_border { border:#5279a5 solid 1px; }
.subtitle { font-weight:bold; margin-top:10px }
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

<? show_sinryo_top_header($session,$fname,"KANRI_ORDSC"); ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">
<?= summary_common_show_admin_left_menu($session, $entity_admin_auth, $section_title, $summary_kubun_title); ?>
</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->






<!-- right -->
<td valign="top">
<?//==========================================================================?>
<?// 処方・注射メニュー                                                       ?>
<?//==========================================================================?>
<?= summary_common_show_ordsc_right_menu($session, "薬効分類")?>

<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr></table>


<?//==========================================================================?>
<?// サブメニュー                                                             ?>
<?//==========================================================================?>

<table border="0" cellspacing="0" cellpadding="0" style="margin-top:8px">
	<tr height="22">
		<td width="100" align="center" bgcolor="#958f28">
			<a href="sum_ordsc_yakkou_list.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>一覧/編集</b></font></a>
		</td>
		<td width="5">&nbsp;</td>
		<td width="100" align="center" bgcolor="#e8e4bd">
			<a href="sum_ordsc_yakkou_regist.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>新規登録</b></font></a>
		</td>
		<td width="5">&nbsp;</td>
		<td></td>
	</tr>
</table>



<?//==========================================================================?>
<?// 一覧                                                                     ?>
<?//==========================================================================?>
<?
	$data1 = array();
	$data2 = array();
	$data3 = array();
	$sql =
		" select".
		" yb1.code1 as code1, yb2.code2 as code2, yb3.code3 as code3".
		",yb1.caption as yb1caption, yb2.caption as yb2caption, yb3.caption as yb3caption".
		",yb3.yakkou_bunrui_cd".
		",yb1.is_disabled as yb1disabled, yb2.is_disabled as yb2disabled, yb3.is_disabled as yb3disabled".
		" from sum_med_yakkou_bunrui1_mst yb1".
		" left join sum_med_yakkou_bunrui2_mst yb2 on (yb2.code1 = yb1.code1)".
		" left join sum_med_yakkou_bunrui3_mst yb3 on (yb3.code1 = yb2.code1 and yb3.code2 = yb2.code2)".
		" order by yb1.sort_order, yb2.sort_order, yb3.sort_order";
	$sel = select_from_table($con, $sql, "", $fname);
	while ($row = pg_fetch_array($sel)) {
		$data1[$row["code1"]] = array("caption"=>$row["yb1caption"], "disabled"=>$row["yb1disabled"]);
		$data2[$row["code1"]][$row["code2"]] = array("caption"=>$row["yb2caption"], "disabled"=>$row["yb2disabled"]);
		$data3[$row["code1"]][$row["code2"]][$row["code3"]] = $row;
	}
?>


<form action="sum_ordsc_yakkou_list.php" method="get" name="frm" style="margin-top:10px">
	<input type="hidden" name="session" value="<?=$session ?>">
	<input type="hidden" name="mode" value="">
	<input type="hidden" name="key" value="">
	<input type="hidden" name="is_disabled" value="">
	<input type="hidden" name="caption" value="">
	<input type="hidden" name="yakkou_bunrui_cd" value="">
</form>
<script type="text/javascript">
	function registData(mode, key){
		var keys = key.split("_");
		document.frm.mode.value = mode;
		document.frm.key.value = key;
		document.frm.caption.value = document.getElementById("data_"+key).value;
		document.frm.yakkou_bunrui_cd.value = document.getElementById("cd_"+key) ? document.getElementById("cd_"+key).value : "";
		document.frm.is_disabled.value =  document.getElementById("chk_"+key).checked ? "0" : "1";
		if (keys.length==3 && document.frm.yakkou_bunrui_cd.value=="") {
			alert("薬効分類コード(3桁)を指定してください。");
			return;
		}
		if (document.frm.caption.value==""){
			alert("薬効分類名を指定してください。");
			return;
		}
		document.frm.submit();
	}
</script>

<? foreach ($data1 as $code1 => $row1){ ?>
	<table border="0" cellspacing="0" cellpadding="2" style="margin-top:10px; border-collapse:collapse">
		<tr>
			<td class="td_border" style="width:180px; background-color:#e3ecf4" colspan="4">&nbsp;&nbsp;<?=$font?><?=$code1?></font></td>
			<td class="td_border"><?=$font?>
				<input type="text" style="width:300px" name="data_<?=$code1?>" id="data_<?=$code1?>" value="<?=h($row1["caption"])?>">
				<input type="button" value="▲" onclick="registData('up','<?=$code1?>')" />
				<input type="button" value="▼" onclick="registData('down','<?=$code1?>')" />
				<label style="padding-left:10px"><input type="checkbox" value="1" id="chk_<?=$code1?>" name="chk_<?=$code1?>" <?=$row1["disabled"]?"":"checked"?> />表示</label>
				<input type="button" value="更新" onclick="registData('update','<?=$code1?>')" />
				<input style="margin-left:10px" type="button" value="削除" onclick="if(!confirm('この操作で、下階層も同時に削除されます。\n\n削除してよろしいですか？')) return; registData('delete','<?=$code1?>')" />
			</font></td>
		</tr>
		<? foreach ($data2[$code1] as $code2 => $row2){ ?>
			<? if ($code2=="") continue; ?>
			<tr>
			<td style="width:30px">&nbsp;</td>
			<td class="td_border" style="width:130px; background-color:#e3ecf4" colspan="3">&nbsp;&nbsp;<?=$font?><?=$code1?>-<?=$code2?></font></td>
			<td class="td_border"><?=$font?>
				<input type="text" style="width:300px" name="data_<?=$code1?>_<?=$code2?>" id="data_<?=$code1?>_<?=$code2?>" value="<?=h($row2["caption"])?>">
				<input type="button" value="▲" onclick="registData('up','<?=$code1?>_<?=$code2?>')" />
				<input type="button" value="▼" onclick="registData('down','<?=$code1?>_<?=$code2?>')" />
				<label style="padding-left:10px"><input type="checkbox" value="1" id="chk_<?=$code1?>_<?=$code2?>" name="chk_<?=$code1?>_<?=$code2?>" <?=$row2["disabled"]?"":"checked"?> />表示</label>
				<input type="button" value="更新" onclick="registData('update','<?=$code1?>_<?=$code2?>')" />
				<input style="margin-left:10px" type="button" value="削除" onclick="if(!confirm('この操作で、下階層も同時に削除されます。\n\n削除してよろしいですか？')) return; registData('delete','<?=$code1?>_<?=$code2?>')" />
			</font></td>
			</tr>
			<? foreach ($data3[$code1][$code2] as $code3 => $row3){ ?>
			<? if ($code3=="") continue; ?>
			<tr>
			<td style="width:30px">&nbsp;</td>
			<td style="width:30px">&nbsp;</td>
			<td class="td_border" style="width:50px; background-color:#e3ecf4; text-align:center"><?=$font?><?=$code1?>-<?=$code2?>-<?=$code3?></font></td>
			<td class="td_border" style="width:50px; text-align:center">
				<input type="text" style="width:50px; ime-mode:disabled" name="cd_<?=$code1?>_<?=$code2?>_<?=$code3?>" id="cd_<?=$code1?>_<?=$code2?>_<?=$code3?>" value="<?=h($row3["yakkou_bunrui_cd"])?>" maxlength="3">
			</td>
			<td class="td_border"><?=$font?>
				<input type="text" style="width:300px" name="data_<?=$code1?>_<?=$code2?>_<?=$code3?>" id="data_<?=$code1?>_<?=$code2?>_<?=$code3?>" value="<?=h($row3["yb3caption"])?>" maxlength="70">
				<input type="button" value="▲" onclick="registData('up','<?=$code1?>_<?=$code2?>_<?=$code3?>')" />
				<input type="button" value="▼" onclick="registData('down','<?=$code1?>_<?=$code2?>_<?=$code3?>')" />
				<label style="padding-left:10px"><input type="checkbox" value="1" id="chk_<?=$code1?>_<?=$code2?>_<?=$code3?>" name="chk_<?=$code1?>_<?=$code2?>_<?=$code3?>" <?=$row3["yb3disabled"]?"":"checked"?> />表示</label>
				<input type="button" value="更新" onclick="registData('update','<?=$code1?>_<?=$code2?>_<?=$code3?>')" />
				<input style="margin-left:10px" type="button" value="削除" onclick="if(!confirm('削除してよろしいですか？')) return; registData('delete','<?=$code1?>_<?=$code2?>_<?=$code3?>')" />
			</font></td>
			</tr>
			<? } ?>
		<? } ?>
	</table>
<? } ?>

</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
