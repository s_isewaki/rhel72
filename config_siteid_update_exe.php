<?
ini_set("max_execution_time", 0);

require_once("about_session.php");
require_once("about_authority.php");
require_once("webmail_quota_functions.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, 20, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($new_site_id != "" && preg_match("/^[a-z]{1,8}$/", $new_site_id) == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('サイトIDは半角小文字8文字以内で入力してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// バッファリングしない
ob_implicit_flush(true);

// IE対策
echo(str_repeat(" ", 256));

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 環境設定情報を取得
$sql = "select site_id, use_cyrus from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$current_site_id = pg_fetch_result($sel, 0, "site_id");
$use_cyrus = pg_fetch_result($sel, 0, "use_cyrus");

// サイトIDが変わらない場合はエラーとする
if ($new_site_id == $current_site_id) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('サイトIDが同一です。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// サイトIDを更新
$sql = "update config set";
$set = array("site_id");
$setvalue = array($new_site_id);
$cond = "";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// ログイン情報を取得
$sql = "select emp_id, emp_login_id, emp_login_mail, emp_login_pass from login";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// ログイン情報更新フラグの設定

//// サイトIDなしからサイトIDありに変わる場合
if ($current_site_id == "" && $new_site_id != "") {
	$update_login = true;

//// サイトIDありからサイトIDなしに変わる場合
} else if ($current_site_id != "" && $new_site_id == "") {
	$update_login = true;

//// それ以外の場合
} else {
	$update_login = false;
}

// ログイン情報をループ
$mail_accounts = array();
while ($row = pg_fetch_array($sel)) {
	$tmp_emp_id = $row["emp_id"];
	$tmp_login_id = $row["emp_login_id"];
	$tmp_mail_id = $row["emp_login_mail"];
	$tmp_password = $row["emp_login_pass"];

	// サイトIDなしからサイトIDありに変わる場合
	if ($current_site_id == "" && $new_site_id != "") {
		$new_mail_id = $tmp_login_id;
		$mail_accounts[] = array(
			"old_account" => $tmp_login_id,
			"new_account" => $tmp_login_id . "_$new_site_id",
			"password" => $tmp_password
		);

	// サイトIDありからサイトIDなしに変わる場合
	} else if ($current_site_id != "" && $new_site_id == "") {
		$new_mail_id = "";
		$mail_accounts[] = array(
			"old_account" => $tmp_mail_id . "_$current_site_id",
			"new_account" => $tmp_login_id,
			"password" => $tmp_password
		);

	// それ以外の場合
	} else {
		$mail_accounts[] = array(
			"old_account" => $tmp_mail_id . "_$current_site_id",
			"new_account" => $tmp_mail_id . "_$new_site_id",
			"password" => $tmp_password
		);
	}

	// 必要があればログイン情報を更新
	if ($update_login) {
		$sql = "update login set";
		$set = array("emp_login_mail");
		$setvalue = array($new_mail_id);
		$cond = "where emp_id = '$tmp_emp_id'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

// トランザクションをコミット
pg_query($con, "commit");
//pg_query($con, "rollback");

// データベース接続を閉じる
pg_close($con);

if ($use_cyrus == "t") {

	// メールアカウントを更新
	echo("メールアカウント：全" . count($mail_accounts) . "件<br>");

	require_once("webmail/config/config.php");
	$dir = getcwd();
	foreach ($mail_accounts as $i => $tmp_account) {
		echo(($i + 1) . "件目を更新中……");

		$tmp_old_account = $tmp_account["old_account"];
		$tmp_new_account = $tmp_account["new_account"];
		$tmp_password = $tmp_account["password"];

		// アカウントにアルファベットなし→アルファベットありの場合、メールアカウントを作成
		if (preg_match("/[a-z]/", $tmp_old_account) == 0 && preg_match("/[a-z]/", $tmp_new_account) > 0) {
			if ($imapServerAddress == "localhost" || $imapServerAddress == "127.0.0.1") {
				exec("/usr/bin/perl $dir/mboxadm/addmailbox $tmp_new_account $tmp_password 2>&1 1> /dev/null", $output, $exit_status);
				if ($exit_status !== 0) {
					exec("/usr/bin/expect -f $dir/expect/addmailbox.exp $tmp_new_account $tmp_password");
				}
			} else {
				exec("/usr/bin/expect -f $dir/expect/rsyncaddmailbox.exp $tmp_new_account $tmp_password $imapServerAddress $dir");
			}
			webmail_quota_change_to_default($tmp_new_account);

		// アカウントがアルファベットあり→アルファベットありの場合、メールアカウントを更新
		} else if (preg_match("/[a-z]/", $tmp_old_account) > 0 && preg_match("/[a-z]/", $tmp_new_account) > 0) {
			if ($imapServerAddress == "localhost" || $imapServerAddress == "127.0.0.1") {
				exec("/usr/bin/perl $dir/mboxadm/updmailbox $tmp_old_account $tmp_new_account $tmp_password 2>&1 1> /dev/null", $output, $exit_status);
				if ($exit_status !== 0) {
					exec("/usr/bin/expect -f $dir/expect/updmailbox.exp $tmp_old_account $tmp_new_account $tmp_password");
				}
			} else {
				exec("/usr/bin/expect -f $dir/expect/rsyncupdmailbox.exp $tmp_old_account $tmp_new_account $tmp_password $imapServerAddress $dir");
			}

		// アカウントがアルファベットあり→アルファベットなしの場合、メールアカウントを削除
		} else if (preg_match("/[a-z]/", $tmp_old_account) > 0 && preg_match("/[a-z]/", $tmp_new_account) == 0) {
			if ($imapServerAddress == "localhost" || $imapServerAddress == "127.0.0.1") {
				exec("/usr/bin/perl $dir/mboxadm/delmailbox $tmp_old_account 2>&1 1> /dev/null", $output, $exit_status);
				if ($exit_status !== 0) {
					exec("/usr/bin/expect -f $dir/expect/delmailbox.exp $tmp_old_account");
				}
			} else {
				exec("/usr/bin/expect -f $dir/expect/rsyncdelmailbox.exp $tmp_old_account $imapServerAddress $dir");
			}
		}

		echo("終了<br>");
	}
}

echo("<script type=\"text/javascript\">alert('更新が完了しました。');</script>");

// 画面をリフレッシュ
echo("<script type=\"text/javascript\">location.href = 'config_siteid.php?session=$session';</script>");
