<?
//<!-- ************************************************************************ -->
//<!-- 勤務シフト作成　届出書添付書類画面 様式９ 共通部品ＣＬＡＳＳ -->
//<!-- ************************************************************************ -->

require_once("Cmx.php");
require_once('Cmx/Model/SystemConfig.php');
require_once("about_comedix.php");
require_once("duty_shift_common_class.php");
require_once("duty_shift_time_common_class.php");
require_once("timecard_paid_hol_hour_class.php"); //時間有休追加 20120213
require_once("date_utils.php"); //時間有休追加 20120213

class duty_shift_report_common_class
{
    var $file_name;             // 呼び出し元ファイル名
    var $_db_con;               // DBコネクション
    var $obj;                   // 勤務シフト作成共通クラス
    var $obj_time;              // 勤務シフト作成 時刻 共通クラス
    var $obj_hol_hour;	        // 時間有休クラス 20120213
    var $wk_kijun_str;          // 「基準」文字列
    var $long_term_care_flg;    // 長期療養フラグ
    var $wk_duty_yyyymm;        // 年月WK
    var $wk_report_kbn;         // 届出区分WK
    var $ave_duty_times;        // 「1日平均看護配置数」を満たす「月延べ勤務時間数」
    var $ave_duty_times_plus;   // 「1日平均看護配置数」を満たす「月延べ勤務時間数」（看護職員＋看護補助者）
    var $top_colspan;           // セルの結合数
    var $wk_staffing_rate;      // 配置比率
    var $colspan_fc;            // 入院基本料施設基準セル結合数
    var $colspan_summary;       // 届出書添付書類(様式９集計表)セル結合数

	/*************************************************************************/
	// コンストラクタ
	// @param object $con DBコネクション
	// @param string $fname 画面名
	/*************************************************************************/
	function duty_shift_report_common_class($con, $fname)
	{
		// 呼び出し元ファイル名を保持
		$this->file_name = $fname;
		// DBコネクションを保持
		$this->_db_con = $con;
		// クラス new
		$this->obj = new duty_shift_common_class($con, $fname);
		$this->obj_time = new duty_shift_time_common_class($con, $fname);
		//時間有休追加 start 20120213
		$this->obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
		$this->obj_hol_hour->select();
		//時間有休追加 end 20120213

        $this->colspan_fc = array(
            "html" => array(1, 1, 1, 1, 2, 3),
            "excel" => array(2, 1, 4, 1, 5, 6)
        );

        $this->colspan_summary = array(
            "html" => array(1, 1, 1, 1, 1, 1),
            "excel" => array(1, 1, 1, 1, 4, 2)
        );
	}

//-------------------------------------------------------------------------------------------------------------------------
// 関数一覧（画面）
//-------------------------------------------------------------------------------------------------------------------------
    /*************************************************************************/
    //
    // 入院基本料の施設基準等に係る届出書添付書類(様式９)
    //
    /*************************************************************************/
    function showData(
                    $prt_flg,               //表示画面フラグ（１：看護職員表、２：看護補助職員表）
                    $excel_flg,             //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
                    $recomputation_flg,     //１ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
                    $data_array,            //看護師／准看護師情報
                    $data_sub_array,        //看護補助者情報
                    $hospital_name,         //保険医療機関名
                    $ward_cnt,              //病棟数
                    $sickbed_cnt,           //病床数
                    $report_kbn,            //届出区分
                    $report_kbn_name,       //届出区分名
                    $report_patient_cnt,    //届出時入院患者数
                    $hosp_patient_cnt,      //１日平均入院患者数
                    $compute_start_yyyy,    //１日平均入院患者数 算出期間（開始年）
                    $compute_start_mm,      //１日平均入院患者数 算出期間（開始月）
                    $compute_end_yyyy,      //１日平均入院患者数 算出期間（終了年）
                    $compute_end_mm,        //１日平均入院患者数 算出期間（終了月）
                    $nurse_sumtime,             //勤務時間数（看護師）
                    $sub_nurse_sumtime,         //勤務時間数（准看護師）
                    $assistance_nurse_sumtime,  //勤務時間数（看護補助者）
                    $nurse_cnt,             //看護師数
                    $sub_nurse_cnt,         //准看護師数
                    $assistance_nurse_cnt,  //看護補助者数
                    $hospitalization_cnt,   //平均在院日数
                    $ave_start_yyyy,        //平均在院日数 算出期間（開始年）
                    $ave_start_mm,          //平均在院日数 算出期間（開始月）
                    $ave_end_yyyy,          //平均在院日数 算出期間（終了年）
                    $ave_end_mm,            //平均在院日数 算出期間（終了月）
                    $prov_start_hh,         //夜勤時間帯 （開始時）
                    $prov_start_mm,         //夜勤時間帯 （開始分）
                    $prov_end_hh,           //夜勤時間帯 （終了時）
                    $prov_end_mm,           //夜勤時間帯 （終了分）
                    $duty_yyyy,             //対象年
                    $duty_mm,               //対象月
                    $day_cnt,               //日数
                    $date_y1,               //カレンダー用年
                    $date_m1,               //カレンダー用月
                    $date_d1,               //カレンダー用日
                    $labor_cnt,             //常勤職員の週所定労働時間
                    $create_ymd,            //作成年月日(yyyymmdd)
                    $nurse_staffing_flg,    //看護配置加算の有無
                    $acute_nursing_flg,     //急性期看護補助加算の有無
                    $acute_nursing_cnt,     //急性期看護補助加算の配置比率
                    $nursing_assistance_flg,    //看護補助加算の有無
                    $nursing_assistance_cnt,    //看護補助加算の配置比率
                    $acute_assistance_flg,  //急性期看護補助体制加算の届出区分
                    $acute_assistance_cnt,  //急性期看護補助体制加算の配置比率
                    $night_acute_assistance_flg,    //夜間急性期看護補助体制加算の届出区分
                    $night_acute_assistance_cnt,    //夜間急性期看護補助体制加算の配置比率
                    $night_shift_add_flg,   //看護職員夜間配置加算の有無
                    $specific_data,         //特定入院料関連データ
                    $send_hm,             //申し送り時間
                    $sum_night_b, //$sum_sub_night_gまで、excelの場合に指定
                    $sum_night_c,
                    $sum_night_d,
                    $sum_night_e,
                    $sum_night_f,
                    $sum_night_g,
                    $sum_sub_night_b,
                    $sum_sub_night_c,
                    $sum_sub_night_d,
                    $sum_sub_night_e,
                    $sum_sub_night_f,
                    $sum_sub_night_g,
                    $plan_result_flag       //予定実績フラグ 20120209
    ) {
        ///-----------------------------------------------------------------------------
        //初期値設定
        ///-----------------------------------------------------------------------------
        $data = "";
        // metaタグで文字コード設定必須（ない場合にはMacで文字化けするため）
        $data .= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Shift_JIS\">\n";
        if ($prt_flg == "2") {
            $this->top_colspan = "8";
        } else {
            $this->top_colspan = "9";
        }
        ///-----------------------------------------------------------------------------
        //現在日付の取得
        ///-----------------------------------------------------------------------------
        $date = getdate();
        $now_yyyy = $date["year"];
        $now_mm = $date["mon"];
        $now_dd = $date["mday"];

        //平成２４年度改訂対応 計算を前へ移動 20120406
        if ($this->wk_report_kbn >= 13) {
            $report_kbn2 = ($nursing_assistance_cnt != "") ? $nursing_assistance_cnt : "";
        } else {
            $report_kbn2 = ($acute_nursing_cnt != "") ? $acute_nursing_cnt : "";
        }
        //excelの場合は、引数を使用するため計算しない 20110901
        if ($excel_flg != "1") {
            $this->calcNightSumTime(
                    $data_array,
                    $day_cnt,
                    $labor_cnt,
                    &$sum_night_b,
                    &$sum_night_d,
                    &$sum_night_e
            );

//showDataSummaryメソッド内で$assistance_nurse_sumtime[勤務時間数（看護補助者）]をセットしているので、削除 20130509
//            //整数で表示
//            if ($report_kbn2 != "") {
//                $wk = ($hosp_patient_cnt / $report_kbn2) * 3;
//                $sum_sub_night_f = ceil($wk); //切り上げに変更 20100729
//            }
//            else {
//                $sum_sub_night_f = "";
//            }
        }

        //-------------------------------------------------------------------------------
        // 入院基本料施設基準
        //-------------------------------------------------------------------------------
        $data .= $this->showDataFacilityCriterion(
                    $prt_flg,               //表示画面フラグ（１：看護職員表、２：看護補助職員表）
                    $excel_flg,             //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
                    $hospital_name,         //保険医療機関名
                    $ward_cnt,              //病棟数
                    $sickbed_cnt,           //病床数
                    $report_kbn,            //届出区分
                    $report_kbn_name,       //届出区分名
                    $report_patient_cnt,    //届出時入院患者数
                    $duty_yyyy,             //対象年
                    $duty_mm,               //対象月
                    $create_ymd,            //作成年月日(yyyymmdd)
                    $nurse_staffing_flg,    //看護配置加算の有無
                    $acute_nursing_flg, //急性期看護補助加算の有無
                    $acute_nursing_cnt, //急性期看護補助加算の配置比率
                    $nursing_assistance_flg,    //看護補助加算の有無
                    $nursing_assistance_cnt,    //看護補助加算の配置比率
                    $acute_assistance_flg,  //急性期看護補助体制加算の届出区分
                    $acute_assistance_cnt,  //急性期看護補助体制加算の配置比率
                    $night_acute_assistance_flg,    //夜間急性期看護補助体制加算の届出区分
                    $night_acute_assistance_cnt,    //夜間急性期看護補助体制加算の配置比率
                    $night_shift_add_flg,   //看護職員夜間配置加算の有無
                    $specific_data,  //特定入院料関連データ
                    $plan_result_flag,      //予定実績フラグ 20120209
                    $recomputation_flg,     //１ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
                    $hosp_patient_cnt,
                    $hospitalization_cnt,
                    compact("date_y1", "date_m1", "date_d1"),    //カレンダー用年月日
                    $send_hm              //申し送り時間
        );

        //-------------------------------------------------------------------------------
        //届出書添付書類(様式９集計表)
        //-------------------------------------------------------------------------------
        $data .= $this->showDataSummary(
                    $excel_flg,                     //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
                    $recomputation_flg,             //１ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
                    $data_sub_array,                //看護補助者情報
                    $report_kbn,                    //届出区分
                    $report_kbn2,                   //届出区分（調整）
                    $hosp_patient_cnt,              //１日平均入院患者数
                    $compute_start_yyyy,            //１日平均入院患者数 算出期間（開始年）
                    $compute_start_mm,              //１日平均入院患者数 算出期間（開始月）
                    $compute_end_yyyy,              //１日平均入院患者数 算出期間（終了年）
                    $compute_end_mm,                //１日平均入院患者数 算出期間（終了月）
                    $nurse_sumtime,                 //勤務時間数（看護師）
                    $sub_nurse_sumtime,             //勤務時間数（准看護師）
                    $assistance_nurse_sumtime,      //勤務時間数（看護補助者）
                    $nurse_cnt,                     //看護師数
                    $sub_nurse_cnt,                 //准看護師数
                    $assistance_nurse_cnt,          //看護補助者数
                    $hospitalization_cnt,           //平均在院日数
                    $ave_start_yyyy,                //平均在院日数 算出期間（開始年）
                    $ave_start_mm,                  //平均在院日数 算出期間（開始月）
                    $ave_end_yyyy,                  //平均在院日数 算出期間（終了年）
                    $ave_end_mm,                    //平均在院日数 算出期間（終了月）
                    $prov_start_hh,                 //夜勤時間帯 （開始時）
                    $prov_start_mm,                 //夜勤時間帯 （開始分）
                    $prov_end_hh,                   //夜勤時間帯 （終了時）
                    $prov_end_mm,                   //夜勤時間帯 （終了分）
                    $duty_yyyy,                     //対象年
                    $day_cnt,                       //日数
                    $date_y1,                       //カレンダー用年
                    $date_m1,                       //カレンダー用月
                    $date_d1,                       //カレンダー用日
                    $labor_cnt,                     //常勤職員の週所定労働時間
                    $nurse_staffing_flg,            //看護配置加算の有無
                    $acute_nursing_flg,             //急性期看護補助加算の有無
                    $acute_nursing_cnt,             //急性期看護補助加算の配置比率
                    $nursing_assistance_cnt,        //看護補助加算の配置比率
                    $acute_assistance_cnt,          //急性期看護補助体制加算の配置比率
                    $night_acute_assistance_cnt,    //夜間急性期看護補助体制加算の配置比率
                    $specific_data,                 //特定入院料関連データ
                    $sum_night_b,                   //$sum_sub_night_gまで、excelの場合に指定
                    $sum_night_c,
                    $sum_night_d,
                    $sum_night_e,
                    $sum_night_f,
                    $sum_night_g,
                    $sum_night_h,
                    $sum_night_i,
                    $sum_sub_night_b,
                    $sum_sub_night_c,
                    $sum_sub_night_d,
                    $sum_sub_night_e,
                    $sum_sub_night_f,
                    $sum_sub_night_g
        );

        ///------------------------------------------------------------------------------
        //年月
        //今月の稼働日数
        //常勤職員の週所定労働時間
        ///------------------------------------------------------------------------------
        $data .= $this->showDataLaborTime($duty_yyyy, $duty_mm, $day_cnt, $labor_cnt, $excel_flg);

        ///------------------------------------------------------------------------------
        //データ設定
        ///------------------------------------------------------------------------------
        $wk_array = array();
        $wk_array["data"] = $data;
        //「看護師」「准看護師」
        $wk_array["sum_night_b"] = $sum_night_b;    //夜勤従事者数(夜勤ありの職員数)〔Ｂ〕
        $wk_array["sum_night_c"] = $sum_night_c;    //月延べ勤務時間数の計〔Ｃ〕
        $wk_array["sum_night_d"] = $sum_night_d;    //月延べ夜勤時間数の計〔Ｄ〕
        $wk_array["sum_night_e"] = $sum_night_e;    //夜勤専従者及び月16時間以下の者の夜勤時間数〔Ｅ〕
        $wk_array["sum_night_f"] = $sum_night_f;    //1日看護配置数〔(Ａ ／届出区分の数)×３〕
        $wk_array["sum_night_g"] = $sum_night_g;    //月平均１日あたり看護配置数〔Ｃ／(日数×８）〕
        $wk_array["sum_night_h"] = $sum_night_h;    //月平均１日当たり夜間看護配置数〔D／(日数×１６）〕
        $wk_array["sum_night_i"] = $sum_night_i;    //１日夜間看護配置数[A/12]※端数切上げ
        //「看護補助者」
        $wk_array["sum_sub_night_b"] = $sum_sub_night_b;    //夜勤従事者数(夜勤ありの職員数)〔Ｂ〕
        $wk_array["sum_sub_night_c"] = $sum_sub_night_c;    //月延べ勤務時間数の計〔Ｃ〕
        $wk_array["sum_sub_night_d"] = $sum_sub_night_d;    //月延べ夜勤時間数の計〔Ｄ〕
        $wk_array["sum_sub_night_e"] = $sum_sub_night_e;    //夜勤専従者及び月16時間以下の者の夜勤時間数〔Ｅ〕
        $wk_array["sum_sub_night_f"] = $sum_sub_night_f;    //1日看護配置数〔(A ／届出区分の数)×３〕
        $wk_array["sum_sub_night_g"] = $sum_sub_night_g;    //月平均１日あたり看護配置数〔Ｃ／(日数×８）〕

        return $wk_array;
    }

	/*************************************************************************/
	//
	// ＨＩＤＤＥＮ
	//
	/*************************************************************************/
    function showHidden(
                        $session,               //セッション
                        $standard_id,           //施設基準ＩＤ
                        $recomputation_flg,     //１ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
                        $night_start_day,       //４週計算用（開始日）
                        $hospital_name,         //保険医療機関名
                        $ward_cnt,              //病棟数
                        $sickbed_cnt,           //病床数
                        $report_kbn,            //届出区分
                        $report_kbn_name,       //届出区分名
                        $report_patient_cnt,    //届出時入院患者数
                        $hosp_patient_cnt,      //１日平均入院患者数
                        $compute_start_yyyy,    //１日平均入院患者数 算出期間（開始年）
                        $compute_start_mm,      //１日平均入院患者数 算出期間（開始月）
                        $compute_end_yyyy,      //１日平均入院患者数 算出期間（終了年）
                        $compute_end_mm,        //１日平均入院患者数 算出期間（終了月）
                        $nurse_sumtime,             //勤務時間数（看護師）
                        $sub_nurse_sumtime,         //勤務時間数（准看護師）
                        $assistance_nurse_sumtime,  //勤務時間数（看護補助者）
                        $nurse_cnt,             //看護師数
                        $sub_nurse_cnt,         //准看護師数
                        $assistance_nurse_cnt,  //看護補助者数
                        $hospitalization_cnt,   //平均在院日数
                        $ave_start_yyyy,        //平均在院日数 算出期間（開始年）
                        $ave_start_mm,          //平均在院日数 算出期間（開始月）
                        $ave_end_yyyy,          //平均在院日数 算出期間（終了年）
                        $ave_end_mm,            //平均在院日数 算出期間（終了月）
                        $prov_start_hh,         //夜勤時間帯 （開始時）
                        $prov_start_mm,         //夜勤時間帯 （開始分）
                        $prov_end_hh,           //夜勤時間帯 （終了時）
                        $prov_end_mm,           //夜勤時間帯 （終了分）
                        $duty_yyyy,             //対象年
                        $duty_mm,               //対象月
                        $date_y1,               //カレンダー用年
                        $date_m1,               //カレンダー用月
                        $date_d1,               //カレンダー用日
                        $labor_cnt,             //常勤職員の週所定労働時間
                        $create_ymd             //作成年月日(yyyymmdd)
    ) {

        echo("<input type=\"hidden\" name=\"session\" value=\"$session\">\n");

        echo("<input type=\"hidden\" name=\"standard_id\" value=\"$standard_id\">\n");

        echo("<input type=\"hidden\" name=\"recomputation_flg\" value=\"$recomputation_flg\">\n");
        echo("<input type=\"hidden\" name=\"night_start_day\" value=\"$night_start_day\">\n");

        echo("<input type=\"hidden\" name=\"hospital_name\" value=\"$hospital_name\">\n");
        echo("<input type=\"hidden\" name=\"ward_cnt\" value=\"$ward_cnt\">\n");
        echo("<input type=\"hidden\" name=\"sickbed_cnt\" value=\"$sickbed_cnt\">\n");

        echo("<input type=\"hidden\" name=\"report_kbn\" value=\"$report_kbn\">\n");
        echo("<input type=\"hidden\" name=\"report_kbn_name\" value=\"$report_kbn_name\">\n");
        echo("<input type=\"hidden\" name=\"report_patient_cnt\" value=\"$report_patient_cnt\">\n");

        echo("<input type=\"hidden\" name=\"hosp_patient_cnt\" value=\"$hosp_patient_cnt\">\n");

        echo("<input type=\"hidden\" name=\"compute_start_yyyy\" value=\"$compute_start_yyyy\">\n");
        echo("<input type=\"hidden\" name=\"compute_start_mm\" value=\"$compute_start_mm\">\n");
        echo("<input type=\"hidden\" name=\"compute_end_yyyy\" value=\"$compute_end_yyyy\">\n");
        echo("<input type=\"hidden\" name=\"compute_end_mm\" value=\"$compute_end_mm\">\n");

        echo("<input type=\"hidden\" name=\"nurse_sumtime\" value=\"$nurse_sumtime\">\n");
        echo("<input type=\"hidden\" name=\"sub_nurse_sumtime\" value=\"$sub_nurse_sumtime\">\n");
        echo("<input type=\"hidden\" name=\"assistance_nurse_sumtime\" value=\"$assistance_nurse_sumtime\">\n");

        echo("<input type=\"hidden\" name=\"nurse_cnt\" value=\"$nurse_cnt\">\n");
        echo("<input type=\"hidden\" name=\"sub_nurse_cnt\" value=\"$sub_nurse_cnt\">\n");
        echo("<input type=\"hidden\" name=\"assistance_nurse_cnt\" value=\"$assistance_nurse_cnt\">\n");

        echo("<input type=\"hidden\" name=\"hospitalization_cnt\" value=\"$hospitalization_cnt\">\n");

        echo("<input type=\"hidden\" name=\"ave_start_yyyy\" value=\"$ave_start_yyyy\">\n");
        echo("<input type=\"hidden\" name=\"ave_start_mm\" value=\"$ave_start_mm\">\n");
        echo("<input type=\"hidden\" name=\"ave_end_yyyy\" value=\"$ave_end_yyyy\">\n");
        echo("<input type=\"hidden\" name=\"ave_end_mm\" value=\"$ave_end_mm\">\n");

        echo("<input type=\"hidden\" name=\"prov_start_hh\" value=\"$prov_start_hh\">\n");
        echo("<input type=\"hidden\" name=\"prov_start_mm\" value=\"$prov_start_mm\">\n");
        echo("<input type=\"hidden\" name=\"prov_end_hh\" value=\"$prov_end_hh\">\n");
        echo("<input type=\"hidden\" name=\"prov_end_mm\" value=\"$prov_end_mm\">\n");

        echo("<input type=\"hidden\" name=\"duty_yyyy\" value=\"$duty_yyyy\">\n");
        echo("<input type=\"hidden\" name=\"duty_mm\" value=\"$duty_mm\">\n");

        echo("<input type=\"hidden\" name=\"date_y1\"   value=\"$date_y1\">\n");
        echo("<input type=\"hidden\" name=\"date_m1\"   value=\"$date_m1\">\n");
        echo("<input type=\"hidden\" name=\"date_d1\"   value=\"$date_d1\">\n");

        echo("<input type=\"hidden\" name=\"labor_cnt\" value=\"$labor_cnt\">\n");

        echo("<input type=\"hidden\" name=\"create_yyyy\" value=\"{$create_ymd["create_yyyy"]}\">\n");
        echo("<input type=\"hidden\" name=\"create_mm\" value=\"{$create_ymd["create_mm"]}\">\n");
        echo("<input type=\"hidden\" name=\"create_dd\" value=\"{$create_ymd["create_dd"]}\">\n");
    }

//-------------------------------------------------------------------------------------------------------------------------
// 関数一覧（新規作成：ＤＢ読み込み）
//-------------------------------------------------------------------------------------------------------------------------
    /**
     * 様式９用　看護職員情報取得
     *
     * @param   $standard_id        施設基準ＩＤ
     * @param   $duty_yyyy          勤務年
     * @param   $duty_mm            勤務月
     * @param   $night_start_day    表集計（開始日）
     * @param   $end_yyyy           表集計（終了年）
     * @param   $end_mm             表集計（終了月）
     * @param   $end_dd             表集計（終了日）
     * @param   $day_cnt            日数
     * @param   $standard_array     施設基準情報（履歴）
     * @param   $group_id           グループID
     * @param   $plan_result_flag   実績予定フラグ  1:実績 2:予定 3:実績と予定の組合せ
     * @param   $draft_reg_flg      下書きデータ登録状態
     *
     * @return  $data   取得情報
     */
    function get_report_array(
                            $standard_id,
                            $duty_yyyy,
                            $duty_mm,
                            $night_start_day,
                            $end_yyyy,
                            $end_mm,
                            $end_dd,
                            $day_cnt,
                            $standard_array,
                            $group_id,
                            $plan_result_flag,
                            $draft_reg_flg = ""
    ) {
        //看護職員判定用配列
        $arr_nurse_chk = array("看護師", "准看護師", "保健師", "助産師");

        // system_config
        $conf = new Cmx_SystemConfig();
        
        // 委員会・WG連携
        $pjt_link = $conf->get('project.link_to_yoshiki9');
        if (empty($pjt_link)) {
            $pjt_link = "f";
        }
        ///-----------------------------------------------------------------------------
        //ＤＢ(stmst)より役職情報取得
        //ＤＢ(jobmst)より職種情報取得
        //ＤＢ(empmst)より職員情報を取得
        //ＤＢ(wktmgrp)より勤務グループパターン情報を取得
        ///-----------------------------------------------------------------------------
//      $data_st = $this->obj->get_stmst_array();
//      $data_job = $this->obj->get_jobmst_array();
//      $data_emp = $this->obj->get_empmst_array("");
        $data_emp = $this->obj->get_standard_empmst_array($standard_id, $duty_yyyy, $duty_mm);
//処理速度改善 連想配列、配列位置を保持して処理
        //職員情報
        $arr_emp_idx = array();

        for ($i=0; $i<count($data_emp); $i++) {
            $wk_emp_id = $data_emp[$i]["id"];
            //配列上の位置
            $arr_emp_idx[$wk_emp_id] = $i;
        }

        $data_wktmgrp = $this->obj->get_wktmgrp_array();
        $arr_nurse = $this->get_nurse_job_id(1);
        $arr_sub_nurse = $this->get_nurse_job_id(2);
        $arr_assist_job = $this->get_arr_assist_job();

        // 申し送り時間を差し引かない職種、パターン
        $arr_no_subtract = $this->get_arr_no_subtract();

        // 半日応援情報 20091020
        $arr_half_assist = $this->obj->get_half_assist_array();

        //勤務開始時刻が前日になる場合がある勤務パターン 20131004
        $arr_previous_day_possible_flag = $this->get_arr_previous_day_possible_flag();
        
        ///-----------------------------------------------------------------------------
        //カレンダー情報
        ///-----------------------------------------------------------------------------
        //日またがりの時間を加算するため、１日前を設定
        if ((int)$night_start_day == 1) {
            $wk_start_yyyy = (int)$duty_yyyy;
            $wk_start_mm = (int)$duty_mm - 1;
            if ((int)$wk_start_mm < 1) {
                $wk_start_yyyy--;
                $wk_start_mm = 12;
            }
            $wk_start_dd = $this->obj->days_in_month($wk_start_yyyy, $wk_start_mm);
        } else {
            $wk_start_yyyy = (int)$duty_yyyy;
            $wk_start_mm = (int)$duty_mm;
            $wk_start_dd = (int)$night_start_day - 1;
        }
        //カレンダー情報
        $start_date = sprintf("%04d%02d%02d",$wk_start_yyyy, $wk_start_mm, $wk_start_dd);
        $end_date = sprintf("%04d%02d%02d",$end_yyyy, $end_mm, $end_dd);
        //翌日 20111019
        $end_date = $this->obj->next_date($end_date);
        $calendar_array = $this->obj->get_calendar_array($start_date, $end_date);
        ///-----------------------------------------------------------------------------
        //出勤表の勤務時間帯情報を取得
        ///-----------------------------------------------------------------------------
        for ($i=0; $i<count($data_wktmgrp); $i++) {
            $officehours_array[$i] = $this->obj_time->get_officehours_array($data_wktmgrp[$i]["id"]);
            $officehours_array[$i]["pattern_id"] = $data_wktmgrp[$i]["id"];
        }

        ///-----------------------------------------------------------------------------
        // 勤務実績情報を取得
        ///-----------------------------------------------------------------------------
        //指定施設基準の対象スタッフ（グループ）のみ設定
        //2009/01/13 応援追加分も含める
        $cond_add = "";
        for ($k=0; $k<count($data_emp); $k++) {
            $wk_id = $data_emp[$k]["id"];
            if ($cond_add == "") {
                $cond_add .= "and (a.emp_id = '$wk_id' ";
            } else {
                $cond_add .= "or a.emp_id = '$wk_id' ";
            }
        }
        if ($cond_add != "") {
            $cond_add .= ") ";
        }
        //予定実績フラグ 1:実績 2:予定 3:実績と予定の組合せ
        if ($plan_result_flag == "1") { //実績
            $rslt_array = $this->obj->get_atdbkrslt_history_array($standard_id, $duty_yyyy, $duty_mm, $cond_add, $calendar_array, $group_id);
        }
        elseif ($plan_result_flag == "2") { //予定
            $rslt_array = $this->obj->get_atdbk_for_report($cond_add, $calendar_array, $standard_id, $group_id, $draft_reg_flg);
        }
        else { //3:実績と予定の組合せ
            $today = date("Ymd");
            //開始日が当日以前の場合、実績取得
            if ($start_date <= $today) {
                $rslt_array = $this->obj->get_atdbkrslt_history_array($standard_id, $duty_yyyy, $duty_mm, $cond_add, $calendar_array, $group_id);
            }
            else { //開始日が当日より後の場合、実績は不要
                $rslt_array = array();
            }
            //終了日より当日が前の場合（以前）、予定取得
            if ($end_date >= $today) {
                $plan_array = $this->obj->get_atdbk_for_report($cond_add, $calendar_array, $standard_id, $group_id, $draft_reg_flg);
            }
            else { //終了日 < 当日 （終了日が過ぎたので、実績使用で予定は不要）
                $plan_array = array();
            }
            $rslt_array = $this->obj->get_atdbkrslt_atdbk_for_report($cond_add, $calendar_array, $standard_id, $group_id, $rslt_array, $plan_array);

        }

        //エラー処理
        if (isset($rslt_array["error_msg"])) {
            //エラーメッセージがセットされていたら、そのまま返す
            return $rslt_array;
        }

        //時間有休追加 start 20120213
        $arr_hol_hour_info = array();
        if ($this->obj_hol_hour->paid_hol_hour_flag == "t") {
            $arr_emp_id = array();
            for ($i=0; $i<count($data_emp); $i++) {
                $wk_emp_id = $data_emp[$i]["id"];
                $arr_emp_id[] = $wk_emp_id;
            }
            $arr_hol_hour_info = $this->obj_hol_hour->get_paid_hol_hour_emp_month($arr_emp_id, $start_date, $end_date);

        }
        //時間有休追加 end 20120213
        //個人別所定時間を優先するための対応 20120309
        $this->obj_time->get_empcond_officehours_pattern();
        $this->obj_time->get_arr_weekday_flag($start_date, $end_date);

        //控除しない委員会WG取得 20140410
        if ($pjt_link == "t") {
            $arr_no_sub_pjt = $this->get_no_subtract_project();
        }
        
        ///-----------------------------------------------------------------------------
        //職員の振り分け
        //看護職員情報設定
        //補助看護職員情報設定
        ///-----------------------------------------------------------------------------
        $data_cnt=0;

        $nurse_cnt = 0;                 //看護師数
        $sub_nurse_cnt = 0;             //准看護師数
        $assistance_nurse_cnt = 0;      //看護補助者数

        $nurse_sumtime = 0;             //看護師総勤務時間
        $sub_nurse_sumtime = 0;         //准看護師総勤務時間
        $assistance_nurse_sumtime = 0;  //看護補助者総勤務時間

        $data_array = array();
        $data_sub_array = array();
        for ($i=0; $i<count($rslt_array); $i++) {
            //実績と予定の組合せの場合で、日毎データがない職員は除く
            if ($plan_result_flag == "3") {
                if ($rslt_array[$i]["day_data_cnt"] == "0" ||
                        $rslt_array[$i]["day_data_cnt"] == "") {
                    continue;
                }
            }
            ///-----------------------------------------------------------------------------
            //勤務日区分の設定
            ///-----------------------------------------------------------------------------
            //※注意、data_arrayの項目_$k 1:前月末、2:1日、31:30日、32:31日
            for ($k=1; $k<=count($calendar_array); $k++) {
                $rslt_array[$i]["officehours_id_$k"] = $calendar_array[$k - 1]["type"];
            }
            //-------------------------------------------------------------------
            //スタッフ情報を取得
            //ＤＢ(duty_shift_staff_employmen)より勤務シフトスタッフ情報(個別勤務条件)を取得
            //-------------------------------------------------------------------
            $wk_emp_id = $rslt_array[$i]["staff_id"];
            $wk_emp_idx = $arr_emp_idx[$wk_emp_id];
            $wk_job_name = $data_emp[$wk_emp_idx]["job_name"];

            //個人別所定時間を優先するための対応 20120309
            $this->obj_time->get_arr_empcond_officehours($wk_emp_id,$start_date); //個人別勤務時間帯履歴対応 20130220
            //委員会、除外リスト対応 20140408
            if ($pjt_link == "t") {
                $arr_project_data = $this->get_project_data($wk_emp_id, $start_date, $end_date, $arr_no_sub_pjt);
            }
            else {
                $arr_project_data = array();
            }
            $project_data_cnt = count($arr_project_data);
            ///-----------------------------------------------------------------------------
            //看護職員情報設定
            ///-----------------------------------------------------------------------------
            $job_id = $data_emp[$wk_emp_idx]["job_id"];
            if (in_array(trim($wk_job_name), $arr_nurse_chk) or in_array($job_id, $arr_nurse) or in_array($job_id, $arr_sub_nurse)) {
                $m = $data_cnt;
                $data_array[$m] = $rslt_array[$i];
                $data_array[$m]["name"] = $data_emp[$wk_emp_idx]["name"];
                $data_array[$m]["job_id"] = $data_emp[$wk_emp_idx]["job_id"];
                $data_array[$m]["job_name"] = $data_emp[$wk_emp_idx]["job_name"];

                $data_array[$m]["night_staff_cnt"] = $data_emp[$wk_emp_idx]["night_staff_cnt"]; //夜勤従事者への計上人数
                $data_array[$m]["night_duty_flg"] = $data_emp[$wk_emp_idx]["night_duty_flg"];       //夜勤の有無（１：有り、２：夜専）
                $data_array[$m]["other_post_flg"] = $data_emp[$wk_emp_idx]["other_post_flg"];       //他部署兼務（１：有り）
                $data_array[$m]["duty_form"] = $data_emp[$wk_emp_idx]["duty_form"];             //勤務形態（1:常勤、2:非常勤）
                //除外リスト対応 20140414
                $data_array[$m]["emp_personal_id"] = $data_emp[$wk_emp_idx]["emp_personal_id"];
                $data_array[$m]["project_data"] = $arr_project_data;
                ///-----------------------------------------------------------------------------
                //勤務時間数算出（月）
                ///-----------------------------------------------------------------------------
                for ($k=1; $k<=count($calendar_array); $k++) {
                    // 施設基準確認 2009/01/13
                    if ($rslt_array[$i]["standard_id_$k"] != $standard_id) {
                        continue;
                    }
                    $wk_group_id = $rslt_array[$i]["pattern_id_$k"];
                    $wk_atdptn_ptn_id = $rslt_array[$i]["atdptn_ptn_id_$k"];
                    //日付
                    $wk_date = $calendar_array[$k - 1]["date"];
                    $wk_empcond_officehours_flg = false;
                    //予定の場合、または、組合せで当日以降の場合
                    $today = date("Ymd");
                    if ($plan_result_flag == "2" ||
                            ($plan_result_flag == "3" && $wk_date >= $today) ) {
                        //個人別所定時間を優先する場合 20120309
                        if ($this->obj_time->arr_empcond_officehours_ptn[$wk_group_id][$wk_atdptn_ptn_id] == 1) {
                            $weekday_flg = $this->obj_time->arr_weekday_flg[$wk_date];
                            $arr_empcond_officehours = $this->obj_time->get_empcond_officehours($weekday_flg,$wk_date); //個人別勤務時間帯履歴対応 20130220
                            if ($arr_empcond_officehours["officehours2_start"] != "") {
                                $rslt_array[$i]["start_time_$k"] = $arr_empcond_officehours["officehours2_start"];
                                $rslt_array[$i]["end_time_$k"] = $arr_empcond_officehours["officehours2_end"];
                                $wk_empcond_officehours_flg = true;
                            }
                        }
                    }

                    if (($rslt_array[$i]["start_time_$k"] != "") &&
                        ($rslt_array[$i]["end_time_$k"] != "")) {

                        //半日応援対応
                        //20140408
                        if ($arr_half_assist["$wk_group_id"]["$wk_atdptn_ptn_id"]["assist_start_time"] != "") {
                            $wk_assist_start_time = $arr_half_assist["$wk_group_id"]["$wk_atdptn_ptn_id"]["assist_start_time"];
                            $wk_assist_end_time = $arr_half_assist["$wk_group_id"]["$wk_atdptn_ptn_id"]["assist_end_time"];
                        } else {
                            $wk_assist_start_time = "";
                            $wk_assist_end_time = "";
                        }
                        // 20140414
                        $data_array[$m]["assist_start_time_$k"] = $wk_assist_start_time;
                        $data_array[$m]["assist_end_time_$k"] = $wk_assist_end_time;
                        //会議・研修
                        $wk_meeting_start_time = $rslt_array[$i]["meeting_start_time_$k"];
                        $wk_meeting_end_time = $rslt_array[$i]["meeting_end_time_$k"];
                        
                        ///-----------------------------------------------------------------------------
                        //日勤時間（月）
                        //夜勤時間（月）
                        ///-----------------------------------------------------------------------------
                        //時間有休追加 start 20120213
                        $wk_hol_hour_start_time = "";
                        $wk_hol_hour_end_time = "";
                        if ($this->obj_hol_hour->paid_hol_hour_flag == "t" &&
                                !$wk_empcond_officehours_flg //個人別所定時間を優先する場合以外
                        ) {
                            $wk_staff_id = $rslt_array[$i]["staff_id"];
                            $wk_hol_hour_start_date = $arr_hol_hour_info[$wk_staff_id][$wk_date]["start_date"];
                            if ($wk_hol_hour_start_date != "") {
                                $wk_hol_hour_start_time = $arr_hol_hour_info[$wk_staff_id][$wk_date]["start_time"];
                                $wk_hol_hour_start_date_time = $wk_hol_hour_start_date.$wk_hol_hour_start_time;
                                //終了時刻を求める
                                $wk_use_hour = $arr_hol_hour_info[$wk_staff_id][$wk_date]["use_hour"];
                                $wk_use_minute = $arr_hol_hour_info[$wk_staff_id][$wk_date]["use_minute"];
                                $wk_minute = $wk_use_hour * 60 + $wk_use_minute;

                                $wk_hol_hour_end_date_time = date_utils::add_minute_ymdhi($wk_hol_hour_start_date_time, $wk_minute);
                                $wk_hol_hour_end_time = substr($wk_hol_hour_end_date_time, 8, 4);
                                // 20140414
                                $data_array[$m]["hol_hour_start_time_$k"] = $wk_hol_hour_start_time;
                                $data_array[$m]["hol_hour_end_time_$k"] = $wk_hol_hour_end_time;
                                //時間有休がある場合、勤務開始終了時刻を所定時刻とする
                                $wk_pattern_id = $rslt_array[$i]["pattern_id_$k"];
                                $wk_atdptn_ptn_id = $rslt_array[$i]["atdptn_ptn_id_$k"];
                                if ($wk_pattern_id >= 1 && $wk_atdptn_ptn_id >= 1) {
                                    $wk_officehours_id = $rslt_array[$i]["officehours_id_$k"];
                                    $wk_officehours_id = ($wk_officehours_id == 2 || $wk_officehours_id == 3) ? $wk_officehours_id : 1;
                                    $wk_pattern_idx = $wk_pattern_id - 1;
                                    $wk_start_hhmm = "start_hhmm_" . $wk_atdptn_ptn_id . "_" . $wk_officehours_id . "_2";
                                    $wk_end_hhmm = "end_hhmm_" . $wk_atdptn_ptn_id . "_" . $wk_officehours_id . "_2";
                                    $rslt_array[$i]["start_time_$k"] = $officehours_array[$wk_pattern_idx][$wk_start_hhmm];
                                    $rslt_array[$i]["end_time_$k"] = $officehours_array[$wk_pattern_idx][$wk_end_hhmm];
                                }

                            }
                        }
                        //時間有休追加 end 20120213
                        //委員会、除外リスト対応 201404
                        if ($project_data_cnt == 0) {
                            $arr_project_time = array();
                        }
                        else {
                            $arr_project_time = $arr_project_data[$wk_date];
                            //翌日の委員会対応
                            $next_date = $this->obj->next_date($wk_date);
                            $p_max = count($arr_project_data[$next_date]);
                            if ($p_max > 0) {
                                $prev_idx = $k - 1;
                                for ($p_idx=0; $p_idx<$p_max; $p_idx++) {
                                    //退勤時刻前のデータを追加設定
                                    if ($arr_project_data[$next_date][$p_idx]["start_time"] <= $rslt_array[$i]["end_time_$k"] &&
                                            $rslt_array[$i]["next_day_flag_$k"] == "1"
                                    ) {
                                        $arr_project_data[$next_date][$p_idx]["next_day_flag"] = "1";
                                        $arr_project_time[] = $arr_project_data[$next_date][$p_idx];
                                    }
                                }
                            }
                        }
                        $time_array = $this->obj_time->get_duty_night_times2(
                                $rslt_array[$i]["officehours_id_$k"],
                                $rslt_array[$i]["pattern_id_$k"],
                                $rslt_array[$i]["atdptn_ptn_id_$k"],
                                $rslt_array[$i]["start_time_$k"],
                                $rslt_array[$i]["end_time_$k"],
                                $standard_array,
                                $officehours_array,
                                $rslt_array[$i]["meeting_time_$k"],
                                $data_emp[$wk_emp_idx]["job_id"],
                                $arr_no_subtract,
                                $rslt_array[$i]["out_time_$k"],
                                $rslt_array[$i]["ret_time_$k"],
                                $wk_meeting_start_time,
                                $wk_meeting_end_time,
                                $wk_hol_hour_start_time,        //時間有休追加 20120213
                                $wk_hol_hour_end_time,      //時間有休追加 20120213
                                $wk_date,
                                $wk_assist_start_time,
                                $wk_assist_end_time,
                                $arr_project_time
                                );
                        //実績の前日フラグと時間帯の前日になる場合があるフラグを確認（所定0:30,実績23:40のようなケース）20131004
                        $wk_prev_possible = $arr_previous_day_possible_flag[$wk_group_id][$wk_atdptn_ptn_id];
                        if ($rslt_array[$i]["previous_day_flag_$k"] != "1" || $wk_prev_possible == "1") {
                            ///-----------------------------------------------------------------------------
                            //設定（当日）
                            ///-----------------------------------------------------------------------------
                            if ($k > 1) {
                                $p = $k - 1;
                                //小数点以下２位まで表示、四捨五入 20120522
                                $data_array[$m]["duty_time_$p"] += sprintf("%01.2f", $time_array["duty_time_to"]);
                                $data_array[$m]["night_time_$p"] += sprintf("%01.2f", $time_array["night_time_to"]);
                            }
                            ///-----------------------------------------------------------------------------
                            //設定（翌日）
                            ///-----------------------------------------------------------------------------
                            $data_array[$m]["duty_time_$k"] += sprintf("%01.2f", $time_array["duty_time_next"]);
                            $data_array[$m]["night_time_$k"] += sprintf("%01.2f", $time_array["night_time_next"]);
                        } else {
                            //前日フラグがONの場合
                            ///-----------------------------------------------------------------------------
                            //設定（当日）
                            ///-----------------------------------------------------------------------------
                            if ($k > 2) {
                                $p = $k - 2;
                                $data_array[$m]["duty_time_$p"] += sprintf("%01.2f", $time_array["duty_time_to"]);
                                $data_array[$m]["night_time_$p"] += sprintf("%01.2f", $time_array["night_time_to"]);
                            }
                            ///-----------------------------------------------------------------------------
                            //設定（翌日）
                            ///-----------------------------------------------------------------------------
                            if ($k > 1) {
                                $p = $k - 1;
                                $data_array[$m]["duty_time_$p"] += sprintf("%01.2f", $time_array["duty_time_next"]);
                                $data_array[$m]["night_time_$p"] += sprintf("%01.2f", $time_array["night_time_next"]);
                            }
                        }

                        //グループ情報
                        $data_array[$m]["assist_group_id_$k"] = $rslt_array[$i]["assist_group_id_$k"];
                        //除外リスト対応20140414
                        $data_array[$m]["officehours_start_hhmm_$k"] = $time_array["officehours_start_hhmm"];
                        $data_array[$m]["officehours_end_hhmm_$k"] = $time_array["officehours_end_hhmm"];
                    }
                }
                ///-----------------------------------------------------------------------------
                //人数カウント
                ///-----------------------------------------------------------------------------
                $data_cnt++;
                if (trim($data_emp[$wk_emp_idx]["job_name"]) === "准看護師" or in_array($job_id, $arr_sub_nurse)) {
                    $sub_nurse_cnt++;
                } else {
                    $nurse_cnt++;
                }
            }
            ///-----------------------------------------------------------------------------
            //補助看護職員情報設定
            ///-----------------------------------------------------------------------------
            if (in_array($data_emp[$wk_emp_idx]["job_id"], $arr_assist_job)) {
                $m = $assistance_nurse_cnt;
                $data_sub_array[$m] = $rslt_array[$i];
                $data_sub_array[$m]["name"] = $data_emp[$wk_emp_idx]["name"];
                $data_sub_array[$m]["job_id"] = $data_emp[$wk_emp_idx]["job_id"];
                $data_sub_array[$m]["job_name"] = $data_emp[$wk_emp_idx]["job_name"];
                $data_sub_array[$m]["night_staff_cnt"] = $data_emp[$wk_emp_idx]["night_staff_cnt"]; //夜勤従事者への計上人数
                $data_sub_array[$m]["night_duty_flg"] = $data_emp[$wk_emp_idx]["night_duty_flg"];       //夜勤の有無（１：有り、２：夜専）
                $data_sub_array[$m]["other_post_flg"] = $data_emp[$wk_emp_idx]["other_post_flg"];       //他部署兼務（１：有り）
                $data_sub_array[$m]["duty_form"] = $data_emp[$wk_emp_idx]["duty_form"];             //勤務形態（1:常勤、2:非常勤）
                //除外リスト対応 20140414
                $data_sub_array[$m]["emp_personal_id"] = $data_emp[$wk_emp_idx]["emp_personal_id"];
                $data_sub_array[$m]["project_data"] = $arr_project_data;
                ///-----------------------------------------------------------------------------
                //勤務時間数算出（月）
                ///-----------------------------------------------------------------------------
                for ($k=1; $k<=count($calendar_array); $k++) {
                    // 施設基準確認 2009/01/13
                    if ($rslt_array[$i]["standard_id_$k"] != $standard_id) {
                        continue;
                    }
                    //個人別所定時間を優先する場合 20120309
                    $wk_group_id = $rslt_array[$i]["pattern_id_$k"];
                    $wk_atdptn_ptn_id = $rslt_array[$i]["atdptn_ptn_id_$k"];
                    //日付
                    $wk_date = $calendar_array[$k - 1]["date"];
                    $wk_empcond_officehours_flg = false;
                    //予定の場合、または、組合せで当日以降の場合
                    $today = date("Ymd");
                    if ($plan_result_flag == "2" ||
                            ($plan_result_flag == "3" && $wk_date >= $today) ) {
                        //個人別所定時間を優先する場合 20120309
                        if ($this->obj_time->arr_empcond_officehours_ptn[$wk_group_id][$wk_atdptn_ptn_id] == 1) {
                            $weekday_flg = $this->obj_time->arr_weekday_flg[$wk_date];
                            $arr_empcond_officehours = $this->obj_time->get_empcond_officehours($weekday_flg,$wk_date); //個人別勤務時間帯履歴対応 20130220
                            if ($arr_empcond_officehours["officehours2_start"] != "") {
                                $rslt_array[$i]["start_time_$k"] = $arr_empcond_officehours["officehours2_start"];
                                $rslt_array[$i]["end_time_$k"] = $arr_empcond_officehours["officehours2_end"];
                                $wk_empcond_officehours_flg = true;
                            }
                        }
                    }
                    if (($rslt_array[$i]["start_time_$k"] != "") &&
                        ($rslt_array[$i]["end_time_$k"] != "")) {
                        //半日応援対応
                        $wk_group_id = $rslt_array[$i]["pattern_id_$k"];
                        $wk_atdptn_ptn_id = $rslt_array[$i]["atdptn_ptn_id_$k"];
                        //20140408
                        if ($arr_half_assist["$wk_group_id"]["$wk_atdptn_ptn_id"]["assist_start_time"] != "") {
                            $wk_assist_start_time = $arr_half_assist["$wk_group_id"]["$wk_atdptn_ptn_id"]["assist_start_time"];
                            $wk_assist_end_time = $arr_half_assist["$wk_group_id"]["$wk_atdptn_ptn_id"]["assist_end_time"];
                        } else {
                            $wk_assist_start_time = "";
                            $wk_assist_end_time = "";
                        }
                        // 20140414
                        $data_sub_array[$m]["assist_start_time_$k"] = $wk_assist_start_time;
                        $data_sub_array[$m]["assist_end_time_$k"] = $wk_assist_end_time;
                        //会議・研修
                        $wk_meeting_start_time = $rslt_array[$i]["meeting_start_time_$k"];
                        $wk_meeting_end_time = $rslt_array[$i]["meeting_end_time_$k"];
                        //日付
                        $wk_date = $calendar_array[$k - 1]["date"];
                        //時間有休追加 start 20120213
                        $wk_hol_hour_start_time = "";
                        $wk_hol_hour_end_time = "";
                        if ($this->obj_hol_hour->paid_hol_hour_flag == "t" &&
                                !$wk_empcond_officehours_flg //個人別所定時間を優先する場合以外
                        ) {
                            $wk_staff_id = $rslt_array[$i]["staff_id"];
                            $wk_hol_hour_start_date = $arr_hol_hour_info[$wk_staff_id][$wk_date]["start_date"];
                            if ($wk_hol_hour_start_date != "") {
                                $wk_hol_hour_start_time = $arr_hol_hour_info[$wk_staff_id][$wk_date]["start_time"];
                                $wk_hol_hour_start_date_time = $wk_hol_hour_start_date.$wk_hol_hour_start_time;
                                //終了時刻を求める
                                $wk_use_hour = $arr_hol_hour_info[$wk_staff_id][$wk_date]["use_hour"];
                                $wk_use_minute = $arr_hol_hour_info[$wk_staff_id][$wk_date]["use_minute"];
                                $wk_minute = $wk_use_hour * 60 + $wk_use_minute;

                                $wk_hol_hour_end_date_time = date_utils::add_minute_ymdhi($wk_hol_hour_start_date_time, $wk_minute);
                                $wk_hol_hour_end_time = substr($wk_hol_hour_end_date_time, 8, 4);
                                // 20140414
                                $data_sub_array[$m]["hol_hour_start_time_$k"] = $wk_hol_hour_start_time;
                                $data_sub_array[$m]["hol_hour_end_time_$k"] = $wk_hol_hour_end_time;
                                //時間有休がある場合、勤務開始終了時刻を所定時刻とする
                                $wk_pattern_id = $rslt_array[$i]["pattern_id_$k"];
                                $wk_atdptn_ptn_id = $rslt_array[$i]["atdptn_ptn_id_$k"];
                                if ($wk_pattern_id >= 1 && $wk_atdptn_ptn_id >= 1) {
                                    $wk_officehours_id = $rslt_array[$i]["officehours_id_$k"];
                                    $wk_officehours_id = ($wk_officehours_id == 2 || $wk_officehours_id == 3) ? $wk_officehours_id : 1;
                                    $wk_pattern_idx = $wk_pattern_id - 1;
                                    $wk_start_hhmm = "start_hhmm_" . $wk_atdptn_ptn_id . "_" . $wk_officehours_id . "_2";
                                    $wk_end_hhmm = "end_hhmm_" . $wk_atdptn_ptn_id . "_" . $wk_officehours_id . "_2";
                                    $rslt_array[$i]["start_time_$k"] = $officehours_array[$wk_pattern_idx][$wk_start_hhmm];
                                    $rslt_array[$i]["end_time_$k"] = $officehours_array[$wk_pattern_idx][$wk_end_hhmm];
                                }
                            }
                        }
                        //時間有休追加 end 20120213
                        if ($project_data_cnt == 0) {
                            $arr_project_time = array();
                        }
                        else {
                            $arr_project_time = $arr_project_data[$wk_date];
                            //翌日の委員会対応
                            $next_date = $this->obj->next_date($wk_date);
                            $p_max = count($arr_project_data[$next_date]);
                            if ($p_max > 0) {
                                for ($p_idx=0; $p_idx<$p_max; $p_idx++) {
                                    //退勤時刻前のデータを追加設定
                                    if ($arr_project_data[$next_date][$p_idx]["start_time"] <= $rslt_array[$i]["end_time_$k"] &&
                                            $rslt_array[$i]["next_day_flag_$k"] == "1"
                                    ) {
                                        $arr_project_data[$next_date][$p_idx]["next_day_flag"] = "1";
                                        $arr_project_time[] = $arr_project_data[$next_date][$p_idx];
                                    }
                                }
                            }
                        }
                        ///-----------------------------------------------------------------------------
                        //日勤時間（月）
                        //夜勤時間（月）
                        ///-----------------------------------------------------------------------------
                        $time_array = $this->obj_time->get_duty_night_times2(
                                $rslt_array[$i]["officehours_id_$k"],
                                $rslt_array[$i]["pattern_id_$k"],
                                $rslt_array[$i]["atdptn_ptn_id_$k"],
                                $rslt_array[$i]["start_time_$k"],
                                $rslt_array[$i]["end_time_$k"],
                                $standard_array,
                                $officehours_array,
                                $rslt_array[$i]["meeting_time_$k"],
                                $data_emp[$wk_emp_idx]["job_id"],
                                $arr_no_subtract,
                                $rslt_array[$i]["out_time_$k"],
                                $rslt_array[$i]["ret_time_$k"],
                                $wk_meeting_start_time,
                                $wk_meeting_end_time,
                                $wk_hol_hour_start_time,        //時間有休追加 20120213
                                $wk_hol_hour_end_time,      //時間有休追加 20120213
                                $wk_date,
                                $wk_assist_start_time,
                                $wk_assist_end_time,
                                $arr_project_time
                                );
                        //実績の前日フラグと時間帯の前日になる場合があるフラグを確認（所定0:30,実績23:40のようなケース）20131004
                        $wk_prev_possible = $arr_previous_day_possible_flag[$wk_group_id][$wk_atdptn_ptn_id];
                        if ($rslt_array[$i]["previous_day_flag_$k"] != "1" || $wk_prev_possible == "1") {
                            ///-----------------------------------------------------------------------------
                            //設定（当日）
                            ///-----------------------------------------------------------------------------
                            if ($k > 1) {
                                $p = $k - 1;
                                $data_sub_array[$m]["duty_time_$p"] += sprintf("%01.2f", $time_array["duty_time_to"]);
                                $data_sub_array[$m]["night_time_$p"] += sprintf("%01.2f", $time_array["night_time_to"]);
                            }
                            ///-----------------------------------------------------------------------------
                            //設定（翌日）
                            ///-----------------------------------------------------------------------------
                            $data_sub_array[$m]["duty_time_$k"] += sprintf("%01.2f", $time_array["duty_time_next"]);
                            $data_sub_array[$m]["night_time_$k"] += sprintf("%01.2f", $time_array["night_time_next"]);
                        } else {
                            //前日フラグがONの場合
                            ///-----------------------------------------------------------------------------
                            //設定（当日）
                            ///-----------------------------------------------------------------------------
                            if ($k > 2) {
                                $p = $k - 2;
                                $data_sub_array[$m]["duty_time_$p"] += sprintf("%01.2f", $time_array["duty_time_to"]);
                                $data_sub_array[$m]["night_time_$p"] += sprintf("%01.2f", $time_array["night_time_to"]);
                            }
                            ///-----------------------------------------------------------------------------
                            //設定（翌日）
                            ///-----------------------------------------------------------------------------
                            if ($k > 1) {
                                $p = $k - 1;
                                $data_sub_array[$m]["duty_time_$p"] += sprintf("%01.2f", $time_array["duty_time_next"]);
                                $data_sub_array[$m]["night_time_$p"] += sprintf("%01.2f", $time_array["night_time_next"]);
                            }
                        }
                        //グループ情報
                        $data_sub_array[$m]["assist_group_id_$k"] = $rslt_array[$i]["assist_group_id_$k"];
                        //除外リスト対応20140414
                        $data_sub_array[$m]["officehours_start_hhmm_$k"] = $time_array["officehours_start_hhmm"];
                        $data_sub_array[$m]["officehours_end_hhmm_$k"] = $time_array["officehours_end_hhmm"];
                    }
                }
                ///-----------------------------------------------------------------------------
                //人数カウント
                ///-----------------------------------------------------------------------------
                $assistance_nurse_cnt++;
            }
        }
        //半日応援分
        $half_assist_array = $this->obj->get_rslt_assist_array($standard_id, $duty_yyyy, $duty_mm, $cond_add, $calendar_array, $group_id, $rslt_array, $plan_result_flag);

        //rsltにない職員分を追加する
        $cnt = count($rslt_array);
        $arr_group_name = array();
        if (count($half_assist_array) > $cnt) {
            $wk_group_info = $this->obj->get_group_id_from_standard_id($standard_id);
            for ($i=0; $i<count($wk_group_info); $i++) {
                $wk_id = $wk_group_info[$i]["group_id"];
                $wk_name = $wk_group_info[$i]["group_name"];
                $arr_group_name[$wk_id] = $wk_name;
            }
        }
        for ($i=$cnt; $i<count($half_assist_array); $i++) {
            if ($i >= $cnt) {
                $rslt_array[$i]["staff_id"] = $half_assist_array[$i]["staff_id"];
                $rslt_array[$i]["staff_name"] = $half_assist_array[$i]["staff_name"];
                $rslt_array[$i]["st_nm"] = $half_assist_array[$i]["st_nm"];
                //$rslt_array[$i]["staff_id"] = $half_assist_array[$i]["staff_id"];
                $rslt_array[$i]["main_group_id"] = $half_assist_array[$i]["main_group_id"];
                $wk_id = $rslt_array[$i]["main_group_id"];
                $rslt_array[$i]["main_group_name"] = $arr_group_name[$wk_id];
            }
        }
        //半日応援データ数、看護職種（並び替えを行う判定のために使用）
        $half_assist_cnt1 = 0;
        //半日応援データ数、看護補助
        $half_assist_cnt2 = 0;
        for ($i=0; $i<count($rslt_array); $i++) {
            //職員別の応援データ件数確認
            if ($half_assist_array[$i]["cnt"] == 0) {
                continue;
            }
            ///-----------------------------------------------------------------------------
            //勤務日区分の設定
            ///-----------------------------------------------------------------------------
            for ($k=1; $k<=count($calendar_array); $k++) {
                $half_assist_array[$i]["officehours_id_$k"] = $calendar_array[$k - 1]["type"];
            }
            //-------------------------------------------------------------------
            //スタッフ情報を取得
            //ＤＢ(duty_shift_staff_employmen)より勤務シフトスタッフ情報(個別勤務条件)を取得
            //-------------------------------------------------------------------
            //$staff_one = $this->obj->get_duty_shift_staff_one($rslt_array[$i]["staff_id"], $data_st, $data_job, $data_emp);
            //$staff_employmen = $this->obj->get_duty_shift_staff_employmen($rslt_array[$i]["staff_id"]);
            $wk_emp_id = $rslt_array[$i]["staff_id"];
            $wk_emp_idx = $arr_emp_idx[$wk_emp_id];
            $wk_job_name = $data_emp[$wk_emp_idx]["job_name"];
            ///-----------------------------------------------------------------------------
            //看護職員情報設定
            ///-----------------------------------------------------------------------------
            $job_id = $data_emp[$wk_emp_idx]["job_id"];
            if (in_array(trim($wk_job_name), $arr_nurse_chk) or in_array($job_id, $arr_nurse) or in_array($job_id, $arr_sub_nurse)) {
                //半日応援データ数、看護職種
                if ($i >= $cnt) {
                    $half_assist_cnt1++;
                }
                //職員位置設定
                //既に存在するか確認
                $wk_exist_flg = false;
                for ($wk_idx=0; $wk_idx<count($data_array); $wk_idx++) {
                    if ($group_id == "0") { //全病棟一括、グループ、職員IDを比較
                        if ($rslt_array[$i]["staff_id"] == $data_array[$wk_idx]["staff_id"] &&
                                $rslt_array[$i]["main_group_id"] == $data_array[$wk_idx]["main_group_id"]) {
                            $wk_exist_flg = true;
                            break;
                        }
                    }
                    else { //病棟グループ指定
                        if ($rslt_array[$i]["staff_id"] == $data_array[$wk_idx]["staff_id"]) {
                            $wk_exist_flg = true;
                            break;
                        }
                    }
                }
                //既にある場合
                if ($wk_exist_flg) {
                    $m = $wk_idx;
                } else {
                //ない場合、追加
                    $m = $data_cnt;
                    $data_cnt++;
                    //人数カウント
                    if (trim($data_emp[$wk_emp_idx]["job_name"]) === "准看護師" or in_array($job_id, $arr_sub_nurse)) {
                        $sub_nurse_cnt++;
                    } else {
                        $nurse_cnt++;
                    }
                    $data_array[$m] = $rslt_array[$i];
                    $data_array[$m]["name"] = $data_emp[$wk_emp_idx]["name"];
                    $data_array[$m]["job_id"] = $data_emp[$wk_emp_idx]["job_id"];
                    $data_array[$m]["job_name"] = $data_emp[$wk_emp_idx]["job_name"];
                    $data_array[$m]["night_staff_cnt"] = $data_emp[$wk_emp_idx]["night_staff_cnt"]; //夜勤従事者への計上人数
                    $data_array[$m]["night_duty_flg"] = $data_emp[$wk_emp_idx]["night_duty_flg"];       //夜勤の有無（１：有り、２：夜専）
                    $data_array[$m]["other_post_flg"] = $data_emp[$wk_emp_idx]["other_post_flg"];       //他部署兼務（１：有り）
                    $data_array[$m]["duty_form"] = $data_emp[$wk_emp_idx]["duty_form"];             //勤務形態（1:常勤、2:非常勤）
                }
                for ($k=1; $k<=count($calendar_array); $k++) {
                    // 施設基準確認
                    if ($half_assist_array[$i]["standard_id_$k"] != $standard_id) {
                        continue;
                    }
                    if (($half_assist_array[$i]["assist_start_time_$k"] != "") &&
                            ($half_assist_array[$i]["assist_end_time_$k"] != "")) {
                        //日付
                        $wk_date = $calendar_array[$k - 1]["date"];
                        //時間取得
                        $time_array = $this->obj_time->get_duty_night_times(
                                $half_assist_array[$i]["officehours_id_$k"],
                                $half_assist_array[$i]["pattern_id_$k"],
                                $half_assist_array[$i]["atdptn_ptn_id_$k"],
                                $half_assist_array[$i]["assist_start_time_$k"],
                                $half_assist_array[$i]["assist_end_time_$k"],
                                $standard_array,
                                $officehours_array,
                                "",
                                $data_emp[$wk_emp_idx]["job_id"],
                                $arr_no_subtract,
                                "",
                                "",
                                "",
                                "",
                                $wk_date
                                );

                        if ($half_assist_array[$i]["previous_day_flag_$k"] != "1") {
                            ///-----------------------------------------------------------------------------
                            //設定（当日）
                            ///-----------------------------------------------------------------------------
                            if ($k > 1) {
                                $p = $k - 1;
                                $data_array[$m]["duty_time_$p"] += sprintf("%01.2f", $time_array["duty_time_to"]);
                                $data_array[$m]["night_time_$p"] += sprintf("%01.2f", $time_array["night_time_to"]);
                            }
                            ///-----------------------------------------------------------------------------
                            //設定（翌日）
                            ///-----------------------------------------------------------------------------
                            $data_array[$m]["duty_time_$k"] += sprintf("%01.2f", $time_array["duty_time_next"]);
                            $data_array[$m]["night_time_$k"] += sprintf("%01.2f", $time_array["night_time_next"]);
                        } else {
                            //前日フラグがONの場合
                            ///-----------------------------------------------------------------------------
                            //設定（当日）
                            ///-----------------------------------------------------------------------------
                            if ($k > 2) {
                                $p = $k - 2;
                                $data_array[$m]["duty_time_$p"] += sprintf("%01.2f", $time_array["duty_time_to"]);
                                $data_array[$m]["night_time_$p"] += sprintf("%01.2f", $time_array["night_time_to"]);
                            }
                            ///-----------------------------------------------------------------------------
                            //設定（翌日）
                            ///-----------------------------------------------------------------------------
                            if ($k > 1) {
                                $p = $k - 1;
                                $data_array[$m]["duty_time_$p"] += sprintf("%01.2f", $time_array["duty_time_next"]);
                                $data_array[$m]["night_time_$p"] += sprintf("%01.2f", $time_array["night_time_next"]);
                            }
                        }

                    }
                }
            }
            ///-----------------------------------------------------------------------------
            //補助看護職員情報設定
            ///-----------------------------------------------------------------------------
            if (in_array(trim($data_emp[$wk_emp_idx]["job_id"]), $arr_assist_job)) {
                //半日応援データ数、看護補助
                if ($i >= $cnt) {
                    $half_assist_cnt2++;
                }
                //職員位置設定
                //既に存在するか確認
                $wk_exist_flg = false;
                for ($wk_idx=0; $wk_idx<count($data_sub_array); $wk_idx++) {
                    if ($group_id == "0") { //全病棟一括指定
                        if ($rslt_array[$i]["staff_id"] == $data_sub_array[$wk_idx]["staff_id"] &&
                                $rslt_array[$i]["main_group_id"] == $data_sub_array[$wk_idx]["main_group_id"]) {
                            $wk_exist_flg = true;
                            break;
                        }
                    }
                    else { //病棟グループ指定
                        if ($rslt_array[$i]["staff_id"] == $data_sub_array[$wk_idx]["staff_id"]) {
                            $wk_exist_flg = true;
                            break;
                        }
                    }
                }
                //既にある場合
                if ($wk_exist_flg) {
                    $m = $wk_idx;
                } else {
                //ない場合、追加
                    $m = $assistance_nurse_cnt;
                    $assistance_nurse_cnt++;
                    $data_sub_array[$m] = $rslt_array[$i];
                    $data_sub_array[$m]["name"] = $data_emp[$wk_emp_idx]["name"];
                    $data_sub_array[$m]["job_id"] = $data_emp[$wk_emp_idx]["job_id"];
                    $data_sub_array[$m]["job_name"] = $data_emp[$wk_emp_idx]["job_name"];
                    $data_sub_array[$m]["night_staff_cnt"] = $data_emp[$wk_emp_idx]["night_staff_cnt"]; //夜勤従事者への計上人数
                    $data_sub_array[$m]["night_duty_flg"] = $data_emp[$wk_emp_idx]["night_duty_flg"];       //夜勤の有無（１：有り、２：夜専）
                    $data_sub_array[$m]["other_post_flg"] = $data_emp[$wk_emp_idx]["other_post_flg"];       //他部署兼務（１：有り）
                    $data_sub_array[$m]["duty_form"] = $data_emp[$wk_emp_idx]["duty_form"];             //勤務形態（1:常勤、2:非常勤）
                }
                for ($k=1; $k<=count($calendar_array); $k++) {
                    // 施設基準確認
                    if ($half_assist_array[$i]["standard_id_$k"] != $standard_id) {
                        continue;
                    }
                    if (($half_assist_array[$i]["assist_start_time_$k"] != "") &&
                            ($half_assist_array[$i]["assist_end_time_$k"] != "")) {
                        //日付
                        $wk_date = $calendar_array[$k - 1]["date"];
                        //時間取得
                        $time_array = $this->obj_time->get_duty_night_times(
                                $half_assist_array[$i]["officehours_id_$k"],
                                $half_assist_array[$i]["pattern_id_$k"],
                                $half_assist_array[$i]["atdptn_ptn_id_$k"],
                                $half_assist_array[$i]["assist_start_time_$k"],
                                $half_assist_array[$i]["assist_end_time_$k"],
                                $standard_array,
                                $officehours_array,
                                "",
                                $data_emp[$wk_emp_idx]["job_id"],
                                $arr_no_subtract,
                                "",
                                "",
                                "",
                                "",
                                $wk_date
                                );

                        if ($half_assist_array[$i]["previous_day_flag_$k"] != "1") {
                            ///-----------------------------------------------------------------------------
                            //設定（当日）
                            ///-----------------------------------------------------------------------------
                            if ($k > 1) {
                                $p = $k - 1;
                                $data_sub_array[$m]["duty_time_$p"] += sprintf("%01.2f", $time_array["duty_time_to"]);
                                $data_sub_array[$m]["night_time_$p"] += sprintf("%01.2f", $time_array["night_time_to"]);
                            }
                            ///-----------------------------------------------------------------------------
                            //設定（翌日）
                            ///-----------------------------------------------------------------------------
                            $data_sub_array[$m]["duty_time_$k"] += sprintf("%01.2f", $time_array["duty_time_next"]);
                            $data_sub_array[$m]["night_time_$k"] += sprintf("%01.2f", $time_array["night_time_next"]);
                        } else {
                            //前日フラグがONの場合
                            ///-----------------------------------------------------------------------------
                            //設定（当日）
                            ///-----------------------------------------------------------------------------
                            if ($k > 2) {
                                $p = $k - 2;
                                $data_sub_array[$m]["duty_time_$p"] += sprintf("%01.2f", $time_array["duty_time_to"]);
                                $data_sub_array[$m]["night_time_$p"] += sprintf("%01.2f", $time_array["night_time_to"]);
                            }
                            ///-----------------------------------------------------------------------------
                            //設定（翌日）
                            ///-----------------------------------------------------------------------------
                            if ($k > 1) {
                                $p = $k - 1;
                                $data_sub_array[$m]["duty_time_$p"] += sprintf("%01.2f", $time_array["duty_time_next"]);
                                $data_sub_array[$m]["night_time_$p"] += sprintf("%01.2f", $time_array["night_time_next"]);
                            }
                        }

                    }
                }
            }
        }
        //debug
        //echo("<!-- 上は今までの人数をカウントアップしていた場所、下は職員別の時間数算出追加 -->\n");
        $nurse_cnt = 0;                 //看護師数
        $sub_nurse_cnt = 0;             //准看護師数
        $assistance_nurse_cnt = 0;      //看護補助者数
        //職員別に当月の時間があるか確認(前月末を除く) 20100826
        $arr_emp_total = array_fill(0, count($data_array), 0);
        ///-----------------------------------------------------------------------------
        //合計設定（看護職員情報設定）
        ///-----------------------------------------------------------------------------
        for ($i=0; $i<count($data_array); $i++){
            //夜勤専従の確認 1日1時間未満に条件変更 20120312
            $wk_duty_time_flg = true;
            for ($k=1; $k<=$day_cnt; $k++) {
                if (trim($data_array[$i]["job_name"]) == "准看護師" or in_array($data_array[$i]["job_id"], $arr_sub_nurse)) {
                    $sub_nurse_sumtime += $data_array[$i]["duty_time_$k"];
                    $sub_nurse_sumtime += $data_array[$i]["night_time_$k"];
                } else {
                    $nurse_sumtime += $data_array[$i]["duty_time_$k"];
                    $nurse_sumtime += $data_array[$i]["night_time_$k"];
                }
                //if ($k > 1) { //1日（ついたち）のみ、登録時、人数がカウントされない
                    $arr_emp_total[$i] += $data_array[$i]["duty_time_$k"];
                    $arr_emp_total[$i] += $data_array[$i]["night_time_$k"];
                //}
                //夜勤専従の確認 1時間以上は夜勤専従ではない
                if ($data_array[$i]["duty_time_$k"] >= 1) {
                    $wk_duty_time_flg = false;
                }
            }
            if ($arr_emp_total[$i] > 0) {
                if (trim($data_array[$i]["job_name"]) === "准看護師" or in_array($data_array[$i]["job_id"], $arr_sub_nurse)) {
                    $sub_nurse_cnt++;
                } else {
                    $nurse_cnt++;
                }
            }
            //夜勤専従の確認 20120220
            $data_array[$i]["night_duty_flg"] = "";
            if ($wk_duty_time_flg) {
	            //個人別所定時間を優先するための対応 20141218
	            $this->obj_time->get_arr_empcond_officehours($data_array[$i]["staff_id"],$start_date); //個人別勤務時間帯履歴対応
                $wk_night_duty_flg = $this->obj_time->check_night_duty_flg($data_array[$i]["staff_id"], $calendar_array, $officehours_array, $standard_array, $plan_result_flag);
                if ($wk_night_duty_flg) {
                    $data_array[$i]["night_duty_flg"] = "2";
                }
            }
        }

        $arr_sub_total = array_fill(0, count($data_sub_array), 0);
        ///-----------------------------------------------------------------------------
        //合計設定（補助看護職員情報設定）
        ///-----------------------------------------------------------------------------
        for ($i=0; $i<count($data_sub_array); $i++){
            $wk_duty_time_flg = true;
            for ($k=1; $k<=$day_cnt; $k++) {
                $assistance_nurse_sumtime += $data_sub_array[$i]["duty_time_$k"];
                $assistance_nurse_sumtime += $data_sub_array[$i]["night_time_$k"];
                //if ($k > 1) {
                    $arr_sub_total[$i] += $data_sub_array[$i]["duty_time_$k"];
                    $arr_sub_total[$i] += $data_sub_array[$i]["night_time_$k"];
                //}
                if ($data_sub_array[$i]["duty_time_$k"] >= 1) {
                    $wk_duty_time_flg = false;
                }
            }
            if ($arr_sub_total[$i] > 0) {
                $assistance_nurse_cnt++;
            }
            //夜勤専従の確認 20120220
            $data_sub_array[$i]["night_duty_flg"] = "";
            if ($wk_duty_time_flg) {
	            //個人別所定時間を優先するための対応 20141218
	            $this->obj_time->get_arr_empcond_officehours($data_sub_array[$i]["staff_id"],$start_date); //個人別勤務時間帯履歴対応
                $wk_night_duty_flg = $this->obj_time->check_night_duty_flg($data_sub_array[$i]["staff_id"], $calendar_array, $officehours_array, $standard_array, $plan_result_flag);
                if ($wk_night_duty_flg) {
                    $data_sub_array[$i]["night_duty_flg"] = "2";
                }
            }
        }
        //予定、予定実績組合せの場合は合計が0の職員を除く
        if ($plan_result_flag == "2" || $plan_result_flag == "3") {
            $data_array_bak = $data_array;
            $data_array = array();
            $wk_nurse_idx = 0;
            for ($i=0; $i<count($data_array_bak); $i++){
                if ($arr_emp_total[$i] > 0) {
                    $data_array[$wk_nurse_idx] = $data_array_bak[$i];
                    $wk_nurse_idx++;
                }
            }

            $data_sub_array_bak = $data_sub_array;
            $data_sub_array = array();
            $wk_nurse_idx = 0;
            for ($i=0; $i<count($data_sub_array_bak); $i++){
                if ($arr_sub_total[$i] > 0) {
                    $data_sub_array[$wk_nurse_idx] = $data_sub_array_bak[$i];
                    $wk_nurse_idx++;
                }
            }
        }

        ///-----------------------------------------------------------------------------
        //データ設定
        ///-----------------------------------------------------------------------------
        $data = array();
        //並び替えを行う
        if ($group_id == "0") { //全病棟一括
            if ($half_assist_cnt1 > 0) {
                $arr_group_id_pos = array(); //確認用

                $wk_pos = 0;
                for ($i=0; $i<count($data_array); $i++) {
                    $wk_group_id = $data_array[$i]["main_group_id"];
                    if (!in_array($wk_group_id, $arr_group_id_pos)) {
                        $arr_group_id_pos[$wk_pos] = $wk_group_id;
                        $wk_pos++;
                    }
                }
                //グループ毎に設定し直す
                $data_array_bak = $data_array;
                $data_array = array();
                $rslt_idx = 0;
                for ($i=0; $i<count($arr_group_id_pos); $i++) {
                    $wk_group_id = $arr_group_id_pos[$i];
                    for ($j=0; $j<count($data_array_bak); $j++) {
                        if ($data_array_bak[$j]["main_group_id"] == $wk_group_id) {
                            $data_array[$rslt_idx] = $data_array_bak[$j];
                            $rslt_idx++;
                        }
                    }
                }
            }
            if ($half_assist_cnt2 > 0) {
                $arr_group_id_pos = array(); //確認用

                $wk_pos = 0;
                for ($i=0; $i<count($data_sub_array); $i++) {
                    $wk_group_id = $data_sub_array[$i]["main_group_id"];
                    if (!in_array($wk_group_id, $arr_group_id_pos)) {
                        $arr_group_id_pos[$wk_pos] = $wk_group_id;
                        $wk_pos++;
                    }
                }
                //グループ毎に設定し直す
                $data_sub_array_bak = $data_sub_array;
                $data_sub_array = array();
                $rslt_idx = 0;
                for ($i=0; $i<count($arr_group_id_pos); $i++) {
                    $wk_group_id = $arr_group_id_pos[$i];
                    for ($j=0; $j<count($data_sub_array_bak); $j++) {
                        if ($data_sub_array_bak[$j]["main_group_id"] == $wk_group_id) {
                            $data_sub_array[$rslt_idx] = $data_sub_array_bak[$j];
                            $rslt_idx++;
                        }
                    }
                }
            }
        }
        $data["data_array"] = $data_array;
        $data["data_sub_array"] = $data_sub_array;

        $data["data_cnt"] = $data_cnt;  //看護師数＋准看護師数

        $data["nurse_cnt"] = $nurse_cnt;                        //看護師数
        $data["sub_nurse_cnt"] = $sub_nurse_cnt;                //准看護師数
        $data["assistance_nurse_cnt"] = $assistance_nurse_cnt;  //看護補助者数

        $data["nurse_sumtime"] = $nurse_sumtime;                        //看護師総勤務時間
        $data["sub_nurse_sumtime"] = $sub_nurse_sumtime;                //准看護師総勤務時間
        $data["assistance_nurse_sumtime"] = $assistance_nurse_sumtime;  //看護補助者総勤務時間;

        return $data;
    }

    /***
     * 看護職員の職種取得
     */
	function get_nurse_job_id($switch) {

		$nurse = array();

        switch ($switch) {
            case 1:
                $sql = "select * from duty_shift_rpt_nurse order by no";
                break;
            case 2:
                $sql = "select * from duty_shift_rpt_sub_nurse order by no";
                break;
            default:
                $sql = "select * from duty_shift_rpt_assist_job order by no";
                break;
        }

        $sel = select_from_table($this->_db_con, $sql, '', $this->file_name);
		if ($sel === 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
        while ($row = pg_fetch_assoc($sel)) {
			$nurse[] = $row['job_id'];
		}
		return $nurse;
	}


    /***
     * 看護職員の職種名取得
     */
	function get_nurse_job_name($switch) {

		$nurse = array();

        switch ($switch) {
            case 1:
                $sql = "select * from duty_shift_rpt_nurse join jobmst using (job_id) order by no";
                break;
            case 2:
                $sql = "select * from duty_shift_rpt_sub_nurse join jobmst using (job_id) order by no";
                break;
            default:
                $sql = "select * from duty_shift_rpt_assist_job join jobmst using (job_id) order by no";
                break;
        }

        $sel = select_from_table($this->_db_con, $sql, '', $this->file_name);
		if ($sel === 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
        while ($row = pg_fetch_assoc($sel)) {
			$nurse[] = trim($row['job_nm']);
		}

        switch ($switch) {
            case 1:
                return array_unique(array_merge($nurse, array('看護師','保健師','助産師')));
                break;
            case 2:
                return array_unique(array_merge($nurse, array('准看護師')));
                break;
            default:
        		return array_unique($nurse);
                break;
        }
	}

    /***
     * 看護補助職員の職種取得
     */
	function get_arr_assist_job() {
        return $this->get_nurse_job_id(3);
	}

	// 申し送り時間を差し引かない職種取得
	function get_arr_no_subtract_job() {

		$arr_no_subtract_job = array();

		$sql = "select * from duty_shift_rpt_no_subtract_job";
		$cond = "order by no";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num = pg_num_rows($sel);

		for ($i=0; $i<$num; $i++) {
			$arr_no_subtract_job[$i] = pg_fetch_result($sel,$i,"job_id");
		}

		return $arr_no_subtract_job;
	}
	// 申し送り時間を差し引かない勤務パターン取得
	function get_arr_no_subtract_ptn() {

		$arr_no_subtract_ptn = array();

		$sql = "select * from duty_shift_rpt_no_subtract_ptn";
		$cond = "order by no";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num = pg_num_rows($sel);

		for ($i=0; $i<$num; $i++) {
			$arr_no_subtract_ptn[$i] = pg_fetch_result($sel,$i,"pattern_id")."_".pg_fetch_result($sel,$i,"atdptn_ptn_id");
		}

		return $arr_no_subtract_ptn;
	}
	// 申し送り時間を差し引かない職種・勤務パターン取得 1_1_1形式（職種_グループID_勤務パターンID）
	function get_arr_no_subtract() {

		$arr_no_subtract = array();

		$sql = "select * from duty_shift_rpt_no_subtract";
		$cond = "order by no";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num = pg_num_rows($sel);

		for ($i=0; $i<$num; $i++) {
			$arr_no_subtract[$i] = pg_fetch_result($sel,$i,"job_id")."_".pg_fetch_result($sel,$i,"pattern_id")."_".pg_fetch_result($sel,$i,"atdptn_ptn_id");
		}

		return $arr_no_subtract;
	}

    /*************************************************************************/
    // 入院基本料施設基準
    /*************************************************************************/
    function showDataFacilityCriterion(
                    $prt_flg,               //表示画面フラグ（１：看護職員表、２：看護補助職員表）
                    $excel_flg,             //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
                    $hospital_name,         //保険医療機関名
                    $ward_cnt,              //病棟数
                    $sickbed_cnt,           //病床数
                    $report_kbn,            //届出区分
                    $report_kbn_name,       //届出区分名
                    $report_patient_cnt,    //届出時入院患者数
                    $duty_yyyy,             //対象年
                    $duty_mm,               //対象月
                    $create_ymd,            //作成年月日(yyyymmdd)
                    $nurse_staffing_flg,    //看護配置加算の有無
                    $acute_nursing_flg,     //急性期看護補助加算の有無
                    $acute_nursing_cnt,     //急性期看護補助加算の配置比率
                    $nursing_assistance_flg,        //看護補助加算の有無
                    $nursing_assistance_cnt,        //看護補助加算の配置比率
                    $acute_assistance_flg,          //急性期看護補助体制加算の届出区分
                    $acute_assistance_cnt,          //急性期看護補助体制加算の配置比率
                    $night_acute_assistance_flg,    //夜間急性期看護補助体制加算の届出区分
                    $night_acute_assistance_cnt,    //夜間急性期看護補助体制加算の配置比率
                    $night_shift_add_flg,   //看護職員夜間配置加算の有無
                    $specific_data,         //特定入院料関連データ
                    $plan_result_flag,      //予定実績フラグ 20120209
                    $recomputation_flg,     //１ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
                    $hosp_patient_cnt,      //１日平均入院患者数
                    $hospitalization_cnt,   //平均在院日数
                    $calendar_ymd,          //カレンダー用年月日
                    $send_hm                //申し送り時間
    ) {
        // タイトルに予定実績の文言追加 20120209
        $wk_plan_result_str = "";
        switch ($plan_result_flag) {
            case "1":
                $wk_plan_result_str = "・実績";
                break;
            case "2":
                $wk_plan_result_str = "・予定";
                break;
            case "3":
                $wk_plan_result_str = "・予定と実績";
                break;
        }

        $wk_title = "入院基本料施設基準" . $wk_plan_result_str;

        if ($excel_flg == "1") {
            $data  = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"non_list\">\n";
            $data .= "<tr style=\"font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;\" height=\"30\">\n";
            $data .= "<td nowrap align=\"left\" colspan=\"{$this->colspan_fc["excel"][0]}\" valign=\"bottom\"><b>" . $wk_title . "</b></td>\n";
            $data .= "<td nowrap align=\"right\" colspan=\"{$this->colspan_fc["excel"][5]}\" valign=\"bottom\">作成年月日　";
            $data .= $create_ymd["create_yyyy"] . "年";
            $data .= $create_ymd["create_mm"] . "月";
            $data .= $create_ymd["create_dd"] . "日";
        } else {
            $data  = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"non_list outer\">\n";
            $data .= "<tr style=\"font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;\" height=\"30\">\n";
            $data .= "<td nowrap align=\"left\" valign=\"bottom\"><b>" . $wk_title . "</b></td>\n";
            $data .= "<td nowrap align=\"right\" valign=\"bottom\">作成年月日\n";
            $data .= "<input type=\"text\" size=\"4\" maxlength=\"4\" name=\"inp_create_yyyy\" value=\"" . $create_ymd["create_yyyy"] . "\">年\n";
            $data .= "<input type=\"text\" size=\"2\" maxlength=\"2\" name=\"inp_create_mm\" value=\"" . $create_ymd["create_mm"] . "\">月\n";
            $data .= "<input type=\"text\" size=\"2\" maxlength=\"2\" name=\"inp_create_dd\" value=\"" . $create_ymd["create_dd"] . "\">日\n";
        }
        $data .= "</td>\n";
        $data .= "</tr>\n";
        $data .= "</table>";

///////////
        if ($excel_flg == "1") {
            $data .= "<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;\">\n";
        } else {
            $data .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list outer\" style=\"font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;\">\n";
        }
////////////

        //-------------------------------------------------------------------------------
        // 病棟数、１日平均入院患者数
        //-------------------------------------------------------------------------------
        $data .= "<tr>\n";

        //病棟数
        $wk_title = "病棟数";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"{$this->colspan_fc["excel"][0]}\" class=\"basis_shift\">{$wk_title}</td>\n";
            $data .= "<td nowrap align=\"center\" class=\"basis_shift\">" . $ward_cnt . "</td>\n";
        } else {
            $data .= "<td align=\"right\" style=\"width: 350px;\" class=\"basis_shift\">{$wk_title}</td>\n";
            $data .= "<td align=\"left\" style=\"width: 150px;\" class=\"basis_shift\">" . $ward_cnt . "</td>\n";
        }

        //１日平均入院患者数
        $wk_title = "１日平均入院患者数〔A〕 ※端数切上げ整数入力";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"{$this->colspan_fc["excel"][2]}\" class=\"basis_shift_input\">{$wk_title}</td>\n";
            $data .= "<td align=\"center\" colspan=\"1\" class=\"basis_shift_input\">{$hosp_patient_cnt}人</td>\n";
        } else {
            $data .= "<td align=\"right\" style=\"width: 350px;\" class=\"basis_shift_input\">{$wk_title}</td>\n";
            $data .= "<td align=\"left\" class=\"basis_shift_input\">";
            $data .= "<input type=\"text\" size=\"10\" maxlength=\"10\" name=\"hosp_patient_cnt\" value=\"{$hosp_patient_cnt}\" style=\"text-align: right; ime-mode: disabled;\">人";
            $data .= "</td>\n";
        }

        $data .= "</tr>\n";

        //-------------------------------------------------------------------------------
        // 病床数、平均在院日数
        //-------------------------------------------------------------------------------
        $data .= "<tr class=\"basis_shift\">\n";
        $wk_title = "病床数";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"{$this->colspan_fc["excel"][0]}\">{$wk_title}</td>\n";
            $data .= "<td nowrap align=\"center\">" . $sickbed_cnt . "</td>\n";
        } else {
            $data .= "<td align=\"right\">{$wk_title}</td>\n";
            $data .= "<td align=\"left\">" . $sickbed_cnt. "</td>\n";
        }

        $wk_title = "�Ｊ振兀澑‘�数 ※端数切上げ整数入力";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"{$this->colspan_fc["excel"][2]}\">{$wk_title}</td>\n";
            if ($hospitalization_cnt == "") {
                $data .= "<td nowrap align=\"center\" colspan=\"1\">0日</td>\n";
            } else {
                $data .= "<td nowrap align=\"center\" colspan=\"1\">{$hospitalization_cnt}日</td>\n";
            }
        } else {
            $data .= "<td align=\"right\" class=\"basis_shift_input\">{$wk_title}</td>\n";
            $data .= "<td align=\"left\" class=\"basis_shift_input\">";
            $data .= "<input type=\"text\" size=\"10\" maxlength=\"5\" name=\"inp_hospitalization_cnt\" value=\"{$hospitalization_cnt}\" style=\"text-align: right; ime-mode: disabled;\">日\n";
            $data .= "</td>\n";
        }
        $data .= "</tr>\n";

        //-------------------------------------------------------------------------------
        // 届出時入院患者数、計算期間
        //-------------------------------------------------------------------------------
        $data .= "<tr>\n";

        //届出時入院患者数
        $wk_title = "届出時入院患者数";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" class=\"basis_shift\" colspan=\"{$this->colspan_fc["excel"][0]}\">{$wk_title}</td>\n";
            $data .= "<td nowrap align=\"center\" class=\"basis_shift\">" . $report_patient_cnt . "</td>\n";
        } else {
            $data .= "<td align=\"right\" class=\"basis_shift\">{$wk_title}</td>\n";
            $data .= "<td align=\"left\" class=\"basis_shift\">" . $report_patient_cnt . "</td>\n";
        }

        //計算期間
        if ($excel_flg == "1") {
            $data .= "<td align=\"center\" colspan=\"{$this->colspan_fc["excel"][4]}\" class=\"basis_shift_input\">";
        } else {
            $data .= "<td align=\"right\" colspan=\"2\" class=\"basis_shift_input\">";
        }

        //１ヵ月計算／４週計算
        if ($excel_flg == "1") {
            $wk_recomputation = "";
            if ($recomputation_flg == "2") {
                $wk_recomputation = "４週計算　開始日" . $calendar_ymd["date_y1"] . "年" . $calendar_ymd["date_m1"] . "月" . $calendar_ymd["date_d1"] . "日";
            } else {
                $wk_recomputation = "1ヶ月計算";
            }
            $data .= $wk_recomputation;
        } else {
            $wk1 = "";
            $wk2 = "";
            if (($recomputation_flg == "1") || ($recomputation_flg == "")){ $wk1 = "checked"; }
            if ($recomputation_flg == "2") { $wk2 = "checked"; }
            $data .= "<label><input type=\"radio\" name=\"recomputation_flg\" value=\"1\" $wk1 >１ヵ月計算</label>\n";
            $data .= "<label><input type=\"radio\" name=\"recomputation_flg\" value=\"2\" $wk2 >４週計算&nbsp;&nbsp;開始日</label>\n";

            // 年（カレンダー）
            $data .= "<input type=\"hidden\" name=\"date_y1\" id=\"date_y1\" value=\"" . $calendar_ymd["date_y1"] . "\">";

            // 月（カレンダー）
            $data .= "<input type=\"hidden\" name=\"date_m1\" id=\"date_m1\" value=\"" . $calendar_ymd["date_m1"] . "\">";

            // 日（カレンダー）
            $wk_cnt = $this->obj->days_in_month($calendar_ymd["date_y1"], $calendar_ymd["date_m1"]);
            $data .= "<select id=\"date_d1\" name=\"date_d1\">\n";
            $data .= "<option value=\"\">\n";
            for ($i = 1; $i <= $wk_cnt; $i++) {
                $val = sprintf("%02d", $i);
                $data .= "<option value=\"$val\"";
                if ($i ==  $calendar_ymd["date_d1"]) {
                    $data .= " selected";
                }
                $data .= ">$val</option>\n";
            }
            $data .= "</select>\n";
            
            $data .= "<div id=\"cal1Container\" style=\"position:absolute;display:none;z-index:10000;\"></div>\n";
            $data .= "<img src=\"img/calendar_link.gif\" style=\"cursor:pointer;\" onclick=\"show_cal1()\"/>\n";
            $data .= "&nbsp;&nbsp;<input type=\"button\" value=\"再計算\" onclick=\"dataRecomputation();\" style=\"width: 60px; height: 22px;\">";
        }

        $data .= "</td>\n";
        $data .= "</tr>\n";

        //-------------------------------------------------------------------------------
        // 届出区分
        //-------------------------------------------------------------------------------
        $this->top_colspan = ($prt_flg == "2") ? 8 : 9;
        $data .= "<tr class=\"basis_shift\">\n";
        //届出区分
        $wk_title = "届出区分";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"{$this->colspan_fc["excel"][0]}\">{$wk_title}</td>\n";
            $data .= "<td nowrap nowrap align=\"left\" colspan=\"{$this->colspan_fc["excel"][5]}\">" . $report_kbn_name . "</td>\n";
        } else {
            $data .= "<td align=\"right\">{$wk_title}</td>\n";
            $data .= "<td nowrap align=\"left\" colspan=\"3\">" . $report_kbn_name. "</td>\n";
        }
        $data .= "</tr>\n";

        if (($report_kbn == REPORT_KBN_SPECIFIC) || ($report_kbn == REPORT_KBN_SPECIFIC_PLUS)) {
            $data .= "<tr class=\"basis_shift\">\n";
            if ($excel_flg == "1") {
                $data .= "<td align=\"right\" colspan=\"{$this->colspan_fc["excel"][0]}\">特定入院料種類</td>\n";
                $data .= "<td align=\"left\" colspan=\"{$this->colspan_fc["excel"][5]}\">{$specific_data["specific_type"]}";
            } else {
                $data .= "<td align=\"right\" colspan=\"{$this->colspan_fc["html"][0]}\">特定入院料種類</td>\n";
                $data .= "<td align=\"left\" colspan=\"{$this->colspan_fc["html"][5]}\">{$specific_data["specific_type"]}";
            }
            $data .= "</td>\n";
            $data .= "</tr>\n";

            //特定入院料看護配置
            $data .= $this->showDataStaffingKbnSpecific($report_kbn, $specific_data, $excel_flg);
        } else {
            //看護配置
            $data .= $this->showDataStaffing(
                    $excel_flg,             //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
                    $duty_yyyy,             //対象年
                    $duty_mm,               //対象月
                    $nurse_staffing_flg,    //看護配置加算の有無
                    $acute_nursing_flg, //急性期看護補助加算の有無
                    $acute_nursing_cnt, //急性期看護補助加算の配置比率
                    $nursing_assistance_flg,    //看護補助加算の有無
                    $nursing_assistance_cnt,    //看護補助加算の配置比率
                    $acute_assistance_flg,  //急性期看護補助体制加算の届出区分
                    $acute_assistance_cnt,  //急性期看護補助体制加算の配置比率
                    $night_acute_assistance_flg,    //夜間急性期看護補助体制加算の届出区分
                    $night_acute_assistance_cnt,    //夜間急性期看護補助体制加算の配置比率
                    $night_shift_add_flg,    //看護職員夜間配置加算の有無
                    $report_kbn               //届出区分 20140324
            );
        }
        $data .= "</tr>\n";

        //申し送り時間
        $data .= "<tr class=\"basis_shift\">\n";
        $wk_title = "申し送り時間";
        $wk_send_hm_str = "";

        for ($i = 1; $i <= 3; $i++) {
            $start = $send_hm["send_start_hhmm_{$i}"];
            $end = $send_hm["send_end_hhmm_{$i}"];
            if (!$start || !$end) {
                continue;
            }
            if ($wk_send_hm_str != "") {
                $wk_send_hm_str .= "　";
            }
            $wk_send_hm_str .= sprintf("%s:%s〜%s:%s", intval(substr($start, 0, 2), 10), substr($start, 2, 2), intval(substr($end, 0, 2), 10), substr($end, 2, 2));
        }

        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"{$this->colspan_fc["excel"][0]}\">{$wk_title}</td>\n";
            $data .= "<td nowrap align=\"center\" colspan=\"{$this->colspan_fc["excel"][5]}\">";
            $data .= $wk_send_hm_str;
            $data .= "</td>\n";
        } else {
            $data .= "<td align=\"right\">{$wk_title}</td>\n";
            $data .= "<td align=\"left\" colspan=\"3\">" . $wk_send_hm_str;
            $data .= "</td>\n";
        }
        $data .= "</tr>\n";

        //-------------------------------------------------------------------------------
        //看護配置20対1以上
        //-------------------------------------------------------------------------------
        //新様式対応 20120409
        //療養病棟の場合
        $this->long_term_care_flg = ($report_kbn == REPORT_KBN_CURE_BASIC1 || $report_kbn == REPORT_KBN_CURE_BASIC2 || $report_kbn == REPORT_KBN_SPECIFIC || $report_kbn == REPORT_KBN_SPECIFIC_PLUS);
        $this->wk_kijun_str = (($this->long_term_care_flg) && ($report_kbn != REPORT_KBN_SPECIFIC_PLUS)) ? "" : "(基準値)";
        if (($report_kbn == REPORT_KBN_CURE_BASIC1) || ($report_kbn == REPORT_KBN_CURE_BASIC2)) {
            $data .= "</table>";
            $data .= $this->showDataStaffingCaution($report_kbn, $excel_flg);
        } else {
            $data .= "</table>\n";
        }

        return $data;
    }

    /*************************************************************************/
    // 看護配置20対1以上
    /*************************************************************************/
    function showDataStaffingCaution($report_kbn, $excel_flg)
    {
        $wk1 = "　";
        $wk2 = "　";
        if ($report_kbn == REPORT_KBN_CURE_BASIC1) {      // 療養病棟入院基本料１
            $wk1 = "看護配置20対1以上・看護補助配置20対1以上　看護職員中看護師比率20％以上";
        }
        if ($report_kbn == REPORT_KBN_CURE_BASIC2) {        // 療養病棟入院基本料２
            $wk1 = "看護配置25対1以上・看護補助配置25対1以上　看護職員中看護師比率20％以上";
            $wk2 = "看護要員(看護職員・看護補助者)の月平均夜勤時間数72時間以内";
        }
        $data .= "<div style=\"font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px; margin-top: 4px;\">&nbsp;" . $wk1 . "　" . $wk2 . "</div>\n";
        return $data;
    }

    /*************************************************************************/
    // 看護配置
    /*************************************************************************/
    function showDataStaffing(
                    $excel_flg,             //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
                    $duty_yyyy,             //対象年
                    $duty_mm,               //対象月
                    $nurse_staffing_flg,    //看護配置加算の有無
                    $acute_nursing_flg, //急性期看護補助加算の有無
                    $acute_nursing_cnt, //急性期看護補助加算の配置比率
                    $nursing_assistance_flg,    //看護補助加算の有無
                    $nursing_assistance_cnt,    //看護補助加算の配置比率
                    $acute_assistance_flg,  //急性期看護補助体制加算の届出区分
                    $acute_assistance_cnt,  //急性期看護補助体制加算の配置比率
                    $night_acute_assistance_flg,    //夜間急性期看護補助体制加算の届出区分
                    $night_acute_assistance_cnt,    //夜間急性期看護補助体制加算の配置比率
                    $night_shift_add_flg,    //看護職員夜間配置加算の有無
                    $report_kbn               //届出区分（地域包括ケア病棟追加対応） 20140324
        ) {
        //看護配置加算の有無
        $data = "<tr class=\"basis_shift\">\n";
        //地域包括ケア病棟 20140324
        if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
            $wk_title = "看護職員配置加算の有無";
        }
        else {
            $wk_title = "看護配置加算の有無";
        }
        $wk_nurse_staffing_str = ($nurse_staffing_flg == "1") ? "有" : "無";
        //地域包括ケア病棟 20140324
        if (($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) && $nurse_staffing_flg == "1") {
            $wk_nurse_staffing_str .= " （50対1）";
        }
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"{$this->colspan_fc["excel"][0]}\">{$wk_title}</td>\n";
            $data .= "<td nowrap align=\"center\" colspan=\"6\">";
            $data .= $wk_nurse_staffing_str;
            $data .= "</td>\n";
        } else {
            $data .= "<td align=\"right\">{$wk_title}</td>\n";
            $data .= "<td align=\"left\" colspan=\"3\">" . $wk_nurse_staffing_str;
            $data .= "</td>\n";
        }
        $data .= "</tr>\n";
        //平成２４年度改訂対応 20120406
        $this->wk_duty_yyyymm = $duty_yyyy.sprintf("%02d", $duty_mm);


        //地域包括ケア病棟以外 20140324
        if ($report_kbn != REPORT_KBN_COMMUNITY_CARE && $report_kbn != REPORT_KBN_COMMUNITY_CARE2) {
            $wk_title = "急性期看護補助加算の届出区分";
            //旧様式
            if ($this->wk_duty_yyyymm < "201204") {
                //急性期看護補助加算の届出区分
                $data .= "<tr class=\"basis_shift\">\n";
                $wk_acute_nursing_str = ($acute_nursing_flg == "1") ? "有" : "無";
                if ($acute_nursing_flg == "1" && $acute_nursing_cnt != "") {
                    $wk_acute_nursing_str .= "（".$acute_nursing_cnt."対1）";
                }
                if ($excel_flg == "1") {
                    $data .= "<td align=\"right\" colspan=\"{$this->colspan_fc["excel"][0]}\">{$wk_title}</td>\n";
                    $data .= "<td nowrap align=\"center\" colspan=\"6\">";
                    $data .= $wk_acute_nursing_str;
                    $data .= "</td>\n";
                } else {
                    $data .= "<td align=\"right\">{$wk_title}</td>\n";
                    $data .= "<td align=\"left\" colspan=\"3\">" . $wk_acute_nursing_str;
                    $data .= "</td>\n";
                }
                $data .= "</tr>\n";
            }
            //24年度から新様式
            else {
                $data .= "<tr class=\"basis_shift\">\n";
                $wk_acute_assistance_str = ($acute_assistance_flg == "1") ? "有" : "無";
                if ($acute_assistance_flg == "1" && $acute_assistance_cnt != "") {
                    $wk_acute_assistance_str .= "（".$acute_assistance_cnt."対1）";
                }
                if ($excel_flg == "1") {
                    $data .= "<td align=\"right\" colspan=\"{$this->colspan_fc["excel"][0]}\">{$wk_title}</td>\n";
                    $data .= "<td nowrap align=\"center\" colspan=\"6\">";
                    $data .= $wk_acute_assistance_str;
                    $data .= "</td>\n";
                } else {
                    $data .= "<td align=\"right\">{$wk_title}</td>\n";
                    $data .= "<td align=\"left\" colspan=\"3\">". $wk_acute_assistance_str;
                    $data .= "</td>\n";
                }
                $data .= "</tr>\n";
                $data .= "<tr class=\"basis_shift\">\n";
                $wk_title = "夜間急性期看護補助体制加算の届出区分";
                $wk_night_acute_assistance_str = ($night_acute_assistance_flg == "1") ? "有" : "無";
                if ($night_acute_assistance_flg == "1" && $night_acute_assistance_cnt != "") {
                    $wk_night_acute_assistance_str .= "（".$night_acute_assistance_cnt."対1）";
                }
                if ($excel_flg == "1") {
                    $data .= "<td align=\"right\" colspan=\"{$this->colspan_fc["excel"][0]}\">{$wk_title}</td>\n";
                    $data .= "<td nowrap align=\"center\" colspan=\"6\">";
                    $data .= $wk_night_acute_assistance_str;
                    $data .= "</td>\n";
                } else {
                    $data .= "<td align=\"right\">{$wk_title}</td>\n";
                    $data .= "<td align=\"left\" colspan=\"3\">" . $wk_night_acute_assistance_str;
                    $data .= "</td>\n";
                }
                $data .= "</tr>\n";
                $data .= "<tr class=\"basis_shift\">\n";
                $wk_title = "看護職員夜間配置加算の有無";
                $wk_night_shift_add_str = ($night_shift_add_flg == "1") ? "有" : "無";
                if ($excel_flg == "1") {
                    $data .= "<td align=\"right\" colspan=\"{$this->colspan_fc["excel"][0]}\">{$wk_title}</td>\n";
                    $data .= "<td nowrap align=\"center\" colspan=\"{$this->colspan_fc["excel"][5]}\">";
                    $data .= $wk_night_shift_add_str;
                    $data .= "</td>\n";
                } else {
                    $data .= "<td align=\"right\">{$wk_title}</td>\n";
                    $data .= "<td align=\"left\" colspan=\"3\">" . $wk_night_shift_add_str;
                    $data .= "</td>\n";
                }
                $data .= "</tr>\n";
            }
        } //end of if 地域包括ケア病棟以外 20140324
        //看護補助加算の届出区分
        $data .= "<tr class=\"basis_shift\">\n";
         //地域包括ケア病棟 20140324
        if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
            $wk_title = "看護補助者配置加算の届出区分";
        }
        else {
            $wk_title = "看護補助加算の届出区分";
        }
        $wk_nursing_assistance_str = ($nursing_assistance_flg == "1") ? "有" : "無";
        //地域包括ケア病棟 20140324
        if (($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) && $nursing_assistance_flg == "1") {
            $wk_nursing_assistance_str .= " （25対1）";
        }
        if ($nursing_assistance_flg == "1" && $nursing_assistance_cnt != "") {
            $wk_nursing_assistance_str .= "（".$nursing_assistance_cnt."対1）";
        }
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"{$this->colspan_fc["excel"][0]}\">{$wk_title}</td>\n";
            $data .= "<td nowrap align=\"center\" colspan=\"{$this->colspan_fc["excel"][5]}\">";
            $data .= $wk_nursing_assistance_str;
            $data .= "</td>\n";
        } else {
            $data .= "<td align=\"right\">{$wk_title}</td>\n";
            $data .= "<td align=\"left\" colspan=\"3\">" . $wk_nursing_assistance_str;
            $data .= "</td>\n";
        }

        return $data;
    }

    /*************************************************************************/
    // 特定入院料看護配置
    /*************************************************************************/
    function showDataStaffingKbnSpecific(
                    $report_kbn,    //届出区分
                    $specific_data, //特定入院料関連データ
                    $excel_flg      //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
    ) {
        if ($report_kbn == REPORT_KBN_SPECIFIC) {
            $data  = "<tr class=\"basis_shift\">\n";
            if ($excel_flg == "1") {
                $data .= "<td align=\"right\" colspan=\"{$this->colspan_fc["excel"][0]}\">看護配置</td>";
                $data .= "<td>{$specific_data["nurse_staffing_cnt"]}対1</td>";
                $data .= "<td align=\"right\" colspan=\"{$this->colspan_fc["excel"][2]}\">看護補助配置</td>";
                $data .= "<td>{$specific_data["assistance_staffing_cnt"]}対1</td>";
            } else {
                $data .= "<td align=\"right\">看護配置</td>";
                $data .= "<td>{$specific_data["nurse_staffing_cnt"]}対1</td>";
                $data .= "<td align=\"right\">看護補助配置</td>";
                $data .= "<td>{$specific_data["assistance_staffing_cnt"]}対1</td>";
            }
            $data .= "</tr>";
        } elseif ($report_kbn == REPORT_KBN_SPECIFIC_PLUS) {
            $data  = "<tr class=\"basis_shift\">\n";
            if ($excel_flg == "1") {
                $data .= "<td align=\"right\" colspan=\"{$this->colspan_fc["excel"][0]}\">看護配置・看護補助配置</td>";
                $data .= "<td colspan=\"{$this->colspan_fc["excel"][5]}\">{$specific_data["nurse_staffing_cnt"]}対1</td>";
            } else {
                $data .= "<td align=\"right\">看護配置・看護補助配置</td>";
                $data .= "<td colspan=\"3\">{$specific_data["nurse_staffing_cnt"]}対1</td>";
            }
            $data .= "</tr>";
        }

        return $data;
    }

    /*************************************************************************/
    //届出書添付書類(様式９集計表)
    /*************************************************************************/
    function showDataSummary(
                    $excel_flg,             //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
                    $recomputation_flg,     //１ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
                    $data_sub_array,        //看護補助者情報
                    $report_kbn,            //届出区分
                    $report_kbn2,           //届出区分（調整）
                    $hosp_patient_cnt,      //１日平均入院患者数
                    $compute_start_yyyy,    //１日平均入院患者数 算出期間（開始年）
                    $compute_start_mm,      //１日平均入院患者数 算出期間（開始月）
                    $compute_end_yyyy,      //１日平均入院患者数 算出期間（終了年）
                    $compute_end_mm,        //１日平均入院患者数 算出期間（終了月）
                    $nurse_sumtime,             //勤務時間数（看護師）
                    $sub_nurse_sumtime,         //勤務時間数（准看護師）
                    $assistance_nurse_sumtime,  //勤務時間数（看護補助者）
                    $nurse_cnt,             //看護師数
                    $sub_nurse_cnt,         //准看護師数
                    $assistance_nurse_cnt,  //看護補助者数
                    $hospitalization_cnt,   //平均在院日数
                    $ave_start_yyyy,        //平均在院日数 算出期間（開始年）
                    $ave_start_mm,          //平均在院日数 算出期間（開始月）
                    $ave_end_yyyy,          //平均在院日数 算出期間（終了年）
                    $ave_end_mm,            //平均在院日数 算出期間（終了月）
                    $prov_start_hh,         //夜勤時間帯 （開始時）
                    $prov_start_mm,         //夜勤時間帯 （開始分）
                    $prov_end_hh,           //夜勤時間帯 （終了時）
                    $prov_end_mm,           //夜勤時間帯 （終了分）
                    $duty_yyyy,             //対象年
                    $day_cnt,               //日数
                    $date_y1,               //カレンダー用年
                    $date_m1,               //カレンダー用月
                    $date_d1,               //カレンダー用日
                    $labor_cnt,             //常勤職員の週所定労働時間
                    $nurse_staffing_flg,    //看護配置加算の有無
                    $acute_nursing_flg,     //急性期看護補助加算の有無
                    $acute_nursing_cnt,     //急性期看護補助加算の配置比率
                    $nursing_assistance_cnt,        //看護補助加算の配置比率
                    $acute_assistance_cnt,          //急性期看護補助体制加算の配置比率
                    $night_acute_assistance_cnt,    //夜間急性期看護補助体制加算の配置比率
                    $specific_data,                 //特定入院料関連データ
                    //$sum_sub_night_gまで、excelの場合に指定
                    &$sum_night_b,  //夜勤従事者数(夜勤ありの職員数)〔Ｂ〕
                    &$sum_night_c,  //月延べ勤務時間数の計〔Ｃ〕
                    &$sum_night_d,  //月延べ夜勤時間数の計〔Ｄ〕
                    &$sum_night_e,  //夜勤専従者及び月16時間以下の者の夜勤時間数〔Ｅ〕
                    &$sum_night_f,  //1日看護配置数〔(Ａ ／届出区分の数)×３〕
                    &$sum_night_g,  //月平均１日あたり看護配置数〔Ｃ／(日数×８）〕
                    &$sum_night_h,  //月平均１日当たり夜間看護配置数〔D／(日数×１６）〕
                    &$sum_night_i,  //１日夜間看護配置数[A/12]※端数切上げ
                    &$sum_sub_night_b,  //夜勤従事者数(夜勤ありの職員数)〔Ｂ〕
                    &$sum_sub_night_c,  //月延べ勤務時間数の計〔Ｃ〕
                    &$sum_sub_night_d,  //月延べ夜勤時間数の計〔Ｄ〕
                    &$sum_sub_night_e,  //夜勤専従者及び月16時間以下の者の夜勤時間数〔Ｅ〕
                    &$sum_sub_night_f,  //1日看護配置数〔(A ／届出区分の数)×３〕
                    &$sum_sub_night_g   //月平均１日あたり看護配置数〔Ｃ／(日数×８）〕
    ) {
        //キャプション
        $data  = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"non_list\">\n";
        $data .= "<tr style=\"font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;\" height=\"30\">\n";
        if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
            $wk_title_9_str = "様式９の３";
        }
        else {
            $wk_title_9_str = "様式９";
        }
            
        $wk_title = "届出書添付書類({$wk_title_9_str}集計表)";
        if ($excel_flg == "1") {
            $data .= "<td nowrap align=\"left\" colspan=\"11\" valign=\"bottom\"><b>" . $wk_title . "</b></td>\n";
        } else {
            $data .= "<td nowrap align=\"left\" valign=\"bottom\"><b>" . $wk_title . "</b></td>\n";
        }
        $data .= "</tr>\n";
        $data .= "</table>";

        if ($excel_flg == "1") {
            $data .= "<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;\">\n";
        } else {
            $data .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list outer\" style=\"font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;\">\n";
        }

        //-------------------------------------------------------------------------------
        //１日平均入院患者数、算出期間
        //-------------------------------------------------------------------------------
        //１日平均入院患者数〔A〕
        $data .= "<tr class=\"summary\">\n";
        $data .= "<td align=\"right\">１日平均入院患者数〔A〕 ※端数切上げ整数入力</td>\n";
        if ($excel_flg == "1") {
            $data .= "<td nowrap align=\"center\">{$hosp_patient_cnt}人</td>\n";
        } else {
            $data .= "<td align=\"left\" style=\"width: 150px;\"><span id=\"hosp_patient_cnt\">" . $hosp_patient_cnt . "</span>人</td>\n";
        }

        //算出期間
        $wk_title = "算出期間";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"{$this->colspan_summary["excel"][2]}\">" . $wk_title . "</td>\n";
            $data .= "<td align=\"center\" colspan=\"{$this->colspan_summary["excel"][3]}\">";
            $data .= $compute_start_yyyy . "年";
            $data .= $compute_start_mm . "月〜";
            $data .= $compute_end_yyyy . "年";
            $data .= $compute_end_mm . "月";
            $data .= "</td>\n";
        } else {
            $data .= "<td align=\"right\" colspan=\"3\">" . $wk_title . "</td>\n";
            $data .= "<td align=\"left\" colspan=\"1\" nowrap>";
            $data .= $compute_start_yyyy . "年";
            $data .= $compute_start_mm . "月〜";
            $data .= $compute_end_yyyy . "年";
            $data .= $compute_end_mm . "月";
            $data .= "</td>\n";
        }

        $data .= "</tr>\n";

        //区分追加対応 20111206
        $this->wk_report_kbn = ($report_kbn > 900) ? $report_kbn - 900 : $report_kbn;

        $this->wk_staffing_rate = "";
        if (($report_kbn != REPORT_KBN_SPECIFIC) && ($report_kbn != REPORT_KBN_SPECIFIC_PLUS)) {
            $this->wk_staffing_rate = $this->wk_report_kbn;
        }

        //-------------------------------------------------------------------------------
        // ��-1　月平均１日当たり看護配置数（看護職員＋看護補助者）
        //-------------------------------------------------------------------------------
        if ($report_kbn == REPORT_KBN_SPECIFIC_PLUS) {
            $data .= $this->showDataSummaryNurseAssistanceAssign(
                        $excel_flg,             //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
                        $hosp_patient_cnt,      //１日平均入院患者数
                        $nurse_sumtime,             //勤務時間数（看護師）
                        $sub_nurse_sumtime,         //勤務時間数（准看護師）
                        $assistance_nurse_sumtime,  //勤務時間数（看護補助者）
                        $day_cnt,               //日数
                        $specific_data,  //特定入院料関連データ
                        $sum_night_c,  //月延べ勤務時間数の計〔Ｃ〕
                        $sum_night_f,  //1日看護配置数〔(Ａ ／届出区分の数)×３〕
                        $sum_night_g   //月平均１日あたり看護配置数〔Ｃ／(日数×８）〕
            );

            $wk_colspan = ($excel_flg == "1") ? 4 : 6;
            $data .= "<tr class=\"summary\">\n";
            $data .= "<td align=\"left\" colspan=\"" . $wk_colspan . "\">&nbsp;</td>\n";
            $data .= "</tr>\n";
        }

        //-------------------------------------------------------------------------------
        // �〃酳振傳影�あたり看護配置数
        //-------------------------------------------------------------------------------
        //月延べ勤務時間数の計〔Ｃ〕
        //月平均１日あたり看護配置数〔C／(日数×８）〕
        //小数点１桁まで表示
        $data .= $this->showDataSummaryNurseAssign(
                    $excel_flg,             //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
                    $report_kbn,            //届出区分
                    $hosp_patient_cnt,      //１日平均入院患者数
                    $nurse_sumtime,         //勤務時間数（看護師）
                    $sub_nurse_sumtime,     //勤務時間数（准看護師）
                    $day_cnt,               //日数
                    $specific_data,         //特定入院料関連データ
                    $sum_night_c,           //月延べ勤務時間数の計〔Ｃ〕
                    $sum_night_f,           //1日看護配置数〔(Ａ ／届出区分の数)×３〕
                    $sum_night_g            //月平均１日あたり看護配置数〔Ｃ／(日数×８）〕
        );

        //-------------------------------------------------------------------------------
        // 月平均１日あたり看護補助配置数(実績値)を算出
        //-------------------------------------------------------------------------------
        $sum_sub_night_f = $assistance_nurse_sumtime;
        $this->calcAssistanceActual(
                    $report_kbn,                //届出区分
                    $this->ave_duty_times,      //「1日平均看護配置数」を満たす「月延べ勤務時間数」
                    $day_cnt,                   //日数
                    $assistance_nurse_sumtime,  //看護補助者総勤務時間;〔F〕
                    $sum_night_c,               //月延べ勤務時間数の計
                    $sum_sub_night_g,           //みなし看護補助者の月延べ勤務時間数の計〔G〕　〔C - 1日看護配置数×８×日数〕
                    $assistance_actual_assign   //月平均１日あたり看護補助配置数(実績値)
        );

        //-------------------------------------------------------------------------------
        // 1日看護補助配置数(基準値)を算出
        //-------------------------------------------------------------------------------
        $this->calcAssistanceStandard(
                    $report_kbn,                   //届出区分
                    $hosp_patient_cnt,             //１日平均入院患者数
                    $nursing_assistance_cnt,       //看護補助加算の配置比率
                    $acute_assistance_cnt,         //急性期看護補助体制加算の配置比率
                    $specific_data["assistance_staffing_cnt"],  //看護補助配置
                    $this->long_term_care_flg,     //長期療養フラグ
                    $this->wk_report_kbn,          //クラス外からもこのメソッドが呼ばれる為、ここではメンバ変数を引数として渡す
                    $this->wk_duty_yyyymm,         //      〃
                    $report_kbn2,                  //届出区分（調整）
                    $assistance_standard_assign    //1日看護補助配置数(基準値)
        );

        //-------------------------------------------------------------------------------
        // ��-2　月平均１日当たり看護補助配置数
        //-------------------------------------------------------------------------------
        //新様式 20120409
        $sum_sub_night_f = $assistance_nurse_sumtime;
        if ($report_kbn != REPORT_KBN_SPECIFIC_PLUS) {
            if ($this->wk_duty_yyyymm >= "201204" || $this->long_term_care_flg) {
                $data .= $this->showDataSummaryAssistanceAssignLong(
                        $excel_flg,             //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
                        $report_kbn,            //届出区分
                        $hosp_patient_cnt,      //１日平均入院患者数
                        $day_cnt,               //日数
                        $specific_data,  //特定入院料関連データ
                        $assistance_standard_assign,    //1日看護補助配置数(基準値)
                        $assistance_actual_assign,  //月平均１日あたり看護補助配置数(実績値)
                        $sum_night_d,  //月延べ夜勤時間数の計〔Ｄ〕
                        $sum_night_h,  //月平均１日当たり夜間看護配置数〔D／(日数×１６）〕
                        $sum_night_i,  //１日夜間看護配置数[A/12]※端数切上げ
                        $sum_sub_night_f,  //勤務時間数（看護補助者）
                        $sum_sub_night_g   //月平均１日あたり看護配置数〔Ｃ／(日数×８）〕
                );
            }
        }

        //-------------------------------------------------------------------------------
        // �� 看護職員中の看護師の比率
        //-------------------------------------------------------------------------------
        $data .= $this->showDataSummaryNurseRate(
                    $excel_flg,             //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
                    $report_kbn,            //届出区分
                    $nurse_sumtime,             //勤務時間数（看護師）
                    $sub_nurse_sumtime,         //勤務時間数（准看護師）
                    $assistance_nurse_sumtime,  //勤務時間数（看護補助者）
                    $nurse_cnt,             //看護師数
                    $sub_nurse_cnt,         //准看護師数
                    $assistance_nurse_cnt,  //看護補助者数
                    $day_cnt,               //日数
                    $sum_sub_night_c        //月延べ勤務時間数の計〔Ｃ〕
        );

        //-------------------------------------------------------------------------------
        // �Ｊ振兀澑‘�数
        //-------------------------------------------------------------------------------
        $data .= "<tr class=\"summary\">\n";
        $data .= "<td align=\"right\">�Ｊ振兀澑‘�数 ※端数切上げ整数入力</td>\n";
        if ($excel_flg == "1") {
            if ($hospitalization_cnt == "") {
                $data .= "<td nowrap align=\"center\">0日</td>\n";
            } else {
                $data .= "<td nowrap align=\"center\">" . $hospitalization_cnt . "日</td>\n";
            }
        } else {
            $data .= "<td align=\"left\">";
            $data .= $hospitalization_cnt . "日</td>\n";
        }
        $wk_title = "算出期間";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"1\">" . $wk_title . "</td>\n";
            $data .= "<td align=\"center\" colspan=\"1\">";
            $data .= $ave_start_yyyy . "年";
            $data .= $ave_start_mm . "月〜";
            $data .= $ave_end_yyyy . "年";
            $data .= $ave_end_mm . "月";
            $data .= "</td>\n";
        } else {
            $data .= "<td align=\"right\" colspan=\"3\">" . $wk_title . "</td>\n";
            $data .= "<td align=\"left\" colspan=\"1\">";
            $data .= $ave_start_yyyy . "年";
            $data .= $ave_start_mm. "月〜";
            $data .= $ave_end_yyyy. "年";
            $data .= $ave_end_mm. "月";
            $data .= "</td>\n";
        }
        $data .= "</tr>\n";

        //-------------------------------------------------------------------------------
        // �ぬ覿仍�間帯(16時間)
        //-------------------------------------------------------------------------------
        $data .= "<tr class=\"summary\">\n";
        $data .= "<td align=\"right\">�ぬ覿仍�間帯(16時間)</td>\n";
        if ($excel_flg == "1") {
            $data .= "<td nowrap align=\"center\" colspan=\"3\">";
            $data .= $prov_start_hh . "時";
            $data .= $prov_start_mm . "分〜";
            $data .= intval($prov_end_hh, 10) . "時";
            $data .= $prov_end_mm . "分";
            $data .= "</td>\n";
        } else {
            $data .= "<td align=\"left\" colspan=\"5\">";
            $data .= $prov_start_hh . "時";
            $data .= $prov_start_mm . "分〜";
            $data .= intval($prov_end_hh, 10) . "時";
            $data .= $prov_end_mm . "分";
            $data .= "</td>\n";
        }
        $data .= "</tr>\n";

        //-------------------------------------------------------------------------------
        // �ゴ埜醉廾�の月平均夜勤時間数
        //-------------------------------------------------------------------------------
        $wk = $this->calcNightSumTimeAverage(
                    $excel_flg,             //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
                    $data_sub_array,        //看護補助者情報
                    $report_kbn,            //届出区分
                    $day_cnt,               //日数
                    $labor_cnt,             //常勤職員の週所定労働時間
                    $sum_night_b,           //夜勤従事者数(夜勤ありの職員数)〔Ｂ〕
                    $sum_night_d,           //月延べ夜勤時間数の計〔Ｄ〕
                    $sum_night_e,           //夜勤専従者及び月16時間以下の者の夜勤時間数〔Ｅ〕
                    $sum_sub_night_b,       //夜勤従事者数(夜勤ありの職員数)〔Ｂ〕
                    $sum_sub_night_d,       //月延べ夜勤時間数の計〔Ｄ〕
                    $sum_sub_night_e        //夜勤専従者及び月16時間以下の者の夜勤時間数〔Ｅ〕
        );

        ///------------------------------------------------------------------------------
        //月平均夜勤時間数の計算期間
        ///------------------------------------------------------------------------------
        //療養病棟の場合、補助者を含む 20120123
        if ($report_kbn == REPORT_KBN_CURE_BASIC1 || $report_kbn == REPORT_KBN_CURE_BASIC2) {
            $wk_name = "※看護職員・看護補助者の月平均夜勤時間数";
            $wk_title = "�ゴ埜醉廾�の月平均夜勤時間数〔(D-E)／B〕";
        }
        //一般の場合、補助者を含まない
        else {
            $wk_name = "";
            $wk_title = "�シ酳振冖覿仍�間数〔(D-E)／B〕";
            $wk_title .= ($report_kbn == REPORT_KBN_SPECIFIC_PLUS) ? "（看護職員）" : "";
        }


        $data .= "<tr class=\"summary\">\n";
        $data .= "<td align=\"right\">{$wk_title}</td>\n";
        //
        if ($excel_flg == "1") {
            $data .= "<td nowrap align=\"center\">" . $wk . "時間</td>\n";
        } else {
            //新様式対応
            $wk_colspan = ($this->wk_duty_yyyymm >= "201204" && !$this->long_term_care_flg) ? 1 : 5;
            $data .= "<td align=\"left\" colspan=\"{$wk_colspan}\">\n";
            $data .= $wk . "時間&nbsp;" . $wk_name . "&nbsp;";
        }

        //地域包括ケア病棟 20150323
        if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
            $wk_title = "&nbsp;";
            $wk_str = "&nbsp;";
            if ($excel_flg == "1") {
                $data .= "<td align=\"right\" colspan=\"1\">". $wk_title . "</td>\n";
                $data .= "<td nowrap align=\"center\" colspan=\"1\">" . $wk_str . "</td>\n";
            } else {
                $data .= "<td align=\"right\" colspan=\"3\">" . $wk_title . "</td>\n";
                $data .= "<td align=\"left\" colspan=\"1\">";
                $data .= $wk_str . "</td>\n";
            }
        }
        elseif ($this->wk_duty_yyyymm >= "201204" && !$this->long_term_care_flg) {
            $wk_title = "�ァ併温諭亡埜酳篏�者（みなしは除く）の月平均夜勤時間数";
            //（看護補助者のみの夜勤時間数（Ｈ）−夜勤専従者及び月16時間以下の者の夜勤時間数）/看護補助者の夜勤従事者数
            $wk = ($sum_sub_night_d - $sum_sub_night_e) / $sum_sub_night_b;
            $wk = floor($wk * 10) / 10;
            if ($excel_flg == "1") {
                $data .= "<td align=\"right\" colspan=\"1\">".$wk_name . " " . $wk_recomputation." " . $wk_title . "</td>\n";
                $data .= "<td nowrap align=\"center\" colspan=\"1\">" . $wk . "時間(実績値)</td>\n";
            } else {
                $data .= "<td align=\"right\" colspan=\"3\">" . $wk_title . "</td>\n";
                $data .= "<td align=\"left\" colspan=\"1\">";
                $data .= $wk . "時間</td>\n";
            }
        }
        else {
            if ($excel_flg == "1") {
                $data .= "<td nowrap align=\"left\" colspan=\"2\">" . $wk_name . "&nbsp;&nbsp;&nbsp;" . $wk_recomputation;
                $data .= "</td>\n";
            }
        }
        $data .= "</tr>\n";
        //地域包括ケア病棟 20140324
        if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
            $data .= "<tr class=\"summary\">\n";
            $wk_title = "＜看護職員配置加算を届け出る場合＞<br>月平均１日当たり当該入院料の施設基準の最小必要人数以上の看護職員配置数看護職員数";
            $wk_colspan = ($excel_flg == "") ? 6 : 4;
            $data .= "<td align=\"left\" colspan=\"{$wk_colspan}\">" . $wk_title . "</td>\n";

            $data .= "</tr>\n";

			//{〔看護職員のみのC〕 − （〔F〕×日数×８（時間））} /（日数×８（時間））
			$wk_assign = ($sum_night_c - ($sum_night_f * $day_cnt * 8)) / ($day_cnt * 8);
            $wk_assign = floor($wk_assign * 10) / 10;
			$wk = $sum_night_c - ($sum_night_f * $day_cnt * 8);
			$wk_title = "";
	        $data .= "<tr class=\"summary\">\n";
	        $data .= "<td align=\"right\">{$wk_title}</td>\n";
	        if ($excel_flg == "1") {
	            $data .= "<td nowrap align=\"center\">" . $wk_assign . "人(実績値)</td>\n";
	        } else {
	            $data .= "<td align=\"left\">";
	            $data .= $wk_assign . "人(実績値)";
	            $data .= "</td>\n";
	        }
	        $wk_title = "月延べ勤務時間数";
	        if ($excel_flg == "1") {
	            $data .= "<td align=\"right\" colspan=\"1\">" . $wk_title . "</td>\n";
	            $data .= "<td nowrap align=\"center\" colspan=\"1\">" . $wk . "時間(実績値)</td>\n";
	        } else {
	            $data .= "<td align=\"right\" colspan=\"3\">" . $wk_title . "</td>\n";
	            $data .= "<td align=\"left\" colspan=\"1\">";
	            $data .= $wk . "時間(実績値)";
	            $data .= "</td>\n";
	        }
	        $data .= "</tr>\n";
	        $data .= "<tr class=\"summary\">\n";
	        $data .= "<td align=\"right\"></td>\n";
	        $kango_standard_assign = ceil($hosp_patient_cnt / 50 * 3);
	        if ($excel_flg == "1") {
	            $data .= "<td nowrap align=\"center\">" . $kango_standard_assign . "人(基準値)</td>\n";
	        } else {
	            $data .= "<td align=\"left\">";
	            $data .= $kango_standard_assign . "人(基準値)";
	            $data .= "</td>\n";
	        }

	        //1日看護補助配置数〔(A ／配置比率)×３〕＊ 8 ＊ 今月の稼働日数
	        $wk = $kango_standard_assign * 8 * $day_cnt;
	        $wk_title = "※「1日平均看護補助者配置数」を満たす「月延べ勤務時間数」";
	        if ($excel_flg == "1") {
	            $data .= "<td align=\"right\" colspan=\"1\">" . $wk_title . "</td>\n";
	            $data .= "<td nowrap align=\"center\" colspan=\"1\">" . $wk . "時間(基準値)</td>\n";
	        } else {
	            $data .= "<td align=\"right\" colspan=\"3\" nowrap>" . $wk_title . "</td>\n";
	            $data .= "<td align=\"left\" colspan=\"1\">";
	            $data .= $wk . "時間(基準値)";
	            $data .= "</td>\n";
	        }
	        $data .= "</tr>\n";


            $data .= "<tr class=\"summary\">\n";
            $wk_title = "＜看護補助者配置加算を届け出る場合＞";
            $wk_colspan = ($excel_flg == "") ? 6 : 4;
            $data .= "<td align=\"left\" colspan=\"{$wk_colspan}\">" . $wk_title . "</td>\n";
            $data .= "</tr>\n";
        }
        elseif ($this->wk_duty_yyyymm >= "201204" && !$this->long_term_care_flg) {
            $wk_title = "[（看護補助者のみの夜勤時間数（Ｈ）−夜勤専従者及び月16時間以下の者の夜勤時間数）/看護補助者の夜勤従事者数]";
            $data .= "<tr class=\"summary\">\n";
            $data .= "<td align=\"right\"></td>\n";
            $wk_colspan = ($excel_flg == "") ? 5 : 3;
            $data .= "<td align=\"right\" colspan=\"{$wk_colspan}\" style=\"font-size: 12px;\">" . $wk_title . "</td>\n";
            $data .= "</tr>\n";
        }
        //一般の場合
        if (!$this->long_term_care_flg) {
            $data .= $this->showDataSummaryAssistanceAssign(
                            $excel_flg,                     //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
                            $hosp_patient_cnt,              //１日平均入院患者数
                            $assistance_nurse_sumtime,      //勤務時間数（看護補助者）
                            $day_cnt,                       //日数
                            $nurse_staffing_flg,            //看護配置加算の有無
                            $acute_nursing_flg,             //急性期看護補助加算の有無
                            $acute_nursing_cnt,             //急性期看護補助加算の配置比率
                            $nursing_assistance_cnt,        //看護補助加算の配置比率
                            $acute_assistance_cnt,          //急性期看護補助体制加算の配置比率
                            $assistance_standard_assign,    //1日看護補助配置数(基準値)
                            $assistance_actual_assign,      //月平均１日あたり看護補助配置数(実績値)
                            $report_kbn2,       //届出区分（調整）
                            $sum_night_c,       //月延べ勤務時間数の計〔Ｃ〕
                            $sum_night_f,       //1日看護配置数〔(A ／届出区分の数)×３〕
                            $sum_sub_night_g    //みなし看護補助者の月延べ勤務時間数の計〔Ｇ〕　〔Ｃ−1日看護配置数×８×日数〕
            );
        }
        //新様式 20120409
        if ($this->wk_duty_yyyymm >= "201204" && !$this->long_term_care_flg && 
        	$report_kbn != REPORT_KBN_COMMUNITY_CARE && $report_kbn != REPORT_KBN_COMMUNITY_CARE2
        	) {
            $data .= "<tr class=\"summary\">\n";
            $wk_colspan = ($excel_flg == "1") ? 4 : 6;
            //地域包括ケア病棟 20140324
            if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
                $wk_title = "&nbsp;";
            }
            else {
                $wk_title = "＜夜間急性期看護補助体制加算を届け出る場合＞";
            }

            $data .= "<td align=\"left\" colspan=\"{$wk_colspan}\">{$wk_title}</td>\n";
            $data .= "</tr>\n";

            $data .= "<tr class=\"summary\">\n";
            //H/(日数×１６）
            $wk = $sum_sub_night_d / ($day_cnt * 16);
            $wk = floor($wk * 10) / 10;
            //地域包括ケア病棟 20140324
            if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
                $wk_title = "&nbsp;";
                $wk_str = "&nbsp;";
                
            }
            else {
                $wk_title = "��-2　うち、月平均1日当たり夜間看護補助者配置数";
                $wk_str = $wk . "人(実績値)";
            }
            
            $data .= "<td align=\"right\">$wk_title</td>\n";
            if ($excel_flg == "1") {
                $data .= "<td nowrap align=\"center\">" . $wk_str . "</td>\n";
            } else {
                $data .= "<td align=\"left\">";
                $data .= $wk_str;
                $data .= "</td>\n";
            }

            $wk = $sum_sub_night_d;
            if ($wk == "") {
                $wk = 0;
            }
            //地域包括ケア病棟 20150323
            if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
                $wk_title = "&nbsp;";
                $wk_str = "&nbsp;";
            }
            else {
            	$wk_title = "月延べ夜勤時間数";
                $wk_str = $wk . "時間(実績値)";
            }
            if ($excel_flg == "1") {
                $data .= "<td align=\"right\" colspan=\"1\">" . $wk_title . "</td>\n";
                $data .= "<td align=\"center\" colspan=\"1\">" . $wk_str . "</td>\n";
            } else {
                $data .= "<td align=\"right\" colspan=\"3\">" . $wk_title . "</td>\n";
                $data .= "<td align=\"left\" colspan=\"1\">";
                $data .= $wk_str;
                $data .= "</td>\n";
            }
            $data .= "</tr>\n";
            $data .= "<tr class=\"summary\">\n";

            //配置比率:夜間急性期看護補助体制加算の配置比率
            $wk = 0;
            if ($night_acute_assistance_cnt > 0) {
                $wk = ceil($hosp_patient_cnt / $night_acute_assistance_cnt);
            }
            //地域包括ケア病棟 20140324
            if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
                $wk_title = "&nbsp;";
                $wk_str = "&nbsp;";
            }
            else {
                $wk_title = "1日夜間看護補助者配置数[A/配置比率{$night_acute_assistance_cnt}]";
                $wk_str = $wk . "人(基準値)";
            }

            $data .= "<td align=\"right\">$wk_title</td>\n";
            if ($excel_flg == "1") {
                $data .= "<td nowrap align=\"center\">" . $wk_str . "</td>\n";
            } else {
                $data .= "<td align=\"left\">";
                $data .= $wk_str;
                $data .= "</td>\n";
            }

            //「１日夜間看護補助者配置数」 * 16 * 日数
            if ($wk > 0) {
                $wk = $wk * 16 * $day_cnt;
            }
            else {
                $wk = 0;
            }
            //地域包括ケア病棟 20140324
            if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
                $wk_title = "&nbsp;";
                $wk_str = "&nbsp;";
            }
            else {
                $wk_title = "※「1日夜間看護補助者配置数」を満たす「月延べ夜間時間数」";
                $wk_str = $wk . "時間(基準値)";
            }
            if ($excel_flg == "1") {
                $data .= "<td align=\"right\" colspan=\"1\">" . $wk_title . "</td>\n";
                $data .= "<td nowrap align=\"center\" colspan=\"1\">" . $wk_str . "</td>\n";
            } else {
                $data .= "<td align=\"right\" colspan=\"3\" nowrap>" . $wk_title . "</td>\n";
                $data .= "<td align=\"left\" colspan=\"1\">";
                $data .= $wk_str;
                $data .= "</td>\n";
            }
            $data .= "</tr>\n";
        }
        $data .= "</table>\n";

        return $data;
    }

    /*************************************************************************/
    // 看護職員中の看護師の比率
    /*************************************************************************/
    function showDataSummaryNurseRate(
                    $excel_flg,             //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
                    $report_kbn,            //届出区分
                    $nurse_sumtime,             //勤務時間数（看護師）
                    $sub_nurse_sumtime,         //勤務時間数（准看護師）
                    $assistance_nurse_sumtime,  //勤務時間数（看護補助者）
                    $nurse_cnt,             //看護師数
                    $sub_nurse_cnt,         //准看護師数
                    $assistance_nurse_cnt,  //看護補助者数
                    $day_cnt,               //日数
                    &$sum_sub_night_c       //月延べ勤務時間数の計〔Ｃ〕
    )
    {
        $data = "<tr class=\"summary\">\n";
        if ($report_kbn == REPORT_KBN_SPECIFIC_PLUS) { // 届出区分IDが「特定入院料（看護職員＋看護補助者）」
            $wk = ($nurse_sumtime + $sub_nurse_sumtime) / $this->ave_duty_times_plus * 100;
            //100%以上は100%で表示
            if ($wk > 100) {
                $wk = 100;
            }
            //小数点１桁まで表示(切捨て)
            $wk = (int)(($wk * 10)); // + 0.5
            $wk = $wk / 10;
            $wk = sprintf("%01.1f",$wk);

            $data .= "<td colspan=\"2\" valign=\"top\">\n";
            if ($excel_flg == "1") {
                $data .= "<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" class=\"list\">\n";
            } else {
                $data .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"list\">\n";
            }
            $data .= "<tr height=\"20\" style=\"font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;\">";
            $data .= "<td align=\"right\" width=\"280\">��-1 看護要員中の看護職員の比率</td>\n";
            $data .= "<td align=\"left\" width=\"380\">";
            $data .= $wk . "％</td>\n";
            $data .= "</tr>\n";
            $data .= "<tr height=\"20\" style=\"font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;\">";
            $data .= "<td align=\"right\" width=\"280\">※看護要員中に必要な看護職員の比率</td>\n";
            $data .= "<td align=\"left\" width=\"380\">";
            $data .= "50％(基準値)</td>\n";
            $data .= "</tr>\n";

            $wk = $nurse_sumtime / $this->ave_duty_times * 100;
            //100%以上は100%で表示
            if ($wk > 100) {
                $wk = 100;
            }
            //小数点１桁まで表示(切捨て)
            $wk = (int)(($wk * 10)); // + 0.5
            $wk = $wk / 10;
            $wk = sprintf("%01.1f",$wk);

            $data .= "<tr height=\"20\" style=\"font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;\">";
            $data .= "<td align=\"right\" width=\"280\">��-2 看護職員中の看護師の比率</td>\n";
            $data .= "<td align=\"left\" width=\"380\">";
            $data .= $wk . "％</td>\n";
            $data .= "</tr>\n";
            $data .= "<tr height=\"20\" style=\"font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;\">";
            $data .= "<td align=\"right\" width=\"280\">※看護職員中に必要な看護師の比率</td>\n";
            $data .= "<td align=\"left\" width=\"380\">";
            $data .= "20％(基準値)</td>\n";
            $data .= "</tr>\n";
            $data .= "</table>";
            $data .= "</td>\n";
        } else {
            //W18(看護師の月間総勤務時間数)／T13(※「1日平均看護配置数」を満たす「月延べ勤務時間数」)
            $wk = $nurse_sumtime / $this->ave_duty_times * 100;
            //100%以上は100%で表示
            if ($wk > 100) {
                $wk = 100;
            }
            //小数点１桁まで表示 切捨てに変更 20100729
            $wk = (int)(($wk * 10)); // + 0.5
            $wk = $wk / 10;
            $wk = sprintf("%01.1f",$wk);

            //月平均１日あたり配置数　看護師 = 月間総勤務時間数　／　（今月の稼働日数　×　8　） 20110929
            $wk_ave_day_nurse_cnt = (int)($nurse_sumtime / ($day_cnt * 8) * 10);
            $wk_ave_day_nurse_cnt = $wk_ave_day_nurse_cnt / 10;
            $wk_ave_day_nurse_cnt = sprintf("%01.1f",$wk_ave_day_nurse_cnt);
            if ($excel_flg == "1") {
                $data .= "<td colspan=\"2\">\n";
                $data .= "<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">\n";
                $data .= "<tr style=\"font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;\">";
                $data .= "<td align=\"right\">�� 看護職員中の看護師の比率</td>\n";
                $data .= "<td nowrap align=\"center\">" . $wk . "(％)</td>\n";
                $data .= "</tr>\n";
                if (!$this->long_term_care_flg) {
                    $data .= "<tr style=\"font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;\">";
                    $data .= "<td align=\"right\">月平均1日あたり配置数　看護師</td>\n";
                    $data .= "<td nowrap align=\"center\">" . $wk_ave_day_nurse_cnt . "人</td>\n";
                    $data .= "</tr>\n";
                }
                $data .= "</table>";
                $data .= "</td>\n";
            } else {
                $data .= "<td colspan=\"2\" valign=\"top\">\n";
                $data .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"list\">\n";
                $data .= "<tr height=\"41\" style=\"font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;\">";
                $data .= "<td align=\"right\" width=\"280\">�� 看護職員中の看護師の比率</td>\n";
                $data .= "<td align=\"left\" width=\"380\">";
                $data .= $wk . "％</td>\n";
                $data .= "</tr>\n";
                if (!$this->long_term_care_flg) {
                    $data .= "<tr height=\"41\" style=\"font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;\">";
                    $data .= "<td align=\"right\" width=\"280\">月平均1日あたり配置数　看護師</td>\n";
                    $data .= "<td align=\"left\" width=\"380\">";
                    $data .= $wk_ave_day_nurse_cnt . "人</td>\n";
                    $data .= "</tr>\n";
                }
                $data .= "</table>\n";
                $data .= "</td>\n";
            }
        }

        //-------------------------------------------------------------------------------
        // 看護師／准看護師／看護補助者　月間総勤務時間数
        //-------------------------------------------------------------------------------
        if ($excel_flg == "1") {
            $data .= "<td colspan=\"1\">\n";
            $data .= "<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">\n";
        } else {
            $data .= "<td colspan=\"4\">\n";
            $data .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n";
        }
        //-------------------------------------------------------------------------------
        // 看護師　月間総勤務時間数
        //-------------------------------------------------------------------------------
        //小数点２桁まで表示 切捨てに変更 20100729
        $wk = (int)(($nurse_sumtime * 100));
        $wk /= 100;
        $wk = sprintf("%01.2f",$wk);
        $wk = $this->obj->rtrim_zero($wk);

        $data .= "<tr style=\"font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;\">\n";
        if ($excel_flg == "1") {
            $data .= "<td nowrap align=\"center\" rowspan=\"3\">看護要員<br>の内訳</td>\n";
        } else {
            $data .= "<td nowrap align=\"center\" rowspan=\"3\">看護要員の内訳</td>\n";
        }
        $wk_title = "看護師";
        if ($excel_flg == "1") {
            $data .= "<td nowrap align=\"right\">" . $wk_title . "</td>\n";
            $data .= "<td nowrap align=\"center\">" . $nurse_cnt . "人</td>\n";
        } else {
            $data .= "<td align=\"right\">" . $wk_title . "</td>\n";
            $data .= "<td align=\"left\">";
            $data .= $nurse_cnt . "人</td>\n";
        }
        $wk_title = "月間総勤務時間数";
        if ($excel_flg == "1") {
            $data .= "<td nowrap align=\"right\">" . $wk_title . "</td>\n";
        } else {
            $data .= "<td align=\"right\">" . $wk_title . "</td>\n";
            $data .= "<td align=\"left\">";
            $data .= $wk . "時間</td>\n";
        }
        $data .= "</tr>\n";
        //-------------------------------------------------------------------------------
        // 准看護師　月間総勤務時間数
        //-------------------------------------------------------------------------------
        //小数点２桁まで表示 切捨てに変更 20100729
        $wk = (int)(($sub_nurse_sumtime * 100));
        $wk /= 100;
        $wk = sprintf("%01.2f",$wk);
        $wk = $this->obj->rtrim_zero($wk);

        $data .= "<tr style=\"font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;\">\n";
        $wk_title = "准看護師";
        if ($excel_flg == "1") {
            $data .= "<td nowrap align=\"right\">" . $wk_title . "</td>\n";
            $data .= "<td nowrap align=\"center\">" . $sub_nurse_cnt . "人</td>\n";
        } else {
            $data .= "<td align=\"right\">" . $wk_title . "</td>\n";
            $data .= "<td align=\"left\">";
            $data .= $sub_nurse_cnt . "人</td>\n";
        }
        $wk_title = "月間総勤務時間数";
        if ($excel_flg == "1") {
            $data .= "<td nowrap align=\"right\">" . $wk_title . "</td>\n";
        } else {
            $data .= "<td align=\"right\">" . $wk_title . "</td>\n";
            $data .= "<td align=\"left\">";
            $data .= $wk . "時間</td>\n";
        }
        $data .= "</tr>\n";
        //-------------------------------------------------------------------------------
        // 看護補助者　月間総勤務時間数
        //-------------------------------------------------------------------------------
        //小数点２桁まで表示 切捨てに変更 20100729
        $sum_sub_night_c = $assistance_nurse_sumtime;
        $wk = (int)(($sum_sub_night_c * 100));
        $wk /= 100;
        $wk = sprintf("%01.2f",$wk);
        $wk = $this->obj->rtrim_zero($wk);

        $data .= "<tr style=\"font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;\">\n";
        $wk_title = "看護補助者";
        if ($excel_flg == "1") {
            $data .= "<td nowrap align=\"right\">" . $wk_title . "</td>\n";
            $data .= "<td nowrap align=\"center\">" . $assistance_nurse_cnt . "人</td>\n";
        } else {
            $data .= "<td align=\"right\">" . $wk_title . "</td>\n";
            $data .= "<td align=\"left\">";
            $data .= $assistance_nurse_cnt . "人</td>\n";
        }
        $wk_title = "月間総勤務時間数";
        if ($excel_flg == "1") {
            $data .= "<td nowrap align=\"right\">" . $wk_title . "</td>\n";
        } else {
            $data .= "<td align=\"right\">" . $wk_title . "</td>\n";
            $data .= "<td align=\"left\">";
            $data .= $wk . "時間</td>\n";
        }
        $data .= "</tr>\n";

        $data .= "</table>";
///////
        if ($excel_flg == "1") {
            $data .= "<td colspan=\"1\">\n";
            $data .= "<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">\n";
            //-------------------------------------------------------------------------------
            // 看護師　月間総勤務時間数
            //-------------------------------------------------------------------------------
            //小数点２桁まで表示 切捨てに変更 20100729
            $wk = (int)(($nurse_sumtime * 100));
            $wk /= 100;
            $wk = sprintf("%01.2f",$wk);
            $wk = $this->obj->rtrim_zero($wk);
            $data .= "<tr style=\"font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;\">\n";
            $data .= "<td nowrap align=\"center\" colspan=\"{$this->colspan_summary["excel"][5]}\">" . $wk . "時間</td>\n";
            $data .= "</tr>\n";
            //-------------------------------------------------------------------------------
            // 准看護師　月間総勤務時間数
            //-------------------------------------------------------------------------------
            //小数点１桁まで表示 切捨てに変更 20100729
            $wk = (int)(($sub_nurse_sumtime * 100));
            $wk /= 100;
            $wk = sprintf("%01.2f",$wk);
            $wk = $this->obj->rtrim_zero($wk);
            $data .= "<tr style=\"font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;\">\n";
            $data .= "<td nowrap align=\"center\" colspan=\"{$this->colspan_summary["excel"][5]}\">" . $wk . "時間</td>\n";
            $data .= "</tr>\n";
            //-------------------------------------------------------------------------------
            // 看護補助者　月間総勤務時間数
            //-------------------------------------------------------------------------------
            //小数点１桁まで表示 切捨てに変更 20100729
            $wk = (int)(($sum_sub_night_c * 100));
            $wk /= 100;
            $wk = sprintf("%01.2f",$wk);
            $wk = $this->obj->rtrim_zero($wk);
            $data .= "<tr style=\"font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;\">\n";
            $data .= "<td nowrap align=\"center\" colspan=\"{$this->colspan_summary["excel"][5]}\">" . $wk . "時間</td>\n";
            $data .= "</tr>\n";
            $data .= "</table>";
        }
        $data .= "</td>\n";
        $data .= "</tr>\n";

        return $data;
    }

    /*************************************************************************/
    // 月平均１日あたり看護配置数
    /*************************************************************************/
    function showDataSummaryNurseAssign(
                    $excel_flg,         //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
                    $report_kbn,        //届出区分
                    $hosp_patient_cnt,  //１日平均入院患者数
                    $nurse_sumtime,     //勤務時間数（看護師）
                    $sub_nurse_sumtime, //勤務時間数（准看護師）
                    $day_cnt,           //日数
                    $specific_data,     //特定入院料関連データ
                    &$sum_night_c,      //月延べ勤務時間数の計〔Ｃ〕
                    &$sum_night_f,      //1日看護配置数〔(Ａ ／届出区分の数)×３〕
                    &$sum_night_g       //月平均１日あたり看護配置数〔Ｃ／(日数×８）〕
    ) {
        //月延べ勤務時間数の計、月平均１日あたり看護配置数、1日看護配置数を算出
        $this->calcNurseAssign(
                $report_kbn,            //届出区分
                $this->wk_report_kbn,   //クラス外からもこのメソッドが呼ばれる為、ここではメンバ変数を引数として渡す
                $hosp_patient_cnt,      //１日平均入院患者数
                $nurse_sumtime,         //勤務時間数（看護師）
                $sub_nurse_sumtime,     //勤務時間数（准看護師）
                $day_cnt,               //日数
                $specific_data["nurse_staffing_cnt"],   //看護配置
                $sum_night_c,           //月延べ勤務時間数の計
                $sum_night_f,           //1日看護配置数(基準値)
                $sum_night_g            //月平均１日あたり看護配置数(実績値)
        );

        //月延べ勤務時間数の計〔Ｃ〕
        //月平均１日あたり看護配置数〔C／(日数×８）〕
        //小数点１桁まで表示
        $wk = sprintf("%01.1f",$sum_night_g);
        $wk = $this->obj->rtrim_zero($wk);
        $wk_title = ($report_kbn == REPORT_KBN_SPECIFIC_PLUS) ? "��-2　月平均１日当たり看護配置数（看護職員）" : "�〃酳振傳影�あたり看護配置数";

        $data = "<tr class=\"summary\">\n";
        $data .= "<td align=\"right\">" . $wk_title . "</td>\n";
        if ($excel_flg == "1") {
            $data .= "<td nowrap align=\"center\">" . $wk . "人(実績値)</td>\n";
        } else {
            $data .= "<td align=\"left\">";
            $data .= $wk . "人(実績値)";
            $data .= "</td>\n";
        }

        //小数点１桁まで表示
        $wk = sprintf("%01.2f",$sum_night_c);
        $wk = $this->obj->rtrim_zero($wk);
        $wk_title = ($report_kbn == REPORT_KBN_SPECIFIC_PLUS) ? "「月延べ勤務時間数」（看護職員）" : "月延べ勤務時間数";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"1\">" . $wk_title . "</td>\n";
            $data .= "<td nowrap align=\"center\" colspan=\"{$this->colspan_summary["excel"][3]}\">" . $wk . "時間(実績値)</td>\n";
        } else {
            $data .= "<td align=\"right\" colspan=\"3\">" . $wk_title . "</td>\n";
            $data .= "<td align=\"left\" colspan=\"1\">";
            $data .= $wk . "時間(実績値)";
            $data .= "</td>\n";
        }
        $data .= "</tr>\n";

        //整数で表示
        //1日看護配置数〔(A ／配置比率)×３〕＊ 8 ＊ 今月の稼働日数
        $this->ave_duty_times = $sum_night_f * 8 * $day_cnt;

        $data .= "<tr class=\"summary\">\n";

        if ($report_kbn == REPORT_KBN_SPECIFIC_PLUS) {
            $wk_title = "看護職員の必要最小な１日看護配置数";
        } else {
            $wk_title = "1日看護配置数〔(A ／配置比率{$this->wk_staffing_rate})×３〕";
        }

        if (!$this->long_term_care_flg || $report_kbn == REPORT_KBN_SPECIFIC_PLUS) {
            $wk_title .= "※端数切上げ";
        }

        $data .= "<td align=\"right\">{$wk_title}";
        $data .= "</td>\n";
        if ($excel_flg == "1") {
            $data .= "<td nowrap align=\"center\">" . $sum_night_f . "人{$this->wk_kijun_str}</td>\n";
        } else {
            $data .= "<td align=\"left\">";
            $data .= $sum_night_f . "人" . $this->wk_kijun_str;
            $data .= "</td>\n";
        }
        $wk_title = "※「1日平均看護配置数」を満たす「月延べ勤務時間数」";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"1\">" . $wk_title . "</td>\n";
            $data .= "<td nowrap align=\"center\" colspan=\"{$this->colspan_summary["excel"][3]}\">" . $this->ave_duty_times . "時間(基準値)</td>\n";
        } else {
            $data .= "<td align=\"right\" colspan=\"3\">" . $wk_title . "</td>\n";
            $data .= "<td align=\"left\" colspan=\"1\">";
            $data .= $this->ave_duty_times ."時間(基準値)";
            $data .= "</td>\n";
        }
        $data .= "</tr>\n";
        return $data;
    }

    /*************************************************************************/
    // 月平均１日当たり看護配置数（看護職員＋看護補助者）
    /*************************************************************************/
    function showDataSummaryNurseAssistanceAssign(
                    $excel_flg,             //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
                    $hosp_patient_cnt,      //１日平均入院患者数
                    $nurse_sumtime,             //勤務時間数（看護師）
                    $sub_nurse_sumtime,         //勤務時間数（准看護師）
                    $assistance_nurse_sumtime,  //勤務時間数（看護補助者）
                    $day_cnt,               //日数
                    $specific_data,  //特定入院料関連データ
                    &$sum_night_c,  //月延べ勤務時間数の計〔Ｃ〕
                    &$sum_night_f,  //1日看護配置数〔(Ａ ／届出区分の数)×３〕
                    &$sum_night_g   //月平均１日あたり看護配置数〔Ｃ／(日数×８）〕
    )
    {
        //月延べ勤務時間数の計〔Ｃ〕
        //月平均１日あたり看護配置数〔C／(日数×８）〕
        //小数点１桁まで表示
        $sum_night_c = $nurse_sumtime + $sub_nurse_sumtime + $assistance_nurse_sumtime;
        $wk = $sum_night_c / ($day_cnt * 8);
        $wk *= 10;
        $wk = (int)$wk; //切捨てに変更 20100729
        $sum_night_g = $wk / 10;
        $wk = sprintf("%01.1f",$sum_night_g);
        $wk = $this->obj->rtrim_zero($wk);
        $wk_title = "��-1　月平均１日当たり看護配置数（看護職員＋看護補助者）";

        $data = "<tr class=\"summary\">\n";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"{$this->colspan_summary[excel][0]}\">{$wk_title}</td>\n";
            $data .= "<td nowrap align=\"center\" colspan=\"1\">{$wk}人(実績値)</td>\n";
        } else {
            $data .= "<td align=\"right\">{$wk_title}</td>\n";
            $data .= "<td align=\"left\">{$wk}人(実績値)</td>\n";
        }

        //小数点１桁まで表示
        $wk = sprintf("%01.2f",$sum_night_c);
        $wk = $this->obj->rtrim_zero($wk);
        $wk_title = "「月延べ勤務時間数」（看護職員＋看護補助者）";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"1\">" . $wk_title . "</td>\n";
            $data .= "<td nowrap align=\"center\" colspan=\"1\">" . $wk . "時間(実績値)</td>\n";
        } else {
            $data .= "<td align=\"right\" colspan=\"3\">" . $wk_title . "</td>\n";
            $data .= "<td align=\"left\" colspan=\"1\">";
            $data .= $wk . "時間(実績値)";
            $data .= "</td>\n";
        }
        $data .= "</tr>\n";

        //整数で表示
        $staffing_cnt = $specific_data["nurse_staffing_cnt"];
        $wk1 = ($hosp_patient_cnt / $staffing_cnt) * 3;
        $sum_night_f = ceil($wk1); //切り上げに変更 20100729

        //1日看護配置数〔(A ／配置比率)×３〕＊ 8 ＊ 今月の稼働日数
        $this->ave_duty_times_plus = $sum_night_f * 8 * $day_cnt;

        if ($excel_flg == "1") {
            $wk_colspan = $this->colspan_summary["excel"][0];
            $wk_align = "center";
        } else {
            $wk_colspan = $this->colspan_summary["html"][0];
            $wk_align = "left";
        }
        $data .= "<tr class=\"summary\">\n";
        $wk_title = "1日看護配置数〔(A ／配置比率{$this->wk_staffing_rate})×３〕※端数切上げ";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"{$this->colspan_summary["excel"][0]}\">{$wk_title}</td>\n";
            $data .= "<td nowrap align=\"center\">{$sum_night_f}人{$this->wk_kijun_str}</td>\n";
        } else {
            $data .= "<td align=\"right\">{$wk_title}</td>\n";
            $data .= "<td align=\"left\">{$sum_night_f}人{$this->wk_kijun_str}</td>\n";
        }

        $wk_title = "※「1日平均看護配置数」を満たす「月延べ勤務時間数」";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"1\">" . $wk_title . "</td>\n";
            $data .= "<td nowrap align=\"center\" colspan=\"1\">" . $this->ave_duty_times_plus . "時間(基準値)</td>\n";
        } else {
            $data .= "<td align=\"right\" colspan=\"3\">" . $wk_title . "</td>\n";
            $data .= "<td align=\"left\" colspan=\"1\">";
            $data .= $this->ave_duty_times_plus . "時間(基準値)";
            $data .= "</td>\n";
        }
        $data .= "</tr>\n";

        return $data;
    }

    /*************************************************************************/
    // 月平均１日当たり看護補助配置数(新様式又は長期療養)
    /*************************************************************************/
    function showDataSummaryAssistanceAssignLong(
                    $excel_flg,         //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
                    $report_kbn,        //届出区分
                    $hosp_patient_cnt,  //１日平均入院患者数
                    $day_cnt,           //日数
                    $specific_data,     //特定入院料関連データ
                    $assistance_standard_assign,    //1日看護補助配置数(基準値)
                    $assistance_actual_assign,  //月平均１日あたり看護補助配置数(実績値)
                    $sum_night_d,       //月延べ夜勤時間数の計〔Ｄ〕
                    &$sum_night_h,      //月平均１日当たり夜間看護配置数〔D／(日数×１６）〕
                    &$sum_night_i,      //１日夜間看護配置数[A/12]※端数切上げ
                    $sum_sub_night_f,   //勤務時間数（看護補助者）
                    $sum_sub_night_g    //月平均１日あたり看護補助配置数〔Ｃ／(日数×８）〕
    )
    {
        $data = "<tr class=\"summary\">\n";
        $wk_colspan = ($excel_flg == "1") ? $this->colspan_summary["excel"][4] : 6;
        //地域包括ケア病棟 20140324
        if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
            $wk_title = "&nbsp;";
        }
        else {
            $wk_title = ($this->long_term_care_flg) ? "&nbsp;" : "＜看護職員夜間配置加算を届け出る場合＞";
        }

        $data .= "<td align=\"left\" colspan=\"{$wk_colspan}\">{$wk_title}</td>\n";
        $data .= "</tr>\n";

        if ($this->long_term_care_flg) {
            $wk = $assistance_actual_assign;
        } else {
            //〔D／(日数×16）〕、小数点1桁まで
            $wk = $sum_night_d / ($day_cnt * 16);
            $wk = floor($wk * 10) / 10;
            //月平均１日当たり夜間看護配置数〔D／(日数×１６）〕
            $sum_night_h = $wk;
        }
        $data .= "<tr class=\"summary\">\n";
        //地域包括ケア病棟 20140324
        if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
            $wk_title = "&nbsp;";
            $wk_str = "&nbsp;";
        }
        else {
            $wk_title = ($this->long_term_care_flg) ? "��-2　月平均１日当たり看護補助配置数" : "��-2　うち、月平均１日当たり夜間看護配置数";
            $wk_str = $wk . "人(実績値)";
        }
        $data .= "<td align=\"right\">$wk_title</td>\n";
        if ($excel_flg == "1") {
            $data .= "<td nowrap align=\"center\">" . $wk_str ."</td>\n";
        } else {
            $data .= "<td align=\"left\">";
            $data .= $wk_str;
            $data .= "</td>\n";
        }

        if ($this->long_term_care_flg) {
            $wk_title = "月延べ勤務時間数";
            if ($report_kbn == REPORT_KBN_SPECIFIC) {
                $wk = $sum_sub_night_f + $sum_sub_night_g;
            } else {
                $wk = $sum_sub_night_f;
            }
        }
        else {
            $wk_title = "月延べ夜勤時間数";
            $wk = $sum_night_d;
        }
        if ($wk == "") {
            $wk = 0;
        }
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"1\">" . $wk_title . "</td>\n";
            $data .= "<td nowrap align=\"center\" colspan=\"{$this->colspan_summary["excel"][3]}\">{$wk}時間(実績値)</td>\n";
        } else {
            $data .= "<td align=\"right\" colspan=\"3\">" . $wk_title . "</td>\n";
            $data .= "<td align=\"left\" colspan=\"1\">";
            $data .= $wk . "時間(実績値)";
            $data .= "</td>\n";
        }
        $data .= "</tr>\n";
        $data .= "<tr class=\"summary\">\n";

        //地域包括ケア病棟 20140324
        if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
            $wk_title = "&nbsp;";
            $wk_str = "&nbsp;";
            $wk = ceil($hosp_patient_cnt / 12);
            $sum_night_i = $wk;
        }
        else {
            if ($this->long_term_care_flg) {
                //1日看護補助配置数　　〔(A ／配置比率20)×３〕
                $wk_title = "1日看護補助配置数〔(A ／配置比率{$this->wk_staffing_rate})×３〕";
                $wk = $assistance_standard_assign;
            } else {
                $wk_title = "1日夜間看護配置数〔A/12〕※端数切上げ";
                $wk = ceil($hosp_patient_cnt / 12);
                $sum_night_i = $wk;
            }
            $wk_str = $wk . "人" . $this->wk_kijun_str;
        }

        $data .= "<td align=\"right\">$wk_title</td>\n";
        if ($excel_flg == "1") {
            $data .= "<td nowrap align=\"center\">" . $wk_str . "</td>\n";
        } else {
            $data .= "<td align=\"left\">";
            $data .= $wk_str;
            $data .= "</td>\n";
        }

        if ($this->long_term_care_flg) {
            $wk_title = "※「1日平均看護補助配置数」を満たす「月延べ勤務時間数」";
            $wk = $wk * 8 * $day_cnt;
        }
        else {
            $wk_title = "※「1日夜間看護配置数」を満たす「月延べ夜間時間数」";
            //「１日夜間看護配置数」 * 16 * 日数
            $wk = $wk * 16 * $day_cnt;
        }
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"1\">" . $wk_title . "</td>\n";
            $data .= "<td nowrap align=\"center\" colspan=\"{$this->colspan_summary["excel"][3]}\">{$wk}時間(基準値)</td>\n";
        } else {
            $data .= "<td align=\"right\" colspan=\"3\" nowrap>" . $wk_title . "</td>\n";
            $data .= "<td align=\"left\" colspan=\"1\">";
            $data .= $wk . "時間(基準値)";
            $data .= "</td>\n";
        }

        $data .= "</tr>\n";

        return $data;
    }

    /*************************************************************************/
    // 月平均１日当たり看護補助配置数(一般)
    /*************************************************************************/
    function showDataSummaryAssistanceAssign(
                    $excel_flg,                     //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
                    $hosp_patient_cnt,              //１日平均入院患者数
                    $assistance_nurse_sumtime,      //勤務時間数（看護補助者）
                    $day_cnt,                       //日数
                    $nurse_staffing_flg,            //看護配置加算の有無
                    $acute_nursing_flg,             //急性期看護補助加算の有無
                    $acute_nursing_cnt,             //急性期看護補助加算の配置比率
                    $nursing_assistance_cnt,        //看護補助加算の配置比率
                    $acute_assistance_cnt,          //急性期看護補助体制加算の配置比率
                    $assistance_standard_assign,    //1日看護補助配置数(基準値)
                    $assistance_actual_assign,      //月平均１日あたり看護補助配置数(実績値)
                    $report_kbn2,       //届出区分（調整）
                    $sum_night_c,       //月延べ勤務時間数の計〔Ｃ〕
                    $sum_night_f,       //1日看護配置数〔(A ／届出区分の数)×３〕
                    $sum_sub_night_g    //みなし看護補助者の月延べ勤務時間数の計〔Ｇ〕　〔Ｃ−1日看護配置数×８×日数〕
    )
    {
        //-------------------------------------------------------------------------------
        // �� 月平均１日当たり看護補助配置数
        //-------------------------------------------------------------------------------
        //　〔(F+G) ／ (日数×８) 〕
        //小数点１桁まで表示
        $wk_title = "�� 月平均１日当たり看護補助者配置数";
        $data = "<tr class=\"summary\">\n";
        $data .= "<td align=\"right\">{$wk_title}</td>\n";
        if ($excel_flg == "1") {
            $data .= "<td nowrap align=\"center\">" . $assistance_actual_assign . "人(実績値)</td>\n";
        } else {
            $data .= "<td align=\"left\">";
            $data .= $assistance_actual_assign . "人(実績値)";
            $data .= "</td>\n";
        }

        //月延べ看護補助時間数＝Ｆ＋Ｇ
        //小数点１桁まで表示
        $wk = $assistance_nurse_sumtime + $sum_sub_night_g;
        $wk = floor($wk * 10) / 10;

        $wk_title = "月延べ勤務時間数";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"1\">" . $wk_title . "</td>\n";
            $data .= "<td nowrap align=\"center\" colspan=\"1\">" . $wk . "時間(実績値)</td>\n";
        } else {
            $data .= "<td align=\"right\" colspan=\"3\">" . $wk_title . "</td>\n";
            $data .= "<td align=\"left\" colspan=\"1\">";
            $data .= $wk . "時間(実績値)";
            $data .= "</td>\n";
        }
        $data .= "</tr>\n";

        /*
        //2010年4月看護補助加算 20101008
        if ($nurse_staffing_flg == "1") {
            if ($acute_nursing_flg == "1") {
                $report_kbn2 = ($acute_nursing_cnt != "") ? $acute_nursing_cnt : 50;
            } else {
                $report_kbn2 = ($nursing_assistance_cnt != "") ? $nursing_assistance_cnt : 30;
            }
        } else {
            $report_kbn2 = "";
        }
        */

        $data .= "<tr class=\"summary\">\n";
        $data .= "<td align=\"right\">1日看護補助者配置数〔(A ／配置比率" . $report_kbn2 . ")×３〕</td>\n";
        if ($excel_flg == "1") {
            $data .= "<td nowrap align=\"center\">" . $assistance_standard_assign . "人(基準値)</td>\n";
        } else {
            $data .= "<td align=\"left\">";
            $data .= $assistance_standard_assign . "人(基準値)";
            $data .= "</td>\n";
        }

        //1日看護補助配置数〔(A ／配置比率)×３〕＊ 8 ＊ 今月の稼働日数
        $wk = $assistance_standard_assign * 8 * $day_cnt;
        $wk_title = "※「1日平均看護補助者配置数」を満たす「月延べ勤務時間数」";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"1\">" . $wk_title . "</td>\n";
            $data .= "<td nowrap align=\"center\" colspan=\"1\">" . $wk . "時間(基準値)</td>\n";
        } else {
            $data .= "<td align=\"right\" colspan=\"3\" nowrap>" . $wk_title . "</td>\n";
            $data .= "<td align=\"left\" colspan=\"1\">";
            $data .= $wk . "時間(基準値)";
            $data .= "</td>\n";
        }
        $data .= "</tr>\n";

        return $data;
    }

    /*************************************************************************/
    // 今月の稼働日数と常勤職員の週所定労働時間
    /*************************************************************************/
    function showDataLaborTime(
                    $duty_yyyy,    //対象年
                    $duty_mm,      //対象月
                    $day_cnt,      //日数
                    $labor_cnt,    //常勤職員の週所定労働時間
                    $excel_flg     //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
    ) {
        $data  = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"non_list\">\n";
        $data .= "<tr style=\"font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;\" height=\"30\">\n";
        if ($excel_flg == "1") {
            $data .= "<td nowrap align=\"left\" colspan=\"" . ($this->top_colspan + $wk_colspan) . "\" valign=\"bottom\">" . $duty_yyyy . "年" . $duty_mm . "月" . "</td>\n";
        } else {
            $data .= "<td align=\"left\" valign=\"bottom\">";
            $data .= $duty_yyyy . "年";
            $data .= $duty_mm ."月";
            $data .= "</td>\n";
        }
        $data .= "</tr>\n";

        if ($excel_flg == "1") {
            $data .= "</table>";
            $data .= "<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;\">\n";
        } else {
            $data .= "</table>\n";
            $data .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list outer\" style=\"font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;\">\n";
        }

        $data .= "<tr class=\"labor_time\">\n";
        $wk_title = "今月の稼働日数";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"{$this->colspan_fc["excel"][0]}\">{$wk_title}</td>\n";
            $data .= "<td nowrap align=\"center\" colspan=\"{$this->colspan_fc["excel"][5]}\">{$day_cnt}日</td>\n";
        } else {
            $data .= "<td align=\"right\" style=\"width: 350px;\">{$wk_title}</td>\n";
            $data .= "<td align=\"left\" colspan=\"5\">";
            $data .= $day_cnt . "日</td>\n";
        }

        $data .= "<tr class=\"labor_time\">\n";
        $wk_title = "常勤職員の週所定労働時間";
        if ($excel_flg == "1") {
            $data .= "<td align=\"right\" colspan=\"{$this->colspan_fc["excel"][0]}\">{$wk_title}</td>\n";
            $data .= "<td nowrap align=\"center\" colspan=\"{$this->colspan_fc["excel"][5]}\">{$labor_cnt}時間</td>\n";
        } else {
            $data .= "<td align=\"right\" style=\"width: 350px;\">{$wk_title}</td>\n";
            $data .= "<td align=\"left\" colspan=\"5\">";
            $data .= $labor_cnt . "時間</td>\n";
        }
        $data .= "</tr>\n";
        $data .= "</table>";

        return $data;
    }

    /*************************************************************************/
    // 月延べ夜勤時間数を算出
    /*************************************************************************/
    function calcNightSumTime(
                    $data_array,    //看護師／准看護師情報
                    $day_cnt,       //日数
                    $labor_cnt,     //常勤職員の週所定労働時間
                    &$sum_night_b,  //夜勤従事者数(夜勤ありの職員数)〔Ｂ〕
                    &$sum_night_d,  //月延べ夜勤時間数の計〔Ｄ〕
                    &$sum_night_e   //夜勤専従者及び月16時間以下の者の夜勤時間数〔E〕
    ) {
        for ($i=0; $i<count($data_array); $i++) {
            $wk = 0.0;
            $wk_duty_time = 0.0;
            for ($k=1; $k<=$day_cnt; $k++) {
                $wk += $data_array[$i]["night_time_$k"];
                $wk_duty_time += $data_array[$i]["duty_time_$k"];
            }
            $sum_night_d += $wk;
            if (($data_array[$i]["night_duty_flg"] == "2") ||
                ($data_array[$i]["duty_form"] != "3" && $wk <= 16) ||   //常勤、非常勤で16時間以内
                ($data_array[$i]["duty_form"] == "3" && $wk < 12)        //短時間で12時間未満
                ) {
                $sum_night_e += $wk;
            } else {

                //"other_post_flg"他部署兼務（１：有り）
                //"duty_form"勤務形態（1:常勤、2:非常勤）
                if (($data_array[$i]["other_post_flg"] == "1") ||
                        ($data_array[$i]["duty_form"] == "2") ||
                        ($data_array[$i]["duty_form"] == "3") ||
                        ($data_array[$i]["duty_form"] == "")) {
                    //時間数から率を計算、小数点第3位以下四捨五入へ変更 20120315
                    // 1より大きい場合の対応 20120501
                    $wk_rate = round((($wk_duty_time + $wk) / ($day_cnt * $labor_cnt / 7)) * 100) / 100;
                    if ($wk_rate > 1) {
                        $wk_rate = 1;
                    }
                    $sum_night_b += $wk_rate;
                } else {
                    //常勤の場合で16時間超の夜勤有りの場合
                    if ($wk >16) {
                        //夜勤従事者数に1を計上
                        $sum_night_b += 1;
                    }
                }
            }
        }
    }

    /*************************************************************************/
    // 月延べ勤務時間数の計、月平均１日あたり看護配置数、1日看護配置数を算出
    /*************************************************************************/
    function calcNurseAssign(
                    $report_kbn,            //届出区分
                    $wk_report_kbn,         //届出区分WK
                    $hosp_patient_cnt,      //１日平均入院患者数
                    $nurse_sumtime,         //勤務時間数（看護師）
                    $sub_nurse_sumtime,     //勤務時間数（准看護師）
                    $day_cnt,               //日数
                    $nurse_staffing_cnt,    //看護配置
                    &$sum_night_c,          //月延べ勤務時間数の計
                    &$sum_night_f,          //1日看護配置数(基準値)
                    &$sum_night_g           //月平均１日あたり看護配置数(実績値)
    )
    {
        //月延べ勤務時間数の計を算出
        $sum_night_c = $nurse_sumtime + $sub_nurse_sumtime;

        //月平均１日あたり看護配置数を算出
        $wk = $sum_night_c / ($day_cnt * 8);
        $wk *= 10;
        $wk = (int)$wk; //切捨てに変更 20100729
        $sum_night_g = $wk / 10;

        //1日看護配置数を算出
        if (($report_kbn == REPORT_KBN_SPECIFIC) || ($report_kbn == REPORT_KBN_SPECIFIC_PLUS)) {
            // 届出区分が「特定入院料」又は「特定入院料（看護職員＋看護補助者）」の場合、入力された配置数を使用
            $staffing_cnt = $nurse_staffing_cnt;
        } else {
            $staffing_cnt = $wk_report_kbn;
        }

        $wk1 = ($hosp_patient_cnt / $staffing_cnt) * 3;
        $sum_night_f = ceil($wk1); //切り上げに変更 20100729
        if ($report_kbn == REPORT_KBN_SPECIFIC_PLUS) {
            $sum_night_f = ceil($sum_night_f * 50 / 100); // 看護要員中に必要な看護職員の比率は固定で50%
        }
    }

    /*************************************************************************/
    // 1日看護補助配置数(基準値)を算出
    /*************************************************************************/
    function calcAssistanceStandard(
                    $report_kbn,                    //届出区分
                    $hosp_patient_cnt,              //１日平均入院患者数
                    $nursing_assistance_cnt,        //看護補助加算の配置比率
                    $acute_assistance_cnt,          //急性期看護補助体制加算の配置比率
                    $assistance_staffing_cnt,       //看護補助配置
                    $long_term_care_flg,            //長期療養フラグ
                    $wk_report_kbn,                 //届出区分WK
                    $wk_duty_yyyymm,                //年月WK
                    &$report_kbn2,                  //届出区分（調整）
                    &$assistance_standard_assign    //1日看護補助配置数(基準値)
    ) {
        if ($long_term_care_flg) {
            if ($report_kbn == REPORT_KBN_SPECIFIC) {
                $staffing_cnt = $assistance_staffing_cnt;
            } else {
                $staffing_cnt = $wk_report_kbn;
            }
            $assistance_standard_assign = ceil(($hosp_patient_cnt / $staffing_cnt) * 3);
        } else {
            //地域包括ケア 20140326
            if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
                $report_kbn2 = 25;
            }
            //計算されない不具合 20120319
            elseif ($wk_report_kbn >= 13) {
                $report_kbn2 = ($nursing_assistance_cnt != "") ? $nursing_assistance_cnt : "";
            } else {
                //201204新様式
                if ($wk_duty_yyyymm >= "201204") {
                    $report_kbn2 = ($acute_assistance_cnt != "") ? $acute_assistance_cnt : "";
                }
                else {
                    $report_kbn2 = ($acute_nursing_cnt != "") ? $acute_nursing_cnt : "";
                }
            }
            //整数で表示
            if ($report_kbn2 != "") {
                $wk = ($hosp_patient_cnt / $report_kbn2) * 3;
                $assistance_standard_assign = ceil($wk); //切り上げに変更 20100729
            }else {
                $assistance_standard_assign = 0;
            }
        }
    }

    /**************************************************************************************/
    // 月平均１日あたり看護補助配置数(実績値)とみなし看護補助者の月延べ勤務時間数の計を算出
    /**************************************************************************************/
    function calcAssistanceActual(
                    $report_kbn,                //届出区分
                    $ave_duty_times,            //「1日平均看護配置数」を満たす「月延べ勤務時間数」
                    $day_cnt,                   //日数
                    $assistance_nurse_sumtime,  //看護補助者総勤務時間;〔F〕
                    $sum_night_c,               //月延べ勤務時間数の計
                    &$sum_sub_night_g,          //みなし看護補助者の月延べ勤務時間数の計〔G〕　〔C - 1日看護配置数×８×日数〕
                    &$assistance_actual_assign  //月平均１日あたり看護補助配置数(実績値)
    ) {
//地域包括、みなし分を除く対応20150323
		global $duty_yyyy, $duty_mm;
		$wk_duty_yyyymm = $duty_yyyy.sprintf("%02d", $duty_mm);
        if ($report_kbn == REPORT_KBN_SPECIFIC_PLUS   //特定入院料（看護職員＋看護補助者）
			|| ($wk_duty_yyyymm >= "201504" && ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2))
        	) {
            $sum_sub_night_g = 0;
        } else {
            // みなし看護補助者の月延べ勤務時間数の計〔Ｇ〕〔Ｃ−１日看護配置数×８×日数：Ｃ−「1日平均看護配置数」を満たす「月延べ勤務時間数」〕
            $wk_kangokasan = 0;
            if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
            	global $nurse_staffing_flg, $hosp_patient_cnt;
            	if ($nurse_staffing_flg == "1") {
            		$wk_kangokasan = ($hosp_patient_cnt / 50) * 3 * $day_cnt * 8;
            	}
            }
            $sum_sub_night_g = max($sum_night_c - $ave_duty_times - $wk_kangokasan, 0);
        }

        //〔(F+G) ／ (日数×８) 〕、小数点1桁まで
        $assistance_actual_assign = ($assistance_nurse_sumtime + $sum_sub_night_g) / ($day_cnt * 8);
        $assistance_actual_assign = floor($assistance_actual_assign * 10) / 10;
    }

    //-------------------------------------------------------------------------------
    // �ゴ埜醉廾�の月平均夜勤時間数
    //-------------------------------------------------------------------------------
    function calcNightSumTimeAverage(
                    $excel_flg,             //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
                    $data_sub_array,        //看護補助者情報
                    $report_kbn,            //届出区分
                    $day_cnt,               //日数
                    $labor_cnt,             //常勤職員の週所定労働時間
                    $sum_night_b,           //夜勤従事者数(夜勤ありの職員数)〔Ｂ〕
                    $sum_night_d,           //月延べ夜勤時間数の計〔Ｄ〕
                    $sum_night_e,           //夜勤専従者及び月16時間以下の者の夜勤時間数〔Ｅ〕
                    &$sum_sub_night_b,      //夜勤従事者数(夜勤ありの職員数)〔Ｂ〕
                    &$sum_sub_night_d,      //月延べ夜勤時間数の計〔Ｄ〕
                    &$sum_sub_night_e       //夜勤専従者及び月16時間以下の者の夜勤時間数〔Ｅ〕
    ) {
        ///------------------------------------------------------------------------------
        //＜看護師／准看護師＞
        ///------------------------------------------------------------------------------
        //夜勤従事者数(夜勤ありの職員数)〔Ｂ〕 -------------- (a-1)
        //月延べ夜勤時間数の計〔Ｄ〕------------------------- (a-2)
        //夜勤専従者及び月16時間以下の者の夜勤時間数〔E〕---- (a-3)

        //月延べ夜勤時間数　〔D−E〕 ------------------------ (a-4)
        $wk_d_e = $sum_night_d - $sum_night_e;
        ///------------------------------------------------------------------------------
        //＜看護補助者＞
        ///------------------------------------------------------------------------------
        //夜勤従事者数(夜勤ありの職員数)〔Ｂ〕 -------------- (b-1)
        //月延べ夜勤時間数の計〔Ｄ〕------------------------- (b-2)
        //夜勤専従者及び月16時間以下の者の夜勤時間数〔E〕---- (b-3)
        //excelの場合は、引数を使用するため計算しない 20110901
        if ($excel_flg != "1") {
            for ($i=0; $i<count($data_sub_array); $i++) {
                $wk = 0.0;
                $wk_duty_time = 0.0;
                for ($k=1; $k<=$day_cnt; $k++) {
                    $wk += $data_sub_array[$i]["night_time_$k"];
                    $wk_duty_time += $data_sub_array[$i]["duty_time_$k"];
                }
                $sum_sub_night_d += $wk;
                if (($data_sub_array[$i]["night_duty_flg"] == "2") ||
                        ($data_sub_array[$i]["duty_form"] != "3" && $wk <= 16) ||   //常勤、非常勤で16時間以内
                        ($data_sub_array[$i]["duty_form"] == "3" && $wk < 12)        //短時間で12時間未満
                    ) {
                    $sum_sub_night_e += $wk;
                } else {
                    //"other_post_flg"他部署兼務（１：有り）
                    //"duty_form"勤務形態（1:常勤、2:非常勤）
                    if (($data_sub_array[$i]["other_post_flg"] == "1") ||
                            ($data_sub_array[$i]["duty_form"] == "2") ||
                            ($data_sub_array[$i]["duty_form"] == "3") ||
                            ($data_sub_array[$i]["duty_form"] == "")) {
                        //時間数から率を計算、小数点第3位以下四捨五入へ変更 20120315
                        $wk_rate = round((($wk_duty_time + $wk) / ($day_cnt * $labor_cnt / 7)) * 100) / 100;
                        if ($wk_rate > 1) {
                            $wk_rate = 1;
                        }
                        $sum_sub_night_b += $wk_rate;
                    } else {
                        //常勤の場合で16時間超の夜勤有りの場合
                        if ($wk >16) {
                            //夜勤従事者数に1を計上
                            $sum_sub_night_b += 1;
                        }
                    }
                }
            }
        }
        //夜勤従事者数[B]の計は小数点1桁まで 20120315 エクセル時も端数処理する20140331
        $sum_night_b = floor($sum_night_b * 10) / 10;
        $sum_sub_night_b = floor($sum_sub_night_b * 10) / 10;
        //月延べ夜勤時間数　〔D−E〕 ------------------------ (b-4)
        $wk_sub_d_e = $sum_sub_night_d - $sum_sub_night_e;
        ///------------------------------------------------------------------------------
        // ((a-4) + (b-4)) / ((a-1) + (b-1))
        //小数点1桁まで表示 切捨てに変更 20100729
        ///------------------------------------------------------------------------------
        //療養病棟の場合、補助者を含む 20120123
        if ($report_kbn == REPORT_KBN_CURE_BASIC1 || $report_kbn == REPORT_KBN_CURE_BASIC2) {
            $wk = ($wk_d_e + $wk_sub_d_e) / ($sum_night_b + $sum_sub_night_b);
        }
        //一般の場合、補助者を含まない
        else {
            $wk = ($wk_d_e) / ($sum_night_b);
        }
        $wk *= 10;
        $wk = (int)($wk);
        $wk /= 10;
        $wk = sprintf("%01.1f",$wk);
        $wk = $this->obj->rtrim_zero($wk);

        return $wk;
    }

    /*************************************************************************/
    // ４週計算時の年月チェック
    /*************************************************************************/
    function checkYm4Week(
                    $duty_yyyy, //対象年
                    $duty_mm,   //対象月
                    &$date_y1,  //カレンダー用年
                    &$date_m1   //カレンダー用月
    ) {
        $wk_err_flg = "";
        if (($date_y1 != $duty_yyyy) ||
                ($date_m1 != $duty_mm)) {
            //エラーメッセージ
            $wk = "指定年月での４週計算はできません。";
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>alert('$wk');</script>");

            $date_y1 = $duty_yyyy;
            $date_m1 = $duty_mm;

            $wk_err_flg = "1";
        }
        return $wk_err_flg;
    }

    /*************************************************************************/
    // ４週計算用の終了年月日設定
    /*************************************************************************/
    function setEndYmd4Week(
                    $night_start_day,   //４週計算用（開始日）
                    $duty_yyyy,         //対象年
                    $duty_mm,           //対象月
                    &$end_yyyy,         //終了年
                    &$end_mm,           //終了月
                    &$end_dd            //終了日
    ) {
        $wk_day = (int)$night_start_day + 27;
        $tmp_time = mktime(0, 0, 0, $duty_mm, $wk_day, $duty_yyyy);
        $tmp_date = date("Ymd", $tmp_time);
        $end_yyyy = (int)substr($tmp_date,0,4);
        $end_mm = (int)substr($tmp_date,4,2);
        $end_dd = (int)substr($tmp_date,6,2);
    }

    //atdptnテーブルから勤務開始時刻が前日になる場合がある勤務パターンを取得する 20131004
    //[tmcd_group_id][pattern]がキーとなる配列に値1が設定される
    function get_arr_previous_day_possible_flag() {

        $arr_ret = array();
        $sql = "select group_id, atdptn_id from atdptn ";
        $cond = "where previous_day_possible_flag = 1 ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        while ($row = pg_fetch_assoc($sel)) {
            $arr_ret[ $row['group_id'] ][ $row['atdptn_id'] ] = 1;
        }
        return $arr_ret;
    }
    
    /**
     * get_project_data 除外用関数 20140404
     * 
     * @param mixed $emp_id 職員ID
     * @param mixed $start_date 開始日
     * @param mixed $end_date 終了日
     * @param mixed $arr_no_sub_pjt 控除しない委員会
     * @return mixed 委員会出席時刻の配列
     *
     */
    function get_project_data($emp_id, $start_date, $end_date, $arr_no_sub_pjt) {
        $arr_ret = array();
        $sql = "select a.pjt_id, c.pjt_name, b.prcd_date as start_date, b.prcd_start_time as start_time, b.prcd_end_time as end_time, c.pjt_public_flag "
            ." from prcdatnd a "
            ." inner join proceeding b on b.pjt_schd_id = a.pjt_schd_id "
            ." inner join project c on c.pjt_id = a.pjt_id and c.pjt_delete_flag = 'f' "
        ;
        $cond = "where a.emp_id = '$emp_id' "
            ." and b.prcd_date >= '$start_date' "
            ." and b.prcd_date <= '$end_date' "
            ." order by b.prcd_date, b.prcd_start_time ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $i = 0;
        $key = "";
        while ($row = pg_fetch_assoc($sel)) {
            //控除しない委員会か確認
            if ($arr_no_sub_pjt[ $row["pjt_id"] ] == "1") {
                continue;
            }
            $wk_date = $row["start_date"];
            if ($wk_date != $key) {
                $i = 0;
            }
            $arr_ret[$wk_date][$i] = $row;
            $key = $wk_date;    
            $i++;
        }
        return $arr_ret;
    }    

    //控除しない委員会WGのリスト取得
    //$arr[$pjt_id] = "1"
    function get_no_subtract_project() {
        $arr_ret = array();
        $sql = "select pjt_id from project where pjt_delete_flag = 'f' and pjt_parent_id in "
            ." (select a.pjt_id from duty_shift_rpt_no_subtract_pjt a "
            ." left join project b on a.pjt_id = b.pjt_id "
            ." where b.pjt_delete_flag = 'f' and b.pjt_parent_id is null) "
            ." union "
            ." select a.pjt_id from duty_shift_rpt_no_subtract_pjt a "
            ." left join project b on a.pjt_id = b.pjt_id "
            ." where b.pjt_delete_flag = 'f' ";
        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        while ($row = pg_fetch_assoc($sel)) {
            $arr_ret[$row["pjt_id"]] = "1";
        }
        return $arr_ret;
    }    
}
