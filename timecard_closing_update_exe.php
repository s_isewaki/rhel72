<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 締め日未選択の場合は空文字を登録
if ($closing == "--") {
	$closing = "";
}
// ***** oose add start *******/
if ($closing_parttime == "--") {
	$closing_parttime = "";
}
// ***** oose add end *******/

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin transaction");

// タイムカードレコードがなければ作成、あれば更新
$sql = "select count(*) from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
if (pg_fetch_result($sel, 0, 0) == 0) {
// ***** oose update start *******/
//	$sql = "insert into timecard (closing) values (";
//	$content = array($closing);
	$sql = "insert into timecard (closing, closing_parttime, closing_paid_holiday, closing_month_flg) values (";
	$content = array($closing, $closing_parttime, $closing_paid_holiday, $closing_month_flg);
// ***** oose update end *******/
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
} else {
	$sql = "update timecard set";
	//締め日（非常勤）、有給休暇締め日追加
	$set = array("closing", "closing_parttime", "closing_paid_holiday", "closing_month_flg");
	$setvalue = array($closing, $closing_parttime, $closing_paid_holiday, $closing_month_flg);
	$cond = "";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query("commit");

// データベース接続を閉じる
pg_close($con);

// 締め日画面を再表示
echo("<script type=\"text/javascript\">location.href = 'timecard_closing.php?session=$session';</script>");
?>
