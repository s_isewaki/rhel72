<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("cl_application_workflow_common_class.php");
require_once("cl_common.ini");
require_once("cl_title_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session=="0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, $CL_MANAGE_AUTH, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$obj = new cl_application_workflow_common_class($con, $fname);

if($mode == "select")
{

// カテゴリ名
if($selected_cate != "")
{
	$wkfl_mst = $obj->get_wkfwcate_mst($selected_cate);
	$folder_path = $wkfl_mst["wkfw_nm"];
}
// フォルダ名
if($selected_folder != "")
{
	$folder_list = $obj->get_folder_path($selected_folder);
	foreach($folder_list as $folder)
	{
		if($folder_path != "")
		{
			$folder_path .= " > ";
		}
		$folder_path .= $folder["name"]; 
	}
}

if($folder_path == "")
{
	$folder_path = "ルート";
}

$folder_path = htmlspecialchars($folder_path, ENT_QUOTES);
$folder_path = str_replace("&#039;", "\'", $folder_path);
$folder_path = str_replace("&quot;", "\"", $folder_path);

$cl_title = cl_title_name();
?>
<script type="text/javascript">


if(window.opener && !window.opener.closed && window.opener.setCategory){
    window.opener.setCategory('<?=$folder_path?>', '<?=$selected_cate?>', '<?=$selected_folder?>');
}

self.close();
</script>
<?
}

// ワークフローカテゴリ/フォルダ情報取得 
$tree_list = $obj->get_workflow_folder_list();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cl_title?> | 保存先選択</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/yui/build/yahoo/yahoo-min.js"></script>
<script type="text/javascript" src="js/yui/build/treeview/treeview-min.js"></script>
<link rel="stylesheet" type="text/css" href="js/yui/build/treeview/css/folders/tree.css">
<script type="text/javascript">

var tree = null;
function initPage()
{
<?
if(count($tree_list) > 0)
{
?>
	// ツリービュー初期化
	tree = new YAHOO.widget.TreeView("tree1");

	<?
	if($screen_mode == "")
	{
	?>
	var root = tree.getRoot(); 
	// ルート
	var root1 = new YAHOO.widget.TextNode('<font size="3" face="ＭＳ Ｐゴシック, Osaka" class=j12>ルート</font><span style="display:none;"></span>', root, true);
	root1.onLabelClick = function () {location.href = '<?=$fname?>?session=<?=$session?>&mode=select'; return false;};
	<?
	}
	else if($screen_mode == "workflow")
	{
	?>
	var root1 = tree.getRoot(); 
	<?
	}
	?>

	<?
	if($selected_cate == "" && $selected_folder == "")
	{
	?>
	root1.labelStyle = 'ygtvlabel selectedLabel';
	<?
	}

	if($mode == "cate_upd")
	{
		$arr_child_folder = array();
		if($cate_id != "" && $folder_id == "")
		{
			// 子フォルダ取得
			$arr_child_folder = $obj->get_wkfwfolder_from_wkfw_type($cate_id);
		}
		else if($cate_id != "" && $folder_id != "")
		{
			// 子フォルダ取得
			$obj->get_child_folder($folder_id, $arr_child_folder);
			$arr_child_folder[] = $folder_id;

			$cate_id = "";
		}

		show_wkfw_folder_js_for_cate_upd($tree_list, $session, $fname, $selected_cate, $selected_folder, $cate_id, $arr_child_folder);
	}
	else
	{
		show_wkfw_folder_js($tree_list, $session, $fname, $selected_cate, $selected_folder);
	}
	?>
	tree.draw();
<?
}
?>
}

function expandAncestor(node)
{
	if (node.parent) {
		node.parent.expand();
		expandAncestor(node.parent);
	}
}

function expandAll()
{
	if (tree) {
		tree.expandAll();
		document.getElementById('expand').style.display = 'none';
		document.getElementById('collapse').style.display = '';
	}
}

function collapseAll()
{
	if (tree) {
		tree.collapseAll();
		document.getElementById('collapse').style.display = 'none';
		document.getElementById('expand').style.display = '';
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#A0D25A solid 1px;}
.list td td {border-width:0;}
.selectedLabel {font-weight:bold; color:black; padding:1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initPage();">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#A0D25A">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>保存先選択</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
</tr>
<tr height="2">
<td></td>
</tr>
<tr valign="top">
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22" bgcolor="#E5F6CD">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">保存先</font></td>
<td align="right">
<span id="expand"><input type="button" value="全て開く" onclick="expandAll();"></span>
<span id="collapse" style="display:none;"><input type="button" value="全て閉じる" onclick="collapseAll();"></span>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td valign="top" height="400"><div id="tree1"></div></td>
</tr>
</table>
</td>
</tr>
<tr height="2">
<td></td>
</tr>
</table>
</center>
</body>
</html>
<?
/**
 * フォルダツリーのjavascript表示
 */
function show_wkfw_folder_js($tree_list, $session, $fname, $selected_cate, $selected_folder)
{
	foreach ($tree_list as $tree)
	{
		$type    = $tree["type"];
		$folders = $tree["folders"];

		// カテゴリ出力
		if($type == "category")
		{
			$wkfw_nm = htmlspecialchars($tree["wkfw_nm"], ENT_QUOTES);
			$wkfw_type = $tree["wkfw_type"];
			$node_name = "folder$wkfw_type";
			echo("var $node_name = new YAHOO.widget.TextNode('<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wkfw_nm</font><span style=\"display:none;\"></span>', root1, false);\n");
			echo("$node_name.onLabelClick = function () {location.href = '$fname?session=$session&mode=select&selected_cate=$wkfw_type'; return false;}\n");

			if($selected_cate == $wkfw_type && $selected_folder == "")
			{
				echo("$node_name.labelStyle = 'ygtvlabel selectedLabel';\n");
				echo("expandAncestor($node_name);\n");
			}

		}
		// フォルダ出力
		else if($type == "folder")
		{
			$tmp_id         = $tree["id"];
			$tmp_cate_id    = $tree["cate"];
			$tmp_parent_id  = $tree["parent_id"];
			$tmp_parent_elm = ($tmp_parent_id == "") ? "folder$tmp_cate_id" : "folder{$tmp_cate_id}_{$tmp_parent_id}";
			$name           = htmlspecialchars($tree["name"], ENT_QUOTES);
			$node_name      = "folder{$tmp_cate_id}_{$tmp_id}";
			echo("var $node_name = new YAHOO.widget.TextNode('<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$name</font><span style=\"display:none;\"></span>', $tmp_parent_elm, false);\n");
			echo("$node_name.onLabelClick = function () {location.href = '$fname?session=$session&mode=select&selected_cate=$tmp_cate_id&selected_folder=$tmp_id'; return false;}\n");

			if($selected_cate == $tmp_cate_id && $selected_folder == $tmp_id)
			{
				echo("$node_name.labelStyle = 'ygtvlabel selectedLabel';\n");
				echo("expandAncestor($node_name);\n");
			}
		}
		show_wkfw_folder_js($folders, $session, $fname, $selected_cate, $selected_folder);
	}

}


/**
 * フォルダツリーのjavascript表示
 */
function show_wkfw_folder_js_for_cate_upd($tree_list, $session, $fname, $selected_cate, $selected_folder, $cate_id, $arr_child_folder)
{
	foreach ($tree_list as $tree)
	{
		$type    = $tree["type"];
		$folders = $tree["folders"];

		// カテゴリ出力
		if($type == "category")
		{
			$wkfw_nm = htmlspecialchars($tree["wkfw_nm"], ENT_QUOTES);
			$wkfw_type = $tree["wkfw_type"];
			$node_name = "folder$wkfw_type";

			if($wkfw_type != $cate_id)
			{
				echo("var $node_name = new YAHOO.widget.TextNode('<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wkfw_nm</font><span style=\"display:none;\"></span>', root1, false);\n");
				echo("$node_name.onLabelClick = function () {location.href = '$fname?session=$session&mode=select&selected_cate=$wkfw_type'; return false;}\n");
			}
			else
			{
				echo("var $node_name = new YAHOO.widget.TextNode('<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"black\">$wkfw_nm</font><span style=\"display:none;\"></span>', root1, false);\n");
			}			

			if($selected_cate == $wkfw_type && $selected_folder == "")
			{
				echo("$node_name.labelStyle = 'ygtvlabel selectedLabel';\n");
				echo("expandAncestor($node_name);\n");
			}

		}
		// フォルダ出力
		else if($type == "folder")
		{
			$tmp_id         = $tree["id"];
			$tmp_cate_id    = $tree["cate"];
			$tmp_parent_id  = $tree["parent_id"];
			$tmp_parent_elm = ($tmp_parent_id == "") ? "folder$tmp_cate_id" : "folder{$tmp_cate_id}_{$tmp_parent_id}";
			$name           = htmlspecialchars($tree["name"], ENT_QUOTES);
			$node_name      = "folder{$tmp_cate_id}_{$tmp_id}";

			$non_selected_flg = false;
			foreach($arr_child_folder as $child_folder)
			{
				if($tmp_id == $child_folder)
				{
					$non_selected_flg = true; 
				}
			}

			if(!$non_selected_flg)
			{
				echo("var $node_name = new YAHOO.widget.TextNode('<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$name</font><span style=\"display:none;\"></span>', $tmp_parent_elm, false);\n");
				echo("$node_name.onLabelClick = function () {location.href = '$fname?session=$session&mode=select&selected_cate=$tmp_cate_id&selected_folder=$tmp_id'; return false;}\n");
			}
			else
			{
				echo("var $node_name = new YAHOO.widget.TextNode('<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"black\">$name</font><span style=\"display:none;\"></span>', $tmp_parent_elm, false);\n");
			}
			if($selected_cate == $tmp_cate_id && $selected_folder == $tmp_id)
			{
				echo("$node_name.labelStyle = 'ygtvlabel selectedLabel';\n");
				echo("expandAncestor($node_name);\n");
			}
		}
		show_wkfw_folder_js_for_cate_upd($folders, $session, $fname, $selected_cate, $selected_folder, $cate_id, $arr_child_folder);
	}

}


// データベース切断
pg_close($con);
?>
