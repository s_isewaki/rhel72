<?

ob_start();

ini_set("max_execution_time", 600);

$session = $s;
$lib_id = $i;
$lib_url = $u;

require_once("Cmx.php");
require_once("about_comedix.php");
require_once("library_common.php");

$fname = $PHP_SELF;

if ($session != "") {

    // セッションのチェック
    $session = qualify_session($session, $fname);
    if ($session == "0") {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
        exit;
    }

    // 権限のチェック
    $check_auth = check_authority($session, 32, $fname);
    if ($check_auth == "0") {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
        exit;
    }
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// セッションIDがあれば職員IDを取得
if ($session != "") {
    $sql = "select emp_id from session";
    $cond = "where session_id = '$session'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $emp_id = pg_fetch_result($sel, 0, "emp_id");
}
else {
    $emp_id = null;
}

// ログを保存
$sql = "insert into libreflog (lib_id, ref_date, emp_id) values (";
$content = array($lib_id, date("YmdHi"), $emp_id);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 参照数を取得
$sql = "select ref_count, lib_nm, lib_extension from libinfo";
$cond = "where lib_id = '$lib_id' for update";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$ref_count = intval(pg_fetch_result($sel, 0, "ref_count"));
$lib_nm = pg_fetch_result($sel, 0, "lib_nm");
$lib_extension = pg_fetch_result($sel, 0, "lib_extension");

// 参照数を更新
$sql = "update libinfo set";
$set = array("ref_count");
$setvalue = array($ref_count + 1);
$cond = "where lib_id = '$lib_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

$file_path = $lib_url;
// 運用ディレクトリ名を取得
$dir = str_replace($_SERVER["DOCUMENT_ROOT"], "", $_SERVER["SCRIPT_FILENAME"]);
$dir = str_replace("library_refer.php", "", $dir);
$scheme = ($_SERVER['SERVER_PORT'] == 443) ? "https" : "http";
$lib_url = $scheme . "://" . $_SERVER['HTTP_HOST'] . $dir . $lib_url;

// file download
$filename_flg = lib_get_filename_flg();
if ($filename_flg > 0) {
    if (is_file($file_path)) {
        if ($filename_flg == 1) { // MSIE
            $lib_nm = to_sjis($lib_nm);
        }
        else if ($filename_flg == 2) { // ff
            $lib_nm = to_utf8($lib_nm);
        }
        else {
            $lib_nm = rawurlencode(to_utf8($lib_nm));
        }
        $file_name = "$lib_nm.$lib_extension";

        ob_end_clean();
        header("Content-Disposition: attachment; filename=\"$file_name\"");
        header("Content-Type: application/octet-stream; name=$file_name");
        header("Content-Length: " . filesize($file_path));
        readfile($file_path);
    }
    else {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    }
}
else {
    ob_end_clean();
    header("Location: $lib_url");
}