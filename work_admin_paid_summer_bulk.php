<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理｜対象者検索</title>
<?
require_once("about_comedix.php");
require_once("show_class_name.ini");
require_once("referer_common.ini");
require_once("show_attendance_pattern.ini");
require_once("date_utils.php");
require_once("show_select_values.ini");
require_once("atdbk_common_class.php");
require_once("work_admin_paid_summer_common.php");
require_once("show_timecard_common.ini");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 勤務管理権限チェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


// DBコネクション作成
$con = connect2db($fname);
//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);

// 遷移元の設定
if ($work != "") {
	set_referer($con, $session, "work", $work, $fname);
	$referer = $work;
} else {
	$referer = get_referer($con, $session, "work", $fname);
}

//付与年月日入力の場合
if ($select_add_yr != "-" && $select_add_yr != "") {
	$year = $select_add_yr;
}

//有給付与日数表 
$sql = "select paid_hol_tbl_id, add_day1 from timecard_paid_hol_summer";
$cond = "where paid_hol_tbl_id in ('1','6','7') order by order_no";               //夏休の付与日数表は 常勤:1、定時:6、パート:7 のみ
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$arr_paid_hol = array();
while($row = pg_fetch_array($sel)){

    $paid_hol_tbl_id = $row["paid_hol_tbl_id"];
    $arr_paid_hol[$paid_hol_tbl_id] = $row["add_day1"];
    
}

// 該当する職員を検索
$sel_emp = false;

if ($srch_flg == "1") {
    
	$sql = get_paid_srch_sql($srch_name, $cls, $atrb, $dept, $group_id, $select_add_yr, $select_add_mon, $select_add_day, $year, $sort);
    $cond = get_paid_srch_cond($srch_name, $cls, $atrb, $dept, $group_id, $select_add_yr, $select_add_mon, $select_add_day, $year, $srch_id);
    
    $sel_emp = select_from_table($con, $sql, $cond, $fname);
	if ($sel_emp == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

    $wk_add_date=$select_add_yr2.$select_add_mon2.$select_add_day2;

    //登録対象データループ
    while ($row = pg_fetch_array($sel_emp)) {

        $wk_emp_id=$row["emp_id"];
        $wk_paid_hol_tbl_id=$row["paid_hol_tbl_id"];

        //
        $sql = "select count(id) as kekka from emppaid_summer";
        $cond= "where emp_id='$wk_emp_id' and add_date='$wk_add_date'";

        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $wk_kekka = pg_fetch_result($sel, 0, "kekka");
        if ($wk_kekka==0){
        
            //
            $sql = "select max(id)+1 as cnt from emppaid_summer";
            $cond= "";
            $sel = select_from_table($con, $sql, $cond, $fname);
            if ($sel == 0) {
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $wk_id = pg_fetch_result($sel, 0, "cnt");
            //一件もデータ無い場合
            if ($wk_id == ""){
                $wk_id = 1;
            }

            //
            $wk_add_days1=0;
            if ($add_days_sel==1){
                $wk_add_days1=$arr_paid_hol[$wk_paid_hol_tbl_id];
            }else{
                $wk_add_days1=$add_days1;
            }
            if ($wk_add_days1 != ""){
                $sql = "insert into emppaid_summer (id, emp_id, add_date, days, lastupdate, paid_hol_tbl_id) values (";
                $content = array($wk_id, $wk_emp_id, $wk_add_date, $wk_add_days1, date("Y-m-d H:i:s"), $wk_paid_hol_tbl_id);

                $ins = insert_into_table($con, $sql, $content, $fname);
                if ($ins == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
                    echo("<script language='javascript'>showErrorPage(window);</script>");
                    exit;
                }
            }
        }
    }

    // トランザクションをコミット
    pg_query("commit");
    // データベース接続を切断
    pg_close($con);

}

$url_srch_name = urlencode($srch_name);

//再表示
echo("<script type=\"text/javascript\">location.href = 'work_admin_paid_summer.php?session=$session&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&group_id=$group_id&srch_flg=$srch_flg&page=$page&select_add_yr=$select_add_yr&select_add_mon=$select_add_mon&select_add_day=$select_add_day&select_close_yr=$select_close_yr&select_close_mon=$select_close_mon&select_close_day=$select_close_day&srch_id=$srch_id';</script>");

?>
