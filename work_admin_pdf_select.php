<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 出勤表 | 出力期間の調整</title>

<?
require_once("Cmx.php");
require_once('Cmx/Model/SystemConfig.php');
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_timecard_common.ini");
require_once("show_select_values.ini");
require_once("show_attendance_pattern.ini");
require_once("atdbk_close_class.php");

$fname = $PHP_SELF;
// データベースに接続
$con = connect2db($fname);

$sql = "select closing, closing_parttime, closing_month_flg, sortkey1, sortkey2, sortkey3, sortkey4, sortkey5, pdf_total_print_flg, pdf_count_print_flg from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {

	//$closing = pg_fetch_result($sel, 0, "closing");
	//$closing_month_flg = pg_fetch_result($sel, 0, "closing_month_flg");
	$sortkey1 = pg_fetch_result($sel, 0, "sortkey1");
	$sortkey2 = pg_fetch_result($sel, 0, "sortkey2");
	$sortkey3 = pg_fetch_result($sel, 0, "sortkey3");
	$sortkey4 = pg_fetch_result($sel, 0, "sortkey4");
	$sortkey5 = pg_fetch_result($sel, 0, "sortkey5");
	$pdf_total_print_flg = pg_fetch_result($sel, 0, "pdf_total_print_flg");
	$pdf_count_print_flg = pg_fetch_result($sel, 0, "pdf_count_print_flg");
} else {
	//$closing = "";
	//$closing_month_flg = "1";
	$sortkey1 = "";
	$sortkey2 = "";
	$sortkey3 = "";
	$sortkey4 = "";
	$sortkey5 = "";
}
if ($pdf_total_print_flg == "") {
	$pdf_total_print_flg = "1";
}
if ($pdf_count_print_flg == "") {
	$pdf_count_print_flg = "1";
}

$year = substr($yyyymm, 0, 4);
$month = intval(substr($yyyymm, 4, 2));

$last_month_y = $year;
$last_month_m = $month - 1;
if ($last_month_m == 0) {
	$last_month_y--;
	$last_month_m = 12;
}
$last_yyyymm = $last_month_y . sprintf("%02d", $last_month_m);

$next_month_y = $year;
$next_month_m = $month + 1;
if ($next_month_m == 13) {
	$next_month_y++;
	$next_month_m = 1;
}
$next_yyyymm = $next_month_y . sprintf("%02d", $next_month_m);

$atdbk_close_class = new atdbk_close_class($con, $fname);
$closing = $atdbk_close_class->get_empcond_closing($top_emp_id);
$closing_month_flg = $atdbk_close_class->closing_month_flg;
// 開始日・締め日を算出
$calced = false;
switch ($closing) {
	case "1":  // 1日
		$closing_day = 1;
		break;
	case "2":  // 5日
		$closing_day = 5;
		break;
	case "3":  // 10日
		$closing_day = 10;
		break;
	case "4":  // 15日
		$closing_day = 15;
		break;
	case "5":  // 20日
		$closing_day = 20;
		break;
	case "6":  // 末日
		$start_year = $year;
		$start_month = $month;
		$start_day = 1;
		$end_year = $start_year;
		$end_month = $start_month;
		$end_day = days_in_month($end_year, $end_month);
		$calced = true;
		break;
}
if (!$calced) {
	$day = date("j");
	$start_day = $closing_day + 1;
	$end_day = $closing_day;

	//当月〜翌月
	if ($closing_month_flg != "2") {
		$start_year = $year;
		$start_month = $month;

		$end_year = $year;
		$end_month = $month + 1;
		if ($end_month == 13) {
			$end_year++;
			$end_month = 1;
		}
	}
	//前月〜当月
	else {
		$start_year = $year;
		$start_month = $month - 1;
		if ($start_month == 0) {
			$start_year--;
			$start_month = 12;
		}

		$end_year = $year;
		$end_month = $month;
	}

}
$start_date = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
$end_date = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);

// 対象年月度を変数にセット
$c_yyyymm = $yyyymm;

//当月〜翌月
if ($closing_month_flg != "2") {
	$default_y = $start_year;
	$default_m = $start_month;
}
//前月〜当月
else {
	$default_y = $end_year;
	$default_m = $end_month;
}

// 組織階層情報取得
$arr_org_level = array ();
$level_cnt = 0;
$sql = "select class_nm, atrb_nm, dept_nm, room_nm, class_cnt from classname ";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo ("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo ("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$arr_org_level[0] = pg_result($sel, 0, "class_nm");
$arr_org_level[1] = pg_result($sel, 0, "atrb_nm");
$arr_org_level[2] = pg_result($sel, 0, "dept_nm");
$arr_org_level[3] = pg_result($sel, 0, "room_nm");

// グループ名を取得 20150820
$group_names = get_timecard_group_names($con, $fname);
// system_config
$conf = new Cmx_SystemConfig();
$pdf_tmcd_group_id = $conf->get('timecard.pdf_tmcd_group_id');
$group_checked = ($pdf_tmcd_group_id != "") ? " checked" : "";
$group_display = ($pdf_tmcd_group_id != "") ? "" : "none";
?>
<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
<!--
//印刷
function print_pdf() {
	if(window.opener && !parent.opener.closed && window.opener.print_pdf){
		window.opener.document.csv.yyyymm.value = document.mainform._yyyymm2.value;
		window.opener.document.csv.select_start_yr.value = document.mainform.start_year.value;
		window.opener.document.csv.select_start_mon.value = document.mainform.start_month.value;
		window.opener.document.csv.select_start_day.value = document.mainform.start_day.value;
		window.opener.document.csv.select_end_yr.value = document.mainform.end_year.value;
		window.opener.document.csv.select_end_mon.value = document.mainform.end_month.value;
		window.opener.document.csv.select_end_day.value = document.mainform.end_day.value;
		window.opener.document.csv.sortkey1.value = document.mainform.sortkey1.value;
		window.opener.document.csv.sortkey2.value = document.mainform.sortkey2.value;
		window.opener.document.csv.sortkey3.value = document.mainform.sortkey3.value;
		window.opener.document.csv.sortkey4.value = document.mainform.sortkey4.value;
		window.opener.document.csv.sortkey5.value = document.mainform.sortkey5.value;
		window.opener.document.csv.default_start_date.value = document.mainform.default_start_date.value;
		window.opener.document.csv.default_end_date.value = document.mainform.default_end_date.value;
		window.opener.document.csv.pdf_total_print_flg.value = (document.mainform.elements['pdf_total_print_flg'][0].checked) ? '1' : '0';
		window.opener.document.csv.pdf_count_print_flg.value = (document.mainform.elements['pdf_count_print_flg'][0].checked) ? '1' : '0';
		window.opener.document.csv.tmcd_group_id.value = '';
		if (document.mainform.chk_group.checked) {
			window.opener.document.csv.tmcd_group_id.value = document.mainform.tmcd_group_id.value;
		}
		window.opener.print_pdf();
		window.close();
	}
}

//月度変更時、期間も変更
function chg_ym(ym) {

	default_y = <?=$default_y?>;
	default_m = <?=$default_m?>;

	yyyy=ym.substring(0, 4);
	mm=eval(ym.substring(4, 6));

	//初期年月との月の差
	diff = (yyyy*12+mm) - (default_y*12+default_m);

	//開始年月を計算し設定
	sy = <?=$start_year?>;
	sm = <?=intval($start_month);?>;
	sd = 1;

	dates = new Date();

	dates.setYear(sy - 1900);
	dates.setMonth(sm - 1);
	dates.setDate(sd);

	dates.setMonth(dates.getMonth()+diff);

	yyyy1 = dates.getYear() + 1900;
	yyyy = (yyyy1 < 1900) ? yyyy1 + 1900 : yyyy1;
	mm = dates.getMonth()+1;
	mm = (mm < 10) ? '0'+mm : mm;

	document.mainform.start_year.value = yyyy;
	document.mainform.start_month.value = mm;

	//終了年月を計算し設定
	ey = <?=$end_year?>;
	em = <?=intval($end_month);?>;
	ed = 1;

	datee = new Date();

	datee.setYear(ey - 1900);
	datee.setMonth(em - 1);
	datee.setDate(ed);

	datee.setMonth(datee.getMonth()+diff);

	yyyy1 = datee.getYear()+1900;
	yyyy = (yyyy1 < 1900) ? yyyy1 + 1900 : yyyy1;
	mm = datee.getMonth()+1;
	mm = (mm < 10) ? '0'+mm : mm;
	document.mainform.end_year.value = yyyy;
	document.mainform.end_month.value = mm;

	set_last_day();
}

//終了日月末設定
function set_last_day() {
	day = document.mainform.end_day.value;
	day = parseInt(day,10);
	if (day >= 28) {
		year = document.mainform.end_year.value;
		month = document.mainform.end_month.value;
		last_day = days_in_month(year, month);
		if (last_day != day) {
			day = last_day;
			dd = (day < 10) ? '0'+day : day;
			document.mainform.end_day.value = dd;
		}
	}

}
//月末取得
function days_in_month(year, month) {
	//月別日数配列
	lastDay = new Array(31,28,31,30,31,30,31,31,30,31,30,31,29);
	//閏年
	wk_year = parseInt(year,10);
	wk_mon = parseInt(month,10);
	if ((wk_mon == 2) &&
		((wk_year % 4) == 0 && ((wk_year % 100) != 0 || (wk_year % 400)))) {
		wk_mon = 13;
	}

	days = lastDay[wk_mon-1];
	return days;
}

function disp_group() {
	document.getElementById('tmcd_group_id').style.display = (document.mainform.chk_group.checked == true) ? '' : 'none';
}
//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
		<tr height="32" bgcolor="#5279a5">
		<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>出力期間の調整</b></font></td>
		<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
		</tr>
	</table>

	<form name="mainform" method="post">

	<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr>
	<td align="left">
	<table width="420" border="0" cellspacing="0" cellpadding="2">
	<tr>
	<td align="left" colspan="5" nowrap><nobr>
<?
echo("<select name=\"_yyyymm2\" onChange=\"chg_ym(this.value);\">\n");
for ($y = date("Y"); $y >= 2004; $y--) {
	for ($m = 12; $m >= 1; $m--) {
		$mm = sprintf("%02d", $m);
		echo("<option value=\"$y$mm\"");
		if ("$y$mm" == $c_yyyymm) {
			echo(" selected");
		}
		echo(">{$y}年{$m}月度\n");
	}
}
echo("</select>");
//勤務グループ 20150820
//<div id="group_sele" style="display:none"></div>
	?>
	<label><input type="checkbox" name="chk_group" onClick="disp_group();"<? echo($group_checked); ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務グループを選択 </font></label>
	<select name="tmcd_group_id" id="tmcd_group_id" style="display:<? echo($group_display); ?>">
	<option value="all">全て</option>
<?
	foreach ($group_names as $tmp_group_id => $group_name) {
		$selected = ($tmp_group_id == $pdf_tmcd_group_id) ? " selected" : "";
		echo("<option value=\"$tmp_group_id\" $selected");
		echo(">$group_name</option>\n");
	}
?>
	</select>
	</nobr>
	</td>
	</tr>
	<tr height="32">
		<td align="left" colspan="5">
			<select name="start_year">
			<? show_select_years(10, $start_year, false, true); ?>
			</select>/<select name="start_month">
			<? show_select_months($start_month, false); ?>
			</select>/<select name="start_day">
			<? show_select_days($start_day, false); ?>
			</select> 〜 <select name="end_year">
			<? show_select_years(10, $end_year, false, true); ?>
			</select>/<select name="end_month">
			<? show_select_months($end_month, false); ?>
			</select>/<select name="end_day">
			<? show_select_days($end_day, false); ?>
			</select>&nbsp;
		</td>
	</tr>
	<tr height="32">
<?
//部署項目名
$arr_sortitem = array("", "職員ID", "氏名", "$arr_org_level[0]", "$arr_org_level[1]", "$arr_org_level[2]", "職種");

for ($key_idx=1; $key_idx<=5; $key_idx++) {

	echo("<td>");

	echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">ソートキー{$key_idx}</font><br>");

	$varname = "sortkey".$key_idx;
	echo("<select name=\"$varname\" id=\"$varname\">");
	$sortkey_val = $$varname;
	for ($i=0; $i<count($arr_sortitem); $i++) {
		//選択
		if ($sortkey_val == $i) {
			$selected = " selected";
		} else {
			$selected = "";
		}
		echo("<option value=\"$i\" $selected>");

		echo($arr_sortitem[$i]);
		echo("</option>");
	}
	echo("</select><br>");
	echo("</td>\n");

}

?>

	</tr>
	<tr height="24">
		<td align="left" colspan="5">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		集計項目の印刷
		<label><input type="radio" id="pdf_total_print_flg" name="pdf_total_print_flg" value="1" <? if ($pdf_total_print_flg == "1") {echo(" checked");} ?>>する</label>
		<label><input type="radio" id="pdf_total_print_flg" name="pdf_total_print_flg" value="0" <? if ($pdf_total_print_flg == "0") {echo(" checked");} ?>>しない</label>
		</font>
		</td>
	</tr>
	<tr height="24">
		<td align="left" colspan="5">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		勤務実績回数の印刷
		<label><input type="radio" id="pdf_count_print_flg" name="pdf_count_print_flg" value="1" <? if ($pdf_count_print_flg == "1") {echo(" checked");} ?>>する</label>
		<label><input type="radio" id="pdf_count_print_flg" name="pdf_count_print_flg" value="0" <? if ($pdf_count_print_flg == "0") {echo(" checked");} ?>>しない</label>
		</font>
	</td>
	</tr>
	<tr height="32">
		<td align="right" colspan="5">
			<input type="button" value="実行" onclick="print_pdf();">&nbsp;

		</td>
	</tr>
	</table>

	</td>
	</tr>
	</table>

	<input type="hidden" name="default_start_date" value="<? echo($start_date); ?>">
	<input type="hidden" name="default_end_date" value="<? echo($end_date); ?>">
	</form>
	<iframe name="download" width="0" height="0" frameborder="0"></iframe>

</body>
<? pg_close($con); ?>
</html>

