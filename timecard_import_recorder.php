<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理 | タイムレコーダ（マックス）</title>
<?
//ini_set("display_errors","1");
ini_set("max_execution_time", 0);
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_class_name.ini");
require_once("referer_common.ini");
require_once("work_admin_menu_common.ini");
require_once("duty_shift_common_class.php");
require_once('timerec_class.php');
require_once("show_select_values.ini");
require_once("timecard_import_menu_common.php");
require_once("atdbk_menu_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
$timerec = new timerec_class($con, $fname);
// 遷移元の取得
$referer = get_referer($con, $session, "work", $fname);

//インポート
if ($mode == "import") {
	// ファイルが正常にアップロードされたかどうかチェック
	$uploaded = false;
	if (array_key_exists("csvfile", $_FILES)) {
		if ($_FILES["csvfile"]["error"] == 0 && $_FILES["csvfile"]["size"] > 0) {
			$uploaded = true;
		}
	}
	if (!$uploaded) {
		echo("<script type=\"text/javascript\">alert('ファイルのアップロードに失敗しました。');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'timecard_import_recorder.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	else {
		$res = $timerec->dakoku_file($_FILES["csvfile"]["tmp_name"], "1", $encoding, "1");
		$errmsgs = $timerec->get_rslt_msg();
	}
}

if ($encoding == "") {
	$encoding = "1";
}

//マッチングキーの更新
if ($mode == "chg_matching_key") {
	$timerec->update_matching_key($matching_key);
}
//職員マッチングキーをDBから取得
$matching_key = $timerec->get_matching_key();

// 年月日の初期値設定
if ($date_y1 == "") {
	$today = date("Y/m/d", strtotime("today"));

	$arr_today = split("/", $today);
	$date_y2 = $arr_today[0];
	$date_m2 = $arr_today[1];
	$date_d2 = $arr_today[2];

	// 前日の日付取得
	$yesterday = date("Y/m/d",strtotime("-1 day" ,strtotime($today)));

	$arr_yesterday = split("/", $yesterday);
	$date_y1 = $arr_yesterday[0];
	$date_m1 = $arr_yesterday[1];
	$date_d1 = $arr_yesterday[2];
}

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
var childwin = null;
function openUpdateWin(ins_upd_flg, no) {
	dx = screen.width;
	dy = screen.top;
	wx = 800;
	wy = 240;
	base = screen.height / 2 - (wy / 2);
	var url = './timecard_import_set.php';
	url += '?session=<?=$session?>';
	url += '&ins_upd_flg='+ins_upd_flg;
	url += '&no='+no;
	childwin = window.open(url, 'timecardimpopup', 'left='+((dx-wx)/2)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}

function closeUpdateWin() {
	if (childwin != null && !childwin.closed) {
		childwin.close();
	}
	childwin = null;
}

function deleteData(no) {

	if (confirm('削除してよろしいですか？')) {

		document.mainform.mode.value = "delete";
		document.mainform.no.value = no;
		document.mainform.action = "timecard_import.php";
		document.mainform.submit();

	}
}

function reload_list() {
	document.mainform.mode.value = "";
	document.mainform.action = "timecard_import.php";
	document.mainform.submit();
}

function checkForm(form) {
	if (form.csvfile.value == '') {
		alert('ファイルを選択してください。');
		return false;
	}
//	alert('職員マッチングキーの更新のみ行います。');
	document.mainform.mode.value = "import";
	return true;
}

//CSVのフォーマット表示
function dispCsvFormat() {
	var disp_stat = document.getElementById('csv_fmt').style.display;
	if (document.getElementById('csv_fmt').style.display == 'none') {
		document.getElementById('csv_fmt').style.display = '';
		document.getElementById('err_srch').style.display = 'none';
	} else {
		document.getElementById('csv_fmt').style.display = 'none';
	}
}
//インポートエラー検索表示
function dispErrorSearch() {
	var disp_stat = document.getElementById('err_srch').style.display;
	if (document.getElementById('err_srch').style.display == 'none') {
		document.getElementById('err_srch').style.display = '';
		document.getElementById('csv_fmt').style.display = 'none';
	} else {
		document.getElementById('err_srch').style.display = 'none';
	}
}
//インポートエラー検索表示
function errorSearch() {
	document.mainform.mode.value = "err_srch";
	document.mainform.submit();

}
//職員マッチングキーの設定変更
function changeMatchingKey() {
	document.mainform.mode.value = "chg_matching_key";
	document.mainform.submit();

}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse; border:#5279a5 solid 1px;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<? echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>"><b>勤務管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>
<?
// メニュータブ
show_work_admin_menuitem($session, $fname, "");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
<tr>
<td>
<?
//インポートメニュータブ表示
show_timecard_import_menuitem($session,$fname);
?>
</td>
</tr>
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="2" alt=""><br>
<form id="mainform" name="mainform" action="timecard_import_recorder.php" method="post" enctype="multipart/form-data" onsubmit="return checkForm(this);">

<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="140" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員マッチングキー</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="matching_key" value="1"<? if ($matching_key == "1") {echo(" checked");} ?>>職員ID
<input type="radio" name="matching_key" value="2"<? if ($matching_key == "2") {echo(" checked");} ?>>カード製造番号
</font>&nbsp;&nbsp;<input type="button" value="設定変更" onClick="changeMatchingKey();"></td>
</tr>
</table>

<font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイムレコーダからの勤務実績インポート</font>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="140" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">CSVファイル</font></td>
<td><input name="csvfile" type="file" value="" size="50"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ファイルの文字コード</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="encoding" value="1"<? if ($encoding == "1") {echo(" checked");} ?>>Shift_JIS（Windows/Mac標準）
<input type="radio" name="encoding" value="2"<? if ($encoding == "2") {echo(" checked");} ?>>EUC-JP
<input type="radio" name="encoding" value="3"<? if ($encoding == "3") {echo(" checked");} ?>>UTF-8
</font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<?
if ($mode == "import" && $uploaded) {
?>
<font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録結果メッセージ<br>
	<?
	foreach ($errmsgs as $errmsg) {
		echo($errmsg);
	}
	?>
</font>
<?
}
?>
<table width="360" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="dispCsvFormat();"><b>CSVのフォーマット</b></a></font>
</td>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="dispErrorSearch();"><b>インポートエラー検索</b></a></font>
</td>
</table>
<div id="csv_fmt" style="display:none">
<dl>
<dt><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></dt>
<dd><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">下記項目をカンマで区切り、レコードごとに改行してください。</font></dd>
<dd>
<ul>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">端末番号　　　　　　　・・・　　　　タイムレコーダの番号（複数有る場合の識別番号）
</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員マッチングキー　・・・　　　　職員IDまたはカード製造番号</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">打刻日時　　　　　　　・・・　　　　2009/10/16   8:25
</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">打刻区分　　　　　　　・・・　　　　出勤、退勤、呼出、呼退、退復、復退、外出、復帰、戻り</font></li>
</ul>
<dl>
<dt><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">例</font></dt>
<dd><font size="3" face="ＭＳ ゴシック, Osaka" class="j12">1,0000000012,2009/10/16 15:12,呼退</font></dd>
<dd><font size="3" face="ＭＳ ゴシック, Osaka" class="j12">1,0000000011,2009/10/16 15:12,呼出</font></dd>
<dd><font size="3" face="ＭＳ ゴシック, Osaka" class="j12">1,0000000012,2009/10/16 15:12,退勤</font></dd>
<dd><font size="3" face="ＭＳ ゴシック, Osaka" class="j12">1,0000000011,2009/10/16 15:12,出勤</font></dd>
<dd><font size="3" face="ＭＳ ゴシック, Osaka" class="j12">9999,0000000012,2009/10/16 15:28,呼退</font></dd>
<dd><font size="3" face="ＭＳ ゴシック, Osaka" class="j12">9999,0000000011,2009/10/16 15:28,呼退</font></dd>
<dd><font size="3" face="ＭＳ ゴシック, Osaka" class="j12">9999,0000000012,2009/10/16 15:29,呼出</font></dd>
<dd><font size="3" face="ＭＳ ゴシック, Osaka" class="j12">9999,0000000011,2009/10/16 15:28,呼出</font></dd>
</dl>
</dd>
</dl>
</div>
<div id="err_srch" style="display:<? if ($mode != "err_srch") echo("none");?>">
<br>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select id="date_y1" name="date_y1"><? show_select_years(10, $date_y1, false); ?></select>年<select id="date_m1" name="date_m1"><? show_select_months($date_m1, false); ?></select>月<select id="date_d1" name="date_d1"><? show_select_days($date_d1, false); ?></select>日
〜
<select id="date_y2" name="date_y2"><? show_select_years(10, $date_y2, false); ?></select>年<select id="date_m2" name="date_m2"><? show_select_months($date_m2, false); ?></select>月<select id="date_d2" name="date_d2"><? show_select_days($date_d2, false); ?></select>日
&nbsp;
職員ID
<input type="text" name="search_emp_id" value="<?=$search_emp_id?>">
<input type="button" value="検索" onClick="errorSearch();">
</font>

<br>
<?
if ($mode == "err_srch") {
	$start_date = $date_y1.$date_m1.$date_d1;
	$end_date = $date_y2.$date_m2.$date_d2;
	$sql = "select a.*, b.emp_lt_nm, b.emp_ft_nm from timecard_import_error a left join empmst b on b.emp_personal_id = a.emp_personal_id";
	$cond ="where a.duty_date>='$start_date' and a.duty_date<='$end_date' ";
	if ($search_emp_id != "") {
		$cond .=" and a.emp_personal_id like '%".pg_escape_string($search_emp_id)."%' ";
	}
	$cond .=" order by a.duty_date, a.emp_personal_id, a.regist_time asc";
	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$num = pg_num_rows($sel);
	if ($num == 0) {
		echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo("検索条件に合うエラーはありませんでした。");
		echo("</font>");
	} else {
?>
<table width="1000" border="0" cellspacing="0" cellpadding="2" class="list">
<tr>
<td width="80" align="center" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
<td align="center" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID</font></td>
<td align="center" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員名</font></td>
<td align="center" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">データ内容</font></td>
<td align="center" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">エラー理由</font></td>
<td width="160" align="center" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">処理時刻</font></td>
</tr>
		<?
		for ($i=0; $i<$num; $i++) {
			$duty_date = pg_fetch_result($sel, $i, "duty_date");
			$emp_personal_id = pg_fetch_result($sel, $i, "emp_personal_id");
			$emp_name = pg_fetch_result($sel, $i, "emp_lt_nm")." ".pg_fetch_result($sel, $i, "emp_ft_nm");
			$data = pg_fetch_result($sel, $i, "data");
			$error_reason = pg_fetch_result($sel, $i, "error_reason");
			$regist_time = pg_fetch_result($sel, $i, "regist_time");

			echo("<tr>\n");
			echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".substr($duty_date,0,4)."/".substr($duty_date,4,2)."/".substr($duty_date,6,2)."</font></td>\n");
			echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$emp_personal_id</font></td>\n");
			echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$emp_name</font></td>\n");
			echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$data</font></td>\n");
			echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$error_reason</font></td>\n");
			echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".substr($regist_time,0,4)."/".substr($regist_time,4,2)."/".substr($regist_time,6,2)." ".substr($regist_time,8,2).":".substr($regist_time,10,2).":".substr($regist_time,12,2)."</font></td>\n");
			echo("</tr>\n");
		}
		echo("</table>\n");
	}
	?>
</font>
	<?
}
?>
</div>


<font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">　</font>

<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="mode" value="">
<input type="hidden" name="no" value="">

</form>
</td>
</tr>
</table>


</body>
<? pg_close($con); ?>
</html>

