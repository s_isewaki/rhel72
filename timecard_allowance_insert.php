<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">

<?
require_once("about_comedix.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// opt以下のphpを表示するかどうか
$opt_display_flag = file_exists("opt/flag");

if ($allow_contents == "") {
	echo("<script type=\"text/javascript\">alert('手当が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
// 入力種別
if ($input_type == "" && $opt_display_flag) {
	echo("<script type=\"text/javascript\">alert('入力種別を選択してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
// 入力チェック
if ($allow_cd == "" && $opt_display_flag) {
	echo("<script type=\"text/javascript\">alert('手当コードが入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if (strlen($allow_contents) > 40) {
	echo("<script type=\"text/javascript\">alert('手当が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if ($allow_price == "") {
	echo("<script type=\"text/javascript\">alert('金額が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if (preg_match("/^[1-9]\d*/", $allow_price) == 0) {
	echo("<script type=\"text/javascript\">alert('金額は半角数字で入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 手当IDを採番
$sql = "select max(allow_id) from tmcdallow";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$allow_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

// 手当を登録
$sql = "insert into tmcdallow (allow_id, allow_contents, allow_price, input_type, allow_cd) values (";
$content = array($allow_id, $allow_contents, $allow_price, $input_type, $allow_cd);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// 画面を再表示
echo("<script type=\"text/javascript\">location.href = 'timecard_allowance.php?session=$session';</script>");
?>

