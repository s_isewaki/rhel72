<?
require_once("about_comedix.php");
?>
<form id="items" name="items" action="facility_reservation_register.php" method="post">
<body>
<input type="hidden" name="back" value="t">
<input type="hidden" name="cate_id" value="<? echo($cate_id); ?>">
<input type="hidden" name="facility_id" value="<? echo($facility_id); ?>">
<input type="hidden" name="sync" value="<? echo($sync); ?>">
<?
foreach ($sync_fcls as $sync_fcl) {
    echo("<input type=\"hidden\" name=\"sync_fcls[]\" value=\"$sync_fcl\">\n");
}
?>
<input type="hidden" name="year" value="<? echo($year); ?>">
<input type="hidden" name="month" value="<? echo($month); ?>">
<input type="hidden" name="day" value="<? echo($day); ?>">
<input type="hidden" name="s_hour" value="<? echo($s_hour); ?>">
<input type="hidden" name="s_minute" value="<? echo($s_minute); ?>">
<input type="hidden" name="e_hour" value="<? echo($e_hour); ?>">
<input type="hidden" name="e_minute" value="<? echo($e_minute); ?>">
<input type="hidden" name="title" value="<? eh($title); ?>">
<input type="hidden" name="marker" value="<? echo($marker); ?>">
<input type="hidden" name="content" value="<? eh($content); ?>">
<input type="hidden" name="users" value="<? echo($users); ?>">
<input type="hidden" name="path" value="<? echo($path); ?>">
<input type="hidden" name="range" value="<? echo($range); ?>">
<input type="hidden" name="mygroup" value="<? echo($mygroup); ?>">
<input type="hidden" name="target_id_list" value="<? echo($target_id_list); ?>">
<input type="hidden" name="pub_limit" value="<? echo($pub_limit); ?>">
<input type="hidden" name="pub_limit_dept" value="<? echo($pub_limit_dept); ?>">
<input type="hidden" name="pub_limit_group_id" value="<? echo($pub_limit_group_id); ?>">
<input type="hidden" name="repeat_type" value="<? echo($repeat_type); ?>">
<input type="hidden" name="rpt2_1" value="<? echo($rpt2_1); ?>">
<input type="hidden" name="rpt2_2" value="<? echo($rpt2_2); ?>">
<input type="hidden" name="rpt3_1" value="<? echo($rpt3_1); ?>">
<input type="hidden" name="rpt3_2" value="<? echo($rpt3_2); ?>">
<input type="hidden" name="rpt3_3" value="<? echo($rpt3_3); ?>">
<input type="hidden" name="rpt_end_year" value="<? echo($rpt_end_year); ?>">
<input type="hidden" name="rpt_end_month" value="<? echo($rpt_end_month); ?>">
<input type="hidden" name="rpt_end_day" value="<? echo($rpt_end_day); ?>">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="calendar" value="<? echo($calendar); ?>">
<input type="hidden" name="repeat" value="<? echo($repeat); ?>">
<input type="hidden" name="datelist" value="<? echo($datelist); ?>">
<input type="hidden" name="pt_ids" value="<? echo($pt_ids); ?>">
<input type="hidden" name="pt_nms" value="<? echo($pt_nms); ?>">
<input type="hidden" name="timeless" value="<? echo($timeless); ?>">
<input type="hidden" name="sendmail" value="<? echo($sendmail); ?>">
<input type="hidden" name="emp_ext" value="<? echo($emp_ext); ?>">
<?
foreach ($type_ids as $type_id) {
    echo("<input type=\"hidden\" name=\"type_ids[]\" value=\"$type_id\">\n");
}
?>
</form>
<script type="text/javascript">
function appendLog(val) {
    var ipt = document.createElement('input');
    ipt.type = 'hidden';
    ipt.name = 'log[]';
    ipt.value = val;

    var frm = document.getElementById("items");
    frm.appendChild(ipt);
}
</script>
<?
require_once("get_values.ini");
define('SM_PATH','webmail/');
require_once("webmail/config/config.php");
require_once("facility_common.ini");
require_once("schedule_common.ini");
require_once("Cmx.php");
require_once("aclg_set.php");
require_once("Cmx/Model/SystemConfig.php");
$conf = new Cmx_SystemConfig();

// ファイル名
$fname = $PHP_SELF;

$with_calendar = ($calendar == "on");
$with_repeat = (!$with_calendar && $repeat == "on");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 設備予約権限のチェック
$checkauth = check_authority($session, 11, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// 入力チェック
if (!checkdate($month, $day, $year)) {
    echo("<script type=\"text/javascript\">alert('日付が不正です。');</script>");
//  echo("<script type=\"text/javascript\">document.items.submit();</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}
if ($with_calendar && $datelist == "") {
    echo("<script type=\"text/javascript\">alert('日付が選択されていません。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}
$s_time = "$s_hour$s_minute";
$e_time = "$e_hour$e_minute";
if ($timeless != "t") {
    if ($s_time >= $e_time) {
        echo("<script type=\"text/javascript\">alert('時刻が不正です。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
}
if ($with_repeat && ($repeat_type == "2" || $repeat_type == "3")) {
    if (!checkdate($rpt_end_month, $rpt_end_day, $rpt_end_year)) {
        echo("<script type=\"text/javascript\">alert('繰り返し終了日が不正です。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
}
if ($title == "") {
    echo("<script type=\"text/javascript\">alert('タイトルが入力されていません。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}
if (strlen($title) > 100) {
    echo("<script type=\"text/javascript\">alert('タイトルが長すぎます。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}
if ($users != "") {
    if (preg_match("/^[0-9]{1,4}$/", $users) == 0) {
        echo("<script type=\"text/javascript\">alert('利用人数は半角数字1〜4桁で入力してください。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
}

if ($pub_limit == "d") {
    $pub_limit = "d" . $pub_limit_dept;
} else if ($pub_limit == "g") {
    $pub_limit = $pub_limit_group_id;
}
if ($pub_limit == "") {
    $pub_limit = null;
}

// 登録対象日を配列で取得
$start_date = "$year$month$day";
$end_date = "$rpt_end_year$rpt_end_month$rpt_end_day";
$arr_date = array();
if ($with_calendar) {
    $arr_date = explode(",", $datelist);
} else if ($with_repeat) {
    switch ($repeat_type) {
    case "1":  // 繰り返さない
        array_push($arr_date, $start_date);
        break;
    case "2":  // 繰り返しタイプ1
        $arr_date = get_target_date1($start_date, $end_date, $rpt2_1, $rpt2_2);
        break;
    case "3":  // 繰り返しタイプ2
        $arr_date = get_target_date2($start_date, $end_date, $rpt3_1, $rpt3_2, $rpt3_3);
        break;
    }
} else {
    array_push($arr_date, $start_date);
}
if (count($arr_date) == 0) {
    echo("<script type=\"text/javascript\">alert('登録対象日がありません。');</script>");
//  echo("<script type=\"text/javascript\">document.items.submit();</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}

// 登録値の編集
if ($show_type_flg == "t") {
    $type1 = isset($type_ids[0]) ? $type_ids[0] : null;
    $type2 = isset($type_ids[1]) ? $type_ids[1] : null;
    $type3 = isset($type_ids[2]) ? $type_ids[2] : null;
} else {
    $type1 = null;
    $type2 = null;
    $type3 = null;
}

// ログインユーザの職員情報を取得
$sql = "select emp_id, emp_lt_nm, emp_ft_nm, get_mail_login_id(emp_id) from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
}
$reg_emp_id = pg_fetch_result($sel, 0, "emp_id");
$emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . pg_fetch_result($sel, 0, "emp_ft_nm");
$emp_login_id = pg_fetch_result($sel, 0, 3);

// 予約可能期間の確認
list($ret, $term) = check_term($con, $fname, $facility_id, $arr_date, $reg_emp_id);
if (!$ret) {
    echo("<script type=\"text/javascript\">alert('予約可能期間内($term)の日付を指定してください。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}
foreach ($sync_fcls as $sync_fcl) {
    list($ret, $term) = check_term($con, $fname, $sync_fcl, $arr_date, $reg_emp_id);
    if (!$ret) {
        echo("<script type=\"text/javascript\">alert('予約可能期間内($term)の日付を指定してください。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
}
// テンプレートの場合XML形式のテキスト$contentを作成
$fclhist_info = get_fclhist($con, $fname, $facility_id, 0);
if ($fclhist_info["fclhist_content_type"] == "2") {
    $ext = ".php";
    $savexmlfilename = "fcl/tmp/{$session}_x{$ext}";

    // ワークフロー情報取得
    $tmpl_content = $fclhist_info["fclhist_content"];
    $tmpl_content_xml = preg_replace("/^.*<\? \/\/ XML/s", "<? // XML", $tmpl_content);
    $fp = fopen($savexmlfilename, "w");
    if (!$fp) {
        echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、登録してください。$savexmlfilename');</script>");
        echo("<script language='javascript'>history.back();</script>");
        exit;
    }
    if(!fwrite($fp, $tmpl_content_xml, 2000000)) {
        fclose($fp);
        echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、登録してください。');</script>");
        echo("<script language='javascript'>history.back();</script>");
        exit;
    }
    fclose($fp);

    include( $savexmlfilename );

    // 個人スケジュール用内容編集
    $schd_content = get_text_from_xml($content);
} else {
// テキストの場合
    $schd_content = $content;
}

// トランザクションの開始
pg_query($con, "begin transaction");

// 登録対象設備情報を配列に格納
$facilities = array();
$facilities[$facility_id] = get_facility_name($con, $facility_id, $fname);
if ($sync == "t") {
    foreach ($sync_fcls as $sync_fcl) {
        $facilities[$sync_fcl] = get_facility_name($con, $sync_fcl, $fname);
    }
}

// スケジュール登録対象職員を配列化
$arr_emp_id = array();
if ($target_id_list != "") {
    $arr_emp_id = array_merge($arr_emp_id, explode(",", $target_id_list));
}

// 登録対象患者を配列化
$pt_ids = ($pt_ids != "") ? explode(",", $pt_ids) : array();

// 繰返しフラグ
$repeat_flg = (count($arr_date) > 1) ? "t" : "f";
// 個人スケジュール連携フラグ
$schd_flg = (count($arr_emp_id) > 0) ? "t" : "f";
// 同時予約フラグ
$sync_flg = ($sync == "t") ? "t" : "f";
// 登録日時
$reg_time = get_millitime();

//重複許可フラグ取得 "t" or "f"
$double_flg = get_double_flg($con, $fname, $cate_id);

// 予約が全て失敗した際のメール送信フラグ
if ($sendmail == 't') {
	$chk = 'f';
}

$success_fcl = array();

// 登録対象日をループ
$refresh_date = "";
foreach ($arr_date as $date) {
    $y = substr($date, 0, 4);
    $m = substr($date, 4, 2);
    $d = substr($date, 6, 2);
    $date_for_log = "$y/$m/$d";

    // 予約済みフラグをfalseで初期化
    $is_date_registered = false;

    // 登録対象職員をループ
    foreach ($arr_emp_id as $tmp_emp_id) {

        // スケジュールIDを採番、採番用テーブルを更新
        $schd_id = get_schd_id($con, $fname, $timeless);

        // スケジュール情報を作成
        $tmp_status = ($tmp_emp_id == $reg_emp_id) ? "1" : "2";
		$aprv_ctrl = $conf->get("schedule.aprv_ctrl");
		if($aprv_ctrl == "2")
		{
			//承認依頼しない(入力画面で指定)
			$tmp_status = "3";
		}

        if ($timeless != "t") {
            // 24:00対応
            $schd_start_time_v = substr($schd_s_time, 0, 2).substr($schd_s_time, 3, 2);
            $schd_dur_v = substr($schd_e_time, 0, 2).substr($schd_e_time, 3, 2);
            if ($schd_e_time == "24:00") {
                $schd_e_time = "23:59";
            }

            $sql = "insert into schdmst (schd_id, emp_id, schd_title, schd_plc, schd_type, schd_imprt, schd_start_date, schd_start_time, schd_day_flg, schd_dur, schd_detail, schd_place_id, schd_reg_id, schd_status, repeat_flg, reg_time, schd_start_time_v, schd_dur_v, marker, pub_limit) values(";
            $contents = array($schd_id, $tmp_emp_id, p($title), $facilities[$facility_id], "1", "2", $date, $schd_s_time, "f", $schd_e_time, p($schd_content), null, $reg_emp_id, $tmp_status, $repeat_flg, $reg_time, $schd_start_time_v, $schd_dur_v, $marker, $pub_limit);
        } else {
            $sql = "insert into schdmst2 (schd_id, emp_id, schd_title, schd_plc, schd_type, schd_imprt, schd_start_date, schd_detail, schd_place_id, schd_reg_id, schd_status, repeat_flg, reg_time, marker, pub_limit) values(";
            $contents = array($schd_id, $tmp_emp_id, p($title), $facilities[$facility_id], "1", "2", $date, p($schd_content), null, $reg_emp_id, $tmp_status, $repeat_flg, $reg_time, $marker, $pub_limit);
        }
        $ins = insert_into_table($con, $sql, $contents, $fname);
        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }

    // 設備をループ
    foreach ($facilities as $tmp_facility_id => $tmp_facility_name) {
        if ($timeless != "t") {
            //重複許可フラグがしないの場合のみ、重複チェック
            if ($double_flg == "f") {
                // 既に予約されている場合
                $sql = "select count(*) from reservation";
                $cond = "where facility_id = $tmp_facility_id and date = '$date' and not (start_time >= '$e_time' or end_time <= '$s_time') and reservation_del_flg = 'f'";
                $sel = select_from_table($con, $sql, $cond, $fname);
                if ($sel == 0) {
                    pg_query("rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
                if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
                    // 失敗ログを出力し、次の設備へ
                    echo("<script type=\"text/javascript\">appendLog('$date_for_log,$tmp_facility_name,失敗（すでに予約あり）');</script>");
                    continue;
                }
            }
            // 予約IDを採番
            $reservation_id = get_reservation_id($con, $fname);

            // 予約レコードを作成
            $sql = "insert into reservation (reservation_id, fclcate_id, facility_id, emp_id, date, start_time, end_time, title, content, users, type1, type2, type3, marker, repeat_flg, schd_flg, sync_flg, reg_time, fclhist_id, pt_name, emp_ext) values (";
            $contents = array($reservation_id, $cate_id, $tmp_facility_id, $reg_emp_id, $date, $s_time, $e_time, p($title), p($content), $users, $type1, $type2, $type3, $marker, $repeat_flg, $schd_flg, $sync_flg, $reg_time, $fclhist_info["fclhist_id"], $pt_nms, $emp_ext);
        } else {
            // 予約IDを採番
            $reservation_id = get_reservation_id2($con, $fname);

            // 予約レコードを作成
            $sql = "insert into reservation2 (reservation_id, fclcate_id, facility_id, emp_id, date, title, content, users, type1, type2, type3, marker, repeat_flg, schd_flg, sync_flg, reg_time, fclhist_id, pt_name, emp_ext) values (";
            $contents = array($reservation_id, $cate_id, $tmp_facility_id, $reg_emp_id, $date, p($title), p($content), $users, $type1, $type2, $type3, $marker, $repeat_flg, $schd_flg, $sync_flg, $reg_time, $fclhist_info["fclhist_id"], $pt_nms, $emp_ext);
        }

        $ins = insert_into_table($con, $sql, $contents, $fname);
        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        $table_name2 = ($timeless != "t") ? "rsvpt" : "rsvpt2";
        // 患者情報をループ
        $order_no = 1;
        foreach ($pt_ids as $tmp_pt_id) {

            // 予約患者情報を登録
            $sql = "insert into $table_name2 (reservation_id, ptif_id, order_no) values (";
            $contents = array($reservation_id, $tmp_pt_id, $order_no);
            $ins = insert_into_table($con, $sql, $contents, $fname);
            if ($ins == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $order_no++;
        }

        // 成功ログを出力
        echo("<script type=\"text/javascript\">appendLog('$date_for_log,$tmp_facility_name,成功');</script>");
        $chk = 't';
        $success_fcl[$date][$tmp_facility_name] = 't';

        // 予約済みフラグを立てる
        $is_date_registered = true;

        // 処理後に表示する画面の日付を設定
        if ($refresh_date == "") {
            $refresh_date = mktime(0, 0, 0, $m, $d, $y);
        }
    }

    // 予約済みフラグが立っていない場合
    if (!$is_date_registered) {

        // 失敗ログを出力し、次の日付へ
        continue;
    }

}

if ($sendmail == 't' && $chk == 'f') {
    echo("<script type=\"text/javascript\">appendLog('登録通知メールは送信されませんでした');</script>");
    $sendmail = 'f';
}

// トランザクションのコミット
pg_query($con, "commit");

if ($sendmail == "t") {
    $to_login_ids = array();
    foreach ($arr_emp_id as $tmp_emp_id) {
        if ($tmp_emp_id != $reg_emp_id) {
            $sql = "select get_mail_login_id(emp_id), emp_lt_nm, emp_ft_nm from empmst";
            $cond = "where emp_id = '$tmp_emp_id'";
            $sel = select_from_table($con, $sql, $cond, $fname);
            if ($sel == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $tmp_emp_login_id = pg_fetch_result($sel, 0, 0);
            $tmp_emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . pg_fetch_result($sel, 0, "emp_ft_nm");
            $to_login_ids["$tmp_emp_login_id"] = $tmp_emp_nm;
        }
    }

    $subject = "[CoMedix] スケジュール登録のお知らせ";

    $message = "下記のスケジュールが登録されました。\n\n";
    $message .= "登録者　：{$emp_nm}\n";
    $message .= "タイトル：{$title}\n";

    // 予約が成功した設備と、予約した時間帯をメールで送信する
    foreach ($facilities as $fcl_name) {
    	$mail_schd_date_times = array();
        foreach ($success_fcl as $date_key => $value) {
            if ($success_fcl[$date_key][$fcl_name] == 't') {
            	$mail_schd_date = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $date_key);
                if ($i == 0) {
                	$message .= "行先　　：{$fcl_name}\n";
                    $mail_schd_time = ($timeless == "t") ? "時刻指定なし" : "{$schd_s_time}〜{$schd_e_time}";
                    $i = 1;
                } else {
                    $mail_schd_time = "　〃";
                }
                $mail_schd_date_times[] = "$mail_schd_date $mail_schd_time";
            }
        }
        if (!empty($mail_schd_date_times)) {
            $mail_schd_date_time = implode("\n　　　　　", $mail_schd_date_times);
            $message .= "日時　　：{$mail_schd_date_time}\n";
        }
        $i = 0;
    }

    $message .= "詳細　　：{$schd_content}\n";
    $message = mb_convert_kana($message, "KV"); // 半角カナ変換
    $additional_headers = "From: ".mb_encode_mimeheader (mb_convert_encoding($emp_nm,"ISO-2022-JP","AUTO")) ." <$emp_login_id@$domain>";

    foreach ($to_login_ids as $to_login_id => $tmp_emp_nm) {
        $to_addr = mb_encode_mimeheader (mb_convert_encoding($tmp_emp_nm,"ISO-2022-JP","AUTO")) ." <$to_login_id@$domain>";
        mb_send_mail($to_addr, $subject, $message, $additional_headers);
    }
}

// データベース接続を閉じる
pg_close($con);

// 親画面を更新
if ($refresh_date != "") {
    switch ($path) {
    case "1":
        $file = "facility_menu.php?session=$session&range=$range&date=$refresh_date";
        break;
    case "2":
        $file = "facility_week.php?session=$session&range=$range&date=$refresh_date";
        break;
    case "3":
        $file = "facility_month.php?session=$session&range=$range&date=$refresh_date";
        break;
    case "4":
        $file = "facility_year.php?session=$session&range=$range&date=$refresh_date";
        break;
    case "5":
        $file = "facility_week_chart.php?session=$session&range=$range&date=$refresh_date&cate_id=$cate_id&fcl_id=$facility_id";
        break;
    }
    echo("<script type=\"text/javascript\">opener.location.href = '$file';</script>");
}

// 「登録対象日が1日」かつ「登録成功」かつ「対象設備が1件」の場合
if (count($arr_date) == 1 && $refresh_date != "" && count($facilities) == 1) {

    // 自画面を閉じる
    echo("<script type=\"text/javascript\">self.close();</script>");
} else {

    // ログを表示
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
}

//------------------------------------------------------------------------------
// 関数
//------------------------------------------------------------------------------

// 登録対象日付を配列で返す（繰り返しタイプ1）
function get_target_date1($start_date, $end_date, $cond1, $cond2) {

    // 戻り値となる配列を宣言
    $arr = array();

    // パス回数の上限をセット
    switch ($cond1) {
    case "1":  // 毎
        $pass_count = 0;
        break;
    case "2":  // 隔
        $pass_count = 1;
        break;
    }

    // スタート日の情報を変数に格納
    $weekday = date("w", to_timestamp($start_date));  // 曜日
    $day = substr($start_date, 6, 2); // 日

    // 期間ループ内で使用する変数を初期化
    $tmp_pass_count = 0;
    $pass_flag = false;
    $tmp_date = $start_date;

    // 期間をループ
    while ($tmp_date <= $end_date) {

        // 処理日付の曜日を取得
        $tmp_weekday = date("w", to_timestamp($tmp_date));

        // 登録対象日かどうか、および特殊日かどうかをチェック
        switch ($cond2) {
        case "1":  // 日：無条件で登録、無条件で特殊日
            $target_flag = true;
            $special_flag = true;
            break;
        case "2":  // 週：スタート日と曜日が同じであれば登録、登録日なら特殊日
            $target_flag = ($tmp_weekday == $weekday);
            $special_flag = $target_flag;
            break;
        case "3":  // 月：スタート日と日が同じであれば登録、登録日なら特殊日
            $target_flag = (substr($tmp_date, 6, 2) == $day);
            $special_flag = $target_flag;
            break;
        case "4":  // 月、水、金：月・水・金のいずれかであれば登録、金なら特殊日
            $target_flag = ($tmp_weekday == 1 || $tmp_weekday == 3 || $tmp_weekday == 5);
            $special_flag = ($tmp_weekday == 5);
            break;
        case "5":  // 火、木：火・木のいずれかであれば登録、木なら特殊日
            $target_flag = ($tmp_weekday == 2 || $tmp_weekday == 4);
            $special_flag = ($tmp_weekday == 4);
            break;
        case "6":  // 月〜金：土・日以外であれば登録、金なら特殊日
            $target_flag = ($tmp_weekday != 6 && $tmp_weekday != 0);
            $special_flag = ($tmp_weekday == 5);
            break;
        case "7":  // 土、日：土・日のいずれかであれば登録、日なら特殊日
            $target_flag = ($tmp_weekday == 6 || $tmp_weekday == 0);
            $special_flag = ($tmp_weekday == 0);
            break;
        }

        // 登録対象日かつパスフラグが立っていなければ配列に追加
        if ($target_flag && !$pass_flag) {
            array_push($arr, $tmp_date);
        }

        // 特殊日の場合
        if ($special_flag) {

            // パスフラグを立てる
            $pass_flag = true;

            // 次回のパス回数が上限を超える場合、パスをキャンセル
            if (++$tmp_pass_count > $pass_count) {
                $pass_flag = false;
                $tmp_pass_count = 0;
            }
        }

        // 日付を進める
        $tmp_date = next_date($tmp_date);
    }

    return $arr;

}

// 登録対象日付を配列で返す（繰り返しタイプ2）
function get_target_date2($start_date, $end_date, $cond1, $cond2, $cond3) {

    // 戻り値となる配列を宣言
    $arr = array();

    // 処理対象曜日を変数にセット
    switch ($cond3) {
    case "1":  // 日
        $weekday = 0;
        break;
    case "2":  // 月
        $weekday = 1;
        break;
    case "3":  // 火
        $weekday = 2;
        break;
    case "4":  // 水
        $weekday = 3;
        break;
    case "5":  // 木
        $weekday = 4;
        break;
    case "6":  // 金
        $weekday = 5;
        break;
    case "7":  // 土
        $weekday = 6;
        break;
    }

    // スタート日付の月の第1Ｘ曜日を求める
    $y = substr($start_date, 0, 4);
    $m = substr($start_date, 4, 2);
    for ($d = 1; $d <= 7; $d++) {
        if (date("w", mktime(0, 0, 0, $m, $d, $y)) == $weekday) {
            break;
        }
    }
    $tmp_date = "{$y}{$m}0{$d}";

    // スタート月からエンド月までの日付情報を月ごとにtmp配列に格納
    $arr_tmp = array();
    $tmp_ym = substr($tmp_date, 0, 6);
    $end_ym = substr($end_date, 0, 6);
    while ($tmp_ym <= $end_ym) {

        // 年月が配列のキーとして存在しなければ追加（配列で初期化）
        if (!array_key_exists($tmp_ym, $arr_tmp)) {
            $arr_tmp[$tmp_ym] = array();
        }

        // 下層の配列に処理日付を追加
        array_push($arr_tmp[$tmp_ym], $tmp_date);

        // 日付を翌週に進める
        $tmp_date = next_week($tmp_date);

        // 処理日付の年月を変数にセット
        $tmp_ym = substr($tmp_date, 0, 6);
    }

    // 日付情報のうち指定週のもののみをtmp配列にセット
    $arr_tmp2 = array();
    foreach ($arr_tmp as $val) {
        switch ($cond2) {
        case "1":  // 第1
            array_push($arr_tmp2, $val[0]);
            break;
        case "2":  // 第2
            array_push($arr_tmp2, $val[1]);
            break;
        case "3":  // 第3
            array_push($arr_tmp2, $val[2]);
            break;
        case "4":  // 第4
            array_push($arr_tmp2, $val[3]);
            break;
        case "5":  // 最終
            array_push($arr_tmp2, $val[count($val) - 1]);
            break;
        }
    }

    // 対象日付がなければリターン
    if (count($arr_tmp2) == 0) {
        return $arr_tmp2;
    }

    // 指定期間から外れる分を削除
    while ($arr_tmp2[0] < $start_date) {
        array_shift($arr_tmp2);
        if (count($arr_tmp2) == 0) {
            return $arr_tmp2;
        }
    }
    while ($arr_tmp2[count($arr_tmp2) - 1] > $end_date) {
        array_pop($arr_tmp2);
        if (count($arr_tmp2) == 0) {
            return $arr_tmp2;
        }
    }

    // 「毎月／毎隔月／毎3ヶ月ごと」を考慮して戻り値の配列にセット
    switch ($cond1) {
    case "1":  // 毎月
        $step = 1;
        break;
    case "2":  // 毎隔月
        $step = 2;
        break;
    case "3":  // 毎3ヶ月ごと
        $step = 3;
        break;
    }
    for ($i = 0; $i < count($arr_tmp2); $i += $step) {
        array_push($arr, $arr_tmp2[$i]);
    }

    return $arr;
}

// 翌日日付をyyyymmdd形式で取得
function next_date($yyyymmdd) {
    return date("Ymd", strtotime("+1 day", to_timestamp($yyyymmdd)));
}

// 翌週日付をyyyymmdd形式で取得
function next_week($yyyymmdd) {
    return date("Ymd", strtotime("+7 days", to_timestamp($yyyymmdd)));
}

// 日付をタイムスタンプに変換
function to_timestamp($yyyymmdd) {
    $y = substr($yyyymmdd, 0, 4);
    $m = substr($yyyymmdd, 4, 2);
    $d = substr($yyyymmdd, 6, 2);
    return mktime(0, 0, 0, $m, $d, $y);
}

?>
</body>
