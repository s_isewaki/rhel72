<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 病床管理 | マスタ管理 | リハビリ算定</title>
<?
require_once("about_comedix.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 21, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

$default_url = ($section_admin_auth == "1") ? "entity_menu.php" : "building_list.php";

// データベースに接続
$con = connect2db($fname);

// 更新処理
if ($is_postback == "true") {
	for ($i = 1; $i <= 4; $i++) {
		$limit_days_var = "limit_days$i";

		if ($$limit_days_var == "") {
			$err_msg = "期限{$i}が入力されていません。";
			break;
		}
		if (!preg_match('/^\d{1,3}$/', $$limit_days_var)) {
			$err_msg = "期限{$i}は、半角数字3桁以内で入力してください。";
			break;
		}
	}

	if ($err_msg == "") {
		$sql = "update bed_rehabilitation set";
		$set = array("limit_days1", "limit_days2", "limit_days3", "limit_days4");
		$setvalue = array($limit_days1, $limit_days2, $limit_days3, $limit_days4);
		$cond = "";
		$upd = update_set_table($con, $sql, $set ,$setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

// 回復期リハ算定情報を取得
if ($err_msg == "") {
	$sql = "select * from bed_rehabilitation";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	for ($i = 1; $i <= 4; $i++) {
		$limit_days_var = "limit_days$i";
		$$limit_days_var = pg_fetch_result($sel, 0, "limit_days$i");
	}
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<? if ($err_msg != "") { ?>
<script type="text/javascript">
alert('<? echo($err_msg); ?>');
</script>
<? } ?>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a> &gt; <a href="<? echo($default_url); ?>?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; <a href="bed_master_menu.php?session=<? echo($session); ?>"><b>マスタ管理</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="bed_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">
<table width="118" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279A5">
<td height="22" class="spacing" colspan="2"><b><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">管理項目</font></b></td>
</tr>
<? if ($section_admin_auth == "1") { ?>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="entity_menu.php?session=<? echo($session); ?>">診療科</a></font></td>
</tr>
<? } ?>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="building_list.php?session=<? echo($session); ?>">病棟</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_display_setting.php?session=<? echo($session); ?>">表示設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_check_setting.php?session=<? echo($session); ?>">管理項目設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_bulk_inpatient_register.php?session=<? echo($session); ?>">一括登録</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_master_menu.php?session=<? echo($session); ?>">マスタ管理</a></font></td>
</tr>
</table>
</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="85" align="center" bgcolor="#bdd1e7"><a href="bed_master_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マスタ作成</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="bed_master_disease.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病名</font></a></td>
<td width="5">&nbsp;</td>
<td width="105" align="center" bgcolor="#bdd1e7"><a href="bed_exception_setting.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">計算除外理由</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_master_decubation.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">回復期リハ算定</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#5279a5"><a href="bed_master_rehabilitation.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>リハビリ算定</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="115" align="center" bgcolor="#bdd1e7"><a href="bed_master_institution.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">医療機関・施設</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="3"><br>
<form name="mainform" action="bed_master_rehabilitation.php" method="post">
<table width="400" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff" align="center">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リハビリ算定期限分類</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">期限</font></td>
</tr>
<tr valign="top">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">心大血管疾患リハビリテーション</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="limit_days1" value="<? echo($limit_days1); ?>" size="4" maxlength="3">日</font></td>
</tr>
<tr valign="top">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">脳血管疾患等リハビリテーション</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="limit_days2" value="<? echo($limit_days2); ?>" size="4" maxlength="3">日</font></td>
</tr>
<tr valign="top">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">運動器リハビリテーション</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="limit_days3" value="<? echo($limit_days3); ?>" size="4" maxlength="3">日</font></td>
</tr>
<tr valign="top">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">呼吸器リハビリテーション
</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="limit_days4" value="<? echo($limit_days4); ?>" size="4" maxlength="3">日</font></td>
</tr>
</table>
<table width="400" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="submit" value="更新"></font></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo(h($session)); ?>">
<input type="hidden" name="is_postback" value="true">
</form>
</td>
</tr>
</table>
</td>
<!-- right -->
</tr>
</table>
</body>
</html>
<? pg_close($con); ?>
